﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;


namespace Controller
{
   public class DescargaController
    {
       public static DataTable fnListaArchivo()
       {
           DataTable dt =DescargaModel.fnListaArchivo();
           
           return dt;
       }
       public static DataTable fnBuscarNombreColumCabecera(String codArchivo,String psTipoArticulo, String pstipoColumna)
       {
           DataTable dt = DescargaModel.fnBuscarNombreColumCabecera(codArchivo,psTipoArticulo, pstipoColumna);
           
           return dt;
       }
       public static Int32 GuardarConfiguracionDescarga(CabeceraDetalleBean obj)
       {
           try
           {
               return Convert.ToInt32(DescargaModel.SubGuardarConfiguracionDescarga(obj));
           }
           catch (Exception e)
           {
               throw new Exception(e.Message);
           }
       }
    }
}
