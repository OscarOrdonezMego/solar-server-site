﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class MantGeneralController
    {
        public static UsuarioBean getLoginWeb(String login, String clave)
        {   UsuarioBean bean = new UsuarioBean();
        DataTable dt = UsuarioModel.getLoginWeb(login, clave);

            if (dt != null && dt.Rows.Count > 0)
            {   
                DataRow row = dt.Rows[0];

              
                bean.nombre = row["usr_nombre"].ToString();
                bean.codigoperfil = row["ven_Perfil"].ToString();
                bean.codigo = row["USR_CODIGO"].ToString();               
                
            }

            return bean;
        }


        public static PaginateMantGeneralBean paginarBuscar(String cgencodigo, String cgennombre, String cgentipo, String FLAG, Int32 page, Int32 rows)
        {
            PaginateMantGeneralBean resultado = new PaginateMantGeneralBean();
            List<MantGeneralBean> list = new List<MantGeneralBean>();

            MantGeneralBean bean;

            DataTable dt = MantGeneralModel.findGeneralBean(cgencodigo, cgennombre, cgentipo, FLAG, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new MantGeneralBean();
                    bean.id = row["gen_PK"].ToString();
                    bean.codigo = row["gen_codigo"].ToString();
                    bean.grupo = row["gen_grupo"].ToString();
                    bean.nombre = row["gen_nombre"].ToString();                   

                    switch (bean.grupo)
                    {
                        case "1":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MOTIVO_NOPEDIDO);
                            break;
                        case "2":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_CONDVENTAUPPERCASE);
                            break;
                        case "3":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_BANCO);
                            break;
                        case "4":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_PAGOSDECOBRANZA);
                            break;
                        case "5":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MOTIVO_CANJE);
                            break;
                        case "6":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MOTIVODEVOLUCION);
                            break;
                        case "7":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_TIPOCLIENTE);
                            break;
                        case "8":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_TIPO_DOCUMENTO);
                            break;
                        case "9":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_FAMILIA);
                            break;
                        case "10":
                            bean.gruponombre = IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MARCA);
                            break;
                        case "11":
                            bean.gruponombre = "Grupo Económico";
                            break;
                        case "12":
                            bean.gruponombre = "Tipo Moneda";
                            break;
                        case "13":
                            bean.gruponombre = "Motivo Anulación Cobranza";
                            break;
                        case "14":
                            bean.gruponombre = "Motivo Cambio Correlativo";
                            break;
                    }

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, rows);

            }

            return resultado;
        }


        public static MantGeneralBean info(String id)
        {
            DataTable dt = MantGeneralModel.info(id);
            MantGeneralBean bean = new MantGeneralBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["gen_pk"].ToString();
                bean.grupo = row["gen_grupo"].ToString();
                bean.codigo = row["gen_codigo"].ToString();
                bean.gruponombre = row["gen_nombre"].ToString();
                if (bean.grupo == "3")
                {
                    bean.nombrecorto = row["gen_nombrecorto"].ToString();
                }
                else
                {
                    bean.nombrecorto = "";
                }

                if (bean.grupo == "4") {
                    bean.banco = row["FLGBANCO"].ToString();
                    bean.nroDocumento = row["FLGDOCUMENTO"].ToString();
                    bean.fechaDiferida = row["FLGFECHADIFERIDA"].ToString();
                }

             }
            return bean;
        }

        public static void crear(MantGeneralBean bean)
        {
            try
            {
                Convert.ToString(MantGeneralModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado( bean.codigo));
                }
                
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static void borrar(String id, String flag)
        {
            MantGeneralModel.borrar(id, flag);
        }
    }
}
