﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

namespace Controller
{
    public class CobranzaController
    {

        public static PaginateCobranzaBean paginarBuscar(String fini,String ffin,String clicod, String vendcod,String codigocliente, String flag, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateCobranzaBean resultado = new PaginateCobranzaBean();
            List<CobranzaBean> list = new List<CobranzaBean>();

            CobranzaBean bean;

            DataTable dt = CobranzaModel.findCobranza(fini,ffin,clicod, vendcod, codigocliente, flag, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new CobranzaBean();
                    bean.id = row["cob_pk"].ToString();
                    bean.cobcodigo = row["cob_codigo"].ToString();
                    bean.vennombre = row["cob_vendedor"].ToString();
                    bean.clicodigo = row["cli_codigo"].ToString();
                    bean.clinombre = row["cli_nombre"].ToString();
                    bean.numdocumento = row["cob_numdocumento"].ToString();
                    bean.tipodocumento = row["cob_tipodocumento"].ToString();
                    bean.montopagadosoles = row["COB_MontoPagadoSoles"].ToString();
                    bean.montototalsoles = row["COB_MontoTotalSoles"].ToString();
                    bean.montopagadodolares = row["COB_MontoPagadoDolares"].ToString();
                    bean.montototaldolares = row["COB_MontoTotalDolares"].ToString();
                    bean.fecvencimiento = row["cob_fecvencimiento"].ToString();
                    bean.serie = row["cob_serie"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }


        public static CobranzaBean info(String id)
        {
            DataTable dt = CobranzaModel.info(id);
            CobranzaBean bean = new CobranzaBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
               
                bean.cobcodigo = row["cob_codigo"].ToString();
                bean.clicodigo = row["cli_codigo"].ToString();
                bean.clinombre = row["cli_nombre"].ToString();
                bean.vencodigo = row["cob_usuario"].ToString();
                bean.vennombre = row["usr_nombre"].ToString();                
                bean.numdocumento = row["cob_numdocumento"].ToString();
                bean.tipodocumento = row["cob_tipodocumento"].ToString();
                bean.montopagadosoles = row["MontoPagadoSoles"].ToString();
                bean.montototalsoles = row["MontoTotalSoles"].ToString();
                bean.montopagadodolares = row["MontoPagadoDolares"].ToString();
                bean.montototaldolares = row["MontoTotalDolares"].ToString();
                bean.fecvencimiento = row["cob_fecvencimiento"].ToString();
                bean.serie = row["cob_serie"].ToString();
            }
            return bean;
        }

        public static void crear(CobranzaBean bean)
        {
            try
            {
                Convert.ToString(CobranzaModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.cobcodigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void editar(CobranzaBean bean)
        {
            try
            {
                Convert.ToString(CobranzaModel.editar(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.clicodigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void borrar(String id, String flag)
        {
            CobranzaModel.borrar(id, flag);
        }

        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
    }
}
