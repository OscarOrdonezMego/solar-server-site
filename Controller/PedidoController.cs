﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

/// <summary>
/// @001 GMC 13/04/2015 Se muestran campos FLGTIPO (P: Pedido - C: Cotización) y TIPO_NOMBRE
/// @002 GMC 14/04/2015 Se muestra campo Dirección de Despacho de Pedido
/// @003 GMC 18/04/2015 Se obtiene Flag para Validar Visualización de Dirección de Despacho
/// @004 GMC 05/05/2015 Se muestra/valida campo Almacén
/// </summary>

namespace Controller
{
    public class PedidoController
    {
        public static PaginatePedidoBean paginarBuscar(String cclicod, String cvendcod, String cFecha, String cflag, String cpage, String crows, String TipoArticulo, String codperfil, String codgrupo, String id_usu, String fTipoPedido, String procesado)
        {
            PaginatePedidoBean resultado = new PaginatePedidoBean();
            List<PedidoBean> list = new List<PedidoBean>();

            PedidoBean bean;

            DataTable dt = PedidoModel.findPedido(cclicod, cvendcod, cFecha, cflag, cpage, crows, TipoArticulo, codperfil, codgrupo, id_usu, fTipoPedido, procesado);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());
                double TOTAL = 0.0;
                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoBean();
                    bean.id = row["ped_pk"].ToString();
                    bean.PED_FECREGISTRO = row["PED_FECREGISTRO"].ToString();
                    bean.PED_FECEDICION = row["PED_FECEDICION"].ToString();
                    bean.PED_CONDVENTA = row["PED_CONDVENTA"].ToString();
					bean.PED_MONTOTOTAL = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL"].ToString();
                    bean.PED_MONTOTOTAL_SOLES = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL_SOLES"].ToString();
                    bean.PED_MONTOTOTAL_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["PED_MONTOTOTAL_DOLARES"].ToString();
                    bean.NOM_USUARIO = row["NOM_USUARIO"].ToString();
                    bean.CONDVTA_NOMBRE = row["CONDVTA_NOMBRE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.PED_DIRECCION = row["PED_DIRECCION"].ToString();
                    bean.TOTALBONIFICACION = row["BONIFICACIONTOTAL"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.FLGTIPO = row["FLGTIPO"].ToString(); //@001 I/F
                    bean.TIPO_NOMBRE = row["TIPO_NOMBRE"].ToString(); //@001 I/F
                    bean.PED_DIRECCION_DESPACHO = row["PED_DIRECCION_DESPACHO"].ToString(); //@002 I/F
                    bean.FlgDireccionDespacho = row["FlgDireccionDespacho"].ToString(); //@003 I/F
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString(); //@004 I/F
                    bean.FLG_MOSTRAR_ALMACEN = row["FlgMostrarAlmacen"].ToString(); //@004 I/F
                    bean.ALM_PK = row["ALM_PK"].ToString();
                    bean.TOTAL_FLETE = row["TOTAL_FLETE"].ToString();
                    bean.MONTO_TOTAL = ManagerConfiguration.moneda + " " + Math.Round(TOTAL,2).ToString();
                    bean.NOMBREGRUPO= row["NOMBREGRUPO"].ToString();
                    bean.ESTADO_PROCESADO = row["ESTADO_PROCESADO"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(crows));
            }
            return resultado;
        }
        public static PedidoBean info(String id)
        {
            DataTable dt = PedidoModel.info(id);
            PedidoBean bean = new PedidoBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = id;// row["ruta_pk"].ToString();
                bean.PED_CODIGO = row["PED_CODIGO"].ToString();
                bean.PED_FECINICIO = row["FECHA"].ToString();
                bean.CLI_CODIGO = row["CLI_CODIGO"].ToString();
                bean.CLI_NOMBRE = row["CLIENTE"].ToString();
                bean.USR_PK = row["USR_PK"].ToString();
                bean.NOM_USUARIO = row["USR_NOMBRE"].ToString();
                bean.PED_CONDVENTA = row["PED_CONDVENTA"].ToString(); //@001 I/F
                bean.FLGTIPO = row["FLGTIPO"].ToString();
                bean.ALM_PK = row["ALM_PK"].ToString(); //@001 I/F
                bean.ESTADO_PROCESADO = row["PROCESADO"].ToString();
                bean.ValorTipoCambioSoles = row["TipoCambioSoles"].ToString();
                bean.ValorTipoCambioDolares = row["TipoCambioDolares"].ToString();
            }
            return bean;
        }
        public static void crear(PedidoBean bean)
        {
            try
            {
                Convert.ToString(PedidoModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.PED_CODIGO));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public static void editar(PedidoBean bean)
        {
            try
            {
                Convert.ToString(PedidoModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.PED_CODIGO));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public static void borrar(String id, String flag)
        {
            PedidoModel.borrar(id, flag);
        }
        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
        public static List<AlmacenBean> fnListaAmacen()
        {
            DataTable dt = PedidoModel.fnListaAlmacen();

            List<AlmacenBean> lst = new List<AlmacenBean>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    AlmacenBean loAlmacenBean = new AlmacenBean();
                    loAlmacenBean.codigo = row["ALM_PK"].ToString();
                    loAlmacenBean.nombre = row["ALM_NOMBRE"].ToString().Trim();
                    lst.Add(loAlmacenBean);
                }

            }
            return lst;
        }
    }
}