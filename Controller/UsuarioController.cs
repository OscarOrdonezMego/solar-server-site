﻿using System;
using System.Collections.Generic;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class UsuarioController
    {
        public static UsuarioBean getLoginWeb(String login, String clave)
        {
            UsuarioBean bean = new UsuarioBean();
            DataTable dt = UsuarioModel.getLoginWeb(login, clave);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                bean.nombre = row["usr_nombre"].ToString();
                bean.codigoperfil = row["ven_Perfil"].ToString();
                bean.codigo = row["USR_CODIGO"].ToString();

            }

            return bean;
        }


        public static UsuarioBean validarUsuario(String login, String clave)
        {
            UsuarioBean loBeanUsuario = new UsuarioBean();
            DataTable dt = UsuarioModel.validarUsuario(login, clave);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                loBeanUsuario.IdResultado = row["Resultado"].ToString();
                loBeanUsuario.codigo = row["Id"].ToString();
                loBeanUsuario.CodigoUsu = row["codigo"].ToString();
                loBeanUsuario.nombre = row["Nombre"].ToString();
                loBeanUsuario.perfil = row["IdPerfil"].ToString();
                loBeanUsuario.id = row["IdSupervisor"].ToString();
                loBeanUsuario.codSupervisor = row["CodSupervisor"].ToString();
                loBeanUsuario.Codgrupo = row["CodGrupo"].ToString();

                dt = UsuarioModel.rolesUsuario(Convert.ToInt32(loBeanUsuario.perfil));
                if (dt != null && dt.Rows.Count > 0)
                {
                    RolBean loRol;
                    loBeanUsuario.hashRol = new Dictionary<String, RolBean>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        loRol = new RolBean();
                        loRol.IdMenu = dr["CodMenu"].ToString();
                        loRol.FlgCrear = dr["FlgCrear"].ToString();
                        loRol.FlgVer = dr["FlgVer"].ToString();
                        loRol.FlgEditar = dr["FlgEditar"].ToString();
                        loRol.FlgEliminar = dr["FlgEliminar"].ToString();
                        loRol.FlgExportar = dr["FlgExportar"].ToString();
                        loBeanUsuario.hashRol.Add(loRol.IdMenu, loRol);
                    }

                    List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
                    loLstConfiguracionBean = ConfiguracionController.subObtenerDatosConfiguracion();
                    foreach (ConfiguracionBean loConfiguracionBean in loLstConfiguracionBean)
                    {
                        loBeanUsuario.hashConfiguracion.Add(loConfiguracionBean.Codigo, loConfiguracionBean);
                    }
                }
            }

            return loBeanUsuario;
        }


        public static PaginateUsuarioBean paginarBuscar(String CODIGO, String LOGIN, String NOMBRE, String CODIGOPERFIL, String FLAG, String CODIGOUSUARIOSESION, Int32 page, Int32 rows, String perfilUsu, String codgrupo, String id_usu, String serie)
        {
            PaginateUsuarioBean resultado = new PaginateUsuarioBean();
            List<UsuarioBean> list = new List<UsuarioBean>();

            UsuarioBean bean;

            DataTable dt = UsuarioModel.findUsuarioBean(CODIGO, LOGIN, NOMBRE, CODIGOPERFIL, page, rows, FLAG, CODIGOUSUARIOSESION, perfilUsu, codgrupo, id_usu, serie);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new UsuarioBean();
                    bean.id = row["USR_PK"].ToString();
                    bean.codigo = row["USR_codigo"].ToString();
                    bean.login = row["USR_login"].ToString();
                    bean.nombre = row["USR_nombre"].ToString();
                    bean.clave = row["USR_clave"].ToString();
                    bean.nombreperfil = row["VEN_PERFIL"].ToString();
                    bean.serie = row["Serie"].ToString();
                    bean.correlativo = row["Correlativo"].ToString();
                    bean.VEN_PERFIL_VALUE = row["VEN_PERFIL_VALUE"].ToString();


                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, rows);

            }


            return resultado;
        }

        public static UsuarioBean info(String id)
        {
            DataTable dt = UsuarioModel.info(id);
            UsuarioBean bean = new UsuarioBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["usr_pk"].ToString();
                bean.codigo = row["usr_codigo"].ToString();
                bean.nombre = row["usr_nombre"].ToString();
                bean.clave = row["usr_clave"].ToString();
                bean.login = row["usr_login"].ToString();
                bean.flag = row["flgenable"].ToString();
                bean.serie = row["Serie"].ToString();
                bean.correlativo = row["Correlativo"].ToString();
                bean.codigoperfil = row["IdPerfil"].ToString();
            }
            return bean;
        }
        public static GrupoBean BuscarGrupo(String codusupk)
        {
            DataTable dt = UsuarioModel.BuscarGrupo(codusupk);
            GrupoBean bean = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean = new GrupoBean();
                bean.codigo = row["Grupo_PK"].ToString();
                bean.Codigo_Grupo = row["Codigo_Grupo"].ToString();
                bean.nombre = row["NombreGrupo"].ToString();

            }
            return bean;
        }

        public static void crear(UsuarioBean bean)
        {
            try
            {
                Convert.ToString(UsuarioModel.crear(bean));
            }

            catch (Exception ex)
            {

                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {

                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else if ("WEB.MANTENIMIENTO.USUARIO.LOGINREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicadoLog(bean.login));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }
        public static void crearGrupoDetalle(UsuarioBean bean)
        {
            try
            {
                Convert.ToString(UsuarioModel.crearGrupDetalle(bean));
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);


            }

        }
        public static void AsignarUsuario(Int32 grupo_pk, String codigousu)
        {
            try
            {
                Convert.ToString(UsuarioModel.AsignarUsuario(grupo_pk, codigousu));
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);


            }

        }
        public static List<GrupoBean> fnListarGrupoActivo(Int32 USUPK, String TIPO)
        {
            List<GrupoBean> loLstGrupoBean = new List<GrupoBean>();
            GrupoBean loGrupoBean = null;
            DataTable loDataTable = UsuarioModel.ListaGruposActivos(USUPK, TIPO);
            try
            {
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.Grupo_PK = Int32.Parse(loDataRow["Grupo_PK"].ToString());
                        loGrupoBean.codigo = loDataRow["Codigo_Grupo"].ToString();
                        loGrupoBean.nombre = loDataRow["NombreGrupo"].ToString();
                        loLstGrupoBean.Add(loGrupoBean);
                    }
                }
                return loLstGrupoBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<GrupoBean> GruposSupervisoresAsignado(Int32 usr_pk, String tipo)
        {
            List<GrupoBean> loLstGrupoBean = new List<GrupoBean>();
            GrupoBean loGrupoBean = null;
            DataTable loDataTable = UsuarioModel.GruposSupervisoresAsignado(usr_pk, tipo);
            try
            {
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.nombre = loDataRow["NombreGrupo"].ToString();
                        loGrupoBean.Grupo_PK = Int32.Parse(loDataRow["Grupo_PK"].ToString());
                        loLstGrupoBean.Add(loGrupoBean);
                    }
                }
                return loLstGrupoBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<GrupoBean> ListaGrupoSupervisor(Int32 usr_pk)
        {
            List<GrupoBean> loLstGrupoBean = new List<GrupoBean>();
            GrupoBean loGrupoBean = null;
            DataTable loDataTable = UsuarioModel.ListaGrupoSupervisor(usr_pk);
            try
            {
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.nombre = loDataRow["NombreGrupo"].ToString();
                        loGrupoBean.Grupo_PK = Int32.Parse(loDataRow["Grupo_PK"].ToString());
                        loLstGrupoBean.Add(loGrupoBean);
                    }
                }
                return loLstGrupoBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<GrupoBean> GrupoVendedores(Int32 usr_pk, String tipo)
        {
            List<GrupoBean> loLstGrupoBean = new List<GrupoBean>();
            GrupoBean loGrupoBean = null;
            DataTable loDataTable = UsuarioModel.GrupoVendedores(usr_pk, tipo);
            try
            {
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.nombre = loDataRow["NombreGrupo"].ToString();
                        loGrupoBean.Grupo_PK = Int32.Parse(loDataRow["Grupo_PK"].ToString());
                        loLstGrupoBean.Add(loGrupoBean);
                    }
                }
                return loLstGrupoBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void EditarGrupoDetalle(UsuarioBean bean)
        {
            try
            {
                Convert.ToString(UsuarioModel.EditarGrupDetalle(bean));
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);


            }

        }
        public static void borrar(String id, String flag)
        {
            UsuarioModel.borrar(id, flag);
        }
        public static void borrarSupervisor(Int32 USR_PK)
        {
            UsuarioModel.BorrarSupervisor(USR_PK);
        }
        public static void EliminarGrupoDetalle(Int32 USR_PK)
        {
            UsuarioModel.EliminarGrupoDetalle(USR_PK);
        }

        public static List<PerfilBean> obtenerListaPerfil(String perfil)
        {
            List<PerfilBean> listaPerfil = new List<PerfilBean>();
            DataTable dataBd = UsuarioModel.getListaPerfil(perfil);
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                PerfilBean p;
                foreach (DataRow row in dataBd.Rows)
                {
                    p = new PerfilBean();
                    p.codigo = row["CodTipoPerfil"].ToString();
                    p.nombre = row["Descripcion"].ToString();
                    listaPerfil.Add(p);
                }
            }
            return listaPerfil;
        }
        public static List<PerfilBean> obtenerListaPerfilCrear(String perfil)
        {
            List<PerfilBean> listaPerfil = new List<PerfilBean>();
            DataTable dataBd = UsuarioModel.getListaPerfilCrear(perfil);
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                PerfilBean p;
                foreach (DataRow row in dataBd.Rows)
                {
                    p = new PerfilBean();
                    p.codigo = row["IdPerfil"].ToString();
                    p.nombre = row["Descripcion"].ToString();
                    p.codigorol = row["CodTipoPerfil"].ToString();
                    listaPerfil.Add(p);
                }
            }
            return listaPerfil;
        }
        public static List<UsuarioBean> ListaVendedores()
        {
            List<UsuarioBean> listaUsuarioBean = new List<UsuarioBean>();
            DataTable dataBd = UsuarioModel.ListaVendedor();
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                UsuarioBean U;
                foreach (DataRow row in dataBd.Rows)
                {
                    U = new UsuarioBean();
                    U.id = row["USR_PK"].ToString();
                    U.codigo = row["USR_CODIGO"].ToString();
                    U.telefono = row["TELEFONO"].ToString();
                    U.nombre = row["USR_NOMBRE"].ToString();
                    listaUsuarioBean.Add(U);
                }
            }
            return listaUsuarioBean;
        }

        public static List<UsuarioBean> ListaSupervisores()
        {
            List<UsuarioBean> listaUsuarioBean = new List<UsuarioBean>();
            DataTable dataBd = UsuarioModel.ListaSupervisores();
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                UsuarioBean U;
                foreach (DataRow row in dataBd.Rows)
                {
                    U = new UsuarioBean();
                    U.id = row["USR_PK"].ToString();
                    U.codigo = row["USR_CODIGO"].ToString();
                    U.nombre = row["USR_NOMBRE"].ToString();
                    listaUsuarioBean.Add(U);
                }
            }
            return listaUsuarioBean;
        }
        public static List<UsuarioBean> ListaVendedorPorSupervisor(Int32 usupk)
        {
            List<UsuarioBean> listaUsuarioBean = new List<UsuarioBean>();
            DataTable dataBd = UsuarioModel.ListaVendedorPorSupervisor(usupk);
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                UsuarioBean U;
                foreach (DataRow row in dataBd.Rows)
                {
                    U = new UsuarioBean();
                    U.id = row["USR_PK"].ToString();
                    U.nombre = row["USR_NOMBRE"].ToString();
                    U.telefono = row["TELEFONO"].ToString();
                    listaUsuarioBean.Add(U);
                }
            }
            return listaUsuarioBean;
        }

        public static List<UsuarioBean> ListaVendedorPorSupervisorMultiple(Int32 usupk, String grupos)
        {
            List<UsuarioBean> listaUsuarioBean = new List<UsuarioBean>();
            DataTable dataBd = UsuarioModel.ListaVendedorPorSupervisorMultiple(usupk,grupos);
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                UsuarioBean U;
                foreach (DataRow row in dataBd.Rows)
                {
                    U = new UsuarioBean();
                    U.id = row["USR_PK"].ToString();
                    U.nombre = row["USR_NOMBRE"].ToString();
                    U.telefono = row["TELEFONO"].ToString();
                    listaUsuarioBean.Add(U);
                }
            }
            return listaUsuarioBean;
        }

    }
}
