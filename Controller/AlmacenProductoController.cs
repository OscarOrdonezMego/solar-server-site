﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class AlmacenProductoController
    {
        public static PaginateAlmacenProductoBean paginarBuscar(String pro_codigo, String pro_nombre, String alm_codigo, String flag, Int32 page, Int32 rows, Int32 poFraccionamientoMaximoDecimales)
        {
            PaginateAlmacenProductoBean resultado = new PaginateAlmacenProductoBean();
            List<AlmacenProductoBean> list = new List<AlmacenProductoBean>();
            AlmacenProductoBean bean;
            DataTable dt = AlmacenProductoModel.findAlmacenProductoBean(pro_codigo, pro_nombre, alm_codigo, flag, page, rows);
            Int32 totalRegistros = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenProductoBean();
                   bean.id_pro_pre = row["ALM_PRO_PK"].ToString();
                    bean.id = row["ALM_PRO_PK"].ToString();
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString();
                    bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.ALM_PRO_STOCK = STOCKPRO(row["ALM_PRO_STOCK"].ToString(), row["PRO_UNIDADDEFECTO"].ToString(), poFraccionamientoMaximoDecimales);
                    bean.PRO_UNIDADDEFECTO = row["PRO_UNIDADDEFECTO"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, rows);
            }
            return resultado;
        }
        public static String STOCKPRO(String STOCK,String PRO_UNIDADDEFECTO, Int32 poFraccionamientoMaximoDecimales){
            String RESULTADO="";
            decimal STOCK1 = decimal.Parse(STOCK);//long.Parse(Math.Round(decimal.Parse(STOCK)).ToString());
            if (STOCK1 == 0){
                RESULTADO = "0";
            }else{
                RESULTADO = STOCK1 + " " + PRO_UNIDADDEFECTO;
            }
            return RESULTADO;
        }
        public static PaginateAlmacenProductoBean paginarBuscarAlmacen(String pro_codigo, String pro_nombre, String alm_codigo, String flag, String presentacion, Int32 page, Int32 rows, Int32 poFraccionamientoMaximoDecimales)
        {
            PaginateAlmacenProductoBean resultado = new PaginateAlmacenProductoBean();
            List<AlmacenProductoBean> list = new List<AlmacenProductoBean>();
            AlmacenProductoBean bean;
            DataTable dt = AlmacenProductoModel.fnAlmacenPresentacion(pro_codigo ,pro_nombre,alm_codigo, flag,presentacion,page, rows);
            Int32 totalRegistros = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenProductoBean();
                  bean.id_pro_pre = row["PK_ALMACEN_PRESENTACION"].ToString();
                    bean.id = row["CODIGOPK_ALMACEN"].ToString();
                    bean.ALM_CODIGO = row["CODIGO_ALMACEN"].ToString();
                    bean.ALM_NOMBRE = row["NOMBRE_ALMACEN"].ToString();
                    bean.PRO_PK = row["CODIGOPK_PRODUCTO"].ToString();
                    bean.PRO_CODIGO = row["CODIGO_PRODUCTO"].ToString();
                    bean.PRO_NOMBRE = row["NOMBRE_PRODUCTO"].ToString();
                    bean.CANTIDAD = row["CANTIDAD"].ToString();
                    bean.CODIGOPK_PRESENTACION =Int32.Parse( row["CODIGOPK_PRESENTACION"].ToString());
                    bean.CODIGO_PRESENTACION = row["CODIGO_PRESENTACION"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.ALM_PRO_STOCK = Stock(row["CANTIDAD"].ToString(), row["STOCK"].ToString(), row["PRO_UNIDADDEFECTO"].ToString(), poFraccionamientoMaximoDecimales);
                    bean.PK_ALMACEN_PRESENTACION = row["PK_ALMACEN_PRESENTACION"].ToString();
                    bean.PRO_UNIDADDEFECTO = row["PRO_UNIDADDEFECTO"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, rows);
            }
            return resultado;
        }
        private static String Stock(String cantidad, String stock, String PRO_UNIDADDEFECTO, Int32 poFraccionamientoMaximoDecimales)
        {
            long cant=0;
            if(cantidad == ""){
                cant=0;
            }else{
                cant=long.Parse( cantidad);
            }
            String resultado = "";
            long PAQUETE = 0;
            long UNID = 0;
            long CANTIDAD = cant;
            decimal STOCK = decimal.Parse(stock);//long.Parse(Math.Round(decimal.Parse(stock)).ToString());
            if (STOCK == 0)
            {
                resultado = "0";
            }else{
                if (cantidad.Equals("0"))
                {
                    resultado = "0";
                }
                else
                {
                    if (poFraccionamientoMaximoDecimales > 0)
                    {
                        resultado = "" + (STOCK / Decimal.Parse(cantidad));
                    }
                    else { 
                        PAQUETE = long.Parse(Math.Round(STOCK).ToString()) / CANTIDAD; //STOCK / CANTIDAD;
                        UNID = long.Parse(Math.Round(STOCK).ToString()) % CANTIDAD; //STOCK % CANTIDAD;
                        resultado = PAQUETE + "-" + UNID + " " + PRO_UNIDADDEFECTO;
                    }
                }                     
                        
            }

            return resultado;

        }
        public static AlmacenProductoBean info(String id,String codigopre)
        {
            DataTable dt = AlmacenProductoModel.info(id,codigopre);
            AlmacenProductoBean bean = new AlmacenProductoBean();
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["PRE_PK"].ToString();
                bean.ALM_PK = row["ALM_PK"].ToString();
                bean.PRO_PK = row["PRO_PK"].ToString();
                bean.ALM_PRO_STOCK = row["STOCK"].ToString();
                bean.ALM_CODIGO = row["ALM_CODIGO"].ToString();
                bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
            }
            return bean;
        }
        public static AlmacenProductoBean info2(String id, String codigopre)
        {
            DataTable dt = AlmacenProductoModel.info6(id, codigopre);
            AlmacenProductoBean bean = new AlmacenProductoBean();
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.ALM_PK = row["ALM_PK"].ToString();
                bean.PRO_PK = row["PRO_PK"].ToString();
                bean.ALM_PRO_STOCK = row["ALM_PRO_STOCK"].ToString();
                bean.ALM_CODIGO = row["ALM_CODIGO"].ToString();
                bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
            }
            return bean;
        }
        public static List<AlmacenProductoBean> infoByProducto(String pro_codigo)
        {
            List<AlmacenProductoBean> list = null;
            DataTable dt = AlmacenProductoModel.infoByProducto(pro_codigo);
            AlmacenProductoBean bean = null;
            list = new List<AlmacenProductoBean>();
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<AlmacenProductoBean>();
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenProductoBean();
                    bean.ALM_CODIGO = row["ALM_CODIGO"].ToString();
                    bean.ALM_PRO_STOCK = row["ALM_PRO_STOCK"].ToString();
                    list.Add(bean);
                }
            }
            return list;
        }
        public static List<AlmacenProductoBean> infoByPresentacion(Int32 pre_pk)
        {
            List<AlmacenProductoBean> list = null;
            DataTable dt = AlmacenProductoModel.infoByPresentacion(pre_pk);
            AlmacenProductoBean bean = null;
            list = new List<AlmacenProductoBean>();
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<AlmacenProductoBean>();
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenProductoBean();
                    bean.ALM_CODIGO = row["ALM_CODIGO"].ToString();
                    bean.ALM_PRO_STOCK = row["STOCK"].ToString();
                    list.Add(bean);
                }
            }
            return list;
        }
        public static void crear(AlmacenProductoBean bean)
        {
            try
            {
                Convert.ToString(AlmacenProductoModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.ALM_CODIGO));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public static void borrar(String id, String flag)
        {
            AlmacenProductoModel.borrar(id, flag);
        }
        public static void borrarProducto(String id, String flag)
        {
            AlmacenProductoModel.borrarProducto(id, flag);
        }
        public static void subCrearAlmacenPresentacion(AlmacenBean bean)
        {
            try
            {
                Convert.ToString(AlmacenProductoModel.fnCrearAlmacenPresentacion(bean));
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }
        }
        public static void subBorrarAlmacenPrsentacion(String id)
        {
            try
            {
                AlmacenProductoModel.fnborrarAlmacenPrsentacion(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           
        }
        public static void subEditarAlmacenPresentacion(AlmacenBean bean)
        {
            try
            {
                Convert.ToString(AlmacenProductoModel.fnEditarAlmacenPresentacion(bean));
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }
        }
        public static void subEditarAlmacenProducto(AlmacenBean bean)
        {
            try
            {
                Convert.ToString(AlmacenProductoModel.fnEditarAlmacenProducto(bean));
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }
        }
    }
}