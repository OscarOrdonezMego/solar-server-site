﻿using System;
using System.Collections.Generic;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class ProductoPresentacionController
    {
        public static List<ProductoPresentacionBean> fnListProductoPaqueteBean()
        {
            List<ProductoPresentacionBean> loLstProductoPaqueteBean = new List<ProductoPresentacionBean>();
            ProductoPresentacionBean loProductoPaqueteBean = null;
            DataTable loDataTable = ProductoPresentacionModel.fnListaProducto();
            if (loDataTable != null && loDataTable.Rows.Count > 0)
            {
                foreach (DataRow loDataRow in loDataTable.Rows)
                {
                    loProductoPaqueteBean = new ProductoPresentacionBean();
                    loProductoPaqueteBean.PrecioPre = loDataRow["ID"].ToString();
                    loProductoPaqueteBean.CodigoProducto = loDataRow["CODIGO"].ToString();
                    loProductoPaqueteBean.Nombre = loDataRow["NOMBRE"].ToString();
                    loLstProductoPaqueteBean.Add(loProductoPaqueteBean);
                }
            }
            return loLstProductoPaqueteBean;
        }
        public static void subCrearPresentacion(ProductoPresentacionBean loProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnCrearPresentacion(loProductoPresentacionBean));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void subCrearListaPrecionPresentacion(ProductoPresentacionBean loProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnCrearListaPrecioPresentacion(loProductoPresentacionBean));
            }
            catch (Exception e)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(e.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeListaPrecioExistente());
                }
                else
                {
                    throw new Exception(e.Message);
                }
            }
        }
        public static void subEditarPresentacion(ProductoPresentacionBean loProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnEditarPresentacion(loProductoPresentacionBean));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void subEditarListaPrecioPresentacion(ProductoPresentacionBean loProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnEditarListaPrecioPresentacion(loProductoPresentacionBean));
            }
            catch (Exception e)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(e.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeListaPrecioExistente());
                }
                else
                {
                    throw new Exception(e.Message);
                }
            }
        }
        public static PaginateProductoPresentacionBean paginarBuscar(ProductoPresentacionBean poProductoPresentacionBean, Int32 poFraccionamientoMaximoDecimales)
        {
            PaginateProductoPresentacionBean loPaginateProductoPresentacionBean = new PaginateProductoPresentacionBean();
            List<ProductoPresentacionBean> loLstProductoPresentacionBean = new List<ProductoPresentacionBean>();

            ProductoPresentacionBean loProductoPresentacionBean;

            DataTable dt = ProductoPresentacionModel.fnListaPrresentacion(poProductoPresentacionBean);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {

                    loProductoPresentacionBean = new ProductoPresentacionBean();
                    loProductoPresentacionBean.CodigoPresentacion = row["ID"].ToString();
                    loProductoPresentacionBean.Codigo = row["CODIGO"].ToString();
                    loProductoPresentacionBean.CodigoProducto = row["IDPRO"].ToString();
                    loProductoPresentacionBean.Nombre = row["NOMBRE"].ToString();
                    loProductoPresentacionBean.NombreProducto = row["NOMBREPRODUCTO"].ToString();
                    loProductoPresentacionBean.Cantidad = row["CANTIDAD"].ToString();
                    loProductoPresentacionBean.UnidaddeFrancionamieno = row["UNIDADFRANCIONADA"].ToString();
                    loProductoPresentacionBean.PrecioPre = row["PRECIO_BASE"].ToString();
                    loProductoPresentacionBean.PrecioBaseSoles = row["PrecioBaseSoles"].ToString();
                    loProductoPresentacionBean.PrecioBaseDolares = row["PrecioBaseDolares"].ToString();
                    loProductoPresentacionBean.Stock = Stock(row["CANTIDAD"].ToString(), row["STOCK"].ToString(), row["PRO_UNIDADDEFECTO"].ToString(), poFraccionamientoMaximoDecimales);
                    loProductoPresentacionBean.Descmin = row["DESC_MIN"].ToString(); //@001 I/F
                    loProductoPresentacionBean.Descmax = row["DESC_MAX"].ToString(); //@001 I/F
                    loProductoPresentacionBean.PRO_UNIDADDEFECTO = row["PRO_UNIDADDEFECTO"].ToString();
                    loLstProductoPresentacionBean.Add(loProductoPresentacionBean);
                }
                loPaginateProductoPresentacionBean.LoLstListaProductoPresentacionBean = loLstProductoPresentacionBean;
                loPaginateProductoPresentacionBean.TotalFila = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(poProductoPresentacionBean.TotalFila.ToString()));
            }
            return loPaginateProductoPresentacionBean;
        }
        private static String Stock(String cantidad, String stock, String PRO_UNIDADDEFECTO, Int32 poFraccionamientoMaximoDecimales)

        {
            String resultado = "";
            long PAQUETE = 0;
            long UNID = 0;
            long CANTIDAD = long.Parse(cantidad);
            decimal STOCK = decimal.Parse(stock);//long.Parse(Math.Round(decimal.Parse(stock)).ToString());

            if (STOCK == 0)
            {
                resultado = "0";
            }
            else
            {
                if (cantidad.Equals("0"))
                {
                    resultado = "0";
                }
                else
                {
                    if (poFraccionamientoMaximoDecimales > 0)
                    {
                        resultado = "" + (STOCK / Decimal.Parse(cantidad));
                    }
                    else
                    {
                        PAQUETE = long.Parse(Math.Round(STOCK).ToString()) / CANTIDAD; //STOCK / CANTIDAD;
                        UNID = long.Parse(Math.Round(STOCK).ToString()) % CANTIDAD; //STOCK % CANTIDAD;
                        resultado = PAQUETE + "-" + UNID + " " + PRO_UNIDADDEFECTO;
                    }
                }

            }

            return resultado;

        }
        public static ProductoPresentacionBean buscarPresentacion(Int32 piCodigo)
        {

            ProductoPresentacionBean loProductoPresentacionBean = null;

            DataTable dt = ProductoPresentacionModel.fnBuscarPresentacion(piCodigo);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    loProductoPresentacionBean = new ProductoPresentacionBean();
                    loProductoPresentacionBean.CodigoPresentacion = row["ID"].ToString();
                    loProductoPresentacionBean.CodigoProducto = row["IDPRO"].ToString();
                    loProductoPresentacionBean.UnidaddeFrancionamieno = row["FRACCIONAMIENTO"].ToString();
                    loProductoPresentacionBean.Nombre = row["NOMBRE"].ToString();
                    loProductoPresentacionBean.NombreProducto = row["NOMBRE_PRO"].ToString();
                    loProductoPresentacionBean.Cantidad = row["CANTIDAD"].ToString();
                    loProductoPresentacionBean.PrecioPre = row["PRECIO_BASE"].ToString();
loProductoPresentacionBean.PrecioBaseSoles = row["PrecioBaseSoles"].ToString();
                    loProductoPresentacionBean.PrecioBaseDolares = row["PrecioBaseDolares"].ToString();
                    loProductoPresentacionBean.Stock = row["STOCK"].ToString();
                    loProductoPresentacionBean.Descmin = row["DESC_MIN"].ToString(); //@001 I/F
                    loProductoPresentacionBean.Descmax = row["DESC_MAX"].ToString(); //@001 I/F
                    loProductoPresentacionBean.Codigo = row["CODIGO"].ToString();

                }

            }
            return loProductoPresentacionBean;

        }
        public static void subBorrarPresentacion(String psCodigo, String psFlag)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnBorrarPresentacion(psCodigo, psFlag));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void subBorrarListaPrecioPresentacion(String psCodigo, String psFlag)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnBorrarListaPrecioPresentacion(psCodigo, psFlag));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static String fnVerSiExiste(ProductoPresentacionBean poProductoPresentacionBean)
        {
            try
            {
                String respuesta = "0";
                DataTable loDataTable = ProductoPresentacionModel.fnVerSiExiste(poProductoPresentacionBean);
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        respuesta = loDataRow["CODIGO"].ToString();
                    }
                }
                return respuesta;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<ProductoPresentacionBean> buscarPresentacionProducto(String psCodigoProducto)
        {

            ProductoPresentacionBean loProductoPresentacionBean = null;
            List<ProductoPresentacionBean> loLstProductoPresentacionBean = new List<ProductoPresentacionBean>();
            DataTable dt = ProductoPresentacionModel.fnBuscarPresentacionProducto(psCodigoProducto);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    loProductoPresentacionBean = new ProductoPresentacionBean();
                    loProductoPresentacionBean.CodigoPresentacion = row["PRE_PK"].ToString();
                    loProductoPresentacionBean.Nombre = row["NOMBRE"].ToString();
                    loProductoPresentacionBean.Cantidad = row["CANTIDAD"].ToString();
                    loLstProductoPresentacionBean.Add(loProductoPresentacionBean);
                }

            }
            return loLstProductoPresentacionBean;

        }
        public static PaginateListaPrecioBean paginarBuscarListaPrecioPresentacion(ListaPrecioBean poListaPrecioBean)
        {
            PaginateListaPrecioBean loPaginateListaPrecioBean = new PaginateListaPrecioBean();
            List<ListaPrecioBean> loLstListaPrecioBean = new List<ListaPrecioBean>();

            ListaPrecioBean loListaPrecioBean;

            DataTable dt = ProductoPresentacionModel.fnListaPrecioPrresentacion(poListaPrecioBean);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    loListaPrecioBean = new ListaPrecioBean();
                    loListaPrecioBean.codigoproducto = row["codigo"].ToString();
                    loListaPrecioBean.producto = row["presentacion"].ToString();
                    loListaPrecioBean.Codpresentacion = row["codigopresentacion"].ToString();
                    loListaPrecioBean.NombreProducto = row["producto"].ToString();
                    loListaPrecioBean.CodProducto = row["codigoProducto"].ToString();
                    loListaPrecioBean.codigocanal = row["canal"].ToString();
                    loListaPrecioBean.condventa = row["cond_vta"].ToString();
                    loListaPrecioBean.precio = row["precio"].ToString();
                    loListaPrecioBean.preciosoles = row["PrecioSoles"].ToString();
                    loListaPrecioBean.preciodolares = row["PrecioDolares"].ToString();
loListaPrecioBean.descmin = row["DESC_MIN"].ToString(); //@001 I/F
                    loListaPrecioBean.descmax = row["DESC_MAX"].ToString(); //@001 I/F

                    loLstListaPrecioBean.Add(loListaPrecioBean);
                }
                loPaginateListaPrecioBean.lstResultados = loLstListaPrecioBean;
                loPaginateListaPrecioBean.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(poListaPrecioBean.TotalFila.ToString()));
            }
            return loPaginateListaPrecioBean;
        }
        public static List<ListaPrecioBean> BuscarListaPrecioPresentacion(Int32 codigo)
        {
            List<ListaPrecioBean> loLstListaPrecioBean = new List<ListaPrecioBean>();

            ListaPrecioBean loListaPrecioBean;

            DataTable dt = ProductoPresentacionModel.BuscarListaPrecioPresentacion(codigo);

            //int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    loListaPrecioBean = new ListaPrecioBean();
                    loListaPrecioBean.codigoproducto = row["codigo"].ToString();
                    loListaPrecioBean.producto = row["presentacion"].ToString();
                    loListaPrecioBean.Codpresentacion = row["codigopresentacion"].ToString();
                    loListaPrecioBean.NombreProducto = row["producto"].ToString();
                    loListaPrecioBean.CodProducto = row["codigoProducto"].ToString();
                    loListaPrecioBean.codigocanal = row["canal"].ToString();
                    loListaPrecioBean.condventa = row["cond_vta"].ToString();
                    loListaPrecioBean.precio = row["precio"].ToString();
loListaPrecioBean.preciosoles = row["PrecioSoles"].ToString();
                    loListaPrecioBean.preciodolares = row["PrecioDolares"].ToString();
                    loListaPrecioBean.descmin = row["DESC_MIN"].ToString(); //@001 I/F
                    loListaPrecioBean.descmax = row["DESC_MAX"].ToString(); //@001 I/F

                    loLstListaPrecioBean.Add(loListaPrecioBean);
                }
            }
            return loLstListaPrecioBean;
        }
        public static PedidoDetalleBean fnBuscarPrecionPresentacion(String psCodigopre)
        {
            PedidoDetalleBean obj = new PedidoDetalleBean();
            //String lsRespuesta = "";
            DataTable loDataTable = ProductoPresentacionModel.fnBuscarPrecioPresentacion(psCodigopre);
            if (loDataTable != null && loDataTable.Rows.Count > 0)
            {
                foreach (DataRow loDataRow in loDataTable.Rows)
                {
                    obj.PRO_PRECIOBASE = loDataRow["PRECIO"].ToString();
                    if (loDataRow["PrecioBaseSoles"].ToString() != string.Empty) obj.PRO_PRECIOBASE_SOLES = loDataRow["PrecioBaseSoles"].ToString();
                    if (loDataRow["PrecioBaseDolares"].ToString() != string.Empty) obj.PRO_PRECIOBASE_DOLARES = loDataRow["PrecioBaseDolares"].ToString();
                }
            }
            return obj;
        }
        public static void subCrearPedidoPresentacionDetalle(ProductoPresentacionBean poProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnCrearDetallePedidoPresentacion(poProductoPresentacionBean));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static PaginatePedidoDetalleBean paginarBuscarListaPedidoPresentacion(PedidoDetalleBean poPedidoDetalleBean)
        {
            PaginatePedidoDetalleBean loPaginatePedidoDetalleBean = new PaginatePedidoDetalleBean();
            List<PedidoDetalleBean> loLstPedidoDetalleBean = new List<PedidoDetalleBean>();

            PedidoDetalleBean loPedidoDetalleBean;

            DataTable dt = ProductoPresentacionModel.fnListaPedidoPresentacionDetalle(poPedidoDetalleBean);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    loPedidoDetalleBean = new PedidoDetalleBean();
                    loPedidoDetalleBean.IDPED = row["CODIGOPED"].ToString();
                    loPedidoDetalleBean.id = row["CODIGODET"].ToString();
                    loPedidoDetalleBean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                    loPedidoDetalleBean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
					loPedidoDetalleBean.PRO_PRECIOBASE = row["PRO_PRECIOBASE"].ToString();
                    loPedidoDetalleBean.PRO_PRECIOBASE_SOLES = ManagerConfiguration.moneda + " " + row["PRO_PRECIOBASE_SOLES"].ToString();
                    loPedidoDetalleBean.PRO_PRECIOBASE_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["PRO_PRECIOBASE_DOLARES"].ToString();
                    loPedidoDetalleBean.CODIGO_PRESENTACION = row["CODIGO_PRESENTACION"].ToString();
                    loPedidoDetalleBean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    loPedidoDetalleBean.CANTIDAD_PRESENTACION = row["CANTIDAD_PRESENTACION"].ToString();
                    loPedidoDetalleBean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
					loPedidoDetalleBean.DET_PRECIOBASE = row["DET_PRECIOBASE"].ToString();
                    loPedidoDetalleBean.DET_PRECIO_SOLES = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE_SOLES"].ToString();
                    loPedidoDetalleBean.DET_PRECIO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_PRECIOBASE_DOLARES"].ToString();
                    loPedidoDetalleBean.DET_CANTIDAD = row["DET_CANTIDAD"].ToString();
					loPedidoDetalleBean.DET_MONTO = row["DET_MONTO"].ToString();
                    loPedidoDetalleBean.DET_MONTO_SOLES = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE_SOLES"].ToString();
                    loPedidoDetalleBean.DET_MONTO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_PRECIOBASE_DOLARES"].ToString();
                    loPedidoDetalleBean.DET_CANTIDAD_FRACCION = row["DET_CANTIDAD_FRACCION"].ToString();
                    loPedidoDetalleBean.DET_DESCUENTO = row["DESCUENTO"].ToString();
                    loPedidoDetalleBean.DET_DESCRIPCION = row["DET_DESCRIPCION"].ToString();
                    loPedidoDetalleBean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    loPedidoDetalleBean.DET_FLETE = row["FLETE"].ToString();
                    loLstPedidoDetalleBean.Add(loPedidoDetalleBean);
                }
                loPaginatePedidoDetalleBean.lstResultados = loLstPedidoDetalleBean;
                loPaginatePedidoDetalleBean.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(poPedidoDetalleBean.TotalFila.ToString()));
            }
            return loPaginatePedidoDetalleBean;
        }
        public static void subEditarPedidoPresentacionDetalle(ProductoPresentacionBean poProductoPresentacionBean)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnEditarDetallePedidoPresentacion(poProductoPresentacionBean));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void subBorrarPedidoDetallePresentacion(String psCodigos, String psflag)
        {
            try
            {
                Convert.ToString(ProductoPresentacionModel.fnBorrarPedidoDetallePresentacion(psCodigos, psflag));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static List<PedidoDetalleBean> fnBuscarPedidoDetallePresentacion(Int32 piCodigo)
        {

            PedidoDetalleBean loPedidoDetalleBean;
            List<PedidoDetalleBean> loLstPedidoDetalleBean = new List<PedidoDetalleBean>();

            DataTable dt = ProductoPresentacionModel.fnBuscarPedidoDetallePresentacion(piCodigo);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    loPedidoDetalleBean = new PedidoDetalleBean();
                    loPedidoDetalleBean.id = row["CODIGODET"].ToString();
                    loPedidoDetalleBean.PRO_CODIGO = row["PRO_PK"].ToString();
                    loPedidoDetalleBean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
					loPedidoDetalleBean.PrecioUnidad = row["PRECIO_UNIDAD"].ToString();
                    loPedidoDetalleBean.PrecioUnidadSoles = row["PRECIO_UND_SOLES"].ToString();
                    loPedidoDetalleBean.PrecioUnidadDolares = row["PRECIO_UND_DOLARES"].ToString();
                    loPedidoDetalleBean.Cod_Presentacion = row["CODIGO_PRESENTACION"].ToString();
                    loPedidoDetalleBean.Nom_presentacion = row["NOMBRE_PRESENTACION"].ToString();
					loPedidoDetalleBean.PrecioPresentacion = row["MONTO_PRE"].ToString();
                    loPedidoDetalleBean.PrecioPresentacionSoles = ManagerConfiguration.moneda + " " + row["PRECIO_PRESENTACION_SOLES"].ToString();
                    loPedidoDetalleBean.PrecioPresentacionDolares = ManagerConfiguration.moneda_dolares + " " + row["PRECIO_PRESENTACION_DOLARES"].ToString();
                    loPedidoDetalleBean.DET_CANTIDAD = row["CANTIDAD"].ToString();
					loPedidoDetalleBean.DET_MONTO = row["PRECIO_DETALLE"].ToString();
                    loPedidoDetalleBean.DET_MONTO_SOLES = ManagerConfiguration.moneda + " " + row["PRECIO_DET_SOLES"].ToString();
                    loPedidoDetalleBean.DET_MONTO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["PRECIO_DET_DOLARES"].ToString();
                    loPedidoDetalleBean.Cant_fraccionada = row["CANT_FRACIONADA"].ToString();
                    loPedidoDetalleBean.DET_DESCUENTO = row["DESCUENTO"].ToString();
                    loPedidoDetalleBean.DET_DESCRIPCION = row["DESCRIPCION"].ToString();
                    loPedidoDetalleBean.DET_BONIFICACION = row["BONIFICACION"].ToString();
                    loPedidoDetalleBean.ALM_PK = Int32.Parse(row["CODIGO_ALMACEN"].ToString());
                    loPedidoDetalleBean.DET_FLETE = row["FLETE"].ToString();
                    loPedidoDetalleBean.PRE_FRACIONADO = row["PRE_FRACIONADO"].ToString();
                    loPedidoDetalleBean.PED_PK = row["PED_PK"].ToString();
                    loPedidoDetalleBean.TipoMoneda = row["TipoMoneda"].ToString();
                    loLstPedidoDetalleBean.Add(loPedidoDetalleBean);

                }

            }
            return loLstPedidoDetalleBean;

        }

    }
}
