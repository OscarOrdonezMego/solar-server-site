using System;
using System.Collections.Generic;
using Model.bean;
using System.Data;
using Model;
using business.functions;

/// <summary>
/// @001 GMC 13/04/2015 Se obtiene lista tipo para Mant Pedido
/// @002 GMC 14/04/2015 Se obtiene lista tipo dirección para Mant Direcciones
/// @003 GMC 16/04/2015 Se obtienen campos Tipo Pedido y Dirección Despacho
/// @004 GMC 16/04/2015 Se agrega filtro Tipo Pedido
/// @005 GMC 16/04/2015 Se muestra campo Direccion para Reporte No Pedido
/// @006 GMC 17/04/2015 Se obtiene Flag para Validar Visualización de Dirección de Despacho
/// @007 GMC 22/04/2015 Se exporta archivo COTIZACION.txt si está habilitado en configuración
/// @008 GMC 05/05/2015 Se muestra/valida campo Almacén
/// @009 GMC 05/05/2015 Se valida visualización de campo almacén para la Descarga
/// </summary>
namespace Controller
{
    public class ReporteController
    {
        #region Graficos
        public static List<GraficoAvanceBean> GraficoAvance(String cfi, String cff, String coperacion)
        {

            List<GraficoAvanceBean> list = new List<GraficoAvanceBean>();

            GraficoAvanceBean bean;

            DataTable dt = ReporteModel.GraficoAvance(cfi, cff, coperacion);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoAvanceBean();

                    bean.fecha = row["fecha"].ToString();
                    bean.monto = row["monto"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }

        public static List<GraficoTopVendedoresBean> GraficoTopVendedores(String cfi, String cff, String ctipo1, String ctipo2)
        {

            List<GraficoTopVendedoresBean> list = new List<GraficoTopVendedoresBean>();

            GraficoTopVendedoresBean bean;

            DataTable dt = ReporteModel.GraficoTopVendedores(cfi, cff, ctipo1, ctipo2);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoTopVendedoresBean();


                    bean.total = row["total"].ToString();
                    bean.avrvendedor = row["avrvendedor"].ToString();
                    bean.vendedor = row["vendedor"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }

        public static PaginateReporteMarcaBean paginaMarcaBuscar(string vendedor, string fini, string ffin, string pagina, string filas, string tipoArticulo, string codperfil, string codgrupo, string id_usu, string marca)
        {
            PaginateReporteMarcaBean resultado = new PaginateReporteMarcaBean();
            List<ReporteMarcaBean> list = new List<ReporteMarcaBean>();
            ReporteMarcaBean bean;
            DataSet dtS = new DataSet();
            if (tipoArticulo == "PRO")
            {
                dtS = ReporteModel.findReporteMarcaBeanPRO(vendedor, fini, ffin,
                              pagina, filas, tipoArticulo, codperfil, codgrupo, id_usu, marca);
            }
            else if (tipoArticulo == "PRE")
            {
                dtS = ReporteModel.findReporteMarcaBeanPRE(vendedor, fini, ffin,
                             pagina, filas, tipoArticulo, codperfil, codgrupo, id_usu, marca);
            }
            DataTable dt = dtS.Tables[0];
            DataTable totalizado = dtS.Tables[1];

            int totalRegistros = 0;
            string totalmonto = "0";
            string totalmontosoles = "0";
            string totalmontodolares = "0";
            string totalcantidad = "0";
            string totalmarcas = "0";
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(totalizado.Rows[0]["TAMANIOTOTAL"].ToString());
                totalmonto = (totalizado.Rows[0]["totalmonto"].ToString());
                totalmontosoles = (totalizado.Rows[0]["totalmontosoles"].ToString());
                totalmontodolares = (totalizado.Rows[0]["totalmontodolares"].ToString());
                totalcantidad = (totalizado.Rows[0]["totalcantidad"].ToString());
                totalmarcas = (totalizado.Rows[0]["totalmarcas"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteMarcaBean();

                    bean.nombre = row["NOMBRE"].ToString();
                    bean.monto = row["MONTO"].ToString();
                    bean.montosoles = row["MONTOSOLES"].ToString();
                    bean.montodolares = row["MONTODOLARES"].ToString();
                    bean.cantidad = row["CANTIDAD"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalmonto = totalmonto;
                resultado.totalmontosoles = totalmontosoles;
                resultado.totalmontodolares = totalmontodolares;
                resultado.totalcantidad = totalcantidad;
                resultado.totalmarcas = totalmarcas;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(filas));

            }

            return resultado;
        }

        public static List<GraficoTopClientesBean> GraficoTopClientes(String cfi, String cff, String ctipo1, String ctipo2)
        {

            List<GraficoTopClientesBean> list = new List<GraficoTopClientesBean>();

            GraficoTopClientesBean bean;

            DataTable dt = ReporteModel.GraficoTopClientes(cfi, cff, ctipo1, ctipo2);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoTopClientesBean();


                    bean.total = row["total"].ToString();
                    bean.avrcliente = row["avrcliente"].ToString();
                    bean.cliente = row["cliente"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }

        public static List<GraficoTopProductosBean> GraficoTopProductos(String cfi, String cff, String ctipo1, String ctipo2)
        {

            List<GraficoTopProductosBean> list = new List<GraficoTopProductosBean>();

            GraficoTopProductosBean bean;

            DataTable dt = ReporteModel.GraficoTopProductos(cfi, cff, ctipo1, ctipo2);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoTopProductosBean();


                    bean.total = row["total"].ToString();
                    bean.avrproducto = row["avrproducto"].ToString();
                    bean.producto = row["producto"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }

        public static List<GraficoIndicadorProductoBean> GraficoIndicadorProducto(String cli, String cls, String cgrafico, String cfi, String cff, String cmeta, String ccodprod, String coperacion)
        {

            List<GraficoIndicadorProductoBean> list = new List<GraficoIndicadorProductoBean>();

            GraficoIndicadorProductoBean bean;

            DataTable dt = ReporteModel.GraficoIndicadorProducto(cli, cls, cgrafico, cfi, cff, cmeta, ccodprod, coperacion);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoIndicadorProductoBean();


                    bean.total = row["total"].ToString();
                    bean.limitesuperior = row["limite_superior"].ToString();
                    bean.limiteinferior = row["limite_inferior"].ToString();
                    bean.meta = row["meta"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }

        public static List<GraficoIndicadorBean> GraficoIndicador(String cli, String cls, String cgrafico, String cfi, String cff, String cmeta, String ccodprod, String coperacion)
        {

            List<GraficoIndicadorBean> list = new List<GraficoIndicadorBean>();

            GraficoIndicadorBean bean;

            DataTable dt = ReporteModel.GraficoIndicador(cli, cls, cgrafico, cfi, cff, cmeta, ccodprod, coperacion);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean = new GraficoIndicadorBean();

                    bean.total = row["total"].ToString();
                    bean.limitesuperior = row["limite_superior"].ToString();
                    bean.limiteinferior = row["limite_inferior"].ToString();
                    bean.meta = row["meta"].ToString();

                    list.Add(bean);



                }

            }

            return list;
        }
        #endregion

        public static DataTable getUSuarios_Ruta()
        {

            DataTable dt = ReporteModel.getUSuarios_Ruta();
            return dt;
        }

        public static DataSet descargaCabecera(String fecinicio, String fecfin, String TipoArticulo, String pVendedor, String pSupervisor, String pgrupo)
        {
            //String NOM = "";
            DataSet ds = ReporteModel.descargaCabecera(fecinicio, fecfin, TipoArticulo, pVendedor, pSupervisor, pgrupo);

            ds.Tables[0].TableName = "PEDIDO";
            ds.Tables[1].TableName = "DETALLE_PEDIDO";
            ds.Tables[2].TableName = "NOVENTA";
            ds.Tables[3].TableName = "CANJE";
            ds.Tables[4].TableName = "DETALLE_CANJE";
            ds.Tables[5].TableName = "DEVOLUCION";
            ds.Tables[6].TableName = "DETALLE_DEVOLUCION";
            ds.Tables[7].TableName = "PAGO";
            ds.Tables[8].TableName = "PROSPECTO";

            //@009 I
            String valorConfig = "F";
            ConfiguracionBean configBean = null;
            //@009 F

            //@007 I
            valorConfig = "F";
            configBean = ConfiguracionController.info(Model.Constantes.CodigoValoresConfiguracion.CFG_COTIZACION);
            if (configBean != null)
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                    valorConfig = configBean.Valor;
            if (valorConfig.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                ds.Tables[9].TableName = "COTIZACION";
            ds.Tables[10].TableName = "DETALLECOTIZACION";
            valorConfig = "F";
            configBean = ConfiguracionController.info(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN);
            if (configBean != null)
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                    valorConfig = configBean.Valor;
            if (valorConfig.Equals(Model.Enumerados.FlagHabilitado.F.ToString()))
            {
                if (ds.Tables[9] != null)
                {
                    ds.Tables[9].TableName = "ALM_CODIGO";
                }
            }
            //@009 F

            return ds;
        }
        public static DataTable PedidoDetalleEdicionList(String xIDpedido, String xIDedicion)
        {
            DataTable dt = ReporteModel.PedidoDetalleEdicionList(xIDpedido, xIDedicion);
            return dt;
        }
        public static DataTable PedidoDetalleList(String xIDpedido)
        {
            DataTable dt = ReporteModel.PedidoDetalleList(xIDpedido);
            return dt;
        }

        public static DataTable PedidoEdicionList(String xIDpedido)
        {
            DataTable dt = ReporteModel.PedidoEdicionList(xIDpedido);
            return dt;
        }

        public static DataTable PedidoDetallePresentacionList(String xIDpedido)
        {
            DataTable dt = ReporteModel.PedidoDetallePresentacionList(xIDpedido);
            return dt;
        }

        public static DataTable CobranzaDetalleList(String vCodigo)
        {
            DataTable dt = ReporteModel.CobroDetalleList(vCodigo);
            return dt;
        }

        #region Excel        

        public static List<ReporteCanjeBean> CanjeBuscarXLS(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String c_codperfil, String c_codgrupo, String id_usu)
        {


            DataTable dt = ReporteModel.findReporteCanjeBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, "-1", "-1", c_codperfil, c_codgrupo, id_usu);


            List<ReporteCanjeBean> list = new List<ReporteCanjeBean>();
            ReporteCanjeBean bean = null;


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCanjeBean();

                    bean.id = row["ID_CAB_CANJE"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CABC_CODCLIENTE = row["CABC_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DETC_CANTIDAD = row["DETC_CANTIDAD"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.DETC_OBSERVACION = row["DETC_OBSERVACION"].ToString();
                    bean.DETC_FECVCMTO = row["DETC_FECVCMTO"].ToString();
                    bean.HABILITADO = "HABILITADO";
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    list.Add(bean);
                }



            }


            return list;
        }

        // REPORTE DE PROSPECTOS
        public static List<ClienteProspeccionBean> ProspectoBuscarXLS(String codvendedor, String prospecto, String fini, String ffin)
        {
            DataTable dt = ClienteProspeccionModel.findClienteProspeccionBean(codvendedor, prospecto, fini, ffin, "-1", "-1");

            List<ClienteProspeccionBean> list = new List<ClienteProspeccionBean>();
            ClienteProspeccionBean bean = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ClienteProspeccionBean();
                    bean.codigo = row["CODIGO"].ToString();
                    bean.nombre = row["NOMBRE"].ToString();
                    bean.direccion = row["DIRECCION"].ToString();
                    bean.usercod = row["USER_COD"].ToString();
                    bean.vendedor = row["VENDEDOR"].ToString();
                    bean.CampoAdicional1 = row["CampoAdicional1"].ToString();
                    bean.CampoAdicional2 = row["CampoAdicional2"].ToString();
                    bean.CampoAdicional3 = row["CampoAdicional3"].ToString();
                    bean.CampoAdicional4 = row["CampoAdicional4"].ToString();
                    bean.CampoAdicional5 = row["CampoAdicional5"].ToString();
                    bean.tclicod = row["TCLI_COD"].ToString();
                    bean.tipocliente = row["TIPOCLIENTE"].ToString();
                    bean.giro = row["GIRO"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    bean.fecha = row["FECHAREGISTRO"].ToString();
                    bean.fechaMovil = row["FECHAMOVIL"].ToString();
                    bean.observacion = row["OBSERVACION"].ToString();

                    list.Add(bean);
                }
            }

            return list;
        }

        public static List<ReporteCanjeBean> CanjeBuscarPresentacionXLS(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String c_codperfil, String c_codgrupo, String id_usu)
        {


            DataTable dt = ReporteModel.findReporteCanjeBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, "-1", "-1", c_codperfil, c_codgrupo, id_usu);


            List<ReporteCanjeBean> list = new List<ReporteCanjeBean>();
            ReporteCanjeBean bean = null;


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCanjeBean();

                    bean.id = row["ID_CAB_CANJE"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CABC_CODCLIENTE = row["CABC_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DETC_CANTIDAD = row["DETC_CANTIDAD"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.DETC_CANTIDAD_PRE = row["DETC_CANTIDAD_PRE"].ToString();
                    bean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                    bean.DETC_CANTIDAD_FRAC = row["DETC_CANTIDAD_FRAC"].ToString();
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.DETC_OBSERVACION = row["DETC_OBSERVACION"].ToString();
                    bean.DETC_FECVCMTO = row["DETC_FECVCMTO"].ToString();
                    bean.HABILITADO = "HABILITADO";
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    list.Add(bean);
                }



            }


            return list;
        }
        public static List<ReporteDevolucionBean> DevolucionBuscarXLS(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String d_codperfil, String d_codgrupo, String id_usu)
        {

            DataTable dt = ReporteModel.findReporteDevolucionBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, "-1", "-1", d_codperfil, d_codgrupo, id_usu);

            List<ReporteDevolucionBean> list = new List<ReporteDevolucionBean>();
            ReporteDevolucionBean bean = null;

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteDevolucionBean();
                    bean.id = row["ID_CAB_DEVOLUCION"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CDEV_CODCLIENTE = row["CDEV_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DDEV_CANTIDAD = row["DDEV_CANTIDAD"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.FECVCMTO = row["FECVCMTO"].ToString();
                    bean.CDEV_OBSERVACION = row["DDEV_OBSERVACION"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.CANTIDA_PRESENTACION = row["CANTIDA_PRESENTACION"].ToString();
                    bean.PRECIO_PRESENTACION = row["PRECIO_PRESENTACION"].ToString();
                    bean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                    bean.DDEV_CANTIDAD_FRAC = row["DDEV_CANTIDAD_FRAC"].ToString();
                    bean.ALMACEN = row["ALMACEN"].ToString();
                    bean.HABILITADO = "HABILITADO";
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }


            }


            return list;
        }
        public static List<ReporteDevolucionBean> DevolucionBuscarSimpleXLS(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String d_codperfil, String d_codgrupo, String id_usu)
        {

            DataTable dt = ReporteModel.findReporteDevolucionBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, "-1", "-1", d_codperfil, d_codgrupo, id_usu);

            List<ReporteDevolucionBean> list = new List<ReporteDevolucionBean>();
            ReporteDevolucionBean bean = null;



            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteDevolucionBean();
                    bean.id = row["ID_CAB_DEVOLUCION"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CDEV_CODCLIENTE = row["CDEV_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.ALMACEN = row["ALMACEN"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DDEV_CANTIDAD = row["DDEV_CANTIDAD"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.FECVCMTO = row["FECVCMTO"].ToString();
                    bean.CDEV_OBSERVACION = row["DDEV_OBSERVACION"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.HABILITADO = "HABILITADO";
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }


            }


            return list;
        }
        public static List<ReporteNoPedidoBean> NoPedidoBuscarXLS(String Vendedor, String FechaIni, String fechaFin,
            String FlgEnCobertura, String a_codperfil, String a_codgrupo, String id_usu, String flagfecreg)
        {

            DataTable dt = ReporteModel.findReporteNoPedidoBean(Vendedor, FechaIni, fechaFin,
                FlgEnCobertura, "-1", "-1", a_codperfil, a_codgrupo, id_usu, flagfecreg);

            List<ReporteNoPedidoBean> list = new List<ReporteNoPedidoBean>();
            ReporteNoPedidoBean bean = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteNoPedidoBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.usr_nombre = row["usr_nombre"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["ped_fecfin"].ToString();
                    bean.cli_codigo = row["cli_codigo"].ToString();
                    bean.cli_nombre = row["cli_nombre"].ToString();
                    bean.direccion = row["PED_DIRECCION"].ToString();
                    bean.mot_nombre = row["mot_nombre"].ToString();
                    bean.habilitado = "HABILITADO";
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }

        public static List<ReportePedidoBean> PedidoBuscarXLS(String Vendedor, String FechaIni, String fechaFin,
            String FlgEnCobertura, String FlgBonificacion, String P_TipoArticulo,
            String p_codperfil, String p_codgrupo, String id_usu, String flagFecRegistro)
        {
            DataTable dt = ReporteModel.findReportePedidoXLS(Vendedor, FechaIni, fechaFin,
                FlgEnCobertura, FlgBonificacion, P_TipoArticulo, "-1", "-1", p_codperfil, p_codgrupo, id_usu, flagFecRegistro);

            List<ReportePedidoBean> list = new List<ReportePedidoBean>();
            ReportePedidoBean bean = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoBean();
                    bean.id = row["PED_PK"].ToString();
                    bean.nom_usuario = row["nom_usuario"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["ped_fecfin"].ToString();
                    bean.cli_codigo = row["cli_codigo"].ToString();
                    bean.cli_nombre = row["cli_nombre"].ToString();
                    bean.direccion = row["direccion"].ToString();
                    bean.totalBonificacion = row["totalBonificacion"].ToString();
                    bean.condvta_nombre = row["condvta_nombre"].ToString();
                    bean.ped_montototal = ManagerConfiguration.moneda + " " + row["ped_montototal"].ToString();
                    bean.MontoTotalSoles = row["ped_montototal_soles"].ToString();
                    bean.MontoTotalDolares = row["ped_montototal_dolares"].ToString();
                    bean.observacion = row["observacion"].ToString();
                    bean.cli_lat = row["cli_lat"].ToString();
                    bean.cli_lon = row["cli_lon"].ToString();
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.distancia = Distancia(bean.latitud, bean.longitud, bean.cli_lat, bean.cli_lon, ManagerConfiguration.decimal_vista).ToString();
                    bean.TOTAL_FLETE = row["TOTAL_FLETE"].ToString();
                    bean.habilitado = "HABILITADO";
                    bean.TIPO_NOMBRE = row["TIPO_NOMBRE"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.FlgDireccionDespacho = row["FlgDireccionDespacho"].ToString();
                    bean.PED_DIRECCION_DESPACHO = row["PED_DIRECCION_DESPACHO"].ToString();

                    list.Add(bean);
                }
            }

            return list;
        }

        public static List<ReporteQuiebreStockBean> PedidoStockQuiebreXLS(String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String codigoproducto, String bonificacion, String qs_codperfil, String qs_codgrupo, String id_usu)
        {
            DataTable dt = ReporteModel.findReporteStockQuiebreXLS(Vendedor, FechaIni, fechaFin, FlgEnCobertura, codigoproducto, bonificacion, "-1", "-1", qs_codperfil, qs_codgrupo, id_usu);
            List<ReporteQuiebreStockBean> list = new List<ReporteQuiebreStockBean>();
            ReporteQuiebreStockBean bean = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteQuiebreStockBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["PED_FECFIN"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.pro_codigo = row["PRO_CODIGO"].ToString();
                    bean.pro_nombre = row["PRO_NOMBRE"].ToString();
                    bean.det_cantidad = row["DET_CANTIDAD"].ToString();
                    bean.pro_stock = row["PRO_STOCK"].ToString();
                    bean.nom_usuario = row["NOM_USUARIO"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

            }

            return list;
        }

        public static List<ReporteCobranzaBean> CobranzaBuscarXLS(String Vendedor, String FechaIni, String fechaFin, String co_codperfil, String co_codgrupo, String id_usu)
        {
            DataTable dt = ReporteModel.findReporteCobranzaBeanXLS(Vendedor, FechaIni, fechaFin, "-1", "-1", co_codperfil, co_codgrupo, id_usu);
            List<ReporteCobranzaBean> list = new List<ReporteCobranzaBean>();
            ReporteCobranzaBean bean = null;
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCobranzaBean();
                    bean.id = row["COB_PK"].ToString();
                    bean.COB_CODIGO = row["COB_CODIGO"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CLI_CODIGO = row["CLI_CODIGO"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.COB_TIPODOCUMENTO = row["COB_TIPODOCUMENTO"].ToString();
                    bean.COB_MONTOTOTAL = row["COB_MONTOTOTAL"].ToString();
                    bean.COB_MONTOPAGADO = row["COB_MONTOPAGADO"].ToString();
                    bean.SALDO = row["SALDO"].ToString();
                    bean.COB_MONTOTOTALDOLARES = row["COB_MONTOTOTALDOLARES"].ToString();
                    bean.COB_MONTOPAGADODOLARES = row["COB_MONTOPAGADODOLARES"].ToString();
                    bean.SALDODOLARES = row["SALDODOLARES"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.PAG_VOUCHER = row["PAG_VOUCHER"].ToString();
                    bean.BAN_NOMLARGO = row["BAN_NOMLARGO"].ToString();
                    bean.PAG_TIPOPAGO = row["PAG_TIPOPAGO"].ToString();
                    bean.PAG_FECREGISTRO = row["PAG_FECREGISTRO"].ToString();
                    bean.PAG_MONTOPAGO = row["PAG_MONTOPAGO"].ToString();
                    bean.PAG_MONTOPAGODOLARES = row["PAG_MONTOPAGODOLARES"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();

                    list.Add(bean);
                }

            }

            return list;
        }
        public static List<ReporteReservaBean> ReservaBuscarXLS(String Vendedor, String FechaIni, String fechaFin, String codigoproducto, String bonificacion, String re_codperfil, String re_codgrupo, String id_usu)
        {
            DataTable dt = ReporteModel.findReporteReservaXLS(Vendedor, FechaIni, fechaFin, codigoproducto, bonificacion, "-1", "-1", re_codperfil, re_codgrupo, id_usu);
            List<ReporteReservaBean> list = new List<ReporteReservaBean>();
            ReporteReservaBean bean = null;
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteReservaBean();
                    bean.id = row["RESE_PK"].ToString();
                    bean.idped = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["FECHA"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.pro_codigo = row["PRO_CODIGO"].ToString();
                    bean.pro_nombre = row["NOM_PRO"].ToString();
                    bean.det_cantidad = row["DET_CANTIDAD"].ToString();
                    bean.pro_stock = row["PRO_STOCK"].ToString();
                    bean.nom_usuario = row["NOM_USUARIO"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

            }

            return list;
        }


        #endregion

        public static PaginateReporteDevolucionBean paginarDevolucionBuscar(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteDevolucionBean resultado = new PaginateReporteDevolucionBean();
            List<ReporteDevolucionBean> list = new List<ReporteDevolucionBean>();

            ReporteDevolucionBean bean;

            DataTable dt = ReporteModel.findReporteDevolucionBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteDevolucionBean();

                    bean.id = row["ID_CAB_DEVOLUCION"].ToString();
                    bean.CDEV_CODDEVOLUCION = row["CDEV_CODDEVOLUCION"].ToString();
                    bean.CDEV_CODUSUARIO = row["CDEV_CODUSUARIO"].ToString();
                    bean.CDEV_CODCLIENTE = row["CDEV_CODCLIENTE"].ToString();
                    bean.ID_DET_DEVOLUCION = row["ID_DET_DEVOLUCION"].ToString();
                    bean.DDEV_CODPRODUCTO = row["DDEV_CODPRODUCTO"].ToString();
                    bean.DDEV_CANTIDAD = row["DDEV_CANTIDAD"].ToString();
                    bean.DDEV_CODMOTIVO = row["DDEV_CODMOTIVO"].ToString();
                    bean.DDEV_CANTIDAD_FRAC = row["DDEV_CANTIDAD_FRAC"].ToString();
                    bean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                    bean.FECVCMTO = row["FECVCMTO"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.CDEV_OBSERVACION = row["DDEV_OBSERVACION"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.CANTIDA_PRESENTACION = row["CANTIDA_PRESENTACION"].ToString();
                    bean.PRECIO_PRESENTACION = row["PRECIO_PRESENTACION"].ToString();
                    bean.ALMACEN = row["ALMACEN"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }

        public static PaginateReporteNoPedidoBean paginarNoPedidoBuscar(String Vendedor, String FechaIni, String fechaFin,
            String FlgEnCobertura, String page, String rows,
            String codperfil, String codgrupo, String id_usu, String flgFecRegistro)
        {
            PaginateReporteNoPedidoBean resultado = new PaginateReporteNoPedidoBean();
            List<ReporteNoPedidoBean> list = new List<ReporteNoPedidoBean>();

            ReporteNoPedidoBean bean;

            DataTable dt = ReporteModel.findReporteNoPedidoBean(Vendedor, FechaIni, fechaFin,
                FlgEnCobertura, page, rows, codperfil, codgrupo, id_usu, flgFecRegistro);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteNoPedidoBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["ped_fecfin"].ToString();
                    bean.ped_fecregistro = row["ped_fecregistro"].ToString();
                    bean.ped_condvta = row["ped_condventa"].ToString();
                    bean.cli_codigo = row["cli_codigo"].ToString();
                    bean.ped_nopedido = row["ped_nopedido"].ToString();
                    bean.ped_montototal = ManagerConfiguration.moneda + " " + row["ped_montototal"].ToString();
                    bean.usr_pk = row["usr_pk"].ToString();
                    bean.usr_codigo = row["usr_codigo"].ToString();
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    bean.usr_nombre = row["usr_nombre"].ToString();
                    bean.cli_nombre = row["cli_nombre"].ToString();
                    bean.mot_nombre = row["mot_nombre"].ToString();
                    bean.PED_DIRECCION = row["PED_DIRECCION"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }

            return resultado;
        }
        public static List<PedidoBean> getSKUMasDemandados(String Operacion, String FechInicio, String FechFin, String asistencia)
        {
            List<PedidoBean> list = new List<PedidoBean>();
            PedidoBean bean = null;

            DataTable dt = ReporteModel.getSKUMasDemandados(Operacion, FechInicio, FechFin, asistencia);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoBean();

                    bean.CLI_NOMBRE = row["CLIENTE"].ToString();
                    bean.NOM_USUARIO = row["VENDEDOR"].ToString(); //vendedor
                    bean.PRODUCTO = row["PRODUCTO"].ToString(); //PRODUCTO
                    bean.PED_MONTOTOTAL = row["MONTO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();

                    list.Add(bean);
                }


            }


            return list;
        }
        public static List<PedidoBean> getSkuMasVendidos(String Operacion, String FechInicio, String FechFin, String asistencia)
        {
            List<PedidoBean> list = new List<PedidoBean>();
            PedidoBean bean = null;

            DataTable dt = ReporteModel.getSkuMasVendidos(Operacion, FechInicio, FechFin, asistencia);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoBean();

                    bean.CLI_NOMBRE = row["CLIENTE"].ToString();
                    bean.NOM_USUARIO = row["VENDEDOR"].ToString(); //vendedor
                    bean.PRODUCTO = row["PRODUCTO"].ToString(); //PRODUCTO
                    bean.PED_MONTOTOTAL = row["MONTO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();

                    list.Add(bean);
                }


            }


            return list;
        }



        public static List<PedidoBean> getCliMasVentas(String Operacion, String FechInicio, String FechFin, String asistencia)
        {
            List<PedidoBean> list = new List<PedidoBean>();
            PedidoBean bean = null;

            DataTable dt = ReporteModel.getCliMasVentas(Operacion, FechInicio, FechFin, asistencia);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoBean();

                    bean.CLI_NOMBRE = row["CLIENTE"].ToString();
                    bean.NOM_USUARIO = row["VENDEDOR"].ToString(); //vendedor
                    bean.PED_MONTOTOTAL = row["MONTO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();

                    list.Add(bean);
                }


            }


            return list;
        }


        public static List<PedidoBean> getCliMasProVendidos(String Operacion, String FechInicio, String FechFin, String asistencia)
        {
            List<PedidoBean> list = new List<PedidoBean>();
            PedidoBean bean = null;

            DataTable dt = ReporteModel.getCliMasProVendidos(Operacion, FechInicio, FechFin, asistencia);


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoBean();

                    bean.CLI_NOMBRE = row["CLIENTE"].ToString();
                    bean.NOM_USUARIO = row["VENDEDOR"].ToString(); //vendedor
                    bean.CANTIDAD = row["CANTIDAD"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();

                    list.Add(bean);
                }


            }


            return list;
        }

        private static bool validaCoordenada(String longitud, String latitud)
        {

            if (!longitud.Equals("") && !latitud.Equals("") && double.Parse(longitud) != 0 && double.Parse(latitud) != 0)
            {
                return true;
            }

            return false;
        }

        public static List<MapBean> GeoAnalisisBuscar(String Operacion, String Tipo, String FechaIni, String FechaFin, String Asistencia)
        {
            List<MapBean> list = new List<MapBean>();

            MapBean bean;
            DataTable dt;
            String operacion = "";
            if (Operacion == "C")
            {
                if (Tipo == "S")
                {
                    dt = ReporteModel.getCliMasVentas("M", FechaIni, FechaFin, Asistencia);
                    operacion = "M";
                }
                else
                {
                    dt = ReporteModel.getCliMasProVendidos("C", FechaIni, FechaFin, Asistencia);
                    operacion = "C";
                }
            }
            else
            {
                if (Tipo == "S")
                {
                    dt = ReporteModel.getSkuMasVendidos("MP", FechaIni, FechaFin, Asistencia);
                    operacion = "MP";
                }
                else
                {
                    dt = ReporteModel.getSKUMasDemandados("CP", FechaIni, FechaFin, Asistencia);
                    operacion = "CP";
                }
            }
            Int32 i = 0;
            if (operacion.Equals("M") || operacion.Equals("C"))
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (validaCoordenada(row["LATITUD"].ToString(), row["LONGITUD"].ToString()))
                        {
                            bean = new MapBean();
                            bean.img = "../../images/gps/" + "m" + (i >= 4 ? 4 : i + 1) + ".png";
                            bean.latitud = row["LATITUD"].ToString();
                            bean.longitud = row["LONGITUD"].ToString();
                            if (operacion.Equals("C"))
                            {
                                bean.msg = "<h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_GRMAPA_CLIENTE_VS_CANTIDAD) + " ( " + (i + 1).ToString() + " )" + "</h4>" + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_CANTIDAD) + ": " + row["CANTIDAD"].ToString() + "</br>";
                            }
                            else //M
                            {
                                bean.msg = "<h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_GRMAPA_CLIENTE_VS_MONTO) + " ( " + (i + 1).ToString() + " )" + "</h4>" + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MONTO) + ": " + ManagerConfiguration.moneda + " " + row["MONTO"].ToString() + "</br>";
                            }
                            bean.msg += IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_CLIENTE) + ": " + row["CLIENTE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_VENDEDOR) + ": " + row["VENDEDOR"].ToString(); // row["DIRECCION"].ToString();
                            bean.titulo = "INDEX : " + (i + 1).ToString();
                            list.Add(bean);
                        }
                        i++;
                    }
                }
            }
            String img = "";
            Boolean val;
            if (operacion.Equals("MP") || operacion.Equals("CP"))
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (validaCoordenada(row["LATITUD"].ToString(), row["LONGITUD"].ToString()))
                        {
                            bean = new MapBean();
                            val = Puntosrepetidos(dt, i);

                            if (val) { img = "m" + 1 + ".png"; } else { img = "m" + (i >= 4 ? 4 : i + 1) + ".png"; }

                            bean.img = "../../images/gps/" + img;
                            bean.latitud = row["LATITUD"].ToString();
                            bean.longitud = row["LONGITUD"].ToString();
                            if (operacion.Equals("CP"))
                            {
                                bean.msg = "<h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_GRMAPA_PRODUCTO_VS_CANTIDAD) + " ( " + (i + 1).ToString() + " )" + "</h4>" + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_PRODUCTO) + ": " + row["PRODUCTO"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_CANTIDAD) + ": " + row["CANTIDAD"].ToString() + "</br>";
                            }
                            else //M
                            {
                                bean.msg = "<h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_GRMAPA_PRODUCTO_VS_MONTO) + " ( " + (i + 1).ToString() + " )" + "</h4>" + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_PRODUCTO) + ": " + row["PRODUCTO"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MONTO) + ": " + ManagerConfiguration.moneda + " " + row["MONTO"].ToString() + "</br>";
                            }
                            bean.msg += IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_CLIENTE) + ": " + row["CLIENTE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_VENDEDOR) + ": " + row["VENDEDOR"].ToString(); // row["DIRECCION"].ToString();
                            bean.titulo = "INDEX : " + (i + 1).ToString();
                            list.Add(bean);
                        }
                        i++;
                    }
                }
            }

            return list;

        }

        private static bool Puntosrepetidos(DataTable lista, int indice)
        {
            int c = 0;
            bool img = false;

            for (int i = 0; i < lista.Rows.Count - 1; i++)
            {

                if ((lista.Rows[i][4].ToString() == lista.Rows[indice][4].ToString()) && (lista.Rows[i][5].ToString() == lista.Rows[indice][5].ToString()) && (i != indice))
                {
                    c = c + 1;
                }
            }

            if (c != 0) { img = true; }

            return img;

        }


        public static List<MapBean> RutaBuscar(String Vendedor, String Fecha, String Asistencia, String codperfil, String codgrupo, String id_usu)
        {

            List<MapBean> list = new List<MapBean>();

            MapBean bean;

            DataTable dt = ReporteModel.findReporteRutaBean(Vendedor, Fecha, Asistencia, codperfil, codgrupo, id_usu);

            Int32 i = 1;
            Int32 j = 1;
            if (dt != null && dt.Rows.Count > 0)
            {
                String primerVendedor = dt.Rows[0]["VENDEDOR"].ToString();
                String anteriorCliente = String.Empty;
                String anteriorVendedor = String.Empty;
                int indice_Color = -1;

                foreach (DataRow row in dt.Rows)
                {

                    bool vendBool = false;

                    if (validaCoordenada(row["GPSLATITUD"].ToString(), row["GPSLONGITUD"].ToString()))
                    {

                        bean = new MapBean();

                        if (!row["VENDEDOR"].ToString().Equals(primerVendedor))
                        {
                            if (!anteriorVendedor.Equals(row["VENDEDOR"].ToString()))
                            {
                                anteriorVendedor = row["VENDEDOR"].ToString();
                                indice_Color++;

                            }
                            vendBool = true;

                        }


                        switch (Convert.ToInt32(row["TIPO"].ToString()))
                        {
                            case 1://pedido
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=pedido.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/pedido.png";
                                }

                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_PEDIDO).ToUpper() + " " + j + " </h4>";
                                j++;
                                break;
                            case 2://No pedido
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=no-pedido.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/no-pedido.png";
                                }
                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_NOPEDIDO).ToUpper() + "</h4>";

                                break;
                            case 3://Canje
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=canje.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/canje.png";
                                }
                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_CANJE).ToUpper() + "</h4>";

                                break;
                            case 4://Devolucion
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=devolucion.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/devolucion.png";
                                }
                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_DEVOLUCION).ToUpper() + "</h4>";

                                break;
                            case 5://Cobranza
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=cobranza.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/cobranza.png";
                                }
                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_COBRANZA).ToUpper() + "</h4>";

                                break;
                            default:
                                break;
                        }
                        bean.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_VENDEDOR).ToUpper() + ":" + row["VENDEDOR"].ToString() + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE).ToUpper() + ":" + row["CLIENTE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MONTO) + ":" + ManagerConfiguration.moneda + " " + row["DETALLE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_FECHA) + ":" + row["FECHA"].ToString();

                        bean.latitud = row["GPSLATITUD"].ToString();
                        bean.longitud = row["GPSLONGITUD"].ToString();
                        bean.titulo = "PUNTOS" + ":" + i.ToString();

                        list.Add(bean);
                        i++;

                        if (validaCoordenada(row["CLI_LATITUD"].ToString(), row["CLI_LONGITUD"].ToString()))
                        {

                            if (row["CLIENTE"].ToString() != anteriorCliente)
                            {
                                anteriorCliente = row["CLIENTE"].ToString();
                                bean = new MapBean();
                                bean.latitud = row["CLI_LATITUD"].ToString();
                                bean.longitud = row["CLI_LONGITUD"].ToString();

                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=cliente.png";
                                    bean.tipo = indice_Color.ToString();
                                }
                                else
                                {
                                    bean.img = "../../images/gps/cliente.png";
                                    bean.tipo = "-1";
                                }
                                bean.titulo = row["CLIENTE"].ToString();
                                bean.vendedor = row["VENDEDOR"].ToString();
                                bean.msg = "<h4>" + row["CLIENTE"].ToString() + "</h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_DIRECCION) + ":" + row["DIRECCION"].ToString();

                                list.Add(bean);
                            }
                        }

                    }


                }

            }

            return list;
        }

        public static EficienciaBean infovendedor(String cod, String fecha)
        {
            EficienciaBean bean;
            DataTable dt = ReporteModel.findVerReporteEficienciaBean(cod, fecha);

            bean = new EficienciaBean();
            bean.pedidosrealizados = dt.Rows[0]["PEDIDOSREALIZADOS"].ToString();
            bean.nopedidos = dt.Rows[0]["NOPEDIDOS"].ToString();
            bean.montototalventa = dt.Rows[0]["MONTOTOALVENTA"].ToString();
            bean.cantidadtotalitems = dt.Rows[0]["CANTIDADTOTALITEMS"].ToString();
            bean.montocobranza = dt.Rows[0]["MONTOTOTALCOBRANZA"].ToString();

            return bean;
        }

        public static List<MapBean> RutaBuscarWSsdr(String Vendedor, String Fecha, String Asistencia, String codperfil, String codgrupo, String id_usu)
        {

            List<MapBean> list = new List<MapBean>();

            MapBean bean;

            DataTable dt = ReporteModel.findReporteRutaBean(Vendedor, Fecha, Asistencia, codperfil, codgrupo, id_usu);

            Int32 i = 1;
            Int32 j = 1;
            if (dt != null && dt.Rows.Count > 0)
            {
                String primerVendedor = dt.Rows[0]["VENDEDOR"].ToString();
                String anteriorCliente = String.Empty;
                String anteriorVendedor = String.Empty;
                int indice_Color = -1;

                foreach (DataRow row in dt.Rows)
                {

                    bool vendBool = false;

                    if (validaCoordenada(row["GPSLATITUD"].ToString(), row["GPSLONGITUD"].ToString()))
                    {

                        bean = new MapBean();

                        if (!row["VENDEDOR"].ToString().Equals(primerVendedor))
                        {
                            if (!anteriorVendedor.Equals(row["VENDEDOR"].ToString()))
                            {
                                anteriorVendedor = row["VENDEDOR"].ToString();
                                indice_Color++;

                            }
                            vendBool = true;

                        }


                        switch (Convert.ToInt32(row["TIPO"].ToString()))
                        {
                            case 1://pedido
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=pedido.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/pedidowssdr.png";
                                }

                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_PEDIDO).ToUpper() + " " + j + " </h4>";
                                j++;
                                break;

                            case 5://Cobranza
                                if (vendBool)
                                {
                                    bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=cobranza.png";
                                }
                                else
                                {
                                    bean.img = "../../images/gps/cobranzawssdr.png";
                                }
                                bean.msg = "<h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NAV_COBRANZA).ToUpper() + "</h4>";

                                break;
                            default:
                                break;
                        }
                        bean.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_VENDEDOR).ToUpper() + ":" + row["VENDEDOR"].ToString() + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE).ToUpper() + ":" + row["CLIENTE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_MONTO) + ":" + ManagerConfiguration.moneda + " " + row["DETALLE"].ToString() + "</br>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_FECHA) + ":" + row["FECHA"].ToString();

                        bean.latitud = row["GPSLATITUD"].ToString();
                        bean.longitud = row["GPSLONGITUD"].ToString();
                        bean.titulo = "PUNTOS" + ":" + i.ToString();
                        list.Add(bean);
                        i++;

                        /*      if (validaCoordenada(row["CLI_LATITUD"].ToString(), row["CLI_LONGITUD"].ToString()))
                              {

                                  if (row["CLIENTE"].ToString() != anteriorCliente)
                                  {
                                      anteriorCliente = row["CLIENTE"].ToString();
                                      bean = new MapBean();
                                      bean.latitud = row["CLI_LATITUD"].ToString();
                                      bean.longitud = row["CLI_LONGITUD"].ToString();

                                      if (vendBool)
                                      {
                                          bean.img = "icono.aspx?indice=" + indice_Color + "&proceso=cliente.png";
                                          bean.tipo = indice_Color.ToString();
                                      }
                                      else
                                      {
                                          bean.img = "../../images/gps/cliente.png";
                                          bean.tipo = "-1";
                                      }
                                      bean.titulo = row["CLIENTE"].ToString();
                                      bean.vendedor = row["VENDEDOR"].ToString();
                                      bean.msg = "<h4>" + row["CLIENTE"].ToString() + "</h4>" + IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_DIRECCION) + ":" + row["DIRECCION"].ToString();
                                      list.Add(bean);
                                  }
                              }*/

                    }


                }

            }

            return list;
        }

        public static PaginateReporteFamiliaBean paginaFamiliaBuscar(String Vendedor, String FechaIni, String fechaFin,
          String page, String rows, String tipoArticulo, String codperfil, String codgrupo, String id_usu, String familia
            )
        {
            PaginateReporteFamiliaBean resultado = new PaginateReporteFamiliaBean();
            List<ReporteFamiliaBean> list = new List<ReporteFamiliaBean>();
            ReporteFamiliaBean bean;
            DataSet dtS = new DataSet();
            if (tipoArticulo == "PRO")
            {
                dtS = ReporteModel.findReporteFamiliaBeanPRO(Vendedor, FechaIni, fechaFin,
                              page, rows, tipoArticulo, codperfil, codgrupo, id_usu, familia);
            }
            else if (tipoArticulo == "PRE")
            {
                dtS = ReporteModel.findReporteFamiliaBeanPRE(Vendedor, FechaIni, fechaFin,
                             page, rows, tipoArticulo, codperfil, codgrupo, id_usu, familia);
            }
            DataTable dt = dtS.Tables[0];
            DataTable totalizado = dtS.Tables[1];

            int totalRegistros = 0;
            string totalmonto = "0";
            string totalmontosoles = "0";
            string totalmontodolares = "0";
            string totalcantidad = "0";
            string totalfamilias = "0";
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(totalizado.Rows[0]["TAMANIOTOTAL"].ToString());
                totalmonto = (totalizado.Rows[0]["totalmonto"].ToString());
                totalmontosoles = (totalizado.Rows[0]["totalmontosoles"].ToString());
                totalmontodolares = (totalizado.Rows[0]["totalmontodolares"].ToString());
                totalcantidad = (totalizado.Rows[0]["totalcantidad"].ToString());
                totalfamilias = (totalizado.Rows[0]["totalfamilias"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteFamiliaBean();

                    bean.nombre = row["NOMBRE"].ToString();
					bean.monto = row["MONTO"].ToString();
                    bean.monto_soles = row["MONTOSOLES"].ToString();
                    bean.monto_dolares = row["MONTODOLARES"].ToString();
                    bean.cantidad = row["CANTIDAD"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalmonto = totalmonto;
                resultado.totalmontosoles = totalmontosoles;
                resultado.totalmontodolares = totalmontodolares;
                resultado.totalcantidad = totalcantidad;
                resultado.totalfamilias = totalfamilias;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }

            return resultado;
        }

        public static PaginateReportePedidoBean paginarPedidoBuscar(String Vendedor, String FechaIni, String fechaFin,
            String FlgEnCobertura, String FlgBonificacion, String page, String rows
            , String FlgTipoPedido, String tipoArticulo, String codperfil, String codgrupo, String id_usu, String flagFecRegistro
            )
        {
            PaginateReportePedidoBean resultado = new PaginateReportePedidoBean();
            List<ReportePedidoBean> list = new List<ReportePedidoBean>();
            ReportePedidoBean bean;

            DataTable dt = ReporteModel.findReportePedidoBean(Vendedor, FechaIni, fechaFin,
                FlgEnCobertura, FlgBonificacion, page, rows
                , FlgTipoPedido //@004 I/F
                , tipoArticulo, codperfil, codgrupo, id_usu, flagFecRegistro);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["ped_fecfin"].ToString();
                    bean.ped_fecregistro = row["ped_fecregistro"].ToString();
                    bean.ped_condvta = row["ped_condventa"].ToString();
                    bean.cli_codigo = row["cli_codigo"].ToString();
                    bean.ped_nopedido = row["ped_nopedido"].ToString();
                    bean.ped_montototal = ManagerConfiguration.moneda + " " + row["ped_montototal"].ToString();
                    bean.MontoTotalSoles = row["ped_montototal_soles"].ToString();
                    bean.MontoTotalDolares = row["ped_montototal_dolares"].ToString();
                    bean.usr_pk = row["usr_pk"].ToString();
                    bean.usr_codigo = row["usr_codigo"].ToString();
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    bean.nom_usuario = row["nom_usuario"].ToString();
                    bean.condvta_nombre = row["condvta_nombre"].ToString();
                    bean.cli_nombre = row["cli_nombre"].ToString();
                    bean.direccion = row["direccion"].ToString();
                    bean.observacion = row["observacion"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.cli_lat = row["cli_lat"].ToString();
                    bean.cli_lon = row["cli_lon"].ToString();
                    bean.totalBonificacion = row["totalBonificacion"].ToString();
                    bean.distancia = Distancia(bean.latitud, bean.longitud, bean.cli_lat, bean.cli_lon, ManagerConfiguration.decimal_vista).ToString();
                    bean.habilitado = "HABILITADO";
                    //@003 I
                    bean.TIPO_NOMBRE = row["TIPO_NOMBRE"].ToString();
                    bean.PED_DIRECCION_DESPACHO = row["PED_DIRECCION_DESPACHO"].ToString();
                    //@003 F
                    bean.FlgDireccionDespacho = row["FlgDireccionDespacho"].ToString(); //@006 I/F
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString(); //@008 I/F
                    bean.FLG_MOSTRAR_ALMACEN = row["FlgMostrarAlmacen"].ToString(); //@008 I/F
                    bean.TOTAL_FLETE = row["TOTAL_FLETE"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }

            return resultado;
        }

        public static List<ReportePedidoBean> ListaCordenadaPedido(String Vendedor, String FechaIni,
            String fechaFin, String FlgEnCobertura, String FlgBonificacion, String FlgTipoPedido,
            String tipoArticulo, String codperfil, String codgrupo, String id_usu)
        {
            List<ReportePedidoBean> list = new List<ReportePedidoBean>();
            ReportePedidoBean bean;
            DataTable dt = ReporteModel.ListaCordenadaPedido(Vendedor, FechaIni, fechaFin, FlgEnCobertura,
                FlgBonificacion, FlgTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["PED_FECFIN"].ToString();
                    bean.ped_fecregistro = row["PED_FECREGISTRO"].ToString();
                    bean.ped_condvta = row["PED_CONDVENTA"].ToString();
                    bean.cli_codigo = row["CLI_CODIGO"].ToString();
                    bean.ped_nopedido = row["PED_NOPEDIDO"].ToString();
                    bean.ped_montototal = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL"].ToString();
                    bean.usr_pk = row["USR_PK"].ToString();
                    bean.usr_codigo = row["USR_CODIGO"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    bean.totalBonificacion = row["BONIFICACION_TOTAL"].ToString();
                    bean.observacion = row["PED_OBSERVACION"].ToString();
                    bean.cli_lat = row["CLI_LATITUD"].ToString();
                    bean.cli_lon = row["CLI_LONGITUD"].ToString();
                    bean.nom_usuario = row["USR_NOMBRE"].ToString();
                    bean.PED_DIRECCION_DESPACHO = row["PED_DIRECCION_DESPACHO"].ToString();
                    bean.condvta_nombre = row["CONDVTA_NOMBRE"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.direccion = row["TOTAL_FLETE"].ToString();
                    bean.tieneBonificacion = row["NOMBREGRUPO"].ToString();
                    bean.distancia = Distancia(bean.latitud, bean.longitud, bean.cli_lat, bean.cli_lon, ManagerConfiguration.decimal_vista).ToString();
                    bean.COLOR = row["fila"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }
        public static List<ReportePedidoBean> ListaVendedorPedido(String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String FlgBonificacion, String FlgTipoPedido, String tipoArticulo, String codperfil, String codgrupo, String id_usu)
        {
            List<ReportePedidoBean> list = new List<ReportePedidoBean>();

            ReportePedidoBean bean;

            DataTable dt = ReporteModel.ListaNombreVendedorPedido(Vendedor, FechaIni, fechaFin, FlgEnCobertura, FlgBonificacion, FlgTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoBean();
                    bean.usr_pk = row["USR_PK"].ToString();
                    bean.nom_usuario = row["USR_NOMBRE"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }
        public static List<ReportePedidoBean> ListaCoordenadaVendedorPedido(String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String FlgBonificacion, String FlgTipoPedido, String tipoArticulo, String codperfil, String codgrupo, String id_usu)
        {
            List<ReportePedidoBean> list = new List<ReportePedidoBean>();

            ReportePedidoBean bean;

            DataTable dt = ReporteModel.ListaCoordenadaVendedorPedido(Vendedor, FechaIni, fechaFin, FlgEnCobertura, FlgBonificacion, FlgTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);


            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoBean();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    list.Add(bean);
                }
            }

            return list;
        }
        public static String SetColor(String usuarioNew, String usuarioOld)
        {
            Int32 val = 0;
            if (usuarioNew == usuarioOld)
            {
                val = 0;
            }
            else
            {
                val = val + 1;
            }
            return "" + val;
        }
        public static PaginateReporteQuiebreStockBean paginarPedidoBuscarQuiebre(String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String codigoproducto, String bonificacion, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteQuiebreStockBean resultado = new PaginateReporteQuiebreStockBean();
            List<ReporteQuiebreStockBean> list = new List<ReporteQuiebreStockBean>();

            ReporteQuiebreStockBean bean;

            DataTable dt = ReporteModel.findReporteQuiebreStockBean(Vendedor, FechaIni, fechaFin, FlgEnCobertura, codigoproducto, bonificacion, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteQuiebreStockBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["PED_FECINICIO"].ToString();
                    bean.ped_fecfin = row["PED_FECFIN"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.pro_codigo = row["PRO_CODIGO"].ToString();
                    bean.pro_nombre = row["PRO_NOMBRE"].ToString();
                    bean.det_cantidad = row["DET_CANTIDAD"].ToString();
                    bean.pro_stock = row["PRO_STOCK"].ToString();
                    bean.nom_usuario = row["NOM_USUARIO"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }

            return resultado;
        }


        public static PaginateReporteReservaBean paginarPedidoBuscarReserva(String Vendedor, String FechaIni, String fechaFin, String codigoproducto, String bonificacion, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteReservaBean resultado = new PaginateReporteReservaBean();
            List<ReporteReservaBean> list = new List<ReporteReservaBean>();

            ReporteReservaBean bean;

            DataTable dt = ReporteModel.findReporteReservaBean(Vendedor, FechaIni, fechaFin, codigoproducto, bonificacion, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteReservaBean();
                    bean.id = row["RESE_PK"].ToString();
                    bean.idped = row["PED_PK"].ToString();
                    bean.ped_fecinicio = row["FECHA"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.pro_codigo = row["PRO_CODIGO"].ToString();
                    bean.pro_nombre = row["NOM_PRO"].ToString();
                    bean.det_cantidad = row["DET_CANTIDAD"].ToString();
                    bean.pro_stock = row["PRO_STOCK"].ToString();
                    bean.nom_usuario = row["NOM_USUARIO"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }


        public static PaginateReportePedidoTotalBean paginarPedidoTotalBuscar(String fTipoPedido, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String Bonificacion, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReportePedidoTotalBean resultado = new PaginateReportePedidoTotalBean();
            List<ReportePedidoTotalBean> list = new List<ReportePedidoTotalBean>();

            ReportePedidoTotalBean bean;

            DataTable dt = ReporteModel.findReportePedidoTotalBean(fTipoPedido, Vendedor, FechaIni, fechaFin, FlgEnCobertura, Bonificacion, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoTotalBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.NOM_USUARIO = row["NOM_USUARIO"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.CONDVTA_NOMBRE = row["CONDVTA_NOMBRE"].ToString();
                    bean.PED_FECINICIO = row["PED_FECINICIO"].ToString();
                    bean.PED_FECFIN = row["PED_FECFIN"].ToString();
                    bean.PED_MONTOTOTAL = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL"].ToString();
                    bean.PED_MONTOTOTAL_SOLES = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL_SOLES"].ToString();
                    bean.PED_MONTOTOTAL_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["PED_MONTOTOTAL_DOLARES"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DET_CANTIDAD = row["DET_CANTIDAD"].ToString();
                    bean.DET_PRECIOBASE = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE"].ToString();
                    bean.DET_PRECIOBASE_SOLES = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE_SOLES"].ToString();
                    bean.DET_PRECIOBASE_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_PRECIOBASE_DOLARES"].ToString();
                    bean.DESCUENTO = row["DESCUENTO"].ToString();
                    bean.DET_MONTO = ManagerConfiguration.moneda + " " + row["DET_MONTO"].ToString();
                    bean.DET_MONTO_SOLES = ManagerConfiguration.moneda + " " + row["DET_MONTO_SOLES"].ToString();
                    bean.DET_MONTO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_MONTO_DOLARES"].ToString();
                    bean.DESCRIPCION = row["DESCRIPCION"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.CLI_LAT = row["CLI_LAT"].ToString();
                    bean.CLI_LON = row["CLI_LON"].ToString();
                    bean.TIPO_NOMBRE = row["TIPO_NOMBRE"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.DISTANCIA = Distancia(bean.LATITUD, bean.LONGITUD, bean.CLI_LAT, bean.CLI_LON, ManagerConfiguration.decimal_vista).ToString();
                    bean.CLI_CODIGO = row["CLI_CODIGO"].ToString();
                    bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                    bean.BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.PED_OBSERVACION = row["PED_OBSERVACION"].ToString();
                    bean.PED_PK = row["PED_PK"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }
        public static PaginateReportePedidoTotalBean paginarPedidopresetacionTotalBuscar(String fTipoPedido,
            String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String Bonificacion,
            String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReportePedidoTotalBean resultado = new PaginateReportePedidoTotalBean();
            List<ReportePedidoTotalBean> list = new List<ReportePedidoTotalBean>();
            ReportePedidoTotalBean bean;

            DataTable dt = ReporteModel.findReportePedidoPresentacionTotalBean(fTipoPedido, Vendedor, FechaIni,
                fechaFin, FlgEnCobertura, Bonificacion, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReportePedidoTotalBean();

                    bean.id = row["PED_PK"].ToString();
                    bean.NOM_USUARIO = row["NOM_USUARIO"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.CONDVTA_NOMBRE = row["CONDVTA_NOMBRE"].ToString();
                    bean.PED_FECINICIO = row["PED_FECINICIO"].ToString();
                    bean.PED_FECFIN = row["PED_FECFIN"].ToString();
                    bean.PED_MONTOTOTAL = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL"].ToString();
                    bean.PED_MONTOTOTAL_SOLES = ManagerConfiguration.moneda + " " + row["PED_MONTOTOTAL_SOLES"].ToString();
                    bean.PED_MONTOTOTAL_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["PED_MONTOTOTAL_DOLARES"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.DET_CANTIDAD_PRESENTACION = row["CANTIDAD_PRESENTACION"].ToString();
                    bean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                    bean.DET_CANTIDAD = row["DET_CANTIDAD"].ToString();
                    bean.DET_CANTIDAD_FRACCION = row["DET_CANTIDAD_FRACCION"].ToString();
                    bean.DET_PRECIOBASE = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE"].ToString();
                    bean.DET_PRECIOBASE_SOLES = ManagerConfiguration.moneda + " " + row["DET_PRECIOBASE_SOLES"].ToString();
                    bean.DET_PRECIOBASE_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_PRECIOBASE_DOLARES"].ToString();
                    bean.DESCUENTO = row["DESCUENTO"].ToString();
                    bean.DET_MONTO = ManagerConfiguration.moneda + " " + row["DET_MONTO"].ToString();
                    bean.DET_MONTO_SOLES = ManagerConfiguration.moneda + " " + row["DET_MONTO_SOLES"].ToString();
                    bean.DET_MONTO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_MONTO_DOLARES"].ToString();
                    bean.DESCRIPCION = row["DESCRIPCION"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.tieneBonificacion = row["TIENE_BONIFICACION"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.CLI_LAT = row["CLI_LAT"].ToString();
                    bean.CLI_LON = row["CLI_LON"].ToString();
                    bean.TIPO_NOMBRE = row["TIPO_NOMBRE"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    bean.DISTANCIA = Distancia(bean.LATITUD, bean.LONGITUD, bean.CLI_LAT, bean.CLI_LON, ManagerConfiguration.decimal_vista).ToString();
                    bean.CLI_CODIGO = row["CLI_CODIGO"].ToString();
                    bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                    bean.BONIFICACION = row["DET_BONIFICACION"].ToString();
                    bean.PED_OBSERVACION = row["PED_OBSERVACION"].ToString();
                    bean.FLETE_TOTAL = row["FLETE_TOTAL"].ToString();
                    bean.MONTO_TOTAL_FLETE = row["MONTO_TOTAL_FLETE"].ToString();
                    bean.PED_PK = row["PED_PK"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }

        public static PaginateReporteEquipoBean paginarEquipoBuscar(String page, String rows)
        {
            PaginateReporteEquipoBean resultado = new PaginateReporteEquipoBean();
            List<ReporteEquipoBean> list = new List<ReporteEquipoBean>();

            ReporteEquipoBean bean;

            DataTable dt = ReporteModel.findReporteEquipoBean(page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteEquipoBean();

                    bean.ULTIMAFECHA = row["ULTIMAFECHA"].ToString();
                    bean.ULTIMALATITUD = row["ULTIMALATITUD"].ToString();
                    bean.ULTIMALONGITUD = row["ULTIMALONGITUD"].ToString();
                    bean.MODELO = row["MODELO"].ToString();
                    bean.SENAL = row["SENAL"].ToString();
                    bean.BATERIA = row["BATERIA"].ToString();
                    bean.IP = row["IP"].ToString();
                    bean.NEXTEL = row["NEXTEL"].ToString();
                    bean.TELEFONO = row["TELEFONO"].ToString();
                    bean.ERRORPOSICION = row["ERRORPOSICION"].ToString();
                    bean.ERRORCONEXION = row["ERRORCONEXION"].ToString();
                    bean.ASISTENCIA = row["ASISTENCIA"].ToString();
                    bean.USUARIO = row["USUARIO"].ToString();
                    bean.COD_USR = row["COD_USR"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }

        public static PaginateReporteCanjeBean paginarCanjeBuscar(String TipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteCanjeBean resultado = new PaginateReporteCanjeBean();
            List<ReporteCanjeBean> list = new List<ReporteCanjeBean>();

            ReporteCanjeBean bean;

            DataTable dt = ReporteModel.findReporteCanjeBean(TipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCanjeBean();

                    bean.id = row["ID_CAB_CANJE"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CABC_CODCLIENTE = row["CABC_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DETC_CANTIDAD = row["DETC_CANTIDAD"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.DETC_OBSERVACION = row["DETC_OBSERVACION"].ToString();
                    bean.DETC_FECVCMTO = row["DETC_FECVCMTO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }
        public static PaginateReporteCanjeBean paginarCanjeBuscarPresentacion(String tipoArticulo, String Vendedor, String FechaIni, String fechaFin, String FlgEnCobertura, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteCanjeBean resultado = new PaginateReporteCanjeBean();
            List<ReporteCanjeBean> list = new List<ReporteCanjeBean>();

            ReporteCanjeBean bean;

            DataTable dt = ReporteModel.findReporteCanjeBean(tipoArticulo, Vendedor, FechaIni, fechaFin, FlgEnCobertura, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCanjeBean();

                    bean.id = row["ID_CAB_CANJE"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CABC_CODCLIENTE = row["CABC_CODCLIENTE"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DETC_CANTIDAD = row["DETC_CANTIDAD"].ToString();
                    bean.MOT_NOMBRE = row["MOT_NOMBRE"].ToString();
                    bean.DETC_OBSERVACION = row["DETC_OBSERVACION"].ToString();
                    bean.UNIDAD_FRACCIONAMIENTO = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                    bean.DETC_FECVCMTO = row["DETC_FECVCMTO"].ToString();
                    bean.LATITUD = row["LATITUD"].ToString();
                    bean.LONGITUD = row["LONGITUD"].ToString();
                    bean.NOMBRE_PRESENTACION = row["NOMBRE_PRESENTACION"].ToString();
                    bean.PRECIO_PRESENTACION = row["PRECIO_PRESENTACION"].ToString();
                    bean.DETC_CANTIDAD_PRE = row["DETC_CANTIDAD_PRE"].ToString();
                    bean.DETC_CANTIDAD_FRAC = row["DETC_CANTIDAD_FRAC"].ToString();
                    bean.ALM_NOMBRE = row["ALM_NOMBRE"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }

        public static PaginateReporteCobranzaBean paginarCobranzaBuscar(String Vendedor, String FechaIni, String fechaFin, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateReporteCobranzaBean resultado = new PaginateReporteCobranzaBean();
            List<ReporteCobranzaBean> list = new List<ReporteCobranzaBean>();

            ReporteCobranzaBean bean;

            DataTable dt = ReporteModel.findReporteCobranzaBean(Vendedor, FechaIni, fechaFin, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteCobranzaBean();
                    bean.COB_CODIGO = row["COB_CODIGO"].ToString();
                    bean.USR_NOMBRE = row["USR_NOMBRE"].ToString();
                    bean.CLI_CODIGO = row["CLI_CODIGO"].ToString();
                    bean.CLI_NOMBRE = row["CLI_NOMBRE"].ToString();
                    bean.COB_TIPODOCUMENTO = row["COB_TIPODOCUMENTO"].ToString();
                    bean.COB_MONTOTOTAL = row["COB_MONTOTOTAL"].ToString();
                    bean.COB_MONTOPAGADO = row["COB_MONTOPAGADO"].ToString();
                    bean.SALDO = row["SALDO"].ToString();
                    bean.COB_MONTOTOTALSOLES = row["COB_MONTOTOTALSOLES"].ToString();
                    bean.COB_MONTOPAGADOSOLES = row["COB_MONTOPAGADOSOLES"].ToString();
                    bean.SALDOSOLES = row["SALDOSOLES"].ToString();
                    bean.COB_MONTOTOTALDOLARES = row["COB_MONTOTOTALDOLARES"].ToString();
                    bean.COB_MONTOPAGADODOLARES = row["COB_MONTOPAGADODOLARES"].ToString();
                    bean.SALDODOLARES = row["SALDODOLARES"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }


        private static double Distancia(string latIni, string lonIni, string latFin, string lonFin, string decimal_vista)
        {

            double pfLatIni = double.Parse(latIni);
            double pfLonIni = double.Parse(lonIni);
            double pfLatFin = double.Parse(latFin);
            double pfLonFin = double.Parse(lonFin);

            int cant_decimales = int.Parse(decimal_vista);

            if (Math.Abs(pfLatIni) < 0.1 && Math.Abs(pfLonIni) < 0.1) return 0;
            if (Math.Abs(pfLatFin) < 0.1 && Math.Abs(pfLonFin) < 0.1) return 0;

            double distancia;
            double liRadio = 6371; //Radio Terreste en KM  

            double dlon;
            double dlat;

            pfLatIni = Radian(pfLatIni);
            pfLonIni = Radian(pfLonIni);
            pfLatFin = Radian(pfLatFin);
            pfLonFin = Radian(pfLonFin);

            dlon = (pfLonFin - pfLonIni);
            dlat = (pfLatFin - pfLatIni);

            double a, c;

            a = (Math.Sin(dlat / 2.0)) * (Math.Sin(dlat / 2.0)) + (Math.Cos(pfLatIni) * Math.Cos(pfLatFin) * (Math.Sin(dlon / 2.0))) * (Math.Cos(pfLatIni) * Math.Cos(pfLatFin) * (Math.Sin(dlon / 2.0)));


            if (1.0 < Math.Sqrt(a))
            {
                c = 2 * Math.Asin(1.0);
            }
            else
            {
                c = 2 * Math.Asin(Math.Sqrt(a));
            }


            distancia = (liRadio * c) * 1000; //Expresado en metros  
            return Math.Round(distancia, cant_decimales);
        }

        private static double Radian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        #region CombosReportes
        public static DataTable getProductos()
        {

            DataTable dt = ReporteModel.getProductos();
            return dt;
        }
        public static List<VendedorBean> BuscarVendedoresPorGrupo(Int32 codigogrupo)
        {
            List<VendedorBean> lista = new List<VendedorBean>();
            VendedorBean obj = null;
            DataTable dt = ReporteModel.BuscarVendedoresPorGrupo(codigogrupo);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dataRow in dt.Rows)
                {
                    obj = new VendedorBean();
                    obj.Usu_PK = dataRow["USR_PK"].ToString();
                    obj.codigo = dataRow["USR_CODIGO"].ToString();
                    obj.nombre = dataRow["USR_NOMBRE"].ToString();
                    lista.Add(obj);
                }
            }
            return lista;
        }
        public static List<VendedorBean> BuscarVendedoresGrupoSupervisor(String codigogrupo, String codigosupervisor)
        {
            List<VendedorBean> lista = new List<VendedorBean>();
            VendedorBean obj = null;
            DataTable dt = ReporteModel.BuscarVendedoresGrupoSupervisor(codigogrupo, codigosupervisor);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dataRow in dt.Rows)
                {
                    obj = new VendedorBean();
                    obj.Usu_PK = dataRow["USR_PK"].ToString();
                    obj.codigo = dataRow["USR_CODIGO"].ToString();
                    obj.nombre = dataRow["USR_NOMBRE"].ToString();
                    lista.Add(obj);
                }
            }
            return lista;
        }
        public static DataTable getVendedores()
        {

            DataTable dt = ReporteModel.getVendedores();
            return dt;
        }
        public static DataTable getLimiteGraficos()
        {
            DataTable dt = ReporteModel.getLimiteGraficos();
            return dt;
        }
        public static DataTable getCondicionVenta()
        {
            DataTable dt = ReporteModel.getCondicionVenta();
            return dt;
        }
        //@001 I
        public static DataTable getTipoPedido()
        {
            DataTable dt = ReporteModel.getTipoPedido();
            return dt;
        }
        //@001 F
        //@002 I
        public static DataTable getTipoDireccion()
        {
            DataTable dt = ReporteModel.getTipoDireccion();
            return dt;
        }
        //@002 F
        #endregion

        public static PaginateAsistenciaReporteBean paginarBuscar(String fechaIni, String fechaFin, String page, String rows, String nombreb, String orden)
        {
            PaginateAsistenciaReporteBean resultado = new PaginateAsistenciaReporteBean();
            List<AsistenciaReporteBean> list = new List<AsistenciaReporteBean>();

            AsistenciaReporteBean bean;

            DataTable dt = ReporteModel.findAsistenciareporteBean(fechaIni, fechaFin, page, rows, nombreb, orden);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new AsistenciaReporteBean();

                    bean.id = row["ID"].ToString();
                    bean.usuario = row["USUARIO"].ToString();
                    bean.fini = row["FECHAINI"].ToString();
                    bean.ffin = row["FECHAFIN"].ToString();
                    bean.hini = row["HORAINI"].ToString();
                    bean.hfin = row["HORAFIN"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();

                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }
        public static DataTable findAsistenciareporteBeanXLS(String fechaIni, String fechaFin)
        {
            DataTable dt = ReporteModel.findAsistenciareporteBeanXLS(fechaIni, fechaFin);
            return dt;
        }
        public static AsistenciaReporteBean findAsistenciareporteBeanID(String id)
        {
            AsistenciaReporteBean bean = new AsistenciaReporteBean();
            DataTable dt = ReporteModel.findAsistenciareporteBeanID(id);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.usuario = row["USUARIO"].ToString();
                bean.fini = row["FECHAINI"].ToString();
                bean.ffin = row["FECHAFIN"].ToString();
                bean.hini = row["HORAINI"].ToString();
                bean.hfin = row["HORAFIN"].ToString();
                bean.latitud = row["LATITUD"].ToString();
                bean.longitud = row["LONGITUD"].ToString();
            }
            return bean;
        }
        public static PaginateVerReporteBean findVerReporteBean(String plantilla, String fechainicio, String fechafin, String perfil, String cliente,
            String usuario, String fechaestado, String novisita, String estadoasig, String estadoreal, String tipoactividad, String estado,
            String codigo, String situacioruta, String page, String rows, String nombreb, String orden)
        {
            PaginateVerReporteBean resultado = new PaginateVerReporteBean();
            List<VerReporteBean> list = new List<VerReporteBean>();

            VerReporteBean bean;

            DataTable dt = ReporteModel.findVerReporteBean(plantilla, fechainicio, fechafin, perfil, cliente, usuario, fechaestado, novisita,
                estadoasig, estadoreal, tipoactividad, estado, codigo, situacioruta, page, rows, nombreb, orden);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow dr in dt.Rows)
                {
                    bean = new VerReporteBean();

                    bean.ID = dr["CODREGISTRO"].ToString().Trim();
                    bean.FECHAINICIO = dr["FECHAINICIO"].ToString().Trim();
                    bean.FECHAFIN = dr["FECHAFIN"].ToString().Trim();
                    bean.PERFIL = dr["PERFIL"].ToString().Trim();
                    bean.CLIENTE = dr["CLIENTE"].ToString().Trim();
                    bean.USUARIO = dr["USUARIO"].ToString().Trim();
                    bean.FECHAESTADO = dr["FECHAESTADO"].ToString().Trim();
                    bean.NOVISITA = dr["NOVISITA"].ToString().Trim();
                    bean.ESTADOASIG = dr["ESTADOASIG"].ToString().Trim();
                    bean.ESTADOREAL = dr["ESTADOREAL"].ToString().Trim();
                    bean.TIPOACTIVIDAD = dr["TIPOACTIVIDAD"].ToString().Trim();
                    bean.ESTADO = dr["ESTADO"].ToString().Trim();
                    bean.ORDEN = dr["ORDEN"].ToString().Trim();
                    bean.CODIGO = dr["CODIGO"].ToString().Trim();
                    bean.SITUACIONRUTA = dr["SITUACIONRUTA"].ToString().Trim();
                    bean.TIPO = dr["TIPO"].ToString().Trim();
                    bean.LATITUD = dr["LATITUD"].ToString().Trim();
                    bean.LONGITUD = dr["LONGITUD"].ToString().Trim();

                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }
        public static DataTable findVerReporteBeanXLS(String plantilla, String fechainicio, String fechafin, String perfil, String cliente,
           String usuario, String fechaestado, String novisita, String estadoasig, String estadoreal, String tipoactividad, String estado,
           String codigo, String situacioruta)
        {
            return ReporteModel.findVerReporteBeanXLS(plantilla, fechainicio, fechafin, perfil, cliente, usuario, fechaestado, novisita,
                estadoasig, estadoreal, tipoactividad, estado, codigo, situacioruta);
        }
        public static DataTable getActividadGPSPK(String id, String tipo)
        {
            DataTable dt = ReporteModel.getActividadGPSPK(id, tipo);
            return dt;
        }
        public static List<AsistenciaReporteBean> findActividadMapaBean(String id, String tipo)
        {
            List<AsistenciaReporteBean> lista = new List<AsistenciaReporteBean>();
            AsistenciaReporteBean bean;
            DataTable dt = ReporteModel.findActividadMapaBean(id, tipo);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AsistenciaReporteBean();
                    bean.id = row["IdRegistro"].ToString();
                    bean.usuario = row["ESTADO"].ToString();
                    bean.fini = row["FECHAMOVIL"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    lista.Add(bean);
                }
            }
            return lista;
        }
        public static DataTable findCONTROLES(String codigo)
        {
            DataTable dt = ReporteModel.findCONTROLES(codigo);
            return dt;
        }
        public static List<FotoDato> fnSeleccionarFoto(String id)
        {
            List<FotoDato> lista = new List<FotoDato>();
            FotoDato bean;
            DataTable dt = ReporteModel.fnSeleccionarFoto(id);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new FotoDato();
                    bean.Foto = (Byte[])row["ImgActividad"];
                    bean.Descripcion = row["Descripcion"].ToString();
                    lista.Add(bean);
                }
            }
            return lista;
        }
        public static void borrarReserva(String codigos)
        {
            ReporteModel.borrarReserva(codigos);
        }

        public static PaginateConsolidadoBean fnObtenerReporteConsolidado(String psFechaInicio, String psFechaFin, String psCodVendedor, String psPaginaActual, String psRegistrosPorPagina, String codperfil, String codgrupo, String id_usu)
        {
            Int32 liTamanioTotal = 0;
            PaginateConsolidadoBean loPaginateConsolidadoBean = new PaginateConsolidadoBean();
            DataTable loReporteConsolidado = ReporteModel.fnObtenerReporteConsolidado(psFechaInicio, psFechaFin, psCodVendedor, psPaginaActual, psRegistrosPorPagina, codperfil, codgrupo, id_usu);
            if (loReporteConsolidado != null && loReporteConsolidado.Rows.Count > 0)
            {

                if (isReporteDinamicoGPS())
                {
                    loReporteConsolidado.Columns.Add("GPS", typeof(string));

                }

                if (loReporteConsolidado.Rows.Count > 0)
                {
                    liTamanioTotal = Int32.Parse(loReporteConsolidado.Rows[0]["TAMANIOTOTAL"].ToString());
                    loReporteConsolidado.Columns.Remove("TAMANIOTOTAL");
                    loReporteConsolidado.Columns.Remove("orden");
                }
                loPaginateConsolidadoBean.totalPages = Utility.calculateNumberOfPages(liTamanioTotal, Int32.Parse(psRegistrosPorPagina));
                loPaginateConsolidadoBean.loConsolidado = loReporteConsolidado;
            }
            return loPaginateConsolidadoBean;
        }


        public static Boolean isReporteDinamicoGPS()
        {
            Boolean resultado = false;
            DataTable dt = ReporteModel.isReporteDinamicoGPS();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    int cantidad = (int)row["cantidad"];

                    if (cantidad > 0)
                    {
                        resultado = true;
                    }

                }
            }

            return resultado;


        }

        public static DataTable fnObtenerReporteConsolidadoXLS(String psFechaInicio, String psFechaFin, String psCodVendedor, String rc_codperfil, String rc_codgrupo, String id_usu)
        {
            return ReporteModel.fnObtenerReporteConsolidadoXLS(psFechaInicio, psFechaFin, psCodVendedor, rc_codperfil, rc_codgrupo, id_usu);
        }

        public static PedidoBean obtenerGpsPedido(string idPedido)
        {
            PedidoBean resultado = new PedidoBean();
            resultado.LATITUD = "0";
            resultado.LONGITUD = "0";

            DataTable dt = ReporteModel.obtenerGpsPedido(idPedido);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    resultado.LATITUD = row["latitud"].ToString();
                    resultado.LONGITUD = row["longitud"].ToString();
                }
            }

            return resultado;


        }

        public static PaginateReporteDocumentoBean paginarDocumentoBuscar(String FechaIni, String FechaFin, String TipoDocumento, String codgrupo,
           String Vendedor, String page, String rows
           )
        {
            PaginateReporteDocumentoBean resultado = new PaginateReporteDocumentoBean();
            List<ReporteDocumentoBean> list = new List<ReporteDocumentoBean>();
            ReporteDocumentoBean bean;

            DataTable dt = ReporteModel.findReporteDocumentoBean(FechaIni, FechaFin, TipoDocumento, codgrupo, Vendedor, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ReporteDocumentoBean();

                    bean.id = row["DOC_PK"].ToString();
                    bean.doc_fecregistro = row["DOC_FECREGISTRO"].ToString();
                    bean.doc_tipo = row["DOC_TIPO"].ToString();
                    bean.gru_nombre = row["NOMBREGRUPO"].ToString();
                    bean.usr_pk = row["USR_PK"].ToString();
                    bean.usr_codigo = row["USR_CODIGO"].ToString();
                    bean.nom_usuario = row["NOM_USUARIO"].ToString();
                    bean.cli_codigo = row["CLI_CODIGO"].ToString();
                    bean.cli_nombre = row["CLI_NOMBRE"].ToString();
                    bean.serie = row["SERIE"].ToString();
                    bean.correlativo = row["CORRELATIVO"].ToString();
                    bean.pag_codigo = row["PAG_CODIGO"].ToString();
                    bean.pag_montosoles = row["MONTOSOLES"].ToString();
                    bean.pag_montodolares = row["MONTODOLARES"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }

            return resultado;
        }
    }
}