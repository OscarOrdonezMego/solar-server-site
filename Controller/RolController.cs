﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using Controller.functions;

namespace Controller
{
    public class RolController
    {


        public static Int32 getNivelPerfil(String perfil)
        {

            Int16 nivelPerfil;
            String str_nivelPerfil = "3";

            DataTable dt = RolModel.getNivelPerfil(perfil);
            foreach (DataRow dr in dt.Rows)
            {
                str_nivelPerfil = dr["NIVEL"].ToString();                              
            }
            try
            {
                nivelPerfil = Int16.Parse(str_nivelPerfil);
            }
            catch (Exception e)
            {
                nivelPerfil = 3; // por defecto nivel 3
            }

            return nivelPerfil;
        }


        public static void crear(String roles, String cadcrear,
            String cadeditar, String cadeliminar, String codigoperfil, String codigoHome)
        {
            try
            {
                Convert.ToString(RolModel.crear( roles,  cadcrear,
             cadeditar,  cadeliminar,  codigoperfil,  codigoHome));

                RolModel.updatePerfilPorRol(codigoperfil, codigoHome);
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);               
            }
        }
    }
}
