﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;

namespace Controller
{
    public class PerfilController
    {

        public static PaginatePerfilBean paginarBuscar(String codigo, String nombre, String CODIGOSESION, String page, String rows, String flag, String nombreb, String orden)
        {
            PaginatePerfilBean resultado = new PaginatePerfilBean();
            List<PerfilBean> list = new List<PerfilBean>();

            PerfilBean bean;

            DataTable dt = PerfilModel.findPerfilBean( codigo, nombre, CODIGOSESION, flag, page, rows, nombreb, orden);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PerfilBean();
                    bean.id = row["id"].ToString();
                    bean.codigo = row["codigo"].ToString();
                    bean.flag = row["FLAG"].ToString();
                    bean.codigorol = row["CODIGOROL"].ToString();
                    bean.nombre = row["NOMBRE"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }

            return resultado;
        }


        public static PerfilBean info(String id)
        {
            DataTable dt = PerfilModel.info(id);
            PerfilBean bean = new PerfilBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["id"].ToString();
                bean.codigo = row["codigo"].ToString();
                bean.flag = row["FLAG"].ToString();
                bean.codigorol = row["CODIGOROL"].ToString();
                bean.nombre = row["NOMBRE"].ToString();
            }
            return bean;
        }

        public static void crear(PerfilBean bean)
        {
            try
            {
                Convert.ToString(PerfilModel.crear(bean));
            }

            catch (Exception ex)
            {
                if (IdiomaCultura.WEB_CODIGOREPETIDO.Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensaje(ex.Message, bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void borrar(String id, String flag)
        {
            PerfilModel.borrar(id, flag);
        }
    }
}
