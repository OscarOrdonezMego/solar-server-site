﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data; //@001 I/F

/// <summary>
/// @001 GMC 12/04/2015 Se agrega método para obtener datos de Configuración por Código
/// </summary>

namespace Controller
{
    public class ConfiguracionController
    {
        public static List<ConfiguracionBean> subObtenerDatosConfiguracion()
        {
            List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
            String hashServicio = ConfiguracionModel.fnSelServicioActualHash();
            ManagerConfiguracion.HashConfig = ConfiguracionModel.fnConfiguracionHash(hashServicio);

            try
            {
                foreach (ConfiguracionBean bean in ManagerConfiguracion.HashConfig.Values)
                {
                    loLstConfiguracionBean.Add(bean);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return loLstConfiguracionBean.OrderBy(o => o.Orden).ToList();
        }

        public static Int32 subInsertarDatosConfiguracion(String psCodConfig, String psCodigo, String psValor)
        {
            try
            {
                return ConfiguracionModel.fnActualizarConfiguracion(psCodConfig, psCodigo, psValor);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static ConfiguracionBean info(String codigo)
        {
            DataTable dt = ConfiguracionModel.info(codigo);
            ConfiguracionBean bean = new ConfiguracionBean();
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.Codigo = row["Codigo"].ToString();
                bean.Descripcion = row["Descripcion"].ToString();
                bean.Valor = row["Valor"].ToString();
                bean.FlagHabilitado = row["FlagHabilitado"].ToString();
                bean.Orden = Int16.Parse(row["Orden"].ToString());
            }
            return bean;
        }
    }
}