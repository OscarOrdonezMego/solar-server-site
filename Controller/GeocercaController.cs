﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
using Model.bean;
using System.Data;

namespace Controller
{
   public class GeocercaController
    {

       public static void borrar(String id)
       {
           GeocercaModel.borrar(id);
       }


       public static GeocercaBean info(String id)
       {
           DataTable dt = GeocercaModel.info(id);
           GeocercaBean bean = new GeocercaBean();

           if (dt != null && dt.Rows.Count > 0)
           {
               DataRow row = dt.Rows[0];
               bean.id = row["IdGeocerca"].ToString();
               bean.nombre = row["DESCRIPCION"].ToString();
               bean.punto = row["PUNTOS"].ToString();
               bean.puntos = puntosGeo(bean.punto);
               
           }
           return bean;
       }


       private static List<GeocercaPuntosBean> puntosGeo(String puntos)
       {
           List<GeocercaPuntosBean> points = new List<GeocercaPuntosBean>();

           String[] rows = puntos.Split('@');

           for (int i = 0; i < rows.Length-1; i++)
           {
               String[] lalo = rows[i].Split('|');
               GeocercaPuntosBean be = new GeocercaPuntosBean();
               be.latitud = lalo[0];
               be.longitud = lalo[1];

               points.Add(be);
           }


           return points;
       }
       public static String crear(GeocercaBean bean)
       {
           try
           {
              return Convert.ToString(GeocercaModel.crear(bean));
           }

           catch (Exception ex)
           {
               if (IdiomaCultura.WEB_DESCRIPCIONREPETIDO.Equals(ex.Message))
               {
                   throw new Exception(IdiomaCultura.getMensaje(ex.Message, bean.nombre));
               }
               else
               {
                   throw new Exception(ex.Message);
               }

           }

       }


       public static List<GeocercaBean> findGeocercaBean()
       {
           DataTable dt = GeocercaModel.findGeocercaBean();

           List<GeocercaBean> lista = new List<GeocercaBean>();

           foreach (DataRow row in dt.Rows)
           {
               GeocercaBean bean = new GeocercaBean();
               bean.id = row["IdGeocerca"].ToString();
               bean.nombre = row["DESCRIPCION"].ToString();
               bean.punto = row["PUNTOS"].ToString();
               lista.Add(bean);
           }
           return lista;
       }
    }
}
