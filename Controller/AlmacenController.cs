﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class AlmacenController
    {
        public static PaginateAlmacenBean paginarBuscar(String codigo, String nombre, String flag, String page, String rows)
        {
            PaginateAlmacenBean resultado = new PaginateAlmacenBean();
            List<AlmacenBean> list = new List<AlmacenBean>();
            AlmacenBean bean;
            DataTable dt = AlmacenModel.findAlmacenBean(codigo, nombre, flag, page, rows);
            Int32 totalRegistros = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenBean();
                    bean.id = row["ALM_PK"].ToString();
                    bean.codigo = row["ALM_CODIGO"].ToString();
                    bean.nombre = row["ALM_NOMBRE"].ToString();
                    bean.direccion = row["ALM_DIRECCION"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }
        public static AlmacenBean info(String id)
        {
            DataTable dt = AlmacenModel.info(id);
            AlmacenBean bean = new AlmacenBean();
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["ALM_PK"].ToString();
                bean.codigo = row["ALM_CODIGO"].ToString();
                bean.nombre = row["ALM_NOMBRE"].ToString();
                bean.direccion = row["ALM_DIRECCION"].ToString();
                bean.flag = row["ALM_HABILITADO"].ToString();
            }
            return bean;
        }
        public static List<AlmacenBean> getAlmacen()
        {
            List<AlmacenBean> list = null;
            AlmacenBean bean = null;
            DataTable dt = AlmacenModel.getAlmacen();
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<AlmacenBean>();
                foreach (DataRow row in dt.Rows)
                {
                    bean = new AlmacenBean();
                    bean.codigo = row["ALM_CODIGO"].ToString();
                    bean.nombre = row["ALM_NOMBRE"].ToString();
                    list.Add(bean);
                }
            }
            return list;
        }
        public static void crear(AlmacenBean bean)
        {
            try
            {
                Convert.ToString(AlmacenModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public static void borrar(String id, String flag)
        {
            AlmacenModel.borrar(id, flag);
        }
    }
}