﻿using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Controller
{
    public class CargaExcelController
    {
        public static List<String> GetExcelSheetNames(String excelFile)
        {
            List<String> excelSheets = new List<String>();// new String[dt.Rows.Count];
            DataTable dt = CargaExcelModel.GetExcelSheetNames(excelFile);
            // Add the sheet name to the string array.
            foreach (DataRow row in dt.Rows)
            {
                excelSheets.Add(row["TABLE_NAME"].ToString());
            }
            return excelSheets;
        }

        public static List<String> GetArchivosExcel(String filesLocation)
        {
            String[] extensions;
            extensions = new String[] { "*.xls", "*.xlsx" };

            List<String> arrArchivosCargados = new List<String>();
            foreach (String extension in extensions)
            {
                String[] filesArr = Directory.GetFiles(filesLocation, extension, SearchOption.TopDirectoryOnly);
                foreach (String file in filesArr)
                    arrArchivosCargados.Add(file);
            }
            return arrArchivosCargados;
        }

        public static List<archivoExcelTablas> GetArchivosTablas(String filesLocation)
        {
            List<String> arrArchivosCargados = CargaExcelController.GetArchivosExcel(filesLocation);
            List<archivoExcelTablas> cargaTabla = new List<archivoExcelTablas>();
            if (arrArchivosCargados != null && arrArchivosCargados.Count > 0)
            {
                foreach (string arch in arrArchivosCargados)
                {
                    var tablasAr = new List<archivoTabla>();
                    foreach (String tabla in CargaExcelController.GetExcelSheetNames(arch))
                    {
                        tablasAr.Add(new archivoTabla { tabla = tabla.Replace("$", "") });
                    }
                    cargaTabla.Add(new archivoExcelTablas
                    {
                        archivo = arch,
                        tablas = tablasAr,
                    });
                }
            }
            else
            {
                throw new Exception("No se encontraron archivos");
            }
            return cargaTabla;
        }
        public static DataTable GetExcelData(String excelFile, String Tabla, String Columnas, String Consulta)
        {
            return CargaExcelModel.GetExcelData(excelFile, Tabla, Columnas, Consulta);
        }
    }
}
