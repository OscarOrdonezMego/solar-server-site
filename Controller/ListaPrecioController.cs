﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

///<summary>
///@001 GMC 15/04/2015 Se muestra Descuento Minimo y Maximo
///</summary>

namespace Controller
{
    public class ListaPrecioController
    {

        public static PaginateListaPrecioBean paginarBuscar(String cproducto, String ccanal, String ccondventa, String flag,  String page, String rows)
        {
            PaginateListaPrecioBean resultado = new PaginateListaPrecioBean();
            List<ListaPrecioBean> list = new List<ListaPrecioBean>();

            ListaPrecioBean bean;

            DataTable dt = ListaPrecioModel.findListaPrecio(cproducto, ccanal, ccondventa, flag, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["tamanioTotal"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ListaPrecioBean();
                    bean.codigoproducto = row["CODIGO"].ToString();;
                    bean.producto = row["PRODUCTO"].ToString();;
                    bean.codigocanal = row["CANAL"].ToString();;
                    bean.condventa = row["COND_VTA"].ToString();;
                    bean.precio = row["PRECIO"].ToString();;
                    bean.descmin = row["DESC_MIN"].ToString(); 
                    bean.descmax = row["DESC_MAX"].ToString();
                    bean.CodProducto = row["CODIGO_PRODUCTO"].ToString();
                    bean.grupoeconomico = row["grupoeconomico"].ToString();
                    bean.preciosoles = row["PrecioSoles"].ToString();
                    bean.preciodolares = row["PrecioDolares"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }

            return resultado;
        }


        public static ListaPrecioBean info(String id)
        {
            DataTable dt = ListaPrecioModel.info(id);
            ListaPrecioBean bean = new ListaPrecioBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["codigo"].ToString();
                bean.codigoproducto = row["codigo_producto"].ToString();
                bean.codigocanal = row["codigo_canal"].ToString();
                bean.precio = row["precio"].ToString();
                bean.condventa = row["condvta_codigo"].ToString();
                bean.flag = row["flag"].ToString();
                bean.producto = row["pro_nombre"].ToString();
                bean.descmin = row["DESC_MIN"].ToString(); //@001 I/F
                bean.descmax = row["DESC_MAX"].ToString(); //@001 I/F
                bean.grupoeconomico = row["grupoeconomico"].ToString();
                bean.preciosoles = row["PrecioSoles"].ToString();
                bean.preciodolares = row["PrecioDolares"].ToString();
            }
            return bean;
        }

        public static void crear(ListaPrecioBean bean)
        {
            try
            {
                Convert.ToString(ListaPrecioModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeListaPrecioExistente());
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static void editar(ListaPrecioBean bean)
        {
            try
            {
                Convert.ToString( ListaPrecioModel.editar(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeListaPrecioExistente());
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static void borrar(String id, String flag)
        {
            ListaPrecioModel.borrar(id, flag);
        }



        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
        public static void crearDescuentoVolumen(DescuentoVolumen bean)
        {
            try
            {
                Convert.ToString(ListaPrecioModel.CrearDescuentoVolumen(bean));
            }

            catch (Exception ex)
            {
                
                    throw new Exception(ex.Message);
            }
        }
        public static void BorrarDescuentoVolumen(Int32 idlistaprecio)
        {
            try
            {
                Convert.ToString(ListaPrecioModel.borrarDescuentoVolumen(idlistaprecio));
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public static List<DescuentoVolumen> BuscarDescuentoVolumen(Int32 idlistaprecio)
        {
            List<DescuentoVolumen> listaDescuentoVolumen = new List<DescuentoVolumen>();

            DataTable dt = ListaPrecioModel.BuscarDescuentoVolumen(idlistaprecio);
            Int32 contador = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    contador = contador + 1;
                    DescuentoVolumen bean = new DescuentoVolumen();
                    bean.RMIN = row["RangoMinimo"].ToString();
                    bean.RMAX = row["RangoMaximo"].ToString().Trim();
                    bean.DSCTOMIN = row["DescuentoMinimo"].ToString().Trim();
                    bean.DSCTOMAX = row["DescuentoMaximo"].ToString().Trim();
                    bean.CODIGOPROD = row["CodigoProducto"].ToString();
                    bean.Codfila = contador.ToString();
                    listaDescuentoVolumen.Add(bean);
                }

            }
            return listaDescuentoVolumen;
        }
    }
}
