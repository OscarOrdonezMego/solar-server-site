﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class GrupoController
    {
        public static Int32 crearGrupo(GrupoBean poGrupoBean)
        {
            try
            {
                return GrupoModel.crearGrupo(poGrupoBean);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public static PaginateGrupoBeans fnBuscarListarGrupo(Int32 pagina, Int32 totalpagina, String codigo, String nombre, String flag)
        {
            PaginateGrupoBeans loPaginateGrupoBeans = new PaginateGrupoBeans();
            List<GrupoBean> listGrupoBean = new List<GrupoBean>();

            GrupoBean bean;

            DataTable dt = GrupoModel.fnBuscarListarGrupo(pagina, totalpagina, codigo,nombre, flag);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new GrupoBean();
                    bean.Grupo_PK =Int32.Parse( row["Grupo_PK"].ToString());
                    bean.nombre = row["Nombre_grupo"].ToString(); ;
                    bean.codigo = row["Codigo_grupo"].ToString(); 
                    listGrupoBean.Add(bean);
                }

                loPaginateGrupoBeans.LsListaGrupoBean= listGrupoBean;
                loPaginateGrupoBeans.TotalFila= Utility.calculateNumberOfPages(totalRegistros, totalpagina);

            }

            return loPaginateGrupoBeans;
        }
        public static GrupoBean fnBuscarGrupo(String codigo)
        {
            
            GrupoBean bean=null;

            DataTable dt = GrupoModel.fnBuscarGrupo(codigo);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new GrupoBean();
                    bean.Grupo_PK = Int32.Parse(row["Grupo_PK"].ToString());
                    bean.nombre = row["NombreGrupo"].ToString(); ;
                    bean.codigo = row["Codigo_Grupo"].ToString();
                }


            }

            return bean;
        }
        public static void EditarGrupo(GrupoBean poGrupoBean)
        {
            try
            {
                GrupoModel.EditarGrupo(poGrupoBean);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public static void EliminarGrupo(String codigo,String flag)
        {
            try
            {
                GrupoModel.EliminarGrupo(codigo,flag);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public static List<GrupoBean> fnListarGrupo()
        {
            List<GrupoBean> loLstGrupoBean = new List<GrupoBean>();
            GrupoBean loGrupoBean = null;
            DataTable loDataTable = GrupoModel.fnListarGrupo();
            try
            {
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.Grupo_PK = Int32.Parse(loDataRow["Grupo_PK"].ToString());
                        loGrupoBean.codigo = loDataRow["Codigo_Grupo"].ToString();
                        loGrupoBean.nombre = loDataRow["NombreGrupo"].ToString();
                        loLstGrupoBean.Add(loGrupoBean);
                    }
                }
                return loLstGrupoBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
