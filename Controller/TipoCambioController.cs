﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

namespace Controller
{
    public class TipoCambioController
    {
        public PaginateTipoCambioBean findTipoCambioBean(TipoCambioBean tipocambio, int page, int rows)
        {
            PaginateTipoCambioBean resultado = new PaginateTipoCambioBean();
            List<TipoCambioBean> list = new List<TipoCambioBean>();
            TipoCambioBean bean;
            DataTable dt = TipoCambioModel.findTipoCambioBean(tipocambio, page, rows);
            int totalRegistros = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new TipoCambioBean();
                    bean.id = Int32.Parse(row["id"].ToString());
                    bean.codigo = row["codigo"].ToString();
                    bean.valor = row["valor"].ToString();
                    bean.fecha = DateTime.Parse(row["fecha"].ToString());
                    bean.flag = row["flag"].ToString();

                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, rows);
            }
            return resultado;
        }

        public static Int32 crearTipoCambio(TipoCambioBean poTipoCambioBean)
        {
            try
            {
                return TipoCambioModel.crearTipoCambio(poTipoCambioBean);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
		
		public static TipoCambioBean buscarTipoCambio(string tipo)
        {
            try
            {
                TipoCambioBean bean = null;
                DataTable dt = TipoCambioModel.buscarTipoCambio(tipo);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        bean = new TipoCambioBean();
                        bean.id = Int32.Parse(row["TC_PK"].ToString());
                        bean.codigo = row["TC_CODIGO"].ToString();
                        bean.valor = row["TC_VALOR"].ToString();
                        bean.fecha = DateTime.Parse(row["TC_FECHA"].ToString());
                        bean.flag = row["FLGENABLE"].ToString();
                    }
                }
                return bean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
