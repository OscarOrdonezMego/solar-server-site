﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

namespace Controller
{
    public class PedidoDetalleController
    {
        public static PaginatePedidoDetalleBean paginarBuscar(String cID, String cCODIGO, String cNOMBRE, String cflag, String cpage, String crows)
        {
            PaginatePedidoDetalleBean resultado = new PaginatePedidoDetalleBean();
            List<PedidoDetalleBean> list = new List<PedidoDetalleBean>();

            PedidoDetalleBean bean;

            DataTable dt = PedidoDetalleModel.findPedidoDetalle(cID, cCODIGO, cNOMBRE, cflag, cpage, crows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new PedidoDetalleBean();
                    bean.IDPED = row["PED_PK"].ToString();
                    bean.id = row["det_pk"].ToString();
                    bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                    bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                    bean.DET_CANTIDAD = row["DET_CANTIDAD"].ToString();
					bean.DET_PRECIO = ManagerConfiguration.moneda + " " + row["DET_PRECIO"].ToString();
                    bean.DET_PRECIO_SOLES = ManagerConfiguration.moneda + " " + row["DET_PRECIO_SOLES"].ToString();
                    bean.DET_PRECIO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_PRECIO_DOLARES"].ToString();
                    bean.DET_DESCUENTO = row["DET_DESCUENTO"].ToString();
					bean.DET_MONTO = ManagerConfiguration.moneda + " " + row["DET_MONTO"].ToString();
                    bean.DET_MONTO_SOLES = ManagerConfiguration.moneda + " " + row["DET_MONTO_SOLES"].ToString();
                    bean.DET_MONTO_DOLARES = ManagerConfiguration.moneda_dolares + " " + row["DET_MONTO_DOLARES"].ToString();
                    bean.DET_DESCRIPCION = row["DET_DESCRIPCION"].ToString();
                    bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(crows));
            }

            return resultado;
        }

        public static PedidoDetalleBean info(String id)
        {

            String[] codigos;
            codigos = id.Split('|');

            String dID;
            String dIDdet;

            dID = codigos[0].ToString();
            dIDdet = codigos[1].ToString();

            DataTable dt = PedidoDetalleModel.info(dID, dIDdet);
            PedidoDetalleBean bean = new PedidoDetalleBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                bean.Pro_pk = Convert.ToInt32(row["PRO_PK"].ToString());
                bean.PRO_CODIGO = row["PRO_CODIGO"].ToString();
                bean.PRO_NOMBRE = row["PRO_NOMBRE"].ToString();
                bean.DET_CANTIDAD = row["DET_CANTIDAD"].ToString();
				bean.DET_PRECIO = row["DET_PRECIOBASE"].ToString();
                bean.DET_PRECIO_SOLES = row["PrecioBaseSoles"].ToString();
                bean.DET_PRECIO_DOLARES = row["PrecioBaseDolares"].ToString();
                bean.DET_DESCUENTO = row["DET_DESCUENTO"].ToString();
				bean.DET_MONTO = row["DET_MONTO"].ToString();
                bean.DET_MONTO_SOLES = row["MontoSoles"].ToString();
                bean.DET_MONTO_DOLARES = row["MontoDolares"].ToString();
                bean.DET_DESCRIPCION = row["DET_DESCRIPCION"].ToString();
                bean.DET_BONIFICACION = row["DET_BONIFICACION"].ToString();
                bean.TipoMoneda = row["TipoMoneda"].ToString();
            }
            return bean;
        }

        public static void crear(PedidoDetalleBean bean)
        {
            try
            {
                Convert.ToString(PedidoDetalleModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.PRO_CODIGO));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void editar(PedidoDetalleBean bean)
        {
            try
            {
                Convert.ToString(PedidoDetalleModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.PRO_CODIGO));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public static void borrar(String id, String flag)
        {
            PedidoDetalleModel.borrar(id, flag);
        }
        
        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }
            }
            return lst;
        }

        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }
            }
            return lst;
        }
        public static List<AlmacenBean> fnBuscarAlmacen(String psCodigoPresentacion)
        {
            DataTable dt = PedidoDetalleModel.fnBuscarAlmacen(psCodigoPresentacion);

            List<AlmacenBean> lst = new List<AlmacenBean>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    AlmacenBean bean = new AlmacenBean();
                    bean.codigo = row["ALM_PK"].ToString();
                    bean.nombre = row["ALM_NOMBRE"].ToString();
                    bean.Stock = row["STOCK"].ToString();
                    bean.PrePK = row["PRE_PK"].ToString();
                    bean.ALM_CODIGO = row["ALM_CODIGO"].ToString();
                    lst.Add(bean);
                }
            }
            return lst;
        }


        public static ProductoPresentacionBean fnBuscarCantidadPresentacion(String psCodigoPresentacion)
        {
            DataTable dt = PedidoDetalleModel.fnBuscarCantidadPresentacion(psCodigoPresentacion);
            ProductoPresentacionBean bean = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new ProductoPresentacionBean();
                    bean.Cantidad = row["CANTIDAD"].ToString();
                    bean.UnidaddeFrancionamieno = row["UNIDAD_FRACCIONAMIENTO"].ToString();
                }

            }
            return bean;
        }
        public static void crearPedidoDetalle(PedidoDetalleBean bean)
        {
            try
            {
                Convert.ToString(PedidoDetalleModel.crearPedidoDetalle(bean));
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public static void editarPedidoDetalle(PedidoDetalleBean bean)
        {
            try
            {
                Convert.ToString(PedidoDetalleModel.editarPedidoDetalle(bean));
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        } 
        public static List<ListaPrecioBean> fnBuscarListaPrecio(Int32 PRE_PK)
        {
            try
            {
                List<ListaPrecioBean> loLstListaPrecioBean=new List<ListaPrecioBean>();
                DataTable loDataTable = PedidoDetalleModel.fnBuscarListaPrecio(PRE_PK);
                ListaPrecioBean loListaPrecioBean = null; 
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loListaPrecioBean = new ListaPrecioBean();
                        loListaPrecioBean.id = loDataRow["CODIGO"].ToString();
                        loListaPrecioBean.Codpresentacion = loDataRow["PRE_PK"].ToString();
						loListaPrecioBean.precio = loDataRow["PRECIO"].ToString();
                        if (loDataRow["PrecioSoles"] != null) loListaPrecioBean.preciosoles = loDataRow["PrecioSoles"].ToString();
                        if (loDataRow["PrecioDolares"] != null) loListaPrecioBean.preciodolares = loDataRow["PrecioDolares"].ToString();
                        loLstListaPrecioBean.Add(loListaPrecioBean);
                    }
                }
                return loLstListaPrecioBean;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static PresentacionBean fnBuscarCantidadPresentacion(Int32 PRE_PK)
        {
            try
            {
                DataTable loDataTable = PedidoDetalleModel.fnBuscarCantidadPresentacion(PRE_PK);
                PresentacionBean loPresentacionBean = null; 
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loPresentacionBean = new PresentacionBean();
                        loPresentacionBean.Cantidad = loDataRow["CANTIDAD"].ToString();
                        loPresentacionBean.UnidadFrancionamiento = loDataRow["UNIDAD_FRACCIONAMIENTO"].ToString();
                        loPresentacionBean.PRO_UNIDADDEFECTO = loDataRow["PRO_UNIDADDEFECTO"].ToString();
                        loPresentacionBean.Nombre = loDataRow["NOMBRE"].ToString();
                    }
                }
                return loPresentacionBean;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public static List<PresentacionBean> fnBuscarCantidadPresentacionLista(Int32 PRE_PK)
        {
            try
            {
                List<PresentacionBean> loLstPresentacionBean = new List<PresentacionBean>();
                DataTable loDataTable = PedidoDetalleModel.fnBuscarCantidadPresentacion(PRE_PK);
                PresentacionBean loPresentacionBean = null;
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loPresentacionBean = new PresentacionBean();
                        loPresentacionBean.Cantidad = loDataRow["CANTIDAD"].ToString();
                        loPresentacionBean.UnidadFrancionamiento = loDataRow["UNIDAD_FRACCIONAMIENTO"].ToString();
                        loPresentacionBean.PRO_UNIDADDEFECTO = loDataRow["PRO_UNIDADDEFECTO"].ToString();
                        loLstPresentacionBean.Add(loPresentacionBean);
                    }
                }
                return loLstPresentacionBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public static PresentacionBean fnBuscarStockPresentacion(Int32 PRE_PK)
        {
            try
            {
                DataTable loDataTable = PedidoDetalleModel.fnBuscarStockPresentacion(PRE_PK);
                PresentacionBean loPresentacionBean = null;
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loPresentacionBean = new PresentacionBean();
                        loPresentacionBean.Stock = loDataRow["STOCK"].ToString();
                        loPresentacionBean.Cantidad = loDataRow["CANTIDAD"].ToString();
                        loPresentacionBean.PRO_UNIDADDEFECTO = loDataRow["PRO_UNIDADDEFECTO"].ToString();
                    }
                }
                return loPresentacionBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        public static List<ProductoPresentacionBean> fnBuscarPresentacionesConStock(String pro_codigo)
        {
            try
            {
                List<ProductoPresentacionBean> loLstPresentacionBean = new List<ProductoPresentacionBean>();
                DataTable loDataTable = PedidoDetalleModel.fnBuscarPresentacionesConStock(pro_codigo);
                ProductoPresentacionBean loPresentacionBean = null;
                if (loDataTable != null && loDataTable.Rows.Count > 0)
                {
                    foreach (DataRow loDataRow in loDataTable.Rows)
                    {
                        loPresentacionBean = new ProductoPresentacionBean();
                        loPresentacionBean.CodigoPresentacion = loDataRow["PRE_PK"].ToString();
                        loPresentacionBean.Nombre = loDataRow["NOMBRE"].ToString();
                        loPresentacionBean.Cantidad = loDataRow["CANTIDAD"].ToString();
                        loLstPresentacionBean.Add(loPresentacionBean);
                    }
                }
                return loLstPresentacionBean;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        
    }
}
