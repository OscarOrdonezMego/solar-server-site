﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;
using Controller.functions;
using System.Web;

namespace Controller
{
    public class NServicesController 
    {

        public static bool nServicesExisteWebConfig()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"] != null
                && !System.Configuration.ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"].ToString().Equals(""))
                return true;
            return false;
        }

        public static String nServicesObtenerRutaInstancia()
        {
            return System.Configuration.ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"].ToString();
        }
    }
}
