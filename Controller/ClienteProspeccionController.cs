﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Model;
using Model.bean;
using business.functions;


namespace Controller
{
    public class ClienteProspeccionController
    {

        public static PaginateClienteDireccionProspeccionBean paginarClienteDireccionBuscar(String ID, String coddir, String direccion, String flag, String page, String rows)
        {
            PaginateClienteDireccionProspeccionBean resultado = new PaginateClienteDireccionProspeccionBean();
            List<ClienteDireccionProspeccionBean> list = new List<ClienteDireccionProspeccionBean>();

            ClienteDireccionProspeccionBean bean;

            DataTable dt = ClienteProspeccionModel.findClienteDireccionBean(ID, coddir, direccion, flag, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ClienteDireccionProspeccionBean();
                    bean.dir_pk = row["ide"].ToString();
                    bean.cli_pk = row["CLIPK"].ToString();
                    bean.codigo = row["coddir"].ToString();
                    bean.nombre = row["direccion"].ToString();
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    bean.dir_tipo = row["DIR_TIPO"].ToString();
                    bean.dir_tipo_des = row["DIR_TIPO_DES"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }

        public static PaginateClienteProspeccionBean paginarBuscar(String codvendedor, String prospecto, String fini, String ffin, String page, String rows)
        {
            PaginateClienteProspeccionBean resultado = new PaginateClienteProspeccionBean();
            List<ClienteProspeccionBean> list = new List<ClienteProspeccionBean>();

            ClienteProspeccionBean bean;

            DataTable dt = ClienteProspeccionModel.findClienteProspeccionBean(codvendedor, prospecto, fini, ffin, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ClienteProspeccionBean();
                    bean.id = row["ID"].ToString();
                    bean.codigo = row["CODIGO"].ToString();
                    bean.nombre = row["NOMBRE"].ToString();
                    bean.direccion = row["DIRECCION"].ToString();
                    bean.vendedor = row["VENDEDOR"].ToString();
                    bean.CampoAdicional1 = row["CampoAdicional1"].ToString();
                    bean.CampoAdicional2 = row["CampoAdicional2"].ToString();
                    bean.CampoAdicional3 = row["CampoAdicional3"].ToString();
                    bean.CampoAdicional4 = row["CampoAdicional4"].ToString();
                    bean.CampoAdicional5 = row["CampoAdicional5"].ToString();
                    bean.tipocliente = row["TIPOCLIENTE"].ToString();
                    bean.giro = row["GIRO"].ToString();
                    bean.fecha = row["FECHAREGISTRO"].ToString();
                    bean.latitud = row["LATITUD"].ToString();
                    bean.longitud = row["LONGITUD"].ToString();
                    bean.observacion = row["OBSERVACION"].ToString();
                    bean.fechaMovil = row["FECHAMOVIL"].ToString();
                    bean.flag = row["FLAG"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }

        public static ClienteProspeccionBean info(String id)
        {
            DataTable dt = ClienteProspeccionModel.info(id);
            ClienteProspeccionBean bean = new ClienteProspeccionBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["PK"].ToString();
                bean.codigo = row["CODIGO"].ToString();
                bean.nombre = row["NOMBRE"].ToString();
                bean.tipocliente = row["TIPO"].ToString();
                bean.giro = row["GIRO"].ToString();
                bean.flag = row["ENABLE"].ToString();
                bean.direccion = row["DIRECCION"].ToString();
                bean.latitud = row["LATITUD"].ToString();
                bean.longitud = row["LONGITUD"].ToString();
                bean.CampoAdicional1 = row["CampoAdicional1"].ToString();
                bean.CampoAdicional2 = row["CampoAdicional2"].ToString();
                bean.CampoAdicional3 = row["CampoAdicional3"].ToString();
                bean.CampoAdicional4 = row["CampoAdicional4"].ToString();
                bean.CampoAdicional5 = row["CampoAdicional5"].ToString();

            }
            return bean;
        }

        public static void crear(ClienteProspeccionBean bean)
        {
            try
            {
                Convert.ToString(ClienteProspeccionModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

    }
}