﻿using System;
using System.Collections.Generic;
using Model.bean;
using Model;
using System.Data;
using System.Web;

///<summary>
///@001 GMC 05/05/2015 Carga de archivos Almacen y Almacén Producto
///</summary>
namespace Controller
{
    public class GeneralController
    {

        public static String obtenerTemaActual()
        {
            return obtenerTemaActual(false);
        }

        public static String obtenerTemaActual(bool fromSession)
        {
            try
            {
                if (fromSession
                && HttpContext.Current != null
                && HttpContext.Current.Session != null
                && HttpContext.Current.Session[SessionManager.THEME_SESSION] != null)
                {
                    return HttpContext.Current.Session[SessionManager.THEME_SESSION].ToString();
                }

                return GeneralModel.obtenerTemaActual();
            }
            catch (Exception e)
            {
                return System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
            }
        }

        public static List<ConfiguracionBean> getConfiguracionVal()
        {
            List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
            DataTable loDataTable = GeneralModel.getConfiguracionValidacion();
            ConfiguracionBean loConfiguracionBean = null;
            if (loDataTable != null && loDataTable.Rows.Count > 0)
            {
                foreach (DataRow loDataRow in loDataTable.Rows)
                {
                    loConfiguracionBean = new ConfiguracionBean();
                    loConfiguracionBean.Codigo = loDataRow["Codigo"].ToString();
                    loConfiguracionBean.Descripcion = loDataRow["Descripcion"].ToString();
                    loConfiguracionBean.Tipo = loDataRow["CodConfig"].ToString();
                    loConfiguracionBean.TipoPadre = loDataRow["CodConfigPadre"].ToString();
                    loConfiguracionBean.Valor = loDataRow["Valor"].ToString();
                    loConfiguracionBean.CodServicio = loDataRow["CodServicio"].ToString();
                    loConfiguracionBean.FlagHabilitado = loDataRow["FlagHabilitado"].ToString();
                    loConfiguracionBean.Orden = Int16.Parse(loDataRow["Orden"].ToString());
                    loLstConfiguracionBean.Add(loConfiguracionBean);
                }
            }
            return loLstConfiguracionBean;
        }

        public static void subInicializarConfiguracion()
        {
            String hashServicio = ConfiguracionModel.fnSelServicioActualHash();
            ManagerConfiguration.HashConfig = ConfiguracionModel.fnConfiguracionHash(hashServicio);
            //  IdiomaCultura.Datos = getCultureIdioma();
            // ManagerConfiguracion.Version = fnVersionSuite();
        }


        public static Dictionary<string, string> getCultureIdioma(string tipo)
        {
            Dictionary<string, string> datos = new Dictionary<string, string>();
            String CULTURE = "";
            DataTable dt = GeneralModel.getCultureIdioma(tipo);
            foreach (DataRow dr in dt.Rows)
            {

                CULTURE = dr["CULTURE"].ToString();
                datos.Add(dr["CODIGO"].ToString(), dr["DESCRIPCION"].ToString());

            }
            datos.Add(IdiomaCultura.CULTURE, CULTURE);


            return datos;
        }

        public static List<BEGenGrafico> getActividadesRealizadas(String fechainicio, String fechafin, String Usuario)
        {
            List<BEGenGrafico> lista = new List<BEGenGrafico>();
            Int64 total = 0;
            DataTable dt = GeneralModel.getActividadesRealizadas(fechainicio, fechafin, Usuario);
            foreach (DataRow dr in dt.Rows)
            {
                BEGenGrafico bean = new BEGenGrafico();
                bean.Total = dr["Total"].ToString();
                bean.usuario = dr["usuario"].ToString();
                bean.avrusuario = dr["avrusuario"].ToString();
                lista.Add(bean);

                total = incrementarTotal(total, bean.Total);
            }

            // si ninguno de los usuarios realizo actividades
            // no se envia ningun resultado
            if (total <= 0)
            {
                lista = new List<BEGenGrafico>();
            }

            return lista;
        }

        public static List<BEGenGrafico> getActProgramdas(String fechainicio, String fechafin, String User)
        {

            List<BEGenGrafico> lista = new List<BEGenGrafico>();
            DataTable dt = GeneralModel.getActProgramdas(fechainicio, fechafin, User);
            foreach (DataRow dr in dt.Rows)
            {
                BEGenGrafico bean = new BEGenGrafico();
                bean.realizado = dr["realizado"].ToString();
                bean.programado = dr["programado"].ToString();
                lista.Add(bean);
            }
            return lista;
        }

        public static List<BEGenGrafico> getNoVisitasRea(String fechainicio, String fechafin, String User)
        {



            List<BEGenGrafico> lista = new List<BEGenGrafico>();
            DataTable dt = GeneralModel.getNoVisitasRea(fechainicio, fechafin, User);
            foreach (DataRow dr in dt.Rows)
            {
                BEGenGrafico bean = new BEGenGrafico();
                bean.novisita = dr["novisita"].ToString();
                bean.programado = dr["programado"].ToString();
                lista.Add(bean);
            }
            return lista;
        }


        public static List<ParametroBean> getParametrosActividad()
        {


            List<ParametroBean> lista = new List<ParametroBean>();
            DataTable dt = GeneralModel.getParametrosActividad();
            foreach (DataRow dr in dt.Rows)
            {
                ParametroBean bean = new ParametroBean();
                bean.idParametro = dr["IDPARAMETRO"].ToString();
                bean.descripcion = Model.bean.IdiomaCultura.getMensaje(dr["DESCRIPCION"].ToString());
                bean.valor = dr["VALOR"].ToString();
                bean.color = dr["COLOR"].ToString();
                lista.Add(bean);
            }
            return lista;
        }


        public static ParametroBean getOneParametro(String id)
        {
            DataTable dt = GeneralModel.getOneParametro(id);
            ParametroBean bean = new ParametroBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.idParametro = row["IDPARAMETRO"].ToString();
                bean.descripcion = row["DESCRIPCION"].ToString();
                bean.color = row["COLOR"].ToString();
                bean.valor = row["VALOR"].ToString();

            }
            return bean;
        }



        public static void updateParametro(ParametroBean bean)
        {
            try
            {
                Convert.ToString(GeneralModel.updateParametro(bean));
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }

        }

        public static String getTextoCuentas()
        {

            return GeneralModel.getTextoCuentas();

        }

        public static void actualizarEtiquetas(String etiqueta)
        {
            try
            {
                Convert.ToString(GeneralModel.actualizarEtiquetas(etiqueta));
                IdiomaCultura.datos = getCultureIdioma(IdiomaCultura.TIPO_WEB);
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }
        }

        public static bool autoGencodigo(String vMantenimiento)
        {

            return GeneralModel.autoGencodigo(vMantenimiento);

        }



        public static List<ComboBean> getGeneralBean(String estado, String spName, String usuarioSesion)
        {
            List<ComboBean> lista = new List<ComboBean>();
            DataTable dt = GeneralModel.getGeneralBean(estado, spName, usuarioSesion);
            foreach (DataRow dr in dt.Rows)
            {
                ComboBean bean = new ComboBean();
                bean.codigo = dr["codigo"].ToString();
                bean.nombre = dr["nombre"].ToString();
                bean.flag = dr["FlgRegHabilitado"].ToString();
                lista.Add(bean);
            }
            return lista;
        }


        public static List<ComboBean> getGeneralBean(String estado, String spName)
        {

            List<ComboBean> lista = new List<ComboBean>();
            DataTable dt = GeneralModel.getGeneralBean(estado, spName);
            foreach (DataRow dr in dt.Rows)
            {
                ComboBean bean = new ComboBean();
                bean.codigo = dr["codigo"].ToString();
                bean.nombre = dr["nombre"].ToString();
                bean.flag = dr["FlgRegHabilitado"].ToString();
                lista.Add(bean);
            }
            return lista;
        }



        public static List<ComboBean> findFiltros(String id, String tipo)
        {
            List<ComboBean> lista = new List<ComboBean>();
            DataTable dt = GeneralModel.findFiltros(id, tipo);
            foreach (DataRow dr in dt.Rows)
            {
                ComboBean bean = new ComboBean();
                bean.codigo = dr["IdFiltro"].ToString();
                bean.nombre = dr["Descripcion"].ToString();
                bean.flag = dr["CHECKED"].ToString();
                lista.Add(bean);
            }
            return lista;
        }


        public static List<ComboBean> getGeneralBean(String spName)
        {

            List<ComboBean> lista = new List<ComboBean>();
            DataTable dt = GeneralModel.getGeneralBean(spName);
            foreach (DataRow dr in dt.Rows)
            {
                ComboBean bean = new ComboBean();
                bean.codigo = dr["IdPlantilla"].ToString();
                bean.nombre = dr["Descripcion"].ToString();
                bean.flag = dr["FlgRegHabilitado"].ToString();
                lista.Add(bean);
            }
            return lista;
        }


        public static List<ComboBean> getGeneralBean2(String spName)
        {

            List<ComboBean> lista = new List<ComboBean>();
            DataTable dt = GeneralModel.getGeneralBean(spName);
            foreach (DataRow dr in dt.Rows)
            {
                ComboBean bean = new ComboBean();
                bean.codigo = dr["CONDVTA_codigo"].ToString();
                bean.nombre = dr["CONDVTA_NOMBRE"].ToString();

                lista.Add(bean);
            }
            return lista;
        }

        public static Int64 incrementarTotal(Int64 valorActual, String valorIncrementar)
        {

            Int64 totalRespuesta = 0;

            try
            {

                totalRespuesta = valorActual + Int64.Parse(valorIncrementar);

            }
            catch (Exception e)
            {
                totalRespuesta = valorActual;
            }

            return totalRespuesta;

        }


        public static DataTable findCONTROLES()
        {


            return GeneralModel.findCONTROLES();


        }

        /****/

        public static void saveConfigMenu(int idUsuario, int idOpcion, String perfil, int Estado)
        {

            int ret = GeneralModel.saveConfigMenu(idUsuario, idOpcion, perfil, Estado);
        }


        public static List<PermisosBean> getOpciones(String idUsuario)
        {

            DataTable dt = GeneralModel.PermisoOpciones(idUsuario);

            List<PermisosBean> lpm = new List<PermisosBean>();

            foreach (DataRow dr in dt.Rows)
            {
                PermisosBean pm = new PermisosBean();
                pm.idOpcion = Convert.ToInt32(dr["IDOPCION"].ToString());
                pm.NombreOpcion = dr["NOMBREOPCION"].ToString();
                pm.NombreCategoria = dr["NOMBRECATEGORIA"].ToString();
                pm.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                lpm.Add(pm);
            }

            return lpm;
        }


        public static DataTable getErroresCarga(String file)
        {


            string SP = "";
            switch (file.ToUpper())
            {
                case "PRODUCTO":
                    SP = "USPC_ERROR_PRODUCTO";
                    break;
                case "LISTAPRECIOS":
                    SP = "USPC_ERROR_LISTAPRECIOS";
                    break;
                case "CLIENTE":
                    SP = "USPC_ERROR_CLIENTE";
                    break;
                case "DIRECCION":
                    SP = "USPC_ERROR_DIRECCION";
                    break;
                case "RUTA":
                    SP = "USPC_ERROR_RUTA";
                    break;
                case "VENDEDOR":
                    SP = "USPC_ERROR_USUARIO";
                    break;
                case "GENERAL":
                    SP = "USPC_ERROR_GENERAL";
                    break;
                case "COBRANZA":
                    SP = "USPC_ERROR_COBRANZA";
                    break;
                case "FAMILIA":
                    SP = "USPC_ERROR_FAMILIA";
                    break;
                case "MARCA":
                    SP = "USPC_ERROR_MARCA";
                    break;
                case "MARCA_PRODUCTO":
                    SP = "USPC_ERROR_MARCA_PRODUCTO";
                    break;
                //@001 I
                case "ALMACEN":
                    SP = "USPC_ERROR_ALMACEN";
                    break;
                case "ALMACEN_PRODUCTO_PRESENTACION":
                    SP = "USPC_ERROR_ALMACEN_PRODUCTO_PRESENTACION";
                    break;
                //@001 F
                case "PRESENTACION":
                    SP = "USPC_ERROR_PRESENTACION";
                    break;
                case "FAMILIA_PRODUCTO":
                    SP = "USPC_ERROR_FAMILIA_PRODUCTO";
                    break;
                case "PRODUCTO.TXT":
                    SP = "USPC_ERROR_PRODUCTO";
                    break;
                case "LISTAPRECIOS.TXT":
                    SP = "USPC_ERROR_LISTAPRECIOS";
                    break;
                case "CLIENTE.TXT":
                    SP = "USPC_ERROR_CLIENTE";
                    break;
                case "DIRECCION.TXT":
                    SP = "USPC_ERROR_DIRECCION";
                    break;
                case "RUTA.TXT":
                    SP = "USPC_ERROR_RUTA";
                    break;
                case "VENDEDOR.TXT":
                    SP = "USPC_ERROR_USUARIO";
                    break;
                case "GENERAL.TXT":
                    SP = "USPC_ERROR_GENERAL";
                    break;
                case "COBRANZA.TXT":
                    SP = "USPC_ERROR_COBRANZA";
                    break;
                case "ALMACEN.TXT":
                    SP = "USPC_ERROR_ALMACEN";
                    break;
                case "ALMACEN_PRODUCTO_PRESENTACION.TXT":
                    SP = "USPC_ERROR_ALMACEN_PRODUCTO_PRESENTACION";
                    break;
                case "PRESENTACION.TXT":
                    SP = "USPC_ERROR_PRESENTACION";
                    break;
                case "MARCA.TXT":
                    SP = "USPC_ERROR_MARCA";
                    break;
                case "FAMILIA.TXT":
                    SP = "USPC_ERROR_FAMILIA";
                    break;
                case "FAMILIA_PRODUCTO.TXT":
                    SP = "USPC_ERROR_FAMILIA_PRODUCTO";
                    break;
                case "MARCA_PRODUCTO.TXT":
                    SP = "USPC_ERROR_MARCA_PRODUCTO";
                    break;
            }

            return CargaModel.getErrores(SP);

        }


    }


}
