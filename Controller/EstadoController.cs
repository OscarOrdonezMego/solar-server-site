﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;
using Controller.functions;

namespace Controller
{
   public class EstadoController
    {

        public static string load()
        {
            DataSet ds = EstadoModel.getPk();


            List<BEEstados> lstEstado = new List<BEEstados>();
            List<BEControl> lstControl;
            if (ds != null && ds.Tables.Count > 0)
            {


                foreach (DataRow drE in ds.Tables[0].Rows)
                {
                    BEEstados beE = new BEEstados();

                    beE.codigo = drE["IdFormulario"].ToString();


                    lstControl = new List<BEControl>();
                    foreach (DataRow drC in ds.Tables[1].Rows)
                    {
                        if (beE.codigo == drC["IdFormulario"].ToString())
                        {
                            BEControl beC = new BEControl();
                            beC.tipo = drC["IdTipoControl"].ToString();
                            beC.max = drC["MaxCaracteres"].ToString();
                            beC.obligatorio = drC["FlgObligatorio"].ToString();
                            beC.grupo = drC["IdGrupo"].ToString();
                            beC.grupoDesc = drC["Nombre"].ToString();
                            beC.etiqueta = drC["EtiquetaControl"].ToString();
                            beC.editable = drC["flgEditable"].ToString();
                            beC.idEstControl = drC["IdControl"].ToString();

                            lstControl.Add(beC);

                        }

                    }
                    beE.controles = lstControl;
                    lstEstado.Add(beE);

                }





            }
            return JSONUtils.serializeToJSON(lstEstado);
        }




        public static void crear(List<BEEstados> lista)
        {
            try
            {
                EstadoModel.borrar("");
                for (int i = 0; i < lista.Count; i++)
                {
                    BEEstados obj = lista[i];
                    for (int j = 0; j < obj.controles.Count; j++)
                    {
                        BEControl objco = obj.controles[j];                        
                        EstadoModel.addCONTROLES(obj.codigo, objco.tipo, objco.max, objco.etiqueta, objco.obligatorio, (j + 1), objco.grupo,objco.editable);
                    }
                }
                EstadoModel.delTableTemp();
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);


            }

        }

        public static void crearGrupo(GrupoBean bean)
        {
            try
            {



                DataTable dt = EstadoModel.addGRUPO(bean.codigo, bean.nombre);
                String grupo = dt.Rows[0][0].ToString();

                int i = 1;
                foreach (OpcionBean be in bean.opciones)
                {
                    EstadoModel.addOPCIONES(grupo, be.codigo, be.nombre);
                    i++;
                }
            }

            catch (Exception ex)
            {


                throw new Exception(ex.Message);


            }



        }


       public static PaginateEstadoBean findEstadoBean(String CODIGO, String NOMBRE, String FLAG, String CODIGOTIPOACTIVIDAD, String PAGINAACTUAL, String REGISTROPORPAGINA)
       {

           PaginateEstadoBean resultado = new PaginateEstadoBean();
           List<BeTipoNew> lista = new List<BeTipoNew>();
           DataSet dst = EstadoModel.findEstadoBean(CODIGO, NOMBRE, FLAG, CODIGOTIPOACTIVIDAD, PAGINAACTUAL, REGISTROPORPAGINA);

           int totalRegistros = 0;


           if (dst.Tables.Count > 0)
           {
               foreach (DataRow dr in dst.Tables[0].Rows)
               {
                   BeTipoNew BE = new BeTipoNew();

                   BE.id = dr["ID"].ToString();
                   BE.codigo = dr["CODIGO"].ToString();
                   BE.nombre = dr["NOMBRE"].ToString();

                   totalRegistros = Int32.Parse(dr["TAMANIOTOTAL"].ToString());

                   List<BeEstadoNew> listD = new List<BeEstadoNew>();
                   foreach (DataRow drE in dst.Tables[1].Rows)
                   {
                       if (dr["CODIGO"].ToString() == drE["CodTipActividad"].ToString())
                       {
                           BeEstadoNew BEE = new BeEstadoNew();
                           BEE.id = drE["IdEstado"].ToString();
                           BEE.codigo = drE["CodEstado"].ToString();
                           BEE.nombre = drE["Nombre"].ToString();
                           BEE.flag = drE["FlgRegHabilitado"].ToString();
                           BEE.orden = drE["orden"].ToString();
                           listD.Add(BEE);
                       }


                   } BE.lista = listD;
                   lista.Add(BE);
               }

               resultado.lstResultados = lista;
               resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(REGISTROPORPAGINA));
           }
           return resultado;
       }

       public static void ordenEstadoBean(String CODIGO, String ORDEN)
       {
           try
           {
               EstadoModel.ordenEstadoBean(CODIGO, ORDEN);
           }

           catch (Exception ex)
           {

               throw new Exception(ex.Message);

           }

       }

       public static DataTable findEstados(String TIPACTIVIDAD)
       {
           return EstadoModel.findEstados(TIPACTIVIDAD);
       }

       public static DataTable findCONTROLESG(String estado)
       {
           return EstadoModel.findCONTROLESG(estado);
       }

       public static DataTable findGRUPOS()
       {
           return EstadoModel.findGRUPOS();
       }

       public static DataTable findOPCIONES(String GRUPO)
       {
           return EstadoModel.findOPCIONES(GRUPO);
       }

       public static EstadoBean getEstadoBeanPK(String id)
       {
           DataTable dt = EstadoModel.getEstadoBeanPK(id);
           EstadoBean bean = new EstadoBean();

           if (dt != null && dt.Rows.Count > 0)
           {
               DataRow row = dt.Rows[0];
               bean.id = row["IdEstado"].ToString();
               bean.codigo = row["CodEstado"].ToString();
               bean.nombre = row["Nombre"].ToString();
               bean.orden =Convert.ToInt32(row["Orden"].ToString());
               bean.codigotipoactividad = row["CodTipActividad"].ToString();
               
           }
           return bean;
       }

       public static void borrar(String codigo)
       {
           EstadoModel.borrar(codigo);

       }


       public static void borrarGrupo(String codigo)
       {
           EstadoModel.borrarGrupo(codigo);

       }


       public static void crear(ClienteBean bean)
       {
           try
           {
               Convert.ToString(ClienteModel.crear(bean));
           }

           catch (Exception ex)
           {
               if (IdiomaCultura.WEB_CODIGOREPETIDO.Equals(ex.Message))
               {
                   throw new Exception(IdiomaCultura.getMensaje(ex.Message, bean.codigo));
               }
               else
               {
                   throw new Exception(ex.Message);
               }

           }

       }

    }
}
