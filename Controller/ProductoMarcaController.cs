﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
namespace Controller
{
   public class ProductoMarcaController
    {
        public static List<MarcaProductoBean> fnlistaMarcaProducto()
        {
            DataTable loDataTable = MarcaProductoModel.fnlistaMarcaProducto();

            List<MarcaProductoBean> lstMarcaProducto = new List<MarcaProductoBean>();
            if (loDataTable != null && loDataTable.Rows.Count > 0)
            {
                foreach (DataRow loRow in loDataTable.Rows)
                {
                    MarcaProductoBean lofamiliaProductoBean = new MarcaProductoBean
                    {
                        id = (loRow["Id"].ToString()),
                        codigo = loRow["Codigo"].ToString(),
                        nombre = loRow["Nombre"].ToString().Trim()
                    };
                    lstMarcaProducto.Add(lofamiliaProductoBean);
                }

            }
            return lstMarcaProducto;
        }
    }
}

