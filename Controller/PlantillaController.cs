﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;

namespace Controller
{
   public class PlantillaController
    {


       public static List<PlantillaBean> findPlantillaBean()
       {
           List<PlantillaBean> lista = new List<PlantillaBean>();
           DataTable dt = PlantillaModel.findPlantillaBean();

           foreach (DataRow dr in dt.Rows)
           {
               PlantillaBean bean = new PlantillaBean();

               bean.codigo = dr["IdPlantilla"].ToString().Trim();
               bean.nombre = dr["Descripcion"].ToString().Trim();
               bean.flag = dr["FlgRegHabilitado"].ToString().Trim();
               bean.id = dr["IdPlantilla"].ToString().Trim();

               lista.Add(bean);

           }

           return lista;
       }

       public static List<PlantillaBean> findFiltros(String id, String tipo)
       {
           List<PlantillaBean> lista = new List<PlantillaBean>();
           DataTable dt = PlantillaModel.findFiltros(id,tipo);

           foreach (DataRow dr in dt.Rows)
           {
               PlantillaBean bean = new PlantillaBean();

               bean.codigo = dr["IdFiltro"].ToString().Trim();
               bean.nombre = dr["Descripcion"].ToString().Trim();
               bean.flag = dr["CHECKED"].ToString().Trim();
               bean.id = dr["IdFiltro"].ToString().Trim();

               lista.Add(bean);

           }

           return lista;
       }


       public static PlantillaBean info(String id)
       {
           DataTable dt = PlantillaModel.info(id);
           PlantillaBean bean = new PlantillaBean();

           if (dt != null && dt.Rows.Count > 0)
           {
               DataRow row = dt.Rows[0];
               bean.id = row["IdPlantilla"].ToString();
               bean.codigo = row["IdPlantilla"].ToString();
               bean.flag = row["FlgRegHabilitado"].ToString();
               bean.nombre = row["Descripcion"].ToString();
           }
           return bean;
       }

       public static void crear(String CODIGO, String  NOMBRE, String LISTA )
       {
           try
           {
               Convert.ToString(PlantillaModel.crear(CODIGO,NOMBRE,LISTA));
           }

           catch (Exception ex)
           {
                throw new Exception(ex.Message);        
           }

       }

       public static void borrar(String id)
       {
           PlantillaModel.borrar(id, "F");
       }

    }
}
