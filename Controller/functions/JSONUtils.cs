﻿using Newtonsoft.Json;
using System;

namespace Controller.functions
{
    public class JSONUtils
    {
        public static String serializeToJSON(Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
