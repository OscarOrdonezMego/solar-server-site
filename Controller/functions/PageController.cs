﻿using System;
using System.Collections.Generic;
using Model.bean;
using Model;
using System.Web.UI;

public abstract class PageController : Page
{
    abstract protected void initialize();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (validateSession() && validateIdioma())
        {
            initialize();
        }
        else
        {
            string myScript = "parent.document.location.href = '" + Request.ApplicationPath + "/default.aspx?acc=EXT';";
            // string myScript = "parent.document.location.href = '/default.aspx?acc=EXT';";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
        }

    }

    private bool validateSession()
    {
        return (Session[SessionManager.USER_SESSION] == null) ? false : true;
    }

    private bool validateIdioma()
    {
        return (IdiomaCultura.datos.Count > 0) ? true : false;
    }

    public Boolean fnValidarPerfilMenu(String psCodMenu, Enumerados.FlagPermisoPerfil pePermiso)
    {
        Boolean lbRetorno = false;
        try
        {
            switch (pePermiso)
            {
                case Enumerados.FlagPermisoPerfil.VER:
                    lbRetorno = ((Dictionary<String, RolBean>)Session["lgn_perfilmenu"])[psCodMenu].FlgVer.Equals(Enumerados.FlagHabilitado.T.ToString());
                    break;
                case Enumerados.FlagPermisoPerfil.EDITAR:
                    lbRetorno = ((Dictionary<String, RolBean>)Session["lgn_perfilmenu"])[psCodMenu].FlgEditar.Equals(Enumerados.FlagHabilitado.T.ToString());
                    break;
                case Enumerados.FlagPermisoPerfil.CREAR:
                    lbRetorno = ((Dictionary<String, RolBean>)Session["lgn_perfilmenu"])[psCodMenu].FlgCrear.Equals(Enumerados.FlagHabilitado.T.ToString());
                    break;
                case Enumerados.FlagPermisoPerfil.ELIMINAR:
                    lbRetorno = ((Dictionary<String, RolBean>)Session["lgn_perfilmenu"])[psCodMenu].FlgEliminar.Equals(Enumerados.FlagHabilitado.T.ToString());
                    break;
                case Enumerados.FlagPermisoPerfil.EXPORTAR:
                    lbRetorno = ((Dictionary<String, RolBean>)Session["lgn_perfilmenu"])[psCodMenu].FlgExportar.Equals(Enumerados.FlagHabilitado.T.ToString());
                    break;
            }
        }
        catch (Exception)
        {
            lbRetorno = false;
        }
        return lbRetorno;
    }

    public ConfiguracionBean fnObtenerConfiguracionPorCodigo(String psCodigo)
    {
        ConfiguracionBean loConfiguracioBean = new ConfiguracionBean();
        try
        {
            loConfiguracioBean = (ConfiguracionBean)ManagerConfiguration.HashConfig[psCodigo];

        }
        catch (Exception e)
        {

        }
        return loConfiguracioBean;
    }
}