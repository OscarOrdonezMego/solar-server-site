﻿using System;
using System.Text;
using Model.bean;
using System.IO;
using System.Web;
using System.Configuration;
using System.Web.Security;

public abstract class PageControllerNtrack : System.Web.UI.Page
{
    abstract protected void initialize();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!fnValidarAccesoRemoto())
        {
            if (validateSession() && validateIdioma())
            {
                initialize();
            }
            else
            {
                string myScript = "parent.document.location.href = '" + Request.ApplicationPath + "/default.aspx?acc=EXT';";
                //string myScript = "parent.document.location.href = 'default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }
        else
        {
            subAccesoRemoto();
        }

    }

    private bool validateSession()
    {
        return (Session[SessionManager.USER_SESSION] == null) ? false : true;
    }

    private bool validateIdioma()
    {
        return (IdiomaCultura.datos.Count > 0) ? true : false;
    }

    private Boolean fnValidarAccesoRemoto()
    {
        if (Request.UrlReferrer != null && ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"] != null)
        {
            string[] arrSegmentosOrigen = Request.UrlReferrer.AbsoluteUri.Split('/');
            StringBuilder segmentos = new StringBuilder();
            for (int i = 0; i < arrSegmentosOrigen.Length - 1; i++)
            {
                segmentos.Append(arrSegmentosOrigen[i]);
            }
            string rutaOrigen = segmentos.ToString();
            string[] arrSegmentosPadre = ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"].Split('/');
            StringBuilder segmentosPadre = new StringBuilder();
            for (int i = 0; i < arrSegmentosPadre.Length - 1; i++)
            {
                segmentosPadre.Append(arrSegmentosPadre[i]);
            }
            return rutaOrigen.ToString().Equals(segmentosPadre.ToString());
        }
        else
        {
            return false;
        }
    }

    private void subAccesoRemoto()
    {
        StreamReader stream = new StreamReader(Request.InputStream);
        string entrada = stream.ReadToEnd();
        if (!entrada.Equals(""))
        {
            entrada = entrada.Substring(2);
            string[] usuarioPassword = HttpUtility.UrlDecode(entrada).Split(new char[] { '|', '@', '|' });
            String pwcrypt = FormsAuthentication.HashPasswordForStoringInConfigFile(usuarioPassword[1], "sha1");
            if (subLogin(usuarioPassword[0], pwcrypt))
            {

                HttpCookie loCookieUsr = new HttpCookie("usr");
                loCookieUsr.Value = usuarioPassword[0];

                HttpCookie loCookiePsw = new HttpCookie("psw");
                loCookiePsw.Value = usuarioPassword[1];

                Response.Cookies.Add(loCookieUsr);
                Response.Cookies.Add(loCookiePsw);

                //Response.Cookies["usr"].Value = usuarioPassword[0];
                //Response.Cookies["psw"].Value = usuarioPassword[1];

                initialize();
            }
            else
            {
                string myScript = "parent.parent.document.location.href = '" + Request.ApplicationPath + "/default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

    }


    public virtual bool subLogin(String usuario, String password)
    {
        return false;
    }

    public ConfiguracionBean fnObtenerConfiguracionPorCodigo(String psCodigo)
    {
        ConfiguracionBean loConfiguracioBean = new ConfiguracionBean();
        try
        {
            loConfiguracioBean = (ConfiguracionBean)ManagerConfiguration.HashConfig[psCodigo];
        }
        catch (Exception e)
        {

        }
        return loConfiguracioBean;
    }
}