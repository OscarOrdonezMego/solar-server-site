﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Linq;

namespace business.functions
{
    public interface IGeoLocation
    {   double latitude { get; set; }
        double longitude { get; set; }
        string geocodeurl { get; set; }
    }

    public struct GeoLocation : IGeoLocation
    {   private double _latitude;
        private double _longitude;
        private string _geocodeurl;

        public GeoLocation(double latitude, double longitude, string geocodeurl)
        {   _latitude = latitude;
            _longitude = longitude;
            _geocodeurl = geocodeurl;
        }

        public double latitude
        {   get { return _latitude; }
            set { _latitude = value; }
        }

        public double longitude
        {   get { return _longitude; }
            set { _longitude = value; }
        }

        public string geocodeurl
        {   get { return _geocodeurl; }
            set { _geocodeurl = value; }
        }
    }

    public class GPSGeoCode
    {
        const string _googleUri = "http://maps.googleapis.com/maps/api/geocode/xml?address=";
        static string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false";
        
        private static Uri GetGeoCodeURI(string address)
        {
            address = HttpUtility.UrlEncode(address);
            string uri = String.Format("{0}{1}&sensor=false", _googleUri, address);

            return new Uri(uri);
        }

        public static GeoLocation GetCoordinates(string address)
        {
            WebClient wc = new WebClient();
            Uri uri = GetGeoCodeURI(address);

            try
            {
                string geoCodeInfo = wc.DownloadString(uri);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(geoCodeInfo);

                string status = xmlDoc.DocumentElement.SelectSingleNode("status").InnerText;
                double geolat = 0.0;
                double geolong = 0.0;
                XmlNodeList nodeCol = xmlDoc.DocumentElement.SelectNodes("result");
                foreach (XmlNode node in nodeCol)
                {
                    geolat = Convert.ToDouble(node.SelectSingleNode("geometry/location/lat").InnerText, System.Globalization.CultureInfo.InvariantCulture);
                    geolong = Convert.ToDouble(node.SelectSingleNode("geometry/location/lng").InnerText, System.Globalization.CultureInfo.InvariantCulture);
                }
                return new GeoLocation(geolat, geolong, uri.ToString());
            }
            catch
            {
                return new GeoLocation(0.0, 0.0, "http://");
            }
        }




        public static String GetFormatedAddress(string lat, string lng)
        {
            string requestUri = string.Format(baseUri, lat, lng);

            using (WebClient wc = new WebClient())
            {
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where elm.Name == "status"
                              select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    var res = (from elm in xmlElm.Descendants()
                               where
                                   elm.Name == "formatted_address"
                               select elm).FirstOrDefault();
                    requestUri = res.Value;
                }
            }

            return requestUri;
        }

    }

}