﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Web;

namespace Controller.functions
{
    public class TextFileUtils
    {

        public static void ExportToTextFile(DataTable dt, string fileName)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();

            foreach (DataRow row in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                { context.Response.Write(row[i].ToString() + ","); }
                context.Response.Write(Environment.NewLine);
            }

            DateTime date = DateTime.Now;
            context.Response.ContentType = "text/csv";
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + "_" + date.Date.ToString("yyyyMMdd") + "_" + date.Hour + date.Minute + date.Millisecond + ".txt");
            context.Response.End();
        }

        public static void createTextFile(DataTable dt, String path, String fileName)
        {
            StringBuilder sb = new StringBuilder();
            if (dt.Columns.Count != 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    String fila = String.Empty;
                    foreach (DataColumn column in dt.Columns)
                    {
                        fila += row[column].ToString() + '|';
                        //sb.Append(row[column].ToString() + '|');
                    } //antes estaba la coma ','

                    int contPipes = fila.Count(c => c=='|');

                    if (contPipes >= dt.Columns.Count) {

                        fila = fila.TrimEnd('|');
                    }

                    sb.Append(fila);
                    sb.Append("\r\n");
                }
            }

            String fileLocation = path + "/" + fileName + ".txt";
            System.IO.File.WriteAllText(fileLocation, sb.ToString());

        }

        public static void appendToTextFile(DataTable dt, String path, String fileName)
        {
            StringBuilder sb = new StringBuilder();
            if (dt.Columns.Count != 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    String fila = String.Empty;
                    foreach (DataColumn column in dt.Columns)
                    {
                        fila += row[column].ToString() + '|';
                        //sb.Append(row[column].ToString() + '|');
                    } //antes estaba la coma ','

                    int contPipes = fila.Count(c => c == '|');

                    if (contPipes >= dt.Columns.Count)
                    {
                        fila = fila.TrimEnd('|');
                    }

                    sb.Append(fila);
                    sb.Append("\r\n");
                }
            }

            String fileLocation = path + "/" + fileName + ".txt";

            if (File.Exists(fileLocation))
            {
                //using (var file = new StreamWriter(File.OpenWrite(fileLocation)))
                using (StreamWriter sw = File.AppendText(fileLocation))
                {
                    //file.Write("\n" + sb.ToString());
                    sw.Write(sb.ToString());
                }
            }
            else
            {
                System.IO.File.WriteAllText(fileLocation, sb.ToString());
            }
        }

    }
}
