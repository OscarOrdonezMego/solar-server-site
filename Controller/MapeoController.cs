﻿using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;

namespace Controller
{
    public class MapeoController
    {
        public static List<MapeoBean> ListarMaestroTabla()
        {
            DataTable dt = MapeoModel.ListarMaestroTabla();
            List<MapeoBean> result = new List<MapeoBean>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    MapeoBean bean = new MapeoBean();
                    bean.id = Int64.Parse(row["id"].ToString());
                    bean.tabla = row["tabla"].ToString();
                    bean.descripcion = row["descripcion"].ToString();

                    result.Add(bean);
                }
            }
            return result;
        }

        public static List<MapeoBean> ListarMaestroTablaColumnas(Int64 idMaestro)
        {
            DataTable dt = MapeoModel.ListarMaestroTablaColumnas(idMaestro);
            List<MapeoBean> result = new List<MapeoBean>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    MapeoBean bean = new MapeoBean();
                    bean.id = Int64.Parse(row["id"].ToString());
                    bean.columna = row["columna"].ToString();
                    bean.descripcion = row["descripcion"].ToString();
                    bean.formato = row["formato"].ToString();
                    bean.obligatorio = row["obligatorio"].ToString();

                    result.Add(bean);
                }
            }
            return result;
        }

        public static MapeoBean ListarMaestroTablaColumnasId(Int64 idMaestro)
        {
            DataTable dt = MapeoModel.ListarMaestroTablaColumnasId(idMaestro);
            MapeoBean result = new MapeoBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    result = new MapeoBean();
                    result.id = Int64.Parse(row["id"].ToString());
                    result.columna = row["columna"].ToString();
                    result.descripcion = row["descripcion"].ToString();
                    result.formato = row["formato"].ToString();
                    result.obligatorio = row["obligatorio"].ToString();

                }
            }
            return result;
        }

        public static void GuardarMaestroTabla(Int64 idTabla, List<columnasMap> columna)
        {
            try
            {
                MapeoModel.GuardarMaestroTabla(idTabla, columna);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al realizar el registro");
            }
        }

        public static List<MapeoBean> ListarMaestrosMapeados()
        {
            DataTable dt = MapeoModel.ListarMaestrosMapeados();
            List<MapeoBean> result = new List<MapeoBean>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    MapeoBean bean = new MapeoBean();
                    bean.tabla = row["tabla"].ToString();
                    bean.tipoFormato = row["tipoFormato"].ToString();

                    result.Add(bean);
                }
            }
            return result;
        }

        public static List<MapeoBean> MapeoMaestroTablaTablaTipo(String tabla, String tipoFormato)
        {
            DataTable dt = MapeoModel.MapeoMaestroTablaTablaTipo(tabla, tipoFormato);
            List<MapeoBean> result = new List<MapeoBean>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    MapeoBean bean = new MapeoBean();
                    bean.tabla = row["tabla"].ToString();
                    bean.columna = row["columna"].ToString();
                    bean.descripcion = row["descripcion"].ToString();
                    bean.columnaXls = row["columnaXls"].ToString();
                    bean.columnaTabla = row["columnaTabla"].ToString();
                    bean.tipoFormato = row["tipoFormato"].ToString();
                    bean.tablaTemp = row["tablaTemp"].ToString();
                    bean.spcargaTabla = row["spcargaTabla"].ToString();

                    result.Add(bean);
                }
            }
            return result;
        }

        public static void LimpiarTabla(String tabla)
        {
            try
            {
                MapeoModel.LimpiarTabla(tabla);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al limpiar la tabla");
            }
        }

        public static void CargarTabla(String tablaDestino, List<MapeoBean> MapeoTabla, DataTable Datos)
        {
            try
            {
                MapeoModel.CargarTabla(tablaDestino, MapeoTabla, Datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Error tabla");
            }
        }

        public static FileCargaBean EjecutarCargaTabla(String sp)
        {
            try
            {
                DataTable dt = MapeoModel.EjecutarCargaTabla(sp);
                FileCargaBean res = new FileCargaBean();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        res.subidos = Int32.Parse(row["Subidos"].ToString());
                        res.actualizados = Int32.Parse(row["Actualizados"].ToString());
                        res.insertados = Int32.Parse(row["Insertados"].ToString());
                        res.errorData = row["Error"].ToString();
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                throw new Exception("Error ejecutar migracion");
            }
        }
    }

    public class ResutCarga
    {
        public String Subidos { get; set; }
        public String Actualizados { get; set; }
        public String Insertados { get; set; }
        public String Error { get; set; }
    }
}
