﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using business.functions;

///<summary>
///@001 GMC 15/04/2015 Se muestra Descuento Minimo y Maximo
///@002 GMC 05/05/2015 Búsqueda de Producto por Código o Nombre
///</summary>

namespace Controller
{
    public class ProductoFamiliaController
    {
        public static PaginateFamiliaProductoBean paginarBuscar(String codigo, String nombre, String flag, String page, String rows)
        {
            PaginateFamiliaProductoBean resultado = new PaginateFamiliaProductoBean();
            List<FamiliaProductoBean> list = new List<FamiliaProductoBean>();

            FamiliaProductoBean bean;

            DataTable dt = FamiliaProductoModel.findFamiliaProducto(codigo, nombre, flag, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new FamiliaProductoBean();
                    bean.id = row["FP_PK"].ToString();
                    bean.codigo = row["FP_CODIGO"].ToString();
                    bean.nombre = row["FP_NOMBRE"].ToString();
                    bean.TotalProductos = row["TOTALPRODUCTOS"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }

        public static FamiliaProductoBean info(String id)
        {
            DataTable dt = FamiliaProductoModel.info(id);
            FamiliaProductoBean bean = new FamiliaProductoBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["fP_PK"].ToString();
                bean.codigo = row["FP_CODIGO"].ToString();
                bean.nombre = row["FP_nombre"].ToString();
                bean.flag = row["FLGENABLE"].ToString();

            }
            return bean;
        }
        public static void crear(FamiliaProductoBean bean)
        {
            try
            {
                Convert.ToString(FamiliaProductoModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public static void borrar(String id, String flag)
        {
            FamiliaProductoModel.borrar(id, flag);
        }

   
    public static List<FamiliaProductoBean> fnListarFamiliaProductos()
    {
        DataTable loDataTable = FamiliaProductoModel.fnlistaFamiliaProducto();

        List<FamiliaProductoBean> lstFamiliaProducto = new List<FamiliaProductoBean>();
        if (loDataTable != null && loDataTable.Rows.Count > 0)
        {
            foreach (DataRow loRow in loDataTable.Rows)
            {
                FamiliaProductoBean lofamiliaProductoBean = new FamiliaProductoBean
                {
                    id = (loRow["Id"].ToString()),
                    codigo = loRow["Codigo"].ToString(),
                    nombre = loRow["Nombre"].ToString().Trim()
                };
                    lstFamiliaProducto.Add(lofamiliaProductoBean);
            }

        }
        return lstFamiliaProducto;
    }
    }
}