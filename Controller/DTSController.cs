﻿using Model;
using Model.bean;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Security;

///<summary>
///@001 GMC 05/05/2015 Carga de archivos Almacen y Almacén Producto
///</summary>
namespace Controller
{
    public class DTSController
    {
        public static String fileProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRODUCTO);
        public static String fileCliente = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_CLIENTE);
        public static String fileDireccion = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_DIRECCION);
        public static String fileRuta = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_RUTA);
        public static String filefamilia = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA);
        public static String filemarca = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA);
        public static String filefamiliaProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA_PRODUCTO);
        public static String fileMarcaProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA_PRODUCTO);
        public static String fileUsuario = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_VENDEDOR);
        public static String fileGeneral = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_GENERAL);
        public static String fileCobranza = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_COBRANZA);
        public static String fileListaPrecios = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_LISTAPRECIOS);
        //@001 I
        public static String fileAlmacen = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACEN);
        public static String fileAlmacenProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACENPRODUCTO);
        //@001 F
        public static String filePresentacion = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRESENTACION);

        public const int STR_SIN_ERROR = 0; // sin error validacion Carga y sin errores en archivo
        public const int STR_ERROR_ARCHIVO = 1; // sin error validacion Carga y con errores en archivo
        public const int STR_ERROR_VALIDAR_CARGA = 2; // con error validacion Carga
        public static int MAX_LON_CARGA = 302; // numero que define la cantidad maxima que se puede definir en una linea del archivo de carga
        public static int MAX_LON_CLAVE_USUARIO = 20; // numero que define la cantidad maxima para la clave del usuario

        public static List<FileCargaBean> ejecutarArchivoDTS(String fileLocation, String DTSLocation, String tipo)
        {
            List<FileCargaBean> lista = new List<FileCargaBean>();

            if (IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_VENDEDOR).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRODUCTO).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_RUTA).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_CLIENTE).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_DIRECCION).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_COBRANZA).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_GENERAL).ToUpper().Equals("")
           || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_LISTAPRECIOS).ToUpper().Equals("")
                //@001 I
                || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACEN).ToUpper().Equals("")
                || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACENPRODUCTO).ToUpper().Equals("")
                 //@001 F
                 || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRESENTACION).ToUpper().Equals("")
                  || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA).ToUpper().Equals("")
                    || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA).ToUpper().Equals("")
                  || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA_PRODUCTO).ToUpper().Equals("")
                     || IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA_PRODUCTO).ToUpper().Equals("")
                )
            {
                throw new Exception("EXPIRED SESSION");
            }

            if (tipo == "TXT")
            {
                lista = cargaDTS_TXT(fileLocation, DTSLocation);
            }
            else
            {
                lista = cargaDTS_XLS(fileLocation, DTSLocation);
            }

            deleteDataFiles(fileLocation);

            return lista;
        }

        public static List<FileCargaBean> cargaDTS_XLS_BC(String filesLocation, List<archivoExcelTablas> cargaTabla)
        {
            /*
            1.verificar archivos a subir
            2.Obtener Mapeo de Tablas
            3.Validar encabezado obligorio
            4.crear Funcion que genere mapeo y carga
            */
            List<FileCargaBean> cargados = new List<FileCargaBean>();
            try
            {
                CargaModel.setConfiguracionCarga("X"); // Texto
                //List<archivoExcelTablas> cargaTabla = CargaExcelController.GetArchivosTablas(filesLocation);
                if (cargaTabla != null && cargaTabla.Count > 0)
                {
                    foreach (archivoExcelTablas item in cargaTabla)
                    {
                        foreach (archivoTabla tabla in item.tablas)
                        {
                            FileCargaBean res = new FileCargaBean();
                            try
                            {
                                String formato = tabla.formatoCliente == "T" ? "C" : (tabla.formatoEntel == "T" ? "E" : "");
                                List<MapeoBean> MapeoTabla = MapeoController.MapeoMaestroTablaTablaTipo(tabla.tabla, formato);
                                if (MapeoTabla == null || MapeoTabla.Count <= 0)
                                {
                                    throw new Exception("No se encontro tabla mapeada");
                                }
                                String columnas = "";
                                String consulta = "";
                                MapeoTabla.ForEach(x => columnas += "[" + x.columnaXls + "],");
                                //MapeoTabla.ForEach(x => consulta += "and [" + x.columnaXls + "]!=\"\"");

                                columnas = columnas.Substring(0, columnas.Length - 1);
                                /*Limpiar Temporal*/
                                MapeoController.LimpiarTabla(MapeoTabla[0].tablaTemp);

                                /*Obtener Data de Excel*/
                                DataTable tablaXls = CargaExcelController.GetExcelData(item.archivo, tabla.tabla, columnas, consulta);

                                /*mapear datos y guardarlos*/
                                MapeoController.CargarTabla(MapeoTabla[0].tablaTemp, MapeoTabla, tablaXls);

                                /*Guardarlos en Maestro*/
                                res = MapeoController.EjecutarCargaTabla(MapeoTabla[0].spcargaTabla);

                            }
                            catch (Exception ex)
                            {
                                res.errorExecute = ex.Message;
                            }
                            res.archivo = tabla.tabla;
                            cargados.Add(res);
                        }
                    }
                }
                else
                {
                    throw new Exception("No se encontraron Tablas a cargar");
                }
            }
            catch (Exception)
            {

            }

            deleteDataFiles(filesLocation);
            return cargados;
        }

        private static List<FileCargaBean> cargaDTS_XLS(String filesLocation, String DTSLocation)
        {
            fileUsuario = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_VENDEDOR).ToUpper();
            fileProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRODUCTO).ToUpper();
            fileRuta = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_RUTA).ToUpper();
            fileCliente = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_CLIENTE).ToUpper();
            fileDireccion = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_DIRECCION).ToUpper();
            fileCobranza = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_COBRANZA).ToUpper();
            fileGeneral = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_GENERAL).ToUpper();
            fileListaPrecios = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_LISTAPRECIOS).ToUpper();
            //@001 I
            fileAlmacen = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACEN).ToUpper();
            fileAlmacenProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_ALMACENPRODUCTO).ToUpper();
            //@001 F
            filePresentacion = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_PRESENTACION);
            filefamilia = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA);
            filefamiliaProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_FAMILIA_PRODUCTO);
            filemarca = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA);
            fileMarcaProducto = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FILENAME_MARCA_PRODUCTO);
            List<String> arrArchivosCargados = new List<String>();
            String[] extensions;
            /*  if (ConfigurationManager.AppSettings["DTSX"].Trim() == "1")*/
            extensions = new String[] { "*.xls", "*.xlsx" };
            /*  else
                  extensions = new String[] { "*.xls" };*/

            CargaModel.setConfiguracionCarga("E"); // Texto

            foreach (String extension in extensions)
            {
                String[] filesArr = Directory.GetFiles(filesLocation, extension, SearchOption.TopDirectoryOnly);
                foreach (String file in filesArr)
                    arrArchivosCargados.Add(file);
            }

            List<FileCargaBean> listaArchivos = new List<FileCargaBean>();
            FileCargaBean FileBean;
            //@001 I
            //string[] arrArchivos = { fileUsuario, fileProducto, fileRuta, fileCliente, fileDireccion, fileCobranza, fileGeneral, fileListaPrecios };
            ArrayList arrArchivos = new ArrayList();

            arrArchivos.Add(fileUsuario);
            arrArchivos.Add(fileProducto);
            arrArchivos.Add(fileRuta);
            arrArchivos.Add(fileCliente);
            arrArchivos.Add(fileDireccion);
            arrArchivos.Add(fileCobranza);
            arrArchivos.Add(fileGeneral);
            arrArchivos.Add(filefamilia);
            arrArchivos.Add(filefamiliaProducto);
            arrArchivos.Add(filemarca);
            arrArchivos.Add(fileMarcaProducto);
            ConfiguracionBean TPAL = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPAL"];
            ConfiguracionBean TPFM = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFM"];
            if (TPFM.Valor == "T")
            {
                arrArchivos.Add(filePresentacion);
                //, , fileAlmacenProducto };
            }
            if (TPAL.Valor == "T")
            {
                arrArchivos.Add(fileAlmacen);
                arrArchivos.Add(fileAlmacenProducto);
                //, fileListaPrecios, fileAlmacenProducto };
            }

            //@001 F
            arrArchivos.Add(fileListaPrecios);
            if (arrArchivosCargados != null && arrArchivosCargados.Count > 0)
            {
                string archivoExcel = arrArchivosCargados[0];
                String txtFileName = "";
                String dtsName = "";
                String tmpTable = "";
                foreach (string arch in arrArchivos)
                {
                    FileBean = new FileCargaBean();
                    txtFileName = TraducirNombreArchivo(arch);
                    if (TraducirNombreArchivo(arch) == "USUARIO")
                    {

                    }
                    FileBean.archivo = arch.ToUpper();

                    dtsName = "PKG_" + TraducirNombreArchivo(arch) + "_EXCEL";
                    tmpTable = "TEMP_" + TraducirNombreArchivo(arch);// arch;

                    String dtsFilePath = DTSLocation + dtsName;

                    if (TraducirNombreArchivo(arch) == "USUARIO")
                    {
                        FileBean = DTSModel.executeDTSX_XLS(dtsFilePath, dtsName, archivoExcel, txtFileName, tmpTable, "VENDEDOR");
                    }
                    else
                    {
                        FileBean = DTSModel.executeDTSX_XLS(dtsFilePath, dtsName, archivoExcel, txtFileName, tmpTable, txtFileName);
                    }
                    listaArchivos.Add(FileBean);
                }
            }

            return listaArchivos;
        }

        private static List<FileCargaBean> cargaDTS_TXT(String filesLocation, String DTSLocation)
        {
            List<FileCargaBean> listaArchivos = new List<FileCargaBean>();
            FileCargaBean archivo;
            String txtFileName = "";
            String[] namesFiles = Directory.GetFiles(filesLocation, "*.txt");

            String fileNameVendedor = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_VENDEDOR);
            String fileNameProducto = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_PRODUCTO);
            String fileNameCliente = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_CLIENTE);
            String fileNameDireccion = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_DIRECCION);
            String fileNameRuta = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_RUTA);
            String fileNameGeneral = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_GENERAL);
            String fileNameCobranza = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_COBRANZA);
            String fileNameListaPrecio = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_LISTAPRECIOS);

            //@001 I
            String fileNameAlmacen = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_ALMACEN);
            String fileNameAlmacenProducto = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_ALMACENPRODUCTO);
            //@001 F
            String fileNamePresentacion = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_PRESENTACION);
            String fileNameFamilia = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_FAMILIA);
            String fileNameMarca = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_MARCA);
            String fileNameFamiliaProducto = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_FAMILIA_PRODUCTO);
            String fileNameMarcaProducto = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FILENAME_MARCA_PRODUCTO);
            CargaModel.setConfiguracionCarga("T");

            ConfiguracionBean TPAL = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPAL"];
            ConfiguracionBean TPFM = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFM"];
            ConfiguracionBean TPFS = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFS"];

            ConfiguracionBean DEHP = (ConfiguracionBean)ManagerConfiguracion.HashConfig["DEHP"];

            for (int i = 0; i < namesFiles.Length; i++)
            {
                string txtFilePath = namesFiles[i].ToUpper();
                txtFileName = Path.GetFileNameWithoutExtension(txtFilePath.ToUpper());

                archivo = null;

                CargaModel.limpiaTemporalesCarga(txtFileName);
                if (txtFileName == fileNameProducto.ToUpper())
                {
                    if (DEHP.Valor == "T")
                    {
                        archivo = ejecutarCarga_Archivo(fileNameProducto, "TMPPRODUCTO", filesLocation, DTSLocation, 8, txtFileName, "PKG_PRODUCTO", "TEMP_PRODUCTO");
                    }
                    else
                    {
                        archivo = ejecutarCarga_Archivo(fileNameProducto, "TMPPRODUCTO", filesLocation, DTSLocation, 4, txtFileName, "PKG_PRODUCTO_GENERAL", "TEMP_PRODUCTO");
                    }
                }
                else if (txtFileName == fileNameListaPrecio.ToUpper())
                {
                    if (DEHP.Valor == "T")
                    {
                        archivo = ejecutarCarga_Archivo(fileNameListaPrecio, "TMPLISTAPRECIOS", filesLocation, DTSLocation, 6, txtFileName, "PKG_PRECIO_PRODUCTO", "TEMP_LISTAPRECIOS");
                    }
                    else
                    {
                        archivo = ejecutarCarga_Archivo(fileNameListaPrecio, "TMPLISTAPRECIOS", filesLocation, DTSLocation, 4, txtFileName, "PKG_PRECIO_PRODUCTO_GENERAL", "TEMP_LISTAPRECIOS");
                    }
                }
                else if (txtFileName == fileNameCliente.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameCliente, "TMPCLIENTE", filesLocation, DTSLocation, 14, txtFileName, "PKG_CLIENTE", "TEMP_CLIENTE");
                }
                else if (txtFileName == fileNameDireccion.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameDireccion, "TMPDIRECCION", filesLocation, DTSLocation, 6, txtFileName, "PKG_DIRECCION", "TEMP_DIRECCION");
                }
                else if (txtFileName == fileNameFamilia.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameFamilia, "TMPFAMILIA", filesLocation, DTSLocation, 2, txtFileName, "PKG_FAMILIA", "TEMP_FAMILIA");
                }
                else if (txtFileName == fileNameMarca.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameMarca, "TMPMARCA", filesLocation, DTSLocation, 2, txtFileName, "PKG_MARCA", "TEMP_MARCA");
                }
                else if (txtFileName == fileNameFamiliaProducto.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameFamiliaProducto, "TMPFAMILIAPRODUCTO", filesLocation, DTSLocation, 2, txtFileName, "PKG_FAMILIA_PRODUCTO", "TEMP_FAMILIA_PRODUCTO");
                }
                else if (txtFileName == fileNameMarcaProducto.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameMarcaProducto, "TMPMARCAPRODUCTO", filesLocation, DTSLocation, 2, txtFileName, "PKG_MARCA_PRODUCTO", "TEMP_MARCA_PRODUCTO");
                }
                else if (txtFileName == fileNameRuta.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameRuta, "TMPRUTA", filesLocation, DTSLocation, 3, txtFileName, "PKG_RUTA", "TEMP_RUTA");
                }
                else if (txtFileName == fileNameCobranza.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameCobranza, "TMPCOBRANZA", filesLocation, DTSLocation, 10, txtFileName, "PKG_COBRANZA", "TEMP_COBRANZA");
                }
                else if (txtFileName == fileNameVendedor.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameVendedor, "TMPVENDEDOR", filesLocation, DTSLocation, 6, txtFileName, "PKG_USUARIO", "TEMP_USUARIO");
                }
                else if (txtFileName == fileNameGeneral.ToUpper())
                {
                    archivo = ejecutarCarga_Archivo(fileNameGeneral, "TMPGENERAL", filesLocation, DTSLocation, 4, txtFileName, "PKG_GENERAL", "TEMP_GENERAL");
                }

                else if (txtFileName == fileNameAlmacen.ToUpper())
                {
                    if (TPAL.Valor == "T")
                    {
                        archivo = ejecutarCarga_Archivo(fileNameAlmacen, "TMPALMACEN", filesLocation, DTSLocation, 3, txtFileName, "PKG_ALMACEN", "TEMP_ALMACEN");
                    }
                }
                else if (txtFileName == fileNameAlmacenProducto.ToUpper())
                {
                    if (TPAL.Valor == "T")
                    {
                        archivo = ejecutarCarga_Archivo(fileNameAlmacenProducto, "TMPALMACENPRODUCTO", filesLocation, DTSLocation, 3, txtFileName, "PKG_ALMACEN_PRODUCTO_PRESENTACION", "TEMP_ALMACEN_PRODUCTO_PRESENTACION");
                    }
                }

                else if (txtFileName == fileNamePresentacion.ToUpper())
                {
                    if (TPFM.Valor == "T" || TPFS.Valor == "T")
                    {
                        archivo = ejecutarCarga_Archivo(fileNamePresentacion, "TMPPRESENTACION", filesLocation, DTSLocation, 9, txtFileName, "PKG_PRESENTACION", "TEMP_PRESENTACION");
                    }
                }

                if (archivo != null)
                {
                    archivo.archivo = archivo.archivo + ".txt";
                    listaArchivos.Add(archivo);
                }
            }
            return listaArchivos;
        }

        private static FileCargaBean ejecutarCarga_Archivo(String FileName, String FileNameTMP, String FilePath, String DTSPath, Int32 CantColumnas, String TableName, String dtsName, String tmpTable)
        {
            FileCargaBean FileBean = new FileCargaBean();
            String rutaArchivoAntigua = FilePath + FileName + ".txt";
            String rutaArchivoNueva = FilePath + FileNameTMP + ".txt";
            FileBean.archivo = FileName;
            ErrorCargaBean errorCarga = validarCarga(rutaArchivoAntigua, rutaArchivoNueva, CantColumnas, TableName);

            // si no hay error en la validacion de carga se ejecuta DTS
            if (!errorEnCargaArchivo(errorCarga))
            {
                String dtsFilePath = DTSPath + dtsName;
                FileBean = DTSModel.executeDTSX(dtsFilePath, dtsName, rutaArchivoNueva, FileNameTMP, tmpTable, FileName);
            }
            else if (errorEnEstructura(errorCarga)) // si hay error en estructura
            {
                FileBean.insertados = 0;
                FileBean.subidos = 0;
                FileBean.actualizados = 0;
                FileBean.errorData = IdiomaCultura.getMensaje(IdiomaCultura.WEB_ERROR_ESTRUCTURA) + FileName;
            }
            else // si el error es de ejecución se muestra el error capturado
            {
                FileBean.insertados = 0;
                FileBean.actualizados = 0;
                FileBean.subidos = 0;
                FileBean.errorExecute = errorCarga.descripcionError + FileName;
            }
            return FileBean;
        }

        private static ErrorCargaBean validarCarga(String pRuta, String pRutaNueva, int cantidadColumnas, string tableName)
        {
            String[] loCadena = null;
            StreamReader loSr = null;
            StreamWriter sw = null;

            List<ErrorCargaBean> listaErrorCarga = new List<ErrorCargaBean>();

            // errores indica
            int errores = 0;
            // indica las lineas que fueron agregadas con data 
            // si este valor es cero indica que el archivo es vacio o tiene columnas vacias
            int lineas_con_data = 0;

            // por defecto se indica que no hubo Error
            ErrorCargaBean errorCarga = new ErrorCargaBean(STR_SIN_ERROR, "");

            // cantidad de columnas leidas del registro
            int cantCol_Leida = 0;

            try
            {
                loSr = new System.IO.StreamReader(pRuta, Encoding.GetEncoding("iso-8859-1"));
                sw = new System.IO.StreamWriter(pRutaNueva, false, Encoding.GetEncoding("iso-8859-1"));

                String[] loCarga = loSr.ReadToEnd().Split('\n');

                loCadena = new string[loCarga.Length];

                String lsCadena = "";

                String str_NumColumnas = IdiomaCultura.getMensaje(IdiomaCultura.WEB_ERROR_NUM_COLUMNAS) + " ";
                String str_tiene = IdiomaCultura.getMensaje(IdiomaCultura.WEB_TIENE) + " ";
                String str_debeTeber = " " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DEBE_TENER) + " ";

                // tamaño que se considera como maximo para truncar los archivos
                Int32 tamañoLimiteTruncarCampo = MAX_LON_CARGA + cantidadColumnas;

                // tamaño que se considera como minimo
                Int32 tamañoMinimo = cantidadColumnas;

                for (int j = 0; j < loCarga.Length; j++)
                {

                    if (loCarga[j] != string.Empty && loCarga[j].Length > tamañoMinimo)
                    {

                        // quitar espacios en blanco y tabs
                        loCarga[j] = loCarga[j].Trim().Replace("\t", string.Empty);

                        // truncar campos que puedan exceder el maximo permetido
                        if (loCarga[j].Length > tamañoLimiteTruncarCampo)
                        {
                            loCarga[j] = truncarCamposFilaArchivoCarga(loCarga[j]);
                        }

                        String[] loCargaTotal = loCarga[j].Split('|');

                        // leemos cantidad de columnas del archivo
                        cantCol_Leida = loCargaTotal.Length;

                        // validamos si estructura es correcta
                        // si es correcta copiamos cadena
                        if (cantidadColumnas == cantCol_Leida)
                        {

                            // solo se considera aquellas filas que no tengan vacio en todas las columnas
                            if (loCarga[j].Length > cantidadColumnas)
                            {
                                string caracterRetorno = loCarga[j].Substring(loCarga[j].Length - 1, 1);

                                // obtenemos texto de la fila j
                                lsCadena = lsCadena + obtenerTextoFila(loCarga[j], tableName);

                                lineas_con_data++;
                            }
                        }
                        else
                        {
                            if (cantCol_Leida > 1) // solo es error si no es la ultima fila y numero de columnas es diferente
                            {
                                errorCarga = new ErrorCargaBean(j + 1, str_NumColumnas + str_tiene + cantCol_Leida + str_debeTeber + cantidadColumnas);
                                listaErrorCarga.Add(errorCarga);
                                errores++;
                            }
                            else
                            {
                                String primerCaracter = "";

                                if (cantCol_Leida >= 1)
                                {
                                    primerCaracter = loCargaTotal[0].Trim();
                                }
                                if (primerCaracter.Equals("\r"))
                                {
                                    // no se hace nada
                                }
                                // si es la ultima fila
                                else if (j > loCarga[j].Length - 2)
                                {
                                    if (cantCol_Leida == 1)
                                    {
                                        lsCadena = lsCadena + '\n';
                                    }
                                }
                            }
                        }
                    }
                }

                // si el archivo no tiene columnas con data es vacio y es considerado error
                if (lineas_con_data == 0 && errores <= 0)
                {
                    lsCadena = lsCadena + '\n';
                    errorCarga = new ErrorCargaBean(STR_ERROR_VALIDAR_CARGA, IdiomaCultura.getMensaje(IdiomaCultura.WEB_ERR_NO_DATA) + "_");
                    listaErrorCarga.Add(errorCarga);
                    errores++;
                }
                else
                {
                    // insertamos la lista de errores si hubiera
                    errorCarga = insertaListaErrorCarga(listaErrorCarga, tableName);
                }
                sw.Write(lsCadena);
            }
            catch (Exception ex)
            {
                // BaseLN.registrarLog("Error validarCarga: " + ex);
                errorCarga = new ErrorCargaBean(STR_ERROR_VALIDAR_CARGA, ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (loSr != null)
                {
                    loSr.Close();
                }
            }

            // si no hay error en la carga de archivo
            if (!errorEnCargaArchivo(errorCarga))
            {
                // si hay errorres, se indica mediante la constante STR_ERROR_ARCHIVO
                if (errores > 0)
                {
                    errorCarga = new ErrorCargaBean(STR_ERROR_ARCHIVO, "");
                }
            }
            return errorCarga;
        }

        public static void deleteDataFiles(String filesLocation)
        {
            DirectoryInfo dir = new DirectoryInfo(filesLocation);
            foreach (FileInfo file in dir.GetFiles())
            {
                if (file.Extension != ".dts" && file.Extension != ".scc")
                    file.Delete();
            }
        }

        private static bool errorEnCargaArchivo(ErrorCargaBean errorCarga)
        {
            bool respuesta = true;

            if (errorCarga != null)
            {
                if (errorCarga.id == STR_SIN_ERROR)
                {
                    respuesta = false;
                }
            }
            return respuesta;
        }

        private static String truncarCamposFilaArchivoCarga(String texto)
        {
            String nuevoTexto = "";

            try
            {
                Int32 longitudMaxima = MAX_LON_CARGA;
                String separador = "";

                if (texto != null && texto != String.Empty)
                {
                    String[] textoArray = texto.Split('|');

                    for (int i = 0; i < textoArray.Length; i++)
                    {
                        if (textoArray[i].Length > longitudMaxima)
                        {
                            textoArray[i] = textoArray[i].Substring(0, longitudMaxima);
                        }

                        separador = "|";
                        if (i == 0)
                            separador = "";

                        nuevoTexto = nuevoTexto + separador + textoArray[i];
                    }
                }
            }
            catch
            {
                nuevoTexto = texto;
            }
            return nuevoTexto;
        }

        private static string obtenerTextoFila(String loCarga, string tableName)
        {
            string nuevoTexto = "";

            String[] loCargaTotal = loCarga.Split('|');
            String loClave = String.Empty;

            string caracterRetorno = "";

            if (tableName == "USUARIO")
            {
                nuevoTexto = loCargaTotal[0].ToString().Trim() + "|" + loCargaTotal[1].ToString().Trim() + "|" + loCargaTotal[2].ToString().Trim() + "|";

                if (loCargaTotal[3].ToString().Trim().Length > MAX_LON_CLAVE_USUARIO)
                {
                    loClave = "&$#;"; // esto es un artificio para que se muestre error de formato (error de formato error por uso de caracteres reservados y por maxima longitud)                    
                }
                else if (loCargaTotal[3].ToString().Trim() != String.Empty)
                {
                    loClave = FormsAuthentication.HashPasswordForStoringInConfigFile((string)(loCargaTotal[3].ToString().Trim()), "sha1");
                }
                nuevoTexto = nuevoTexto.Trim() + loClave.Trim() + '\r';
            }
            else
            {
                nuevoTexto = loCarga;

                // reparar columna, si columna no tiene el caracter \r al final se inserta
                caracterRetorno = loCarga.Substring(loCarga.Length - 1, 1);
                if (caracterRetorno != "\r")
                {
                    nuevoTexto = nuevoTexto + '\r';
                }
            }
            nuevoTexto = nuevoTexto + '\n'; // al finalizar
            return nuevoTexto;
        }

        private static ErrorCargaBean insertaListaErrorCarga(List<ErrorCargaBean> listaErrorCarga, string tableName)
        {
            String strErrorInsertando = IdiomaCultura.getMensaje(IdiomaCultura.WEB_ERROR_INSERTANDO);
            ErrorCargaBean errorCarga = new ErrorCargaBean(STR_SIN_ERROR, ""); // SIN ERROR

            if (listaErrorCarga.Capacity > 0)
            {
                foreach (ErrorCargaBean itemErrorCarga in listaErrorCarga)
                {
                    try
                    {
                        CargaModel.insertaErrorCarga(itemErrorCarga.id, itemErrorCarga.descripcionError, tableName);
                    }
                    catch (Exception err)
                    {
                        //  BaseLN.registrarLog("Error spS_CarInsError " + tableName + " " + err);
                        errorCarga = new ErrorCargaBean(STR_ERROR_VALIDAR_CARGA, "(spS_CarInsError) " + err.Message);
                    }
                }
            }
            return errorCarga;
        }

        private static bool errorEnEstructura(ErrorCargaBean errorCarga)
        {
            bool respuesta = false;

            if (errorCarga != null)
            {
                if (errorCarga.id == STR_ERROR_ARCHIVO)
                {
                    respuesta = true;
                }
            }
            return respuesta;
        }

        private static string TraducirNombreArchivo(string nombre)
        {
            string nombretraducido = nombre;
            if (nombre.Equals(fileUsuario))
                nombretraducido = "USUARIO";
            else if (nombre.Equals(fileProducto))
                nombretraducido = "PRODUCTO";
            else if (nombre.Equals(fileDireccion))
                nombretraducido = "DIRECCION";
            else if (nombre.Equals(fileCliente))
                nombretraducido = "CLIENTE";
            else if (nombre.Equals(fileGeneral))
                nombretraducido = "GENERAL";
            else if (nombre.Equals(fileRuta))
                nombretraducido = "RUTA";
            else if (nombre.Equals(fileListaPrecios))
                nombretraducido = "LISTAPRECIOS";
            else if (nombre.Equals(fileCobranza))
                nombretraducido = "COBRANZA";
            //@001 I
            else if (nombre.Equals(fileAlmacen))
                nombretraducido = "ALMACEN";
            else if (nombre.Equals(fileAlmacenProducto))
                nombretraducido = "ALMACEN_PRODUCTO_PRESENTACION";
            //@001 F

            else if (nombre.Equals(filePresentacion))
                nombretraducido = "PRESENTACION";
            return nombretraducido;
        }

        public static void eliminarDataAnteriorCarga(String listaArchivosEliminar)
        {
            CargaModel.eliminarDataAnteriorCarga(listaArchivosEliminar);
        }
    }
}