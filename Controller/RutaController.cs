﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

namespace Controller
{
    public class RutaController
    {

        public static PaginateRutaBean paginarBuscar(String clicod, String vendcod, String codigocliente, String flag, String page, String rows, String codperfil, String codgrupo, String id_usu)
        {
            PaginateRutaBean resultado = new PaginateRutaBean();
            List<RutaBean> list = new List<RutaBean>();

            RutaBean bean;

            DataTable dt = RutaModel.findRuta(clicod, vendcod, codigocliente, flag, page, rows, codperfil, codgrupo, id_usu);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new RutaBean();
                  
                    bean.id = row["ruta_pk"].ToString();
                    bean.clicodigo = row["cli_cod"].ToString();
                    bean.vendcodigo = row["ven_cod"].ToString();
                    bean.programacion = row["vclid_fecha"].ToString();
                    bean.clinombre = row["cli_nombre"].ToString();
                    bean.usrnombre = row["usr_nombre"].ToString();
                    bean.NOMBREGRUPO = row["NOMBREGRUPO"].ToString();
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }

            return resultado;
        }


        public static RutaBean info(String id)
        {
            DataTable dt = RutaModel.info(id);
            RutaBean bean = new RutaBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["ruta_pk"].ToString();
                bean.clicodigo = row["cli_Cod"].ToString();
                bean.vendcodigo = row["ven_cod"].ToString();
                bean.programacion = row["ruta_diavisita"].ToString();
                bean.flag = row["flgenable"].ToString();
                bean.usrnombre = row["usr_nombre"].ToString();
                bean.clinombre = row["cli_nombre"].ToString();
                
            }
            return bean;
        }

        public static void crear(RutaBean bean)
        {
            try
            {
                if (RutaModel.crear(bean) < 1)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJEREPITEASIG));
                }
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.clicodigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void editar(RutaBean bean)
        {
            try
            {
                Convert.ToString(RutaModel.editar(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.clicodigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void borrar(String id, String flag)
        {
            RutaModel.borrar(id, flag);
        }

        
        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
    }
}
