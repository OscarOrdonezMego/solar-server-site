﻿using business.functions;
using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Data;

///<summary>
///@001 GMC 15/04/2015 Se muestra Descuento Minimo y Maximo
///@002 GMC 05/05/2015 Búsqueda de Producto por Código o Nombre
///@003 GMC 19/08/2019 Se muestra Descuento Precio Soles y Precio Dolares
///</summary>

namespace Controller
{
    public class ProductoController
    {
        public static PaginateProductoBean paginarBuscar(String codigo, String nombre, String flag, String page, String rows, String familia, String marca)
        {
            PaginateProductoBean resultado = new PaginateProductoBean();
            List<ProductoBean> list = new List<ProductoBean>();

            ProductoBean bean;

            DataTable dt = ProductoModel.findProductoBean(codigo, nombre, flag, page, rows, familia, marca);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ProductoBean();
                    bean.id = row["pro_pk"].ToString();
                    bean.codigo = row["pro_codigo"].ToString();
                    bean.stock = row["pro_stock"].ToString();
					bean.preciobase = row["pro_preciobase"].ToString();
                    bean.preciobasesoles = row["PrecioBaseSoles"].ToString();//@003 I/F
                    bean.preciobasedolares = row["PrecioBaseDolares"].ToString();//@003 I/F
                    bean.unidad = row["pro_unidaddefecto"].ToString();
                    bean.nombre = row["pro_nombre"].ToString();
                    bean.color = row["prod_color"].ToString();
                    bean.descuentomin = row["DESC_MIN"].ToString(); //@001 I/F
                    bean.descuentomax = row["DESC_MAX"].ToString(); //@001 I/F
                    bean.familia = row["FAMILIA"].ToString();
                    bean.marca = row["MARCA"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }

        public static ProductoBean info(String id)
        {
            DataTable dt = ProductoModel.info(id);
            ProductoBean bean = new ProductoBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["PRO_PK"].ToString();
                bean.codigo = row["PRO_CODIGO"].ToString();
                bean.color = row["PROD_COLOR"].ToString();
                bean.stock = row["PRO_STOCK"].ToString();
                bean.preciobase = row["PRO_PRECIOBASE"].ToString();
                bean.preciobasesoles = row["PrecioBaseSoles"].ToString(); //@003 I/F
                bean.preciobasedolares = row["PrecioBaseDolares"].ToString(); //@003 I/F
                bean.unidad = row["PRO_UNIDADDEFECTO"].ToString();
                bean.nombre = row["PRO_nombre"].ToString();
                bean.flag = row["FLGENABLE"].ToString();
                bean.descuentomin = row["DESC_MIN"].ToString(); //@001 I/F
                bean.descuentomax = row["DESC_MAX"].ToString();
                bean.familia = row["FAMILIA"].ToString();//@001 I/F
                bean.marca = row["MARCA"].ToString();
            }
            return bean;
        }

        public static ProductoBean infoxcod(String codigo)
        {
            DataTable dt = ProductoModel.infoxcod(codigo);
            ProductoBean bean = new ProductoBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["PRO_PK"].ToString();
                bean.codigo = row["PRO_CODIGO"].ToString();
                bean.color = row["PROD_COLOR"].ToString();
                bean.stock = row["PRO_STOCK"].ToString();
                bean.preciobase = row["PRO_PRECIOBASE"].ToString();
                if (row["PrecioBaseSoles"].ToString() != string.Empty) bean.preciobasesoles = row["PrecioBaseSoles"].ToString(); //@003 I/F
                if (row["PrecioBaseDolares"].ToString() != string.Empty) bean.preciobasedolares = row["PrecioBaseDolares"].ToString(); //@003 I/F
                bean.unidad = row["PRO_UNIDADDEFECTO"].ToString();
                bean.nombre = row["PRO_nombre"].ToString();
                bean.flag = row["FLGENABLE"].ToString();
                bean.descuentomin = row["DESC_MIN"].ToString(); //@001 I/F
                bean.descuentomax = row["DESC_MAX"].ToString();
                bean.familia = row["FAMILIA"].ToString();//@001 I/F
                bean.marca = row["MARCA"].ToString();
            }
            return bean;
        }

        public static void crear(ProductoBean bean)
        {
            try
            {
                Convert.ToString(ProductoModel.crear(bean));
            }
            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public static List<Combo> matchProductoBean(String prod)
        {
            DataTable dt = ProductoModel.matchProductoBean(prod);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["pro_codigo"].ToString();
                    bean.Nombre = row["pro_NOMBRE"].ToString().Trim();
					bean.Precio = row["PRO_PRECIOBASE"].ToString().Trim();
                    if (row["PrecioBaseSoles"].ToString().Trim().Length > 0) bean.PrecioSoles = row["PrecioBaseSoles"].ToString().Trim();
                    if (row["PrecioBaseDolares"].ToString().Trim().Length > 0) bean.PrecioDolares = row["PrecioBaseDolares"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
        public static List<Combo> matchProductoBeanBonificacion(String prod, String tippArticulo)
        {
            DataTable dt = ProductoModel.matchProductoBonificacionBean(prod, tippArticulo);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["pro_codigo"].ToString();
                    bean.Nombre = row["pro_NOMBRE"].ToString();
                    //bean.Precio = row["PRO_PRECIOBASE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
        public static void borrar(String id, String flag)
        {
            ProductoModel.borrar(id, flag);
        }
        //@002 I
        public static List<Combo> searchProductoBean(String prod_busqueda)
        {
            DataTable dt = ProductoModel.searchProductoBean(prod_busqueda);
            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["PRO_CODIGO"].ToString();
                    bean.Nombre = row["PRO_NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }
            }
            return lst;
        }
        //@002 F

        public static List<ProductoBean> listaProductosXFamilia(Int32 familiapk)
        {
            List<ProductoBean> listaproducto = new List<ProductoBean>();
            DataTable dataBd = ProductoModel.listaProductosXFamilia(familiapk);
            if (dataBd != null && dataBd.Rows.Count > 0)
            {
                ProductoBean U;
                foreach (DataRow row in dataBd.Rows)
                {
                    U = new ProductoBean();
                    U.nombre = row["PRO_NOMBRE"].ToString();

                    listaproducto.Add(U);
                }
            }
            return listaproducto;
        }
    }
}