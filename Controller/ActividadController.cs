﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;
using Controller.functions;

namespace Controller
{
    public class ActividadController
    {

        public static PaginateActividadBean paginarBuscar(String codigo, String codigocliente, String codigousuario, String flag, String fechainicio, String usuarioSesion, String codigotipoactividad,  String page, String rows,  String nombreb, String orden)
        {
            PaginateActividadBean resultado = new PaginateActividadBean();
            List<ActividadBean> list = new List<ActividadBean>();

            ActividadBean bean;

            DataTable dt = ActividadModel.findActividad(codigo, codigocliente, codigousuario, flag, fechainicio, usuarioSesion, codigotipoactividad, page, rows, nombreb, orden);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ActividadBean();

                    bean.id = row["ID"].ToString();
                    bean.codigo = row["CODIGO"].ToString();
                    bean.nombre = row["CLIENTE"].ToString();
                    bean.fechainicio = row["FECHAINICIO"].ToString();
                    bean.fechafin = row["FECHAFIN"].ToString();
                    bean.programacion = row["PROGRAMACION"].ToString();
                    bean.flag = row["FLAG"].ToString();
                    bean.descripcion = row["DESCRIPCION"].ToString();
                    bean.tipoactividad = row["TIPOACTIVIDAD"].ToString();
                    bean.usuario = row["USUARIO"].ToString();                    
                    bean.esValido = row["ESVALIDO"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));

            }


            return resultado;
        }


        public static ActividadBean info(String id)
        {
            DataTable dt = ActividadModel.info(id);
            ActividadBean bean = new ActividadBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["IdActividad"].ToString();
                bean.codigo = row["CodActividad"].ToString();
                bean.nombre = row["NOMBRE"].ToString();
                bean.fechainicio = row["FECHAINICIO"].ToString();
                bean.fechafin = row["FECHAFIN"].ToString();
                bean.codigousuario = row["CodUsuario"].ToString();
                bean.orden = Utils.fnVerNuloInt(row["ORDEN"]);
                bean.flag = row["FlgRegHabilitado"].ToString();
                bean.descripcion = row["DESCRIPCION"].ToString();
                bean.codigotipoactividad = row["CodTipActividad"].ToString();
                bean.programacion = row["PROGRAMACION"].ToString();
                bean.usuario = row["USUARIONOMBRE"].ToString();
                bean.tipoactividad = row["TIPOACTIVIDADNOMBRE"].ToString();
                bean.codigocliente = row["CodCliente"].ToString();
                bean.nombrecliente = row["NOMBRECLIENTE"].ToString();
                bean.flgcliespecial = row["FlgCliEspecial"].ToString();
                bean.flgdiasemana = row["FlgDiaSemana"].ToString();
                bean.flgrangofechas = row["FlgRangoFechas"].ToString();
            }
            return bean;
        }

        public static void crear(ActividadBean bean)
        {
            try
            {
                Convert.ToString(ActividadModel.crear(bean));
            }

            catch (Exception ex)
            {
                if (IdiomaCultura.WEB_CODIGOREPETIDO.Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensaje(ex.Message, bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }


            }

         }

        public static void borrar(String id, String flag)
        {
            ActividadModel.borrar(id, flag);
        }


        public static List<Combo> matchActividadBean(String nombre)
        {
            DataTable dt = ActividadModel.matchActividadBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodActividad"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static List<Combo> matchEstadoBean(String nombre)
        {
            DataTable dt = ActividadModel.matchEstadoBean(nombre);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["CodEstado"].ToString();
                    bean.Nombre = row["NOMBRE"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }
    }
}
