﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;

namespace Controller
{
   public class TipoActividadController
    {

        public static PaginateTipoActividadBean paginarBuscar(String codigo, String nombre,  String page, String rows, String flag, String nombreb, String orden)
        {
            PaginateTipoActividadBean resultado = new PaginateTipoActividadBean();
            List<TipoActividadBean> list = new List<TipoActividadBean>();

            TipoActividadBean bean;

            DataTable dt = TipoActividadModel.findTipoActividadBean(codigo, nombre,  flag, page, rows, nombreb, orden);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new TipoActividadBean();
                    bean.id = row["IdTipActividad"].ToString();
                    bean.codigo = row["codigo"].ToString();
                    bean.flag = row["FlgRegHabilitado"].ToString();                    
                    bean.nombre = row["NOMBRE"].ToString();

                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }

            return resultado;
        }


        public static TipoActividadBean info(String id)
        {
            DataTable dt = TipoActividadModel.info(id);
            TipoActividadBean bean = new TipoActividadBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["IdTipActividad"].ToString();
                bean.codigo = row["CodTipActividad"].ToString();
                bean.flag = row["FlgRegHabilitado"].ToString();                
                bean.nombre = row["NOMBRE"].ToString();
            }
            return bean;
        }

        public static void crear(TipoActividadBean bean)
        {
            try
            {
                Convert.ToString(TipoActividadModel.crear(bean));
            }

            catch (Exception ex)
            {
                if (IdiomaCultura.WEB_CODIGOREPETIDO.Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensaje(ex.Message, bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void borrar(String id, String flag)
        {
            TipoActividadModel.borrar(id, flag);
        }
    }
}
