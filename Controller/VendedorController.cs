﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using System.Data;
using Model;
using business.functions;

namespace Controller
{
   public class VendedorController
    {
  

        public static List<Combo> matchVendedorBean(String cod)
        {
            DataTable dt = VendedorModel.matchVendedorBean(cod);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["usr_codigo"].ToString();
                    bean.Nombre = row["usr_nombre"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static List<Combo> matchVendedorBean2(String cod,String codusu)
        {
            DataTable dt = VendedorModel.matchVendedorBean2(cod,codusu);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["USR_PK"].ToString();
                    bean.Nombre = row["usr_nombre"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

      
    }
}
