﻿using System;
using System.Collections.Generic;
using Model.bean;
using System.Data;
using Model;
using business.functions;

namespace Controller
{
    public class ClienteController
    {
        public static PaginateClienteDireccionBean paginarClienteDireccionBuscar(String ID, String coddir, String direccion, String flag, String page, String rows)
        {
            PaginateClienteDireccionBean resultado = new PaginateClienteDireccionBean();
            List<ClienteDireccionBean> list = new List<ClienteDireccionBean>();

            ClienteDireccionBean bean;

            DataTable dt = ClienteModel.findClienteDireccionBean(ID, coddir, direccion, flag, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ClienteDireccionBean();
                    bean.dir_pk = row["ide"].ToString();
                    bean.cli_pk = row["CLIPK"].ToString();
                    bean.codigo = row["coddir"].ToString();
                    bean.nombre = row["direccion"].ToString();
                    bean.latitud = row["latitud"].ToString();
                    bean.longitud = row["longitud"].ToString();
                    bean.dir_tipo = row["DIR_TIPO"].ToString();
                    bean.dir_tipo_des = row["DIR_TIPO_DES"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }
        public static PaginateClienteBean paginarBuscar(String codigo, String nombre, String direccion, String flag, String xtipo, String xgiro, String page, String rows)
        {
            PaginateClienteBean resultado = new PaginateClienteBean();
            List<ClienteBean> list = new List<ClienteBean>();

            ClienteBean bean;

            DataTable dt = ClienteModel.findClienteBean(codigo, nombre, direccion, xtipo, xgiro, flag, page, rows);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["TAMANIOTOTAL"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    bean = new ClienteBean();
                    bean.id = row["cli_pk"].ToString();
                    bean.codigo = row["cli_codigo"].ToString();
                    bean.nombre = row["cli_NOMBRE"].ToString();
                    bean.canal = row["tcli_cod"].ToString();
                    bean.giro = row["cli_giro"].ToString();
                    bean.direccion = row["cli_direccion"].ToString();
                    bean.grupoeconomico = row["grupoeconomico"].ToString();
                    //@003 I
                    bean.CampoAdicional1 = row["CampoAdicional1"].ToString();
                    bean.CampoAdicional2 = row["CampoAdicional2"].ToString();
                    bean.CampoAdicional3 = row["CampoAdicional3"].ToString();
                    bean.CampoAdicional4 = row["CampoAdicional4"].ToString();
                    bean.CampoAdicional5 = row["CampoAdicional5"].ToString();
                    bean.limiteCredito = row["LimiteCredito"].ToString();
                    bean.creditoUtilizado = row["CreditoUtilizado"].ToString();
                    
                    list.Add(bean);
                }

                resultado.lstResultados = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(rows));
            }
            return resultado;
        }


        public static ClienteBean info(String id)
        {
            DataTable dt = ClienteModel.info(id);
            ClienteBean bean = new ClienteBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.id = row["PK"].ToString();
                bean.codigo = row["CODIGO"].ToString();
                bean.nombre = row["NOMBRE"].ToString();
                bean.canal = row["TIPO"].ToString();
                bean.giro = row["GIRO"].ToString();
                bean.flag = row["ENABLE"].ToString();
                bean.direccion = row["DIRECCION"].ToString();
                bean.latitud = row["LATITUD"].ToString();
                bean.longitud = row["LONGITUD"].ToString();
                bean.grupoeconomico = row["grupoeconomico"].ToString();
                //@001 I
                bean.CampoAdicional1 = row["CampoAdicional1"].ToString();
                bean.CampoAdicional2 = row["CampoAdicional2"].ToString();
                bean.CampoAdicional3 = row["CampoAdicional3"].ToString();
                bean.CampoAdicional4 = row["CampoAdicional4"].ToString();
                bean.CampoAdicional5 = row["CampoAdicional5"].ToString();
                bean.limiteCredito = row["LimiteCredito"].ToString();
                bean.creditoUtilizado = row["CreditoUtilizado"].ToString();

                //@001 F
            }
            return bean;
        }

        public static ClienteDireccionBean infoDireccion(Int32 xIDcliente, Int32 xIDdir)
        {
            DataTable dt = ClienteModel.infoDirecciones(xIDcliente, xIDdir);
            ClienteDireccionBean bean = new ClienteDireccionBean();

            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                bean.dir_pk = row["DIR_PK"].ToString();
                bean.cli_pk = row["CLI_PK"].ToString();
                bean.codigo = row["DIR_CODIGO"].ToString();
                bean.nombre = row["DIR_NOMBRE"].ToString();
                bean.latitud = row["LATITUD"].ToString();
                bean.longitud = row["LONGITUD"].ToString();
                bean.dir_tipo = row["DIR_TIPO"].ToString(); //@002 I/F
            }
            return bean;
        }

        public static List<Combo> matchClienteBean(String cliente)
        {
            DataTable dt = ClienteModel.matchClienteBean(cliente);

            List<Combo> lst = new List<Combo>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Combo bean = new Combo();
                    bean.Codigo = row["cli_codigo"].ToString();
                    bean.Nombre = row["cli_nombre"].ToString().Trim();
                    lst.Add(bean);
                }

            }
            return lst;
        }

        public static void crear(ClienteBean bean)
        {
            try
            {
                Convert.ToString(ClienteModel.crear(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void creardireccion(ClienteDireccionBean bean)
        {
            try
            {
                Convert.ToString(ClienteModel.creardireccion(bean));
            }

            catch (Exception ex)
            {
                if ("WEB.MANTENIMIENTO.CODIGOREPETIDO".Equals(ex.Message))
                {
                    throw new Exception(IdiomaCultura.getMensajeDuplicado(bean.codigo));
                }
                else
                {
                    throw new Exception(ex.Message);
                }

            }

        }

        public static void borrar(String id, String flag)
        {
            ClienteModel.borrar(id, flag);
        }

        public static void borrarDirecciones(String cIDcli, String id, String flag)
        {
            ClienteModel.borrarDirecciones(cIDcli, id, flag);
        }

        public static List<TipoClienteBean> fnListarTipoCliente()
        {
            DataTable loDataTable = ClienteModel.fnListarTipoCliente();

            List<TipoClienteBean> poLstTipoClienteBean = new List<TipoClienteBean>();
            if (loDataTable != null && loDataTable.Rows.Count > 0)
            {
                foreach (DataRow loRow in loDataTable.Rows)
                {
                    TipoClienteBean loTipoClienteBean = new TipoClienteBean
                    {
                        Id = Convert.ToInt32(loRow["Id"].ToString()),
                        Codigo = loRow["Codigo"].ToString(),
                        Nombre = loRow["Nombre"].ToString().Trim()
                    };
                    poLstTipoClienteBean.Add(loTipoClienteBean);
                }

            }
            return poLstTipoClienteBean;
        }
    }
}
