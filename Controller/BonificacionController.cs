﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model;
using System.Data;
using Controller.functions;
using Model.functions;
using business.functions;

namespace Controller
{
   public class BonificacionController
    {
       public static RespondInsert crearBonificacion(BonificacionBean bean)
       {
           try
           {
               RespondInsert  resultado=null;
               DataTable loDataTable = BonificacionModel.crearBonificacion(bean);
               if (loDataTable != null && loDataTable.Rows.Count > 0)
               {
                   foreach (DataRow loDataRow in loDataTable.Rows)
                   {
                       resultado = new RespondInsert();
                       resultado.Valor=Int32.Parse( loDataRow["VALOR"].ToString());
                       resultado.Codigo = loDataRow["CODIGO"].ToString(); 
                   }
               }
               return   resultado;
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
       public static PaginateBonificacionBean paginarBuscarBonificacionDetalle(BonificacionBean poBonificacionBean,List<BonificacionBean>listaBonificaciones)
       {
           PaginateBonificacionBean resultado = new PaginateBonificacionBean();
           List<BonificacionBean> list = listaBonificaciones;
           Int32 totalRegistros=0;
               if(list.Count > 0){
                  totalRegistros= listaBonificaciones.Count;
                  resultado.LoLstBonificacionBean = list;
                  resultado.TotalFila = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(poBonificacionBean.TotalPaginas));
               }
               
           return resultado;
       }
       public static PaginateBonificacionBean ListaBonificacion(BonificacionBean poBonificacionBean)
       {
           PaginateBonificacionBean resultado = new PaginateBonificacionBean();
           List<BonificacionBean> list = new List<BonificacionBean>();
           BonificacionBean loBonificacionBean = null;
           Int32 totalRegistros = 0;
           DataTable loDataTable = BonificacionModel.ListaBonificacion(poBonificacionBean); 
           if (loDataTable != null && loDataTable.Rows.Count > 0)
           {
               totalRegistros = Int32.Parse(loDataTable.Rows[0]["TAMANIOTOTAL"].ToString());
               foreach (DataRow loDataRow in loDataTable.Rows)
               {
                   loBonificacionBean = new BonificacionBean();
                   loBonificacionBean.CODIGO_BON_PK =Int32.Parse( loDataRow["BON_PK"].ToString());
                   loBonificacionBean.PRO_PK = loDataRow["PRO_PK"].ToString();
                   loBonificacionBean.PRO_CODIGO = loDataRow["PROD_CODIGO"].ToString();
                   loBonificacionBean.PRO_NOMBRE = loDataRow["PROD_NOMBRE"].ToString();
                   loBonificacionBean.CONPRO_NOMBRE = loDataRow["NOM_PRODUCTO"].ToString();
                   list.Add(loBonificacionBean);
               }
               resultado.LoLstBonificacionBean = list;
               resultado.TotalFila = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(poBonificacionBean.TotalPaginas));
           }

           return resultado;
       }
       public static void crearBonificacionDetalle(BonificacionBean bean)
       {
           try
           {
               
               Convert.ToString( BonificacionModel.crearBonificacionDetalle(bean));
              
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
       public static BonificacionBean BuscarBonificacion(BonificacionBean poBonificacionBean)
       {
           BonificacionBean loBonificacionBean=null;
           DataTable loDataTable = BonificacionModel.BuscarBonificacion(poBonificacionBean);
           if (loDataTable != null && loDataTable.Rows.Count > 0)
           {
               foreach (DataRow loDataRow in loDataTable.Rows)
               {
                   loBonificacionBean = new BonificacionBean();
                   loBonificacionBean.CODIGO_BON_PK=Int32.Parse( loDataRow["BON_PK"].ToString());
                   loBonificacionBean.PRO_CODIGO = loDataRow["PROD_CODIGO"].ToString();
                   loBonificacionBean.PRO_PK = loDataRow["PRO_PK"].ToString();
                   loBonificacionBean.PRO_NOMBRE = loDataRow["PROD_NOMBRE"].ToString();
                   loBonificacionBean.BON_CANTIDAD = loDataRow["BON_CANTIDAD"].ToString();
                   loBonificacionBean.BON_MAXIMO = loDataRow["BON_MAXIMO"].ToString();
                   loBonificacionBean.BON_EDITABLE = loDataRow["BON_EDITABLE"].ToString();
                   loBonificacionBean.CONPRO_NOMBRE = loDataRow["NOMBRE"].ToString();
               }
           }
           return loBonificacionBean;
       }
       public static List< BonificacionBean> BuscarBonificacionDeatalle(BonificacionBean poBonificacionBean)
       {
           List<BonificacionBean> loLstBonificacionBean = new List<BonificacionBean>();
           BonificacionBean loBonificacionBean = null;
           DataTable loDataTable = BonificacionModel.BuscarBonificacionDetalle(poBonificacionBean);
           if (loDataTable != null && loDataTable.Rows.Count > 0)
           {
               foreach (DataRow loDataRow in loDataTable.Rows)
               {
                   loBonificacionBean = new BonificacionBean();
                   loBonificacionBean.BON_DET_PK =Int32.Parse( loDataRow["BON_DET_PK"].ToString());
                   loBonificacionBean.CODIGO_BON_PK =Int32.Parse( loDataRow["BON_PK"].ToString());
                   loBonificacionBean.PRO_CODIGO = loDataRow["PROD_CODIGO"].ToString();
                   loBonificacionBean.PRO_NOMBRE = loDataRow["PRO_NOMBRE"].ToString();
                   loBonificacionBean.BON_CANTIDAD = loDataRow["BON_CANTIDAD"].ToString();
                   loBonificacionBean.BON_MAXIMO = loDataRow["BON_MAXIMO"].ToString();
                   loBonificacionBean.BON_EDITABLE = loDataRow["BON_EDITABLE"].ToString();
                   loBonificacionBean.CONPRO_CODIGO = loDataRow["CONPRO_CODIGO"].ToString();
                   loBonificacionBean.CONPRO_NOMBRE = loDataRow["CONPRO_NOMBRE"].ToString();
                   loBonificacionBean.CONBON_CANTIDAD = loDataRow["CONBON_CANTIDAD"].ToString();
                   loBonificacionBean.Existe = loDataRow["PROD_EXISTE"].ToString();
                   loBonificacionBean.NOM_PRO_MOSTRAR_BON = loDataRow["NOM_PRO_MOSTRAR_BON"].ToString();
                   loLstBonificacionBean.Add(loBonificacionBean);
               }
           }
           return loLstBonificacionBean;
       }
       public static void EditarBonificacion(BonificacionBean bean)
       {
           try
           {

               Convert.ToString(BonificacionModel.EditarBonificacion(bean));

           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
       public static void EditarBonificacionDetalle(BonificacionBean bean)
       {
           try
           {

               Convert.ToString(BonificacionModel.EditarBonificacionDetalle(bean));

           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
       public static void EliminarBonificacionDetalle(Int32 Codigo )
       {
           try
           {

               Convert.ToString(BonificacionModel.EliminarBonificacionDetalle(Codigo));

           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
           public static void EliminarBonificacion(Int32 Codigo ,String tipoArticulo)
       {
           try
           {

               Convert.ToString(BonificacionModel.EliminarBonificacion(Codigo,tipoArticulo));

           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }
           public static List<PresentacionBean> fnBuscarPresentacionBonificacion(String codigopro)
           {
               List<PresentacionBean> loLstPresentacionBean = new List<PresentacionBean>();
               PresentacionBean loPresentacionBean = null;
               DataTable loDataTable = BonificacionModel.fnBuscarPresentacionBonificacion(codigopro);
               if (loDataTable != null && loDataTable.Rows.Count > 0)
               {
                   foreach (DataRow loDataRow in loDataTable.Rows)
                   {
                       loPresentacionBean = new PresentacionBean();
                       loPresentacionBean.Prepk =loDataRow["PRE_PK"].ToString();
                       loPresentacionBean.CODIGOPRE = loDataRow["CODIGO"].ToString();
                       loPresentacionBean.Nombre = loDataRow["NOMBRE"].ToString();
                       loPresentacionBean.Cantidad = loDataRow["CANTIDAD"].ToString();
                       loLstPresentacionBean.Add(loPresentacionBean);
                   }
               }
               return loLstPresentacionBean;
           }
    }
}
