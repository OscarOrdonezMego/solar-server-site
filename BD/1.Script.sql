--USE BD_PED_CUS_ESTANDAR
GO
IF NOT EXISTS (select * from sys.objects OBJ where OBJ.object_id= object_id('GRMaestroTabla') )
BEGIN
	create table GRMaestroTabla(
		id bigint identity(1,1),
		tabla varchar(100),
		descripcion varchar(100),
		tablaTemp varchar(100),
		spcargaTabla varchar(100),
		FlgHabilitado char(1),
		primary key (id)
	)

	PRINT 'GRMaestroTabla'
	END
GO
--select * from GRMaestroTabla
if not exists (select * from GRMaestroTabla where tabla='Cliente')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('Cliente','Tabla cliente','T','TEMP_CLIENTE','USPM_CARGA_CLIENTE_NEW')

if not exists (select * from GRMaestroTabla where tabla='Producto')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('Producto','Tabla Producto','T','TEMP_PRODUCTO','USPM_CARGA_ARTICULO_NEW')

if not exists (select * from GRMaestroTabla where tabla='GENERAL')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('GENERAL','Tabla GENERAL','T','TEMP_GENERAL','USPM_CARGA_GENERAL_NEW')

if not exists (select * from GRMaestroTabla where tabla='VENDEDOR')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('VENDEDOR','Tabla VENDEDOR','T','TEMP_USUARIO','USPM_CARGA_USUARIO_NEW')

if not exists (select * from GRMaestroTabla where tabla='RUTA')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('RUTA','Tabla RUTA','T','TEMP_RUTA','USPM_CARGA_RUTA_NEW')

if not exists (select * from GRMaestroTabla where tabla='FAMILIA')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('FAMILIA','Tabla FAMILIA','T','TEMP_FAMILIA','USPM_CARGA_FAMILIA_NEW')

if not exists (select * from GRMaestroTabla where tabla='FAMILIA_PRODUCTO')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('FAMILIA_PRODUCTO','Tabla FAMILIA_PRODUCTO','T','TEMP_FAMILIA_PRODUCTO','USPM_CARGA_FAMILIA_PRODUCTO_NEW')

if not exists (select * from GRMaestroTabla where tabla='MARCA')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('MARCA','Tabla MARCA','T','TEMP_MARCA','USPM_CARGA_MARCA_NEW')

if not exists (select * from GRMaestroTabla where tabla='MARCA_PRODUCTO')
	insert into GRMaestroTabla(tabla,descripcion,FlgHabilitado,tablaTemp,spcargaTabla) 
	values('MARCA_PRODUCTO','Tabla MARCA_PRODUCTO','T','TEMP_MARCA_PRODUCTO','USPM_CARGA_MARCA_PRODUCTO_NEW')


IF NOT EXISTS (select * from sys.objects OBJ where OBJ.object_id= object_id('GRMaestroTablaColumnas') )
BEGIN
	create table GRMaestroTablaColumnas(
		id bigint identity(1,1),
		idMaestro bigint,
		columna varchar(100),
		columnaTabla varchar(100),
		descripcion varchar(100),
		formato varchar(100),
		obligatorio char(1),
		FlgHabilitado char(1),
		primary key (id)
	)

	PRINT 'GRMaestroTablaColumnas'
	END
GO
--select * from GRMaestroTabla where tabla='Cliente'
--select * from TEMP_CLIENTE
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'CODIGO','Codigo de cliente','TEXTO (500)','T','T','CLI_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='NOMBRE' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'NOMBRE','Nombre de cliente','TEXTO (500)','T','T','CLI_NOMBRE')
if not exists (select * from GRMaestroTablaColumnas where columna='DIRECCION' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'DIRECCION','Direccion de cliente','TEXTO (500)','T','T','CLI_DIRECCION')
if not exists (select * from GRMaestroTablaColumnas where columna='TIPO DE CLIENTE' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'TIPO DE CLIENTE','TipoCliente de cliente','TEXTO (500)','T','T','TCLI_COD')
if not exists (select * from GRMaestroTablaColumnas where columna='GIRO' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'GIRO','GIRO de cliente','TEXTO (500)','T','F','CLI_GIRO')
if not exists (select * from GRMaestroTablaColumnas where columna='LATITUD' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'LATITUD','LATITUD de cliente','TEXTO (500)','T','F','CLI_LATITUD')
if not exists (select * from GRMaestroTablaColumnas where columna='LONGITUD' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'LONGITUD','LONGITUD de cliente','TEXTO (500)','T','F','CLI_LONGITUD')
if not exists (select * from GRMaestroTablaColumnas where columna='ADICIONAL 1' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'ADICIONAL 1','ADICIONAL 1 de cliente','TEXTO (500)','T','F','CLI_ADICIONAL_1')
if not exists (select * from GRMaestroTablaColumnas where columna='ADICIONAL 2' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'ADICIONAL 2','ADICIONAL 2 de cliente','TEXTO (500)','T','F','CLI_ADICIONAL_2')
if not exists (select * from GRMaestroTablaColumnas where columna='ADICIONAL 3' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'ADICIONAL 3','ADICIONAL 3 de cliente','TEXTO (500)','T','F','CLI_ADICIONAL_3')
if not exists (select * from GRMaestroTablaColumnas where columna='ADICIONAL 4' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'ADICIONAL 4','ADICIONAL 4 de cliente','TEXTO (500)','T','F','CLI_ADICIONAL_4')
if not exists (select * from GRMaestroTablaColumnas where columna='ADICIONAL 5' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'ADICIONAL 5','ADICIONAL 5 de cliente','TEXTO (500)','T','F','CLI_ADICIONAL_5')
if not exists (select * from GRMaestroTablaColumnas where columna='LIMITE CREDITO' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'LIMITE CREDITO','LIMITE CREDITO de cliente','TEXTO (500)','T','F','LIMITE_CREDITO')
if not exists (select * from GRMaestroTablaColumnas where columna='CREDITO UTILIZADO' and idMaestro in (select id from GRMaestroTabla where tabla='Cliente'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Cliente'),'CREDITO UTILIZADO','CREDITO UTILIZADO de cliente','TEXTO (500)','T','F','CREDITO_UTILIZADO')
	--select * from GRMaestroTabla
	--select * from GRMaestroTablaColumnas
----	select * from tbl_cliente
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),1,'CODIGO','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),2,'NOMBRE','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),3,'DIRECCION','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),4,'TIPO DE CLIENTE','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),5,'GIRO','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),6,'LATITUD','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),7,'LONGITUD','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),8,'ADICIONAL 1','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),9,'ADICIONAL 2','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),10,'ADICIONAL 3','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),11,'ADICIONAL 4','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),12,'ADICIONAL 5','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),13,'LIMITE CREDITO','E')
--insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--values((select id from GRMaestroTabla where tabla='Cliente'),14,'CREDITO UTILIZADO','E')


if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO PRO/PRE' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'CODIGO PRO/PRE','CODIGO PRO/PRE de Producto','TEXTO (500)','T','T','PRO_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='NOMBRE' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'NOMBRE','NOMBRE de Producto','TEXTO (500)','T','T','PRO_NOMBRE')
if not exists (select * from GRMaestroTablaColumnas where columna='PRECIO BASE' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'PRECIO BASE','PRECIO BASE de Producto','TEXTO (500)','T','T','PRO_PRECIOBASE')
if not exists (select * from GRMaestroTablaColumnas where columna='STOCK' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'STOCK','STOCK de Producto','TEXTO (500)','T','T','PRO_STOCK')
if not exists (select * from GRMaestroTablaColumnas where columna='DESCUENTO MINIMO' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'DESCUENTO MINIMO','DESCUENTO MINIMO de Producto','TEXTO (500)','T','F','DESC_MIN')
if not exists (select * from GRMaestroTablaColumnas where columna='DESCUENTO MAXIMO' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'DESCUENTO MAXIMO','DESCUENTO MAXIMO de Producto','TEXTO (500)','T','F','DESC_MAX')
if not exists (select * from GRMaestroTablaColumnas where columna='UNIDAD FRACCIONAMIENTO' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'UNIDAD FRACCIONAMIENTO','UNIDAD FRACCIONAMIENTO de Producto','TEXTO (500)','T','F','UNIDAD_FRACCIONAMIENTO')
if not exists (select * from GRMaestroTablaColumnas where columna='CANTIDAD' and idMaestro in (select id from GRMaestroTabla where tabla='Producto'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='Producto'),'CANTIDAD','CANTIDAD de Producto','TEXTO (500)','T','F','CANTIDAD')
	


if not exists (select * from GRMaestroTablaColumnas where columna='TIPO CARGA' and idMaestro in (select id from GRMaestroTabla where tabla='GENERAL'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='GENERAL'),'TIPO CARGA','TIPO CARGA de GENERAL','TEXTO (500)','T','T','Tipo')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO CONCEPTO' and idMaestro in (select id from GRMaestroTabla where tabla='GENERAL'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='GENERAL'),'CODIGO CONCEPTO','CODIGO CONCEPTO de GENERAL','TEXTO (500)','T','T','Codigo')
if not exists (select * from GRMaestroTablaColumnas where columna='DESCRIPCION CONCEPTO' and idMaestro in (select id from GRMaestroTabla where tabla='GENERAL'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='GENERAL'),'DESCRIPCION CONCEPTO','DESCRIPCION CONCEPTO de GENERAL','TEXTO (500)','T','T','Nombre')
if not exists (select * from GRMaestroTablaColumnas where columna='ABREVIATURA' and idMaestro in (select id from GRMaestroTabla where tabla='GENERAL'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='GENERAL'),'ABREVIATURA','ABREVIATURA de GENERAL','TEXTO (500)','T','T','Abreviatura')
	

if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO VENDEDOR' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'CODIGO VENDEDOR','CODIGO VENDEDOR de VENDEDOR','TEXTO (500)','T','T','USR_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='LOGIN' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'LOGIN','LOGIN de VENDEDOR','TEXTO (500)','T','T','USR_LOGIN')
if not exists (select * from GRMaestroTablaColumnas where columna='NOMBRE VENDEDOR' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'NOMBRE VENDEDOR','NOMBRE VENDEDOR de VENDEDOR','TEXTO (500)','T','T','USR_NOMBRE')
if not exists (select * from GRMaestroTablaColumnas where columna='CLAVE VENDEDOR' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'CLAVE VENDEDOR','CLAVE VENDEDOR de VENDEDOR','TEXTO (500)','T','T','USR_CLAVE')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO GRUPO' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'CODIGO GRUPO','CODIGO GRUPO de VENDEDOR','TEXTO (500)','T','T','USR_GRUPO')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO PERFIL' and idMaestro in (select id from GRMaestroTabla where tabla='VENDEDOR'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='VENDEDOR'),'CODIGO PERFIL','CODIGO PERFIL de VENDEDOR','TEXTO (500)','T','T','USR_PERFIL')


if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO CLIENTE' and idMaestro in (select id from GRMaestroTabla where tabla='RUTA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='RUTA'),'CODIGO CLIENTE','CODIGO CLIENTE de RUTA','TEXTO (500)','T','T','CLI_COD')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO VENDEDOR' and idMaestro in (select id from GRMaestroTabla where tabla='RUTA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='RUTA'),'CODIGO VENDEDOR','CODIGO VENDEDOR de RUTA','TEXTO (500)','T','T','VEN_COD')
if not exists (select * from GRMaestroTablaColumnas where columna='RUTA' and idMaestro in (select id from GRMaestroTabla where tabla='RUTA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='RUTA'),'RUTA','RUTA de RUTA','TEXTO (500)','T','T','VCLID_FECHA')
	

if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO FAMILIA' and idMaestro in (select id from GRMaestroTabla where tabla='FAMILIA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='FAMILIA'),'CODIGO FAMILIA','CODIGO FAMILIA de FAMILIA','TEXTO (500)','T','T','FAM_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='NOMBRE FAMILIA' and idMaestro in (select id from GRMaestroTabla where tabla='FAMILIA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='FAMILIA'),'NOMBRE FAMILIA','NOMBRE FAMILIA de FAMILIA','TEXTO (500)','T','T','FAM_NOMBRE')


if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO FAMILIA' and idMaestro in (select id from GRMaestroTabla where tabla='FAMILIA_PRODUCTO'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='FAMILIA_PRODUCTO'),'CODIGO FAMILIA','CODIGO FAMILIA de FAMILIA','TEXTO (500)','T','T','FAM_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO PRODUCTO' and idMaestro in (select id from GRMaestroTabla where tabla='FAMILIA_PRODUCTO'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='FAMILIA_PRODUCTO'),'CODIGO PRODUCTO','CODIGO PRODUCTO de FAMILIA','TEXTO (500)','T','T','PRO_CODIGO')


if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO MARCA' and idMaestro in (select id from GRMaestroTabla where tabla='MARCA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='MARCA'),'CODIGO MARCA','CODIGO MARCA de MARCA','TEXTO (500)','T','T','MAR_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='NOMBRE MARCA' and idMaestro in (select id from GRMaestroTabla where tabla='MARCA'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='MARCA'),'NOMBRE MARCA','NOMBRE MARCA de MARCA','TEXTO (500)','T','T','MAR_NOMBRE')



if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO MARCA' and idMaestro in (select id from GRMaestroTabla where tabla='MARCA_PRODUCTO'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='MARCA_PRODUCTO'),'CODIGO MARCA','CODIGO MARCA de MARCA_PRODUCTO','TEXTO (500)','T','T','MAR_CODIGO')
if not exists (select * from GRMaestroTablaColumnas where columna='CODIGO PRODUCTO' and idMaestro in (select id from GRMaestroTabla where tabla='MARCA_PRODUCTO'))
	insert into GRMaestroTablaColumnas(idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio,columnaTabla) 
	values((select id from GRMaestroTabla where tabla='MARCA_PRODUCTO'),'CODIGO PRODUCTO','CODIGO PRODUCTO de MARCA_PRODUCTO','TEXTO (500)','T','T','PRO_CODIGO')

--	insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
--SELECT idMaestro,id,columna,'E' FROM GRMaestroTablaColumnas WHERE ID NOT IN (SELECT idColumna FROM GRMapeoMaestroTabla where tipoFormato='E') 

--exec SPS_MapeoMaestroTabla 
IF NOT EXISTS (select * from sys.objects OBJ where OBJ.object_id= object_id('GRMapeoMaestroTabla') )
BEGIN
	create table GRMapeoMaestroTabla(
		id bigint identity(1,1),
		idMaestro bigint,
		idColumna bigint,
		columnaXls varchar(100),
		tipoFormato char(1)--C Cliente, E Entell
		primary key (id)
	)

	PRINT 'GRMapeoMaestroTabla'
	END
GO

IF OBJECT_ID('FK_GRMaestroTablaColumnas_GRMaestroTabla', 'F') IS NULL 
begin
	ALTER TABLE [dbo].GRMaestroTablaColumnas  WITH NOCHECK ADD  CONSTRAINT FK_GRMaestroTablaColumnas_GRMaestroTabla FOREIGN KEY(idMaestro)
	REFERENCES [dbo].GRMaestroTabla (id)
end
go

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_LISTAR_MaestroTabla' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_LISTAR_MaestroTabla
END
GO
create procedure SPS_LISTAR_MaestroTabla
as
begin
	select id,tabla,descripcion
	from GRMaestroTabla
	where FlgHabilitado='T'
end
go

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_LISTAR_MaestroTablaColumnas' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_LISTAR_MaestroTablaColumnas
END
GO
create procedure SPS_LISTAR_MaestroTablaColumnas
 @idMaestro bigint
as
begin
	select id,idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio
	from GRMaestroTablaColumnas
	where FlgHabilitado='T' and
	idMaestro=@idMaestro
end
go
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_LISTAR_MaestroTablaColumnasID' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_LISTAR_MaestroTablaColumnasID
END
GO
create procedure SPS_LISTAR_MaestroTablaColumnasID
 @idMaestroColumna bigint
as
begin
	select id,idMaestro,columna,descripcion,formato,FlgHabilitado,obligatorio
	from GRMaestroTablaColumnas
	where FlgHabilitado='T' and
	id=@idMaestroColumna
end
go
IF not EXISTS (SELECT 1 FROM systypes st WHERE st.name = 'LstColumnasXls')
BEGIN
	CREATE TYPE [dbo].LstColumnasXls AS TABLE(
			idColumnaxls varchar(100),
			idColumna varchar(100),
			columnaxls varchar(100)
	)
END
go
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_GuardarMaestroTabla' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_GuardarMaestroTabla
END
GO
create procedure SPS_GuardarMaestroTabla
	@idTabla bigint,
	@lstCol AS dbo.LstColumnasXls READONLY
as
begin
	--select idMaestro,idColumna,columnaXls from GRMapeoMaestroTabla
	delete from GRMapeoMaestroTabla where idMaestro=@idTabla and tipoFormato='C'
	insert into GRMapeoMaestroTabla(idMaestro,idColumna,columnaXls,tipoFormato)
	select @idTabla,idColumna,columnaxls,'C' from @lstCol
end
go

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_MapeoMaestroTabla' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_MapeoMaestroTabla
END
GO
create procedure SPS_MapeoMaestroTabla
as
begin
	select distinct b.tabla, a.tipoFormato from GRMapeoMaestroTabla a
	inner join GRMaestroTabla b on a.idMaestro=b.id
end
go

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SPS_MapeoMaestroTablaTablaTipo' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].SPS_MapeoMaestroTablaTablaTipo
END
GO
create procedure SPS_MapeoMaestroTablaTablaTipo
(
	@tabla varchar(100),
	@tipo varchar(1)
)
as
begin
	select b.tabla,c.columna,c.descripcion,a.columnaXls,c.columnaTabla, a.tipoFormato,b.tablaTemp,b.spcargaTabla from GRMapeoMaestroTabla a
	inner join GRMaestroTabla b on a.idMaestro=b.id
	inner join GRMaestroTablaColumnas c on c.idMaestro=b.id and c.id=a.idColumna
	where tabla=@tabla and a.tipoFormato=@tipo
end
go
if not exists(select * from CFEtiqueta where CodEtiqueta = 'JAVA_LINKAUTOAYUDA')
	insert into CFEtiqueta(CodEtiqueta,FlgSincronizar,Descripcion,FchRegistro) VALUES('JAVA_LINKAUTOAYUDA','T','Link Autoayuda',getdate())
if not exists (select * from CFValorEtiqueta where IdEtiqueta in (select IdEtiqueta from CFEtiqueta where CodEtiqueta = 'JAVA_LINKAUTOAYUDA'))
	insert into CFValorEtiqueta (IdPais,IdEtiqueta,Descripcion,FchRegistro) values('1',(select IdEtiqueta from CFEtiqueta where CodEtiqueta = 'JAVA_LINKAUTOAYUDA'),'',getdate())
go
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'spS_AuxSelLinkAutoAyuda' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].spS_AuxSelLinkAutoAyuda
END
GO
create PROCEDURE [dbo].spS_AuxSelLinkAutoAyuda
(
	@CUENTAS VARCHAR(200) OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;

	SET @CUENTAS = ISNULL((SELECT dbo.FX_GetIdiomaWeb('JAVA_LINKAUTOAYUDA')), '')

END
go
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'spS_AuxUpdEtiquetasLink' and xtype = 'P') BEGIN
	DROP PROCEDURE [dbo].spS_AuxUpdEtiquetasLink
END
GO
create PROCEDURE [dbo].spS_AuxUpdEtiquetasLink
(
	@ETIQUETA VARCHAR(200)
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE CFValorEtiqueta
	SET Descripcion = @ETIQUETA
	WHERE IdPais IN (SELECT P.IdPais FROM CFPais P INNER JOIN VW_CFCONFIGURACION C ON P.CodPais = C.Culture)
		AND IdEtiqueta IN (SELECT E.IdEtiqueta FROM CFEtiqueta E WHERE E.CodEtiqueta = 'JAVA_LINKAUTOAYUDA')
END
go



--select * from CFValorEtiqueta
--	WHERE IdPais IN (SELECT P.IdPais FROM CFPais P INNER JOIN VW_CFCONFIGURACION C ON P.CodPais = C.Culture)
--		AND IdEtiqueta IN (SELECT E.IdEtiqueta FROM CFEtiqueta E WHERE E.CodEtiqueta = 'JAVA_LINKAUTOAYUDA')
--sp_helptext SPS_MapeoMaestroTablaTablaTipo 'cliente','e'
--USPC_LIMPIAR_TABLA
--select * from GRMapeoMaestroTabla a
--select * from GRMaestroTablaColumnas a
--left outer join GRMaestroTabla b on b.id=a.idMaestro
--left outer join GRMaestroTablaColumnas c on C.id=a.idColumna
--select * from GRMaestroTabla
--select * from GRMapeoMaestroTabla
--select * from TEMP_CLIENTE
--select * from ERR_CLIENTE
--select * from TBL_CLIENTE
--select * from ERR_CLIENTE 
--select  dbo.VALIDA_CARACTERES_ESPECIALES('|7.\/%&#$%&/()=')
--exec USPM_CARGA_CLIENTE_NEW

--sp_helptext USPM_CARGA_CLIENTE_NEW
--SELECT TIPO FROM TBL_CONFIG_CARGA