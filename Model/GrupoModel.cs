﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Model.bean;
using Model.functions;

namespace Model
{
    public class GrupoModel
    {
        public static Int32 crearGrupo(GrupoBean poGrupoBean)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
            parameter.Value = poGrupoBean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 100);
            parameter.Value = poGrupoBean.nombre;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearGrupo", alParameters));

        }
        public static DataTable fnBuscarListarGrupo(Int32 pagina, Int32 totalpagina, String codigo, String nombre, String flag)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.Int);
            parameter.Value = pagina;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.Int);
            parameter.Value = totalpagina;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 100);
            parameter.Value = nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@flag", SqlDbType.VarChar,1);
            parameter.Value = flag;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarListar", alParameters);
        }
        public static DataTable fnBuscarGrupo(String codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter
            parameter = new SqlParameter("@codigo", SqlDbType.VarChar, 20);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarGrupoCodigo", alParameters);
        }
         public static Int32 EditarGrupo(GrupoBean poGrupoBean)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
            parameter.Value = poGrupoBean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 100);
            parameter.Value = poGrupoBean.nombre;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarGrupo", alParameters));

        }
         public static Int32 EliminarGrupo(String codigo,String flag)
         {
             ArrayList alParameters = new ArrayList();

             SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 900);
             parameter.Value = codigo;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 1);
             parameter.Value = flag;
             alParameters.Add(parameter);

             return Convert.ToInt32(SqlConnector.executeScalar("spS_EliminarGrupo", alParameters));

         }
         public static DataTable fnListarGrupo()
         {
             return SqlConnector.getDataTable("spS_ListaGrupo");
         }
    }
}
