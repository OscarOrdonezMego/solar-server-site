﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Security.Cryptography;

/// <summary>
/// @001 GMC 11/04/2015 Ajustes para actualizar Campos Adicionales Cliente
/// @002 GMC 12/04/2015 Se agrega método para obtener datos de Configuración por Código
/// </summary>

namespace Model
{
    public class ConfiguracionModel
    {
        public static String fnSelServicioActualHash()
        {
            return SqlConnector.executeScalar("spS_AuxSelSeguridad", new ArrayList()).ToString();
        }
        public static Hashtable fnConfiguracionHash(String hashServicio)
        {
            ArrayList lvArrayParameter = new ArrayList();
            ConfiguracionBean loConfig;
            Hashtable loHash = new Hashtable();
            try
            {
                DataSet dst = SqlConnector.getDataset("spS_ManSelConfiguracion", lvArrayParameter);
                MD5 md5 = System.Security.Cryptography.MD5.Create();
                foreach (DataRow row in dst.Tables[0].Rows)
                {
                    String servicio = row["codServicio"].ToString();
                    byte[] hash = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(servicio));
                    servicio = BitConverter.ToString(hash).Replace("-", "");
                    //if (hashServicio.Substring(2).ToUpper().Equals(servicio))
                    if (servicio.Equals("9E3360AC711FCD82CEEA74C8EB69BDA9"))
                    {
                        loConfig = new ConfiguracionBean();
                        loConfig.Codigo = row["Codigo"].ToString().ToUpper().Trim();
                        loConfig.Descripcion = row["Descripcion"].ToString();
                        loConfig.Tipo = row["CodConfig"].ToString().ToUpper().Trim();
                        loConfig.TipoPadre = row["CodConfigPadre"].ToString().ToUpper().Trim();
                        loConfig.Valor = row["Valor"].ToString().Trim();
                        loConfig.FlagHabilitado = row["FlagHabilitado"].ToString().Trim();
                        loConfig.Orden = Convert.ToInt16(row["Orden"]);
                        loHash.Add(loConfig.Codigo, loConfig);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return loHash;
        }
        public static Int32 fnActualizarConfiguracion(String psCodConfig, String psCodigo, String psValor)
        {
            ArrayList loAlParametros = new ArrayList();

            SqlParameter loSqlParametro = new SqlParameter("@CodConfig", SqlDbType.Char, 3);
            loSqlParametro.Value = psCodConfig;
            loAlParametros.Add(loSqlParametro);

            loSqlParametro = new SqlParameter("@Codigo", SqlDbType.VarChar, 10);
            loSqlParametro.Value = psCodigo;
            loAlParametros.Add(loSqlParametro);

            //@001 I
            //loSqlParametro = new SqlParameter("@Valor", SqlDbType.VarChar, 10);
            loSqlParametro = new SqlParameter("@Valor", SqlDbType.VarChar, 50);
            //@001 F
            loSqlParametro.Value = psValor;
            loAlParametros.Add(loSqlParametro);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_ManUpdConfiguracion", loAlParametros));
        }
        //@002 I
        public static DataTable info(String codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PAR_CODIGO", SqlDbType.Char, 4);
            parameter.Value = codigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_OBTENER_VALOR_CONFIGURACION", alParameters);
        }
        //@002 F
    }
}