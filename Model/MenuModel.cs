﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;

namespace Model
{
    public class MenuModel
    {
        public static DataTable fnObtenerDatosMenu(Int32 piIdPerfil)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IdPerfil", SqlDbType.Int);
            parameter.Value = piIdPerfil;
            alParametros.Add(parameter);
            return SqlConnector.getDataTable("spS_TraSelMenu", alParametros);
        }

        public static DataTable fnObtenerDatosManuales(Int32 piIdSupervisor)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IdPerfil", SqlDbType.Int);
            parameter.Value = piIdSupervisor;
            alParametros.Add(parameter);
            return SqlConnector.getDataTable("spS_ListarManual", alParametros);
        }
    }
}
