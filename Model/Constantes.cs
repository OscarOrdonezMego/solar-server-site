﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;

namespace Model
{
    public class Constantes
    {
        public const String FUN_GENERAL = "FGEN";
        public const String MOD_GENERAL = "MGEN";
        public class CodigoValoresConfiguracion
        {
            
            public const String CFG_DESCUENTO_PORCENTAJE = "DEDP";
            public const String CFG_RANGO_DESCUENTO_MINIMO = "DERI";
            public const String CFG_RANGO_DESCUENTO_MAXIMO = "DERA";
            public const String CFG_STOCK = "STST";
            public const String CFG_VALIDAR_STOCK = "STVS";
            public const String CFG_DESCONTAR_STOCK = "STDS";
            public const String CFG_CONSULTA_STOCk_LINEA = "STCL";
            public const String CFG_STOCK_RESTRICTIVO = "STSR";
            public const String CFG_VALIDAR_PRECIO = "PRVP";
            public const String CFG_PRECIO_EDITABLE = "PRPE";
            public const String CFG_PRECIO_TIPO_CLIENTE= "PRTC";
            public const String CFG_PRECIO_CONDICION_VENTA = "PRCV";
            public const String CFG_BONIFICACION_MANUAL = "BOBM";
            public const String CFG_MOSTRAR_CONDICION_VENTA = "TPMV";
            public const String CFG_COBRANZA = "COCO";
            public const String CFG_DEVOLUCION = "DEDE";
            public const String CFG_CANJE= "CACN";
            public const String CFG_GPS = "GEGP";
            public const String CFG_EFICIENCIA = "GEEF";
            public const String CFG_SEGUIMIENTO_RUTA = "GESR";
            public const String CFG_ES_EMBEBIDO = "GEEE";
            public const String CFG_NUMEROS_DECIMALES_VISTA = "GEDV";
            public const String CFG_PAGINACION = "GEPA";
            public const String CFG_DESCUENTO_MONTO = "GEDM";
            public const String CFG_SIMBOLO_MONEDA = "GESM";
            public const String CFG_MOSTRAR_ICONO = "GEMI";
            public const String CFG_IMAGEN_ICONO= "GEII";
            public const String CFG_TECLADO_ALFANUMERICO = "GETA";
            public const String CFG_DESCUENTO_PRODUCTO = "DEHP";
            public const String CFG_DESCUENTO_GENERAL = "DEHG";
            public const String CFG_CLIENTE_CAMPO_1 = "CLC1";
            public const String CFG_CLIENTE_CAMPO_2 = "CLC2";
            public const String CFG_CLIENTE_CAMPO_3 = "CLC3";
            public const String CFG_CLIENTE_CAMPO_4 = "CLC4";
            public const String CFG_CLIENTE_CAMPO_5 = "CLC5";
            public const String CFG_DIRECCION_PEDIDO = "TPDP";
            public const String CFG_DIRECCION_DESPACHO = "TPDD";
            public const String CFG_DIRECCION_NO_VISITA = "TTDV";
            public const String CFG_COTIZACION = "CTCT";
            public const String CFG_MOSTRAR_ALMACEN = "TPAL";
            public const String CFG_FRACCIONAMIENTO_SIMPLE = "TPFS";
            public const String CFG_FRACCIONAMIENTO_MULTIPLE = "TPFM";
            public const String CFG_MAXIMO_NUMERO_DECIMALES = "TPDA";
            public const String CFG_MOSTRAR_CABECERA_PEDIDO = "TPCP";
            public const String CFG_MOSTRAR_FIN_PEDIDO = "TPFP";
            public const String CFG_MOSTRAR_REPORTE_DINAMICO = "FRDI";
            public const String CFG_CONSULTA_PRODUCTO_WS = "TPCW";
            public const String CFG_BONIFICACION_AUTOMATICA = "BONM";
            public const String CFG_DESCUENTO_VOLUMEN = "DEVL";
            public const String CFG_CONSULTA_SEGUIMIENTO_PEDIDO_WS = "TPSP";
            public const String CFG_VERIFICACION_PEDIDO_WS = "PTVW";
            public const String CFG_CLIENTE_FUERA_RUTA = "TPCF";
            public const String CFG_LIMITE_CREDITO = "TPLC";
            public const String CFG_DEUDA_VENCIDA = "TPDV";
            public const String CFG_MONTO_MIN_PEDIDO = "TPMN";
            public const String CFG_MONTO_MAX_PEDIDO = "TPMX";
            public const String CFG_MAX_ITEMS_PEDIDO = "TPMI";

            public const String CFG_BUSQUEDA_NUM_CLIENTE = "BNUC";
            public const String CFG_BUSQUEDA_NUM_ARTICULO = "BNUA";
            public const String CFG_MODO_FUERA_DE_COBERTURA= "MFDC";
            public const String CFG_REPORTE_TRACKING = "RPTK";
        }

        public class CodigoValoresMenu
        {
            public const String COD_CANJE = "RCA";
            public const String COD_DEVOLUCION = "RDE";
            public const String COD_MANTENIMIENTO_FORMULARIO = "MFO";
            public const String COD_MENU_MANT_ALMACEN = "MAL";
            public const String COD_MENU_MANT_PRODUCTO_ALMACEN = "MPA";
            public const String COD_MENU_MANT_PEDIDO = "MPE";
            public const String COD_MENU_REP_DINAMICO = "RDC";
            public const String COD_BONIFICACION_AUTOMATICA = "MBO";
            public const String COD_MANTTENIMIENTO_GENERAL = "MGE";
            public const String COD_MANTTENIMIENTO_LISTA_PRECIO = "MLP";
            public const String COD_DASHBOARD = "RRD";
            public const String COD_MENU_REP_TRACKING = "RRT";
        }
        public class CodigoValoresModulo
        {
            public const String COD_MODULO_DEVOLUCION = "MDEV";
            public const String COD_MODULO_CANJE = "MCAN";
        }
            public class CodigoValoresFuncion
        {
     
            public const String COD_FUNCION_FRACIONAMIENTO = "FFRA";
            public const Int32 FRACIONAMIENTO_ACTIVADO = 1;
            public const Int32 FRACIONAMIENTO_DESACTIVADO = 0;
            public const String COD_FUNCION_BONIFICACION = "MBA";
            public const String CFG_HABILITAR_DESCUENTO = "FDES";
        }
        public class EstadoFlag {
            public const String TRUE = "T";
            public const String FALSE = "F";
        }
       
        
        
    }
}
