﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;

namespace Model
{
    public class GPSModel
    {
        //create procedure SPU_WEB_OBTENERPOSICIONGPS(
	    //    @cod numeric
        //)
        public static DataTable obtenerPosicionGPS(String codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod", SqlDbType.Int);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_OBTENERPOSICIONGPS", alParameters);

        }

        //create procedure SPU_WEB_TRACKINGUSUARIO(
	    //    @usu varchar(250),
	    //    @fec varchar(20)
        //)
        public static DataTable obtenerTrackingGPS(String usuario, String fecha)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar,250);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fec", SqlDbType.VarChar, 20);
            parameter.Value = fecha;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_TRACKINGUSUARIO", alParameters);

        }
        
        /*
        public static String registrarGeoCerca(GPSGeoCercaBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = bean.usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@nom", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);

            String codigo = (SqlConnector.executeScalar("SPU_WEB_REGISTRO_INFOGEOCERCA", alParameters)).ToString();

            foreach (GPSGeoCercaPosicionBean pos in bean.lstPosiciones)
            {
                ArrayList alParametersDeta = new ArrayList();
                SqlParameter parameterDeta = new SqlParameter("@cod", SqlDbType.BigInt);
                parameterDeta.Value = Int32.Parse(codigo);
                alParametersDeta.Add(parameterDeta);

                parameterDeta = new SqlParameter("@lat", SqlDbType.Float);
                parameterDeta.Value = pos.latitud;
                alParametersDeta.Add(parameterDeta);

                parameterDeta = new SqlParameter("@lng", SqlDbType.Float);
                parameterDeta.Value = pos.longitud;
                alParametersDeta.Add(parameterDeta);

                SqlConnector.executeNonQuery("SPU_WEB_REGISTRO_POSGEOCERCA", alParametersDeta);

            }

            return codigo;
        }

        //create procedure SPU_WEB_PAGINAR_REPORTE_GPS
        public static DataTable paginarReporteLocalizacion(String usu, String fecini, String fecfin, String page, String totalrows)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usu;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecini", SqlDbType.VarChar, 10);
            parameter.Value = fecini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecfin", SqlDbType.VarChar, 10);
            parameter.Value = fecfin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = Int32.Parse(page);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = Int32.Parse(totalrows);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_PAGINAR_REPORTE_GPS", alParameters);

        }

        public static DataTable reporteLocalizacion(String usu, String fecini, String fecfin)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usu;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecini", SqlDbType.VarChar, 10);
            parameter.Value = fecini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecfin", SqlDbType.VarChar, 10);
            parameter.Value = fecfin;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_REPORTE_GPS", alParameters);

        }

        public static DataTable trackingUsuario(String usu, String fecha)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usu;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecha", SqlDbType.VarChar, 10);
            parameter.Value = fecha;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_TRACKING_GPS", alParameters);

        }
        */
    }
}
