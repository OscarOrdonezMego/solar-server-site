﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

namespace Model
{
    public class DescargaModel
    {
        public static DataTable fnListaArchivo()
        {
            return SqlConnector.getDataTable("spS_ListaNombreArchivo");
        }
        public static DataTable fnBuscarNombreColumCabecera(String pscodArchivo,String psTipoArticulo,String psTipoColumna)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 4);
            parameter.Value = psTipoArticulo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@TipoColumna", SqlDbType.VarChar, 4);
            parameter.Value = psTipoColumna;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CodArticulo", SqlDbType.VarChar, 4);
            parameter.Value = pscodArchivo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarNombreColumCabecera", alParameters);
        }
       /*  public static DataTable fnBuscarNombreColumCabecera(Int32 piIdColArchivo,String psHabilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IDCOL_ARCHIVO", SqlDbType.Int);
            parameter.Value = piIdColArchivo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FLGHABILITADO", SqlDbType.VarChar, 1);
            parameter.Value = psHabilitado;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_GuardarConfiguracionDescarga", alParameters);
        }*/
         public static Int32 SubGuardarConfiguracionDescarga(CabeceraDetalleBean obj)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@IDCOL_ARCHIVO", SqlDbType.Int);
             parameter.Value = obj.ID;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@FLGHABILITADO", SqlDbType.VarChar, 1);
             parameter.Value = obj.Habilitado;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@ORDEN", SqlDbType.Int);
             parameter.Value = obj.Orden;
             alParameters.Add(parameter);
             return SqlConnector.executeNonQuery("spS_GuardarConfiguracionDescarga", alParameters);
         }

    }
}
