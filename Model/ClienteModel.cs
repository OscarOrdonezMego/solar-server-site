﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

/// <summary>
/// @001 GMC 12/04/2015 Ajustes para actualizar Campos Adicionales Cliente
/// @002 GMC 14/04/2015 Ajustes para actualizar Campos DIR_TIPO (P: Pedido, D: Despacho) y FECCREACION en Mant Direcciones
/// </summary>

namespace Model
{
    public class ClienteModel
    {

        public static DataTable findClienteDireccionBean(String xID, String xCODDIR, String xDIRECCION,  String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@IDCLIENTE", SqlDbType.Int);
            parameter.Value = Int32.Parse(xID);
            alParameters.Add(parameter);

            if (xCODDIR.Trim().Length != 0)
            {
                parameter = new SqlParameter("@CODDIR", SqlDbType.VarChar, 10);
                parameter.Value = xCODDIR;
                alParameters.Add(parameter);
            }
            if (xDIRECCION.Trim().Length != 0)
            {
                parameter = new SqlParameter("@DIRECCION", SqlDbType.VarChar, 00);
                parameter.Value = xDIRECCION;
                alParameters.Add(parameter);
            }

            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 1);
            parameter.Value = FLAG;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_CLIENTE_DIRECCIONES_LIST", alParameters);

        }

        public static DataTable findClienteBean(String CODIGO, String NOMBRE, String direccion, String cTipo,String cGiro, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_NOMBRE", SqlDbType.VarChar, 200);
            parameter.Value = NOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_DIRECCION", SqlDbType.VarChar, 200);
            parameter.Value = direccion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TCLI_COD", SqlDbType.VarChar, 15);
            parameter.Value = cTipo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 200);
            parameter.Value = FLAG;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_GIRO", SqlDbType.VarChar, 15);
            parameter.Value = cGiro;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_CLIENTE", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_CLIENTE", alParameters);

        }
        public static void borrarDirecciones(String cIDcliente,String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_PK", SqlDbType.BigInt);
            parameter.Value = Int32.Parse(cIDcliente);
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_CLIENTEDIRECCION", alParameters);

        }


        public static Int32 crear(ClienteBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_DIRECCION", SqlDbType.VarChar, 250);
            parameter.Value = bean.direccion;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@TCLI_COD", SqlDbType.VarChar, 250);
            parameter.Value = bean.canal;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_GIRO", SqlDbType.VarChar, 250);
            parameter.Value = bean.giro;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_PK", SqlDbType.VarChar, 250);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_SECUENCIA", SqlDbType.Int);
            parameter.Value = bean.secuencia;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_LATITUD", SqlDbType.VarChar, 250);
            parameter.Value = bean.latitud;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_LONGITUD", SqlDbType.VarChar, 250);
            parameter.Value = bean.longitud;
            alParameters.Add(parameter);
            //@001 I
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL1", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional1;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL2", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional2;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL3", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional3;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL4", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional4;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL5", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional5;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@LIMITE_CREDITO", SqlDbType.VarChar, 100);
            parameter.Value = bean.limiteCredito;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CREDITO_UTILIZADO", SqlDbType.VarChar, 100);
            parameter.Value = bean.creditoUtilizado;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@GRUPO_ECONOMICO", SqlDbType.VarChar, 20);
            parameter.Value = bean.grupoeconomico;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_CLIENTE_I", alParameters));

        }
        public static Int32 creardireccion(ClienteDireccionBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_PK", SqlDbType.BigInt);
            parameter.Value =  Int32.Parse(bean.cli_pk) ;
            alParameters.Add(parameter);

            if (bean.dir_pk != "")
            {
                parameter = new SqlParameter("@DIR_PK", SqlDbType.BigInt);
                parameter.Value = Int32.Parse(bean.dir_pk);
                alParameters.Add(parameter);
            }
            parameter = new SqlParameter("@DIR_CODIGO", SqlDbType.VarChar, 10);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DIR_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@LATITUD", SqlDbType.VarChar, 50);
            parameter.Value = bean.latitud;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@LONGITUD", SqlDbType.VarChar, 50);
            parameter.Value = bean.longitud;
            alParameters.Add(parameter);
            //@002 I
            parameter = new SqlParameter("@DIR_TIPO", SqlDbType.Char, 1);
            parameter.Value = bean.dir_tipo;
            alParameters.Add(parameter);
            //@002 F
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_CLIENTEDIRECCION_I", alParameters));
        }
        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_CLIENTE", alParameters);

        }
        public static DataTable infoDirecciones(Int32 cIDcliente,Int32 cIDdir)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_PK", SqlDbType.BigInt);
            parameter.Value = cIDcliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DIR_PK", SqlDbType.BigInt);
            parameter.Value = cIDdir;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_CLIENTEDIRECCION", alParameters);

        }

        public static DataTable matchClienteBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_CLIENTES_BUSQ", alParameters);

        }


        public static DataTable fnListarTipoCliente()
        {
            ArrayList poLstParametros = new ArrayList();
            return SqlConnector.getDataTable("spS_ListarTipoCliente", poLstParametros);

        }
    }
}
