﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateRutaBean
    {
      public List<RutaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateRutaBean()
        {
            lstResultados = new List<RutaBean>();
            totalPages = 0;
            
        }

    }
}
