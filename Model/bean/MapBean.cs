﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class MapBean
    {
        public String latitud { get; set; }
        public String longitud { get; set; }
        public String msg { get; set; }
        public String img { get; set; }
        public String titulo { get; set; }
        public String tipo { get; set; }
        public String vendedor { get; set; }
        public String fecha { get; set; }
        
        public MapBean()
        {
            latitud = "";
            longitud = "";
            msg = "";
            img = "";
            titulo = "";
            tipo = "";
            fecha = "";


        }

    }
}
