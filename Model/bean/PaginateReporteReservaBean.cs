﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateReporteReservaBean
    {
        public List<ReporteReservaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteReservaBean()
        {
            lstResultados = new List<ReporteReservaBean>();
            totalPages = 0;
            
        }
    }
}
