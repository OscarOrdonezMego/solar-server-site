﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteRutaBean
    {
      public List<ReporteRutaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteRutaBean()
        {
            lstResultados = new List<ReporteRutaBean>();
            totalPages = 0;
            
        }

    }
}
