﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class BonificacionBean
    {
        private String _PRO_PK;
        private String _PRE_PK;
        private String _PRO_CODIGO;
        private String _PRO_NOMBRE;
        private String _BON_CANTIDAD;
        private String _BON_MAXIMO;
        private String _BON_EDITABLE;
        private String _TIPO_ARTICULO;
        private String _Pagina;
        private String _totalPaginas;
        private String _Existe;
        private String _CONPRO_CODIGO;
        private String _CONPRO_NOMBRE;
        private String _CONBON_CANTIDAD;
        private Int32 _CODIGO_BON_PK;
        private Int32 _BON_DET_PK;
        private String _NOM_PRO_MOSTRAR_BON;
        private String _NOM_PRO_MOSTRAR_CONDI;

        public String NOM_PRO_MOSTRAR_CONDI
        {
            get { return _NOM_PRO_MOSTRAR_CONDI; }
            set { _NOM_PRO_MOSTRAR_CONDI = value; }
        }

        public String NOM_PRO_MOSTRAR_BON
        {
            get { return _NOM_PRO_MOSTRAR_BON; }
            set { _NOM_PRO_MOSTRAR_BON = value; }
        }
        public Int32 BON_DET_PK
        {
            get { return _BON_DET_PK; }
            set { _BON_DET_PK = value; }
        }

        public Int32 CODIGO_BON_PK
        {
            get { return _CODIGO_BON_PK; }
            set { _CODIGO_BON_PK = value; }
        }

        public String CONBON_CANTIDAD
        {
            get { return _CONBON_CANTIDAD; }
            set { _CONBON_CANTIDAD = value; }
        }

        public String CONPRO_NOMBRE
        {
            get { return _CONPRO_NOMBRE; }
            set { _CONPRO_NOMBRE = value; }
        }

        public String CONPRO_CODIGO
        {
            get { return _CONPRO_CODIGO; }
            set { _CONPRO_CODIGO = value; }
        }


        public String Existe
        {
            get { return _Existe; }
            set { _Existe = value; }
        }

        public String TotalPaginas
        {
            get { return _totalPaginas; }
            set { _totalPaginas = value; }
        }

        public String Pagina
        {
            get { return _Pagina; }
            set { _Pagina = value; }
        }


        public String PRO_CODIGO
        {
            get { return _PRO_CODIGO; }
            set { _PRO_CODIGO = value; }
        }

        public String PRO_NOMBRE
        {
            get { return _PRO_NOMBRE; }
            set { _PRO_NOMBRE = value; }
        }


        public String BON_CANTIDAD
        {
            get { return _BON_CANTIDAD; }
            set { _BON_CANTIDAD = value; }
        }

        public String BON_MAXIMO
        {
            get { return _BON_MAXIMO; }
            set { _BON_MAXIMO = value; }
        }
        private String _BON_ITEMS;

        public String BON_ITEMS
        {
            get { return _BON_ITEMS; }
            set { _BON_ITEMS = value; }
        }

        public String BON_EDITABLE
        {
            get { return _BON_EDITABLE; }
            set { _BON_EDITABLE = value; }
        }
       

        public String TIPO_ARTICULO
        {
            get { return _TIPO_ARTICULO; }
            set { _TIPO_ARTICULO = value; }
        }

        public String PRE_PK
        {
            get { return _PRE_PK; }
            set { _PRE_PK = value; }
        }
        public String PRO_PK
        {
            get { return _PRO_PK; }
            set { _PRO_PK = value; }
        }
        public BonificacionBean()
        {
            _PRO_PK = String.Empty;
            _PRE_PK = String.Empty;
            _PRO_CODIGO = String.Empty;
            _PRO_NOMBRE = String.Empty;
            _BON_CANTIDAD = String.Empty;
            _BON_MAXIMO = String.Empty;
            _BON_EDITABLE = String.Empty;
            _TIPO_ARTICULO = String.Empty;
            _Pagina = String.Empty;
            _totalPaginas = String.Empty;
            _Existe = String.Empty;
            _CONPRO_CODIGO = String.Empty;
            _CONPRO_NOMBRE = String.Empty;
            _CONBON_CANTIDAD = String.Empty;
            _CODIGO_BON_PK = 0;
            _BON_DET_PK = 0;
            _NOM_PRO_MOSTRAR_BON = String.Empty;
            _NOM_PRO_MOSTRAR_CONDI = String.Empty;
        }
    }
}
