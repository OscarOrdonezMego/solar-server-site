﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class AlmacenBean
    {
        public String id { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String direccion { get; set; }
        public String flag { get; set; }
        private string _Stock;
        private string _PrePK;
        private String _ALM_CODIGO;

        public String ALM_CODIGO
        {
            get { return _ALM_CODIGO; }
            set { _ALM_CODIGO = value; }
        }

        public string PrePK
        {
            get { return _PrePK; }
            set { _PrePK = value; }
        }

        public string Stock
        {
            get { return _Stock; }
            set { _Stock = value; }
        }


        public AlmacenBean()
        {
            this.id = "";
            this.codigo = "";
            this.nombre = "";
            this.direccion = "";
            this.flag = "T";
            _Stock = String.Empty;
            _PrePK = String.Empty;
            _ALM_CODIGO = String.Empty;
        }

    }
}