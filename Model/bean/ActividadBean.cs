﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ActividadBean
    {
        public String id { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String flag { get; set; }
        public String fechainicio { get; set; }
        public String fechafin { get; set; }
        public String codigousuario { get; set; }
        public int orden { get; set; }
        public String descripcion { get; set; }
        public String codigotipoactividad { get; set; }
        public String programacion { get; set; }
        public String codigoperfil { get; set; }
        public String tipoactividad { get; set; }
        public String perfil { get; set; }
        public String usuario { get; set; }
        public String codigocliente { get; set; }
        public String nombrecliente { get; set; }
        public String esValido { get; set; }
        public String flgdiasemana { get; set; }
        public String flgrangofechas { get; set; }
        public String flgcliespecial { get; set; }

        public string PROGRAMACIONFORMAT
        {

            get
            {
                string valor = "";

                if (programacion != null && programacion.Length > 6)
                {
                    if (programacion.Substring(0, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_LU) + ",";
                    if (programacion.Substring(1, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_MA) + ",";
                    if (programacion.Substring(2, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_MI) + ",";
                    if (programacion.Substring(3, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_JU) + ",";
                    if (programacion.Substring(4, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_VI) + ",";
                    if (programacion.Substring(5, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_SA) + ",";
                    if (programacion.Substring(6, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_DO) + ",";
                }
                return valor;

            }

        }

        public ActividadBean()
        {
        }
    }
}
