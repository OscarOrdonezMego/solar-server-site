﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// @001 GMC 14/04/2015 Se agrega Tipo Pedido y Dirección Despacho
/// @002 GMC 17/04/2015 Se agrega Flag Mostrar Direccion Despacho
/// @003 GMC 05/05/2015 Se agrega campo y flag para mostrar Almacén
/// </summary>

namespace Model.bean
{
    public class ReportePedidoBean
    {
        public String id { get; set; }
        public String ped_fecinicio { get; set; }
        public String ped_fecfin { get; set; }
        public String ped_fecregistro { get; set; }
        public String ped_condvta { get; set; }
        public String cli_codigo { get; set; }
        public String ped_nopedido { get; set; }
        public String ped_montototal { get; set; }
		public String MontoTotalSoles { get; set; }
        public String MontoTotalDolares { get; set; }
        public String usr_pk { get; set; }
        public String usr_codigo { get; set; }
        public String latitud { get; set; }
        public String longitud { get; set; }
        public String nom_usuario { get; set; }
        public String condvta_nombre { get; set; }
        public String cli_nombre { get; set; }
        public String direccion { get; set; }
        public String observacion { get; set; }
        public String cli_lat { get; set; }
        public String cli_lon { get; set; }
        public String distancia { get; set; }
        public String habilitado { get; set; }
        public String FlgEnCobertura { get; set; }
        public String tieneBonificacion { get; set; }
        public String totalBonificacion { get; set; }
        //@001 I
        public String TIPO_NOMBRE { get; set; }
        public String PED_DIRECCION_DESPACHO { get; set; }
        //@001 F
        public String FlgDireccionDespacho { get; set; } //@002 I/F
        public String ALM_NOMBRE { get; set; } //@003 I/F
        public String FLG_MOSTRAR_ALMACEN { get; set; } //@003 I/F
        private String _TOTAL_FLETE;
        private String _TIPO_NOMBRE;
        private String _NOMBREGRUPO;
        private String _COLOR;
        private String _Coordenda;

        public String Coordenda
        {
            get { return _Coordenda; }
            set { _Coordenda = value; }
        }

        public String COLOR
        {
            get { return _COLOR; }
            set { _COLOR = value; }
        }


        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }

        public String TIPO_NOMBRE1
        {
            get { return _TIPO_NOMBRE; }
            set { _TIPO_NOMBRE = value; }
        }

        public String TOTAL_FLETE
        {
            get { return _TOTAL_FLETE; }
            set { _TOTAL_FLETE = value; }
        }

        public ReportePedidoBean()
        {
            _TOTAL_FLETE = String.Empty;
            _TIPO_NOMBRE = String.Empty;
            _NOMBREGRUPO = String.Empty;
            _COLOR = String.Empty;
            _Coordenda = String.Empty;
        }
    }
}