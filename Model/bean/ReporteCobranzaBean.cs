﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteCobranzaBean
    {


       public String id  { get; set; }
       public String USR_NOMBRE { get; set; }
       public String CLI_CODIGO { get; set; }
       public String COB_CODIGO { get; set; }
       public String CLI_NOMBRE { get; set; }
       public String COB_TIPODOCUMENTO { get; set; }
       public String COB_MONTOTOTAL { get; set; }
       public String COB_MONTOPAGADO { get; set; }
       public String SALDO { get; set; }
       private String _NOMBREGRUPO;
       private String _TIPO_DOCUMENTO;
        public String PAG_VOUCHER { get; set; }
        public String BAN_NOMLARGO { get; set; }
        public String PAG_TIPOPAGO { get; set; }
        public String PAG_FECREGISTRO { get; set; }
        public String PAG_MONTOPAGO { get; set; }
        public String PAG_MONTOPAGODOLARES { get; set; }
        public String LATITUD { get; set; }
        public String LONGITUD { get; set; }
        public String COB_MONTOTOTALSOLES { get; set; }
        public String COB_MONTOPAGADOSOLES { get; set; }
        public String SALDOSOLES { get; set; }
        public String COB_MONTOTOTALDOLARES { get; set; }
        public String COB_MONTOPAGADODOLARES { get; set; }
        public String SALDODOLARES { get; set; }
        public String TIPO_DOCUMENTO
       {
           get { return _TIPO_DOCUMENTO; }
           set { _TIPO_DOCUMENTO = value; }
       }
       
       public String NOMBREGRUPO
       {
           get { return _NOMBREGRUPO; }
           set { _NOMBREGRUPO = value; }
       }
       public ReporteCobranzaBean()
        {
            _NOMBREGRUPO = String.Empty;
            _TIPO_DOCUMENTO = String.Empty;
         }
    }
}
