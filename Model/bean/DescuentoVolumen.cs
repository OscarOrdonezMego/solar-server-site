﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class DescuentoVolumen
    {
        private String  _IDLISTAPRECIO;
        private String _CODIGOPROD;
        private String _CODIGOPRE;
        private String _RMIN;
        private String _RMAX;
        private String _DSCTOMIN;
        private String _DSCTOMAX;
        private String _TIPOARTICULO;
        private String _Codfila;

        public String Codfila
        {
            get { return _Codfila; }
            set { _Codfila = value; }
        }

public String IDLISTAPRECIO
{
  get { return _IDLISTAPRECIO; }
  set { _IDLISTAPRECIO = value; }
}


public String CODIGOPROD
{
  get { return _CODIGOPROD; }
  set { _CODIGOPROD = value; }
}


public String CODIGOPRE
{
  get { return _CODIGOPRE; }
  set { _CODIGOPRE = value; }
}


public String RMIN
{
  get { return _RMIN; }
  set { _RMIN = value; }
}



public String RMAX
{
  get { return _RMAX; }
  set { _RMAX = value; }
}


public String DSCTOMIN
{
  get { return _DSCTOMIN; }
  set { _DSCTOMIN = value; }
} 


public String DSCTOMAX
{
  get { return _DSCTOMAX; }
  set { _DSCTOMAX = value; }
}



public String TIPOARTICULO
{
    get { return _TIPOARTICULO; }
    set { _TIPOARTICULO = value; }
}
public DescuentoVolumen()
{
      _IDLISTAPRECIO = String.Empty;
      _CODIGOPROD = String.Empty;
      _CODIGOPRE = String.Empty;
      _RMIN = String.Empty;
      _RMAX = String.Empty;
      _DSCTOMIN = String.Empty;
      _DSCTOMAX = String.Empty;
      _TIPOARTICULO = String.Empty;
      _Codfila = String.Empty;
}
    }
   
}
