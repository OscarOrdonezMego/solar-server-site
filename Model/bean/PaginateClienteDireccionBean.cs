﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateClienteDireccionBean
    {
      public List<ClienteDireccionBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateClienteDireccionBean()
        {
            lstResultados = new List<ClienteDireccionBean>();
            totalPages = 0;
            
        }

    }
}
