﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReportePedidoBean
    {
      public List<ReportePedidoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReportePedidoBean()
        {
            lstResultados = new List<ReportePedidoBean>();
            totalPages = 0;
            
        }

    }
}
