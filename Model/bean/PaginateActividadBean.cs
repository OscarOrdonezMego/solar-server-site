﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateListaPrecioBean
    {
      public List<ListaPrecioBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }

        public PaginateListaPrecioBean()
        {
            lstResultados = new List<ListaPrecioBean>();
            totalPages = 0;
            
        }

    }
}
