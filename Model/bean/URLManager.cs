﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Model.bean
{
   public class URLManager
    {

        public const string WEB_MANTENIMIENTO = "WEB_MANTENIMIENTO";
        public const string WEB_USUARIO = "WEB_USUARIO";
        public const string WEB_USUARIO_NUEVO = "3";
        public const string WEB_ACTIVIDAD = "WEB_ACTIVIDAD";
        public const string WEB_ACTIVIDAD_NUEVO = "5";
        public const string WEB_PERFIL = "WEB_PERFIL";
        public const string WEB_ESTADOS = "WEB_ESTADOS";
        public const string WEB_TIPOACTIVIDAD = "WEB_TIPOACTIVIDAD";
        public const string WEB_MOTIVOS = "WEB_MOTIVOS";
        public const string WEB_PARAMETRO = "WEB_PARAMETRO";
        public const string WEB_CONFIGURAR = "WEB_CONFIGURAR";
        public const string WEB_REPORTE = "WEB_REPORTE";
        public const string WEB_REPORTE_VER = "WEB_REPORTE_VER";
        public const string WEB_REPORTE_ASISTENCIA = "WEB_REPORTE_ASISTENCIA";
        public const string WEB_REPORTE_MONITOREO = "WEB_REPORTE_MONITOREO";
        public const string WEB_REPORTE_VISITA = "WEB_REPORTE_VISITA";
        public const string WEB_REPORTE_MONITOREOV = "WEB_REPORTE_MONITOREOV";
        public const string WEB_CARGA_DESCARGA = "WEB_CARGA_DESCARGA";
        public const string WEB_CARGA = "WEB_CARGA";
        public const string WEB_DESCARGA = "WEB_DESCARGA";
        public const string WEB_ROL = "WEB_ROL";
        public const string WEB_ACTIVIDAD_MODULO = "WEB_ACTIVIDAD_MODULO";
        public const string WEB_DASHBOARD = "WEB_DASHBOARD";
        public const string WEB_REPORTE_MAPA = "WEB_REPORTE_MAPA";
        public const string WEB_CLIENTE = "WEB_CLIENTE";
        public const string WEB_GEOCERCA = "WEB_GEOCERCA";
        public const string WEB_DISPATCH = "WEB_DISPATCH";
        public const string WEB_PLANTILLA = "WEB_PLANTILLA";

        public const string WEB_ICONO = "WEB_ICONO";
        public const string WEB_ETIQUETAS = "WEB_ETIQUETAS";
        public const string WEB_BORRADO_MAESTRO = "WEB_BORRADO_MAESTRO";

        public static string URL_MANTENIMIENTO = "";
        public static string URL_REPORTE = "";
        public static string URL_CARGA_DESCARGA = "";

        public const string URL_USUARIO = "/mantenimiento/usuario/usuario.aspx";
        public const string URL_USUARIO_NUEVO = "/mantenimiento/usuario/usuarioNuevo.aspx";
        public const string URL_ACTIVIDAD = "/actividad/actividad/actividad.aspx";
        public const string URL_ACTIVIDAD_NUEVO = "/mantenimiento/actividad/actividadNuevo.aspx";
        public const string URL_PERFIL = "/mantenimiento/perfil/perfil.aspx";
        public const string URL_PERFIL_NUEVO = "/mantenimiento/usuario/PerfilNuevo.aspx";

        public const string URL_ESTADOS = "/actividad/estado/Estado.aspx";
        public const string URL_ESTADOS_NUEVO = "/mantenimiento/actividad/EstadoNuevo.aspx";

        public const string URL_TIPOACTIVIDAD = "/actividad/tipo/tipo.aspx";
        public const string URL_TIPOACTIVIDAD_NUEVO = "/mantenimiento/actividad/TipoActividadNuevo.aspx";
        public const string URL_MOTIVOS = "/mantenimiento/motivos/Motivo.aspx";
        public const string URL_MOTIVOS_NUEVO = "/mantenimiento/general/MotivoNuevo.aspx";
        public const string URL_PARAMETRO = "/mantenimiento/parametro/Parametro.aspx";
        public const string URL_PARAMETRO_EDITAR = "/mantenimiento/general/ParametrosEditar.aspx";
        public const string URL_CONFIGURAR = "/mantenimiento/configurar/configurar.aspx";
        public const string URL_PLANTILLA = "/mantenimiento/plantilla/plantilla.aspx";


        public const string URL_REPORTE_VER = "/reporte/verReporte/Reporte.aspx";
        public const string URL_REPORTE_ASISTENCIA = "/reporte/ASISTENCIA/asistencia.aspx";//"WEB.REPORTE.ACTIVIDAD.ESTADO";
        public const string URL_REPORTE_MONITOREO = "/reporte/monitoreo/MonUsuario/ReporteMonitoreo.aspx";//"WEB.REPORTE.MONITOREO";
        public const string URL_REPORTE_VISITA = "/reporte/ReporteVisita.aspx";//"WEB.REPORTE.VISITA";
        public const string URL_REPORTE_MONITOREOV = "/reporte/ReporteMonitoreoV.aspx";//"WEB.REPORTE.MONITOREOV";
        public const string URL_REPORTE_MAPA = "/reporte/monitoreo/ReporteMonitoreoMapa.aspx";

        public const string URL_CARGA = "/Carga/Carga.aspx";//"WEB.CARGA";
        public const string URL_DESCARGA = "/Carga/Descarga.aspx";//"WEB.DESCARGA";
        public const string URL_ROL = "/mantenimiento/rol/rol.aspx";

        public const string URL_CLIENTE = "/mantenimiento/cliente/Cliente.aspx";
        public const string URL_CLIENTE_NUEVO = "/mantenimiento/cliente/ClienteNuevo.aspx";
        public const string URL_GEOCERCA = "/actividad/geocerca/Geocerca.aspx";
        public const string URL_DISPATCH = "/reporte/monitoreo/dispatch/Dispatch.aspx";

        public const string URL_ICONO = "/mantenimiento/icono/icono.aspx";
        public const string URL_ETIQUETAS = "/mantenimiento/etiqueta/etiqueta.aspx";
        public const string URL_BORRADO_MAESTRO = "/CargaDatos/BorradoMaestro.aspx";

        public static string getURL(string codigo)
        {
            switch (codigo)
            {
                //case WEB_MANTENIMIENTO: return URL_MANTENIMIENTO;
                case WEB_USUARIO: return URL_USUARIO;
                case WEB_ACTIVIDAD: return URL_ACTIVIDAD;
                case WEB_PERFIL: return URL_PERFIL;
                case WEB_ESTADOS: return URL_ESTADOS;
                case WEB_TIPOACTIVIDAD: return URL_TIPOACTIVIDAD;
                case WEB_MOTIVOS: return URL_MOTIVOS;
                case WEB_PARAMETRO: return URL_PARAMETRO;
                case WEB_CONFIGURAR: return URL_CONFIGURAR;
                case WEB_PLANTILLA: return URL_PLANTILLA;
                //case WEB_REPORTE: return URL_REPORTE;
                case WEB_REPORTE_ASISTENCIA: return URL_REPORTE_ASISTENCIA;
                case WEB_REPORTE_VER: return URL_REPORTE_VER;
                case WEB_REPORTE_MONITOREO: return URL_REPORTE_MONITOREO;
                case WEB_REPORTE_VISITA: return URL_REPORTE_VISITA;
                case WEB_REPORTE_MONITOREOV: return URL_REPORTE_MONITOREOV;
                // case WEB_CARGA_DESCARGA: return URL_CARGA_DESCARGA;
                case WEB_CARGA: return URL_CARGA;
                case WEB_DESCARGA: return URL_DESCARGA;
                case WEB_ROL: return URL_ROL;
                case WEB_REPORTE_MAPA: return URL_REPORTE_MAPA;
                case WEB_CLIENTE: return URL_CLIENTE;
                case WEB_GEOCERCA: return URL_GEOCERCA;
                case WEB_DISPATCH: return URL_DISPATCH;
                case WEB_ICONO: return URL_ICONO;
                case WEB_ETIQUETAS: return URL_ETIQUETAS;

                case WEB_BORRADO_MAESTRO: return URL_BORRADO_MAESTRO;
                default: return "/home.aspx";
            }
        }

        public static string getURLEncodeHTML(string code)
        {
            return HttpUtility.HtmlEncode(HttpContext.Current.Request.ApplicationPath + getURL(code));
        }
    }
}
