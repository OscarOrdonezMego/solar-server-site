﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class GeocercaBean
    {
       public String id { get; set; }
       public String nombre { get; set; }
       public String punto { get; set; }
       public String flag { get; set; }
       public List<GeocercaPuntosBean> puntos { get; set; }

       public GeocercaBean()
        {
        }
    }

   public class GeocercaPuntosBean
   {
       public String latitud { get; set; }
       public String longitud { get; set; }


       public GeocercaPuntosBean()
       {
           latitud = "";

           longitud = "";

       }
   }
}
