﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginatePedidoDetalleBean
    {
      public List<PedidoDetalleBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginatePedidoDetalleBean()
        {
            lstResultados = new List<PedidoDetalleBean>();
            totalPages = 0;
            
        }

    }
}
