﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteCanjeBean
    {

        private String _NOMBRE_PRESENTACION;
        private String _DETC_CANTIDAD_PRE;
        private String _DETC_CANTIDAD_FRAC;
        private String _ALM_NOMBRE;

       public String id  { get; set; }
       public String COD_CANJE { get; set; }
       public String CABC_CODUSUARIO { get; set; }
       public String CABC_CODCLIENTE { get; set; }
       public String CABC_FECHA { get; set; }
       public String CABC_NRODOC { get; set; }
       public String CABC_FECHA_CEL { get; set; }
       public String ID_DET_CANJE { get; set; }
       public String DETC_CODPRODUCTO { get; set; }
       public String DETC_CANTIDAD { get; set; }
       public String DETC_FECVCMTO { get; set; }
       public String DETC_OBSERVACION { get; set; }
       public String UNIDAD_FRACCIONAMIENTO { get; set; }
        public String PRECIO_PRESENTACION { get; set; }
        public String PRO_NOMBRE { get; set; }
       public String CLI_NOMBRE { get; set; }
       public String USR_NOMBRE { get; set; }
       public String MOT_NOMBRE { get; set; }
       public String LATITUD { get; set; }
       public String LONGITUD { get; set; }
       public String HABILITADO { get; set; }
       public String FlgEnCobertura { get; set; }
       private String _NOMBREGRUPO;

       public String NOMBREGRUPO
       {
           get { return _NOMBREGRUPO; }
           set { _NOMBREGRUPO = value; }
       }

       
public String ALM_NOMBRE
{
  get { return _ALM_NOMBRE; }
  set { _ALM_NOMBRE = value; }
}

public String DETC_CANTIDAD_FRAC
{
  get { return _DETC_CANTIDAD_FRAC; }
  set { _DETC_CANTIDAD_FRAC = value; }
}

public String DETC_CANTIDAD_PRE
{
  get { return _DETC_CANTIDAD_PRE; }
  set { _DETC_CANTIDAD_PRE = value; }
}

public String NOMBRE_PRESENTACION
{
  get { return _NOMBRE_PRESENTACION; }
  set { _NOMBRE_PRESENTACION = value; }
}
       
       public ReporteCanjeBean()
        {
            _NOMBRE_PRESENTACION = String.Empty;
            _DETC_CANTIDAD_PRE = String.Empty;
            _DETC_CANTIDAD_FRAC = String.Empty;
            _ALM_NOMBRE = String.Empty; _NOMBREGRUPO = String.Empty;
         }
    }
}
