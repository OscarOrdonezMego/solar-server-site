﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateBonificacionBean
    {
        private List<BonificacionBean> _loLstBonificacionBean;
        private Int64 _TotalFila;

        public List<BonificacionBean> LoLstBonificacionBean
        {
            get { return _loLstBonificacionBean; }
            set { _loLstBonificacionBean = value; }
        }

        public Int64 TotalFila
        {
            get { return _TotalFila; }
            set { _TotalFila = value; }
        }
        public PaginateBonificacionBean() {
            _loLstBonificacionBean =new  List<BonificacionBean>();
            _TotalFila = 0;

        }
    }
}
