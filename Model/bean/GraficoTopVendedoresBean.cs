﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class GraficoTopVendedoresBean
    {
        public String avrvendedor { get; set; }
        public String vendedor { get; set; }
        public String total { get; set; }


        public GraficoTopVendedoresBean()
        {
            avrvendedor = "0";
            vendedor = "";
            total = "";
        }

    }
}
