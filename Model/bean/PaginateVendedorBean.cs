﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateVendedorBean
    {
      public List<VendedorBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateVendedorBean()
        {
            lstResultados = new List<VendedorBean>();
            totalPages = 0;
            
        }

    }
}
