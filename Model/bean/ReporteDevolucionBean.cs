﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteDevolucionBean
    {


       public String id  { get; set; }
       public String CDEV_CODDEVOLUCION { get; set; }
       public String CDEV_CODUSUARIO { get; set; }
       public String CDEV_CODCLIENTE { get; set; }
       public String ID_DET_DEVOLUCION { get; set; }
       public String DDEV_CODPRODUCTO { get; set; }
       public String DDEV_CANTIDAD { get; set; }
       public String DDEV_CODMOTIVO { get; set; }
       public String DDEV_CANTIDAD_FRAC { get; set; }
      public String UNIDAD_FRACCIONAMIENTO { get; set; }
       public String FECVCMTO { get; set; }
       public String PRO_NOMBRE { get; set; }
       public String USR_NOMBRE { get; set; }
       public String CLI_NOMBRE { get; set; }
       public String MOT_NOMBRE { get; set; }
       public String CDEV_OBSERVACION { get; set; }
       public String LATITUD { get; set; }
       public String LONGITUD { get; set; }
       public String HABILITADO { get; set; }
       public String FlgEnCobertura { get; set; }
       private String _NOMBRE_PRESENTACION;
        private String _CANTIDA_PRESENTACION;
        private String _PRECIO_PRESENTACION;
       private String _ALMACEN;
       private String _NOMBREGRUPO;
      
    public String NOMBREGRUPO
      {
           get { return _NOMBREGRUPO; }
           set { _NOMBREGRUPO = value; }
       }

public String ALMACEN
{
  get { return _ALMACEN; }
  set { _ALMACEN = value; }
}

public String PRECIO_PRESENTACION
{
  get { return _PRECIO_PRESENTACION; }
  set { _PRECIO_PRESENTACION = value; }
}

public String CANTIDA_PRESENTACION
{
  get { return _CANTIDA_PRESENTACION; }
  set { _CANTIDA_PRESENTACION = value; }
}

public String NOMBRE_PRESENTACION
{
  get { return _NOMBRE_PRESENTACION; }
  set { _NOMBRE_PRESENTACION = value; }
}
  
       public ReporteDevolucionBean()
        {
            _NOMBRE_PRESENTACION = String.Empty;
            _CANTIDA_PRESENTACION = String.Empty;
            _PRECIO_PRESENTACION = String.Empty;
            _ALMACEN = String.Empty;
            _NOMBREGRUPO = String.Empty;
         }
    }
}
