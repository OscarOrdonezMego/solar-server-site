﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class BECbx
    {
        public String texto { get; set; }
        public String valor { get; set; }

        public BECbx(String psValor, String psTexto)
        {
            texto = psTexto;
            valor = psValor;
        }

    }
}
