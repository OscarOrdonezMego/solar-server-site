﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class CabeceraDetalleBean
    {
        private String _ID;
        private String _Nombre;
        private String _Habilitado;
        private String _Orden;

        public String Orden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }

        public String Habilitado
        {
            get { return _Habilitado; }
            set { _Habilitado = value; }
        }


        public String Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public String ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public CabeceraDetalleBean()
        {
            _ID = String.Empty;
            _Nombre = String.Empty;
            _Habilitado = String.Empty;
            _Orden = String.Empty;
        }
    }
}
