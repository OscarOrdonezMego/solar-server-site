﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateEstadoBean
    {
      public List<BeTipoNew> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateEstadoBean()
        {
            lstResultados = new List<BeTipoNew>();
            totalPages = 0;
            
        }

    }
}
