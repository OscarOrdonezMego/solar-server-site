﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginatePerfilBean
    {
      public List<PerfilBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginatePerfilBean()
        {
            lstResultados = new List<PerfilBean>();
            totalPages = 0;
            
        }

    }
}
