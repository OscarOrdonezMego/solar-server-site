﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateAsistenciaReporteBean
    {
      public List<AsistenciaReporteBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateAsistenciaReporteBean()
        {
            lstResultados = new List<AsistenciaReporteBean>();
            totalPages = 0;
            
        }

    }
}
