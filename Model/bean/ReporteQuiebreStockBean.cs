﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class ReporteQuiebreStockBean
    {
        public String id { get; set; }
        public String ped_fecinicio { get; set; }
        public String ped_fecfin { get; set; }        
        public String usr_pk { get; set; }
        public String usr_codigo { get; set; }       
        public String nom_usuario { get; set; }
        public String cli_nombre { get; set; }
        public String pro_codigo { get; set; }
        public String pro_nombre { get; set; }
        public String det_cantidad { get; set; }
        public String pro_stock  { get; set; }
        public String incod_pro { get; set; }
        public String DET_BONIFICACION { get; set; }
        public String tieneBonificacion { get; set; }
        private String _NOMBREGRUPO;

        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }
        

       public ReporteQuiebreStockBean()
        {
            _NOMBREGRUPO = String.Empty;
         }
       
    }
}
