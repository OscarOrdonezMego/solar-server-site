﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class EficienciaBean
    {
        public String pedidosrealizados { get; set; }
        public String nopedidos { get; set; }
        public String montototalventa { get; set; }
        public String cantidadtotalitems { get; set; }
        public String montocobranza { get; set; }
    }
}