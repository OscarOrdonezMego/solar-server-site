﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateTipoCambioBean
    {
        public List<TipoCambioBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }

        public PaginateTipoCambioBean()
        {
            lstResultados = new List<TipoCambioBean>();
            totalPages = 0;
        }
    }
}
