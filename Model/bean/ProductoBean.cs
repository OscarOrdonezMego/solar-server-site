﻿using System;

///<summary>
///@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
///@002 JBMC 19/08/2019 Ajustes para actualizar PrecioBaseSoles y PrecioBaseDolares
///</summary>

namespace Model.bean
{
    public class ProductoBean
    {

        public const string VISITA = "1";
        public const string NO_VISITA = "2";

        public String stock { get; set; }
        public String codigo { get; set; }
        public String preciobase { get; set; }
        public String unidad { get; set; }
        public String color { get; set; }
        public String nombre { get; set; }
        public String familia { get; set; }
        public String id { get; set; }
        public String flag { get; set; }
        public String descuentomin { get; set; } //@001 I/F
        public String descuentomax { get; set; } //@001 I/F
        public String marca { get; set; }
        public String preciobasesoles { get; set; }//@002 I/F
        public String preciobasedolares { get; set; }//@002 I/F

        public ProductoBean()
        {
            id = "";
            codigo = "";
            nombre = "";
            flag = "T";
            preciobase = "";
            unidad = "";
            color = "";
            descuentomin = ""; //@001 I/F
            descuentomax = ""; //@001 I/F
            //preciobasesoles = "";//@002 I/F
            //preciobasedolares = "";//@002 I/F
        }

    }
}
