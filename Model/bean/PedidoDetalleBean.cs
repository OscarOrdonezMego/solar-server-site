﻿using System;

namespace Model.bean
{
    public class PedidoDetalleBean
    {
        public String IDPED { get; set; }
        public String id { get; set; }
        public String idDET { get; set; }
        public String PRO_CODIGO { get; set; }
        public String PRO_NOMBRE { get; set; }
        public String DET_CANTIDAD { get; set; }
        public String DET_PRECIO { get; set; }
        public String DET_PRECIO_SOLES { get; set; }
        public String DET_PRECIO_DOLARES { get; set; }
        public String DET_DESCUENTO { get; set; }
        public String DET_MONTO { get; set; }
        public String DET_MONTO_SOLES{ get; set; }
        public String DET_MONTO_DOLARES { get; set; }
        public String DET_DESCRIPCION { get; set; }
        public String DET_BONIFICACION { get; set; }
        public String PRO_PRECIOBASE { get; set; }
        public String PRO_PRECIOBASE_SOLES { get; set; }
        public String PRO_PRECIOBASE_DOLARES { get; set; }
        public String CANTIDAD_PRESENTACION { get; set; }
        public String UNIDAD_FRACCIONAMIENTO { get; set; }
		public String DET_PRECIOBASE { get; set; }
        public String DET_CANTIDAD_FRACCION { get; set; }
        public String CODIGO_PRESENTACION { get; set; }
        public String NOMBRE_PRESENTACION { get; set; }
        public String DET_FLETE { get; set; }
        public String PED_PK { get; set; }
        private Int32 _Pagina;
        private Int32 _TotalFila;
        private Int32 _Pro_pk;
        public String Flag { get; set; }
        public String PK_PRESENTACION { get; set; }
        public Int32 ALM_PK { get; set; }

        public string TipoMoneda { get; set; }

        public Int32 Pro_pk
        {
            get { return _Pro_pk; }
            set { _Pro_pk = value; }
        }

        public Int32 TotalFila
        {
            get { return _TotalFila; }
            set { _TotalFila = value; }
        }

        public Int32 Pagina
        {
            get { return _Pagina; }
            set { _Pagina = value; }
        }

        private String _PrecioUnidad;
        private String _PrecioUnidadSoles;
        private String _PrecioUnidadDolares;
        private String _Cod_Presentacion;
        private String _Nom_presentacion;
		private String _PrecioPresentacion;
        private String _PrecioPresentacionSoles;
        private String _PrecioPresentacionDolares;
        private String _Cant_fraccionada;
        private String _Flag;
        private String _PED_PK;
        private String _CODIGO_PRE_PK;
        // private Int32 _ALM_PK;
        private String _PRE_CANTIDAD;
        private String _DET_CANTIDAD_FRAC;
        private String _DET_PRECIO_FRAC;
        private String _DET_FLETE;
        private String _DET_TIPODESC;
        private String _CodListaPrecio;
        private String _PRE_FRACIONADO;

        public String PRE_FRACIONADO
        {
            get { return _PRE_FRACIONADO; }
            set { _PRE_FRACIONADO = value; }
        }


        public String CodListaPrecio
        {
            get { return _CodListaPrecio; }
            set { _CodListaPrecio = value; }
        }

        public String DET_TIPODESC
        {
            get { return _DET_TIPODESC; }
            set { _DET_TIPODESC = value; }
        }


        public String DET_PRECIO_FRAC
        {
            get { return _DET_PRECIO_FRAC; }
            set { _DET_PRECIO_FRAC = value; }
        }

        public String DET_CANTIDAD_FRAC
        {
            get { return _DET_CANTIDAD_FRAC; }
            set { _DET_CANTIDAD_FRAC = value; }
        }

        public String PRE_CANTIDAD
        {
            get { return _PRE_CANTIDAD; }
            set { _PRE_CANTIDAD = value; }
        }



        public String CODIGO_PRE_PK
        {
            get { return _CODIGO_PRE_PK; }
            set { _CODIGO_PRE_PK = value; }
        }



        public String Cant_fraccionada
        {
            get { return _Cant_fraccionada; }
            set { _Cant_fraccionada = value; }
        }
		
		public String PrecioPresentacion
        {
            get { return _PrecioPresentacion; }
            set { _PrecioPresentacion = value; }
        }

        public String PrecioPresentacionSoles
        {
            get { return _PrecioPresentacionSoles; }
            set { _PrecioPresentacionSoles = value; }
        }
        public String PrecioPresentacionDolares
        {
            get { return _PrecioPresentacionDolares; }
            set { _PrecioPresentacionDolares = value; }
        }


        public String Nom_presentacion
        {
            get { return _Nom_presentacion; }
            set { _Nom_presentacion = value; }
        }


        public String Cod_Presentacion
        {
            get { return _Cod_Presentacion; }
            set { _Cod_Presentacion = value; }
        }
        public String PrecioUnidad
        {
            get { return _PrecioUnidad; }
            set { _PrecioUnidad = value; }
        }

        public String PrecioUnidadSoles
        {
            get { return _PrecioUnidadSoles; }
            set { _PrecioUnidadSoles = value; }
        }

        public String PrecioUnidadDolares
        {
            get { return _PrecioUnidadDolares; }
            set { _PrecioUnidadDolares = value; }
        }

        public PedidoDetalleBean()
        {
            _Pagina = 0;
            _TotalFila = 0;
            _Pro_pk = 0;
            _PrecioUnidad = String.Empty;
            _PrecioUnidadSoles = String.Empty;
            _PrecioUnidadDolares = String.Empty;
            _Flag = String.Empty;
            _PED_PK = String.Empty;
            _CODIGO_PRE_PK = String.Empty;
            //_ALM_PK = 0;
            _PRE_CANTIDAD = String.Empty;
            _DET_CANTIDAD_FRAC = String.Empty;
            _DET_PRECIO_FRAC = String.Empty;
            _DET_FLETE = String.Empty;
            _DET_TIPODESC = String.Empty;
            _CodListaPrecio = String.Empty;
            _PRE_FRACIONADO = String.Empty;
        }
    }
}