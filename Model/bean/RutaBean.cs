﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class RutaBean
    {
        public String id { get; set; }
        public String clicodigo { get; set; }
        public String vendcodigo { get; set; }
        public String clinombre { get; set; }
        public String flag { get; set; }
        public String usrnombre { get; set; }
        public String programacion { get; set; }

        public string PROGRAMACIONFORMAT
        {

            get
            {
                string valor = "";

                if (programacion != null && programacion.Length > 6)
                {
                    if (programacion.Substring(0, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_LU) + ",";
                    if (programacion.Substring(1, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_MA) + ",";
                    if (programacion.Substring(2, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_MI) + ",";
                    if (programacion.Substring(3, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_JU) + ",";
                    if (programacion.Substring(4, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_VI) + ",";
                    if (programacion.Substring(5, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_SA) + ",";
                    if (programacion.Substring(6, 1) == "1") valor = valor + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIASEMANA_DO);
                }
                return valor;

            }

        }
        private String _NOMBREGRUPO;

        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }
        public RutaBean()
        {
            _NOMBREGRUPO = String.Empty;
        }
    }
}
