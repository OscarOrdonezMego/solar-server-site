﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateConsolidadoBean
    {
        public DataTable loConsolidado { get; set; }
        public Int32 totalPages { get; set; }

        public PaginateConsolidadoBean()
        {
            loConsolidado = new DataTable();
            totalPages = 0;
        }
    }
}