﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class FotoDato
    {

       public String IdFoto { get; set; }
       public Byte[] Foto { get; set; }
       public String Descripcion { get; set; }
       public String nombreUsuario { get; set; }
       public int nextel { get; set; }
       public String FechaFoto { get; set; }
       public String codigousuario { get; set; }
       public String codigoActividad { get; set; }
       public String NombreArchivo { get; set; }
       public String cod_sec_transaccion{ get; set; }

    }
}
