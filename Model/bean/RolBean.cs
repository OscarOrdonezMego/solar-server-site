﻿using System;
using System.Collections.Generic;

namespace Model.bean
{
    public class RolBean
    {
        public static String ACTIVADO = "T";
        public static String DESACTIVADO = "T";
        public String IdMenu { get; set; }
        public String IdMenuPadre { get; set; }
        public List<RolBean> listaRolHijos { get; set; }
        public String FlgCrear { get; set; }
        public String FlgVer { get; set; }
        public String FlgEditar { get; set; }
        public String FlgEliminar { get; set; }
        public String FlgExportar { get; set; }
        public String DescripcionMenu { get; set; }
    }
}
