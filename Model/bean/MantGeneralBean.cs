﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.bean
{
    public class MantGeneralBean
    {
        public String id { get; set; }
        public String grupo { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String flag { get; set; }
        public String gruponombre { get; set; }
        public String nombrecorto { get; set; }
        public String nuevo { get; set; }
        public String banco { get; set; }
        public String nroDocumento { get; set; }
        public String fechaDiferida { get; set; }

        public MantGeneralBean()
        {   
            id = "";
            codigo="";
            nombre="";
            grupo = "";
            gruponombre = "";
            nombrecorto = "";
            nuevo = "";
            banco = "";
            nroDocumento = "";
            fechaDiferida = "";
        }

    }
}
