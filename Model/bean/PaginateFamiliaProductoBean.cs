﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateFamiliaProductoBean
    {
      public List<FamiliaProductoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateFamiliaProductoBean()
        {
            lstResultados = new List<FamiliaProductoBean>();
            totalPages = 0;
            
        }

    }
}
