﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class TipoActividadBean
    {
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String id { get; set; }
        public String flag { get; set; }


        public TipoActividadBean()
        {
        }
    }
}
