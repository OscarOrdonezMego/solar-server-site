﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class AsistenciaReporteBean
    {
       
       public String id  { get; set; }
       public String usuario { get; set; }
       public String fini { get; set; }
       public String ffin { get; set; }
       public String hini { get; set; }
       public String hfin { get; set; }
       public String latitud { get; set; }
       public String longitud { get; set; }

       public AsistenciaReporteBean()
        { 
         }
    }
}
