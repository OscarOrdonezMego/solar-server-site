﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateReporteQuiebreStockBean
    {
        public List<ReporteQuiebreStockBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteQuiebreStockBean()
        {
            lstResultados = new List<ReporteQuiebreStockBean>();
            totalPages = 0;
            
        }

    }
}
