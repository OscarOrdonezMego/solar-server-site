﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteCobranzaBean
    {
      public List<ReporteCobranzaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteCobranzaBean()
        {
            lstResultados = new List<ReporteCobranzaBean>();
            totalPages = 0;
            
        }

    }
}
