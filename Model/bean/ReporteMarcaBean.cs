﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteMarcaBean
    {
        public string nombre { get; set; }
        public string cantidad { get; set; }
        public string monto { get; set; }
        public string montosoles { get; set; }
        public string montodolares { get; set; }
    }
}
