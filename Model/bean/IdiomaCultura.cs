﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

///<summary>
///@001 GMC 02/05/2015 Etiquetas para Mantenimiento de Almacén
///@002 GMC 05/05/2015 Carga de archivos Almacen y Almacén Producto
///</summary>

namespace Model.bean
{
    public class IdiomaCultura
    {
        //public const string TIPO_JAVA = "J";
        //public const string TIPO_WEB = "W";

        public static string WEB_NSERVICES = "WEB_NSERVICES";
        public static string WEB_NSERVICES_DESACTIVADO = "WEB_NSERVICES_DESACTIVADO";
        public static string WEB_NSERVICES_EMBEBIDO = "WEB_NSERVICES_EMBEBIDO";
        public static string WEB_NSERVICES_EMBEBIDO_REFRESH = "WEB_NSERVICES_EMBEBIDO_REFRESH";
        public static String WEB_NSERVICES_OBLIGA = "WEB_NSERVICES_OBLIGA";
        public static String WEB_ESLOGAN_01 = "WEB_ESLOGAN_01";
        public static String WEB_ESLOGAN_02 = "WEB_ESLOGAN_02";
        public static String WEB_OPERADOR = "WEB_OPERADOR";

        public static String CULTURE = "IdiomaCultura_CULTURA";
        public static String WEB_TODOS = "WEB_TODOS";
        public static String WEB_BUSCAR = "WEB_BUSCAR";
        public static String WEB_GRABAR = "WEB_GRABAR";
        public static String WEB_CANCELAR = "WEB_CANCELAR";
        public static String WEB_VOLVER = "WEB_VOLVER";
        public static String WEB_FECHA = "WEB_FECHA";
        public static String WEB_FECHAINICIO = "WEB_FECHAINICIO";
        public static String WEB_FECHAREGISTRO = "WEB_FECHAREGISTRO";
        public static String WEB_FECHAEDICION= "WEB_FECHAEDICION";
        public static String WEB_FECHAFIN = "WEB_FECHAFIN";
        public static String WEB_SI = "WEB_SI";
        public static String WEB_NO = "WEB_NO";
        public static String WEB_CREAR = "WEB_CREAR";
        public static String WEB_PROGRAMACION = "WEB_PROGRAMACION";
        public static String WEB_ORDEN = "WEB_ORDEN";
        public static String WEB_DURACION = "WEB_DURACION";
        public static String WEB_VER = "WEB_VER";
        public static String WEB_RESTAURA = "WEB_RESTAURA";
        public static String WEB_MANT_BONIFICACIONES = "WEB_MANT_BONIFICACIONES";
        public static String WEB_MANT_PROD_A_BON = "WEB_MANT_PROD_A_BON";
        public static String WEB_MANT_PRES_A_BON = "WEB_MANT_PRES_A_BON";
        public static String WEB_MANT_PRESENTACION = "WEB_MANT_PRESENTACION";
        public static String WEB_MANT_BONOMAX = "WEB_MANT_BONOMAX";
        public static String WEB_MANT_EDITABLE = "WEB_MANT_EDITABLE";
        public static String WEB_MANT_AGREGAR_CONDICION = "WEB_MANT_AGREGAR_CONDICION";
        public static String WEB_EXISTE = "WEB_EXISTE";
        public static String WEB_ALMACEN = "WEB_ALMACEN";
        public static String WEB_PRECIO_UNIDAD = "WEB_PRECIO_UNIDAD";
        public static String WEB_CANTIDAD_PRESENTACION = "WEB_CANTIDAD_PRESENTACION";
        public static String WEB_PRESENTACION = "WEB_PRESENTACION";
        public static String WEB_NOMBRE_PRESENTACION = "WEB_NOMBRE_PRESENTACION";
        public static String WEB_CODIGO_COBRANZA = "WEB_CODIGO_COBRANZA";
        public static String WEB_PRECIO_PRESENTACION = "WEB_PRECIO_PRESENTACION";
        public static String WEB_CANT_FRACCIONADA = "WEB_CANT_FRACCIONADA";
        public static String WEB_FLETE = "WEB_FLETE";
        public static String WEB_LISTA_PRECIO = "WEB_LISTA_PRECIO";
        public static String WEB_ALMACEN_STOCK = "WEB_ALMACEN_STOCK";
        public static String WEB_FRACCION = "WEB_FRACCION";
        public static String WEB_FRACCIONAMIENTO = "WEB_FRACCIONAMIENTO";
        public static String WEB_DESCUENTO_MIN = "WEB_DESCUENTO_MIN";
        public static String WEB_DESCUENTO_MAX = "WEB_DESCUENTO_MAX";
        public static String WEB_COD_PRODUCTO = "WEB_COD_PRODUCTO";
        public static String WEB_NOM_PRODUCTO = "WEB_NOM_PRODUCTO";
        public static String WEB_UNID_FRACIONAMIENTO = "WEB_UNID_FRACIONAMIENTO";
        public static String WEB_DESC_VOL = "WEB_DESC_VOL";
        public static String WEB_TITULO_DESCUENTO_VOL = "WEB_TITULO_DESCUENTO_VOL";
        public static String ETI_MANT_PER_GRU = "ETI_MANT_PER_GRU";
        public static String WEB_MANT_NUEVO_GRUPO = "WEB_MANT_NUEVO_GRUPO";
        public static String WEB_MANT_PERFIL_GRUPO = "WEB_MANT_PERFIL_GRUPO";
        public static String WEB_NOMBRE_GRUPO = "WEB_NOMBRE_GRUPO";
        public static String WEB_EXISTE_GRUPO = "WEB_EXISTE_GRUPO";
        public static String WEB_ELIMINO_EXITO = "WEB_ELIMINO_EXITO";
        public static String WEB_NOMBREGRUPO = "WEB_NOMBREGRUPO";
        public static String WEB_NRO_DOCUMENTO = "WEB_NRO_DOCUMENTO";
        public static String WEB_FECHA_DIFERIDA = "WEB_FECHA_DIFERIDA";
        public static String WEB_TOTAL_PRODUCTOS = "WEB_TOTAL_PRODUCTOS";

        /* nuevos */

        public static String WEB_TITULO = "WEB_TITULO";
        public static String WEB_TITULOCORTO = "WEB_TITULOCORTO";
        public static String WEB_DATOS_GUARDADOS = "WEB_DATOS_GUARDADOS";
        public static String WEB_SHOW = "WEB_SHOW";
        public static String WEB_OPEN_BAR = "WEB_OPEN_BAR";
        public static String WEB_CLOSE_BAR = "WEB_CLOSE_BAR";
        public static String WEB_ERROR_POSICION = "WEB_ERROR_POSICION";
        public static String WEB_ERROR_CONEXION = "WEB_ERROR_CONEXION";

        public static String WEB_FUERA_RUTA = "WEB_FUERA_RUTA";
        public static String WEB_DIADESEMANA = "WEB_DIADESEMANA";
        public static String WEB_RANGODEFECHAS = "WEB_RANGODEFECHAS";
        public static String WEB_DIASEMANA_DO = "WEB_DIASEMANA_DO";
        public static String WEB_DIASEMANA_LU = "WEB_DIASEMANA_LU";
        public static String WEB_DIASEMANA_MA = "WEB_DIASEMANA_MA";
        public static String WEB_DIASEMANA_MI = "WEB_DIASEMANA_MI";
        public static String WEB_DIASEMANA_JU = "WEB_DIASEMANA_JU";
        public static String WEB_DIASEMANA_VI = "WEB_DIASEMANA_VI";
        public static String WEB_DIASEMANA_SA = "WEB_DIASEMANA_SA";
        public static String ETI_MEN_MPP = "ETI_MEN_MPP";

        public static String WEB_TRACKING_ACTIVIDAD = "WEB_TRACKING_ACTIVIDAD";

        public static String WEB_RESUMEN_TRACKING = "WEB_RESUMEN_TRACKING";

        public static String WEB_CANT_ESTADO = "WEB_CANT_ESTADO";
        public static String WEB_CANT_REALIZADO = "WEB_CANT_REALIZADO";
        public static String WEB_FIN_REALIZADO = "WEB_FIN_REALIZADO";
        public static String WEB_INICIO_REALIZADO = "WEB_INICIO_REALIZADO";
        public static String WEB_TRACKING_ESTADO = "WEB_TRACKING_ESTADO";

        public static String WEB_FECHA_REPORTE = "WEB_FECHA_REPORTE";
        public static String WEB_ULTIMA_POSICION = "WEB_ULTIMA_POSICION";
        public static String WEB_TRACKING = "WEB_TRACKING";
        public static String WEB_USUARIO_UPPER = "WEB_USUARIO_UPPER";
        public static String WEB_OBSERVACION_UPPER = "WEB_OBSERVACION_UPPER";

        public static String WEB_NO_ENCONTRARON_RESULTADOS = "WEB_NO_ENCONTRARON_RESULTADOS";


        public static string WEB_MANTENIMIENTO_ICONO = "WEB_MANTENIMIENTO_ICONO";
        public static string WEB_ERROR_EXTENSION_ICONO = "WEB_ERROR_EXTENSION_ICONO";
        public static string WEB_ERROR_TAMANIO_ICONO = "WEB_ERROR_TAMANIO_ICONO";
        public static string WEB_ICONO_ACTUALIZADO_EXITOSAMENTE = "WEB_ICONO_ACTUALIZADO_EXITOSAMENTE";
        public static string WEB_CONF_ICONO_GUARDADA = "WEB_CONF_ICONO_GUARDADA";
        public static string WEB_MOSTRAR_ICONO = "WEB_MOSTRAR_ICONO";
        public static string WEB_SUBIR_ICONO = "WEB_SUBIR_ICONO";

        public static string WEB_MENSAJE_CONFIG_ROL = "WEB_MENSAJE_CONFIG_ROL";
        public static string WEB_SEL_DATA = "WEB_SEL_DATA";

        public static String WEB_CUENTAS = "WEB_CUENTAS";
        public static String JAVA_CUENTAS = "JAVA_CUENTAS";

        public static string WEB_MANT_CONFIG_DESCARGA = "WEB_MANT_CONFIG_DESCARGA";
        public static string WEB_MANT_ARCHIVO_DESCARGA = "WEB_MANT_ARCHIVO_DESCARGA";
        public static string WEB_CAMPOS_CABECERA = "WEB_CAMPOS_CABECERA";
        public static string WEB_CAMPOS_DETALLE = "WEB_CAMPOS_DETALLE";
        public static string WEB_MANT_GRUARDA_CONFIGURACION = "WEB_MANT_GRUARDA_CONFIGURACION";

        #region LOGIN
        public static string WEB_LOGOEMPRESA = "WEB_LOGOEMPRESA";
        public static string WEB_BIENVENIDO_H2 = "WEB_BIENVENIDO_H2";
        public static string WEB_BIENVENIDO_P = "WEB_BIENVENIDO_P";

        public static string WEB_USUARIO_ALERTA = "WEB_USUARIO_ALERTA";
        public static string WEB_CLAVE = "WEB_CLAVE";
        public static string WEB_CLAVE_ALERTA = "WEB_CLAVE_ALERTA";
        public static string WEB_ERROR = "WEB_ERROR";
        public static string WEB_PIE_P = "WEB_PIE_P";
        public static string WEB_PIE_BR = "WEB_PIE_BR";
        public static string WEB_PIE2_BR = "WEB_PIE2_BR";

        public static String WEB_RUTA_ICONO_LOGO = "WEB_RUTA_ICONO_LOGO";
        public static String RUTA_ICONO_LOGO_DEFAULT = "nextel-logo-footer.jpg";
        public static string WEB_INGRESAR = "WEB_INGRESAR";
        #endregion;

        #region MASTERPAGE
        public static string WEB_INICIO = "WEB_INICIO";
        public static string WEB_AYUDA = "WEB_AYUDA";
        public static string WEB_SALIR = "WEB_SALIR";
        public static string WEB_MANTENIMIENTO = "WEB_MANTENIMIENTO";
        public static string WEB_ACTIVIDAD = "WEB_ACTIVIDAD";
        public static string WEB_USUARIO = "WEB_USUARIO";
        public static string WEB_PERFIL = "WEB_PERFIL";
        public static string WEB_ESTADOS = "WEB_ESTADOS";
        public static string WEB_TIPOACTIVIDAD = "WEB_TIPOACTIVIDAD";
        public static string WEB_MOTIVOS = "WEB_MOTIVOS";
        public static string WEB_PARAMETRO = "WEB_PARAMETRO";
        public static string WEB_CONFIGURAR = "WEB_CONFIGURAR";
        public static string WEB_REPORTE = "WEB_REPORTE";
        public static string WEB_REPORTE_ASISTENCIA = "WEB_REPORTE_ASISTENCIA";
        public static string WEB_REPORTE_VER = "WEB_REPORTE_VER";
        public static string WEB_REPORTE_MONITOREO = "WEB_REPORTE_MONITOREO";
        public static string WEB_REPORTE_VISITA = "WEB_REPORTE_VISITA";
        public static string WEB_CARGA_DESCARGA = "WEB_CARGA_DESCARGA";
        public static string WEB_FORMATOS_CARGA = "WEB_FORMATOS_CARGA";

        public static string WEB_CARGA_DESCARGA_TITLE = "WEB_CARGA_DESCARGA_TITLE";
        public static string WEB_CARGA = "WEB_CARGA";
        public static string WEB_DESCARGA = "WEB_DESCARGA";
        public static string WEB_ROL = "WEB_ROL";
        public static string WEB_MANTENIMIENTOROL = "WEB_MANTENIMIENTOROL";
        public static string WEB_REALIZADO = "WEB_REALIZADO";
        public static string WEB_ULTIMO_REGISTRO = "WEB_ULTIMO_REGISTRO";
        public static string WEB_TIEMPO_INACTIVO = "WEB_TIEMPO_INACTIVO";

        public static string WEB_ICONO = "WEB_ICONO";
        public static String WEB_ETIQUETAS = "WEB_ETIQUETAS";

        #endregion;

        #region HOME
        public static string WEB_HOME_ASPX_BIENVENIDO = "WEB_HOME_ASPX_BIENVENIDO";
        public static string WEB_HOME_ASPX_MENSAJE = "WEB_HOME_ASPX_MENSAJE";
        public static string WEB_HOME_ASPX_IR = "WEB_HOME_ASPX_IR";
        #endregion

        #region mantenimiento

        public static String WEB_EDITARCLIENTE = "WEB_EDITARCLIENTE";
        public static String WEB_EDITARDIRECCION = "WEB_EDITARDIRECCION";
        public static String WEB_NUEVOCLIENTE = "WEB_NUEVOCLIENTE";
        public static String WEB_NUEVADIRECCION = "WEB_NUEVADIRECCION";

        public static string WEB_MANTENIMIENTOUSUARIO = "WEB_MANTENIMIENTOUSUARIO";
        public static string WEB_EDITARUSUARIO = "WEB_EDITARUSUARIO";
        public static String WEB_NUEVOUSUARIO = "WEB_NUEVOUSUARIO";

        public static string WEB_NUEVOSUARIO = "WEB_NUEVOSUARIO";

        public static string WEB_NUEVOGENERAL = "WEB_NUEVOGENERAL";

        public static string WEB_MANTENIMIENTOACTIVIDAD = "WEB_MANTENIMIENTOACTIVIDAD";
        public static string WEB_EDITARACTIVIDAD = "WEB_EDITARACTIVIDAD";
        public static String WEB_NUEVOACTIVIDAD = "WEB_NUEVOACTIVIDAD";

        public static string WEB_NUEVODETALLE = "WEB_NUEVODETALLE";
        public static String WEB_EDITARDETALLE = "WEB_EDITARDETALLE";

        public static string WEB_MANTENIMIENTOPEDIDO = "WEB_MANTENIMIENTOPEDIDO";
        public static string WEB_EDITARPEDIDO = "WEB_EDITARPEDIDO";
        public static String WEB_NUEVOPEDIDO = "WEB_NUEVOPEDIDO";

        public static string WEB_EDITARCOBRANZA = "WEB_EDITARCOBRANZA";
        public static String WEB_NUEVOCOBRANZA = "WEB_NUEVOCOBRANZA";

        public static string WEB_EDITARPRODUCTO = "WEB_EDITARPRODUCTO";
        public static String WEB_NUEVOPRODUCTO = "WEB_NUEVOPRODUCTO";
        public static string WEB_FAMILIA_PRODUCTO = "WEB_FAMILIA_PRODUCTO";
        public static string WEB_FAMILIA = "WEB_FAMILIA";

        public static string WEB_EDITARPRECIO = "WEB_EDITARPRECIO";
        public static String WEB_NUEVOPRECIO = "WEB_NUEVOPRECIO";

        public static string WEB_NUEVOREGGENERAL = "WEB_NUEVOREGGENERAL";
        public static String WEB_EDITARGENERAL = "WEB_EDITARGENERAL";

        public static string WEB_EDITARRUTA = "WEB_EDITARRUTA";
        public static String WEB_NUEVARUTA = "WEB_NUEVARUTA";

        public static String WEB_REGISTRO = "WEB_REGISTRO";
        public static String WEB_NUEVO = "WEB_NUEVO";
        public static String WEB_EDITAR = "WEB_EDITAR";
        public static String WEB_ELIMINAR = "WEB_ELIMINAR";
        public static String WEB_RESTAURAR = "WEB_RESTAURAR";
        public static String WEB_MENSAJE_SELECCION = "WEB_MENSAJE_SELECCION";

        public static String WEB_MENSAJE_ELIMINAR = "WEB_MENSAJE_ELIMINAR";
        public static String WEB_MENSAJE_ELIMINARVARIOS = "WEB_MENSAJE_ELIMINARVARIOS";
        public static String WEB_MENSAJE_RESTAURAR = "WEB_MENSAJE_RESTAURAR";
        public static String WEB_MENSAJE_RESTAURARVARIOS = "WEB_MENSAJE_RESTAURARVARIOS";

        public static String WEB_CODIGO = "WEB_CODIGO";
        public static String WEB_NOMBRE = "WEB_NOMBRE";

        public static String WEB_HABILITADO = "WEB_HABILITADO";
        public static String WEB_MENSAJEOBLIGATORIO = "WEB_MENSAJEOBLIGATORIO";

        public static String WEB_INGRESEVALOR = "WEB_INGRESEVALOR";
        public static String WEB_MENSAJE_EXITO = "WEB_MENSAJE_EXITO";
        public static String WEB_CODIGOREPETIDO = "WEB_CODIGOREPETIDO";
        public static String WEB_LOGINREPETIDO = "WEB_LOGINREPETIDO";
        public static String WEB_FUNCIONALIDAD = "WEB_FUNCIONALIDAD";
        public static String WEB_ACTIVIDAD_MODULO = "WEB_ACTIVIDAD_MODULO";
        public static String WEB_SUPERVISOR = "WEB_SUPERVISOR";
        public static String WEB_DESCRIPCION = "WEB_DESCRIPCION";
        public static String WEB_FECHA_MENOR_MAYOR = "WEB_FECHA_MENOR_MAYOR";
        public static String WEB_FECHA_INCORRECTA = "WEB_FECHA_INCORRECTA";
        public static String WEB_MENSAJE_DESCUENTO = "WEB_MENSAJE_DESCUENTO";

        public static String WEB_USUARIO_RESTRINGIDO = "WEB_USUARIO_RESTRINGIDO";

        public static String WEB_EDITARTIPOACTIVIDAD = "WEB_EDITARTIPOACTIVIDAD";
        public static String WEB_NUEVOTIPOACTIVIDAD = "WEB_NUEVOTIPOACTIVIDAD";
        public static String WEB_MANTENIMIENTOTIPOACTIVIDAD = "WEB_MANTENIMIENTOTIPOACTIVIDAD";

        public static String WEB_EDITARPERFIL = "WEB_EDITARPERFIL";
        public static String WEB_NUEVOPERFIL = "WEB_NUEVOPERFIL";
        public static String WEB_MANTENIMIENTOPERFIL = "WEB_MANTENIMIENTOPERFIL";
        public static String WEB_MANTENIMIENTOESTADOS = "WEB_MANTENIMIENTOESTADOS";
        public static String WEB_VISITA = "WEB_VISITA";
        public static String WEB_NOVISITA = "WEB_NOVISITA";
        public static String WEB_MANTENIMIENTOMOTIVO = "WEB_MANTENIMIENTOMOTIVO";
        public static String WEB_GRUPO = "WEB_GRUPO";
        public static String WEB_EDITARMOTIVO = "WEB_EDITARMOTIVO";
        public static String WEB_NUEVOMOTIVO = "WEB_NUEVOMOTIVO";
        public static String WEB_TIPOMOTIVO = "WEB_TIPOMOTIVO";
        public static String WEB_DETALLE_PRODUCTOS = "WEB_DETALLE_PRODUCTOS";

        public static String WEB_MANTENIMIENTOESTADO = "WEB_MANTENIMIENTOESTADO";
        public static String WEB_EDITARESTADO = "WEB_EDITARESTADO";
        public static String WEB_NUEVOESTADO = "WEB_NUEVOESTADO";

        public static String WEB_TELEFONO = "WEB_TELEFONO";
        public static String WEB_MODELO = "WEB_MODELO";
        public static String WEB_SENAL = "WEB_SENAL";
        public static String WEB_BATERIA = "WEB_BATERIA";
        public static String WEB_LATITUD = "WEB_LATITUD";
        public static String WEB_LONGITUD = "WEB_LONGITUD";
        public static String WEB_INFORMACION = "WEB_INFORMACION";

        public static String WEB_PUSH_MENSAJE_ENTRENGA = "WEB_PUSH_MENSAJE_ENTRENGA";
        public static String WEB_PUSH_MENSAJE_NOENTREGA = "WEB_PUSH_MENSAJE_NOENTREGA";
        public static String WEB_CARACTER_RESERVADO = "WEB_ERROR_CARACTERES_ESPECIALES";

        #endregion

        public static String WEB_CLIENTE_INVALIDO = "WEB_CLIENTE_INVALIDO";
        // Validando Ruta o Fecha al ingresar actividad
        public static String WEB_RUTA_FECHA = "WEB_RUTA_FECHA";
        public static String WEB_RUTA_BLANCO = "WEB_RUTA_BLANCO";
        public static String WEB_ORDEN_ERROR = "WEB_ORDEN_ERROR";
        public static String WEB_LIMITE_CREDITO = "WEB_LIMITE_CREDITO";
        public static String WEB_CREDITO_UTILIZADO = "WEB_CREDITO_UTILIZADO";

        //Otros 
        public static String WEB_GPSCLIENTE = "WEB_GPSCLIENTE";
        public static String WEB_FOTO = "WEB_FOTO";
        public static String WEB_LISTA = "WEB_LISTA";
        public static String WEB_TEXTO = "WEB_TEXTO";
        public static String WEB_ACTIVO = "WEB_ACTIVO";
        public static String WEB_NO_ACTIVO = "WEB_NO_ACTIVO";
        public static String WEB_TEXTO_INGRESO = "WEB_TEXTO_INGRESO";
        public static String WEB_ETIQUETA_INGRESO = "WEB_ETIQUETA_INGRESO";
        public static String WEB_RUTA = "WEB_RUTA";
        public static String WEB_DESDE = "WEB_DESDE";
        public static String WEB_HASTA = "WEB_HASTA";
        public static String WEB_ERROR_ETIQUETA = "WEB_ERROR_ETIQUETA";
        public static String WEB_CONFIGURACION_SELECCION = "WEB_CONFIGURACION_SELECCION";
        public static String WEB_CONFIGURACION_GENERAL = "WEB_CONFIGURACION_GENERAL";
        public static String WEB_GPS = "WEB_GPS";
        public static String WEB_CONFIGURACION_MOVIL = "WEB_CONFIGURACION_MOVIL";
        //  //  // CAM
        public static String WEB_RUTA_DIARIA = "WEB_RUTA_DIARIA";
        public static String WEB_REVISITAS = "WEB_REVISITAS";
        public static String WEB_TIPOLISTADO = "WEB_TIPOLISTADO";
        public static String WEB_ORDENAMIENTO = "WEB_ORDENAMIENTO";
        public static String WEB_ADICIONALES = "WEB_ADICIONALES";
        public static String WEB_PREGUNTA_CONFIRMACION = "WEB_PREGUNTA_CONFIRMACION";
        public static String WEB_GPSASISTIDO = "WEB_GPSASISTIDO";
        public static String WEB_CONFIGURACION_AUTOGENERADOS = "WEB_CONFIGURACION_AUTOGENERADOS";
        public static String WEB_ESTATICA = "WEB_ESTATICA";
        public static String WEB_DINAMICA = "WEB_DINAMICA";
        public static String WEB_SECUENCIAL = "WEB_SECUENCIAL";
        public static String WEB_BORRADO_MAESTRO = "WEB_BORRADO_MAESTRO";
        public static String WEB_ELIMINACIONMAESTROS_CORRECTA = "WEB_ELIMINACIONMAESTROS_CORRECTA";
        public static String WEB_TIPO_CARGA = "WEB_TIPO_CARGA";
        public static String WEB_ERROR_CARGA = "WEB_ERROR_CARGA";
        public static String WEB_FILENAME_CLIENTE = "WEB_FILENAME_CLIENTE";
        public static String WEB_FILENAME_FAMILIA = "WEB_FILENAME_FAMILIA";
        public static String WEB_FILENAME_MARCA= "WEB_FILENAME_MARCA";
        public static String WEB_FILENAME_FAMILIA_PRODUCTO = "WEB_FILENAME_FAMILIA_PRODUCTO";
        public static String WEB_FILENAME_MARCA_PRODUCTO = "WEB_FILENAME_MARCA_PRODUCTO";
        public static String WEB_FILENAME_DIRECCION = "WEB_FILENAME_DIRECCION";
        public static String WEB_FILENAME_USUARIO = "WEB_FILENAME_USUARIO";
        public static String WEB_FILENAME_MOTIVO = "WEB_FILENAME_MOTIVO";
        public static String WEB_FILENAME_ACTIVIDAD = "WEB_FILENAME_ACTIVIDAD";
        public static String WEB_FILENAME_PERFIL = "WEB_FILENAME_PERFIL";
        public static String WEB_FILENAME_TIPOACTIVIDAD = "WEB_FILENAME_TIPOACTIVIDAD";
        /// //////////////////////

        public static String WEB_MOTIVO = "WEB_MOTIVO";
        public static String WEB_CONFIGURACION = "WEB_CONFIGURACION";
        public static String WEB_CONFIGURACION_REGIONAL = "WEB_CONFIGURACION_REGIONAL";
        public static String WEB_IDIOMA = "WEB_IDIOMA";
        public static String WEB_SIGNO = "WEB_SIGNO";
        public static String WEB_FECHA_MMDDYY = "WEB_FECHA_MMDDYY";
        public static String WEB_FECHA_DDMMYY = "WEB_FECHA_DDMMYY";
        public static String WEB_ESPANOL = "WEB_ESPANOL";
        public static String WEB_INGLES = "WEB_INGLES";
        public static String WEB_PORTUGUES = "WEB_PORTUGUES";
        public static String WEB_DASHBOARD = "WEB_DASHBOARD";
        public static String WEB_LIMITE_INFERIOR = "WEB_LIMITE_INFERIOR";
        public static String WEB_LIMITE_MEDIO = "WEB_LIMITE_MEDIO";
        public static String WEB_LIMITE_SUPERIOR = "WEB_LIMITE_SUPERIOR";
        public static String WEB_NUMERO_VISITAS_PROGRAMADAS = "WEB_NUMERO_VISITAS_PROGRAMADAS";
        public static String WEB_NUMERO_MAXIMO_TIEMPO_INACTIVO = "WEB_NUMERO_MAXIMO_TIEMPO_INACTIVO";
        public static String WEB_NUMERO_ACTIVIDADES_PROGRAMADAS = "WEB_NUMERO_ACTIVIDADES_PROGRAMADAS";
        public static String WEB_INDICADOR = "WEB_INDICADOR";
        public static String WEB_VALOR = "WEB_VALOR";
        public static String WEB_COLOR = "WEB_COLOR";
        public static String WEB_MANTENIMIENTO_PARAMETRO_MONITOREO = "WEB_MANTENIMIENTO_PARAMETRO_MONITOREO";
        public static String WEB_EDITAR_PARAMETRO = "WEB_EDITAR_PARAMETRO";
        public static String WEB_CONFIGURACION_CORRECTA = "WEB_CONFIGURACION_CORRECTA";
        public static String WEB_MANTENIMIENTO_CONFIGURACION = "WEB_MANTENIMIENTO_CONFIGURACION";
        public static String WEB_TIPOACTIVIDADACTUAL = "WEB_TIPOACTIVIDADACTUAL";
        public static String WEB_PERFILACTUAL = "WEB_PERFILACTUAL";
        public static String WEB_USUARIOACTUAL = "WEB_USUARIOACTUAL";
        public static String WEB_ENTREGA = "WEB_ENTREGA";
        public static String WEB_NO_ENTREGA = "WEB_NO_ENTREGA";
        public static String WEB_REPORTE_MAPA = "WEB_REPORTE_MAPA";
        public static String WEB_CARACTERES_ACTIVIDAD = "WEB_CARACTERES_ACTIVIDAD";
        public static String WEB_ACTUALIZAR = "WEB_ACTUALIZAR";
        public static String WEB_INCLUYE_CELDA = "WEB_INCLUYE_CELDA";
        public static String WEB_TRACKING_USUARIO = "WEB_TRACKING_USUARIO";
        public static String WEB_CLIENTE = "WEB_CLIENTE";
        public static String WEB_CLIENTEACTUAL = "WEB_CLIENTEACTUAL";
        public static String WEB_CLIENTE_ESPECIAL = "WEB_CLIENTE_ESPECIAL";
        public static String WEB_MANTENIMIENTOCLIENTES = "WEB_MANTENIMIENTOCLIENTES";
        public static String WEB_MANTENIMIENTOCLIENTESDIRECCIONES = "WEB_MANTENIMIENTOCLIENTESDIRECCIONES";

        public static String WEB_DIRECCION = "WEB_DIRECCION";
        public static String WEB_DISPATCH = "WEB_DISPATCH";
        public static String WEB_MAPA = "WEB_MAPA";
        public static String WEB_GEOCERCA = "WEB_GEOCERCA";
        public static String WEB_GEOCERCA_CREACION = "WEB_GEOCERCA_CREACION";
        public static String WEB_UBICACION = "WEB_UBICACION";
        public static String WEB_GEOCERCA_GUARDAR = "WEB_GEOCERCA_GUARDAR";
        public static String WEB_GEOCERCA_BUSQUEDA = "WEB_GEOCERCA_BUSQUEDA";
        public static String WEB_ZONAS = "WEB_ZONAS";
        public static String WEB_LIMPIAR = "WEB_LIMPIAR";
        public static String WEB_POLIGONO = "WEB_POLIGONO";
        public static String WEB_PUNTOS = "WEB_PUNTOS";
        public static String WEB_OBTENER = "WEB_OBTENER";
        public static String WEB_DESCRIPCIONREPETIDO = "WEB_DESCRIPCIONREPETIDO";
        public static String WEB_ABRIRBUSCADOR = "WEB_ABRIRBUSCADOR";
        public static String WEB_INGRESECONCEPTO = "WEB_INGRESECONCEPTO";
        public static String WEB_PROCESO = "WEB_PROCESO";
        public static String WEB_FECHAMOVIL = "WEB_FECHAMOVIL";
        public static String WEB_SECUENCIA = "WEB_SECUENCIA";
        public static String WEB_ASISTENCIA = "WEB_ASISTENCIA";
        public static String WEB_CELDA = "WEB_CELDA";
        public static String WEB_SATELITE = "WEB_SATELITE";
        public static String WEB_LISTAUSUARIOS = "WEB_LISTAUSUARIOS";
        public static String WEB_PAPELERA = "WEB_PAPELERA";
        public static String WEB_NR0_ESTADO = "WEB_NR0_ESTADO";
        public static String WEB_NRO_REALIZADO = "WEB_NRO_REALIZADO";
        public static String WEB_PUNTO_CERCANO = "WEB_PUNTO_CERCANO";
        public static String WEB_FECHA_INICIO_REALIZADO = "WEB_FECHA_INICIO_REALIZADO";
        public static String WEB_FECHA_FIN_REALIZADO = "WEB_FECHA_FIN_REALIZADO";
        public static String WEB_OBSERVACION = "WEB_OBSERVACION";
        public static String WEB_UBIQUE_ARCHIVO = "WEB_UBIQUE_ARCHIVO";
        public static String WEB_SELECCIONE_ARCHIVO = "WEB_SELECCIONE_ARCHIVO";
        public static String WEB_CARGAR_ARCHIVOS = "WEB_CARGAR_ARCHIVOS";
        public static String WEB_EJECUTAR_ARCHIVOS = "WEB_EJECUTAR_ARCHIVOS";
        public static String WEB_CONFIGURACION_PAIS = "WEB_CONFIGURACION_PAIS";
        public static String WEB_GEOCERCA_NO_ENCONTRO_DIR = "WEB_GEOCERCA_NO_ENCONTRO_DIR";
        public static String WEB_EJECUTAR = "WEB_EJECUTAR";
        public static String WEB_AUMENTO_ARCHIVO = "WEB_AUMENTO_ARCHIVO";
        public static String WEB_REMOVIO_ARCHIVO = "WEB_REMOVIO_ARCHIVO";
        public static String WEB_NINGUN_ARCHIVO = "WEB_NINGUN_ARCHIVO";
        public static String WEB_ERROR_NOSELECCIONADO = "WEB_ERROR_NOSELECCIONADO";
        public static String WEB_ERROR_CARGANDO_ARCHIVO = "WEB_ERROR_CARGANDO_ARCHIVO";
        public static String WEB_CARGA_EXITO = "WEB_CARGA_EXITO";
        public static String WEB_ARCHIVO_NOEJECUTADO = "WEB_ARCHIVO_NOEJECUTADO";
        public static String WEB_ERROR_EJECUTAR_DTS = "WEB_ERROR_EJECUTAR_DTS";
        public static String WEB_NOEXISTE = "WEB_NOEXISTE";
        public static String WEB_ERRORDTS_VARIABLESGLOBALES = "WEB_ERRORDTS_VARIABLESGLOBALES";
        public static String WEB_ERROR_ARCHIVO = "WEB_ERROR_ARCHIVO";
        public static String WEB_OK_ARCHIVO = "WEB_OK_ARCHIVO";
        public static String WEB_REGISTROS_INSERTADOS = "WEB_REGISTROS_INSERTADOS";
        public static String WEB_REGISTROS_ACTUALIZADOS = "WEB_REGISTROS_ACTUALIZADOS";
        public static String WEB_DESCARGAR_INFORMACION = "WEB_DESCARGAR_INFORMACION";
        public static String WEB_DESCARGAR_FOTOS = "WEB_DESCARGAR_FOTOS";
        public static String WEB_FECHA_DESCARGA = "WEB_FECHA_DESCARGA";
        public static String WEB_DESCARGA_EXITOSA = "WEB_DESCARGA_EXITOSA";
        public static String WEB_NODATOS_DESCARGAR = "WEB_NODATOS_DESCARGAR";
        public static String WEB_ERROR_DESCARGA_DATOS = "WEB_ERROR_DESCARGA_DATOS";
        public static String WEB_ERROR_DATA = "WEB_ERROR_DATA";
        public static String WEB_SOLONUMEROS_USRJAVA = "WEB_SOLONUMEROS_USRJAVA";

        public static String WEB_VALORES_PARAMETRO = "WEB_VALORES_PARAMETRO";

        public static String WEB_POSITION_CONNECTION = "WEB_POSITION_CONNECTION";
        // VALUES
        public static String WEB_POSITION_WAITING_RESPONSE = "WEB_POSITION_WAITING_RESPONSE"; // -1
        public static String WEB_POSITION_NO_RESPONSE = "WEB_POSITION_NO_RESPONSE"; // -2
        public static String WEB_POSITION_RESPONSE_ERROR = "WEB_POSITION_RESPONSE_ERROR"; // -3
        public static String WEB_POSITION_RESPONSE_RESTRICTED = "WEB_POSITION_RESPONSE_RESTRICTED"; // -4

        public static String WEB_MANUALTECNICO = "WEB_MANUALTECNICO";
        public static String WEB_GENERADOR = "WEB_GENERADOR";
        public static String WEB_UTILITARIOS = "WEB_UTILITARIOS";

        public static String WEB_MANUALPROCEDIMIENTOS = "WEB_MANUALPROCEDIMIENTOS";
        public static String WEB_MIDDLEWARE = "WEB_MIDDLEWARE";
        public static String WEB_ERROR_EN_DATOS = "WEB_ERROR_EN_DATOS";
        public static String WEB_ACTIVIDADES_USER = "WEB_ACTIVIDADES_USER";
        public static String WEB_PROGRAMADAS = "WEB_PROGRAMADAS";
        public static String WEB_ACT_REALIZADAS = "WEB_ACT_REALIZADAS";
        public static String WEB_NOVISIT_REALIZADAS = "WEB_NOVISIT_REALIZADAS";
        public static String WEB_RUTA_MANUAL_USUARIO = "WEB_RUTA_MANUAL_USUARIO";
        public static String WEB_RUTA_MANUAL_TECNICO = "WEB_RUTA_MANUAL_TECNICO";
        public static String WEB_RUTA_GENERADOR = "WEB_RUTA_GENERADOR";
        public static String WEB_RUTA_MANUAL_PROCEDIMIENTOS = "WEB_RUTA_MANUAL_PROCEDIMIENTOS";
        public static String WEB_RUTA_MIDDLEWARE = "WEB_RUTA_MIDDLEWARE";

        // carga de datos

        public static String WEB_ARCHIVO = "WEB_ARCHIVO";
        public static String WEB_SUBIDOS = "WEB_SUBIDOS";
        public static String WEB_INSERTADOS = "WEB_INSERTADOS";
        public static String WEB_ACTUALIZADOS = "WEB_ACTUALIZADOS";
        public static String WEB_TOTAL_CARGADOS = "WEB_TOTAL_CARGADOS";
        public static String WEB_TOTAL_NO_CARGADOS = "WEB_TOTAL_NO_CARGADOS";
        public static String WEB_ARCHIVOS_EJECUTADO = "WEB_ARCHIVOS_EJECUTADO";
        public static String WEB_ERROR_ESTRUCTURA = "WEB_ERROR_ESTRUCTURA";
        public static String WEB_AGREGAR_ARCHIVO_MIN = "WEB_AGREGAR_ARCHIVO_MIN";
        public static String WEB_ERROR_SUBIENDO_ARCHI = "WEB_ERROR_SUBIENDO_ARCHI";
        public static String WEB_AGREGAR = "WEB_AGREGAR";
        public static String WEB_NUEVA_CARGA = "WEB_NUEVA_CARGA";
        public static String WEB_ERROR_NUM_COLUMNAS = "WEB_ERROR_NUM_COLUMNAS";
        public static String WEB_TIENE = "WEB_TIENE";
        public static String WEB_DEBE_TENER = "WEB_DEBE_TENER";
        public static String WEB_ERROR_INSERTANDO = "WEB_ERROR_INSERTANDO";
        public static String WEB_FORMATO_INVAL_ARCHIV = "WEB_FORMATO_INVAL_ARCHIV";
        public static String WEB_PORFAVOR_REVISARLO = "WEB_PORFAVOR_REVISARLO";
        public static String WEB_DETALLE_ERROR = "WEB_DETALLE_ERROR";
        public static String WEB_DATOS_INVALIDOS = "WEB_DATOS_INVALIDOS";
        public static String WEB_ERR_DATA = "WEB_ERR_DATA";
        public static String WEB_EJECUCION_EXITOSA = "WEB_EJECUCION_EXITOSA";
        public static String WEB_ERROR_EN_ARCHIVO = "WEB_ERROR_EN_ARCHIVO";

        // idioma de errores de carga 

        public static String WEB_LOGIN = "WEB_LOGIN";
        public static String WEB_NOMBRE_CLIENTE = "WEB_NOMBRE_CLIENTE";
        public static String WEB_CODIGO_GRUPO = "WEB_CODIGO_GRUPO";
        public static String WEB_FILA = "WEB_FILA";
        public static String WEB_ERR = "WEB_ERR";
        public static String WEB_ERR_NO_DATA = "WEB_ERR_NO_DATA";
        public static String WEB_FORMATO = "WEB_FORMATO";
        public static String WEB_ETIQUETADO_CORRECTA = "WEB_ETIQUETADO_CORRECTA";

        public const string TIPO_JAVA = "J";
        public const string TIPO_WEB = "W";

        public static String WEB_BUSCAR_LUPA = "WEB_BUSCAR_LUPA";
        public static String WEB_FECHA_VENCIMIENTO_CANJE = "WEB_FECHA_VENCIMIENTO_CANJE";
        public static String WEB_ELIMINAR_TODO = "WEB_ELIMINAR_TODO";
        public static String WEB_ELIMINAR_TODO_DATOS = "WEB_ELIMINAR_TODO_DATOS";
        public static String WEB_RESTAURAR_TODO_DATOS = "WEB_RESTAURAR_TODO_DATOS";
        public static String WEB_RESTAURAR_TODO = "WEB_RESTAURAR_TODO";

        public static String WEB_ERROR_CARACTERES_ESPECIALES = "WEB_ERROR_CARACTERES_ESPECIALES";

        public static String WEB_NO_INFORMACION = "WEB_NO_INFORMACION";
        public static String WEB_NO_UBICO = "WEB_NO_UBICO";
        public static String WEB_FACTURA = "WEB_FACTURA";
        public static String WEB_BOLETA = "WEB_BOLETA";
        public static String WEB_MONITOREO = "WEB_MONITOREO";
        public static String WEB_DETALLE_COBRANZA = "WEB_DETALLE_COBRANZA";
        public static String WEB_DETALLE_GENERAL = "WEB_DETALLE_GENERAL";
        public static String WEB_SIN_DATOS = "WEB_SIN_DATOS";

        public static String WEB_CONTROL_STOCK = "WEB_CONTROL_STOCK";
        public static String WEB_RESTRICTIVO = "WEB_RESTRICTIVO";
        public static String WEB_NO_RESTRICTIVO = "WEB_NO_RESTRICTIVO";
        public static String WEB_VALIDARSTOCK = "WEB_VALIDARSTOCK";
        public static String WEB_COBERTURA = "WEB_COBERTURA";
        public static String WEB_FUERACOBERTURA = "WEB_FUERACOBERTURA";
        public static String WEB_ENCOBERTURA = "WEB_ENCOBERTURA";
        public static String WEB_BONIFICACIONMANUAL = "WEB_BONIFICACIONMANUAL";
        public static String WEB_BONIFICACIONREP = "WEB_BONIFICACIONREP";
        public static String WEB_CONBONIFICACION = "WEB_CONBONIFICACION";
        public static String WEB_SINBONIFICACION = "WEB_SINBONIFICACION";
        public static String WEB_TOTALBONIFICACION = "WEB_TOTALBONIFICACION";
        public static String WEB_CODIGOPRODUCTO = "WEB_CODIGOPRODUCTO";
        public static String WEB_PROSPECTO = "WEB_PROSPECTO";

        #region equipo

        public static String WEB_EQUIPO = "WEB_EQUIPO";
        public static String WEB_REPORTE_EQUIPO = "WEB_REPORTE_EQUIPO";
        public static String WEB_ULT_FEC_REGISTRO = "WEB_ULT_FEC_REGISTRO";
        public static String WEB_ULT_POS_EQUIPO = "WEB_ULT_POS_EQUIPO";
        public static String WEB_NEXTEL = "WEB_NEXTEL";
        public static String WEB_PAGINACION = "WEB_PAGINACION";
        public static String WEB_SELE_ICONO_G = "WEB_SELE_ICONO_G";
        public static String WEB_DETALLEICONO = "WEB_DETALLEICONO";

        #endregion

        #region LOGIN

        public static string WEB_LOGIN_ERROR = "WEB_LOGIN_ERROR"; // nuevos
        public static string WEB_DISTRIBUIDORA = "WEB_DISTRIBUIDORA"; // nuevos

        #endregion;

        #region MASTERPAGE

        // /*- nuevos
        public static string WEB_NAV_MANTENIMIENTO = "WEB_NAV_MANTENIMIENTO";
        public static string WEB_NAV_CLIENTES = "WEB_NAV_CLIENTES";
        public static string WEB_NAV_RUTAS = "WEB_NAV_RUTAS";
        public static string WEB_NAV_USUARIOS = "WEB_NAV_USUARIOS";
        public static string WEB_NAV_PRODUCTOS = "WEB_NAV_PRODUCTOS";
        public static string WEB_NAV_LISTAPRECIOS = "WEB_NAV_LISTAPRECIOS";
        public static string WEB_NAV_CONFIGURACION = "WEB_NAV_CONFIGURACION";
        public static string WEB_NAV_REPORTES = "WEB_NAV_REPORTES";
        public static string WEB_NAV_PEDIDO = "WEB_NAV_PEDIDO";
        public static string WEB_NAV_NOPEDIDO = "WEB_NAV_NOPEDIDO";
        public static string WEB_NAV_DEVOLUCION = "WEB_NAV_DEVOLUCION";
        public static string WEB_NAV_CANJE = "WEB_NAV_CANJE";
        public static string WEB_NAV_COBRANZA = "WEB_NAV_COBRANZA";
        public static string WEB_NAV_PEDIDOTOTAL = "WEB_NAV_PEDIDOTOTAL";
        public static string WEB_NAV_GPSRUTA = "WEB_NAV_GPSRUTA";
        public static string WEB_NAV_CARGADESCARGA = "WEB_NAV_CARGADESCARGA";
        public static string WEB_NAV_CARGA = "WEB_NAV_CARGA";
        public static string WEB_NAV_DESCARGA = "WEB_NAV_DESCARGA";
        public static string WEB_NAV_CARGA_TAB = "WEB_NAV_CARGA_TAB";
        public static string WEB_NAV_GENERAL = "WEB_NAV_GENERAL";
        public static string WEB_NAV_QUIEBRESTOCK = "WEB_NAV_QUIEBRESTOCK";
        public static string WEB_NAV_RESERVA = "WEB_NAV_RESERVA";
        public static string WEB_NAV_GEOANALISIS = "WEB_NAV_GEOANALISIS";
        public static string WEB_GEOANALISIS_DETGEOANALISIS = "WEB_GEOANALISIS_DETGEOANALISIS";
        public static string WEB_MAPA_GEOANALISIS = "WEB_MAPA_GEOANALISIS";
        public static string WEB_MAPA_GEOANALISIS_DATO = "WEB_MAPA_GEOANALISIS_DATO";
        public static string WEB_MAPA_GRAFICOS = "WEB_MAPA_GRAFICOS";
        public static string WEB_NAV_GRAFICOS = "WEB_NAV_GRAFICOS";
        public static string WEB_GRAFICOS_DETGRAFICOS = "WEB_GRAFICOS_DETGRAFICOS";

        #endregion;

        #region HOME

        public static string WEB_MANUAL_IT = "WEB_MANUAL_IT";
        public static string WEB_EJEMPLO_XLS = "WEB_EJEMPLO_XLS";
        public static string WEB_RUTA_MANUAL = "WEB_RUTA_MANUAL";
        public static string WEB_RUTA_MANUAL_IT = "WEB_RUTA_MANUAL_IT";
        public static string WEB_GEN_ARCHIVOS = "WEB_GEN_ARCHIVOS";
        public static string WEB_SERVICIO_WINDOWS = "WEB_SERVICIO_WINDOWS";
        #endregion

        #region mantenimiento

        public static string WEB_CLI_DESHAB = "WEB_CLI_DESHAB";
        public static string WEB_TIPO = "WEB_TIPO";
        public static string WEB_TIPO_CANAL = "WEB_TIPO_CANAL";
        public static string WEB_SOLO_BANCO = "WEB_SOLO_BANCO";

        public static string WEB_GIRO = "WEB_GIRO";
        public static string WEB_EMPRESA = "WEB_EMPRESA";
        public static string WEB_CONDICION = "WEB_CONDICION";
        public static string WEB_MONTO = "WEB_MONTO";
        public static string WEB_DETALLE = "WEB_DETALLE";
        public static string WEB_MOTIVO_VALOR = "WEB_MOTIVO_VALOR";
        public static string WEB_CANTIDAD = "WEB_CANTIDAD";
        public static string WEB_DESCPORCENTAJE = "WEB_DESCPORCENTAJE";
        public static string WEB_DESC = "WEB_DESC";
        public static string WEB_GEORUTA = "WEB_GEORUTA";
        public static string WEB_BUSQUEDAGEORUTA = "WEB_BUSQUEDAGEORUTA";
        public static string WEB_BUSCADOR = "WEB_BUSCADOR";
        public static string WEB_VENDEDOR = "WEB_VENDEDOR";
        public static string WEB_FECHAVISITA = "WEB_FECHAVISITA";

        public static string WEB_REPORTEPEDIDOS = "WEB_REPORTEPEDIDOS";
        public static string WEB_REPORTENOPEDIDOS = "WEB_REPORTENOPEDIDOS";
        public static string WEB_REPORTEDEVOLUCION = "WEB_REPORTEDEVOLUCION";
        public static string WEB_REPORTECANJE = "WEB_REPORTECANJE";
        public static string WEB_REPORTEPEDIDOTOTAL = "WEB_REPORTEPEDIDOTOTAL";
        public static string WEB_MODULOCARGADESCARGA = "WEB_MODULOCARGADESCARGA";
        public static string WEB_MENSAJE_GUIA = "WEB_MENSAJE_GUIA";
        public static string WEB_CARGADETALLE = "WEB_CARGADETALLE";
        public static string WEB_DESCARGADETALLE = "WEB_DESCARGADETALLE";
        public static string WEB_GENERARREPORTE = "WEB_GENERARREPORTE";
        public static string WEB_GRAFICO = "WEB_GRAFICO";
        public static string WEB_MANTENIMIENTORUTAS = "WEB_MANTENIMIENTORUTAS";
        public static string WEB_MANTENIMIENTOPRODUCTOS = "WEB_MANTENIMIENTOPRODUCTOS";
        public static string WEB_MANTENIMIENTOFAMILIAPRODUCTOS = "WEB_MANTENIMIENTOFAMILIAPRODUCTOS";
        //@001 I
        public static string WEB_MANTENIMIENTOALMACEN = "WEB_MANTENIMIENTOALMACEN";
        public static string WEB_NUEVOALMACEN = "WEB_NUEVOALMACEN";
        public static string WEB_EDITARALMACEN = "WEB_EDITARALMACEN";
        //@001 F
        public static string WEB_REGISTROS = "WEB_REGISTROS";
        public static string WEB_ELIMINAR_REGISTRO = "WEB_ELIMINAR_REGISTRO";
        public static string WEB_NOMBRE_CORTO = "WEB_NOMBRE_CORTO";
        public static string WEB_ELIJAVALOR = "WEB_ELIJAVALOR";
        public static string WEB_EDITARLISTAPRECIO = "WEB_EDITARLISTAPRECIO";
        public static string WEB_MENSAJEDATOSGRABADOS = "WEB_MENSAJEDATOSGRABADOS";
        public static string WEB_MENSAJECODIGOYAREGISTRADO = "WEB_MENSAJECODIGOYAREGISTRADO";
        public static string WEB_MENSAJEDATOSINCORRECTOS = "WEB_MENSAJEDATOSINCORRECTOS";
        public static string WEB_MENSAJEDATOSINCOMPLETOS = "WEB_MENSAJEDATOSINCOMPLETOS";
        public static string WEB_MENSAJEFECHASINCORRECTOS = "WEB_MENSAJEFECHASINCORRECTOS";
        public static string WEB_MENSAJEDATOYAREGISTRADO = "WEB_MENSAJEDATOYAREGISTRADO";
        public static string WEB_MENSAJELOGINYAREGISTRADO = "WEB_MENSAJELOGINYAREGISTRADO";
        public static string WEB_MENSAJEDATOOBLIGATORIOSEL = "WEB_MENSAJEDATOOBLIGATORIOSEL";
        public static string WEB_MENSAJEDATOOBLIGATORIO = "WEB_MENSAJEDATOOBLIGATORIO";
        public static string WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE = "WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE";


        public static string WEB_MENSAJENOEXISTENDATOS = "WEB_MENSAJENOEXISTENDATOS";
        public static string WEB_NUEVOCLIENTEGUARDADO = "WEB_NUEVOCLIENTEGUARDADO";
        public static string WEB_NUEVARUTAGUARDADO = "WEB_NUEVARUTAGUARDADO";
        public static string WEB_NUEVOPRODUCTOGUARDADO = "WEB_NUEVOPRODUCTOGUARDADO";
        public static string WEB_NUEVOLISTAPRECIOGUARDADO = "WEB_NUEVOLISTAPRECIOGUARDADO";
        public static string WEB_NUEVOUSUARIOGUARDADO = "WEB_NUEVOUSUARIOGUARDADO";
        public static string WEB_NUEVOGENERALGUARDADO = "WEB_NUEVOGENERALGUARDADO";
        public static string WEB_CERRAR = "WEB_CERRAR";
        public static string WEB_MOSTRAR = "WEB_MOSTRAR";
        public static string WEB_SELECCIONE = "WEB_SELECCIONE";
        public static string WEB_MENSAJEERRORBD = "WEB_MENSAJEERRORBD";
        public static string WEB_MENSAJEREPITEASIG = "WEB_MENSAJEREPITEASIG";
        public static string WEB_USUARIOS_DESHA = "WEB_USUARIOS_DESHA";
        public static string WEB_PRODUCTO = "WEB_PRODUCTO";
        public static string WEB_CONDVTA = "WEB_CONDVTA";
        public static string WEB_CANAL = "WEB_CANAL";
        public static string WEB_LISTAPRECIO = "WEB_LISTAPRECIO";
        public static string WEB_STOCK = "WEB_STOCK";
        public static string WEB_STOCK_ALL = "WEB_STOCK_ALL";
        public static string WEB_PRECIO_VALIDAR = "WEB_PRECIO_VALIDAR";

        public static string WEB_PRECIOBASE = "WEB_PRECIOBASE";
        public static string WEB_UNIDADDEFECTO = "WEB_UNIDADDEFECTO";
        public static string WEB_UNIDADMEDIDA = "WEB_UNIDADMEDIDA";
        public static string WEB_PRODUCTOS_DESHA = "WEB_PRODUCTOS_DESHA";

        public static string WEB_PRECIO = "WEB_PRECIO";
        public static string WEB_COND_VTA = "WEB_COND_VTA";
        public static string WEB_PRECIO_POR = "WEB_PRECIO_POR";
        public static string WEB_PRECIO_EDITABLE = "WEB_PRECIO_EDITABLE";
        public static string WEB_TABLA = "WEB_TABLA";
        public static string WEB_ERRORGRABAR = "WEB_ERRORGRABAR";
        public static string WEB_VALIDARDESC = "WEB_VALIDARDESC";
        public static string WEB_CONFIGURACIONGUARDADO = "WEB_CONFIGURACIONGUARDADO";
        public static string WEB_DESCUENTOMIN = "WEB_DESCUENTOMIN";
        public static string WEB_DESCUENTOMAX = "WEB_DESCUENTOMAX";

        public static string WEB_SELECCIONECONFIGURACION = "WEB_SELECCIONECONFIGURACION";
        public static string WEB_CONFIG_GENERAL = "WEB_CONFIG_GENERAL";
        public static string WEB_CONFIG_MOVIL = "WEB_CONFIG_MOVIL";
        public static string WEB_CONFIG_PRECIO = "WEB_CONFIG_PRECIO";
        public static string WEB_CONFIG_DESCUENTO = "WEB_CONFIG_DESCUENTO";
        public static string WEB_CONFIG_SIMBOLO = "WEB_CONFIG_SIMBOLO";
        public static string WEB_CONFIG_GPSRUTA = "WEB_CONFIG_GPSRUTA";
        public static string WEB_CONFIG_LONGCODPROD = "WEB_CONFIG_LONGCODPROD";
        public static string WEB_CONFIG_EFICIENCIA = "WEB_CONFIG_EFICIENCIA";
        public static string WEB_CONFIG_CONDVTA = "WEB_CONFIG_CONDVTA";
        public static string WEB_CONFIG_PORCENTAJE = "WEB_CONFIG_PORCENTAJE";
        public static string WEB_CONFIG_MONTO = "WEB_CONFIG_MONTO";
        public static string WEB_CONFIG_RANGO_DESC = "WEB_CONFIG_RANGO_DESC";
        public static string WEB_CONFIG_RANGO_MIN = "WEB_CONFIG_RANGO_MIN";
        public static string WEB_CONFIG_RANGO_MAX = "WEB_CONFIG_RANGO_MAX";
        public static string WEB_CONFIG_TIPOCLI = "WEB_CONFIG_TIPOCLI";
        public static string WEB_CONFIG_MANT_DESCUENTO = "WEB_CONFIG_MANT_DESCUENTO";
        public static string WEB_CONFIG_CABECERA_DESCARGA = "WEB_CONFIG_CABECERA_DESCARGA";

        public static string WEB_CONFIG_PUNTO = "WEB_CONFIG_PUNTO";
        public static string WEB_CONFIG_COMA = "WEB_CONFIG_COMA";
        public static string WEB_CONFIG_MONEDAIDIOMA = "WEB_CONFIG_MONEDAIDIOMA";
        public static string WEB_CONFIG_MONEDA = "WEB_CONFIG_MONEDA";
        public static string WEB_CONFIG_IDIOMA = "WEB_CONFIG_IDIOMA";
        public static string WEB_CONFIG_CANTIDAD_DECIMALES_VISTA = "WEB_CONFIG_CANTIDAD_DECIMALES_VISTA";
        public static string WEB_CONFIG_CANTIDAD_DECIMALES_CARGA = "WEB_CONFIG_CANTIDAD_DECIMALES_CARGA";
        public static string WEB_CONFIG_DECIMAL = "WEB_CONFIG_DECIMAL";
        public static string WEB_FLAG_GENERADOR = "WEB_FLAG_GENERADOR";

        public static string WEB_DETALLECLIENTE = "WEB_DETALLECLIENTE";
        public static string WEB_DETALLERUTA = "WEB_DETALLERUTA";
        public static string WEB_DETALLEUSUARIO = "WEB_DETALLEUSUARIO";
        public static string WEB_DETALLEPRODUCTO = "WEB_DETALLEPRODUCTO";
        public static string WEB_DETALLELISTAPRECIO = "WEB_DETALLELISTAPRECIO";
        public static string WEB_DETALLECONFIGURACION = "WEB_DETALLECONFIGURACION";
        public static string WEB_PEDIDOSHOY = "WEB_PEDIDOSHOY";
        public static string WEB_COBRANZASHOY = "WEB_COBRANZASHOY";
        public static string WEB_REPORTECOBRANZA = "WEB_REPORTECOBRANZA";
        public static string WEB_PEDIDO_DETPEDIDO = "WEB_PEDIDO_DETPEDIDO";
        public static string WEB_NOPEDIDO_DETNOPEDIDO = "WEB_NOPEDIDO_DETNOPEDIDO";
        public static string WEB_DEVOLUCI_DETDEVOLUCI = "WEB_DEVOLUCI_DETDEVOLUCI";
        public static string WEB_CANJE_DETCANJE = "WEB_CANJE_DETCANJE";
        public static string WEB_COBRANZA_DETCOBRANZA = "WEB_COBRANZA_DETCOBRANZA";
        public static string WEB_PEDTOTAL_DETPEDTOTAL = "WEB_PEDTOTAL_DETPEDTOTAL";
        public static string WEB_RUTA_DETRUTA = "WEB_RUTA_DETRUTA";
        public static string WEB_CAMPOREQUERIDO = "WEB_CAMPOREQUERIDO";
        public static string WEB_FIN = "WEB_FIN";
        public static string WEB_DETALLEPEDIDO = "WEB_DETALLEPEDIDO";
        public static string WEB_DETALLE_PEDIDOS_EDITADOS = "WEB_DETALLE_PEDIDOS_EDITADOS";
        public static string WEB_PEDIDOS_EDITADOS = "WEB_PEDIDOS_EDITADOS";
        public static string WEB_TOTAL = "WEB_TOTAL";
        public static string WEB_TOTAL_VENTA = "WEB_TOTAL_VENTA";
        public static string WEB_PAGADO = "WEB_PAGADO";
        public static string WEB_SALDO = "WEB_SALDO";
        public static string WEB_DETALLECOBRANZA = "WEB_DETALLECOBRANZA";
        public static string WEB_REPORTEDEPEDIDOS = "WEB_REPORTEDEPEDIDOS";
        public static string WEB_REPORTEFAMILIA = "WEB_REPORTEFAMILIA";
        public static string WEB_REPORTEMARCA = "WEB_REPORTEMARCA";
        public static string WEB_REPORTEDECANJE = "WEB_REPORTEDECANJE";
        public static string WEB_REPORTEDECOBRANZA = "WEB_REPORTEDECOBRANZA";
        public static string WEB_REPORTEDEDEVOLUCION = "WEB_REPORTEDEDEVOLUCION";
        public static string WEB_REPORTEQUIEBRESTOCK = "WEB_REPORTEQUIEBRESTOCK";
        public static String WEB_REPORTERESERVA = "WEB_REPORTERESERVA";

        public static string WEB_REPORTEDENOPEDIDOS = "WEB_REPORTEDENOPEDIDOS";
        public static string WEB_REPORTEDEPEDIDOTOTAL = "WEB_REPORTEDEPEDIDOTOTAL";
        public static string WEB_RUTAVENDEDOR = "WEB_RUTAVENDEDOR";
        public static string WEB_NOTIENEGPS = "WEB_NOTIENEGPS";
        public static string WEB_SESSIONFINALIZADO = "WEB_SESSIONFINALIZADO";
        public static string WEB_NOPOSICIONESFILTRO = "WEB_NOPOSICIONESFILTRO";
        public static string WEB_NOLATLONREGISTRADA = "WEB_NOLATLONREGISTRADA";
        public static string WEB_NUMPEDIDOUPPERCASE = "WEB_NUMPEDIDOUPPERCASE";
        public static string WEB_CONDVENTAUPPERCASE = "WEB_CONDVENTAUPPERCASE";
        public static string WEB_NUMCANJEUPPERCASE = "WEB_NUMCANJEUPPERCASE";
        public static string WEB_NUMNOPEDIDOUPPERCASE = "WEB_NUMNOPEDIDOUPPERCASE";
        public static string WEB_NUMDEVOLUCIONUPPERCASE = "WEB_NUMDEVOLUCIONUPPERCASE";

        public static string WEB_PAGOSDECOBRANZA = "WEB_PAGOSDECOBRANZA";
        public static string WEB_VOUCHER = "WEB_VOUCHER";
        public static string WEB_BANCO = "WEB_BANCO";
        public static string WEB_TRACKINGCOBRANZA = "WEB_TRACKINGCOBRANZA";
        public static string WEB_TRACKINGPEDIDO = "WEB_TRACKINGPEDIDO";
        public static string WEB_TRACKINGCANJE = "WEB_TRACKINGCANJE";
        public static string WEB_TRACKINGNOPEDIDO = "WEB_TRACKINGNOPEDIDO";
        public static string WEB_TRACKINGDEVOLUCION = "WEB_TRACKINGDEVOLUCION";
        public static string WEB_MOTIVODEVOLUCION = "WEB_MOTIVODEVOLUCION";
        public static string WEB_MOTIVO_CANJE = "WEB_MOTIVO_CANJE";
        public static string WEB_MOTIVO_NOPEDIDO = "WEB_MOTIVO_NOPEDIDO";
        public static string WEB_IZQUIERDA = "WEB_IZQUIERDA";
        public static string WEB_MSJ_YAEXISTEZONADESCRIPCION = "WEB_MSJ_YAEXISTEZONADESCRIPCION";
        public static string WEB_TIPOCLIENTE = "WEB_TIPOCLIENTE";
        public static string WEB_MARCA = "WEB_MARCA";
        public static string WEB_GRUPO_ECONOMICO = "WEB_GRUPO_ECONOMICO";

        public static string WEB_GRAFICOPEDIDO = "WEB_GRAFICOPEDIDO";
        public static string WEB_PEDIDOSDELDIAMES = "WEB_PEDIDOSDELDIAMES";
        public static string WEB_TOPPRODUCTOSMES = "WEB_TOPPRODUCTOSMES";

        public static string WEB_VENTASULT30D = "WEB_VENTASULT30D";
        public static string WEB_CLIENTESMASVIS30D = "WEB_CLIENTESMASVIS30D";
        public static string WEB_GRAFICONOPEDIDO = "WEB_GRAFICONOPEDIDO";
        public static string WEB_NOPEDIDOSULT5D = "WEB_NOPEDIDOSULT5D";
        public static string WEB_CLIENTESMASNOPED30D = "WEB_CLIENTESMASNOPED30D";
        public static string WEB_GRAFICOCANJE = "WEB_GRAFICOCANJE";
        public static string WEB_CANJESULT5D = "WEB_CANJESULT5D";
        public static string WEB_CLIENTESMASCANJES30D = "WEB_CLIENTESMASCANJES30D";
        public static string WEB_GRAFICODEVOLUCION = "WEB_GRAFICODEVOLUCION";
        public static string WEB_PAGOSULT5D = "WEB_PAGOSULT5D";
        public static string WEB_CLIENTESMASPAGOS30D = "WEB_CLIENTESMASPAGOS30D";

        public static string WEB_GRAFICOCOBRANZA = "WEB_GRAFICOCOBRANZA";
        public static string WEB_DEVOLUCIONESULT5D = "WEB_DEVOLUCIONESULT5D";
        public static string WEB_CLIENTESMASDEV30D = "WEB_CLIENTESMASDEV30D";
        public static string WEB_EJECUCIONEXITO = "WEB_EJECUCIONEXITO";

        public static string WEB_MODULODE = "WEB_MODULODE";
        public static string WEB_VERGRAFICO = "WEB_VERGRAFICO";
        public static string WEB_NOPOSICIONESREGISTRADAS = "WEB_NOPOSICIONESREGISTRADAS";
        public static string WEB_MENSAJE_ACCIONREGISTROS = "WEB_MENSAJE_ACCIONREGISTROS";
        public static string WEB_MENSAJE_ACCIONREGISTRO = "WEB_MENSAJE_ACCIONREGISTRO";
        public static string WEB_MENSAJE_ACCION = "WEB_MENSAJE_ACCION";
        public static string WEB_NOCOBRANZASHOY = "WEB_NOCOBRANZASHOY";
        public static string WEB_NOCLIENTESVISITA = "WEB_NOCLIENTESVISITA";
        public static string WEB_CLIENTESCONPEDIDO = "WEB_CLIENTESCONPEDIDO";

        public static string WEB_ABRIRBARRA = "WEB_ABRIRBARRA";
        public static string WEB_DESCARGAEXITOSA = "WEB_DESCARGAEXITOSA";
        public static string WEB_CARGA_DETCARGA = "WEB_CARGA_DETCARGA";
        public static string WEB_DESCARGA_DETDESCARGA = "WEB_DESCARGA_DETDESCARGA";
        public static string WEB_CARGAR = "WEB_CARGAR";
        public static string WEB_DESCARGAR = "WEB_DESCARGAR";
        public static string WEB_SELECCIONAR_RUTA = "WEB_SELECCIONAR_RUTA";
        public static string WEB_CLAVE_NUMERICA = "WEB_CLAVE_NUMERICA";

        public static string WEB_MESES = "WEB_MESES";
        public static string WEB_DIAS = "WEB_DIAS";
        public static string WEB_MONT_PRODUCTO = "WEB_MONT_PRODUCTO";
        public static string WEB_MONT_VENDEDOR = "WEB_MONT_VENDEDOR";
        public static string WEB_MONT_CLIENTE = "WEB_MONT_CLIENTE";
        public static string WEB_VEND_MAS_VENTAS = "WEB_VEND_MAS_VENTAS";
        public static string WEB_CLI_PEDIDOS = "WEB_CLI_PEDIDOS";
        public static string WEB_SKU_MAS_ROTACION = "WEB_SKU_MAS_ROTACION";
        public static string WEB_PRO_MAS_VENDIDOS = "WEB_PRO_MAS_VENDIDOS";
        public static string WEB_VENTAS_TOTALES = "WEB_VENTAS_TOTALES";
        public static string WEB_COBRANZAS_TOTALES = "WEB_COBRANZAS_TOTALES";

        public static string WEB_GRAFICO_LINEA = "WEB_GRAFICO_LINEA";
        public static string WEB_GRAFICO_PIE = "WEB_GRAFICO_PIE";
        public static string WEB_GRAFICO_BARRAS = "WEB_GRAFICO_BARRAS";
        public static string WEB_SKU_DEMANDADOS = "WEB_SKU_DEMANDADOS";
        public static string WEB_SKU_MAS_VENDIDOS = "WEB_SKU_MAS_VENDIDOS";
        public static string WEB_CLI_PRO_MAS_VENDIDOS = "WEB_CLI_PRO_MAS_VENDIDOS";
        public static string WEB_CLIENTES_MAS_VENTAS = "WEB_CLIENTES_MAS_VENTAS";
        public static string WEB_MANUAL_PROCEDURES = "WEB_MANUAL_PROCEDURES";
        public static string WEB_GRAFICO_DONA = "WEB_GRAFICO_DONA";
        public static string WEB_CLIENTES_MAS_DEVOLUCIONES = "WEB_CLIENTES_MAS_DEVOLUCIONES";
        public static string WEB_CLIENTES_MAS_CANJES = "WEB_CLIENTES_MAS_CANJES";
        public static string WEB_INDICADOR_CIRCULAR = "WEB_INDICADOR_CIRCULAR";
        public static string WEB_INDICADOR_SEMICIRCULAR = "WEB_INDICADOR_SEMICIRCULAR";
        public static string WEB_INDICADOR_TERMOMETRO = "WEB_INDICADOR_TERMOMETRO";
        public static string WEB_META = "WEB_META";
        public static string WEB_OPERACION = "WEB_OPERACION";
        public static string WEB_UNIDADES_VENDIDAS = "WEB_UNIDADES_VENDIDAS";
        public static string WEB_SELECCIONAR = "WEB_SELECCIONAR";
        public static string WEB_VENTAS_TOTALES_PRODUCTO = "WEB_VENTAS_TOTALES_PRODUCTO";
        public static string WEB_UNIDADES = "WEB_UNIDADES";
        public static String WEB_RUTA_EXCEL_EJEMPLO = "WEB_RUTA_EXCEL_EJEMPLO";
        public static String WEB_RUTA_MANUAL_PROCEDURE = "WEB_RUTA_MANUAL_PROCEDURE";
        public static String WEB_RUTA_WIN_SERVICE = "WEB_RUTA_WIN_SERVICE";
        public static String WEB_RUTA_TERMINOS_Y_CONDICIONES = "WEB_RUTA_TERMINOS_Y_CONDICIONES";
        public static String WEB_TERMINOS_Y_CONDICIONES = "WEB_TERMINOS_Y_CONDICIONES";
        public static String WEB_FLAG_MOSTRARLOGOTOP = "WEB_FLAG_MOSTRARLOGOTOP";

        //***news***/// 

        public static String WEB_NIN_ARCHIVO_EJECUTADO = "WEB_NIN_ARCHIVO_EJECUTADO";
        public static String WEB_ARCHIVOS_EJECUTADO_CORRECT = "WEB_ARCHIVOS_EJECUTADO_CORRECT";
        public static String WEB_ERROR_ENCRIPTACION = "WEB_ERROR_ENCRIPTACION";
        public static String WEB_ARCHIVOS = "WEB_ARCHIVOS";
        public static String WEB_ESTADO = "WEB_ESTADO";
        public static String WEB_HISTORIAL_EDICION_PEDIDOS = "WEB_HISTORIAL_EDICION_PEDIDOS";
        public static String WEB_SUBIDO = "WEB_SUBIDO";
        public static String WEB_RUTA_ICONO_LOGO_SOLNEXTEL = "WEB_RUTA_ICONO_LOGO_SOLNEXTEL";
        public static String RUTA_ICONO_LOGONO_DEFAULT = "logono.jpg";
        public static String WEB_GEO_GRAFNPANT = "WEB_GEO_GRAFNPANT";
        public static String WEB_FILENAME_VENDEDOR = "WEB_FILENAME_VENDEDOR";
        public static String WEB_FILENAME_PRODUCTO = "WEB_FILENAME_PRODUCTO";
        public static String WEB_FILENAME_RUTA = "WEB_FILENAME_RUTA";
        public static String WEB_FILENAME_COBRANZA = "WEB_FILENAME_COBRANZA";
        public static String WEB_FILENAME_GENERAL = "WEB_FILENAME_GENERAL";
        public static String WEB_FILENAME_LISTAPRECIOS = "WEB_FILENAME_LISTAPRECIOS";
        //@002 I
        public static String WEB_FILENAME_ALMACEN = "WEB_FILENAME_ALMACEN";
        public static String WEB_FILENAME_ALMACENPRODUCTO = "WEB_FILENAME_ALMACENPRODUCTO";
        public static String WEB_REPORTECONSOLIDADO = "WEB_REPORTECONSOLIDADO";
        //@002 F

        public static String WEB_FILENAME_PRESENTACION = "WEB_FILENAME_PRESENTACION";

        #endregion

        #region new carga

        public static String WEB_NUMERO = "WEB_NUMERO";
        public static String WEB_TIPO_CLIENTE = "WEB_TIPO_CLIENTE";
        public static String WEB_CODIGO_CLIENTE = "WEB_CODIGO_CLIENTE";
        public static String WEB_CODIGO_VENDEDOR = "WEB_CODIGO_VENDEDOR";
        public static String WEB_TIPO_DOCUMENTO = "WEB_TIPO_DOCUMENTO";
        public static String WEB_MONTO_PAGADO = "WEB_MONTO_PAGADO";
        public static String WEB_MONTO_TOTAL = "WEB_MONTO_TOTAL";
        public static String WEB_FECHA_VENCIMIENTO = "WEB_FECHA_VENCIMIENTO";
        public static String WEB_SERIE = "WEB_SERIE";
        public static String WEB_FECHA_REGISTRO = "WEB_FECHA_REGISTRO";
        public static String WEB_DESCRIPCION_PRODUCTO = "WEB_DESCRIPCION_PRODUCTO";
        public static String WEB_DISTANCIA = "WEB_DISTANCIA";
        public static String WEB_ABREVIATURA = "WEB_ABREVIATURA";
        public static String WEB_CODIGO_CANAL = "WEB_CODIGO_CANAL";
        public static String WEB_CODIGO_PRODUCTO = "WEB_CODIGO_PRODUCTO";

        #endregion

        #region descarga nombre archivos
        public static string WEB_DESCARGA_PEDIDO = "WEB_DESCARGA_PEDIDO";
        public static string WEB_DESCARGA_DETALLE_PEDIDO = "WEB_DESCARGA_DETALLE_PEDIDO";
        public static string WEB_DESCARGA_NOVENTA = "WEB_DESCARGA_NOVENTA";
        public static string WEB_DESCARGA_CANJE = "WEB_DESCARGA_CANJE";
        public static string WEB_DESCARGA_DETALLE_CANJE = "WEB_DESCARGA_DETALLE_CANJE";
        public static string WEB_DESCARGA_DEVOLUCION = "WEB_DESCARGA_DEVOLUCION";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION = "WEB_DESCARGA_DETALLE_DEVOLUCION";
        public static string WEB_DESCARGA_PAGO = "WEB_DESCARGA_PAGO";
        #endregion

        #region descarga cabecera archivos
        public static string WEB_DESCARGA_PEDIDO_COL01 = "WEB_DESCARGA_PEDIDO_COL01";
        public static string WEB_DESCARGA_PEDIDO_COL02 = "WEB_DESCARGA_PEDIDO_COL02";
        public static string WEB_DESCARGA_PEDIDO_COL03 = "WEB_DESCARGA_PEDIDO_COL03";
        public static string WEB_DESCARGA_PEDIDO_COL04 = "WEB_DESCARGA_PEDIDO_COL04";
        public static string WEB_DESCARGA_PEDIDO_COL05 = "WEB_DESCARGA_PEDIDO_COL05";
        public static string WEB_DESCARGA_PEDIDO_COL06 = "WEB_DESCARGA_PEDIDO_COL06";
        public static string WEB_DESCARGA_PEDIDO_COL07 = "WEB_DESCARGA_PEDIDO_COL07";
        public static string WEB_DESCARGA_PEDIDO_COL08 = "WEB_DESCARGA_PEDIDO_COL08";//OBSERVACION
        public static string WEB_DESCARGA_PEDIDO_COL09 = "WEB_DESCARGA_PEDIDO_COL09";//LATITUD
        public static string WEB_DESCARGA_PEDIDO_COL10 = "WEB_DESCARGA_PEDIDO_COL10";//LONGITUD

        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL01 = "WEB_DESCARGA_DETALLE_PEDIDO_COL01";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL02 = "WEB_DESCARGA_DETALLE_PEDIDO_COL02";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL03 = "WEB_DESCARGA_DETALLE_PEDIDO_COL03";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL04 = "WEB_DESCARGA_DETALLE_PEDIDO_COL04";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL05 = "WEB_DESCARGA_DETALLE_PEDIDO_COL05";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL06 = "WEB_DESCARGA_DETALLE_PEDIDO_COL06";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL07 = "WEB_DESCARGA_DETALLE_PEDIDO_COL07";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL08 = "WEB_DESCARGA_DETALLE_PEDIDO_COL08";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL09 = "WEB_DESCARGA_DETALLE_PEDIDO_COL09";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL10 = "WEB_DESCARGA_DETALLE_PEDIDO_COL10";
        public static string WEB_DESCARGA_DETALLE_PEDIDO_COL11 = "WEB_DESCARGA_DETALLE_PEDIDO_COL11";

        public static string WEB_DESCARGA_DEVOLUCION_COL01 = "WEB_DESCARGA_DEVOLUCION_COL01";
        public static string WEB_DESCARGA_DEVOLUCION_COL02 = "WEB_DESCARGA_DEVOLUCION_COL02";
        public static string WEB_DESCARGA_DEVOLUCION_COL03 = "WEB_DESCARGA_DEVOLUCION_COL03";
        public static string WEB_DESCARGA_DEVOLUCION_COL04 = "WEB_DESCARGA_DEVOLUCION_COL04";
        public static string WEB_DESCARGA_DEVOLUCION_COL05 = "WEB_DESCARGA_DEVOLUCION_COL05";

        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL01 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL01";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL02 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL02";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL03 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL03";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL04 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL04";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL05 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL05";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL06 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL06";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL07 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL07";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL08 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL08";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL09 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL09";
        public static string WEB_DESCARGA_DETALLE_DEVOLUCION_COL10 = "WEB_DESCARGA_DETALLE_DEVOLUCION_COL10";

        public static string WEB_DESCARGA_CANJE_COL01 = "WEB_DESCARGA_CANJE_COL01";
        public static string WEB_DESCARGA_CANJE_COL02 = "WEB_DESCARGA_CANJE_COL02";
        public static string WEB_DESCARGA_CANJE_COL03 = "WEB_DESCARGA_CANJE_COL03";
        public static string WEB_DESCARGA_CANJE_COL04 = "WEB_DESCARGA_CANJE_COL04";
        public static string WEB_DESCARGA_CANJE_COL05 = "WEB_DESCARGA_CANJE_COL05";

        public static string WEB_DESCARGA_DETALLE_CANJE_COL01 = "WEB_DESCARGA_DETALLE_CANJE_COL01";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL02 = "WEB_DESCARGA_DETALLE_CANJE_COL02";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL03 = "WEB_DESCARGA_DETALLE_CANJE_COL03";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL04 = "WEB_DESCARGA_DETALLE_CANJE_COL04";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL05 = "WEB_DESCARGA_DETALLE_CANJE_COL05";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL06 = "WEB_DESCARGA_DETALLE_CANJE_COL06";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL07 = "WEB_DESCARGA_DETALLE_CANJE_COL07";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL08 = "WEB_DESCARGA_DETALLE_CANJE_COL08";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL09 = "WEB_DESCARGA_DETALLE_CANJE_COL09";
        public static string WEB_DESCARGA_DETALLE_CANJE_COL10 = "WEB_DESCARGA_DETALLE_CANJE_COL10";

        public static string WEB_DESCARGA_NOVENTA_COL01 = "WEB_DESCARGA_NOVENTA_COL01";
        public static string WEB_DESCARGA_NOVENTA_COL02 = "WEB_DESCARGA_NOVENTA_COL02";
        public static string WEB_DESCARGA_NOVENTA_COL03 = "WEB_DESCARGA_NOVENTA_COL03";
        public static string WEB_DESCARGA_NOVENTA_COL04 = "WEB_DESCARGA_NOVENTA_COL04";
        public static string WEB_DESCARGA_NOVENTA_COL05 = "WEB_DESCARGA_NOVENTA_COL05";
        public static string WEB_DESCARGA_NOVENTA_COL06 = "WEB_DESCARGA_NOVENTA_COL06";

        public static string WEB_DESCARGA_PAGO_COL01 = "WEB_DESCARGA_PAGO_COL01";
        public static string WEB_DESCARGA_PAGO_COL02 = "WEB_DESCARGA_PAGO_COL02";
        public static string WEB_DESCARGA_PAGO_COL03 = "WEB_DESCARGA_PAGO_COL03";
        public static string WEB_DESCARGA_PAGO_COL04 = "WEB_DESCARGA_PAGO_COL04";
        public static string WEB_DESCARGA_PAGO_COL05 = "WEB_DESCARGA_PAGO_COL05";
        public static string WEB_DESCARGA_PAGO_COL06 = "WEB_DESCARGA_PAGO_COL06";
        public static string WEB_DESCARGA_PAGO_COL07 = "WEB_DESCARGA_PAGO_COL07";
        public static string WEB_DESCARGA_PAGO_COL08 = "WEB_DESCARGA_PAGO_COL08";

        #endregion

        #region graficos
        public static String WEB_GRP_TOP_CLIENTES = "WEB_GRP_TOP_CLIENTES";
        public static String WEB_GRP_TOP_PRODUCTOS = "WEB_GRP_TOP_PRODUCTOS";
        public static String WEB_GRP_TOP_VENDEDORES = "WEB_GRP_TOP_VENDEDORES";

        public static String WEB_GRP_AVANCE = "WEB_GRP_AVANCE";
        public static String WEB_GRP_INDICADOR = "WEB_GRP_INDICADOR";
        public static String WEB_GRP_INDICADORPRODUCTO = "WEB_GRP_INDICADORPRODUCTO";
        public static String WEB_GRP_FILTROOPERACION = "WEB_GRP_FILTROOPERACION";
        public static String WEB_GRP_FILTROTIPO = "WEB_GRP_FILTROTIPO";
        public static String WEB_GRP_OPERACIONCANTIDAD = "WEB_GRP_OPERACIONCANTIDAD";
        public static String WEB_GRP_OPERACIONMONTO = "WEB_GRP_OPERACIONMONTO";
        public static String WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD = "WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRCLIENTE_PEDIDO_VS_MONTO = "WEB_GRCLIENTE_PEDIDO_VS_MONTO";
        public static String WEB_GRCLIENTE_COBRANZA_VS_MONTO = "WEB_GRCLIENTE_COBRANZA_VS_MONTO";
        public static String WEB_GRCLIENTE_CANJE_VS_CANTIDAD = "WEB_GRCLIENTE_CANJE_VS_CANTIDAD";
        public static String WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD = "WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD = "WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_PEDIDO_VS_MONTO = "WEB_GRPRODUCTO_PEDIDO_VS_MONTO";
        public static String WEB_GRPRODUCTO_CANJE_VS_CANTIDAD = "WEB_GRPRODUCTO_CANJE_VS_CANTIDAD";
        public static String WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD = "WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD = "WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_PEDIDO_VS_MONTO = "WEB_GRVENDEDOR_PEDIDO_VS_MONTO";
        public static String WEB_GRVENDEDOR_COBRANZA_VS_MONTO = "WEB_GRVENDEDOR_COBRANZA_VS_MONTO";
        public static String WEB_GRVENDEDOR_CANJE_VS_CANTIDAD = "WEB_GRVENDEDOR_CANJE_VS_CANTIDAD";
        public static String WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD = "WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD";
        public static String WEB_GRMAPA_CLIENTE_VS_CANTIDAD = "WEB_GRMAPA_CLIENTE_VS_CANTIDAD";
        public static String WEB_GRMAPA_CLIENTE_VS_MONTO = "WEB_GRMAPA_CLIENTE_VS_MONTO";
        public static String WEB_GRMAPA_PRODUCTO_VS_CANTIDAD = "WEB_GRMAPA_PRODUCTO_VS_CANTIDAD";
        public static String WEB_GRMAPA_PRODUCTO_VS_MONTO = "WEB_GRMAPA_PRODUCTO_VS_MONTO";
        public static String WEB_CODIGO_PEDIDO = "WEB_CODIGO_PEDIDO";
        public static String WEB_CODIGO_PEDIDO_EDITADO = "WEB_CODIGO_PEDIDO_EDITADO";
        public static String WEB_CODIGO_DETALLE = "WEB_CODIGO_DETALLE";                
        public static String WEB_ETIQUETA_PROCESADO = "WEB_ETIQUETA_PROCESADO";
        public static String WEB_ETIQUETA_NO_PROCESADO = "WEB_ETIQUETA_NO_PROCESADO";
        #endregion

        #region icono
        //public static String WEB_MANTENIMIENTO_ICONO = "WEB_MANTENIMIENTO_ICONO";
        //public static String WEB_ERROR_EXTENSION_ICONO = "WEB_ERROR_EXTENSION_ICONO";
        //public static String WEB_ERROR_TAMANIO_ICONO = "WEB_ERROR_TAMANIO_ICONO";
        //public static String WEB_ICONO_ACTUALIZADO_EXITOSAMENTE = "WEB_ICONO_ACTUALIZADO_EXITOSAMENTE";
        //public static String WEB_CONF_ICONO_GUARDADA = "WEB_CONF_ICONO_GUARDADA";
        //public static String WEB_ICONO = "WEB_ICONO";
        //public static String WEB_MOSTRAR_ICONO = "WEB_MOSTRAR_ICONO";
        //public static String WEB_SHOW = "WEB_SHOW";
        //public static String WEB_SUBIR_ICONO = "WEB_SUBIR_ICONO";


        #endregion


        public static Dictionary<string, string> datos = new Dictionary<string, string>();

        public static String getMensaje(String key)
        {
            if (datos.ContainsKey(key))
            {
                return datos[key].ToLower();
            }
            return "";
        }

        public static String getMensaje_Upper(String key)
        {
            if (datos.ContainsKey(key))
            {
                return datos[key].ToLower();
            }
            return "";
        }

        public static String getMensajeEncodeHTML_Upper(String key)
        {
            if (datos.ContainsKey(key))
            {
                return HttpUtility.HtmlEncode(datos[key].ToLower());
            }
            return "";
        }


        //aMendiola 07/10/2010
        public static String getMensajeEncodeHTML(String key)
        {
            if (datos.ContainsKey(key))
            {
                return HttpUtility.HtmlEncode(datos[key].ToLower());
            }
            return "";
        }

        private static string primeraLetraMayuscula(string palabra)
        {
            if (string.IsNullOrEmpty(palabra))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(palabra[0]) + palabra.Substring(1).ToLower();
        }

        public static String getMensajeEncodeHTML_FistLetterCapital(String key)
        {
            if (datos.ContainsKey(key))
            {
                return primeraLetraMayuscula(HttpUtility.HtmlEncode(datos[key]));

            }
            return "";
        }

        private static String JSEncode(String valor)
        {
            if (valor == null) { return ""; }
            String s1 = valor.Replace("\\", "\\\\");
            String s2 = s1.Replace("\n", "\\n");
            String s3 = s2.Replace("\r", "\\r");
            String s4 = s3.Replace("'", "\\'");
            return s4.Replace("\"", "\\\"");
        }

        public static String getMensajeEncodeJS(String key)
        {
            if (datos.ContainsKey(key))
            {
                return JSEncode(datos[key]);
            }
            return "";
        }

        public static String getMensajeEncodeJS_Upper(String key)
        {
            if (datos.ContainsKey(key))
            {
                return JSEncode(datos[key].ToUpper());
            }
            return "";
        }

        public static String getMensaje(String key, String code)
        {
            return getMensaje(key).Replace("{0}", code);
        }

        public static String getCodeCountry()
        {
            return getMensaje(CULTURE);
        }

        public static String getRutaIconoLogo()
        {
            String rutaLogo = getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_ICONO_LOGO).Trim();
            if (rutaLogo == String.Empty)
            {
                rutaLogo = RUTA_ICONO_LOGO_DEFAULT;
            }

            return rutaLogo;
        }

        public static String getRutaIconoLogoSolNextel()
        {
            String rutaLogo;
            try { rutaLogo = getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_ICONO_LOGO_SOLNEXTEL).Trim(); }
            catch { rutaLogo = RUTA_ICONO_LOGONO_DEFAULT; }

            return rutaLogo;
        }

        public static String getTituloGrafico(String strgrafico, String strOperacion, String strTipo)
        {

            String titulo = "";

            String datos = strgrafico.Trim() + "||" + strOperacion.Trim() + "||" + strTipo.Trim();

            switch (datos)
            {
                case "GR_TOP_CLIENTES||P||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD);
                    break;
                case "GR_TOP_CLIENTES||P||M":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_MONTO);
                    break;
                case "GR_TOP_CLIENTES||CO||M":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_COBRANZA_VS_MONTO);
                    break;
                case "GR_TOP_CLIENTES||C||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_CANJE_VS_CANTIDAD);
                    break;
                case "GR_TOP_CLIENTES||D||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD);
                    break;

                case "GR_TOP_PRODUCTOS||P||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_PEDIDO_VS_CANTIDAD);
                    break;
                case "GR_TOP_PRODUCTOS||P||M":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_PEDIDO_VS_MONTO);
                    break;
                case "GR_TOP_PRODUCTOS||C||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_CANJE_VS_CANTIDAD);
                    break;
                case "GR_TOP_PRODUCTOS||D||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRPRODUCTO_DEVOLUCION_VS_CANTIDAD);
                    break;


                case "GR_TOP_VENDEDORES||P||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD);
                    break;
                case "GR_TOP_VENDEDORES||P||M":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_MONTO);
                    break;
                case "GR_TOP_VENDEDORES||CO||M":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_COBRANZA_VS_MONTO);
                    break;
                case "GR_TOP_VENDEDORES||C||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_CANJE_VS_CANTIDAD);
                    break;
                case "GR_TOP_VENDEDORES||D||C":
                    titulo = getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD);
                    break;


            }


            return titulo;
        }

        public static String getmensajeGuia(String value)
        {
            String mensajeGuia = IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_GUIA);
            mensajeGuia = mensajeGuia.Replace("{0}", value);
            return mensajeGuia;
        }
        public static String getMensajeListaPrecioExistente()
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJE_COMBINACION_LISTAPRECIOEXISTENTE).ToUpper();
        }

        public static String getMensajeDuplicado(String value)
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJECODIGOYAREGISTRADO).Replace("{0}", value).ToUpper();
        }

        public static String getMensajeDatoDuplicadoEti(String value)
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJEDATOOBLIGATORIO).Replace("{0}", value).ToUpper();
        }

        public static String getMensajeDatoDuplicado(String value)
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJEDATOYAREGISTRADO).Replace("{0}", value).ToUpper();
        }

        public static String getMensajeDuplicadoLog(String value)
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJELOGINYAREGISTRADO).Replace("{0}", value).ToUpper();
        }

        public static String getmensajeDatoObligatorio(String value)
        {
            return IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJEDATOOBLIGATORIOSEL).Replace("{0}", value).ToUpper();
        }

        public static String getmensajeDuplicadoOrden(String value1, String value2)
        {
            String mensaje = "";

            mensaje = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MENSAJEDATOYAREGISTRADO).Replace("{0}", value1);
            mensaje = mensaje.Replace("{1}", value2);
            return mensaje.ToUpper();

        }

        public static String getMensajeEncodeHTML(String key, String code)
        {
            return getMensajeEncodeHTML(key).Replace("{0}", code);

        }
    }
}