﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class ReporteFamiliaBean
    {
        public string nombre { get; set; }
        public string cantidad { get; set; }
        public string monto { get; set; }
        public string monto_soles { get; set; }
        public string monto_dolares { get; set; }
    }
}
