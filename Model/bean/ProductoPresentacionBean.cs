﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class ProductoPresentacionBean
    {
        private String _CodigoPresentacion;
        private String _Nombre;
        private String _CodigoProducto;
        private String _Cantidad;
        private String _UnidaddeFrancionamieno;
        private String _PrecioPre;
        private String _PrecioBaseSoles;
        private String _PrecioBaseDolares;
        private String _Stock;
        private String _Descmin;
        private String _Descmax;
        private Int32 _Pagina;
        private Int32 _TotalFila;
        private String _NombreProducto;
        private String _Estado;
        private Int32 _Condicion;
        private String _Canal;
        private String _CodVenta;
        private String _Codigo;
        private String _CodigoPedido;
        private String _Descripcion;
        private String _Bonificacion;
        private String _NombrePesentacion;
        private String _Monto;
        private Int32 _Cod_Det_Ped_PK;
        private String _PRO_UNIDADDEFECTO;

        public String PRO_UNIDADDEFECTO
        {
            get { return _PRO_UNIDADDEFECTO; }
            set { _PRO_UNIDADDEFECTO = value; }
        }

        public Int32 Cod_Det_Ped_PK
        {
            get { return _Cod_Det_Ped_PK; }
            set { _Cod_Det_Ped_PK = value; }
        }

        public String Monto
        {
            get { return _Monto; }
            set { _Monto = value; }
        }

        public String NombrePesentacion
        {
            get { return _NombrePesentacion; }
            set { _NombrePesentacion = value; }
        }

        public String Bonificacion
        {
            get { return _Bonificacion; }
            set { _Bonificacion = value; }
        }

        public String Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        public String CodigoPedido
        {
            get { return _CodigoPedido; }
            set { _CodigoPedido = value; }
        }

        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String CodVenta
        {
            get { return _CodVenta; }
            set { _CodVenta = value; }
        }

        public String Canal
        {
            get { return _Canal; }
            set { _Canal = value; }
        }

        public Int32 Condicion
        {
            get { return _Condicion; }
            set { _Condicion = value; }
        }

        public String Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }

        public String NombreProducto
        {
            get { return _NombreProducto; }
            set { _NombreProducto = value; }
        }

public Int32 TotalFila
{
  get { return _TotalFila; }
  set { _TotalFila = value; }
}
public Int32 Pagina
{
  get { return _Pagina; }
  set { _Pagina = value; }
}

public String Descmax
{
  get { return _Descmax; }
  set { _Descmax = value; }
}
public String Descmin
{
  get { return _Descmin; }
  set { _Descmin = value; }
}

public String Stock
{
  get { return _Stock; }
  set { _Stock = value; }
}
public String PrecioPre
{
  get { return _PrecioPre; }
  set { _PrecioPre = value; }
}
public String PrecioBaseSoles
        {
            get { return _PrecioBaseSoles; }
            set { _PrecioBaseSoles = value; }
        }

        public String PrecioBaseDolares
        {
            get { return _PrecioBaseDolares; }
            set { _PrecioBaseDolares = value; }
        }

public String UnidaddeFrancionamieno
{
  get { return _UnidaddeFrancionamieno; }
  set { _UnidaddeFrancionamieno = value; }
}

public String Cantidad
{
  get { return _Cantidad; }
  set { _Cantidad = value; }
}

public String CodigoProducto
{
  get { return _CodigoProducto; }
  set { _CodigoProducto = value; }
}

        public String Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public String CodigoPresentacion
        {
            get { return _CodigoPresentacion; }
            set { _CodigoPresentacion = value; }
        }


        
        public ProductoPresentacionBean()
        {
                       _CodigoPresentacion=String.Empty;
                       _Nombre = String.Empty;
                       _CodigoProducto = String.Empty;
                       _Cantidad = String.Empty;
                       _UnidaddeFrancionamieno = String.Empty;
                       _PrecioPre = String.Empty;
                       _Stock = String.Empty;
                       _Descmin = String.Empty;
                       _Descmax = String.Empty;
                       _Pagina = 0;
                       _TotalFila =0;
                       _NombreProducto = String.Empty;
                       _Estado = String.Empty;
                       _Condicion =0;
                       _Canal = String.Empty;
                       _CodVenta = String.Empty;
                       _Codigo = String.Empty;
                       _CodigoPedido = String.Empty;
                       _Descripcion = String.Empty;
                       _Bonificacion = String.Empty;
                       _Monto = string.Empty;
                       _Cod_Det_Ped_PK = 0;
                       _PRO_UNIDADDEFECTO = String.Empty;
        }
    }
}
