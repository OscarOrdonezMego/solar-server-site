﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class GrupoBean
    {
        public String codigo { get; set; }
        public String nombre { get; set; }
        public List<OpcionBean> opciones { get; set; }
        private Int32 _grupo_PK;
        private String _Codigo_Grupo;

        public String Codigo_Grupo
        {
            get { return _Codigo_Grupo; }
            set { _Codigo_Grupo = value; }
        }

        public Int32 Grupo_PK
        {
            get { return _grupo_PK; }
            set { _grupo_PK = value; }
        }
        public GrupoBean()
        {
            _grupo_PK = 0;
            _Codigo_Grupo = String.Empty;
        }
    }


   public class OpcionBean
   {
       public String codigo { get; set; }
       public String nombre { get; set; }
       public OpcionBean()
       {
      

       }
   }
}
