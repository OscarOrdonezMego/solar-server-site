﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateProductoBean
    {
      public List<ProductoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateProductoBean()
        {
            lstResultados = new List<ProductoBean>();
            totalPages = 0;
            
        }

    }
}
