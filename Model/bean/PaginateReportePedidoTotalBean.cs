﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReportePedidoTotalBean
    {
      public List<ReportePedidoTotalBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReportePedidoTotalBean()
        {
            lstResultados = new List<ReportePedidoTotalBean>();
            totalPages = 0;
            
        }

    }
}
