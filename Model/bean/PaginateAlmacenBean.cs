﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateAlmacenBean
    {
        public List<AlmacenBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }

        public PaginateAlmacenBean()
        {
            lstResultados = new List<AlmacenBean>();
            totalPages = 0;
        }
    }
}