﻿using System;

namespace Model.bean
{
    public class TipoCambioBean
    {
        public int id { get; set; } //TC_PK
        public String codigo { get; set; } //TC_CODIGO
        public String valor { get; set; } //TC_VALOR
        public DateTime fecha { get; set; } //TC_FECHA
        public String flag { get; set; } //FLGENABLE

        public TipoCambioBean()
        {

        }
    }
}
