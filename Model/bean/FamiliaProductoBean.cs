﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

///<summary>
///@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
///</summary>

namespace Model.bean
{
    public class FamiliaProductoBean
    {




        public String codigo { get; set; }

        public String nombre { get; set; }
        public String id { get; set; }
        public String flag { get; set; }
        public String TotalProductos { get; set; }
               
        public FamiliaProductoBean()
        {
            id = "";
            codigo = "";
            nombre = "";
            flag = "";
        }

    }
}
