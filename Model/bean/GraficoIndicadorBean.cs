﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class GraficoIndicadorBean
    {
        public String total { get; set; }
        public String limiteinferior { get; set; }
        public String limitesuperior { get; set; }
        public String meta { get; set; }


        public GraficoIndicadorBean()
        {
            total = "0";
            limiteinferior = "";
            limitesuperior = "";
            meta = "";
        }

    }
}
