﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Model.bean
{
   public class ComboBean
    {
        public String codigo;
        public String nombre;
        public String flag;

        public ComboBean()
        {

        }

        public ComboBean(String _codigo, String _nombre)
        {
            codigo = _codigo;
            nombre = _nombre;
        }

        [XmlAttribute("CODIGO")]
        public String CODIGO
        {
            get { return codigo; }
            set { codigo = value; }
        }

        [XmlAttribute("NOMBRE")]
        public String NOMBRE
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [XmlAttribute("FLAG")]
        public String FLAG
        {
            get { return flag; }
            set { flag = value; }
        }
    }


   [Serializable]
   [XmlRoot("AutoGenBean")]
   public class AutoGenBean
   {
       private List<ComboBean> items;
       public AutoGenBean() { Items = new List<ComboBean>(); }
       [XmlElement("ComboBean")]
       public List<ComboBean> Items
       {
           get { return items; }
           set { items = value; }
       }
   }
}
