﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PermisosBean
    {
        public int idPermiso { get; set; }
        public int idOpcion { get; set; }
        public String NombreOpcion { get; set; }
        public String Url { get; set; }
        public String NombreCategoria { get; set; }
        public String Referencia { get; set; }
        public int Estado { get; set; }


        public PermisosBean()
        {
            idPermiso = 0;
            idOpcion = 0;
            NombreOpcion = "";
            Url = "";
            NombreCategoria = "";
            Referencia = "";
            Estado = 0;
        }
    }
}
