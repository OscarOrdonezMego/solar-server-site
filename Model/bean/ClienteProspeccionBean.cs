﻿using System;

namespace Model.bean
{
    public class ClienteProspeccionBean
    {
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String id { get; set; }
        public String flag { get; set; }
        public String direccion { get; set; }
        public String usercod { get; set; }
        public String vendedor { get; set; }
        public String giro { get; set; }
        public String tclicod { get; set; }
        public String tipocliente { get; set; }
        public String latitud { get; set; }
        public String longitud { get; set; }
        public String fecha { get; set; }
        public String observacion { get; set; }
        public String fechaMovil { get; set; }
        public String secuencia { get; set; }

        public String CampoAdicional1 { get; set; }
        public String CampoAdicional2 { get; set; }
        public String CampoAdicional3 { get; set; }
        public String CampoAdicional4 { get; set; }
        public String CampoAdicional5 { get; set; }
        

        public ClienteProspeccionBean()
        {
        }
    }
}
