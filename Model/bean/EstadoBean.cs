﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class EstadoBean
    {

       public String id { get; set; }
       public String codigo { get; set; }
       public String nombre { get; set; }
       public String flag { get; set; }
       public String descripcion { get; set; }
       public String flaglista { get; set; }
       public String flagtexto { get; set; }
       public String flagfoto { get; set; }
       public int orden { get; set; }
       public String etiqueta { get; set; }
       public String codigotipoactividad { get; set; }
       public String nombretipoactividad { get; set; }


       public String FLAGLISTA
       {
           get { return flaglista == "T" ? IdiomaCultura.getMensaje(IdiomaCultura.WEB_ACTIVO) : IdiomaCultura.getMensaje(IdiomaCultura.WEB_NO_ACTIVO); }
       }

       public String FLAGTEXTO
       {
           get { return flagtexto == "T" ? IdiomaCultura.getMensaje(IdiomaCultura.WEB_ACTIVO) : IdiomaCultura.getMensaje(IdiomaCultura.WEB_NO_ACTIVO); }
       }

       public String FLAGFOTO
       {
           get { return flagfoto == "T" ? IdiomaCultura.getMensaje(IdiomaCultura.WEB_ACTIVO) : IdiomaCultura.getMensaje(IdiomaCultura.WEB_NO_ACTIVO); }
       }

       public EstadoBean()
        {
        }
    }
}
