﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteCanjeBean
    {
      public List<ReporteCanjeBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteCanjeBean()
        {
            lstResultados = new List<ReporteCanjeBean>();
            totalPages = 0;
            
        }

    }
}
