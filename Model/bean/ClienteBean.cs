﻿using System;

/// <summary>
/// @001 GMC 12/04/2015 Ajustes para actualizar Campos Adicionales Cliente
/// </summary>
namespace Model.bean
{
    public class ClienteBean
    {
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String id { get; set; }
        public String flag { get; set; }
        public String direccion { get; set; }
        public String canal { get; set; }
        public String giro { get; set; }
        public String latitud { get; set; }
        public String secuencia { get; set; }
        public String longitud { get; set; }

        //@001 I
        public String CampoAdicional1 { get; set; }
        public String CampoAdicional2 { get; set; }
        public String CampoAdicional3 { get; set; }
        public String CampoAdicional4 { get; set; }
        public String CampoAdicional5 { get; set; }
        public String limiteCredito { get; set; }
        public String creditoUtilizado { get; set; }
        public String grupoeconomico { get; set; }

        public ClienteBean()
        {
        }
    }
}
