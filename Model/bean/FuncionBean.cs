﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class FuncionBean
    {
        private String _CodigoFuncion;
        private String _Habilitado;

        public String Habilitado
        {
            get { return _Habilitado; }
            set { _Habilitado = value; }
        }

        public String CodigoFuncion
        {
            get { return _CodigoFuncion; }
            set { _CodigoFuncion = value; }
        }
        public FuncionBean()
        {
            _CodigoFuncion = String.Empty;
            _Habilitado = String.Empty;
        }
    }
}
