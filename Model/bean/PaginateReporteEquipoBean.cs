﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteEquipoBean
    {
      public List<ReporteEquipoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteEquipoBean()
        {
            lstResultados = new List<ReporteEquipoBean>();
            totalPages = 0;
            
        }

    }
}
