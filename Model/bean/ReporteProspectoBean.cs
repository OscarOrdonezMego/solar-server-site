﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class ReporteProspectoBean
    {
        public String codigo { get; set; }
       public String nombre { get; set; }
       public String id { get; set; }
       public String flag { get; set; }
       public String direccion { get; set; }
       public String vendedor { get; set; }
       public String canal { get; set; }
       public String giro { get; set; }
       public String latitud { get; set; }
       public String secuencia { get; set; }
       public String longitud { get; set; }

       public String CampoAdicional1 { get; set; }
       public String CampoAdicional2 { get; set; }
       public String CampoAdicional3 { get; set; }
       public String CampoAdicional4 { get; set; }
       public String CampoAdicional5 { get; set; }
       public String limiteCredito{ get; set; }
       public String creditoUtilizado { get; set; }

       public String CTRLALFANUMERICO { get; set; }
       public String CTRLCHECKBOX { get; set; }
       public String CTRLCOMBOBOX { get; set; }
       public String CTRLNUMERICO { get; set; }
       public String CTRLFECHA { get; set; }
       public String CTRLDECIMAL { get; set; }
       public String CTRLHORA { get; set; }
       public String CTRLRADIOBUTTON { get; set; }

        public String fechaRegistro { get; set; }

        public ReporteProspectoBean()
        {
        }
    }
}