﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class CobranzaBean
    {
        public String id { get; set; }
         public String cobcodigo { get; set; }
         public String vencodigo { get; set; }
        public String clicodigo { get; set; }
        public String clinombre { get; set; }
        public String vennombre { get; set; }
        public String clinumdocumento { get; set; }
        public String numdocumento { get; set; }
        public String tipodocumento { get; set; }
        public String montopagado { get; set; }
        public String montototal { get; set; }
        public String fecvencimiento { get; set; }
        public String serie { get; set; }
        public String montopagadosoles { get; set; }
        public String montototalsoles { get; set; }
        public String montopagadodolares { get; set; }
        public String montototaldolares { get; set; }
        private String _NOMBREGRUPO;

        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }

        public CobranzaBean()
        {
            _NOMBREGRUPO = String.Empty;
        }
    }
}
