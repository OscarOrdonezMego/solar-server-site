﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateMantGeneralBean
    {
        public List<MantGeneralBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateMantGeneralBean()
        {
            lstResultados = new List<MantGeneralBean>();
            totalPages = 0;
            
        }
    }
}
