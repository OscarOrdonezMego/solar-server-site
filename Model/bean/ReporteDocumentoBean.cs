﻿using System;

namespace Model.bean
{
    public class ReporteDocumentoBean
    {
        public String id { get; set; }
        public String doc_fecregistro { get; set; }
        public String doc_tipo { get; set; }
        public String gru_nombre { get; set; }
        public String usr_pk { get; set; }
        public String usr_codigo { get; set; }
        public String nom_usuario { get; set; }
        public String cli_codigo { get; set; }
        public String cli_nombre { get; set; }
        public String serie { get; set; }
        public String correlativo { get; set; }
        public String pag_codigo { get; set; }
        public String pag_montosoles { get; set; }
        public String pag_montodolares { get; set; }
        public String latitud { get; set; }
        public String longitud { get; set; }
    }
}
