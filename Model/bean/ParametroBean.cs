﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ParametroBean
    {

       public String idParametro { get; set; }
       public String descripcion { get; set; }
       public String valor { get; set; }
       public String color { get; set; }

       public ParametroBean()
        {
        }
    }
}
