﻿using System;
using System.Collections.Generic;

namespace Model.bean
{
    public class PaginateReporteDocumentoBean
    {
        public List<ReporteDocumentoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteDocumentoBean()
        {
            lstResultados = new List<ReporteDocumentoBean>();
            totalPages = 0;

        }
    }
}
