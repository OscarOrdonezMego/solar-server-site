﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class GraficoTopProductosBean
    {
        public String avrproducto { get; set; }
        public String producto { get; set; }
        public String total { get; set; }


        public GraficoTopProductosBean()
        {
            avrproducto = "";
            producto = "";
            total = "";
        }

    }
}
