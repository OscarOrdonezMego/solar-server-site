﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteDevolucionBean
    {
      public List<ReporteDevolucionBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteDevolucionBean()
        {
            lstResultados = new List<ReporteDevolucionBean>();
            totalPages = 0;
            
        }

    }
}
