﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public  class PaginateReporteFamiliaBean
    {
         
      public List<ReporteFamiliaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }
        public String totalmonto { get; set; }
        public String totalmontosoles { get; set; }
        public String totalmontodolares { get; set; }
        public String totalcantidad { get; set; }
        public String totalfamilias { get; set; }
        public PaginateReporteFamiliaBean()
        {
            lstResultados = new List<ReporteFamiliaBean>();
            totalPages = 0;
            totalmonto = "0";
            totalmontosoles = "0";
            totalmontodolares = "0";
            totalcantidad = "0";
            totalfamilias = "0";
        }

    }
}

