﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// @001 GMC 16/04/2015 Se agrega campo Direccion
/// </summary>

namespace Model.bean
{
    public class ReporteNoPedidoBean
    {

        public String id { get; set; }
        public String ped_fecinicio { get; set; }
        public String ped_fecfin { get; set; }
        public String ped_fecregistro { get; set; }
        public String ped_condvta { get; set; }
        public String cli_codigo { get; set; }
        public String ped_nopedido { get; set; }
        public String ped_montototal { get; set; }
        public String usr_pk { get; set; }
        public String usr_codigo { get; set; }
        public String latitud { get; set; }
        public String longitud { get; set; }
        public String nom_usuario { get; set; }
        public String usr_nombre { get; set; }
        public String condvta_nombre { get; set; }
        public String cli_nombre { get; set; }
        public String direccion { get; set; }
        public String observacion { get; set; }
        public String cli_lat { get; set; }
        public String cli_lon { get; set; }
        public String distancia { get; set; }
        public String mot_nombre { get; set; }
        public String habilitado { get; set; }
        public String FlgEnCobertura { get; set; }
        public String PED_DIRECCION { get; set; } //@001 I/F

        private String _NOMBREGRUPO;

        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }
        public ReporteNoPedidoBean()
        {
            _NOMBREGRUPO = String.Empty;
        }
    }
}