﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateVerReporteBean
    {
      public List<VerReporteBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateVerReporteBean()
        {
            lstResultados = new List<VerReporteBean>();
            totalPages = 0;
            
        }

    }
}
