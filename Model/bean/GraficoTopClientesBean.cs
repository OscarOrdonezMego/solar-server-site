﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class GraficoTopClientesBean
    {
        public String avrcliente { get; set; }
        public String cliente { get; set; }
        public String total { get; set; }


        public GraficoTopClientesBean()
        {
            avrcliente = "0";
            cliente = "";
            total = "";
        }

    }
}
