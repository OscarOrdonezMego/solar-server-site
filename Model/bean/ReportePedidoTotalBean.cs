﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReportePedidoTotalBean
    {

       public String id  { get; set; }
       public String NOM_USUARIO { get; set; }
       public String CLI_NOMBRE { get; set; }
       public String CLI_CODIGO { get; set; }
       public String CONDVTA_NOMBRE { get; set; }
       public String PED_FECINICIO { get; set; }
       public String PED_FECFIN { get; set; }
       public String PED_MONTOTOTAL { get; set; }
        public String PED_MONTOTOTAL_SOLES { get; set; }
        public String PED_MONTOTOTAL_DOLARES { get; set; }
        public String PRO_CODIGO { get; set; }
       public String PRO_NOMBRE { get; set; }
       public String DET_CANTIDAD { get; set; }
       public String DET_CANTIDAD_PRESENTACION { get; set; }
       public String DET_PRECIOBASE { get; set; }
        public String DET_PRECIOBASE_SOLES { get; set; }
        public String DET_PRECIOBASE_DOLARES { get; set; }
        public String DESCUENTO { get; set; }
       public String DET_MONTO { get; set; }
        public String DET_MONTO_SOLES { get; set; }
        public String DET_MONTO_DOLARES { get; set; }
        public String DESCRIPCION { get; set; }
       public String PED_OBSERVACION { get; set; }
       public String LATITUD { get; set; }
       public String LONGITUD { get; set; }
       public String CLI_LAT { get; set; }
       public String CLI_LON { get; set; }
       public String DISTANCIA { get; set; }
       public String BONIFICACION { get; set; }
       public String DET_BONIFICACION { get; set; }
       public String tieneBonificacion { get; set; }
       private String _FLETE_TOTAL;
       private String _MONTO_TOTAL_FLETE;
       private String _DIRECCION;
       private String _HABILITADO;
       private String _TIPO_NOMBRE;
       private String _NOMBREGRUPO;
       public String PED_PK { get; set; }
       public String NOMBRE_PRESENTACION { get; set; }
       public String UNIDAD_FRACCIONAMIENTO { get; set; }
       public String DET_CANTIDAD_FRACCION { get; set; }

        public String NOMBREGRUPO
       {
           get { return _NOMBREGRUPO; }
           set { _NOMBREGRUPO = value; }
       }

       public String TIPO_NOMBRE
       {
           get { return _TIPO_NOMBRE; }
           set { _TIPO_NOMBRE = value; }
       }


       public String HABILITADO
       {
           get { return _HABILITADO; }
           set { _HABILITADO = value; }
       }


       public String DIRECCION
       {
           get { return _DIRECCION; }
           set { _DIRECCION = value; }
       }

       public String MONTO_TOTAL_FLETE
       {
           get { return _MONTO_TOTAL_FLETE; }
           set { _MONTO_TOTAL_FLETE = value; }
       }

       public String FLETE_TOTAL
       {
           get { return _FLETE_TOTAL; }
           set { _FLETE_TOTAL = value; }
       }
       public ReportePedidoTotalBean()
        {
            _FLETE_TOTAL = String.Empty;
            _MONTO_TOTAL_FLETE = String.Empty;
            _DIRECCION = String.Empty;
            _HABILITADO = String.Empty;
            _TIPO_NOMBRE = String.Empty;
            _NOMBREGRUPO = String.Empty;
            PED_PK = String.Empty;
        }
    }
}
