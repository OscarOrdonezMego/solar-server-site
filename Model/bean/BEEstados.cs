﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{


    public class BETipoActividad
    {
        public String id { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String perfil { get; set; }
        public String adicional1 { get; set; }
        public String adicional2 { get; set; }
        public String adicional3 { get; set; }
        public String adicional4 { get; set; }
        public String adicional5 { get; set; }

        public List<BEEstados> estados { get; set; }
        public BETipoActividad()
        {
        }
    }


  public  class BEEstados
    {

      public String codigo { get; set; }
      public List<BEControl> controles { get; set; }
     
      
      
      public BEEstados()
      {
      }
    }


  public class BEControl
  {
      public String tipo { get; set; }
      public String max { get; set; }
      public String obligatorio { get; set; }
      public String grupo { get; set; }
      public String grupoDesc { get; set; }
      public String etiqueta { get; set; }
      public String idEstControl { get; set; }
      public String editable { get; set; }
      public BEControl()
      {
      }
  }
}
