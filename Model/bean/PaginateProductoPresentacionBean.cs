﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateProductoPresentacionBean
    {
        private List<ProductoPresentacionBean> _loLstListaProductoPresentacionBean;
        private Int32 _TotalFila;

        public Int32 TotalFila
        {
            get { return _TotalFila; }
            set { _TotalFila = value; }
        }

        public List<ProductoPresentacionBean> LoLstListaProductoPresentacionBean
        {
            get { return _loLstListaProductoPresentacionBean; }
            set { _loLstListaProductoPresentacionBean = value; }
        }
        public PaginateProductoPresentacionBean()
        {
            _loLstListaProductoPresentacionBean = new List<ProductoPresentacionBean>();
            _TotalFila = 0;
        }
    }
}
