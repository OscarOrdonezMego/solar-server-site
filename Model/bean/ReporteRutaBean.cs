﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteRutaBean
    {


       public String TIPO  { get; set; }
       public String GPSLATITUD { get; set; }
       public String GPSLONGITUD { get; set; }
       public String FECHA { get; set; }
       public String CLIENTE { get; set; }
       public String CLI_LATITUD { get; set; }
       public String CLI_LONGITUD { get; set; }
       public String VENDEDOR { get; set; }
       public String DETALLE { get; set; }
       public String DIRECCION { get; set; }

       public ReporteRutaBean()
        { 
         }
    }
}
