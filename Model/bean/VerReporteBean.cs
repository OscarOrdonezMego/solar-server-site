﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class VerReporteBean
    {
        public String plantilla { get; set; }
        public String codregistro { get; set; }
        public String FECHAINICIO { get; set; }
        public String FECHAFIN { get; set; }
        public String PERFIL { get; set; }
        public String CLIENTE { get; set; }
        public String USUARIO { get; set; }
        public String FECHAESTADO { get; set; }
        public String NOVISITA { get; set; }
        public String ESTADOASIG{ get; set; }
        public String ESTADOREAL { get; set; }
        public String TIPOACTIVIDAD { get; set; }
        public String ESTADO { get; set; }
        public String ORDEN{ get; set; }
        public String CODIGO { get; set; }
        public String SITUACIONRUTA { get; set; }
        public String TIPO{ get; set; }
        public String LATITUD{ get; set; }
        public String LONGITUD{ get; set; }
        public String ID{ get; set; }
        public String ETIQUETA { get; set; }
        public String VALOR { get; set; }


        public VerReporteBean()
        {
        }
    }
}
