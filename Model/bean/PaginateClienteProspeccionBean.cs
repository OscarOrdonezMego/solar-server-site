﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateClienteProspeccionBean
    {
      public List<ClienteProspeccionBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateClienteProspeccionBean()
        {
            lstResultados = new List<ClienteProspeccionBean>();
            totalPages = 0;
            
        }

    }
}
