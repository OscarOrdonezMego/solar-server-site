﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateActividadBean
    {
      public List<ActividadBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateActividadBean()
        {
            lstResultados = new List<ActividadBean>();
            totalPages = 0;
            
        }

    }
}
