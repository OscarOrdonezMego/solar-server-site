﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PresentacionBean
    {
        private String _Cantidad;
        private String _UnidadFrancionamiento;
        private Int32 _Total;
        private String _Resulado;
        private String _PRO_UNIDADDEFECTO;
        private String _Stock;
        private String _Nombre;
        private String _Prepk;
        private String _CODIGOPRE;

        public String CODIGOPRE
        {
            get { return _CODIGOPRE; }
            set { _CODIGOPRE = value; }
        }

        public String Prepk
        {
            get { return _Prepk; }
            set { _Prepk = value; }
        }

        public String Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
      
        public String Stock
        {
            get { return _Stock; }
            set { _Stock = value; }
        }


        public String PRO_UNIDADDEFECTO
        {
            get { return _PRO_UNIDADDEFECTO; }
            set { _PRO_UNIDADDEFECTO = value; }
        }
        public String Resulado
        {
            get { return _Resulado; }
            set { _Resulado = value; }
        }

        public Int32 Total
        {
            get { return _Total; }
            set { _Total = value; }
        }

        public String UnidadFrancionamiento
        {
            get { return _UnidadFrancionamiento; }
            set { _UnidadFrancionamiento = value; }
        }

        public String Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
        public PresentacionBean()
        {
            _Cantidad = String.Empty;
            _UnidadFrancionamiento = String.Empty;
            _Total = 0;
            _Resulado = String.Empty;
            _PRO_UNIDADDEFECTO = String.Empty;
            _Stock = String.Empty;
            _Nombre = String.Empty;
            _Prepk = String.Empty;
            _CODIGOPRE = String.Empty;
        }
    }
}
