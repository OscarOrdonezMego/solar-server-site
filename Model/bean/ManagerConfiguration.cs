﻿using System;
using System.Collections;
using System.Configuration;

namespace Model.bean
{
    public class ManagerConfiguration
    {
            
        private static Hashtable _HashConfig = new Hashtable();
        private static String _Version = "0.0.0";

        public static String Version
        {
            set { _Version = value; }
            get { return _Version; }
        }

        public static Hashtable HashConfig
        {
            set { _HashConfig = value; }
            get { return _HashConfig; }
        }
            

        public static String pedido = "1";
        public static String devolucion = "1";
        public static String canje = "1";
        public static String cobranza = "1";
        public static String gps = "1";
        public static String eficiencia = "1";
        public static String nServices = "F";//F
        public static String nServicesEmbebido = "F"; //F

        public static String stock_linea = "1";
        public static String stock_validar = "1";
        public static String stock_descontar = "1";
        public static String precio_validar = "1";
        public static String descuento = "1";
        public static String precio_editable = "1";
        public static String precio_tipocli = "1";
        public static String precio_condvta = "1";
        public static String mostrar_condvta = "1";
        public static String precio_por = "C";
        public static String cabecera_descarga = "1";
        public static String decimal_carga = "2";
        public static String decimal_vista = "2";
        public static String descuento_porcentaje = "1";
        public static String descuento_monto = "0";
        public static String descuento_minimo = "0";
        public static String descuento_maximo = "0";

        public static String moneda = "";
        public static String moneda_dolares = "";
        public static String simbolo_decimal = "";
        public static String idioma = "";
        public static String dashboard = "";
        public static String tema = "";


        public static String almacen = "";
        public static String fraccionamiento_simple = "";
        public static String fraccionamiento_multiple = "";

        public static String java_addpedido_stock = "0";
        public static String carga_cobranza_fecharegistro = "1";
        public static String descarga_columnaempresa = "1";
        public  static String descarga_barrafinal = "1";
        
        public static String nValidarstock = "0";
        public static String nRestrictivo = "0";
        public static String nCobertura = "1";
        public static String nBonificacionManual = "1";
        
        public static String grafico;
        public static String posicion;
        public static String paginacion;
        public static String longitudcodigoproducto;
        public static String icono = "0";
        public static String NAMEICON = "LOGO.JPG";
        public static String rutaIcono = "/Mantenimiento/icono/";

        public static String laltitudlongitud = "-12.044425642104926,-77.04471575634764"; // ubicacion defecto en Perú

        // si no existe valor de las rutas, se muestran las por defecto
        public static String rutaDescarga = "http://demosdata.entel.pe/descargas/";
        public static String rutaManualUsuario = "estandares/Iorder/Manual_Usuario.zip";
        public static String rutaExcel = "estandares/Iorder/Excel_Example.zip";
        public static String rutaManualTecnico = "estandares/Iorder/Manual_IT.zip";
        public static String rutaManualProcedures = "estandares/Iorder/Manual_procedures.zip";
        public static String rutaGenerador = "GeneradorArchivosIOrder.zip";
        public static String rutaWinService = "ServicioWindowsIOrderV25.zip";


        public String getConversion(String cantidad)
        {
            return cantidad.Replace(".", simbolo_decimal);
        }


        // convierte el numero a punto para grabar en BD
        public String getConversionToPoint(String cantidad)
        {
            return cantidad.Replace(",", ".");
        }

        public static bool existeRutaTerminos()
        {

            bool flag = true;

            String rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_TERMINOS_Y_CONDICIONES);
            if (rutaFile.Trim().Equals(""))
                flag = false;

            return flag;
        }

        public String getCodeCountryGMaps()
        {
            switch (idioma)
            {
                case "EN": return "&language=en";
                case "MX":
                case "AR":
                case "PE":
                case "CL": return "&language=es";
                case "PT": return "&language=pt-BR";
                default: return "&language=es";
            }
        }

        public String getCodeCountryDataPicker()
        {
            switch (idioma)
            {
                case "EN": return "en";
                case "MX":
                case "AR":
                case "PE":
                case "CL": return "es";
                case "PT": return "pt-BR";
                default: return "es";
            }
        }
        public String getTemaColors()
        {
            switch (tema)
            {
                case "ORANGE": return "ORANGE";
                case "RED": return "RED";
                case "NII": return "NII";
                default: return "RED";
            }
        }

        public static String getRuta(String tipo)
        {
            String ruta = "";
            String rutaFile = "";

            try
            {

                ruta = ConfigurationManager.AppSettings["RUTA_DESCARGA"].ToString();

            }
            catch (Exception e)
            { // si no se tiene el key en web config se obtiene el valor por defecto
                ruta = rutaDescarga;
            }

            switch (tipo)
            {
                case "U":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_MANUAL_USUARIO);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaManualUsuario;
                    break;
                case "T":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_MANUAL_TECNICO);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaManualTecnico;
                    break;
                case "P":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_MANUAL_PROCEDIMIENTOS);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaManualProcedures;
                    break;
                case "G":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_GENERADOR);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaGenerador;
                    break;
                case "W":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_WIN_SERVICE);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaWinService;
                    break;
                case "E":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_EXCEL_EJEMPLO);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaExcel;
                    break;
                case "TC":
                    rutaFile = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_RUTA_TERMINOS_Y_CONDICIONES);
                    if (rutaFile.Equals(""))
                        rutaFile = rutaWinService;
                    break;

            }

            ruta = ruta + rutaFile;


            return ruta;

        }

        public String getFormatDecimalChart(String decimalSeparator, String thousandSeparator, String numberPrefix, String decimalPrecision)
        {
            String formato = "decimalSeparator = '" + decimalSeparator + "'   thousandSeparator='" + thousandSeparator + "' decimalPrecision='" + decimalPrecision + "' formatNumberScale='0' numberPrefix='" + numberPrefix + " '";

            return formato;

        }


        public String getStyleChart()
        {
            String estilo = "canvasBorderColor='FFFFFF' showValues='0' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' canvasBorderColor='666666' divLineAlpha='20' alternateHGridAlpha='30' ";

            return estilo;
        }

    }
}
