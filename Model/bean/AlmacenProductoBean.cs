﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class AlmacenProductoBean
    {
        public String id_pro_pre { get; set; }
        public String id { get; set; }
        public String ALM_PK { get; set; }
        public String ALM_CODIGO { get; set; }
        public String ALM_NOMBRE { get; set; }
        public String PRO_PK { get; set; }
        public String PRO_CODIGO { get; set; }
        public String PRO_NOMBRE { get; set; }
        public String ALM_PRO_STOCK { get; set; }
        public String xml_almacen { get; set; }
       private Int32 _CODIGOPK_PRESENTACION;
       private String _CODIGO_PRESENTACION ;
       private String _NOMBRE_PRESENTACION;
       private String _PK_ALMACEN_PRESENTACION;
       private String _CANTIDAD;
       private String _PRO_UNIDADDEFECTO;

       public String PRO_UNIDADDEFECTO
       {
           get { return _PRO_UNIDADDEFECTO; }
           set { _PRO_UNIDADDEFECTO = value; }
       }

       public String CANTIDAD
       {
           get { return _CANTIDAD; }
           set { _CANTIDAD = value; }
       }

       public String PK_ALMACEN_PRESENTACION
       {
           get { return _PK_ALMACEN_PRESENTACION; }
           set { _PK_ALMACEN_PRESENTACION = value; }
       }

       public String NOMBRE_PRESENTACION
       {
           get { return _NOMBRE_PRESENTACION; }
           set { _NOMBRE_PRESENTACION = value; }
       }
public String CODIGO_PRESENTACION
{
  get { return _CODIGO_PRESENTACION; }
  set { _CODIGO_PRESENTACION = value; }
}
public Int32 CODIGOPK_PRESENTACION
{
  get { return _CODIGOPK_PRESENTACION; }
  set { _CODIGOPK_PRESENTACION = value; }
}
	

        public AlmacenProductoBean()
        {
            _CODIGO_PRESENTACION = String.Empty;
            _CODIGOPK_PRESENTACION = 0;
            _NOMBRE_PRESENTACION = String.Empty;
            _PK_ALMACEN_PRESENTACION = String.Empty;
            _CANTIDAD = String.Empty;
            _PRO_UNIDADDEFECTO = String.Empty;
        }
    }
}
