﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateReporteNoPedidoBean
    {
      public List<ReporteNoPedidoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateReporteNoPedidoBean()
        {
            lstResultados = new List<ReporteNoPedidoBean>();
            totalPages = 0;
            
        }

    }
}
