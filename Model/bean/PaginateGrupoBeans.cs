﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateGrupoBeans
    {
        private List<GrupoBean> _lsListaGrupoBean;
        private Int32 _TotalFila;

        public Int32 TotalFila
        {
            get { return _TotalFila; }
            set { _TotalFila = value; }
        }

        public List<GrupoBean> LsListaGrupoBean
        {
            get { return _lsListaGrupoBean; }
            set { _lsListaGrupoBean = value; }
        }
        public PaginateGrupoBeans()
        {
            _lsListaGrupoBean = new List<GrupoBean>();
            _TotalFila = 0;
        }
    }
}
