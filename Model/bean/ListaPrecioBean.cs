﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ListaPrecioBean
    {
        public String id { get; set; }
        public String codigoproducto { get; set; }
        public String producto { get; set; }
        public String codigocanal { get; set; }
        public String precio { get; set; }
        public String flag { get; set; }
        public String condventa { get; set; }
        public String grupoeconomico { get; set; }
        public String preciosoles { get;set; }
        public String preciodolares { get; set; }
        private Int32 _FlagInsert;
        private Int32 _Pagina;
        private Int32 _TotalFila;
        private String _NombreProducto;
        private String _Presentacion;
        private String _CodProducto;
        private String _Codpresentacion;

        public String Codpresentacion
        {
            get { return _Codpresentacion; }
            set { _Codpresentacion = value; }
        }

        public String CodProducto
        {
            get { return _CodProducto; }
            set { _CodProducto = value; }
        }

        public String Presentacion
        {
            get { return _Presentacion; }
            set { _Presentacion = value; }
        }

        public String NombreProducto
        {
            get { return _NombreProducto; }
            set { _NombreProducto = value; }
        }


        public Int32 TotalFila
        {
            get { return _TotalFila; }
            set { _TotalFila = value; }
        }

        public Int32 Pagina
        {
            get { return _Pagina; }
            set { _Pagina = value; }
        }

        public Int32 FlagInsert
        {
            get { return _FlagInsert; }
            set { _FlagInsert = value; }
        }
        public String descmin { get; set; }
        public String descmax { get; set; }

        public ListaPrecioBean()
        {
            _FlagInsert = 0;
            _Pagina = 0;
            _TotalFila = 0;
            _NombreProducto = String.Empty;
            _Presentacion = String.Empty;
            _CodProducto = String.Empty;
            _Codpresentacion= String.Empty;
        }
    }
}
