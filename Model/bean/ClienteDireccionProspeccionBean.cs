﻿using System;

/// <summary>
/// @001 GMC 12/04/2015 Ajustes para actualizar Campos DIR_TIPO (P: Pedido, D: Despacho) y FECCREACION en Mant Direcciones
/// </summary>
namespace Model.bean
{
    public class ClienteDireccionProspeccionBean
    {
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String dir_pk { get; set; }
        public String cli_pk { get; set; }
        public String latitud { get; set; }
        public String longitud { get; set; }
        public String dir_tipo { get; set; } //@001 I/F
        public String dir_tipo_des { get; set; } //@001 I/F
        public DateTime fe_creacion { get; set; } //@001 I/F

        public ClienteDireccionProspeccionBean()
        {
        }
    }
}