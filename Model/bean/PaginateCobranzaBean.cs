﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateCobranzaBean
    {
      public List<CobranzaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateCobranzaBean()
        {
            lstResultados = new List<CobranzaBean>();
            totalPages = 0;
            
        }

    }
}
