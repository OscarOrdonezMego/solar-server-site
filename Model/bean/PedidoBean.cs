﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// @001 GMC 13/04/2015 Se agregan campos FLGTIPO (P: Pedido - C: Cotización) y TIPO_NOMBRE
/// @002 GMC 14/04/2015 Se muestra campo Dirección de Despacho de Pedido
/// @003 GMC 18/04/2015 Se agrega Flag Mostrar Direccion Despacho
/// @004 GMC 05/05/2015 Se agrega campo y flag para mostrar Almacén
/// </summary>

namespace Model.bean
{
    public class PedidoBean
    {
        private String _ALM_PK;
        private String _TipoArticulo;
        private String _TOTAL_FLETE;
        private String _TOTAL_CANT_FRAC;
        private String _TOTAL_PRECIO_FRAC;
        private String _MONTO_TOTAL;
        public int TipoCambioSoles { get; set; }
        public int TipoCambioDolares { get; set; }
public string ValorTipoCambioSoles { get; set; }
        public string ValorTipoCambioDolares { get; set; }

        public String MONTO_TOTAL
        {
            get { return _MONTO_TOTAL; }
            set { _MONTO_TOTAL = value; }
        }
        public String id { get; set; }
        public String PED_CODIGO { get; set; }
        public String CLI_CODIGO { get; set; }
        public String PED_FECINICIO { get; set; }
        public String PED_FECEDICION { get; set; }
        public String PED_CONDVENTA { get; set; }
        public String PED_MONTOTOTAL { get; set; }
        public String PED_MONTOTOTAL_SOLES { get; set; }
        public String PED_MONTOTOTAL_DOLARES { get; set; }
        public String NOM_USUARIO { get; set; }
        public String USR_PK { get; set; }
        public String CONDVTA_NOMBRE { get; set; }
        public String CLI_NOMBRE { get; set; }
        public String LATITUD { get; set; }
        public String LONGITUD { get; set; }
        public String CANTIDAD { get; set; }
        public String PRODUCTO { get; set; }
        public String tieneBonificacion { get; set; }
        public String TOTALBONIFICACION { get; set; }
        public String PED_DIRECCION { get; set; }
        public String FLGTIPO { get; set; } //@001 I/F
        public String TIPO_NOMBRE { get; set; } //@001 I/F
        public String PED_DIRECCION_DESPACHO { get; set; } //@002 I/F
        public String FlgDireccionDespacho { get; set; } //@003 I/F
        public String ALM_NOMBRE { get; set; } //@004 I/F
        public String FLG_MOSTRAR_ALMACEN { get; set; } //@004 I/F
        private String _NOMBREGRUPO;
        public String ESTADO_PROCESADO { get; set; }
        public String PED_FECREGISTRO { get; set; }

        public String NOMBREGRUPO
        {
            get { return _NOMBREGRUPO; }
            set { _NOMBREGRUPO = value; }
        }
        

        public String TOTAL_PRECIO_FRAC
        {
            get { return _TOTAL_PRECIO_FRAC; }
            set { _TOTAL_PRECIO_FRAC = value; }
        }

        public String TOTAL_CANT_FRAC
        {
            get { return _TOTAL_CANT_FRAC; }
            set { _TOTAL_CANT_FRAC = value; }
        }

        public String TOTAL_FLETE
        {
            get { return _TOTAL_FLETE; }
            set { _TOTAL_FLETE = value; }
        }

        public String TipoArticulo
        {
            get { return _TipoArticulo; }
            set { _TipoArticulo = value; }
        }

        public String ALM_PK
        {
            get { return _ALM_PK; }
            set { _ALM_PK = value; }
        }

        public PedidoBean()
        {
            _ALM_PK = String.Empty;
            _TipoArticulo = String.Empty;
            _TOTAL_FLETE = String.Empty;
            _TOTAL_CANT_FRAC = String.Empty;
            _TOTAL_PRECIO_FRAC = String.Empty;
            _MONTO_TOTAL = String.Empty;
            _NOMBREGRUPO = String.Empty;
        }
    }
}