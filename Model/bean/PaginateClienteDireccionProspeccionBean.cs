﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginateClienteDireccionProspeccionBean
    {
        public List<ClienteDireccionProspeccionBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginateClienteDireccionProspeccionBean()
        {
            lstResultados = new List<ClienteDireccionProspeccionBean>();
            totalPages = 0;
            
        }
    }
}
