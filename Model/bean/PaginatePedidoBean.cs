﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
  public  class PaginatePedidoBean
    {
      public List<PedidoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }


        public PaginatePedidoBean()
        {
            lstResultados = new List<PedidoBean>();
            totalPages = 0;
            
        }

    }
}
