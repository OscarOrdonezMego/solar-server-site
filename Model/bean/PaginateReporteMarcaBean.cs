﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateReporteMarcaBean
    {

        public List<ReporteMarcaBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }
        public String totalmonto { get; set; }
        public String totalmontosoles { get; set; }
        public String totalmontodolares { get; set; }
        public String totalcantidad { get; set; }
        public String totalmarcas { get; set; }
        public PaginateReporteMarcaBean()
        {
            lstResultados = new List<ReporteMarcaBean>();
            totalPages = 0;
            totalmonto = "0";
            totalmontosoles = "0";
            totalmontodolares = "0";
            totalcantidad = "0";
            totalmarcas = "0";
        }

    }
}

