﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class VendedorBean
    {
       public String codigo { get; set; }
       public String nombre { get; set; }
       private String _Usu_PK;

       public String Usu_PK
       {
           get { return _Usu_PK; }
           set { _Usu_PK = value; }
       }
     

       public VendedorBean()
        {
            _Usu_PK = String.Empty;
        }
    }
}
