﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class SessionManager
    {
        public static String USER_SESSION = "USER_SESSION";
        public static String ROL_SESSION = "ROL_SESSION";
        public static String MSGBOX_MENSAJE = "MSGBOX_MENSAJE";
        public static String THEME_SESSION = "THEME_SESSION";
        public static String LISTA_SESSION = "LISTA_SESSION";
        public static String LISTA_SESSION_CONFIGURACION = "LISTA_SESSION_CONFIGURACION";
        public static String LISTA_SESSION_PRESENTACION = "LISTA_SESSION_PRESENTACION";
        public static String LISTA_SESSION_PEDIDO_DETALLE_PRESENTACION = "LISTA_SESSION_PEDIDO_DETALLE_PRESENTACION";
    }
}
