﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
   public class ReporteEquipoBean
    {


       public String id  { get; set; }
       public String ULTIMAFECHA { get; set; }
       public String ULTIMALATITUD { get; set; }
       public String ULTIMALONGITUD { get; set; }
       public String MODELO { get; set; }
       public String SENAL { get; set; }
       public String BATERIA { get; set; }
       public String IP { get; set; }
       public String NEXTEL { get; set; }
       public String TELEFONO { get; set; }
       public String ERRORPOSICION { get; set; }
       public String ERRORCONEXION { get; set; }
       public String ASISTENCIA { get; set; }
       public String USUARIO { get; set; }
       public String COD_USR { get; set; }

       public ReporteEquipoBean()
        { 
         }
    }
}
