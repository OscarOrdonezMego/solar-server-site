﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class TipoClienteBean
    {
        public Int32 Id { get; set; }
        public String Codigo { get; set; }
        public String Nombre { get; set; }
    }
}
