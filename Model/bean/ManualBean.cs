﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class ManualBean
    {
        private String _IdManual;
        private String _Descripcion;
        private String _Url;

        public String IdManual
        {
            get { return _IdManual; }
            set { _IdManual = value; }
        }

        public String Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        public String Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
    }
}
