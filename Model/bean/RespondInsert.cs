﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class RespondInsert
    {
        private Int32 _Valor;
        private String _Codigo;

        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public Int32 Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        public RespondInsert()
        {
            _Valor = 0;
            _Codigo = String.Empty;
        }
    }
}
