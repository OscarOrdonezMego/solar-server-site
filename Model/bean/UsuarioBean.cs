﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.bean
{
    public class UsuarioBean
    {
        public String id { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String login { get; set; }
        public String clave { get; set; }
        public String codigorol { get; set; }
        public String estadoultimo { get; set; }
        public String codigoperfil { get; set; }
        public String nivelPerfil { get; set; }
        public String supervisor { get; set; }
        public String flag { get; set; }
        public String flagmode { get; set; }
        public String idgeocerca { get; set; }
        public String telefono { get; set; }
        public String nombreperfil { get; set; }
        public String descripciongeocerca { get; set; }
        public String perfil { get; set; }
        public String codSupervisor { get; set; }
        public Dictionary<String, RolBean> hashRol { get; set; }
        public String serie { get; set; }
        public String correlativo { get; set; }
        public Dictionary<String, ConfiguracionBean> hashConfiguracion { get; set; }
        private String _codgrupo;
        private String _GRUPO;
        private String _VEN_PERFIL_VALUE;
        private String _CodigoUsu;

        public String CodigoUsu
        {
            get { return _CodigoUsu; }
            set { _CodigoUsu = value; }
        }

        

        public String GRUPO
        {
            get { return _GRUPO; }
            set { _GRUPO = value; }
        }
        public String VEN_PERFIL_VALUE
        {
            get { return _VEN_PERFIL_VALUE; }
            set { _VEN_PERFIL_VALUE = value; }
        }

        public String Codgrupo
        {
            get { return _codgrupo; }
            set { _codgrupo = value; }
        }

        public String IdResultado { get; set; }

        public String SUPERVISORNAME
        {
            get { return supervisor == "T" ? IdiomaCultura.getMensaje(IdiomaCultura.WEB_SUPERVISOR) : ""; }
        }

        public String getNombreCorto()
        {
            return nombre != null && nombre.Length > 16 ? nombre.Substring(0, 16) : nombre;
        }


        public UsuarioBean()
        {
            id = String.Empty;
            codigo = String.Empty;
            nombre = String.Empty;
            login = String.Empty;
            clave = String.Empty;
            codigorol = String.Empty;
            estadoultimo = String.Empty;
            codigoperfil = String.Empty;
            nivelPerfil = String.Empty;
            perfil = String.Empty;
            codSupervisor = String.Empty;
            serie = String.Empty;
            correlativo = String.Empty;
            hashRol = new Dictionary<String, RolBean>();
            hashConfiguracion = new Dictionary<String, ConfiguracionBean>();
            _codgrupo = String.Empty;
            _GRUPO = String.Empty;
            _VEN_PERFIL_VALUE = String.Empty;
            _CodigoUsu = String.Empty;
        }

    }
}
