﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.bean
{
    public class PaginateAlmacenProductoBean
    {
        public List<AlmacenProductoBean> lstResultados { get; set; }
        public Int32 totalPages { get; set; }

        public PaginateAlmacenProductoBean()
        {
            lstResultados = new List<AlmacenProductoBean>();
            totalPages = 0;
        }
    }
}