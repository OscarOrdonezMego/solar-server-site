﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.bean
{
    public class MapeoBean
    {
        public Int64 id { get; set; }
        public Int64 idMaestro { get; set; }
        public String tabla { get; set; }
        public String columna { get; set; }
        public String descripcion { get; set; }
        public String formato { get; set; }
        public String obligatorio { get; set; }
        public String flgHabilitado { get; set; }
        public String tipoFormato { get; set; }
        public String columnaTabla { get; set; }
        public String columnaXls { get; set; }
        public String tablaTemp { get; set; }
        public String spcargaTabla { get; set; }

        public MapeoBean()
        {
            columna = "";
            formato = "";
            obligatorio = "";
        }
    }
    public class columnasMap
    {
        public String idColumnaxls { get; set; }
        public String idColumna { get; set; }
        public String columnaxls { get; set; }
    }
    public class archivoExcelTablas
    {
        public String archivo { get; set; }
        public List<archivoTabla> tablas{ get; set; }
    }
    public class archivoTabla
    {
        public String tabla { get; set; }
        public String formatoCliente { get; set; }
        public String formatoEntel { get; set; }
    }
}
