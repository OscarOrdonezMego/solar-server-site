﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
  public  class CargaModel
    {

      public static DataTable getErrores(String spName)
      {
          ArrayList alParameters = new ArrayList();

         return SqlConnector.getDataTable(spName, alParameters);
      }


      public static void setConfiguracionCarga(String TIPO)
      {
          ArrayList alParameters = new ArrayList();

          SqlParameter parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 250);
          parameter.Value = TIPO;
          alParameters.Add(parameter);

          SqlConnector.getDataTable("USPC_ACTUALIZAR_CONFIGURACION", alParameters);
      }


      public static void limpiaTemporalesCarga(String tableName)
      {
          ArrayList alParameters = new ArrayList();

          SqlParameter parameter = new SqlParameter("@tableName", SqlDbType.VarChar, 250);
          parameter.Value = tableName;
          alParameters.Add(parameter);

          SqlConnector.getDataTable("USPC_LIMPIA_TEMPORALES", alParameters);
      }


      public static Int32 insertaErrorCarga(Int64 id, string error, string tableName)
      {
          ArrayList alParameters = new ArrayList();
          SqlParameter parameter = new SqlParameter("@FILA", SqlDbType.VarChar, 250);
          parameter.Value = id;
          alParameters.Add(parameter);

          parameter = new SqlParameter("@ERROR", SqlDbType.VarChar, 4000);
          parameter.Value = error;
          alParameters.Add(parameter);

          parameter = new SqlParameter("@PKG", SqlDbType.VarChar, 250);
          parameter.Value = tableName;
          alParameters.Add(parameter);

          return Convert.ToInt32(SqlConnector.executeScalar("USPS_ERROR_CARGA", alParameters));

      }

        public static void eliminarDataAnteriorCarga(String listaArchivosEliminar)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@LISTATABLAS", SqlDbType.NVarChar);
            parameter.Value = listaArchivosEliminar;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPC_ELIMINAR_DATA_PREVIA_CARGA", alParameters);
        }

    }
}
