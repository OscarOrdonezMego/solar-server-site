﻿using Model.functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Model
{
    public class MarcaProductoModel
    {
        public static DataTable fnlistaMarcaProducto()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("spS_ListarMarcaProducto", alParameters);
        }
    }
}
