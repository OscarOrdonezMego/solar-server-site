﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;

namespace Model
{
   public  class Operation
    {
        public class Habilitados
        {
            public const String HABILITADO = "T";
            public const String DESHABILITADO = "F";
        }

        public class CSS
        {
            public const String STYLE_DISPLAY_NONE = " style='display:none'";
            public const String STYLE_DISPLAY_BLOCK = " style='display:block'";
        }
        public class VACIO
        {
            public const String TEXT_VACIO = "";
        }
        public class flagACIVADO
        {
            public const Int32 ACTIVO = 1;
            public const Int32 DESACTIVADO = 0;
        }
       public class TipoArticulo{
           public const String PRODUCTO = "PRO";
           public const String PRESENTACION = "PRE";
       }
       public class Condicion
       {
           public static String If(Int32 Respuesta)
           {
               String rpta="";
               if (Respuesta == 0)
               {
                   rpta = IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
               }
               else
               {
                   rpta = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EXISTE_GRUPO);
               }
               return rpta;
           }
       }
        public class ValidacionSession
        {
            public static FuncionBean fnExtraerCodigoFuncion(List<ConfiguracionBean> loLstConfiguracionBean, FuncionBean poFuncionBean)
            {
                try
                {

                    FuncionBean loFuncionBean = null;
                    if (loLstConfiguracionBean != null && loLstConfiguracionBean.Count > 0)
                    {
                        for (Int32 i = 0; i < loLstConfiguracionBean.Count; i++)
                        {
                            if (loLstConfiguracionBean[i].Codigo.Equals(poFuncionBean.CodigoFuncion))
                            {
                                loFuncionBean = new FuncionBean();
                                loFuncionBean.CodigoFuncion = loLstConfiguracionBean[i].Codigo;
                                loFuncionBean.Habilitado = loLstConfiguracionBean[i].Valor;
                                break;
                            }
                        }
                    }
                    return loFuncionBean;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            public static Int32 fnValidarFuncion(FuncionBean poFuncionBean)
            {
                Int32 liRespuesta = 0;
                if (poFuncionBean.CodigoFuncion.Equals(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO) && poFuncionBean.Habilitado.Equals(Operation.Habilitados.HABILITADO))
                {
                    liRespuesta = 1;
                }
                return liRespuesta;
            }
            public static String fnValidarFuncionOcultar(FuncionBean poFuncionBean)
            {
                String liRespuesta = "";
                if (poFuncionBean.CodigoFuncion.Equals(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO) && poFuncionBean.Habilitado.Equals(Operation.Habilitados.DESHABILITADO))
                {
                    liRespuesta = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                return liRespuesta;
            }
            public static String fnValidarFuncionMostrar(FuncionBean poFuncionBean)
            {
                String liRespuesta = Operation.CSS.STYLE_DISPLAY_NONE;
                if (poFuncionBean.CodigoFuncion.Equals(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO) && poFuncionBean.Habilitado.Equals(Operation.Habilitados.DESHABILITADO))
                {
                    liRespuesta ="" ;
                }
                return liRespuesta;
            }
            public static String fnValidarAlmacenActivo(List<ConfiguracionBean> poListaConfiguracion, String Cod)
            {
                String valor = "";
                ConfiguracionBean loConfiguracionBean = null;
                if (poListaConfiguracion.Count > 0 || poListaConfiguracion !=null)
                {
                    for (Int32 i = 0; i < poListaConfiguracion.Count; i++)
                    {
                        if (poListaConfiguracion[i].Codigo.Equals(Cod))
                        {
                            loConfiguracionBean = new ConfiguracionBean();
                            loConfiguracionBean.Codigo = poListaConfiguracion[i].Codigo;
                            loConfiguracionBean.Valor = poListaConfiguracion[i].Valor;
                        }
                    }
                    if (loConfiguracionBean.Codigo.Equals(Cod) && loConfiguracionBean.Valor.Equals("T"))
                    {
                        valor = Operation.CSS.STYLE_DISPLAY_NONE;
                    }
                    else
                    {
                        valor = "";
                    }
                }
                return valor;
            }
            public static String fnValidarAlmacenDesactivado(List<ConfiguracionBean> poListaConfiguracion, String Cod)
            {
                String valor = "";
                ConfiguracionBean loConfiguracionBean = null;
                if (poListaConfiguracion.Count > 0 || poListaConfiguracion != null)
                {
                    for (Int32 i = 0; i < poListaConfiguracion.Count; i++)
                    {
                        if (poListaConfiguracion[i].Codigo.Equals(Cod))
                        {
                            loConfiguracionBean = new ConfiguracionBean();
                            loConfiguracionBean.Codigo = poListaConfiguracion[i].Codigo;
                            loConfiguracionBean.Valor = poListaConfiguracion[i].Valor;
                        }
                    }
                    if (loConfiguracionBean.Codigo.Equals(Cod) && loConfiguracionBean.Valor.Equals("F"))
                    {
                        valor = Operation.CSS.STYLE_DISPLAY_NONE;
                    }
                }
                return valor;
            }
            public static Int32 fnValidarAlmacenActivoInt(List<ConfiguracionBean> poListaConfiguracion, String Cod)
            {
                Int32 valor = 0;
               ConfiguracionBean loConfiguracionBean = null;
               if (poListaConfiguracion.Count > 0 || poListaConfiguracion != null)
                {
                    for (Int32 i = 0; i < poListaConfiguracion.Count; i++)
                    {
                        if (poListaConfiguracion[i].Codigo.Equals(Cod))
                        {
                            loConfiguracionBean = new ConfiguracionBean();
                            loConfiguracionBean.Codigo = poListaConfiguracion[i].Codigo;
                            loConfiguracionBean.Valor = poListaConfiguracion[i].Valor;
                        }
                    }
                    if (loConfiguracionBean.Codigo.Equals(Cod) && loConfiguracionBean.Valor.Equals("T"))
                    {
                        valor = 0;
                    }
                    else
                    {
                        valor = 1;
                    }
                }
                return valor;
            }
            public static Boolean ValidarPerfil(String perfil)
            {
                Boolean rpta = false;
                if (perfil.Equals("ADM") || perfil.Equals("DBO"))
                {
                    rpta = false;
                }
                else
                {
                    rpta = true;
                }

                return rpta;

            }
        }

        public class Operaciones
        {
            public static String fnMontoTotalNeto(String pdPrecio, String poCantidad, String poDescuento)
            {
                double lopdPrecio = double.Parse(pdPrecio);
                double lopoCantidad =double.Parse(poCantidad);
                double lopoDescuento = double.Parse(poDescuento);
                double ldMontoSinDescuento = 0.0;
                double ldMontoconDescuento = 0.0;
                double total=0.0;
                if (lopoDescuento == 0)
                {
                    ldMontoSinDescuento = lopoCantidad * lopdPrecio;
                    total = ldMontoSinDescuento;
                }
                else
                {
                    ldMontoSinDescuento = lopoCantidad * lopdPrecio;
                    ldMontoconDescuento = (ldMontoSinDescuento * lopoDescuento) / 100;
                    total = ldMontoconDescuento;
                }
                
                return total.ToString();
            }

            public static String fnObtenerCodigoPedidoDetalle(String psCodigo)
            {
                char[] delimiterChars = { '|' };
                String[] vector = psCodigo.Split(delimiterChars);
                String iddetallePedido = "";
                for (Int32 i = 0; i < vector.Length; i++)
                {
                    iddetallePedido += vector[i].ToString() + ",";
                }
                return iddetallePedido;
            }
            public static String fnObtenerCodigoPedido(String psCodigo)
            {
                String[] vector = psCodigo.Split('|');
                String idPedido = "";
                foreach (String cod in vector)
                {
                    idPedido = cod[0].ToString();
                }
                return idPedido;
            }
            public static String fnObtenerFraccionamiento(String psCantidadPre, String psUnidadFrac, String psPrecio)
            {
                double ldTotalPrecio = 0.0;
                Int32 liCantidadPre = Int32.Parse(psCantidadPre);
                Int32 liUnidadFrac = Int32.Parse(psUnidadFrac);
                double ldPrecio = double.Parse(psPrecio);
                ldTotalPrecio = (liCantidadPre * ldPrecio) / liUnidadFrac;

                return "";
            }
        }
        public class SplitVector
        {
            public static List<AlmacenBean> fnGetCodigoAlmacen(String lsCodigoAlmacen)
            {
                List<AlmacenBean> loLstAlmacenBean = new List<AlmacenBean>();
                String[] vector = lsCodigoAlmacen.Split('|');
                AlmacenBean loAlmacenBean = null;
                foreach (String vec in vector)
                {
                    if (vec.Length > 0)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.ALM_CODIGO = vec.ToString();
                        loLstAlmacenBean.Add(loAlmacenBean);
                    }
                   
                }
                return loLstAlmacenBean;
            }
            public static List<AlmacenBean> fnGetStockAlmacen(String lsStockAlmacen)
            {
                List<AlmacenBean> loLstAlmacenBean = new List<AlmacenBean>();
                String[] vector = lsStockAlmacen.Split('|');
                AlmacenBean loAlmacenBean = null;
                foreach (String vec in vector)
                {
                    if (vec.Length > 0)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.Stock = vec.ToString();
                        loLstAlmacenBean.Add(loAlmacenBean);
                    }

                }
                return loLstAlmacenBean;
            }
            public static List<DescuentoVolumen> ListaRangoMin(String rangominimo)
            {
                String[] vectorRangomin = rangominimo.Split('|');
                List<DescuentoVolumen> listaDescuentoVolumen = new List<DescuentoVolumen>();
                DescuentoVolumen loDescuentoVolumen = null;

                for (Int32 i = 0; i < vectorRangomin.Length; i++)
                {
                    if (vectorRangomin[i].Length > 0)
                    {
                        loDescuentoVolumen = new DescuentoVolumen();
                        loDescuentoVolumen.RMIN = vectorRangomin[i].ToString();
                        listaDescuentoVolumen.Add(loDescuentoVolumen);
                    }
                }
                return listaDescuentoVolumen;
            }
            public static List<DescuentoVolumen> ListaRangoMax(String rangomaximo)
            {
                String[] vectorRangomax = rangomaximo.Split('|');
                List<DescuentoVolumen> listaDescuentoVolumen = new List<DescuentoVolumen>();
                DescuentoVolumen loDescuentoVolumen = null;

                for (Int32 i = 0; i < vectorRangomax.Length; i++)
                {
                    if (vectorRangomax[i].Length > 0)
                    {
                        loDescuentoVolumen = new DescuentoVolumen();
                        loDescuentoVolumen.RMAX = vectorRangomax[i].ToString();
                        listaDescuentoVolumen.Add(loDescuentoVolumen);
                    }
                }
                return listaDescuentoVolumen;
            }
            public static List<DescuentoVolumen> Listadsctominimo(String dsctominimo)
            {
                String[] vectordsctominimo = dsctominimo.Split('|');
                List<DescuentoVolumen> listaDescuentoVolumen = new List<DescuentoVolumen>();
                DescuentoVolumen loDescuentoVolumen = null;

                for (Int32 i = 0; i < vectordsctominimo.Length; i++)
                {
                    if (vectordsctominimo[i].Length > 0)
                    {
                        loDescuentoVolumen = new DescuentoVolumen();
                        loDescuentoVolumen.DSCTOMIN = vectordsctominimo[i].ToString();
                        listaDescuentoVolumen.Add(loDescuentoVolumen);
                    }
                }
                return listaDescuentoVolumen;
            }
            public static List<DescuentoVolumen> Listadsctomaximo(String dsctomaximo)
            {
                String[] vectordsctomaximo = dsctomaximo.Split('|');
                List<DescuentoVolumen> listaDescuentoVolumen = new List<DescuentoVolumen>();
                DescuentoVolumen loDescuentoVolumen = null;

                for (Int32 i = 0; i < vectordsctomaximo.Length; i++)
                {
                    if (vectordsctomaximo[i].Length > 0)
                    {
                        loDescuentoVolumen = new DescuentoVolumen();
                        loDescuentoVolumen.DSCTOMAX = vectordsctomaximo[i].ToString();
                        listaDescuentoVolumen.Add(loDescuentoVolumen);
                    }
                }
                return listaDescuentoVolumen;
            }
            public static List<GrupoBean> ListaCodigoGrupo(String codigos)
            {
                String[] vectordsctomaximo = codigos.Split(',');
                List<GrupoBean> listaGrupoBean = new List<GrupoBean>();
                GrupoBean loGrupoBean = null;

                for (Int32 i = 0; i < vectordsctomaximo.Length; i++)
                {
                    if (vectordsctomaximo[i].Length > 0)
                    {
                        loGrupoBean = new GrupoBean();
                        loGrupoBean.Grupo_PK =Int32.Parse( vectordsctomaximo[i].ToString());
                        listaGrupoBean.Add(loGrupoBean);
                    }
                }
                return listaGrupoBean;
            }
        }
       public static List<CabeceraDetalleBean> ListaSplitComa(String codigos)
            {
                String[] vectordsctomaximo = codigos.Split(',');
                List<CabeceraDetalleBean> listaCabeceraDetalleBean = new List<CabeceraDetalleBean>();
                CabeceraDetalleBean loCabeceraDetalleBean = null;

                for (Int32 i = 0; i < vectordsctomaximo.Length; i++)
                {
                    if (vectordsctomaximo[i].Length > 0)
                    {
                        loCabeceraDetalleBean = new CabeceraDetalleBean();
                        loCabeceraDetalleBean.ID =vectordsctomaximo[0].ToString();
                        loCabeceraDetalleBean.Nombre=vectordsctomaximo[1].ToString();
                        loCabeceraDetalleBean.Habilitado=vectordsctomaximo[2].ToString();
                        loCabeceraDetalleBean.Orden=vectordsctomaximo[3].ToString();
                        listaCabeceraDetalleBean.Add(loCabeceraDetalleBean);
                    }
                }
                return listaCabeceraDetalleBean;
            }
       public static String[] ListaSplitPalotes(String codigos)
       {
           String[] vectordsctomaximo = codigos.Split('|');
           
           return vectordsctomaximo;
       }
       public static String columnas (List<CabeceraDetalleBean> Lista,String TipoColumna)
       {
           String RetornoCadena = "";
           
           if (Lista.Count > 0)
           {
               for (Int32 i = 0; i < Lista.Count; i++)
               {
                   if (TipoColumna.Equals("ID"))
                   {
                       RetornoCadena = Lista[i].ID.ToString();
                   }
                   if (TipoColumna.Equals("NOMBRE"))
                   {
                       RetornoCadena = Lista[i].Nombre.ToString();
                   }
                   if (TipoColumna.Equals("HABILITADO"))
                   {
                       RetornoCadena = Lista[i].Habilitado.ToString();
                   }
                   if (TipoColumna.Equals("ORDEN"))
                   {
                       RetornoCadena = Lista[i].Orden.ToString();
                   }
               }
           }
           return RetornoCadena;
       }

        public class Validacion
        {
            public static Int32 ValidacionRango(List<DescuentoVolumen> ioListaDsctoVol, String ranminnum, String ranmaxnum)
            {
                Int32 rpta=0;
                if (ioListaDsctoVol.Count > 0)
                {

                    for (Int32 i = 0; i < ioListaDsctoVol.Count; i++)
                    {

                        if ((Int32.Parse(ranminnum) <= Int32.Parse(ioListaDsctoVol[i].RMIN) &&
                            Int32.Parse(ranmaxnum) <= Int32.Parse(ioListaDsctoVol[i].RMIN)) || (
                            Int32.Parse(ranminnum) >= Int32.Parse(ioListaDsctoVol[i].RMAX) &&
                            Int32.Parse(ranmaxnum) >= Int32.Parse(ioListaDsctoVol[i].RMAX)))
                        {
                            rpta = 1;
                            break;
                        }
                    }
                }
                else
                {
                    rpta = 1;
                }
                return rpta;
            }
            public static Int32 ValidacionDescuento(List<DescuentoVolumen> ioListaDsctoVol, String destominnum, String destomaxnum)
            {
                Int32 rpta = 0;
                if (ioListaDsctoVol.Count > 0)
                {

                    for (Int32 i = 0; i < ioListaDsctoVol.Count; i++)
                    {

                        if ((Int32.Parse(destominnum) <= Int32.Parse(ioListaDsctoVol[i].DSCTOMIN) &&
                            Int32.Parse(destomaxnum) <= Int32.Parse(ioListaDsctoVol[i].DSCTOMIN)) || (
                            Int32.Parse(destominnum) >= Int32.Parse(ioListaDsctoVol[i].DSCTOMAX) &&
                            Int32.Parse(destomaxnum) >= Int32.Parse(ioListaDsctoVol[i].DSCTOMAX)))
                        {
                            rpta = 1;
                            break;
                        }
                    }
                }
                else
                {
                    rpta = 1;
                }
                return rpta;
            }
        }
    }
}
