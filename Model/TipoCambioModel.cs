﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

namespace Model
{
    public class TipoCambioModel
    {
        public static DataTable findTipoCambioBean(TipoCambioBean tipocambio, int page, int rows)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@TC_CODIGO", SqlDbType.VarChar, 10);
            parameter.Value = tipocambio.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITADO", SqlDbType.Char, 1);
            parameter.Value = tipocambio.flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PAGE", SqlDbType.Int);
            parameter.Value = page;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ROWS", SqlDbType.Int);
            parameter.Value = rows;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_TIPO_CAMBIO", alParameters);

        }

        public static Int32 crearTipoCambio(TipoCambioBean poTipoCambioBean)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@TC_CODIGO", SqlDbType.VarChar, 10);
            parameter.Value = poTipoCambioBean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TC_VALOR", SqlDbType.VarChar, 20);
            parameter.Value = poTipoCambioBean.valor;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearTipoCambio", alParameters));
        }

        public static DataTable buscarTipoCambio(String tipo)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@TIPO", SqlDbType.Char, 1);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarTipoCambio", alParameters);
        }

    }
}