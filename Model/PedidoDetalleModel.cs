﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
    public class PedidoDetalleModel
    {

        public static DataTable findPedidoDetalle(String gID, String gCODIGO, String gNOMBRE, String flag, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
            parameter.Value = Int32.Parse(gID);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_COD", SqlDbType.VarChar, 10);
            parameter.Value = gCODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_NOM", SqlDbType.VarChar, 20);
            parameter.Value = gNOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_PEDIDO_DETALLE", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_DETALLEPEDIDO", alParameters);

        }

        public static Int32 crear(PedidoDetalleBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.VarChar, 50);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_PK", SqlDbType.VarChar, 250);
            parameter.Value = bean.idDET;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.PRO_CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_CANTIDAD;
            alParameters.Add(parameter);

			parameter = new SqlParameter("@DET_PRECIO", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DET_PRECIO_SOLES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_SOLES;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DET_PRECIO_DOLARES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_DOLARES;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DET_DESCUENTO", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_DESCUENTO;
            alParameters.Add(parameter);
			
			parameter = new SqlParameter("@DET_MONTO", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_MONTO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_DESCRIPCION", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_DESCRIPCION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_BONIFICACION", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_BONIFICACION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoMoneda", SqlDbType.Char, 1);
            parameter.Value = bean.TipoMoneda;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_PEDIDODETALLE_I", alParameters));

        }

        public static String fnVerNuloStr(String poObjeto)
        {
            return poObjeto == null ? "" : poObjeto;
        }

        public static DataTable info(String id, String det_id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
            parameter.Value = Int32.Parse(id);
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DET_PK", SqlDbType.Int);
            parameter.Value = Int32.Parse(det_id);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_PEDIDO_DETALLE", alParameters);

        }

        public static DataTable matchActividadBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);

        }


        public static DataTable matchEstadoBean(String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = estado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);

        }
        public static DataTable fnBuscarAlmacen(String psCodigoPresentacion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CodigoPresentacion", SqlDbType.VarChar, 20);
            parameter.Value = psCodigoPresentacion;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarAlmacenPorPresentacion", alParameters);

        }

        public static DataTable fnBuscarCantidadPresentacion(String psCodigoPresentacion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@codigopre", SqlDbType.VarChar, 20);
            parameter.Value = psCodigoPresentacion;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarCantidadPresentacion", alParameters);

        }
        public static Int32 crearPedidoDetalle(PedidoDetalleBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.PED_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_PRE_PK", SqlDbType.Int);
            parameter.Value = bean.CODIGO_PRE_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ALM_PK", SqlDbType.Int);
            parameter.Value = bean.ALM_PK;
            alParameters.Add(parameter); 

            parameter = new SqlParameter("@PRE_CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = bean.PRE_CANTIDAD;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DET_CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_CANTIDAD;
            alParameters.Add(parameter);
		
parameter = new SqlParameter("@TipoMoneda", SqlDbType.Char, 1);
            parameter.Value = bean.TipoMoneda;
            alParameters.Add(parameter);
	
			parameter = new SqlParameter("@DET_PRECIOBASE", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_PRECIOBASE_SOLES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_SOLES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_PRECIOBASE_DOLARES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_DOLARES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_DESCUENTO", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_DESCUENTO;
            alParameters.Add(parameter);
			
			parameter = new SqlParameter("@DET_MONTO", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_MONTO_SOLES", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO_SOLES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_MONTO_DOLARES", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO_DOLARES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_CANTIDAD_FRAC", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_CANTIDAD_FRAC;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_DESCRIPCION", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_DESCRIPCION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_BONIFICACION", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_BONIFICACION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_FLETE", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_FLETE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_TIPODESC", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_TIPODESC;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COD_LISTAPRECIO", SqlDbType.VarChar, 20);
            parameter.Value = bean.CodListaPrecio;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearPedidoDetallePresentacion", alParameters));
        }
        public static Int32 editarPedidoDetalle(PedidoDetalleBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@DET_PRE_PK", SqlDbType.Int);
            parameter.Value = bean.idDET;
            alParameters.Add(parameter);  

            parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
            parameter.Value = bean.PED_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_PRE_PK", SqlDbType.VarChar, 250);
            parameter.Value = bean.CODIGO_PRE_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ALM_PK", SqlDbType.Int);
            parameter.Value = bean.ALM_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRE_CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = bean.PRE_CANTIDAD;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_CANTIDAD;
            alParameters.Add(parameter);
			
			parameter = new SqlParameter("@DET_PRECIOBASE", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO;
            alParameters.Add(parameter);
			
            parameter = new SqlParameter("@DET_PRECIOBASE_SOLES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_SOLES;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DET_PRECIOBASE_DOLARES", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_DOLARES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_DESCUENTO", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_DESCUENTO;
            alParameters.Add(parameter);

			parameter = new SqlParameter("@DET_MONTO", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO;
            alParameters.Add(parameter);
			
            parameter = new SqlParameter("@DET_MONTO_SOLES", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO_SOLES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_MONTO_DOLARES", SqlDbType.VarChar, 30);
            parameter.Value = bean.DET_MONTO_DOLARES;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_CANTIDAD_FRAC", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_CANTIDAD_FRAC;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_PRECIO_FRAC", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_PRECIO_FRAC;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_DESCRIPCION", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_DESCRIPCION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_BONIFICACION", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_BONIFICACION;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_FLETE", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_FLETE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DET_TIPODESC", SqlDbType.VarChar, 20);
            parameter.Value = bean.DET_TIPODESC;
            alParameters.Add(parameter);

parameter = new SqlParameter("@TipoMoneda", SqlDbType.Char, 1);
            parameter.Value = bean.TipoMoneda;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarPedidoPresentacionDetalle", alParameters));

        }
        public static DataTable fnBuscarListaPrecio(Int32 piPre_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@pre_pk", SqlDbType.Int);
            parameter.Value =piPre_pk;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarListaPrecio", alParameters);
        }
        public static DataTable fnBuscarCantidadPresentacion(Int32 piPre_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@codigopre", SqlDbType.Int);
            parameter.Value = piPre_pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarCantidadPresentacion", alParameters);
        }
     
        public static DataTable fnBuscarStockPresentacion(Int32 piPre_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRE_PK", SqlDbType.Int);
            parameter.Value = piPre_pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarStockPresentacion", alParameters);
        }
        public static DataTable fnBuscarPresentacionesConStock(String Procodigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = Procodigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarPresentacionGeneral", alParameters);
        }
    }

    
}
