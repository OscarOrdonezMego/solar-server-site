﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

namespace Model
{
    public class VendedorModel
    {

        public static DataTable matchVendedorBean(String xsea)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = xsea; 
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_USUARIO_BUSQ", alParameters); 

        }
        public static DataTable matchVendedorBean2(String xsea, String codudu)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = xsea;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USU_PK", SqlDbType.VarChar, 250);
            parameter.Value = codudu;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_USUARIO_BUSQ_VEN", alParameters);

        }

    }
}
