﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

namespace Model
{
    public class AlmacenModel
    {
        public static DataTable findAlmacenBean(String CODIGO, String NOMBRE, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ALM_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = NOMBRE;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITADO", SqlDbType.Char, 1);
            parameter.Value = FLAG;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_ALMACEN", alParameters);
        }
        public static void borrar(String id, String HABILITADO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITAR", SqlDbType.Char, 1);
            parameter.Value = HABILITADO;
            alParameters.Add(parameter);
            SqlConnector.getDataTable("USPD_ALMACEN", alParameters);
        }
        public static Int32 crear(AlmacenBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ALM_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ALM_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ALM_DIRECCION", SqlDbType.VarChar, 100);
            parameter.Value = bean.direccion;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_ALMACEN_I", alParameters));
        }
        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_ALMACEN", alParameters);
        }
        public static DataTable getAlmacen()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("USPS_ALMACEN_LIST", alParameters);
        }
    }
}