﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
    public class CobranzaModel
    {

        public static DataTable findCobranza(String fini,String ffin,String xclicod, String xvendcod, String xcodigocliente, String flag, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@CLI_COD", SqlDbType.VarChar, 200);
            parameter.Value = xclicod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = fini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = ffin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@VEN_COD", SqlDbType.VarChar, 200);
            parameter.Value = xvendcod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLIENTE_CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = xcodigocliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_COBRANZA", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_COBRANZA", alParameters);

        }


        public static Int32 crear(CobranzaBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@COB_PK", SqlDbType.VarChar, 50);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.cobcodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.clicodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USU_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.vencodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_NUMERO", SqlDbType.VarChar, 250);
            parameter.Value = bean.numdocumento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_TIPODOC", SqlDbType.VarChar, 250);
            parameter.Value = bean.tipodocumento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagado;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG_Soles", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagadosoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT_Soles", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototalsoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG_Dolares", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagadodolares;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT_Dolares", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototaldolares;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_FECVEN", SqlDbType.VarChar, 250);
            parameter.Value = bean.fecvencimiento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_SERIE", SqlDbType.VarChar, 250);
            parameter.Value = bean.serie;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_COBRANZA_I", alParameters));

        }

        public static Int32 editar(CobranzaBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@COB_PK", SqlDbType.VarChar, 50);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.cobcodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.clicodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USU_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.vencodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_NUMERO", SqlDbType.VarChar, 250);
            parameter.Value = bean.numdocumento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_TIPODOC", SqlDbType.VarChar, 250);
            parameter.Value = bean.tipodocumento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagado;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG_Soles", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagadosoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT_Soles", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototalsoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOPAG_Dolares", SqlDbType.VarChar, 250);
            parameter.Value = bean.montopagadodolares;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_MONTOTOT_Dolares", SqlDbType.VarChar, 250);
            parameter.Value = bean.montototaldolares;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_FECVEN", SqlDbType.VarChar, 250);
            parameter.Value = bean.fecvencimiento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COB_SERIE", SqlDbType.VarChar, 250);
            parameter.Value = bean.serie;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_COBRANZA_I", alParameters));

        }

        public static String fnVerNuloStr(String poObjeto)
        {
            return poObjeto == null ? "" : poObjeto;
        }

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@COB_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_COBRANZA", alParameters);

        }
        

        public static DataTable matchActividadBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);

        }


        public static DataTable matchEstadoBean(String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = estado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);

        }

    }
        
}
