﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Model.functions
{
    public class SqlConnector
    {
        public static DataSet getDataset(string spName, ArrayList alParametros)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, spName, alParametros);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            conn.Close();
            return ds;
        }

        public static DataSet getDataset(string sql)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, sql);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            conn.Close();
            return ds;
        }

        public static DataTable getDataTable(string spName, ArrayList alParametros)
        {
            try
            {
                SqlConnection conn = getConnection();
                SqlCommand cmd = getCommand(conn, spName, alParametros);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                conn.Close();
                return dt;
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message.ToString());
                // Console.Write(e.Message.ToString());
            }
        }

        public static void GetBulkCopy(String tablaDestino, List<SqlBulkCopyColumnMapping> mapColumn, DataTable datos)
        {
            try
            {
                SqlConnection conn = getConnection();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
                {
                    bulkCopy.DestinationTableName = tablaDestino;
                    foreach (var item in mapColumn)
                    {
                        bulkCopy.ColumnMappings.Add(item);
                    }
                    try
                    {
                        // Write the array of rows to the destination.
                        bulkCopy.WriteToServer(datos);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                conn.Close();
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message.ToString());
                // Console.Write(e.Message.ToString());
            }
        }

        public static DataTable getDataTable(string sql)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, sql);
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            conn.Close();
            return dt;
        }

        public static object executeScalar(string sql, ArrayList alParametros)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, sql, alParametros);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            object obj = cmd.ExecuteScalar();
            conn.Close();
            return obj;
        }

        public static int executeNonQuery(string spName, ArrayList alParametros)
        {
            SqlConnection conn = getConnection();
            SqlCommand cmd = getCommand(conn, spName, alParametros);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            int res = cmd.ExecuteNonQuery();
            conn.Close();
            return res;
        }

        public static SqlConnection getConnection()
        {
            string strcnx = getConnectionString();
            SqlConnection conn = new SqlConnection(strcnx);
            conn.Open();
            return conn;
        }

        public static string getConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        }

        public static Hashtable getConnectionStringTable()
        {
            string[] parameters = getConnectionString().Split(';');
            string[] key_value;
            Hashtable table = new Hashtable(4);

            foreach (string parameter in parameters)
            {
                key_value = parameter.Trim().Split('=');
                table.Add(key_value[0].Trim().ToUpper(), key_value[1]);
            }

            return table;
        }

        private static SqlCommand getCommand(SqlConnection conn, string spName, ArrayList alParametros)
        {
            SqlCommand cmd = new SqlCommand(spName, conn);

            IEnumerator ieParametros = alParametros.GetEnumerator();
            while (ieParametros.MoveNext())
            { cmd.Parameters.Add((SqlParameter)ieParametros.Current); }

            return cmd;
        }

        private static SqlCommand getCommand(SqlConnection conn, string sql)
        {
            SqlCommand cmd = new SqlCommand(sql, conn);
            return cmd;
        }

        public static SqlParameter SetSqlParameter(string parameterName, SqlDbType dbType, object value)
        {
            return SetSqlParameter(parameterName, dbType, 0, value);
        }

        public static SqlParameter SetSqlParameter(string parameterName, SqlDbType dbType, int size, object value)
        {
            SqlParameter parameter;
            if (dbType == SqlDbType.Int || dbType == SqlDbType.Structured || dbType == SqlDbType.DateTime)
            {
                parameter = new SqlParameter(parameterName, dbType);
                parameter.Value = value;
            }
            else
            {
                parameter = new SqlParameter(parameterName, dbType, size);
                parameter.Value = value;
            }

            return parameter;
        }

        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}