﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
    public class TipoActividadModel
    {


        public static DataTable findTipoActividadBean(String CODIGO, String NOMBRE, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA, String BUSCARNAME, String BUSCARORDEN)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 200);
            parameter.Value = NOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 200);
            parameter.Value = FLAG;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BUSCARNAME", SqlDbType.VarChar, 200);
            parameter.Value = BUSCARNAME;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BUSCARORDEN", SqlDbType.VarChar, 200);
            parameter.Value = BUSCARORDEN;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelTipActividad", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("spS_ManDelTipActividad", alParameters);

        }


        public static Int32 crear(TipoActividadBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 250);
            parameter.Value = bean.flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ID", SqlDbType.VarChar, 250);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsTipActividad", alParameters));

        }


        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelTipActividadPorId", alParameters);

        }
    }
}