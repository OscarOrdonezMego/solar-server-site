﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;

namespace Model
{
   public class EstadoModel
    {
        public static DataSet getPk()
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter;


            return SqlConnector.getDataset("spS_ManSelFormulario", alParameters);

        }



        public static DataTable addGRUPO(String codigo, String nombre)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 4000);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 4000);
            parameter.Value = nombre;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManInsGrupo", alParameters);



        }
       public static DataSet findEstadoBean(String CODIGO, String NOMBRE, String FLAG, String CODIGOTIPOACTIVIDAD, String PAGINAACTUAL, String REGISTROPORPAGINA)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter;

           parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 60);
           parameter.Value = CODIGO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 200);
           parameter.Value = NOMBRE;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 50);
           parameter.Value = FLAG;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGOTIPOACTIVIDAD", SqlDbType.VarChar, 100);
           parameter.Value = CODIGOTIPOACTIVIDAD;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PAGINAACTUAL", SqlDbType.Int);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@REGISTROPORPAGINA", SqlDbType.Int);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           return SqlConnector.getDataset("spS_RepSelEstado", alParameters);

       }


       public static void ordenEstadoBean(String CODIGO, String ORDEN)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter;

           parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 60);
           parameter.Value = CODIGO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ORDEN", SqlDbType.VarChar, 200);
           parameter.Value = ORDEN;
           alParameters.Add(parameter);

           Convert.ToInt32(SqlConnector.executeScalar("spS_UPDOrdenEstado", alParameters));

       }

       public static DataTable findEstados(String TIPACTIVIDAD)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@TIPACTIVIDAD", SqlDbType.VarChar, 205);
           parameter.Value = TIPACTIVIDAD;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelEstados", alParameters);

       }


       public static DataTable getEstadoBeanPK(String ID)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 205);
           parameter.Value = ID;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_ManSelEstadoPorId", alParameters);

       }

       public static DataTable findCONTROLESG(String estado)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@estado", SqlDbType.VarChar, 205);
           parameter.Value = estado;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_ManSelControlesPorEstado", alParameters);

       }


       public static DataTable findOPCIONES(String GRUPO)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@GRUPO", SqlDbType.VarChar, 205);
           parameter.Value = GRUPO;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelOPCIONGrupo", alParameters);

       }


       public static DataTable findGRUPOS()
       {
           ArrayList alParameters = new ArrayList();

           return SqlConnector.getDataTable("spS_RepSelGrupos", alParameters);

       }



       public static void borrar(String id)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 4000);
           parameter.Value = id;
           alParameters.Add(parameter);

           SqlConnector.getDataTable("sps_ManDelControl", alParameters);

       }

       public static void borrarGrupo(String id)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 4000);
           parameter.Value = id;
           alParameters.Add(parameter);          

           SqlConnector.getDataTable("spS_MandelGrupo", alParameters);

       }



       public static Int32 addOPCIONES(String GRUPO, String CODIGO, String NOMBRE)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@GRUPO", SqlDbType.VarChar, 200);
           parameter.Value = GRUPO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 250);
           parameter.Value = CODIGO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 4000);
           parameter.Value = NOMBRE;
           alParameters.Add(parameter);

           return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsOpcion", alParameters));

       }


       public static Int32 addCONTROLES(String ESTADO, String CONTROL, String MAX, String ETIQUETA, String OBLIGATORIO, Int32 ORDEN, String GRUPO,String EDITABLE)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 200);
           parameter.Value = ESTADO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CONTROL", SqlDbType.VarChar, 250);
           parameter.Value = CONTROL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@MAX", SqlDbType.VarChar, 4000);
           parameter.Value = MAX;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ETIQUETA", SqlDbType.VarChar, 4000);
           parameter.Value = ETIQUETA;
           alParameters.Add(parameter);


           parameter = new SqlParameter("@OBLIGATORIO", SqlDbType.VarChar, 4000);
           parameter.Value = OBLIGATORIO;
           alParameters.Add(parameter);


           parameter = new SqlParameter("@ORDEN", SqlDbType.VarChar, 4000);
           parameter.Value = ORDEN;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@GRUPO", SqlDbType.VarChar, 4000);
           parameter.Value = GRUPO;
           alParameters.Add(parameter);


            parameter = new SqlParameter("@EDITABLE", SqlDbType.VarChar, 4000);
            parameter.Value = EDITABLE;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("sps_ManInsControl", alParameters));

       }
       public static Int32 delTableTemp()
       {
          ArrayList alParameters = new ArrayList();
           return Convert.ToInt32(SqlConnector.executeScalar("spS_DelTableTemp",alParameters));

       }
    }
}
