﻿using System;
using System.Text;
using DTS;
using System.Collections;
using Model.functions;
using Model.bean;
using System.IO;
using Microsoft.SqlServer.Dts;

namespace Model
{
    public class DTSModel
    {

        private static Hashtable obtenerConeccionDTS(Package2Class package)
        {
            Hashtable connectionTable = SqlConnector.getConnectionStringTable();

            package.GlobalVariables.Item("DB_SQL_SERVER").let_Value(connectionTable["DATA SOURCE"]);
            package.GlobalVariables.Item("DB_SQL_DB").let_Value(connectionTable["INITIAL CATALOG"]);
            package.GlobalVariables.Item("DB_SQL_USER").let_Value(connectionTable["USER ID"]);
            package.GlobalVariables.Item("DB_SQL_PASS").let_Value(connectionTable["PASSWORD"]);

            return connectionTable;
        }


        private static string findStepsErrors(Package2Class package)
        {
            int pErrorCode, pHelpContext;
            string pbstrSource, pbstrDescription, pbstrHelpFile, pbstrIDofInterfaceWithError;
            // string errores = "";
            System.Text.StringBuilder sbErrores = new System.Text.StringBuilder();

            foreach (Step2 step in package.Steps)
            {
                string stepName = step.Description;

                if (step.ExecutionStatus == DTSStepExecStatus.DTSStepExecStat_Completed)
                {
                    if (step.ExecutionResult == DTSStepExecResult.DTSStepExecResult_Failure)
                    {
                        step.GetExecutionErrorInfo(out pErrorCode, out pbstrSource, out pbstrDescription, out pbstrHelpFile, out pHelpContext, out pbstrIDofInterfaceWithError);

                        sbErrores.Append("<ul>");
                        sbErrores.Append("<li>").Append("Step Name: ").Append(stepName).Append("</li>");
                        sbErrores.Append("<li>").Append("Error Code: ").Append(pErrorCode).Append("</li>");
                        sbErrores.Append("<li>").Append("Source: ").Append(pbstrSource).Append("</li>");
                        sbErrores.Append("<li>").Append("Description: ").Append(pbstrDescription).Append("</li>");
                        sbErrores.Append("</ul>");
                    }
                }
            }
            return sbErrores.ToString();
        }

        private static string findStepsErrors2005(Microsoft.SqlServer.Dts.Runtime.Package package)
        {
            StringBuilder sbErrores = new StringBuilder();

            if (package == null) return string.Empty;

            foreach (Microsoft.SqlServer.Dts.Runtime.DtsError error in package.Errors)
            {
                sbErrores.Append("<ul>");
                sbErrores.Append("<li>").Append("Error Code: ").Append(error.ErrorCode).Append("</li>");
                sbErrores.Append("<li>").Append("Source: ").Append(error.Source).Append("</li>");
                sbErrores.Append("<li>").Append("Description: ").Append(error.Description).Append("</li>");
                sbErrores.Append("</ul><br/>");
            }

            return sbErrores.ToString();
        }

        public static FileCargaBean executeDTS(string dtsFilePath, string dtsName, string dataFilePath, string dataFileName, string tmpTable, string Nombre)
        {
            FileCargaBean BE = new FileCargaBean();
            BE.archivo = Nombre;

            try
            {
                Package2Class package = new Package2Class();

                object pVarPersistStgOfHost = null;

                package.LoadFromStorageFile(
                    dtsFilePath,
                    null,
                    null,
                    null,
                    dtsName,
                    ref pVarPersistStgOfHost);

                if (package.GlobalVariables.Item(Nombre) == null)
                {
                    BE.errorExecute += Nombre + " " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOEXISTE);
                }

                package.GlobalVariables.Item("RUTA_TXT").let_Value(dataFilePath);
                package.GlobalVariables.Item("insertados").let_Value("0");
                package.GlobalVariables.Item("actualizados").let_Value("0");
                package.GlobalVariables.Item("subidos").let_Value("0");
                package.GlobalVariables.Item("error").let_Value("");

                Hashtable connectionTable = obtenerConeccionDTS(package);
                string prefijoTransformation = "[" + connectionTable["INITIAL CATALOG"] + "].[DBO]";

                package.GlobalVariables.Item("DTS_TMP").let_Value(prefijoTransformation + ".[" + tmpTable + "]");

                package.Execute();

                BE.errorExecute = findStepsErrors(package);
                BE.insertados = Convert.ToInt32(("0" + package.GlobalVariables.Item("insertados").Value).Trim());
                BE.actualizados = Convert.ToInt32(("0" + package.GlobalVariables.Item("actualizados").Value).Trim());
                BE.subidos = Convert.ToInt32(("0" + package.GlobalVariables.Item("subidos").Value).Trim());

                BE.errorData = ("" + package.GlobalVariables.Item("error").Value).Trim();

                package.UnInitialize();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(package);
                package = null;

            }
            catch (System.Runtime.InteropServices.COMException e)
            { BE.errorExecute += e; }
            catch (System.Exception e)
            {
                BE.errorExecute += (e.Message);
            }

            return BE;

        }


        public static FileCargaBean executeDTSX(string dtsFilePath, string dtsName, string dataFilePath, string dataFileName, string tmpTable, string Nombre)
        {
            Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
            Microsoft.SqlServer.Dts.Runtime.Package package = null;
            FileCargaBean BE = new FileCargaBean();
            BE.archivo = Nombre;

            try
            {
                String dtsLocal = dtsFilePath + ".dtsx";
                package = app.LoadPackage(dtsLocal, null);

                Hashtable connectionTable = SqlConnector.getConnectionStringTable();
                String connection = String.Format("Driver={{SQL Server}};Provider=SQLOLEDB;Server={0};Database={1};user id={2};password={3}", connectionTable["DATA SOURCE"].ToString(), connectionTable["INITIAL CATALOG"].ToString(), connectionTable["USER ID"].ToString(), connectionTable["PASSWORD"].ToString());


                package.Variables["DB_TMP"].Value = tmpTable;
                package.Connections["TXT_CARGA"].ConnectionString = dataFilePath;
                package.Connections["DB"].ConnectionString = connection;

                Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();

                if (results.ToString() == "Failure")
                {
                    BE.errorExecute += findStepsErrors2005(package);
                }

                int valor = 0;
                int.TryParse(package.Variables["insertados"].Value.ToString(), out valor);
                BE.insertados = valor;

                valor = 0;
                int.TryParse(package.Variables["actualizados"].Value.ToString(), out valor);
                BE.actualizados = valor;

                valor = 0;

                valor = 0;
                if (int.TryParse(package.Variables["subidos"].Value.ToString(), out valor))
                    BE.subidos = valor;

                BE.errorData = package.Variables["error"].Value.ToString().Trim();

            }
            catch (Exception ex1)
            {
                BE.errorExecute += ex1.Message;

            }
            finally
            {
                if (package != null)
                {
                    package.Dispose();
                    package = null;
                }
            }

            return BE;
        }


        public static FileCargaBean executeDTSX_XLS(string dtsFilePath, string dtsName, string dataFilePath, string dataFileName, string tmpTable, string Nombre)
        {
            Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
            Microsoft.SqlServer.Dts.Runtime.Package package = null;
            FileCargaBean BE = new FileCargaBean();
            BE.archivo = Nombre;

            try
            {
                String dtsLocal = dtsFilePath + ".dtsx";
                package = app.LoadPackage(dtsLocal, null);


                Hashtable connectionTable = SqlConnector.getConnectionStringTable();
                String connection = String.Format("Driver={{SQL Server}};Provider=SQLOLEDB;Server={0};Database={1};user id={2};password={3}", connectionTable["DATA SOURCE"].ToString(), connectionTable["INITIAL CATALOG"].ToString(), connectionTable["USER ID"].ToString(), connectionTable["PASSWORD"].ToString());

                String extension = Path.GetExtension(dataFilePath);
                string tipoDriver = "Microsoft.ACE.OLEDB.12.0"; // Por defecto es el de 32 bits
                if (extension == ".xlsx")
                {
                    tipoDriver = "Microsoft.ACE.OLEDB.12.0";
                }
                String excel_conexion = "Driver={{SQL Server}};provider=" + tipoDriver + ";data source=" + dataFilePath + ";Extended Properties=Excel 8.0;HDR=NO;IMEX=1";

                package.Variables["DB_TMP"].Value = tmpTable;
                package.Connections["EXCEL_CONEXION"].ConnectionString = excel_conexion;
                package.Connections["DB"].ConnectionString = connection;
                package.Variables["SHEET"].Value = Nombre + "$";

                Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();

                if (results.ToString() == "Failure")
                {

                    BE.errorExecute += findStepsErrors2005(package);

                }

                int valor = 0;
                int.TryParse(package.Variables["insertados"].Value.ToString(), out valor);
                BE.insertados = valor;

                valor = 0;
                int.TryParse(package.Variables["actualizados"].Value.ToString(), out valor);
                BE.actualizados = valor;

                valor = 0;

                valor = 0;
                if (int.TryParse(package.Variables["subidos"].Value.ToString(), out valor))
                    BE.subidos = valor;


                BE.errorData = package.Variables["error"].Value.ToString().Trim();

            }
            catch (Exception ex1)
            {

                BE.errorExecute += ex1.Message;


            }
            finally
            {
                if (package != null)
                {
                    package.Dispose();
                    package = null;
                }
            }

            return BE;
        }

    }
}
