﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

/// <summary>
/// @001 GMC 13/04/2015 Se graba/actualiza campo FLGTIPO (P: Pedido - C: Cotización)
/// </summary>

namespace Model
{
    public class PedidoModel
    {

        public static DataTable findPedido(String xclicod, String xvendcod, String xFecha, String flag, String PAGINAACTUAL, String REGISTROPORPAGINA, String TipoArticulo, String codperfil, String codgrupo, String id_usu, String fTipoPedido, String procesado)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
            parameter.Value = xvendcod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_CLIENTE", SqlDbType.VarChar, 10);
            parameter.Value = xclicod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 20);
            parameter.Value = xFecha;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 200);
            parameter.Value = TipoArticulo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
            parameter.Value = fTipoPedido;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_PROCESADO", SqlDbType.Char, 1);
            parameter.Value = procesado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_PEDIDO", alParameters);

        }

         public static void borrar(String id, String habilitado)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
             parameter.Value = id;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 4000);
             parameter.Value = habilitado;
             alParameters.Add(parameter);
             SqlConnector.getDataTable("USPD_CABECERAPEDIDO", alParameters);
         }
         public static Int32 crear(PedidoBean bean)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.VarChar, 50);
             parameter.Value = bean.id;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@PED_CODIGO", SqlDbType.VarChar, 20);
             parameter.Value = bean.PED_CODIGO;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@PED_FECHA", SqlDbType.VarChar, 20);
             parameter.Value = bean.PED_FECINICIO;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@PED_CONDVENTA", SqlDbType.VarChar, 20);
             parameter.Value = bean.PED_CONDVENTA;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 20);
             parameter.Value = bean.CLI_CODIGO;
             alParameters.Add(parameter);
             parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
             parameter.Value = Int32.Parse(bean.USR_PK);
             alParameters.Add(parameter);
             //@001 I
             parameter = new SqlParameter("@FLGTIPO", SqlDbType.Char, 1);
             parameter.Value = bean.FLGTIPO;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@COD_ALMACEN", SqlDbType.Int);
             parameter.Value = (bean.ALM_PK == null)?"0":bean.ALM_PK;
             alParameters.Add(parameter);


             parameter = new SqlParameter("@TIPOARTICULO", SqlDbType.VarChar ,20);
             parameter.Value = bean.TipoArticulo;
             alParameters.Add(parameter);
            //@001 F

            parameter = new SqlParameter("@PROCESADO", SqlDbType.Char, 1);
            parameter.Value = bean.ESTADO_PROCESADO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoCambioSoles", SqlDbType.Int);
            parameter.Value = bean.TipoCambioSoles;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@TipoCambioDolares", SqlDbType.Int);
            parameter.Value = bean.TipoCambioDolares;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_PEDIDO_I", alParameters));
         }
         public static String fnVerNuloStr(String poObjeto)
         {
             return poObjeto == null ? "" : poObjeto;
         }

         public static DataTable info(String id)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.BigInt);
             parameter.Value = Int32.Parse(id);
             alParameters.Add(parameter);

             return SqlConnector.getDataTable("USPSO_PEDIDO", alParameters);

         }

         public static DataTable matchActividadBean(String cliente)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
             parameter.Value = cliente;
             alParameters.Add(parameter);

             return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);

         }

         public static DataTable matchEstadoBean(String estado)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
             parameter.Value = estado;
             alParameters.Add(parameter);

             return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);

         }

         public static DataTable fnListaAlmacen()
         {
             return SqlConnector.getDataTable("spS_ListaAlmacenPresentacion");

         }

    }

    
}
