﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;

namespace Model
{
   public class PlantillaModel
    {

       public static DataTable findFiltros(String id, String tipo)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 50);
           parameter.Value = id;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 50);
           parameter.Value = tipo;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("SPS_MANSELFILTRO", alParameters);

       }


       public static Int32 crear(String CODIGO, String NOMBRE, String LISTA)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 250);
           parameter.Value = CODIGO;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
           parameter.Value = NOMBRE;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@LISTA", SqlDbType.VarChar, 250);
           parameter.Value = LISTA;
           alParameters.Add(parameter);


           return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsUpdPlantilla", alParameters));

       }


       public static DataTable info(String id)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@codigo", SqlDbType.VarChar, 25);
           parameter.Value = id;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_ManSelPlantilla_one", alParameters);

       }

       public static DataTable findPlantillaBean()
       {
           ArrayList alParameters = new ArrayList();          

           return SqlConnector.getDataTable("spS_ManSelPlantilla", alParameters);

       }


       public static void borrar(String id, String habilitado)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
           parameter.Value = id;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
           parameter.Value = habilitado;
           alParameters.Add(parameter);

           SqlConnector.getDataTable("spS_ManDelPlantilla", alParameters);

       }
        
    }
}
