﻿using Model.bean;
using Model.functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class MapeoModel
    {
        public static DataTable ListarMaestroTabla()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPS_LISTAR_MaestroTabla", alParameters);

        }
        public static DataTable ListarMaestroTablaColumnas(Int64 idMaestro)
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList
            {
                SqlConnector.SetSqlParameter("@idMaestro", SqlDbType.BigInt,idMaestro),
            };
            return SqlConnector.getDataTable("SPS_LISTAR_MaestroTablaColumnas", alParameters);

        }
        public static DataTable ListarMaestroTablaColumnasId(Int64 idMaestroColumna)
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList
            {
                SqlConnector.SetSqlParameter("@idMaestroColumna", SqlDbType.BigInt,idMaestroColumna),
            };
            return SqlConnector.getDataTable("SPS_LISTAR_MaestroTablaColumnasID", alParameters);

        }
        public static void GuardarMaestroTabla(Int64 idTabla,List<columnasMap> columna)
        {
            DataTable oDataTableLstContac = new DataTable();
            oDataTableLstContac = SqlConnector.ConvertToDataTable(columna);
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList
            {
                SqlConnector.SetSqlParameter("@idTabla", SqlDbType.BigInt,idTabla),
                SqlConnector.SetSqlParameter("@lstCol", SqlDbType.Structured,oDataTableLstContac),
            };
            SqlConnector.getDataTable("SPS_GuardarMaestroTabla", alParameters);

        }
        public static DataTable ListarMaestrosMapeados()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPS_MapeoMaestroTabla", alParameters);

        }
        public static DataTable MapeoMaestroTablaTablaTipo(String tabla, String tipoFormato)
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList
            {
                SqlConnector.SetSqlParameter("@tabla", SqlDbType.VarChar,100,tabla),
                SqlConnector.SetSqlParameter("@tipo", SqlDbType.VarChar,1,tipoFormato),
            };
            return SqlConnector.getDataTable("SPS_MapeoMaestroTablaTablaTipo", alParameters);
        }
        public static void LimpiarTabla(String tabla)
        {
            ArrayList alParameters = new ArrayList();
            alParameters = new ArrayList
            {
                SqlConnector.SetSqlParameter("@SP_NAME", SqlDbType.VarChar,100,tabla),
            };
            SqlConnector.getDataTable("USPC_LIMPIAR_TABLA", alParameters);

        }
        public static void CargarTabla(String tablaDestino, List<MapeoBean> MapeoTabla,DataTable Datos)
        {
            List<SqlBulkCopyColumnMapping> mapColumn =new  List<SqlBulkCopyColumnMapping>();
            foreach (var item in MapeoTabla)
            {
                mapColumn.Add(new SqlBulkCopyColumnMapping
                {
                    DestinationColumn = item.columnaTabla,
                    SourceColumn = item.columnaXls,
                });
            }
            SqlConnector.GetBulkCopy(tablaDestino, mapColumn, Datos);
        }
        public static DataTable EjecutarCargaTabla(String sp)
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable(sp, alParameters);

        }
    }
}
