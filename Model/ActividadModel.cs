﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
   public class ActividadModel
    {

         public static DataTable findActividad(String codigo, String codigocliente, String codigousuario, String flag, String fechainicio, String usuarioSesion, String codigotipoactividad, String PAGINAACTUAL, String REGISTROPORPAGINA, String BUSCARNAME, String BUSCARORDEN)
        {
            ArrayList alParameters = new ArrayList();
             
            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLIENTE", SqlDbType.VarChar, 200);
            parameter.Value = codigocliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOUSUARIO", SqlDbType.VarChar, 200);
            parameter.Value = codigousuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FECHA", SqlDbType.VarChar, 200);
            parameter.Value = fechainicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOUSUARIO_SESION", SqlDbType.VarChar, 200);
            parameter.Value = usuarioSesion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG_SUPERVISOR", SqlDbType.VarChar, 200);
            parameter.Value = "T";
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOTIPOACTIVIDAD", SqlDbType.VarChar, 200);
            parameter.Value = codigotipoactividad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PAGINAACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@REGISTROPORPAGINA", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BUSCARNAME", SqlDbType.VarChar, 200);
            parameter.Value = BUSCARNAME;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BUSCARORDEN", SqlDbType.VarChar, 200);
            parameter.Value = BUSCARORDEN;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_RepSelActividadPorFiltro", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("spS_ManDelActividad", alParameters);

        }


        public static Int32 crear(ActividadBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 250);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FECHAINICIO", SqlDbType.VarChar, 250);
            parameter.Value = bean.fechainicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FECHAFIN", SqlDbType.VarChar, 250);
            parameter.Value = bean.fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOUSUARIO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigousuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 250);
            parameter.Value = "T";
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250);
            parameter.Value = bean.descripcion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOTIPOACTIVIDAD", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigotipoactividad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PROGRAMACION", SqlDbType.VarChar, 250);
            parameter.Value = bean.programacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 250);
            parameter.Value = fnVerNuloStr(bean.codigoperfil);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOCLIENTE", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigocliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ORDEN", SqlDbType.VarChar, 250);
            parameter.Value = bean.orden;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FlgDiaSemana", SqlDbType.VarChar, 250);
            parameter.Value = bean.flgdiasemana;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FlgRangoFechas", SqlDbType.VarChar, 250);
            parameter.Value = bean.flgrangofechas;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FlgCliEspecial", SqlDbType.VarChar, 250);
            parameter.Value = bean.flgcliespecial;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsActividad", alParameters));

        }

        public static String fnVerNuloStr(String poObjeto)
        {
            return poObjeto == null ? "" : poObjeto;
        }

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_RepSelActividadPorId", alParameters);

        }

        public static DataTable matchActividadBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);

        }

        public static DataTable matchEstadoBean(String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = estado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);

        }

    }
        
}
