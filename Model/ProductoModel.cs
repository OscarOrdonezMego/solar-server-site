﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

///<summary>
///@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
///@002 GMC 05/05/2015 Búsqueda de Producto por Código o Nombre
///</summary>

namespace Model
{
    public class ProductoModel
    {
        public static DataTable findProductoBean(String CODIGO, String NOMBRE, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA, String familia , String marca )
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 200);
            parameter.Value = NOMBRE;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FAMILIA", SqlDbType.VarChar, 200);
            parameter.Value = familia;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@MARCA", SqlDbType.VarChar, 200);
            parameter.Value = marca;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = FLAG;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_PRODUCTO", alParameters);
        }
        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);
            SqlConnector.getDataTable("USPD_PRODUCTO", alParameters);
        }
        public static Int32 crear(ProductoBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_STOCK", SqlDbType.VarChar, 250);
            parameter.Value = bean.stock;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_UNIDADDEFECTO", SqlDbType.VarChar, 250);
            parameter.Value = bean.unidad;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PROD_COLOR", SqlDbType.VarChar, 250);
            parameter.Value = bean.color;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_PRECIOBASE", SqlDbType.VarChar, 250);
            parameter.Value = bean.preciobase;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FAMILIA", SqlDbType.VarChar, 250);
            parameter.Value = bean.familia;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@Marca", SqlDbType.VarChar, 250);
            parameter.Value = bean.marca;
            alParameters.Add(parameter);
            //@001 I
            parameter = new SqlParameter("@DESC_MIN", SqlDbType.VarChar, 250);
            parameter.Value = bean.descuentomin;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DESC_MAX", SqlDbType.VarChar, 250);
            parameter.Value = bean.descuentomax;
            alParameters.Add(parameter);
            //@001 F
            parameter = new SqlParameter("@PrecioBaseSoles", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciobasesoles;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PrecioBaseDolares", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciobasedolares;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_PRODUCTO_I", alParameters));
        }
        public static DataTable matchProductoBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_PRODUCTO_BUSQ", alParameters);
        }
        public static DataTable matchProductoBonificacionBean(String cliente, String tipoArticulo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 3);
            parameter.Value = tipoArticulo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_PRODUCTO_BUSQ_BONIFICACION", alParameters);
        }
        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_PRODUCTO", alParameters);
        }
        public static DataTable infoxcod(String codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = codigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_PRODUCTOXCOD", alParameters);
        }
        //@002 I
        public static DataTable searchProductoBean(String prod_busqueda)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PROD_BUSQUEDA", SqlDbType.VarChar, 100);
            parameter.Value = prod_busqueda;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_PRODUCTO_SEARCH", alParameters);
        }

        public static DataTable listaProductosXFamilia(int pkfamilia)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Pk_FAMILIA", SqlDbType.VarChar, 100);
            parameter.Value = pkfamilia;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_ListaProductoxFamilia", alParameters);
        }
        //@002 F
    }
}