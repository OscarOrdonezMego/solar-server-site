﻿using System;
using System.Data;
using Model.functions;
using Model.bean;
using System.Collections;
using System.Data.SqlClient;

namespace Model
{
    public class ProductoPresentacionModel
    {
        public static DataTable fnListaProducto()
        {
            return SqlConnector.getDataTable("USPS_LISTAR_PRODUCTOS");
        }
        public static DataTable fnListaPrresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.Int);
            parameter.Value = poProductoPresentacionBean.Pagina;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.Int);
            parameter.Value = poProductoPresentacionBean.TotalFila;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@codigoProducto", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoProducto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@flag", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Estado;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 50);
            parameter.Value = poProductoPresentacionBean.Nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ListaPresentacion", alParameters);
        }
        public static Int32 fnCrearPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Id_PK", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.CodigoProducto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Cantidad", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Cantidad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@UniFracionamiento", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.UnidaddeFrancionamieno;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PrecioBase", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@Stock", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Stock;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCMIN", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCMAX", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearPresentacion", alParameters));
        }
        public static DataTable fnBuscarPresentacion(Int32 picodigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CodPresentacion", SqlDbType.VarChar, 20);
            parameter.Value = picodigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarPresentacion", alParameters);
        }
        public static Int32 fnEditarPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Id_PK_Pre", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CodigoProducto", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.CodigoProducto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombre", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Cantidad", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Cantidad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@UniFracionamiento", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.UnidaddeFrancionamieno;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Preciopre", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@Stock", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Stock;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCMIN", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCMAX", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarPresentacion", alParameters));
        }
        public static Int32 fnBorrarPresentacion(String psCodigos, String psflag)
        {
            try
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
                parameter.Value = psCodigos;
                alParameters.Add(parameter);

                parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 200);
                parameter.Value = psflag;
                alParameters.Add(parameter);
                return Convert.ToInt32(SqlConnector.executeScalar("spS_BorrarPresentacion", alParameters));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static Int32 fnBorrarListaPrecioPresentacion(String psCodigos, String psflag)
        {
            try
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
                parameter.Value = psCodigos;
                alParameters.Add(parameter);

                parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 200);
                parameter.Value = psflag;
                alParameters.Add(parameter);
                return Convert.ToInt32(SqlConnector.executeScalar("spS_BorrarListaPrecioPresentacion", alParameters));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static DataTable fnVerSiExiste(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Condicion", SqlDbType.Int);
            parameter.Value = poProductoPresentacionBean.Condicion;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_VerSiExiste", alParameters);
        }
        public static DataTable fnBuscarPresentacionProducto(String psCodigoProducto)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = psCodigoProducto;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarPresentacionProducto", alParameters);
        }
        public static DataTable fnListaPrecioPrresentacion(ListaPrecioBean poListaPrecioBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.Int);
            parameter.Value = poListaPrecioBean.Pagina;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.Int);
            parameter.Value = poListaPrecioBean.TotalFila;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@producto", SqlDbType.VarChar, 200);
            parameter.Value = poListaPrecioBean.producto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@canal", SqlDbType.VarChar, 200);
            parameter.Value = poListaPrecioBean.codigocanal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@condeventa", SqlDbType.VarChar, 200);
            parameter.Value = poListaPrecioBean.condventa;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@presentacion", SqlDbType.VarChar, 200);
            parameter.Value = poListaPrecioBean.Presentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@flag", SqlDbType.VarChar, 200);
            parameter.Value = poListaPrecioBean.flag;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_ListaPrecioPresentacion", alParameters);
        }
        public static DataTable BuscarListaPrecioPresentacion(Int32 CODIGO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PK", SqlDbType.Int);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarListaPrecioPresentacion", alParameters);
        }
        public static Int32 fnCrearListaPrecioPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGO_PRE", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_CANAL", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Canal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COND_VTA", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.CodVenta;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESC_MIN", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmin;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DESC_MAX", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearListaPrecioPresentacion", alParameters));
        }
        public static Int32 fnEditarListaPrecioPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_PRE", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_CANAL", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Canal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COND_VTA", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.CodVenta;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESC_MIN", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmin;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DESC_MAX", SqlDbType.VarChar, 200);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarListaPrecioPresentacion", alParameters));
        }

        public static DataTable fnBuscarPrecioPresentacion(String psCodigoPresentacion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@codigoPre", SqlDbType.VarChar, 20);
            parameter.Value = psCodigoPresentacion;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_BuscarPrecio", alParameters);
        }

        public static Int32 fnCrearDetallePedidoPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPedido;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRE_PK", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Cantidad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCUENTO", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Descripcion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BONIFICACION", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Bonificacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Monto", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Monto;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearPedidoPresentacionDetalle", alParameters));
        }
        public static Int32 fnEditarDetallePedidoPresentacion(ProductoPresentacionBean poProductoPresentacionBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@COD_PED_DET_PK", SqlDbType.Int);
            parameter.Value = poProductoPresentacionBean.Cod_Det_Ped_PK;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@PED_PK", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPedido;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRE_PK", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.CodigoPresentacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CANTIDAD", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Cantidad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.PrecioPre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DESCUENTO", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Descmax;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Descripcion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BONIFICACION", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Bonificacion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Monto", SqlDbType.VarChar, 20);
            parameter.Value = poProductoPresentacionBean.Monto;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarPedidoPresentacionDetalle", alParameters));
        }
        public static DataTable fnListaPedidoPresentacionDetalle(PedidoDetalleBean poPedidoDetalleBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.Int);
            parameter.Value = poPedidoDetalleBean.Pagina;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.Int);
            parameter.Value = poPedidoDetalleBean.TotalFila;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ID", SqlDbType.VarChar, 50);
            parameter.Value = poPedidoDetalleBean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IDPEDIDO", SqlDbType.VarChar, 50);
            parameter.Value = poPedidoDetalleBean.idDET;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBREPRO", SqlDbType.VarChar, 100);
            parameter.Value = poPedidoDetalleBean.PRO_NOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NOMBREPRE", SqlDbType.VarChar, 100);
            parameter.Value = poPedidoDetalleBean.NOMBRE_PRESENTACION;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 100);
            parameter.Value = poPedidoDetalleBean.Flag;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_ListaPedidoPresentacionDetalle", alParameters);
        }
        public static Int32 fnBorrarPedidoDetallePresentacion(String psCodigos, String psflag)
        {
            try
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
                parameter.Value = psCodigos;
                alParameters.Add(parameter);

                parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 200);
                parameter.Value = psflag;
                alParameters.Add(parameter);
                return Convert.ToInt32(SqlConnector.executeScalar("spS_BorrarPedidoDetallePresentacion", alParameters));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static DataTable fnBuscarPedidoDetallePresentacion(Int32 codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@coddet_pk", SqlDbType.Int);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarPedidoDetallePresentacion", alParameters);
        }

    }
}
