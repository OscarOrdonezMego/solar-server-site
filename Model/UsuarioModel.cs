﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;

namespace Model
{
    public class UsuarioModel
    {

        public static DataTable validarUsuario(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 50);
            parameter.Value = lgn;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Clave", SqlDbType.VarChar, 50);
            parameter.Value = pwd;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_TraSelLogin", alParameters);

        }

        public static DataTable rolesUsuario(Int32 idSupervisor)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IdPerfil", SqlDbType.Int);
            parameter.Value = idSupervisor;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_TraSelPerfilMenu", alParameters);

        }


        public static DataTable getLoginWeb(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@LOGIN", SqlDbType.VarChar, 50);
            parameter.Value = lgn;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PASSWORD", SqlDbType.VarChar, 100);
            parameter.Value = pwd;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_VALIDAR_USUARIO_WEB", alParameters);

        }


        public static DataTable getUsuarioRolPerfil(String perfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 50);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_RepSelUsuarioPorRolPerfil", alParameters);

        }
        public static DataTable getListaPerfil(String perfil)
        {   
            ArrayList lisparemetro=new ArrayList();
            SqlParameter parametro=new SqlParameter("@TIPO_PERFIL",SqlDbType.VarChar,20);
            parametro.Value=perfil;
            lisparemetro.Add(parametro);
            return SqlConnector.getDataTable("spS_ListaPerfil", lisparemetro);
        }
        public static DataTable getListaPerfilCrear(String perfil)
        {
            ArrayList lisparemetro = new ArrayList();
            SqlParameter parametro = new SqlParameter("@TIPO_PERFIL", SqlDbType.VarChar, 20);
            parametro.Value = perfil;
            lisparemetro.Add(parametro);
            return SqlConnector.getDataTable("spS_ListaPerfilCrear",lisparemetro);
        }

        public static DataTable findUsuarioBean(String usr_codigo, String usr_login, String usr_nombre, String usr_perfil, int paginaActual, int filasPorPagina, String psHabilitado, String psUserAplication, String perfilUsu, String codgrupo, String id_usu, String serie)
        {
            ArrayList lvArrayParameter = new ArrayList();

            SqlParameter lvParametro = new SqlParameter("@USR_CODIGO", SqlDbType.VarChar, 20);
            lvParametro.Value = usr_codigo.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@USR_LOGIN", SqlDbType.VarChar, 10);
            lvParametro.Value = usr_login.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@USR_NOMBRE", SqlDbType.VarChar, 100);
            lvParametro.Value = usr_nombre.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@USR_PERFIL", SqlDbType.VarChar, 20);
            lvParametro.Value = usr_perfil.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@Habilitado", SqlDbType.VarChar, 1);
            lvParametro.Value = psHabilitado;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@piv_pag_actual", SqlDbType.Int);
            lvParametro.Value = paginaActual;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@piv_numero_registros", SqlDbType.Int);
            lvParametro.Value = filasPorPagina;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@USER_APLICATION", SqlDbType.VarChar, 20);
            lvParametro.Value = psUserAplication.Trim();
            lvArrayParameter.Add(lvParametro);


            lvParametro = new SqlParameter("@PerfilUus", SqlDbType.VarChar, 20);
            lvParametro.Value = perfilUsu;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            lvParametro.Value = codgrupo;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@Idusu", SqlDbType.VarChar,20);
            lvParametro.Value = id_usu;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@SERIE", SqlDbType.VarChar, 5);
            lvParametro.Value = (string.IsNullOrWhiteSpace(serie) ? null : serie);
            lvArrayParameter.Add(lvParametro);

            return SqlConnector.getDataTable("USPS_USUARIO", lvArrayParameter);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_USUARIO", alParameters);

        }


        public static Int32 crear(UsuarioBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USR_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USR_LOGIN", SqlDbType.VarChar, 10);
            parameter.Value = bean.login;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USR_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USR_CLAVE", SqlDbType.VarChar, 100);
            parameter.Value = bean.clave;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@USR_PERFIL", SqlDbType.VarChar, 10);
            parameter.Value = bean.codigoperfil;
            alParameters.Add(parameter);
         
            parameter = new SqlParameter("@FlagModClave",  SqlDbType.Char, 1);
            parameter.Value = bean.flagmode;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Serie", SqlDbType.VarChar, 5);
            parameter.Value = bean.serie;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Correlativo", SqlDbType.VarChar, 10);
            parameter.Value = bean.correlativo;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_USUARIO_I", alParameters));

        }
        public static Int32 crearGrupDetalle(UsuarioBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo_pk", SqlDbType.VarChar,20);
            parameter.Value = bean.Codgrupo;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearGrupoDetalle", alParameters));

        }
        public static Int32 EditarGrupDetalle(UsuarioBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@grupo_pk", SqlDbType.Int);
            parameter.Value = bean.Codgrupo;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarGrupoDetalle", alParameters));

        }
        public static Int32 AsignarUsuario(Int32 grupo_pk,String codigousu)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@GRUP_PK", SqlDbType.Int);
            parameter.Value = grupo_pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COD_USU", SqlDbType.VarChar,20);
            parameter.Value = codigousu;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarUsuarioSupervisor", alParameters));

        }
        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_USUARIO", alParameters);

        }
        public static DataTable BuscarGrupo(String codusu)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.VarChar, 25);
            parameter.Value = codusu;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarGrupo", alParameters);

        }

        public static DataTable usuarioHabilitadoEdicionActividades(String usuarioSesion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOUSUARIO_SESION", SqlDbType.VarChar, 25);
            parameter.Value = usuarioSesion;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spM_ManUpdActividad", alParameters);

        }
        public static DataTable ListaGruposActivos(Int32 USUPK,String TIPO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USU_PK", SqlDbType.Int);
            parameter.Value = USUPK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO", SqlDbType.VarChar,6);
            parameter.Value = TIPO;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ListaGruposActivos",alParameters);

        }

        public static DataTable GruposSupervisoresAsignado(Int32 usr_pk,String tipo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
            parameter.Value = usr_pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO", SqlDbType.VarChar,20);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_GrupoSupervisores", alParameters);

        }
        public static DataTable ListaGrupoSupervisor(Int32 usr_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
            parameter.Value = usr_pk;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ListaGrupoSupervisor", alParameters);

        }
        public static DataTable GrupoVendedores(Int32 usr_pk, String tipo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
            parameter.Value = usr_pk;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 20);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_GrupoVendedores", alParameters);

        }

        public static Int32 BorrarSupervisor(Int32 USU_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USR_PK", SqlDbType.Int);
            parameter.Value = USU_pk;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_BorrarSupervisor", alParameters));

        }
        public static Int32 EliminarGrupoDetalle(Int32 USU_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Usr_PK", SqlDbType.Int);
            parameter.Value = USU_pk;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EliminarGrupoDetalle", alParameters));

        }
        public static DataTable ListaVendedor()
        {

            return SqlConnector.getDataTable("spS_ListaVendedores");

        }

        public static DataTable ListaSupervisores()
        {

            return SqlConnector.getDataTable("spS_ListaSupervisores");

        }
        public static DataTable ListaVendedorPorSupervisor(Int32 USU_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USU_PK", SqlDbType.Int);
            parameter.Value = USU_pk;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_ListaVendedorPorSupervisor",alParameters);

        }

        public static DataTable ListaVendedorPorSupervisorMultiple(Int32 USU_pk, String grupos)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@USU_PK", SqlDbType.Int);
            parameter.Value = USU_pk;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@grupos", SqlDbType.VarChar);
            parameter.Value = grupos;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("spS_ListaVendedorPorSupervisorMultiple", alParameters);

        }
    }
}
