﻿using System;
using System.Collections;

namespace Model
{
    public class ManagerConfiguracion
    {
        private static Hashtable hashConfig = new Hashtable();
        private static String version = "2.0.0";

        public static String Version
        {
            set { version = value; }
            get { return version; }
        }

        public static Hashtable HashConfig
        {
            set { hashConfig = value; }
            get { return hashConfig; }
        }
    }
}
