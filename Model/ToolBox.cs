﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class ToolBox
    {
        public static StringBuilder Option(String value,String opc,String data)
        {
            StringBuilder rpta = new StringBuilder();
            rpta.Append("<option value=\"" + value + "\" opc=\"" + opc + "\" >" + data + "</option>");
            return rpta;
        }
        public static StringBuilder Checkbox(String id, String value, String Tipo)
        {
            StringBuilder rpta=new StringBuilder();
            rpta.Append( "<input  id='" + id + "'   value='" + value + "' type='" + Tipo + "' />");
            return rpta;
        }
        public static StringBuilder CheckboxConClass(String id,String classs,String opc, String value, String Tipo)
        {
            StringBuilder rpta = new StringBuilder();
            rpta.Append("<input  id='" + id + "'  class='" + classs + "' value='"  + value + "' type='" + Tipo + "' opc='"+opc+"' />");
            return rpta;
        }
        public static StringBuilder DIV(String id, String Class, String Nombre)
        {
            StringBuilder rpta = new StringBuilder();
            rpta.Append("<div id=\"" + id + "\" class=\"" + Class + "\">" + Nombre + "</div>");
            return rpta;
        }
    }
}
