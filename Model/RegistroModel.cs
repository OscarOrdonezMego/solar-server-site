﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Model.functions;
using System.Collections;
using Model.bean;
using System.Data.SqlClient;

namespace Model
{
    public class RegistroModel
    {
        /*create procedure SPU_WEB_TRACKINGUSUARIO(
	        @usu varchar(250),
	        @fec varchar(20),
	        @page int,            
	        @rows int
        )*/
        public static DataTable reporteTracking(String usuario, String fecha, String pagina, String filas)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fec", SqlDbType.VarChar, 10);
            parameter.Value = fecha;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = Int32.Parse(pagina);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = Int32.Parse(filas);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_REPORTETRACKING", alParameters);

        }

        /*create procedure SPU_WEB_REPORTECITAS(
	        @usu varchar(20),  
	        @fecini varchar(10),  
	        @fecfin varchar(10),
	        @tipper char(1),
	        @cierre char(1),
	        @oportn char(1),      
	        @page int,          
	        @rows int 
        )*/
        public static DataTable reporteCitas(String usuario, String fechaini, String fechafin, String tipoper, String cierre, String oportunidad, String pagina, String filas)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecini", SqlDbType.VarChar, 10);
            parameter.Value = fechaini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecfin", SqlDbType.VarChar, 10);
            parameter.Value = fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipper", SqlDbType.VarChar, 1);
            parameter.Value = tipoper;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@cierre", SqlDbType.VarChar, 1);
            parameter.Value = cierre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@oportn", SqlDbType.VarChar, 1);
            parameter.Value = oportunidad;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = Int32.Parse(pagina);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = Int32.Parse(filas);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_REPORTECITAS", alParameters);

        }

        //create procedure SPU_WEB_INFOCITA(
	    //    @cod numeric
        //)
        public static DataTable infoCita(String codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod", SqlDbType.Int);
            parameter.Value = int.Parse(codigo);
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPU_WEB_INFOCITA", alParameters);

        }

        /*create procedure SPU_WEB_EXCELREPORTECITAS(
	        @usu varchar(20),  
	        @fecini varchar(10),  
	        @fecfin varchar(10),
	        @tipper char(1),
	        @cierre char(1),
	        @oportn char(1)
        )*/
        public static DataTable excelReporteCitas(String usuario, String fechaini, String fechafin, String tipoper, String cierre, String oportunidad)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecini", SqlDbType.VarChar, 10);
            parameter.Value = fechaini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fecfin", SqlDbType.VarChar, 10);
            parameter.Value = fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@tipper", SqlDbType.VarChar, 1);
            parameter.Value = tipoper;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@cierre", SqlDbType.VarChar, 1);
            parameter.Value = cierre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@oportn", SqlDbType.VarChar, 1);
            parameter.Value = oportunidad;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_EXCELREPORTECITAS", alParameters);

        }

        /*create procedure SPU_WEB_EXCELREPORTETRACKING(
	        @usu varchar(250),
	        @fec varchar(20)
        )*/
        public static DataTable excelReporteTracking(String usuario, String fecha)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usu", SqlDbType.VarChar, 20);
            parameter.Value = usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@fec", SqlDbType.VarChar, 10);
            parameter.Value = fecha;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_EXCELREPORTETRACKING", alParameters);

        }

        
    }
}
