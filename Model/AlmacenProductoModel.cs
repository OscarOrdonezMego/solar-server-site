﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

namespace Model
{
    public class AlmacenProductoModel
    {
        public static DataTable findAlmacenProductoBean(String PRO_CODIGO, String PRO_NOMBRE, String ALM_CODIGO, String FLAG, Int32 PAGINAACTUAL, Int32 REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = PRO_CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = PRO_NOMBRE;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@ALM_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = ALM_CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITADO", SqlDbType.Char, 1);
            parameter.Value = FLAG;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.Int);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.Int);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_ALMACEN_PRODUCTO", alParameters);
        }
        public static DataTable fnAlmacenPresentacion(String pro_codigo ,String pro_nombre,String alm_codigo,String flag,String presentacion, Int32 PAGINAACTUAL, Int32 REGISTROPORPAGINA)
        {
            
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.Int);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.Int);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@procodigo", SqlDbType.VarChar,20);
            parameter.Value = pro_codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@pro_nombre", SqlDbType.VarChar, 200);
            parameter.Value = pro_nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@alm_codigo", SqlDbType.VarChar, 20);
            parameter.Value = alm_codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@flag", SqlDbType.VarChar, 20);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@presentacion", SqlDbType.VarChar, 100);
            parameter.Value = presentacion;
            alParameters.Add(parameter);

            
            return SqlConnector.getDataTable("spS_ListaAlmacenPresentacionProducto", alParameters);
        }
        public static void borrar(String id, String HABILITADO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITAR", SqlDbType.Char, 1);
            parameter.Value = HABILITADO;
            alParameters.Add(parameter);
            SqlConnector.getDataTable("USPD_ALMACENPRODUCTO", alParameters);
        }
         public static void borrarProducto(String id, String HABILITADO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 200);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITAR", SqlDbType.Char, 1);
            parameter.Value = HABILITADO;
            alParameters.Add(parameter);
            SqlConnector.getDataTable("USPD_ALMACENPRODUCTO2", alParameters);
        }
        public static Int32 crear(AlmacenProductoBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_PRO_PK", SqlDbType.Int);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.PRO_CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@XML_ALMACEN", SqlDbType.VarChar);
            parameter.Value = bean.xml_almacen;
            alParameters.Add(parameter);
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_ALMACENPRODUCTO_I", alParameters));
        }
        public static DataTable info(String id, String codigopre)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_PRO_PK", SqlDbType.Int);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPREPK", SqlDbType.Int);
            parameter.Value = codigopre;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_ALMACENPRODUCTO", alParameters);
        }
        public static DataTable info6(String id, String codigopre)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ALM_PRO_PK", SqlDbType.Int);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPRO", SqlDbType.VarChar,20);
            parameter.Value = codigopre;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_ALMACENPRESENTACION", alParameters);
        }
        public static DataTable infoByProducto(String pro_codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = pro_codigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_ALMACENPRODUCTO_BY_CODPRODUCTO", alParameters);
        }
        public static DataTable infoByPresentacion(Int32 pre_pk)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PRE_PK", SqlDbType.VarChar, 20);
            parameter.Value = pre_pk;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_ALMACENPRESENTACION_BY_CODPRODUCTO", alParameters);
        }
        public static DataTable fnCrearAlmacenPresentacion(AlmacenBean poAlmacenBean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Alm_Pk", SqlDbType.VarChar,20);
            parameter.Value = poAlmacenBean.ALM_CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Pre_Pk", SqlDbType.Int);
            parameter.Value = poAlmacenBean.PrePK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Stock", SqlDbType.VarChar, 20);
            parameter.Value = poAlmacenBean.Stock;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_CrearAlmacenPresentacion", alParameters);
        }
         public static Int32 fnborrarAlmacenPrsentacion(String pro_codigo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Pre_Pk", SqlDbType.VarChar, 20);
            parameter.Value = pro_codigo;
            alParameters.Add(parameter);
            return Convert.ToInt32( SqlConnector.executeScalar("spS_BorrarAlmacenPresentacion", alParameters));
        }
         public static Int32 fnEditarAlmacenPresentacion(AlmacenBean poAlmacenBean)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@Alm_Pk", SqlDbType.VarChar,20);
             parameter.Value = poAlmacenBean.ALM_CODIGO;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@Pre_Pk", SqlDbType.Int);
             parameter.Value = poAlmacenBean.PrePK;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@Stock", SqlDbType.VarChar,20);
             parameter.Value = poAlmacenBean.Stock;
             alParameters.Add(parameter);
             return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarAlmacenPresentacion", alParameters));
         }
         public static Int32 fnEditarAlmacenProducto(AlmacenBean poAlmacenBean)
         {
             ArrayList alParameters = new ArrayList();
             SqlParameter parameter = new SqlParameter("@Alm_Pk", SqlDbType.VarChar, 20);
             parameter.Value = poAlmacenBean.ALM_CODIGO;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@Pro_Pk", SqlDbType.VarChar,20);
             parameter.Value = poAlmacenBean.PrePK;
             alParameters.Add(parameter);

             parameter = new SqlParameter("@Stock", SqlDbType.VarChar, 20);
             parameter.Value = poAlmacenBean.Stock;
             alParameters.Add(parameter);

             return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarAlmacenProducto", alParameters));
         }
    }
}