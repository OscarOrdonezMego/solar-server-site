﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

namespace Model
{
   public class RutaModel
    {

       public static DataTable findRuta(String xclicod, String xvendcod, String xcodigocliente, String flag, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@CLI_COD", SqlDbType.VarChar, 200);
            parameter.Value = xclicod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@VEN_COD", SqlDbType.VarChar, 200);
            parameter.Value = xvendcod;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CLIENTE_CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = xcodigocliente;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_RUTA", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_RUTA", alParameters);

        }


        public static Int32 crear(RutaBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_COD", SqlDbType.VarChar, 250);
            parameter.Value = bean.clicodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@VEN_COD", SqlDbType.VarChar, 250);
            parameter.Value = bean.vendcodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@VCLID_FECHA", SqlDbType.VarChar, 250);
            parameter.Value = bean.programacion;
            alParameters.Add(parameter);


            return Convert.ToInt32(SqlConnector.executeScalar("USPI_RUTA", alParameters));

        }

        public static Int32 editar(RutaBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@pk_ruta", SqlDbType.VarChar, 50);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@VEN_COD", SqlDbType.VarChar, 250);
            parameter.Value = bean.vendcodigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@ruta_diavisita", SqlDbType.VarChar, 250);
            parameter.Value = bean.programacion;
            alParameters.Add(parameter);


            return Convert.ToInt32(SqlConnector.executeScalar("USPU_RUTA", alParameters));

        }

        public static String fnVerNuloStr(String poObjeto)
        {
            return poObjeto == null ? "" : poObjeto;
        }

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ruta_pk", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_RUTA", alParameters);

        }
       

        public static DataTable matchActividadBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);
        }


        public static DataTable matchEstadoBean(String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = estado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);

        }

    }

    
}
