﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using Model.functions;
using Model.bean;

namespace Model
{
   public class GeocercaModel
    {

       public static void borrar(String id)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@id", SqlDbType.VarChar, 4000);
           parameter.Value = id;
           alParameters.Add(parameter);

           SqlConnector.getDataTable("spS_ManDelGeocerca", alParameters);

       }


       public static DataTable info(String id)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 25);
           parameter.Value = id;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelDasGeocercaPorId", alParameters);

       }
       
       public static Int32 crear(GeocercaBean bean)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 20);
           parameter.Value = bean.id;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250);
           parameter.Value = bean.nombre;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PUNTOS", SqlDbType.VarChar, 4000);
           parameter.Value = bean.punto;
           alParameters.Add(parameter);
       
           parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 250);
           parameter.Value = "T";
           alParameters.Add(parameter); 
           
           return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsGeocerca", alParameters));

       }


       public static DataTable findGeocercaBean()
       {
           ArrayList alParameters = new ArrayList();
          
           return SqlConnector.getDataTable("spS_RepSelDasGeocerca", alParameters);

       }

    }
}
