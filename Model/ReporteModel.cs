﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;

/// <summary>
/// @001 GMC 13/04/2015 Se obtiene lista tipo para Mant Pedido
/// @002 GMC 14/04/2015 Se obtiene lista tipo dirección para Mant Direcciones
/// @003 GMC 16/04/2015 Se agrega filtro Tipo Pedido
/// </summary>

namespace Model
{
   public class ReporteModel
   {
       #region Graficos
       public static DataTable GraficoAvance(String fi, String ff, String operacion)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 50);
           parameter.Value = fi;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 50);
           parameter.Value = ff;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@operacion", SqlDbType.VarChar, 50);
           parameter.Value = operacion;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usp_Grafic_BarLine", alParameters);

       }
       public static DataTable GraficoTopVendedores(String fi, String ff, String tipo1, String tipo2)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 30);
           parameter.Value = fi;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 30);
           parameter.Value = ff;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo1", SqlDbType.VarChar, 30);
           parameter.Value = tipo1;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo2", SqlDbType.VarChar, 30);
           parameter.Value = tipo2;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("USP_GRAFICO_TOP_VENDEDOR", alParameters);

       }
       public static DataTable GraficoTopClientes( String fi, String ff, String tipo1, String tipo2)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 30);
           parameter.Value = fi;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 30);
           parameter.Value = ff;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo1", SqlDbType.VarChar, 30);
           parameter.Value = tipo1;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo2", SqlDbType.VarChar, 30);
           parameter.Value = tipo2;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("USP_GRAFICO_TOP_CLIENTES", alParameters);

       }
       public static DataTable GraficoTopProductos(String fi, String ff, String tipo1, String tipo2)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 30);
           parameter.Value = fi;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 30);
           parameter.Value = ff;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo1", SqlDbType.VarChar, 30);
           parameter.Value = tipo1;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@tipo2", SqlDbType.VarChar, 30);
           parameter.Value = tipo2;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("USP_GRAFICO_TOP_PRODUCTOS", alParameters);

       }
       public static DataTable GraficoIndicadorProducto(String li,String ls,String grafico,String fi, String ff, String meta, String codprod, String operacion)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@LInferior", SqlDbType.VarChar, 30);
           parameter.Value = li;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@LSuperior", SqlDbType.VarChar, 30);
           parameter.Value = ls;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Grafico", SqlDbType.VarChar, 30);
           parameter.Value = grafico;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 30);
           parameter.Value = fi;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 30);
           parameter.Value = ff;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@Meta", SqlDbType.VarChar, 30);
           parameter.Value = meta;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@CodProd", SqlDbType.VarChar, 30);
           parameter.Value = codprod;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 30);
           parameter.Value = operacion;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_GRAFICO_TOP_INDICADOR", alParameters);

       }
       public static DataTable GraficoIndicador(String li, String ls, String grafico, String fi, String ff, String meta, String codprod, String operacion)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@LInferior", SqlDbType.VarChar, 30);
           parameter.Value = li;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@LSuperior", SqlDbType.VarChar, 30);
           parameter.Value = ls;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Grafico", SqlDbType.VarChar, 30);
           parameter.Value = grafico;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 30);
           parameter.Value = fi;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 30);
           parameter.Value = ff;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@Meta", SqlDbType.VarChar, 30);
           parameter.Value = meta;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@CodProd", SqlDbType.VarChar, 30);
           parameter.Value = codprod;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 30);
           parameter.Value = operacion;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_GRAFICO_TOP_INDICADOR", alParameters);

       }
       #endregion

       public static DataTable getUSuarios_Ruta()
       {
           ArrayList alParameters = new ArrayList();

           return SqlConnector.getDataTable("USPS_USUARIORUTA", alParameters);

       }
       public static DataSet descargaCabecera(String fecinicio, String fecfin,String TipoArticulo,String pVendedor,String pSupervisor,String pgrupo)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = fecinicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = fecfin;
           alParameters.Add(parameter);


           parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 200);
           parameter.Value = TipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Pvendedor", SqlDbType.VarChar, 200);
           parameter.Value = pVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@psupervisor", SqlDbType.VarChar, 200);
           parameter.Value = pSupervisor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@pgrupo", SqlDbType.VarChar, 200);
           parameter.Value = pgrupo;
           alParameters.Add(parameter);


           return SqlConnector.getDataset("USPS_DESCARGA", alParameters);

       }
        public static DataTable PedidoDetalleEdicionList(String cIDpedido, String cIDedicion)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
            parameter.Value = Int32.Parse(cIDpedido);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@id_edicion", SqlDbType.Int);
            parameter.Value = Int32.Parse(cIDedicion);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_PEDIDO_EDICION_DeTALLE", alParameters);

        }
        public static DataTable PedidoDetalleList(String cIDpedido)
       {
           ArrayList alParameters = new ArrayList();
           
           SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
           parameter.Value = Int32.Parse( cIDpedido);
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_det_pedido", alParameters);

       }
        public static DataTable PedidoEdicionList(String cIDpedido)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
            parameter.Value = Int32.Parse(cIDpedido);
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_PEDIDO_EDICION", alParameters);

        }
        public static DataTable PedidoDetallePresentacionList(String cIDpedido)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@PED_PK", SqlDbType.Int);
           parameter.Value = Int32.Parse(cIDpedido);
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_DET_PEDIDO_PRESENTACION", alParameters);

       }
       public static DataTable CobroDetalleList(String cCodigo)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@COB_CODIGO", SqlDbType.VarChar, 20);
           parameter.Value = cCodigo;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_pago", alParameters);

       }
       public static DataTable getSKUMasDemandados(String cOperacion, String cFechInicio, String cFechFin, String casistencia)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 50);
           parameter.Value = cOperacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 50);
           parameter.Value = cFechInicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 50);
           parameter.Value = cFechFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@asistencia", SqlDbType.VarChar, 2);
           parameter.Value = casistencia;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("usp_SelMaxSKUCantMontoProducto", alParameters);
       }

       public static DataTable getSkuMasVendidos(String cOperacion, String cFechInicio, String cFechFin, String casistencia)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 50);
           parameter.Value = cOperacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 50);
           parameter.Value = cFechInicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 50);
           parameter.Value = cFechFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@asistencia", SqlDbType.VarChar, 2);
           parameter.Value = casistencia;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("usp_SelMaxSKUCantMontoProducto", alParameters);
       }

       public static DataTable getCliMasVentas(String cOperacion, String cFechInicio, String cFechFin, String casistencia)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 50);
           parameter.Value = cOperacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 50);
           parameter.Value = cFechInicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 50);
           parameter.Value = cFechFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@asistencia", SqlDbType.VarChar, 2);
           parameter.Value = casistencia;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("usp_SelMaxCantMontoPedido", alParameters);

       }

       public static DataTable getCliMasProVendidos(String cOperacion, String cFechInicio, String cFechFin, String casistencia)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@Operacion", SqlDbType.VarChar, 50);
           parameter.Value = cOperacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 50);
           parameter.Value = cFechInicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 50);
           parameter.Value = cFechFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@asistencia", SqlDbType.VarChar, 2);
           parameter.Value = casistencia;
           alParameters.Add(parameter);


           return SqlConnector.getDataTable("usp_SelMaxCantMontoPedido", alParameters);

       }


       public static DataTable findReporteNoPedidoBean(String cVendedor, String cfechaIni, String cfechaFin, 
           String cFlgEnCobertura, String PAGINAACTUAL, String REGISTROPORPAGINA, 
           String codperfil, String codgrupo, String id_usu, String flgFecRegistro)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

            parameter = new SqlParameter("@FLGFECREGISTRO", SqlDbType.VarChar, 20);
            parameter.Value = flgFecRegistro;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("usps_repno_pedido", alParameters);

       }
       public static DataTable findReportePedidoBean(String cVendedor, String cfechaIni, String cfechaFin, 
           String cFlgEnCobertura, String cFlgBonificacion,String PAGINAACTUAL, String REGISTROPORPAGINA
           , String cFlgTipoPedido //@003 I/F
           , String tipoArticulo, String codperfil, String codgrupo, String id_usu, String flagfecregistro)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cFlgBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
           parameter.Value = cFlgTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
           parameter.Value = tipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

            parameter = new SqlParameter("@FLGFECREGISTRO", SqlDbType.VarChar, 20);
            parameter.Value = flagfecregistro;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_REPORTE_PEDIDO", alParameters);

       }

        public static DataTable findReporteProspectosXLS(String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();            

            SqlParameter parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_REPORTE_PROSPECTOS_EXCEL", alParameters);
        }

        public static DataTable ListaCordenadaPedido(String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cFlgBonificacion, String cFlgTipoPedido , String tipoArticulo, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cFlgBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
           parameter.Value = cFlgTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
           parameter.Value = tipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);
           return SqlConnector.getDataTable("spS_ListaCordenadaPedido", alParameters);

       }
       public static DataTable ListaNombreVendedorPedido(String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cFlgBonificacion, String cFlgTipoPedido, String tipoArticulo, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cFlgBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
           parameter.Value = cFlgTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
           parameter.Value = tipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);
           return SqlConnector.getDataTable("spS_ListaNombreVendedoresPedido", alParameters);

       }
       public static DataTable ListaCoordenadaVendedorPedido(String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cFlgBonificacion, String cFlgTipoPedido, String tipoArticulo, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cFlgBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
           parameter.Value = cFlgTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
           parameter.Value = tipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);
           return SqlConnector.getDataTable("spS_ListaCordenadaPedidoLine", alParameters);

       }
       public static DataTable findReportePedidoXLS(String cVendedor, String cfechaIni, String cfechaFin, 
           String cFlgEnCobertura, String cFlgBonificacion, String P_TipoArticulo, 
           String PAGINAACTUAL, String REGISTROPORPAGINA, String p_codperfil, 
           String p_codgrupo, String id_usu, String flagFecReg)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cFlgBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 3);
           parameter.Value = P_TipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = p_codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = p_codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

            parameter = new SqlParameter("@FLGFECREG", SqlDbType.VarChar, 20);
            parameter.Value = flagFecReg;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_REPORTE_PEDIDO_EXCEL", alParameters);

       }



       public static DataTable findReporteStockQuiebreXLS(String cVendedor, String cfechaIni, String cfechaFin,String cFlgEnCobertura,String cCodigoproducto,String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA,String qs_codperfil,String qs_codgrupo,String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_CODIGOPRO", SqlDbType.VarChar, 20);
           parameter.Value = cCodigoproducto;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 20);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = qs_codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = qs_codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_QUIEBRESTOCK_EXCEL", alParameters);

       }



       public static DataTable findReporteReservaXLS(String cVendedor, String cfechaIni, String cfechaFin, String cCodigoproducto, String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA, String re_codperfil, String re_codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_CODIGOPRO", SqlDbType.VarChar, 20);
           parameter.Value = cCodigoproducto;
           alParameters.Add(parameter);
           
           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 20);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = re_codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = re_codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_RESERVA_EXCEL", alParameters);
       }

       public static DataTable findReporteRutaBean(String cVendedor, String cfecha, String cAsistencia,String codperfil,String codgrupo,String id_usu )
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@piIdVendedor", SqlDbType.VarChar, 200);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PSFECHA", SqlDbType.VarChar, 200);
           parameter.Value = cfecha;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ASISTENCIA", SqlDbType.VarChar, 10);
           parameter.Value = cAsistencia;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USP_PERSELGPS_GEORUT", alParameters);

       }

       public static DataTable findReportePedidoTotalBean(String fTipoPedido, String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.VarChar, 1);
           parameter.Value = fTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_repdetpedido_todos", alParameters);

       }

       public static DataTable findReportePedidoPresentacionTotalBean(String fTipoPedido, String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo,String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 20);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 20);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 10);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 10);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 10);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_TIPO_PEDIDO", SqlDbType.Char, 1);
           parameter.Value = fTipoPedido;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPDETPEDIDO_PRESENTACION_TODOS", alParameters);

       }

       public static DataTable findReporteEquipoBean(String PAGINAACTUAL, String REGISTROPORPAGINA)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_MONITOREO_EQUIPO", alParameters);

       }

       public static DataTable findReporteCanjeBean(String TipoArticulo, String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);
           parameter = new SqlParameter("@TIPO_ARTICULO", SqlDbType.VarChar, 3);
           parameter.Value = TipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_repcanje", alParameters);

       }
       public static DataTable findReporteCobranzaBean(String cVendedor, String cfechaIni, String cfechaFin, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_repcobranza", alParameters);

       }

       public static DataTable findReporteCobranzaBeanXLS(String cVendedor, String cfechaIni, String cfechaFin, String PAGINAACTUAL, String REGISTROPORPAGINA, String co_codperfil, String co_codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = co_codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = co_codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPCOBRANZA_EXCEL", alParameters);

       }


       public static DataTable findReporteDevolucionBean(String tipoArticulo, String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPOARTICLO", SqlDbType.VarChar, 3);
           parameter.Value = tipoArticulo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("usps_repdevolucion", alParameters);

       }
       #region CombosReportes

       public static DataTable getLimiteGraficos()
       {
           ArrayList alParameters = new ArrayList();

           return SqlConnector.getDataTable("USPS_OBTENER_LIMITES_GRAF", alParameters);
       }

       public static DataTable getProductos()
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@PRODUCTO", SqlDbType.VarChar, 100);
           parameter.Value = "";
           alParameters.Add(parameter);
           return SqlConnector.getDataTable("USPS_PRODUCTOS_AUTOCOMPLET", alParameters);
       }
       public static DataTable getVendedores()
       {
           ArrayList alParameters = new ArrayList();
           return SqlConnector.getDataTable("USPS_USUARIOS_X_EMPRESA", alParameters);
       }
       public static DataTable BuscarVendedoresPorGrupo(Int32 codigogrupo)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@cod_pk_grupo", SqlDbType.Int);
           parameter.Value = codigogrupo;
           alParameters.Add(parameter);
           return SqlConnector.getDataTable("spS_BuscarVendedoresPorGrupo", alParameters);
       }
        public static DataTable BuscarVendedoresGrupoSupervisor(String codigogrupo, String codigosupervisor)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod_pk_grupo", SqlDbType.VarChar, 20);
            parameter.Value = codigogrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@cod_pk_supervisor", SqlDbType.VarChar, 20);
            parameter.Value = codigosupervisor;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_BuscarVendedoresGrupoSupervisor", alParameters);
        }
        public static DataTable getCondicionVenta()
       {

           ArrayList alParameters = new ArrayList();
           return SqlConnector.getDataTable("USPS_CONDICIONVETA_LIST", alParameters);
       }
       //@001 I
       public static DataTable getTipoPedido()
       {
           ArrayList alParameters = new ArrayList();
           return SqlConnector.getDataTable("USPS_TIPOPEDIDO_LIST", alParameters);
       }
       //@001 F
       //@002 I
       public static DataTable getTipoDireccion()
       {
           ArrayList alParameters = new ArrayList();
           return SqlConnector.getDataTable("USPS_TIPODIR_LIST", alParameters);
       }
       //@002 F
       #endregion

       public static DataTable findReporteQuiebreStockBean(String cVendedor, String cfechaIni, String cfechaFin, String cFlgEnCobertura, String cCodigoproducto, String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_COBERTURA", SqlDbType.VarChar, 10);
           parameter.Value = cFlgEnCobertura;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_CODIGOPRO", SqlDbType.VarChar, 20);
           parameter.Value = cCodigoproducto;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 20);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_QUIEBRESTOCK", alParameters);

       }


       public static DataTable findReporteReservaBean(String cVendedor, String cfechaIni, String cfechaFin, String cCodigoproducto, String cBonificacion, String PAGINAACTUAL, String REGISTROPORPAGINA, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
           parameter.Value = cVendedor;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
           parameter.Value = cfechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
           parameter.Value = cfechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_CODIGOPRO", SqlDbType.VarChar, 20);
           parameter.Value = cCodigoproducto;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IN_BONIFICACION", SqlDbType.VarChar, 20);
           parameter.Value = cBonificacion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
           parameter.Value = codgrupo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
           parameter.Value = id_usu;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_RESERVA", alParameters);

       }

       public static DataTable findAsistenciareporteBean(String fechaIni, String fechaFin, String PAGINAACTUAL, String REGISTROPORPAGINA, String BUSCARNAME, String BUSCARORDEN)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@FECHAINICIO", SqlDbType.VarChar, 200);
           parameter.Value = fechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAFIN", SqlDbType.VarChar, 200);
           parameter.Value = fechaFin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PAGINAACTUAL", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@REGISTROPORPAGINA", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@BUSCARNAME", SqlDbType.VarChar, 200);
           parameter.Value = BUSCARNAME;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@BUSCARORDEN", SqlDbType.VarChar, 200);
           parameter.Value = BUSCARORDEN;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelAsistencia", alParameters);

       }

       public static DataTable findAsistenciareporteBeanXLS(String fechaIni, String fechaFin)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@FECHAINICIO", SqlDbType.VarChar, 200);
           parameter.Value = fechaIni;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAFIN", SqlDbType.VarChar, 200);
           parameter.Value = fechaFin;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelAsistenciaXLS", alParameters);

       }

       
       public static DataTable findAsistenciareporteBeanID(String id)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 200);
           parameter.Value = id;
           alParameters.Add(parameter);
         
           return SqlConnector.getDataTable("spS_RepSelAsistenciaID", alParameters);
       }

           public static DataTable findVerReporteBean(String plantilla, String fechainicio, String fechafin, String perfil, String cliente,
           String usuario, String fechaestado, String novisita, String estadoasig, String estadoreal, String tipoactividad, String estado,
           String codigo, String situacioruta,
           String PAGINAACTUAL, String REGISTROPORPAGINA, String BUSCARNAME, String BUSCARORDEN)
       {
           ArrayList alParameters = new ArrayList();


           SqlParameter parameter = new SqlParameter("@PLANTILLA", SqlDbType.VarChar, 200);
           parameter.Value = plantilla;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAINICIO", SqlDbType.VarChar, 200);
           parameter.Value = fechainicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAFIN", SqlDbType.VarChar, 200);
           parameter.Value = fechafin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PERFIL", SqlDbType.VarChar, 200);
           parameter.Value = perfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CLIENTE", SqlDbType.VarChar, 200);
           parameter.Value = cliente;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@USUARIO", SqlDbType.VarChar, 200);
           parameter.Value = usuario;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAESTADO", SqlDbType.VarChar, 200);
           parameter.Value = fechaestado;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@NOVISITA", SqlDbType.VarChar, 200);
           parameter.Value = novisita;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADOASIGNADO", SqlDbType.VarChar, 200);
           parameter.Value = estadoasig;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADOREALIZADO", SqlDbType.VarChar, 200);
           parameter.Value = estadoreal;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPOACTIVIDAD", SqlDbType.VarChar, 200);
           parameter.Value = tipoactividad;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 200);
           parameter.Value = estado;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 200);
           parameter.Value = codigo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@SITUACIONRUTA", SqlDbType.VarChar, 200);
           parameter.Value = situacioruta;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PAGINAACTUAL", SqlDbType.VarChar, 200);
           parameter.Value = PAGINAACTUAL;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@REGISTROPORPAGINA", SqlDbType.VarChar, 200);
           parameter.Value = REGISTROPORPAGINA;
           alParameters.Add(parameter);


           parameter = new SqlParameter("@BUSCARNAME", SqlDbType.VarChar, 200);
           parameter.Value = BUSCARNAME;
           alParameters.Add(parameter);


           parameter = new SqlParameter("@BUSCARORDEN", SqlDbType.VarChar, 200);
           parameter.Value = BUSCARORDEN;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelVer", alParameters);

       }


       public static DataTable findVerReporteBeanXLS(String plantilla, String fechainicio, String fechafin, String perfil, String cliente,
         String usuario, String fechaestado, String novisita, String estadoasig, String estadoreal, String tipoactividad, String estado,
         String codigo, String situacioruta)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@PLANTILLA", SqlDbType.VarChar, 200);
           parameter.Value = plantilla;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAINICIO", SqlDbType.VarChar, 200);
           parameter.Value = fechainicio;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAFIN", SqlDbType.VarChar, 200);
           parameter.Value = fechafin;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PERFIL", SqlDbType.VarChar, 200);
           parameter.Value = perfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CLIENTE", SqlDbType.VarChar, 200);
           parameter.Value = cliente;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@USUARIO", SqlDbType.VarChar, 200);
           parameter.Value = usuario;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FECHAESTADO", SqlDbType.VarChar, 200);
           parameter.Value = fechaestado;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@NOVISITA", SqlDbType.VarChar, 200);
           parameter.Value = novisita;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADOASIGNADO", SqlDbType.VarChar, 200);
           parameter.Value = estadoasig;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADOREALIZADO", SqlDbType.VarChar, 200);
           parameter.Value = estadoreal;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPOACTIVIDAD", SqlDbType.VarChar, 200);
           parameter.Value = tipoactividad;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 200);
           parameter.Value = estado;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 200);
           parameter.Value = codigo;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@SITUACIONRUTA", SqlDbType.VarChar, 200);
           parameter.Value = situacioruta;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelVerXLS", alParameters);

       }

        public static DataSet findReporteMarcaBeanPRO(string vendedor, string fechaIni, string fechaFin, string page, string rows, string tipoArticulo, string codperfil, string codgrupo, string id_usu, string marca)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = fechaIni;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
            parameter.Value = vendedor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = page;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = rows;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
            parameter.Value = tipoArticulo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@MARCA", SqlDbType.VarChar, 20);
            parameter.Value = marca;
            alParameters.Add(parameter);

            return SqlConnector.getDataset("USPS_REPORTE_MARCA_PRO", alParameters);

        }
        public static DataSet findReporteMarcaBeanPRE(string vendedor, string fechaIni, string fechaFin, string page, string rows, string tipoArticulo, string codperfil, string codgrupo, string id_usu, string familia)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = fechaIni;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
            parameter.Value = vendedor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = page;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = rows;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
            parameter.Value = tipoArticulo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@familia", SqlDbType.VarChar, 20);
            parameter.Value = familia;
            alParameters.Add(parameter);

            return SqlConnector.getDataset("USPS_REPORTE_MARCA_PRE", alParameters);

        }
        public static DataSet findReporteFamiliaBeanPRO(string vendedor, string fechaIni, string fechaFin, string page, string rows, string tipoArticulo, string codperfil, string codgrupo, string id_usu, string familia)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = fechaIni;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
            parameter.Value = vendedor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = page;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = rows;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
            parameter.Value = tipoArticulo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@familia", SqlDbType.VarChar, 20);
            parameter.Value = familia;
            alParameters.Add(parameter);

            return SqlConnector.getDataset("USPS_REPORTE_FAMILIA_PRO", alParameters);

        }
        public static DataSet findReporteFamiliaBeanPRE(string vendedor, string fechaIni, string fechaFin, string page, string rows, string tipoArticulo, string codperfil, string codgrupo, string id_usu, string familia)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = fechaIni;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = fechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 10);
            parameter.Value = vendedor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = page;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = rows;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TipoArticulo", SqlDbType.Char, 3);
            parameter.Value = tipoArticulo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
            parameter.Value = codperfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.Int);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 20);
            parameter.Value = id_usu;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@familia", SqlDbType.VarChar, 20);
            parameter.Value = familia;
            alParameters.Add(parameter);

            return SqlConnector.getDataset("USPS_REPORTE_FAMILIA_PRE", alParameters);

        }
        public static DataTable getActividadGPSPK(String id, String tipo)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 200);
           parameter.Value = id;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 200);
           parameter.Value = tipo;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelActMapaPorId", alParameters);

       }


       public static DataTable findActividadMapaBean(String id, String tipo)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 200);
           parameter.Value = id;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 200);
           parameter.Value = tipo;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelActMapa", alParameters);
       }


       public static DataTable findCONTROLES(String codigo)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@idregistro", SqlDbType.VarChar, 200);
           parameter.Value = codigo;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_ManSel_CONTROL_REG", alParameters);

       }

       public static DataTable matchProductoBean(String xprod)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
           parameter.Value = xprod;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("USPS_PRODUCTO_BUSQ", alParameters);

       }

       public static DataTable fnSeleccionarFoto(String idRegistro)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@IDREGISTRO", SqlDbType.VarChar, 200);
           parameter.Value = idRegistro;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_RepSelFoto", alParameters);

       }
       
       public static void borrarReserva(String codigos)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@RESERVA_PK", SqlDbType.VarChar, 100);
           parameter.Value = codigos;
           alParameters.Add(parameter);
           
           SqlConnector.getDataTable("USPD_RESERVA", alParameters);

       }

       public static DataTable fnObtenerReporteConsolidado(String psFechaInicio, String psFechaFin, String psCodVendedor, String psPaginaActual, String psRegistrosPorPagina, String codperfil, String codgrupo, String id_usu)
       {
           ArrayList loLstParametros = new ArrayList();

           SqlParameter parameter = new SqlParameter("@par_codusuario", SqlDbType.VarChar, 10);
           parameter.Value = psCodVendedor;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@par_fecinicio", SqlDbType.VarChar, 20);
           parameter.Value = psFechaInicio;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@par_fecfin", SqlDbType.VarChar, 20);
           parameter.Value = psFechaFin;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@pag_actual", SqlDbType.Int);
           parameter.Value = psPaginaActual;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@numero_registros", SqlDbType.Int);
           parameter.Value = psRegistrosPorPagina;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = codperfil;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.VarChar,10);
           parameter.Value = codgrupo;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 10);
           parameter.Value = id_usu;
           loLstParametros.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_DINAMICO_CONSOLIDADO", loLstParametros);
       }

        public static DataTable isReporteDinamicoGPS()
        {
            ArrayList loLstParametros = new ArrayList();
            return SqlConnector.getDataTable("USPS_REPORTE_DINAMICO_ISGPS", loLstParametros);
        }

        


       public static DataTable fnObtenerReporteConsolidadoXLS(String psFechaInicio, String psFechaFin, String psCodVendedor, String rc_codperfil, String rc_codgrupo, String id_usu)
       {
           ArrayList loLstParametros = new ArrayList();

           SqlParameter parameter = new SqlParameter("@par_codusuario", SqlDbType.VarChar, 10);
           parameter.Value = psCodVendedor;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@par_fecinicio", SqlDbType.VarChar, 20);
           parameter.Value = psFechaInicio;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@par_fecfin", SqlDbType.VarChar, 20);
           parameter.Value = psFechaFin;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@CODPERFIL", SqlDbType.VarChar, 20);
           parameter.Value = rc_codperfil;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@CODGRUPO", SqlDbType.VarChar,10);
           parameter.Value = rc_codgrupo;
           loLstParametros.Add(parameter);

           parameter = new SqlParameter("@Idusu", SqlDbType.VarChar, 10);
           parameter.Value = id_usu;
           loLstParametros.Add(parameter);

           return SqlConnector.getDataTable("USPS_REPORTE_DINAMICO_CONSOLIDADO_EXCEL", loLstParametros);
       }
        public static DataTable findVerReporteEficienciaBean(String codigo, String fecha)
        {
            ArrayList loLstParametros = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_vendedor", SqlDbType.VarChar, 10);
            parameter.Value = codigo;
            loLstParametros.Add(parameter);

            parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 20);
            parameter.Value = fecha;
            loLstParametros.Add(parameter);

            return SqlConnector.getDataTable("spS_infovendedor", loLstParametros);
        }

        public static DataTable obtenerGpsPedido(String idPedido)
        {
            ArrayList loLstParametros = new ArrayList();

            SqlParameter parameter = new SqlParameter("@idPedido", SqlDbType.VarChar, 50);
            parameter.Value = idPedido;
            loLstParametros.Add(parameter);

            return SqlConnector.getDataTable("USPS_OBTENER_GPS_PEDIDO", loLstParametros);
        }

        //FechaIni, fechaFin, TipoDocumento, codgrupo, Vendedor, page, rows

        public static DataTable findReporteDocumentoBean(String cfechaIni, String cfechaFin,
           String TipoDocumento, String codgrupo, String cVendedor, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 200);
            parameter.Value = cfechaIni;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 200);
            parameter.Value = cfechaFin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DOC_TIPO", SqlDbType.Char, 1);
            parameter.Value = (string.IsNullOrWhiteSpace(TipoDocumento) ? null : TipoDocumento);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODGRUPO", SqlDbType.VarChar, 4000);
            parameter.Value = codgrupo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 4000);
            parameter.Value = cVendedor;
            alParameters.Add(parameter);
            

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_DOCUMENTO_HISTORIAL", alParameters);

        }
    }
}
