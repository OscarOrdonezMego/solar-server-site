﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;

namespace Model
{
    public class MantGeneralModel
    {
        
        public static DataTable getLoginWeb(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@LOGIN", SqlDbType.VarChar, 50);
            parameter.Value = lgn;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PASSWORD", SqlDbType.VarChar, 100);
            parameter.Value = pwd;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_VALIDAR_USUARIO_WEB", alParameters);

        }


        public static DataTable getUsuarioRolPerfil(String perfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 50);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_RepSelUsuarioPorRolPerfil", alParameters);

        }


        public static DataTable findGeneralBean(String gencodigo,String gennombre,String gentipo,String psHabilitado, int paginaActual, int filasPorPagina)
            
        {
            ArrayList lvArrayParameter = new ArrayList();

            SqlParameter lvParametro = new SqlParameter("@GEN_CODIGO", SqlDbType.VarChar, 20);
            lvParametro.Value = gencodigo.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@GEN_NOMBRE", SqlDbType.VarChar, 25);
            lvParametro.Value = gennombre.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@GEN_TIPO", SqlDbType.VarChar, 100);
            lvParametro.Value = gentipo.Trim();
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 1);
            lvParametro.Value = psHabilitado;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@piv_pag_actual", SqlDbType.Int);
            lvParametro.Value = paginaActual;
            lvArrayParameter.Add(lvParametro);

            lvParametro = new SqlParameter("@piv_numero_registros", SqlDbType.Int);
            lvParametro.Value = filasPorPagina;
            lvArrayParameter.Add(lvParametro);

            return SqlConnector.getDataTable("USPS_GENERAL", lvArrayParameter);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_GENERAL", alParameters);

        }


        public static Int32 crear(MantGeneralBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@GEN_GRUPO", SqlDbType.VarChar, 250);
            parameter.Value = bean.grupo;
            alParameters.Add(parameter);
            
            parameter = new SqlParameter("@GEN_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GEN_CODIGO", SqlDbType.VarChar, 20);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GEN_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GEN_NOMBRECORTO", SqlDbType.VarChar, 20);
            parameter.Value = bean.nombrecorto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLG_NUEVO", SqlDbType.VarChar, 10);
            parameter.Value = bean.nuevo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLG_BANCO", SqlDbType.VarChar, 10);
            parameter.Value = bean.banco;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLG_NRODOCUMENTO", SqlDbType.VarChar, 10);
            parameter.Value = bean.nroDocumento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLG_FECHADIFERIDA", SqlDbType.VarChar, 10);
            parameter.Value = bean.fechaDiferida;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_GENERAL_I", alParameters));

        }


        public static DataTable info(String id)
        {
            String[] xDatos;

           xDatos = id.Split('|');

            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@GEN_GRUPO", SqlDbType.VarChar, 25);
            parameter.Value = xDatos[0].ToString();
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GEN_CODIGO", SqlDbType.Char, 20);
            parameter.Value = xDatos[1].ToString().Trim();
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_GENERAL", alParameters);

        }


        public static DataTable usuarioHabilitadoEdicionActividades(String usuarioSesion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOUSUARIO_SESION", SqlDbType.VarChar, 25);
            parameter.Value = usuarioSesion;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spM_ManUpdActividad", alParameters);

        }             
      
    }
}
