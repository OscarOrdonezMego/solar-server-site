﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

///<summary>
///@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
///@002 GMC 05/05/2015 Búsqueda de Producto por Código o Nombre
///</summary>

namespace Model
{
    public class FamiliaProductoModel
    {
        public static DataTable findFamiliaProducto(String CODIGO, String NOMBRE, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FP_CODIGO", SqlDbType.VarChar, 200);
            parameter.Value = CODIGO;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FP_NOMBRE", SqlDbType.VarChar, 200);
            parameter.Value = NOMBRE;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 200);
            parameter.Value = FLAG;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPS_FAMILIA_PRODUCTO", alParameters);
        }
        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);
            SqlConnector.getDataTable("USPD_FAMILIAPRODUCTO", alParameters);
        }
        public static Int32 crear(FamiliaProductoBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FP_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FP_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@FP_NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
       
            return Convert.ToInt32(SqlConnector.executeScalar("USPW_FAMILIAPRODUCTO_I", alParameters));
        }
 

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FP_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("USPSO_FAMILIA_PRODUCTO", alParameters);
        }
        public static DataTable fnlistaFamiliaProducto()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("spS_ListarFamiliaProducto", alParameters);
        }

    }
}