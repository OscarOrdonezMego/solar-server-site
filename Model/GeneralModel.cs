﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;

namespace Model
{
    public class GeneralModel
    {
        public static String obtenerTemaActual()
        {
            WS.Service.Service loService = new WS.Service.Service();
            try
            {
                return loService.obtenerTemaActual();
            }
            finally
            {
                loService = null;
            }
        }

        public static DataTable getCultureIdioma(String tipo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 10);
            parameter.Value = tipo;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPW_PRINCIPAL_S_IDIOMA", alParameters);

        }

        public static DataTable getConfiguracionValidacion()
        {

            return SqlConnector.getDataTable("spS_ObtenertConfiguracion");
        }

       
        public static DataTable getParametrosActividad()
        {
            ArrayList alParameters = new ArrayList();

            return SqlConnector.getDataTable("spS_AuxSelParametroActividad", alParameters);

        }

        public static DataTable getActProgramdas(String fechainicio, String fechafin, String User)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechainicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 100);
            parameter.Value = fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@User", SqlDbType.VarChar, 100);
            parameter.Value = User;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG_SUPERVISOR", SqlDbType.VarChar, 100);
            parameter.Value = "T";
            alParameters.Add(parameter);

            
            return SqlConnector.getDataTable("spS_RepSelGrafActividades", alParameters);

        }

        public static DataTable getNoVisitasRea(String fechainicio, String fechafin, String User)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FechInicio", SqlDbType.VarChar, 100);
            parameter.Value = fechainicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FechFin", SqlDbType.VarChar, 100);
            parameter.Value = fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@User", SqlDbType.VarChar, 100);
            parameter.Value = User;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG_SUPERVISOR", SqlDbType.VarChar, 100);
            parameter.Value = "T";
            alParameters.Add(parameter);


            return SqlConnector.getDataTable("spS_RepSelGrafNoVisita", alParameters);

        }

        public static DataTable getActividadesRealizadas(String fechainicio, String fechafin, String Usuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FECHA_INICIO", SqlDbType.VarChar, 100);
            parameter.Value = fechainicio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FECHA_FIN", SqlDbType.VarChar, 100);
            parameter.Value = fechafin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IDUSUARIO", SqlDbType.VarChar, 100);
            parameter.Value = Usuario;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG_SUPERVISOR", SqlDbType.VarChar, 100);
            parameter.Value = "T";
            alParameters.Add(parameter);


            return SqlConnector.getDataTable("spS_RepSelChartActRealizadas", alParameters);

        }


        public static bool autoGencodigo(String vMantenimiento)
        {
            bool resultado = false;
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@MANTENIMIENTO", SqlDbType.VarChar, 100);
            parameter.Value = vMantenimiento;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@AUTOGEN", SqlDbType.Bit);
            parameter.Direction = ParameterDirection.Output;
            alParameters.Add(parameter);            

             SqlConnector.executeNonQuery("spS_AuxSelAutoGenCodigo", alParameters);
             return resultado = Convert.ToBoolean(parameter.Value);
        }


        public static String getTextoCuentas()
        {
            
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CUENTAS", SqlDbType.VarChar,100);
            parameter.Direction = ParameterDirection.Output;
            alParameters.Add(parameter);

            SqlConnector.executeNonQuery("spS_AuxSelCuentas", alParameters);
            return  parameter.Value.ToString();
        }



        public static Int32 actualizarEtiquetas(String etiqueta)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ETIQUETA", SqlDbType.VarChar, 20);
            parameter.Value = etiqueta;
            alParameters.Add(parameter);
            
            return Convert.ToInt32(SqlConnector.executeScalar("spS_AuxUpdEtiquetas", alParameters));
        }

        public static DataTable getGeneralBean(String estado, String spName, String usuarioSesion)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 100);
            parameter.Value = estado;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOUSUARIO_SESION", SqlDbType.VarChar, 100);
            parameter.Value = usuarioSesion;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG_SUPERVISOR", SqlDbType.VarChar, 100);
            parameter.Value = "T";
            alParameters.Add(parameter);


            return SqlConnector.getDataTable(spName, alParameters);
        }

        public static DataTable getGeneralBean(String estado, String spName)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 100);
            parameter.Value = estado;
            alParameters.Add(parameter);
          

            return SqlConnector.getDataTable(spName, alParameters);
        }


        public static DataTable findFiltros(String ID, String TIPO)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@ID", SqlDbType.VarChar, 100);
            parameter.Value = ID;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO", SqlDbType.VarChar, 100);
            parameter.Value = TIPO;
            alParameters.Add(parameter);


            return SqlConnector.getDataTable("SPS_MANSELFILTRO", alParameters);
        }



        public static DataTable getGeneralBean( String spName)
        {
            ArrayList alParameters = new ArrayList();
           

            return SqlConnector.getDataTable(spName, alParameters);
        }


        public static DataTable getOneParametro(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PAR_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_AuxSelParametroPorId", alParameters);

        }

        public static Int32 updateParametro(ParametroBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.idParametro;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@DESCRIPCION", SqlDbType.VarChar, 250);
            parameter.Value = bean.descripcion;
            alParameters.Add(parameter);


            parameter = new SqlParameter("@VALOR", SqlDbType.VarChar, 250);
            parameter.Value = bean.valor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COLOR", SqlDbType.VarChar, 250);
            parameter.Value = bean.color;
            alParameters.Add(parameter);

               return Convert.ToInt32(SqlConnector.executeScalar("spS_AuxUpdParametro", alParameters));

        }



        public static DataTable findCONTROLES()
        {
            ArrayList alParameters = new ArrayList();

            return SqlConnector.getDataTable("spS_RepSelControles", alParameters);

        }

        
       public static DataTable MenuPrincipal(String perfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@perfil", SqlDbType.VarChar, 10);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_WEB_USUARIO_MENU", alParameters);

        }


       public static Int32 saveConfig(String paginas)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@PAGINAS", SqlDbType.VarChar, 10);
           parameter.Value = paginas;
           alParameters.Add(parameter);

           return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_CONFIG_EDITAR", alParameters));

       }

       public static Int32 saveConfigMenu(int idUsuario, int idOpcion, String perfil, int Estado)
       {
           ArrayList alParameters = new ArrayList();

           SqlParameter parameter = new SqlParameter("@IDUSUARIO", SqlDbType.Int);
           parameter.Value = idUsuario;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@IDOPCION", SqlDbType.BigInt);
           parameter.Value = idOpcion;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@PERFIL", SqlDbType.VarChar, 10);
           parameter.Value = perfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@ESTADO", SqlDbType.Int);
           parameter.Value = Estado;
           alParameters.Add(parameter);

           return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_CONFIG_PERMISOS", alParameters));

       }
        
       public static DataTable PermisoOpciones(String idUsuario)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@idUsuario", SqlDbType.VarChar, 10);
           parameter.Value = idUsuario;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("SPU_WEB_LIST_PERMISOS_MENU", alParameters);
       }
     
    }
}
