﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;
using System.Collections;

///<summary>
///@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
///</summary>

namespace Model
{
    public class ListaPrecioModel
    {

        public static DataTable findListaPrecio(String producto, String canal, String condventa, String flag, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();


            SqlParameter parameter = new SqlParameter("@PRODUCTO", SqlDbType.VarChar, 200);
            parameter.Value = producto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CANAL", SqlDbType.VarChar, 200);
            parameter.Value = canal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COND_VTA", SqlDbType.VarChar, 200);
            parameter.Value = condventa;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Habilitado", SqlDbType.VarChar, 200);
            parameter.Value = flag;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_pag_actual", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@piv_numero_registros", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_LISTAPRECIOS", alParameters);

        }

        public static void borrar(String id, String habilitado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOS", SqlDbType.VarChar, 4000);
            parameter.Value = id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@HABILITAR", SqlDbType.VarChar, 4000);
            parameter.Value = habilitado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("USPD_LISTAPRECIOS", alParameters);

        }


        public static Int32 crear(ListaPrecioBean bean)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 50);
            parameter.Value = bean.codigoproducto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CANAL", SqlDbType.VarChar, 50);
            parameter.Value = bean.codigocanal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.precio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COND_VTA", SqlDbType.VarChar, 250);
            parameter.Value = bean.condventa;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GRUPO_ECONOMICO", SqlDbType.VarChar, 20);
            parameter.Value = bean.grupoeconomico;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO_SOLES", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciosoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO_DOLARES", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciodolares;
            alParameters.Add(parameter);

            //@001 I
            parameter = new SqlParameter("@DESC_MIN", SqlDbType.VarChar, 250);
            parameter.Value = bean.descmin;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DESC_MAX", SqlDbType.VarChar, 250);
            parameter.Value = bean.descmax;
            alParameters.Add(parameter);
            //@001 F

            return Convert.ToInt32(SqlConnector.executeScalar("USPI_LISTAPRECIO", alParameters));

        }

        public static Int32 editar(ListaPrecioBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PK", SqlDbType.VarChar, 50);
            parameter.Value = bean.id;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 50);
            parameter.Value = bean.codigoproducto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_CANAL", SqlDbType.VarChar, 50);
            parameter.Value = bean.codigocanal;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.precio;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 250);
            parameter.Value = "";
            alParameters.Add(parameter);

            parameter = new SqlParameter("@COND_VTA", SqlDbType.VarChar, 250);
            parameter.Value = bean.condventa;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@GRUPO_ECONOMICO", SqlDbType.VarChar, 20);
            parameter.Value = bean.grupoeconomico;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO_SOLES", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciosoles;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRECIO_DOLARES", SqlDbType.VarChar, 20);
            parameter.Value = bean.preciodolares;
            alParameters.Add(parameter);

            //@001 I
            parameter = new SqlParameter("@DESC_MIN", SqlDbType.VarChar, 250);
            parameter.Value = bean.descmin;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DESC_MAX", SqlDbType.VarChar, 250);
            parameter.Value = bean.descmax;
            alParameters.Add(parameter);
            //@001 F

            return Convert.ToInt32(SqlConnector.executeScalar("USPU_LISTAPRECIOS", alParameters));

        }

        public static String fnVerNuloStr(String poObjeto)
        {
            return poObjeto == null ? "" : poObjeto;
        }

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_LISTAPRECIOS", alParameters);
        }
        public static DataTable matchActividadBean(String cliente)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = cliente;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelActividadPorNombre", alParameters);
        }
        public static DataTable matchEstadoBean(String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = estado;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_ManSelEstadoPorNombre", alParameters);
        }
        public static Int32 CrearDescuentoVolumen(DescuentoVolumen bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IDLISTAPRECIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.IDLISTAPRECIO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOPROD", SqlDbType.VarChar, 50);
            parameter.Value = bean.CODIGOPROD;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGOPRE", SqlDbType.VarChar, 50);
            parameter.Value = bean.CODIGOPRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@RMIN", SqlDbType.VarChar, 50);
            parameter.Value = bean.RMIN;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@RMAX", SqlDbType.VarChar, 250);
            parameter.Value = bean.RMAX;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@DSCTOMIN", SqlDbType.VarChar, 250);
            parameter.Value = bean.DSCTOMIN;
            alParameters.Add(parameter);

            //@001 I
            parameter = new SqlParameter("@DSCTOMAX", SqlDbType.VarChar, 250);
            parameter.Value = bean.DSCTOMAX;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@TIPOARTICULO", SqlDbType.VarChar, 250);
            parameter.Value = bean.TIPOARTICULO;
            alParameters.Add(parameter);
            //@001 F

            return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearDescuentoVolumen", alParameters));

        }
        public static Int32 borrarDescuentoVolumen(Int32 idListaprecio)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IdListaPrecio", SqlDbType.Int);
            parameter.Value = idListaprecio;
            alParameters.Add(parameter);

            return Convert.ToInt32( SqlConnector.executeScalar("spS_EliminarDescuentoVolumen", alParameters));

        }
        public static DataTable BuscarDescuentoVolumen(Int32 idlistaprecio)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@idListaPrecio", SqlDbType.Int);
            parameter.Value = idlistaprecio;
            alParameters.Add(parameter);


            return SqlConnector.getDataTable("spS_BuscarDescuentoVolumen", alParameters);
        }
    }
}