﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace Model
{
    public class CargaExcelModel
    {
        public static DataTable GetExcelSheetNames(String excelFile)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;

            try
            {
                //string tipoDriver = "Microsoft.Jet.OLEDB.4.0"; // Por defecto es el de 32 bits
                string tipoDriver = "Microsoft.ACE.OLEDB.12.0"; // Por defecto es el de 32 bits
                string ExtPor = "Excel 8.0;";
                String extension = Path.GetExtension(excelFile);
                if (extension == ".xlsx")
                {
                    tipoDriver = "Microsoft.ACE.OLEDB.12.0";
                    ExtPor = "Excel 12.0";
                }
                //String excel_conexion = "Driver={{SQL Server}};provider=" + tipoDriver + ";data source=" + excelFile + ";Extended Properties=Excel 8.0;HDR=NO;IMEX=1";

                string excel_conexion = "provider=" + tipoDriver + ";Data Source='" + excelFile + "';Extended Properties=" + ExtPor + ";";

                ////para archivos de 97-2003 usar la siguiente cadena
                //string excel_conexion = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + archivo + "';Extended Properties=Excel 8.0;";

                // Connection String. Change the excel file to the file you
                // will search.
                //String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                //  "Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
                // Create connection object by using the preceding connection string.
                objConn = new OleDbConnection(excel_conexion);
                // Open connection with the database.
                objConn.Open();
                // Get the data table containg the schema guid.
                dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if (dt == null)
                {
                    return null;
                }

                //List<String> excelSheets = new List<String>();// new String[dt.Rows.Count];
                //int i = 0;

                //// Add the sheet name to the string array.
                //foreach (DataRow row in dt.Rows)
                //{
                //    excelSheets.Add(row["TABLE_NAME"].ToString())
                //    //excelSheets[i] = row["TABLE_NAME"].ToString();
                //    i++;
                //}

                //// Loop through all of the sheets if you want too...
                ////for (int j = 0; j < excelSheets.Length; j++)
                ////{
                ////    // Query each excel sheet.
                ////}

                return dt;
            }
            catch (Exception ex)
            {
                //new LoggerHelper().Debug("Debug", "Error al obtener datos del archivo: " + file);
                throw new Exception("Error al obtener datos del archivo", ex);
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }


        public static DataTable GetExcelData(String excelFile, String Tabla, String Columnas, String Consulta)
        {
            OleDbConnection objConn = null;
            DataTable dt = null;

            try
            {
                //string tipoDriver = "Microsoft.Jet.OLEDB.4.0";
                string tipoDriver = "Microsoft.ACE.OLEDB.12.0";
                string ExtPor = "Excel 8.0;";
                String extension = Path.GetExtension(excelFile);
                if (extension == ".xlsx")
                {
                    tipoDriver = "Microsoft.ACE.OLEDB.12.0";
                    ExtPor = "Excel 12.0";
                }

                string excel_conexion = "provider=" + tipoDriver + ";Data Source='" + excelFile + "';Extended Properties=" + ExtPor + ";";
                string Query = string.Format("Select {1} FROM [{0}] where 1=1 {2}", Tabla + "$", Columnas, Consulta);
                //Query = string.Format("Select * FROM [{0}]", Tabla+"$");

                objConn = new OleDbConnection(excel_conexion);
                objConn.Open();
                DataSet ds = new DataSet();
                OleDbDataAdapter oda = new OleDbDataAdapter(Query, objConn);
                objConn.Close();
                oda.Fill(ds);
                dt = ds.Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener datos del archivo");
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }
    }
}
