﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.bean;
using Model.functions;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace Model
{
    public class BonificacionModel
    {
          public static DataTable crearBonificacion(BonificacionBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOPRE", SqlDbType.VarChar, 20);
            parameter.Value = bean.PRE_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 100);
            parameter.Value = bean.PRO_CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.PRO_NOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_CANTIDAD", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_CANTIDAD;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_MAXIMO", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_MAXIMO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_ITEMS", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_ITEMS;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_EDITABLE", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_EDITABLE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO_ARTICULO", SqlDbType.VarChar, 100);
            parameter.Value = bean.TIPO_ARTICULO;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_CrearBonificacion_Web", alParameters);
        }
          public static DataTable ListaBonificacion(BonificacionBean poBonificacionBean)
          {
              ArrayList alParameters = new ArrayList();
              SqlParameter parameter = new SqlParameter("@Piv_pagina_actual", SqlDbType.VarChar, 3);
              parameter.Value = poBonificacionBean.Pagina;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@Piv_numero_registro", SqlDbType.VarChar, 3);
              parameter.Value = poBonificacionBean.TotalPaginas;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 3);
              parameter.Value = poBonificacionBean.TIPO_ARTICULO;
              alParameters.Add(parameter); 
              
              parameter = new SqlParameter("@Codigo", SqlDbType.VarChar, 20);
              parameter.Value = poBonificacionBean.PRO_CODIGO;
              alParameters.Add(parameter);


              return SqlConnector.getDataTable("spS_ListaBonificacion", alParameters);
          }
          public static Int32 crearBonificacionDetalle(BonificacionBean bean)
          {
              ArrayList alParameters = new ArrayList();
              SqlParameter parameter = new SqlParameter("@CODIGO_BON_PK", SqlDbType.VarChar, 20);
              parameter.Value = bean.CODIGO_BON_PK;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 100);
              parameter.Value = bean.CONPRO_CODIGO;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 100);
              parameter.Value = bean.CONPRO_NOMBRE;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@BON_CANTIDAD", SqlDbType.VarChar, 100);
              parameter.Value = bean.CONBON_CANTIDAD;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@EXISTE", SqlDbType.VarChar, 100);
              parameter.Value = bean.Existe;
              alParameters.Add(parameter);

              parameter = new SqlParameter("@TIPO_ARTICULO", SqlDbType.VarChar, 100);
              parameter.Value = bean.TIPO_ARTICULO;
              alParameters.Add(parameter);

              return Convert.ToInt32(SqlConnector.executeScalar("spS_CrearBonnificacionDetalle_web", alParameters));
          }
        public static DataTable BuscarBonificacion(BonificacionBean bean)
          {
              ArrayList alParameters = new ArrayList();
              SqlParameter parameter = new SqlParameter("@BON_PK", SqlDbType.VarChar, 20);
              parameter.Value = bean.CODIGO_BON_PK;
              alParameters.Add(parameter);

              return SqlConnector.getDataTable("spS_BuscarBonificacion", alParameters);
          }
        public static DataTable BuscarBonificacionDetalle(BonificacionBean bean)
          {
              ArrayList alParameters = new ArrayList();
              SqlParameter parameter = new SqlParameter("@BON_PK", SqlDbType.VarChar, 20);
              parameter.Value = bean.CODIGO_BON_PK;
              alParameters.Add(parameter);

              return SqlConnector.getDataTable("spS_BuscarBonificacionDetalle", alParameters);
          }
        public static Int32 EditarBonificacion(BonificacionBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@BON_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.CODIGO_BON_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_CANTIDAD", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_CANTIDAD;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_MAXIMO", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_MAXIMO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_ITEMS", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_ITEMS;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_EDITABLE", SqlDbType.VarChar, 100);
            parameter.Value = bean.BON_EDITABLE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO_ARTICULO", SqlDbType.VarChar, 100);
            parameter.Value = bean.TIPO_ARTICULO;
            alParameters.Add(parameter);

            return Convert.ToInt32( SqlConnector.executeScalar("spS_EditarBonificacion_Web", alParameters));
        }
        public static Int32 EditarBonificacionDetalle(BonificacionBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@BON_DET_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.BON_DET_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@CODIGO_BON_PK", SqlDbType.VarChar, 20);
            parameter.Value = bean.CODIGO_BON_PK;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 100);
            parameter.Value = bean.CONPRO_CODIGO;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PRO_NOMBRE", SqlDbType.VarChar, 100);
            parameter.Value = bean.CONPRO_NOMBRE;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@BON_CANTIDAD", SqlDbType.VarChar, 100);
            parameter.Value = bean.CONBON_CANTIDAD;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@EXISTE", SqlDbType.VarChar, 100);
            parameter.Value = bean.Existe;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@TIPO_ARTICULO", SqlDbType.VarChar, 100);
            parameter.Value = bean.TIPO_ARTICULO;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("spS_EditarBonnificacionDetalle_web", alParameters));
        }
           public static DataTable EliminarBonificacionDetalle(Int32 bean)
          {
              ArrayList alParameters = new ArrayList();
              SqlParameter parameter = new SqlParameter("@BON_PK", SqlDbType.VarChar, 20);
              parameter.Value = bean;
              alParameters.Add(parameter);

              return SqlConnector.getDataTable("spS_EliminarBonificacionDetalle", alParameters);
          }
           public static DataTable EliminarBonificacion(Int32 bean,String tipoArticulo)
           {
               ArrayList alParameters = new ArrayList();
               SqlParameter parameter = new SqlParameter("@BON_PK", SqlDbType.VarChar, 20);
               parameter.Value = bean;
               alParameters.Add(parameter);


               parameter = new SqlParameter("@TipoArticulo", SqlDbType.VarChar, 3);
               parameter.Value = tipoArticulo;
               alParameters.Add(parameter);

               return SqlConnector.getDataTable("spS_EliminarBonificacion_web", alParameters);
           }
           public static DataTable fnBuscarPresentacionBonificacion(String codigoProducto)
           {
               ArrayList alParameters = new ArrayList();
               SqlParameter parameter = new SqlParameter("@PRO_CODIGO", SqlDbType.VarChar, 20);
               parameter.Value = codigoProducto;
               alParameters.Add(parameter);

               return SqlConnector.getDataTable("spS_BuscarPresentacionBonificacion", alParameters);
           }
        
    }
}
