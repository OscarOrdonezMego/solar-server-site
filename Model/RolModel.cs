﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Model.functions;

namespace Model
{
   public class RolModel
    {

       public static DataTable getUsuarioRolPerfil(String perfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 25);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("spS_RepSelUsuarioPorRolPerfil", alParameters);

        }

       public static DataTable getNivelPerfil(String perfil)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 25);
           parameter.Value = perfil;
           alParameters.Add(parameter);

           return SqlConnector.getDataTable("spS_ManSelPerfilNivel", alParameters);

       }

       public static Int32 crear(String roles, String cadcrear,
            String cadeditar, String cadeliminar, String codigoperfil, String codigoHome)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@ROLVALUES", SqlDbType.VarChar, 550);
           parameter.Value = roles;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CREARVALUES", SqlDbType.VarChar, 550);
           parameter.Value = cadcrear;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@EDITARVALUES", SqlDbType.VarChar, 550);
           parameter.Value = cadeditar;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@DROPVALUES", SqlDbType.VarChar, 550);
           parameter.Value = cadeliminar;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.VarChar, 550);
           parameter.Value = codigoperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGOHOME", SqlDbType.VarChar, 550);
           parameter.Value = codigoHome;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@FLAG", SqlDbType.VarChar, 250);
           parameter.Value = "T";
           alParameters.Add(parameter);

           return Convert.ToInt32(SqlConnector.executeScalar("spS_ManInsRolPerfil", alParameters));

       }


       public static Int32 updatePerfilPorRol(String codigoperfil, String codigorol)
       {
           ArrayList alParameters = new ArrayList();
           SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 550);
           parameter.Value = codigoperfil;
           alParameters.Add(parameter);

           parameter = new SqlParameter("@CODIGOROL", SqlDbType.VarChar, 550);
           parameter.Value = codigorol;
           alParameters.Add(parameter);

           return Convert.ToInt32(SqlConnector.executeScalar("spS_ManUpdPerfilPorRol", alParameters));

       }             
    }
}
