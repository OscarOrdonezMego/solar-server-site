﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using Model.functions;
using Model.bean;
using System.Data;

/// <summary>
/// @001 GMC 12/04/2015 Ajustes para actualizar Campos Adicionales Cliente
/// @002 GMC 14/04/2015 Ajustes para actualizar Campos DIR_TIPO (P: Pedido, D: Despacho) y FECCREACION en Mant Direcciones
/// </summary>

namespace Model
{
    public class ClienteProspeccionModel
    {

        public static DataTable findClienteDireccionBean(String xID, String xCODDIR, String xDIRECCION, String FLAG, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@IDCLIENTE", SqlDbType.Int);
            parameter.Value = Int32.Parse(xID);
            alParameters.Add(parameter);

            if (xCODDIR.Trim().Length != 0)
            {
                parameter = new SqlParameter("@CODDIR", SqlDbType.VarChar, 10);
                parameter.Value = xCODDIR;
                alParameters.Add(parameter);
            }
            if (xDIRECCION.Trim().Length != 0)
            {
                parameter = new SqlParameter("@DIRECCION", SqlDbType.VarChar, 00);
                parameter.Value = xDIRECCION;
                alParameters.Add(parameter);
            }

            parameter = new SqlParameter("@HABILITADO", SqlDbType.VarChar, 1);
            parameter.Value = FLAG;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 200);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 200);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_CLIENTE_DIRECCIONES_LIST_PROSPECCION", alParameters);
            // LISTO

        }

        public static DataTable findClienteProspeccionBean(String codvendedor, String prospecto, String fini, String ffin, String PAGINAACTUAL, String REGISTROPORPAGINA)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@IN_FECINICIO", SqlDbType.VarChar, 50);
            parameter.Value = fini;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_FECFIN", SqlDbType.VarChar, 50);
            parameter.Value = ffin;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_USUARIO", SqlDbType.VarChar, 20);
            parameter.Value = codvendedor;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IN_PROSPECTO", SqlDbType.VarChar, 20);
            parameter.Value = prospecto;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_PAG_ACTUAL", SqlDbType.VarChar, 50);
            parameter.Value = PAGINAACTUAL;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@PIV_NUMERO_REGISTROS", SqlDbType.VarChar, 50);
            parameter.Value = REGISTROPORPAGINA;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPS_PROSPECTO", alParameters);
        }

        

        public static DataTable fnListarTipoCliente()
        {
            ArrayList poLstParametros = new ArrayList();
            return SqlConnector.getDataTable("spS_ListarTipoCliente_Prospeccion", poLstParametros);
            //LISTO
        }

        public static DataTable info(String id)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_PK", SqlDbType.VarChar, 25);
            parameter.Value = id;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("USPSO_PROSPECTO", alParameters);

        }

        public static Int32 crear(ClienteProspeccionBean bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CLI_CODIGO", SqlDbType.VarChar, 250);
            parameter.Value = bean.codigo;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_NOMBRE", SqlDbType.VarChar, 250);
            parameter.Value = bean.nombre;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_DIRECCION", SqlDbType.VarChar, 250);
            parameter.Value = bean.direccion;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@TCLI_COD", SqlDbType.VarChar, 250);
            parameter.Value = bean.tipocliente;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_GIRO", SqlDbType.VarChar, 250);
            parameter.Value = bean.giro;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_PK", SqlDbType.VarChar, 250);
            parameter.Value = bean.id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_SECUENCIA", SqlDbType.Int);
            parameter.Value = bean.secuencia;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_LATITUD", SqlDbType.VarChar, 250);
            parameter.Value = bean.latitud;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_LONGITUD", SqlDbType.VarChar, 250);
            parameter.Value = bean.longitud;
            alParameters.Add(parameter);
            //@001 I
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL1", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional1;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL2", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional2;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL3", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional3;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL4", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional4;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CLI_CAMPO_ADICIONAL5", SqlDbType.VarChar, 100);
            parameter.Value = bean.CampoAdicional5;
            alParameters.Add(parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("USPW_PROSPECTO_CLIENTE_I", alParameters));

        }
    }
}
