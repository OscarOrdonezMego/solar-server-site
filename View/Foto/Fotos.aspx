﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Foto.Foto_Fotos" Codebehind="Fotos.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel" runat="server">Visor de Fotos</h3>
        </div>
        <div id="myModalContent" class="modal-body">
            <div id="myCarousel" class="carousel slide">
                <asp:Literal ID="ltrFoto" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="modal-footer">
            <div id="cz-modal-fotos-buttons">
                <a class="carousel-control" href="#myCarousel" data-slide="prev">‹</a>
                <a class="carousel-control cz-modal-fotos-buttons-text">1</a>
                <a class="carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
            <asp:Literal ID="ltrIndic" runat="server"></asp:Literal>
        </div>
    </form>
    <script type="text/javascript">
        cz_foto_event();
    </script>
</body>
</html>
