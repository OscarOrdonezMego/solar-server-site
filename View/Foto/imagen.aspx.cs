﻿using System;
using System.Collections.Generic;
using System.IO;
using Model.bean;
using System.Drawing;
using System.Web.UI;

namespace Pedidos_Site.Foto
{
    public partial class Foto_imagen : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Int32 pagactual = Convert.ToInt32(Request.QueryString["item"].ToString());
                byte[] imageData = null;
                MemoryStream ms = null;

                imageData = ((List<FotoDato>)Session["FotosLista"])[pagactual].Foto;

                if (imageData != null)
                {

                    ms = new MemoryStream(imageData);

                    System.Drawing.Image loDrawImage = System.Drawing.Image.FromStream(ms);

                    //-------------------------------------------
                    //Decirle al Image que pinte en la pagina
                    //-------------------------------------------

                    System.Drawing.Image thumbnail = new Bitmap(500, 379);
                    System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(thumbnail);
                    graphic.DrawImage(loDrawImage, 0, 0, 500, 379);
                    thumbnail.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Response.ContentType = "image/jpeg";

                    //loDrawImage.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }
    }
}