﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller;
using System.Text;
using Model.bean;
using System.Web.UI;

namespace Pedidos_Site.Foto
{
    public partial class Foto_Fotos : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                String codigo = dataJSON["codigo"];
                List<FotoDato> lista = ReporteController.fnSeleccionarFoto(codigo);

                Session["FotosLista"] = lista;
                int cantidaFotos = lista.Count;
                ltrFoto.Text = loadFoto(cantidaFotos);
            }
        }

        private string loadFoto(int cantidaFotos)
        {
            StringBuilder indicadores = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            indicadores.Append("<ol class='carousel-indicators'>\n");
            for (int i = 0; i < cantidaFotos; i++)
            {
                indicadores.Append("<li data-target='#myCarousel' data-slide-to='" + i.ToString() + "'><img src='../../foto/imagen.aspx?item=" + i.ToString() + "'></li>\n");
            }

            indicadores.Append("</ol>\n");

            ltrIndic.Text = indicadores.ToString();

            sb.Append(" <div class='carousel-inner'>\n");
            for (int i = 0; i < cantidaFotos; i++)
            {
                if (i == 0)
                {
                    sb.Append("<div class='item active'>\n");
                    sb.Append("<img id='img" + i.ToString() + "' src='../../foto/imagen.aspx?item=" + i.ToString() + "'/>\n");
                    sb.Append(" </div>\n");
                }
                else
                {
                    sb.Append("<div class='item'>\n");
                    sb.Append("<img id='img" + i.ToString() + "' src='../../foto/imagen.aspx?item=" + i.ToString() + "'/>\n");
                    sb.Append(" </div>\n");
                }
            }
            sb.Append("</div>\n");

            return sb.ToString();
        }
    }
}