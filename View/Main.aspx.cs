﻿using System;
using System.Collections.Generic;
using System.IO;
using Controller;
using Model.bean;
using System.Text;
using Model;
using System.Linq;
using System.Configuration;

/// <summary>
/// @001 GMC 03/05/2015 Validar visualización de menú Almacén y Producto por Almacén
/// </summary>
namespace Pedidos_Site
{
    public partial class Main : PageControllerNtrack
    {
        //@001 I
        private String valorConfigMostrarAlmacen = "F";
        //@001 F

        protected override void initialize()
        {
            String accion = Request["acc"];

            if (accion == null)
            { accion = ""; }
            //string urlhome = "../Dashboard/Default.aspx"; 
            string urlhome = ConfigurationManager.AppSettings["Url_Inicio"];//"http://localhost/server-reportes/Inicio.aspx"; 
            //string urlhome = "inicio.aspx";
            if (accion.Equals("ACT"))
            {
                string aplicationPath = Request.ApplicationPath;
                string entrada = Request.UrlReferrer.AbsolutePath;
                String lsScript = "addnotify('notify', 'Configuracion actualizada exitosamente', 'configsatisfact');";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }

            subInicializarMenu(urlhome);
            InicializarFuncion();
        }

        private void InicializarFuncion()
        {
            List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
            loLstConfiguracionBean = ConfiguracionController.subObtenerDatosConfiguracion();
        }

        public override bool subLogin(String usuario, String password)
        {
            UsuarioBean bean = UsuarioController.validarUsuario(usuario, password);
            if (!bean.codigo.Equals("0"))
            {
                Session["lgn_id"] = bean.id;
                Session["lgn_codigo"] = bean.codigo;
                Session["lgn_login"] = bean.nombre;
                Session["lgn_nombre"] = bean.nombre;
                Session["lgn_perfil"] = bean.perfil;
                Session["lgn_codsupervisor"] = bean.codSupervisor;
                Session["lgn_perfilmenu"] = bean.hashRol;
                Session["lgn_configuracion"] = bean.hashConfiguracion;

                GeneralController.subInicializarConfiguracion();
                //subInicializarMenu();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void subInicializarMenu(string urlhome)
        {
            Int32 liIdPerfil = 0;
            liIdPerfil = Convert.ToInt32(Session["lgn_perfil"].ToString());

            StringBuilder loMenuTop = new StringBuilder();
            StringBuilder loMenuLateral = new StringBuilder();
            string urlDashboard = "../Dashboard/Default.aspx";
            List<MenuBean> loLstMenuBean = MenuController.subObtenerDatosMenu(liIdPerfil);
            if (loLstMenuBean.Count > 0)
            {
                List<MenuBean> loLstMenuBeanPadre = loLstMenuBean.Where(obj => obj.IdMenuPadre.Equals(String.Empty)).ToList();

                for (int i = 0; i < loLstMenuBeanPadre.Count; i++)
                {
                    List<MenuBean> loLstMenuBeanHijo = loLstMenuBean.Where(obj => obj.IdMenuPadre.Equals(loLstMenuBeanPadre[i].IdMenu)).ToList();

                    if (loLstMenuBeanHijo.Count > 0)
                    {
                        loMenuTop.Append("<div id=\"cz-principal-menu-option-" + loLstMenuBeanPadre[i].UrlImagen + "\" class=\"cz-principal-menu-option\">");
                        loMenuTop.Append("<div class=\"cz-principal-menu-option-button\">");
                        loMenuTop.Append("<div class=\"cz-principal-menu-option-title\">");
                        loMenuTop.Append("<div class=\"cz-principal-menu-option-image\"></div>");
                        loMenuTop.Append("<a>" + loLstMenuBeanPadre[i].Descripcion + "</a>");
                        loMenuTop.Append("</div>");
                        loMenuTop.Append("</div>");
                        loMenuTop.Append("<div class=\"cz-principal-menu-suboptions\">");

                        //
                        loMenuLateral.Append("<div class=\"cz-menu-lateral-option\">");
                        loMenuLateral.Append("<div id=\"cz-menu-lateral-option-" + loLstMenuBeanPadre[i].UrlImagen + "\" class=\"cz-menu-lateral-title\">");
                        loMenuLateral.Append("<div class=\"cz-menu-lateral-title-icon\" style=\"width:35px\"></div>");
                        loMenuLateral.Append("<div class=\"cz-menu-lateral-title-text\" style=\"width:115px\">" + loLstMenuBeanPadre[i].Descripcion + "</div>");
                        loMenuLateral.Append("<div class=\"cz-menu-lateral-title-status\">+</div>");
                        loMenuLateral.Append("</div>");
                        loMenuLateral.Append("<div class=\"cz-menu-lateral-suboptions\">");

                        for (int j = 0; j < loLstMenuBeanHijo.Count; j++)
                        {
                            String lsDescripcionMenuLateral = loLstMenuBeanHijo[j].Descripcion;
                            if (lsDescripcionMenuLateral.Length >= 18)
                            {
                                lsDescripcionMenuLateral = lsDescripcionMenuLateral.Substring(0, 18) + " ...";
                            }

                            if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_MANT_ALMACEN ||
                                loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_MANT_PRODUCTO_ALMACEN
                                )
                            {
                                if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                {
                                    loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                }
                            }
                            else
                            {
                                if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_DASHBOARD)
                                {
                                    loMenuTop.Append("<div class=\"cz-principal-menu-suboption-nojs\"> <a href=\"" + urlDashboard + "\" target=\"_blank\">" + loLstMenuBeanHijo[j].Descripcion + "</a> </div>");
                                    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption-njs\"><a target=\"_blank\" href='" + urlDashboard + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                }
                                else
                                {
                                    if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_REP_TRACKING)
                                    {
                                        if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_REPORTE_TRACKING).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                        {
                                            loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\" target=\"_blank\">" + loLstMenuBeanHijo[j].Descripcion + "</a> </div>");
                                            loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                        }
                                    }
                                    else
                                    {
                                        if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_REP_DINAMICO)
                                        {
                                            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CABECERA_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()) ||
                                                fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_FIN_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                            {
                                                //lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                            }
                                        }
                                        else
                                        {
                                            if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MANTENIMIENTO_FORMULARIO)
                                            {
                                                if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CABECERA_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())
                                                       || fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_FIN_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                {
                                                    loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                                }
                                            }
                                            else
                                            {
                                                if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresFuncion.COD_FUNCION_BONIFICACION)
                                                {
                                                    string valor = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor;
                                                    if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                    {
                                                        //lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                        loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                        loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                                    }
                                                }
                                                else
                                                {
                                                    if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_DEVOLUCION)
                                                    {
                                                        if ((fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresModulo.COD_MODULO_DEVOLUCION).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())))
                                                        {
                                                            loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                            loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_CANJE)
                                                        {
                                                            if ((fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresModulo.COD_MODULO_CANJE).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())))
                                                            {
                                                                loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                                loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                            loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                                            loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresFuncion.COD_FUNCION_BONIFICACION)
                                //{
                                //    string valor = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor;
                                //    if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                //    {
                                //        loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                //        loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                //    }
                                //}
                                //else
                                //{
                                //    loMenuTop.Append("<div class=\"cz-principal-menu-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + loLstMenuBeanHijo[j].Descripcion + "</a></div>");
                                //    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"><a target=\"centerFrame\" href='" + loLstMenuBeanHijo[j].Url + "'>" + lsDescripcionMenuLateral + "</a></div>");
                                //} 

                            }
                        }
                        loMenuTop.Append("</div></div>");
                        loMenuLateral.Append("</div></div>");
                    }
                }
                loMenuTop.Append("</div></div>");
            }

            List<ManualBean> loLstManuales = MenuController.subObtenerDatosManuales(liIdPerfil);
            string lsRutaManual = ConfigurationManager.AppSettings["urlDocs"] + "/";
            if (loLstManuales != null && loLstManuales.Count > 0)
            {
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box\">");
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box-title\">Ayuda</div>");
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box-options\">");

                loMenuLateral.Append("<div class=\"cz-menu-lateral-option\">");
                loMenuLateral.Append("<div id=\"cz-menu-lateral-option-WEB_INFO" + "\" class=\"cz-menu-lateral-title\">");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-icon\" style=\"width:35px\"></div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-text\" style=\"width:115px\">Ayuda</div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-status\">+</div>");
                loMenuLateral.Append("</div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-suboptions\">");
                foreach (ManualBean loManual in loLstManuales)
                {
                    //loMenuLateral.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + lsRutaManual + loManual.Url + "\" >" + loManual.Descripcion + "</a> </div>");
                    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption\"> <a target='blank' href=\"" + lsRutaManual + loManual.Url + "\" >" + loManual.Descripcion + "</a> </div>");
                }
            }
            loMenuLateral.Append("</div></div>");
            List<Tipo> tipos = Util.obtenerTipos();
            //lsMenuInicio.Append("<div class=\"cz-other-box-main\">");
            foreach (Tipo tipo in tipos)
            {
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box\">");
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box-title\">").Append(tipo.Nombre).Append("</div>");
                //loMenuLateral.Append("<div class=\"cz-other-box-center-box-options\">");

                loMenuLateral.Append("<div class=\"cz-menu-lateral-option\">");
                loMenuLateral.Append("<div id=\"cz-menu-lateral-option-WEB_INFO" + "\" class=\"cz-menu-lateral-title\">");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-icon\" style=\"width:35px\"></div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-text\" style=\"width:115px\">" + tipo.Nombre + "</div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-title-status\">+</div>");
                loMenuLateral.Append("</div>");
                loMenuLateral.Append("<div class=\"cz-menu-lateral-suboptions\">");

                foreach (Recurso recurso in tipo.Recursos)
                {
                    //lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option-nojs\"> <a target='blank' href=\"" + recurso.URL + "\" >" + recurso.Descripcion + "</a> </div>");
                    loMenuLateral.Append("<div class=\"cz-menu-lateral-suboption-nojs\"> <a target='blank' href=\"" + recurso.URL + "\" >" + recurso.Descripcion + "</a> </div>");
                }
                loMenuLateral.Append("</div></div>");
            }
            //MenuTop.Text = loMenuTop.ToString();
            MenuLateral.Text = loMenuLateral.ToString();

            if (Session["lgn_nombre"] != null)
            { this.lbNomUsuario.Text = Session["lgn_nombre"].ToString(); }
            else
            { Response.Redirect("Default.aspx?acc=SES"); }

            StreamReader stream = new StreamReader(Request.InputStream);
            string entrada = stream.ReadToEnd();
            if (entrada != null && !entrada.Equals(""))
            {
                String lsPagina = Uri.UnescapeDataString(entrada.Split('=')[1]);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "initialize", "$('#centerFrame').attr('src', '" + lsPagina + "');", true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "initialize", "$('#centerFrame').attr('src', '" + urlhome + "');", true);
            }
        }

        protected void opcInicio_Click(object sender, EventArgs e)
        {
            Response.Redirect("Main.aspx");
        }

        protected void opcSalir_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Default.aspx?acc=EXT");
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        private List<ConfiguracionBean> subObtenerListaConfiguracionPorCodigo(List<ConfiguracionBean> poLstConfiguracionBean, Enumerados.CodigoConfiguracion peCodigoConfiguracion)
        {
            List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
            loLstConfiguracionBean = poLstConfiguracionBean.FindAll(obj => obj.Tipo.Equals(peCodigoConfiguracion.ToString()));
            return loLstConfiguracionBean;
        }

        public bool nServicesEsEmbebido()
        {
            ConfiguracionBean loConfiguracionBean = new ConfiguracionBean();
            loConfiguracionBean = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_ES_EMBEBIDO);
            return loConfiguracionBean.Valor.Equals(Enumerados.FlagHabilitado.T.ToString());
        }

        public bool nServicesExisteConfiguracion()
        {
            ConfiguracionBean loConfiguracionBean = new ConfiguracionBean();
            loConfiguracionBean = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_SEGUIMIENTO_RUTA);
            return loConfiguracionBean.Valor.Equals(Enumerados.FlagHabilitado.T.ToString());
        }
    }
}