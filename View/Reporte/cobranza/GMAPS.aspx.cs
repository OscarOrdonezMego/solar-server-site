﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;

namespace Pedidos_Site.Reporte.cobranza
{
    public partial class Reporte_asistencia_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "" + dataJSON["codigo"].ToString() + "";
            String[] dDato = codigo.Split('|');
            lblNomUsuario.Text = dDato[0].ToString();
            lblCliente.Text = dDato[1].ToString();
            lblMonto.Text = ManagerConfiguration.moneda + " " + dDato[2].ToString();
            lblMontoDolares.Text = dDato[10].ToString();
            lblFechaRegistro.Text = dDato[3].ToString();
            lblPagado.Text = ManagerConfiguration.moneda + " " + dDato[4].ToString();
            lblPagadoDolares.Text = dDato[11].ToString();
            lblVoucher.Text = dDato[5].ToString();
            lblBanco.Text = dDato[6].ToString();
            lblTipo.Text = dDato[7].ToString();
        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {
                String[] xdatos = codigo.Split('|');

                String xusu = xdatos[0].ToString();
                String xcli = xdatos[1].ToString();
                String xTot = xdatos[2].ToString();
                String xTotDol = xdatos[10].ToString();
                String xFec = xdatos[3].ToString();
                String xPag = xdatos[4].ToString();
                String xPagDol = xdatos[11].ToString();
                String xVou = xdatos[5].ToString();
                String xlat = xdatos[8].ToString();
                String xlon = xdatos[9].ToString();

                List<MapBean> lst = new List<MapBean>();


                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;
                be.msg = "<h4>" + "COBRANZA:" + "</h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + xusu + "</br>" +
                    IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE).ToString() + ": " + xcli + "</br>";
                if (Convert.ToDouble(xTot) > 0) be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_TOTAL).ToString() + " S/ : " + xTot + "</br>";
                if (Convert.ToDouble(xTotDol) > 0) be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_TOTAL).ToString() + " $ : " + xTotDol + "</br>";
                be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHA_REGISTRO).ToString() + ": " + xFec + "</br>";
                if (Convert.ToDouble(xTot) > 0) be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_PAGADO).ToString() + " S/ : " + xPag + "</br>";
                if (Convert.ToDouble(xTotDol) > 0) be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_PAGADO).ToString() + " $ : " + xPagDol + "</br>";
                be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_VOUCHER) + ": " + xVou;

                be.img = "../../images/gps/pedido.png";

                be.titulo = "COBRANZA";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}