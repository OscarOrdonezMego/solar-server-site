﻿using System;
using Controller;
using System.Data;

namespace Pedidos_Site.Reporte.cobranza
{
    public partial class cobranzadet : PageController
    {
        protected override void initialize()
        {
            String xCobro = "";
            xCobro = Request.QueryString["codcobro"].ToString();

            DataTable odt = ReporteController.CobranzaDetalleList(xCobro);

            grdMant.DataSource = odt;
            grdMant.DataBind();
        }

        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }
    }
}