﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Reporte.cobranza.cobranzadet" Codebehind="cobranzadet.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>

    <script type="text/javascript" src="<%
        string url = "&";
        string client = (ConfigurationManager.AppSettings["MAPS_API_CLIENT"]);
        if (!client.Equals(""))
        {
            client = "client=" + client;
            url += client;
        }
        string key = (ConfigurationManager.AppSettings["MAPS_API_KEY"]);
        if (!key.Equals(""))
        {
            key = "key=" + key;
            url += key;
        }
        %><%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es<%=url%>"></script>


    <script>
        $(document).ready(function () {
            detReg(".detGPS", "GMAPS.aspx");
        });
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon_report.png"
                            alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p>
                                <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_REPORTEDECOBRANZA)%>
                            </p>
                        </div>
                    </div>
                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <a href="javascript: history.go(-1)">
                            <img src="../../imagery/all/icons/arrow-left.png" border="0" style="margin-top: 10px;"
                                alt="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VOLVER)%>"></a>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div id="divGridView" runat="server">
                                    <asp:Repeater ID="grdMant" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%" class="grilla table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th nomb="VENDEDOR">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VOUCHER)%>
                                                    </th>
                                                        <th nomb="FECHAINICIO">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BANCO)%>
                                                    </th>
                                                        <th nomb="FECHAFIN">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                                                    </th>
                                                        <th nomb="CLIENTE">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHA)%>
                                                    </th>
                                                    <th nomb="MONTOSOLES">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%> S/.
                                                    </th>
                                                        <th nomb="MONTODOLARES">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%> $
                                                    </th>
                                                        <th nomb="WEB_TIPO">
                                                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHA_DIFERIDA)%>
                                                        </th>
                                                        <th style="width: 40px;"></th>
                                                        <%--<th nomb="CONDICION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%>
                            </th>
                            <th nomb="MONTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                            </th>--%>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# Eval("pag_voucher")%>
                                            </td>
                                                <td>
                                                    <%# Eval("ban_nomlargo")%>
                                            </td>
                                                <td>
                                                    <%# Eval("PAG_TIPOPAGO")%>
                                            </td>
                                                <td>
                                                    <%# Eval("pag_fecregistro")%>
                                            </td>
                                            <td>
                                                    <%# Eval("pag_montopagosoles")%>
                                            </td>
                                            <td>
                                                    <%# Eval("pag_montopagodolares")%>
                                            </td>
                                                <td style="text-align:center">
                                                    <%# Eval("FEC_DIFERIDA")%>
                                                </td>
                                                <td>
                                                    <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>"
                                                        class="detGPS form-icon" cod="<%# Eval("usr_nombre") %>|<%# Eval("cli_nombre") %>|<%# Eval("MontoTotalSoles") %>|<%# Eval("pag_fecregistro") %>|<%# Eval("pag_montopagosoles") %>|<%# Eval("pag_voucher") %>|<%# Eval("ban_nomlargo") %>|<%# Eval("pag_tipopago") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>|<%# Eval("MontoTotalDolares") %>|<%# Eval("PAG_MONTOPAGODOLARES") %>">
                                                        <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="alt">
                                                <td>
                                                    <%# Eval("pag_voucher")%>
                                            </td>
                                                <td>
                                                    <%# Eval("ban_nomlargo")%>
                                            </td>
                                                <td>
                                                    <%# Eval("pag_tipopago")%>
                                            </td>
                                                <td>
                                                    <%# Eval("pag_fecregistro")%>
                                            </td>
                                            <td>
                                                    <%# Eval("pag_montopagosoles")%>
                                            </td>
                                            <td>
                                                    <%# Eval("pag_montopagodolares")%>
                                            </td>
                                                <td  style="text-align:center">
                                                    <%# Eval("FEC_DIFERIDA")%>
                                                </td>
                                                <td>
                                                    <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>"
                                                        class="detGPS form-icon" cod="<%# Eval("usr_nombre") %>|<%# Eval("cli_nombre") %>|<%# Eval("MontoTotalSoles") %>|<%# Eval("pag_fecregistro") %>|<%# Eval("pag_montopagosoles") %>|<%# Eval("pag_voucher") %>|<%# Eval("ban_nomlargo") %>|<%# Eval("pag_tipopago") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>|<%# Eval("MontoTotalDolares") %>|<%# Eval("PAG_MONTOPAGODOLARES") %>">
                                                        <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </tbody> </table>
                                   
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
</body>
</html>
