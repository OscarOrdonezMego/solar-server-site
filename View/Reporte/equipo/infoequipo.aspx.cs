﻿using System;
using System.Web.UI;
using System.Text;
using Model.bean;

namespace Pedidos_Site.Reporte.equipo
{
    public partial class infoequipo : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String xINFO = Request.QueryString["info"].ToString();

            String xNextel = xINFO.ToString().Split('|')[0].ToString();
            String xIp = xINFO.ToString().Split('|')[1].ToString();
            String xModelo = xINFO.ToString().Split('|')[2].ToString();
            String xSenal = xINFO.ToString().Split('|')[3].ToString();
            String xBateria = xINFO.ToString().Split('|')[4].ToString();
            String xLat = xINFO.ToString().Split('|')[5].ToString();
            String xLng = xINFO.ToString().Split('|')[6].ToString();


            StringBuilder t1 = new StringBuilder();

            t1.Append("<table class='grilla' style='margin: 10px;'>\n");
            t1.Append("<tbody>\n");

            t1.Append("<tr >\n");
            //NEXTEL
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NEXTEL).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append(xNextel + "\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //IP
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append("IP:" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append(xIp + "\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //MODELO
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_MODELO).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append(xModelo + "\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //SEÑAL
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_SENAL).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<img src='../../images/sinal_" + xSenal.ToString().Trim() + ".gif' border='0'/>\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //BATERIA
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_BATERIA).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<img src='../../images/ico_bat_" + xBateria.ToString().Trim() + ".jpg' border='0'/>\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //LATITUD
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append(xLat + "\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            //LONGITUD
            t1.Append("<tr >\n");
            t1.Append("<td>\n");
            t1.Append(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD).ToString() + ":" + "\n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append(xLng + "\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");


            t1.Append("</tbody>\n");
            t1.Append("</table>\n");


            litControls.Text = t1.ToString();

        }
    }
}