﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.equipo.equipo" Codebehind="equipo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3&sensor=false&language=es"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>

     <style>
        .divclearable
        {
            float: left;
        }
        
        #contentTooltip
        {
            position: absolute;
            left: 0;
            top: 0;
            display: none;
            z-index: 100;
        }
    </style>
    <script>
        
        
        var urlbus = 'grid.aspx';
        
        $(document).ready(function () {

            busReg();
            $('#buscar').trigger("click");
//            exportarReg(".xlsReg", "../exportar.aspx", '.grilla'); //funcion encarga de la exportacion
            detReg(".detGPS", "GMAPS.aspx");


            $('.btnTooltip').live("click", function(){
              var xInfo = $(this).attr('cod');
              var pos = $(this).offset();
              var width = $(this).width();
              
              $.post("infoequipo.aspx?info="+xInfo, function( data ) {
                     $('#contentTooltip').find('.portlet-content').html(data);
                     $('#contentTooltip').height($('#contentTooltip').find('.portlet-content').height()+40);
                     var pAlto=($('#contentTooltip').find('.portlet-content').height()+40)/2
                     $('#contentTooltip').find('.portlet-content').width(200);
                      $('#contentTooltip').css({
                            left: (pos.left + 20) + 'px',
                            top: pos.top - pAlto + 'px',
                            
                     });
                     $('#contentTooltip').show(200);
              });
   
        });
          $('.closeTooltip').click(function(){
            $('#contentTooltip').hide(100);
          });
        });

        

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
//            strData.fini = $('#txtFechaIni').val();
//            strData.ffin = $('#txtFechaFin').val(); // + ' ' + $('#txtHoraFin').val();
//            strData.vendedor = $('#cboVendedor').val();
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();

            return strData;
        }

//        function getParametrosXLS() {
//            var fini = $('#txtFechaIni').val();//  + ' ' + $('#txtHoraIni').val();
//            var ffin = $('#txtFechaFin').val(); //  + ' ' + $('#txtHoraFin').val();
//            var fven = $('#cboVendedor').val();
//            var mod = "REP_PEDIDO";
//            return 'mod=' + mod + '&fini=' + fini + '&ffin=' + ffin + '&fven=' + fven ;
//        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon_report.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_REPORTE_EQUIPO)%></p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
                <div style="display: none;" class="cz-form-content cz-util-right cz-util-right-text">
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                </div>
            </div>
            <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div class="form-gridview-data" id="divGridViewData" runat="server">
                            </div>
                            <div class="form-gridview-error" id="divGridViewError" runat="server">
                            </div>
                            <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                <p>
                                    buscando resultados</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Hidden Fields to control pagination-->
            <div id="paginator-hidden-fields">
                <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_RESTAURARVARIOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ELIMINARVARIOS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ELIMINAR)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_RESTAURAR)%>" />
            </div>
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
            </div>
        </div>
    </div>
    </form>
    <%--<div id="calendarwindow" class="calendar-window">
    </div>--%>
     <div id="contentTooltip" >

     <div class="cz-form-content cz-content-extended">
                        <div class="cz-form-subcontent">
                            <div class="cz-form-subcontent-title">
                              <span style="float: right;"><a class="closeTooltip"></a></span>
                                <p>
                                  <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INFORMACION)%></p>
                            </div>
                            <div class="cz-form-subcontent-content" style="background: white;">
                               <div class="portlet-content" 
                                  </div>
                                
                            </div>
                        </div>
                    </div>
        
       
    </div>
</body>
</html>
