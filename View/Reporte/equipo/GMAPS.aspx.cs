﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;
using System.Web.UI;

namespace Pedidos_Site.Reporte.equipo
{
    public partial class Reporte_asistencia_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');

            lblNomUsuario.Text = dDato[0].ToString();

        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {
                String[] xdatos = codigo.Split('|');

                String xusu = xdatos[0].ToString();
                String xtel = xdatos[1].ToString();
                String xfec = xdatos[2].ToString();
                String xlat = xdatos[3].ToString();
                String xlon = xdatos[4].ToString();

                List<MapBean> lst = new List<MapBean>();

                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;
                be.msg = "<h4>" + "Equipo" + "</h4> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO).ToString() + ": " + xusu + "</br> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_TELEFONO) + ": " + xtel + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHA) + ": " + xfec;

                be.img = "../../images/gps/phone.png";

                be.titulo = "Equipo";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}