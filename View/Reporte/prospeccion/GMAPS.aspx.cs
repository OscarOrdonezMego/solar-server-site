﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;

namespace Pedidos_Site.Reporte.prospeccion
{
    public partial class Reporte_prospecto_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');

            lblNombre.Text = dDato[1].ToString();
            lblFecha.Text = dDato[2].ToString();
            lblDireccion.Text = dDato[3].ToString();
            lblNomUsuario.Text = dDato[4].ToString();

        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {

                String[] xdatos = codigo.Split('|');

                String id = xdatos[0].ToString();
                String nombre = xdatos[1].ToString();
                String fecha = xdatos[2].ToString();
                String usuario = xdatos[3].ToString();
                String direccion = xdatos[4].ToString();
                String xlat = xdatos[5].ToString();
                String xlon = xdatos[6].ToString();


                List<MapBean> lst = new List<MapBean>();

                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;
                be.msg = "<h4>" + "Prospecto" + "</h4> Num. Prospecto: " + id + "</br> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE) + ": " + nombre + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHA) + ": " + fecha + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + usuario + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIRECCION) + ": " + direccion;

                be.img = "../../images/gps/cliente.png";

                be.titulo = "Prospecto";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}