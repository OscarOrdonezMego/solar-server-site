﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;
using Controller.functions;

namespace Pedidos_Site.Reporte.prospeccion
{
    public partial class Mantenimiento_prospeccion_grid : PageController
    {
        public static String lsCodMenu = "RPR";
        private String valorCampoAdicional1 = "F";
        private String valorCampoAdicional2 = "F";
        private String valorCampoAdicional3 = "F";
        private String valorCampoAdicional4 = "F";
        private String valorCampoAdicional5 = "F";
        public String DescripcionCampoAdicional1 = "";
        public String DescripcionCampoAdicional2 = "";
        public String DescripcionCampoAdicional3 = "";
        public String DescripcionCampoAdicional4 = "";
        public String DescripcionCampoAdicional5 = "";
        public String visibleCampoAdicional1 = "";
        public String visibleCampoAdicional2 = "";
        public String visibleCampoAdicional3 = "";
        public String visibleCampoAdicional4 = "";
        public String visibleCampoAdicional5 = "";
        public String mostrarCredito = "";

        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
            String vendedor = dataJSON["vendedor"].ToString();
            String prospecto = dataJSON["prospecto"].ToString();

            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_1, out this.valorCampoAdicional1, out this.DescripcionCampoAdicional1);
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_2, out this.valorCampoAdicional2, out this.DescripcionCampoAdicional2);
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_3, out this.valorCampoAdicional3, out this.DescripcionCampoAdicional3);
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_4, out this.valorCampoAdicional4, out this.DescripcionCampoAdicional4);
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_5, out this.valorCampoAdicional5, out this.DescripcionCampoAdicional5);

            this.visibleCampoAdicional1 = (this.valorCampoAdicional1 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.visibleCampoAdicional2 = (this.valorCampoAdicional2 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.visibleCampoAdicional3 = (this.valorCampoAdicional3 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.visibleCampoAdicional4 = (this.valorCampoAdicional4 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.visibleCampoAdicional5 = (this.valorCampoAdicional5 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

            PaginateClienteProspeccionBean paginate = ClienteProspeccionController.paginarBuscar(vendedor, prospecto, fini, ffin, pagina, filas);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ClienteProspeccionBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo, out String descripcionCampo)
        {
            valorCampo = "F";
            descripcionCampo = "";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                    descripcionCampo = configBean.Descripcion;
                }
            }
        }

        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }

        protected String isEnable(String flag)
        {
            if (flag != "F")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }
    }
}