﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.prospeccion.Mantenimiento_prospeccion_Clientes" Codebehind="Clientes.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>

    <script type="text/javascript" src="<%
        string url = "&";
        string client = (ConfigurationManager.AppSettings["MAPS_API_CLIENT"]);
        if (!client.Equals("")){
            client = "client=" + client;
            url += client;
        }
        string key = (ConfigurationManager.AppSettings["MAPS_API_KEY"]);
        if (!key.Equals("")){
            key = "key=" + key;
            url += key;
        }
        %><%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es<%=url%>">

    </script>

    <script type="text/javascript">
        var urldel = 'Clientes.aspx/borrar';
        var urlbus = 'grid.aspx';
        var urlsavE = 'Clientes.aspx/editar';
        var urlins = 'nuevo.aspx'

        $(document).ready(function () {
            busReg();
            modReg();
            $('#buscar').trigger("click");
            exportarReg(".xlsReg", "../exportar.aspx", '.grilla');
            detReg(".detGPS", "GMAPS.aspx");
        });

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.fini = $('#txtFechaIni').val();
            strData.ffin = $('#txtFechaFin').val();
            strData.prospecto = $('#txtProspecto').val();
            strData.vendedor = $('#cboVendedor').val();
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            return strData;
        }

        function getParametrosXLS() {
            var fini = $('#txtFechaIni').val();
            var ffin = $('#txtFechaFin').val();
            var fven = $('#cboVendedor').val();
            var fpro = $('#txtProspecto').val();
            var mod = "REP_PROSPECTO";
            return 'mod=' + mod + '&fini=' + fini + '&ffin=' + ffin + '&fven=' + fven + '&fpro=' + fpro;
        }
    </script>
    <style type="text/css">
        .setStyleCbo {
            font-family: sans-serif !important;
            font-size: 12px !important;
            color: #666 !important;
            margin: 0;
            padding: 0;
            width: 202px;
            outline: none !important;
            height: 26px;
            position: relative;
            margin-left: 1px;
            z-index: 1;
            cursor: pointer;
        }

            .setStyleCbo > option {
                background-color: #E6E6E6 !important;
            }
    </style>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon-users.png"
                            alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p style="font-family: sans-serif; font-weight: bold !important;">Reporte de Prospectos</p>
                        </div>
                    </div>
                    <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                        data-grid-id="divGridViewData" value="Ver Tabla" />

                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EXPORTAR))
                      {%>
                    <input type="button" id="cz-form-box-exportar" class="cz-form-content-input-button cz-form-content-input-button-image form-button xlsReg cz-util-right"
                        value="Exportar" />
                    <%} %>
                </div>
                <div class="cz-form-box-content">
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAINICIO)%>
                        </p>
                        <input name="txtFechaIni" type="text" value="30/05/2014" maxlength="10" id="txtFechaIni"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAFIN)%>
                        </p>
                        <input name="txtFechaFin" type="text" value="30/05/2014" maxlength="10" id="txtFechaFin"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img2" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                        </p>
                        <asp:DropDownList ID="cboVendedor" runat="server" CssClass="cz-form-content-input-select">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                        </p>
                        <asp:TextBox ID="txtProspecto" runat="server" class="cz-form-content-input-text"></asp:TextBox>
                        <div class="cz-form-content-input-text-visible">
                            <div class="cz-form-content-input-text-visible-button">
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>" />
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server"></div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server"></div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" alt="" />
                                    <p>Buscando resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hidden Fields to control pagination-->
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                    <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                    <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                    <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                    <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                    <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                    <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>
