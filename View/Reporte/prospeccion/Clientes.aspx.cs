﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using Model;
using Controller.functions;
using System.Web.Services;

namespace Pedidos_Site.Reporte.prospeccion
{
    public partial class Mantenimiento_prospeccion_Clientes : PageController
    {
        public static String lsCodMenu = "RPR";
        private static String valorCampoAdicional1 = "F";
        private static String valorCampoAdicional2 = "F";
        private static String valorCampoAdicional3 = "F";
        private static String valorCampoAdicional4 = "F";
        private static String valorCampoAdicional5 = "F";
        public static String idusuario = "";
        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            if (!IsPostBack)
            {
                idusuario = Session["lgn_id"].ToString();
                llenarComboBoxVendedor(idusuario);
                txtFechaIni.Value = Utils.getFechaActual();
                txtFechaFin.Value = Utils.getFechaActual();
            }
        }

        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();

                }
            }
            else
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
            }
        }

        [WebMethod]
        public static String editar(String id, String codigo, String nombre, String direccion, String canal, String giro, String xSec, String latitud, String longitud
        , String campoAdicional1, String campoAdicional2, String campoAdicional3, String campoAdicional4, String campoAdicional5, String limiteCredito, String creditoUtilizado
        )
        {
            try
            {

                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }

                else if (Utils.tieneCaracteresReservados(direccion))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIRECCION, direccion) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, direccion));
                }

                else if (latitud.Length > 0 && longitud.Length == 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD, longitud));
                }
                else if (latitud.Length == 0 && longitud.Length > 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD, latitud));
                }

                else
                {

                    ClienteProspeccionBean bean = new ClienteProspeccionBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.direccion = direccion;
                    bean.flag = "T";
                    bean.latitud = latitud;
                    bean.secuencia = xSec;
                    bean.giro = giro;
                    bean.tipocliente = canal;
                    bean.longitud = longitud;
                    bean.CampoAdicional1 = campoAdicional1;
                    bean.CampoAdicional2 = campoAdicional2;
                    bean.CampoAdicional3 = campoAdicional3;
                    bean.CampoAdicional4 = campoAdicional4;
                    bean.CampoAdicional5 = campoAdicional5;

                    ClienteProspeccionController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}