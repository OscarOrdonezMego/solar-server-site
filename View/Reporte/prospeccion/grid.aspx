﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.prospeccion.Mantenimiento_prospeccion_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divGridView" runat="server">
            <asp:Repeater ID="grdMant" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%;text-align:center;" class="grilla table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 100px;" nomb="CODIGO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                                </th>
                                <th nomb="NOMBRE">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                                </th>
                                <th nomb="DIRECCION">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%>
                                </th>
                                <th nomb="VENDEDOR">Vendedor
                                </th>
                                <th nomb="FECHAREGISTRO">F. Registro
                                </th>
                                <th nomb="FECHAMOVIL">F. Móvil
                                </th>
                                <th nomb="TIPOCLIENTE">Tipo cliente
                                </th>
                                <th nomb="GIRO">Giro
                                </th>
                                <th nomb="OBSERVACION">Observación
                                </th>
                                <th nomb="CAMPO_1" <%= visibleCampoAdicional1 %>>
                                    <%=DescripcionCampoAdicional1%>
                                </th>
                                <th nomb="CAMPO_2" <%= visibleCampoAdicional2 %>>
                                    <%=DescripcionCampoAdicional2%>
                                </th>
                                <th nomb="CAMPO_3" <%= visibleCampoAdicional3 %>>
                                    <%=DescripcionCampoAdicional3%>
                                </th>
                                <th nomb="CAMPO_4" <%= visibleCampoAdicional4 %>>
                                    <%=DescripcionCampoAdicional4%>
                                </th>
                                <th nomb="CAMPO_5" <%= visibleCampoAdicional5 %>>
                                    <%=DescripcionCampoAdicional5%>
                                </th>
                                <th style="width: 40px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                                </th>
                                <th style="width: 40px;">Crear
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <%--<td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>--%>
                        <td>
                            <%# Eval("CODIGO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("DIRECCION")%>
                        </td>
                        <td>
                            <%# Eval("VENDEDOR")%>
                        </td>
                        <td>
                            <%# Eval("FECHA")%>
                        </td>
                        <td>
                            <%# Eval("FECHAMOVIL")%>
                        </td>
                        <td>
                            <%# Eval("TIPOCLIENTE")%>
                        </td>
                        <td>
                            <%# Eval("GIRO")%>
                        </td>
                        <td>
                            <%# Eval("OBSERVACION")%>
                        </td>
                        <td <%= visibleCampoAdicional1 %>>
                            <%# Eval("CampoAdicional1")%>
                        </td>
                        <td <%= visibleCampoAdicional2 %>>
                            <%# Eval("CampoAdicional2")%>
                        </td>
                        <td <%= visibleCampoAdicional3 %>>
                            <%# Eval("CampoAdicional3")%>
                        </td>
                        <td <%= visibleCampoAdicional4 %>>
                            <%# Eval("CampoAdicional4")%>
                        </td>
                        <td <%= visibleCampoAdicional5 %>>
                            <%# Eval("CampoAdicional5")%>
                        </td>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                                cod="<%# Eval("ID") %>|<%# Eval("NOMBRE") %>|<%# Eval("FECHA") %>|<%# Eval("VENDEDOR") %>|<%# Eval("DIRECCION") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                                <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>
                        </td>
                        <td >
                            <a class="editItemReg" style="cursor: pointer;display: <%# isEnable(Eval("flag").ToString())%>"  data-toggle="modal" cod="<%# Eval("ID") %>" >
                                <img src="../../imagery/all/icons/add.png" border="0" title="Agregar Cliente" /></a>
                        </td>


                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt">
                        <%--<td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>--%>
                        <td>
                            <%# Eval("CODIGO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("DIRECCION")%>
                        </td>
                        <td>
                            <%# Eval("VENDEDOR")%>
                        </td>
                        <td>
                            <%# Eval("FECHA")%>
                        </td>
                        <td>
                            <%# Eval("FECHAMOVIL")%>
                        </td>
                        <td>
                            <%# Eval("TIPOCLIENTE")%>
                        </td>
                        <td>
                            <%# Eval("GIRO")%>
                        </td>
                        <td>
                            <%# Eval("OBSERVACION")%>
                        </td>
                        <td <%= visibleCampoAdicional1 %>>
                            <%# Eval("CampoAdicional1")%>
                        </td>
                        <td <%= visibleCampoAdicional2 %>>
                            <%# Eval("CampoAdicional2")%>
                        </td>
                        <td <%= visibleCampoAdicional3 %>>
                            <%# Eval("CampoAdicional3")%>
                        </td>
                        <td <%= visibleCampoAdicional4 %>>
                            <%# Eval("CampoAdicional4")%>
                        </td>
                        <td <%= visibleCampoAdicional5 %>>
                            <%# Eval("CampoAdicional5")%>
                        </td>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                                cod="<%# Eval("ID") %>|<%# Eval("NOMBRE") %>|<%# Eval("FECHA") %>|<%# Eval("VENDEDOR") %>|<%# Eval("DIRECCION") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                                <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>
                        </td>

                        <td>
                            <a class="editItemReg" style="cursor: pointer; display: <%# isEnable(Eval("flag").ToString())%>" data-toggle="modal" cod="<%# Eval("ID") %>" >
                                <img src="../../imagery/all/icons/add.png" border="0" title="Agregar Cliente" /></a>
                        </td>

                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Pagina</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            Buscando resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">
                    ×
                </div>
            </div>
        </div>
    </form>
</body>
</html>
