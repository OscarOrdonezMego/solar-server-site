﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Reporte.prospeccion
{
    public partial class Reporte_prospeccion_nuevo : PageController
    {
        public bool GenCodigo;
        public float latitud;
        public float longitud;
        String secuencia = "1";
        private String valorCampoAdicional1 = "F";
        private String valorCampoAdicional2 = "F";
        private String valorCampoAdicional3 = "F";
        private String valorCampoAdicional4 = "F";
        private String valorCampoAdicional5 = "F";
        public String DescripcionCampoAdicional1 = "";
        public String DescripcionCampoAdicional2 = "";
        public String DescripcionCampoAdicional3 = "";
        public String DescripcionCampoAdicional4 = "";
        public String DescripcionCampoAdicional5 = "";
        public String visibleCampoAdicional1 = "";
        public String visibleCampoAdicional2 = "";
        public String visibleCampoAdicional3 = "";
        public String visibleCampoAdicional4 = "";
        public String visibleCampoAdicional5 = "";
        public String mostrarCredito = "";

        protected override void initialize()
        {
            hidSecuencia.Value = secuencia;
            String ValorLimiteCredito = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_LIMITE_CREDITO).Valor;

            mostrarCredito = (ValorLimiteCredito == "F" ? "style='display:none'" : "");

            if (!IsPostBack)
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOCLIENTE);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                if (ManagerConfiguration.laltitudlongitud != "0")
                {
                    String[] latlo = ManagerConfiguration.laltitudlongitud.Split(',');

                    latitud = float.Parse(latlo[0]);
                    longitud = float.Parse(latlo[1]);
                }
                else
                {
                    latitud = float.Parse("-12.0462037802177");
                    longitud = float.Parse("-77.0656585693359");
                }

                IniciarTipoCliente();


                if (dataJSON != null)
                {
                    ClienteProspeccionBean usuario = ClienteProspeccionController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"];

                    MtxtNombre.Value = usuario.nombre;
                    MtxtCodigo.Value = usuario.codigo;
                    txtDireccion.Value = usuario.direccion;
                    txtLatitud.Value = usuario.latitud;
                    txtLongitud.Value = usuario.longitud;
                    txtGiro.Value = usuario.giro;
                    cboTipoCliente.SelectedValue = usuario.tipocliente;

                    if (txtLatitud.Value.Trim().ToString() != "")
                    {
                        latitud = float.Parse(txtLatitud.Value);
                    }
                    if (txtLongitud.Value.Trim().ToString() != "")
                    {
                        longitud = float.Parse(txtLongitud.Value);
                    }

                    txtCampoAdicional1.Value = usuario.CampoAdicional1;
                    txtCampoAdicional2.Value = usuario.CampoAdicional2;
                    txtCampoAdicional3.Value = usuario.CampoAdicional3;
                    txtCampoAdicional4.Value = usuario.CampoAdicional4;
                    txtCampoAdicional5.Value = usuario.CampoAdicional5;

                }
                else
                {
                    hidPk.Value = "";
                    MtxtCodigo.Disabled = GenCodigo;
                }

                if (GenCodigo)
                    MtxtNombre.Focus();
                else
                    MtxtCodigo.Focus();





                //@001 I
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_1, out this.valorCampoAdicional1, out this.DescripcionCampoAdicional1);
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_2, out this.valorCampoAdicional2, out this.DescripcionCampoAdicional2);
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_3, out this.valorCampoAdicional3, out this.DescripcionCampoAdicional3);
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_4, out this.valorCampoAdicional4, out this.DescripcionCampoAdicional4);
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_5, out this.valorCampoAdicional5, out this.DescripcionCampoAdicional5);

                this.visibleCampoAdicional1 = (this.valorCampoAdicional1 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleCampoAdicional2 = (this.valorCampoAdicional2 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleCampoAdicional3 = (this.valorCampoAdicional3 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleCampoAdicional4 = (this.valorCampoAdicional4 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleCampoAdicional5 = (this.valorCampoAdicional5 == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                //@001 F

            }
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo, out String descripcionCampo)
        {
            valorCampo = "F";
            descripcionCampo = "";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                    descripcionCampo = configBean.Descripcion;
                }
            }
        }

        private void IniciarTipoCliente()
        {
            cboTipoCliente.DataTextField = "Nombre";
            cboTipoCliente.DataValueField = "Id";
            cboTipoCliente.DataSource = ClienteController.fnListarTipoCliente();
            cboTipoCliente.DataBind();
            cboTipoCliente.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }
    }
}