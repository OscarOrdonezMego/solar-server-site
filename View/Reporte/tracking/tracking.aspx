﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.tracking.tracking" Codebehind="tracking.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="<%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es&libraries=drawing,places&client=<%=(ConfigurationManager.AppSettings["MAPS_API_CLIENT"])%>"></script>--%>
    <script src="../../js/alertify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../css/alertify.core.css" />
    <link rel="stylesheet" href="../../css/alertify.default.css" />

     <script type="text/javascript" src="<%
        string url = "&";
        string client = (ConfigurationManager.AppSettings["MAPS_API_CLIENT"]);
        if (!client.Equals("")){
            client = "client=" + client;
            url += client;
        }
        string key = (ConfigurationManager.AppSettings["MAPS_API_KEY"]);
        if (!key.Equals("")){
            key = "key=" + key;
            url += key;
        }
        %><%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es&libraries=drawing,places<%=url%>"></script>

    <title></title>
    <style type="text/css">
        .modalGeo {
            right: 0%;
            left: inherit;
            width: 300px;
        }

        #map img {
            max-width: none;
        }

        #map label {
            width: auto;
            display: inline;
        }
    </style>
    <script>
        function pulsar(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13) return false;
        }
    </script>
    <script>
        var markersArray = [];
        var numero;
        var posiciones = [];
       // var urlLoad = 'ruta.aspx/cargarRuta';
        var urlTracking = 'tracking.aspx/cargarTracking';
        var infovendedorefi = 'tracking.aspx/infovendedorefi';


        $(document).ready(function () {
           // clickCBOSupervisor();
           // BUSCARGRUPOSCUANDOHACEMOSCHANGE();
            $('#cboVendedorcod').change(function () {
                var send = new Object();
                send.index = this.selectedIndex;
                var i = this.selectedIndex;
                $.ajax({
                    type: 'POST',
                    url: 'tracking.aspx/getSelectedIndex',
                    data: JSON.stringify(send),
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (data) {
                        numero = data.d.telefono;
                        nombre = data.d.nombre;
                        $('#numberSelected').val(numero);
                        $('#usuarioSelected').val(nombre);
                    },
                    error: function (xhr, status, error) {
                    }
                });
                console.log(i);
            });

            initialize('<%=latitudIni %>', '<%=longitudIni %>');
            
            $('#btnpedidos').click(function (e) {
                // deleteMarkerRuta();
                transacciones();
            });
            $('#btnGenerar').click(function (e) {
                // deleteMarkerRuta();
                var obj = getDataWS();
                
                clearinfowindow();
                clearFlighpath();
                deleteMarkerRutaWSDR()
                deleteMarkerRuta();
                console.log(getDataWS());
                if (obj.usuario == "") {
                    alertify.log("Debe seleccionar un vendedor", 10000);
                } else {
                    tracking();
                 // cargaPointsRuta(urlTracking, true);
                  setTimeout(function () {
                        cargaPointsRuta(urlTracking, true);
                    }, 2000);

                }
                infoVendedor();

            });
            $('#btnCancelar').click(function (e) {
                clearinfowindow();
                clearFlighpath();
                deleteMarkerRutaWSDR();
                deleteMarkerRuta();
                $("#divvisible").hide();
                $('#lblPedidosRealizados').text("");
                $('#lblNopedidos').text("");
                $('#lblmontoventa').text("");
                $('#lblcantitems').text("");
                $('#lblcobranza').text("");
            });
        });

        function infoVendedor() {
            var obj = getDataWS();
            var cod = obj.usuario;
            var fec = obj.fecha;

            var vend = {
                cod : cod,
                fec: fec
            }
            $.ajax({
                type: "POST",
                data: JSON.stringify(vend),
                contentType: "application/json; charset=utf-8",
                url: infovendedorefi,
                dataType: "json",
                success: function (data) {
                   var objdata = JSON.parse(data.d);
                   $('#lblPedidosRealizados').text(objdata.pedidosrealizados);
                    $('#lblNopedidos').text(objdata.nopedidos);
                    $('#lblmontoventa').text(objdata.montototalventa);
                    $('#lblcantitems').text(objdata.cantidadtotalitems);
                    $('#lblcobranza').text(objdata.montocobranza);
                    console.log(objdata);

                },
                error: function (data) { console.log(data); console.log(url); }
            });
        }

        function tracking() {

            //27-09-2016
          
            var number = numero.substring(2, 11);
            var obj = getDataWS();
            if (obj.usuario == "") {
                alertify.log("Debe seleccionar un vendedor", 10000);
            } else {
                var fecha = obj.fecha;
                var fec = fecha.replace(/\//g, "-");
                var urltrack = '<%=rutaws %>' + number + "/" + fec + "";
                //    var urltrack = " http://localhost/WsPedSdr/Service1.svc/tracking/" + number + "/" + fec + "";
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: urltrack,
                    beforeSend: function () {
                        $(".form-gridview-search").show();
                        $("#leyenda").find('ul').html('');
                        $("#leyenda").hide();
                    },
                    dataType: "json",
                    success: function (data) {
                        markerWSsdr(data, obj);
                        posiciones.push(data);

                        console.log(posiciones);

                    },
                    error: function (data) { console.log(data); console.log(url); }
                });
            }
        }
        function cargaPointsRuta(urlM, polilyne) {
            // deleteMarker();
          
            $.ajax({
                type: 'POST',
                url: urlM,
                data: JSON.stringify(getDataWS()),
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                dataType: "json",
                beforeSend: function () {
                    $(".form-gridview-search").show();
                    $("#leyenda").find('ul').html('');
                    $("#leyenda").hide();
                },
                success: function (data) {
                        if (data.d == "[]") {
                            alertify.log("No existe Puntos de ventas en esta fecha.", 10000);
                        } else {
                            var items = 0;
                            var isAlone = true;
                            $.each(jQuery.parseJSON(data.d), function (index, objPoint) {
                                crearPoint(objPoint, jQuery.parseJSON(data.d));
                                $("#divvisible").show();
                                if (objPoint.tipo != "") {
                                    items++;

                                    if (objPoint.tipo != '-1') {
                                        isAlone = false;
                                        $("#leyenda").find('ul').append('<li style="list-style: none;"><img style="padding-right: 10px;" src="icono.aspx?indice=' + objPoint.tipo + '&proceso=leyenda.png" border="0"/>' + objPoint.vendedor + '</li>');
                                    }
                                    else {

                                        $("#leyenda").find('ul').append('<li style="list-style: none;"><img style="padding-right: 10px;" src="../../images/gps/leyenda.png" border="0"/>' + objPoint.vendedor + '</li>');
                                    }
                                }
                            });

                        }
                    if (items > 1) {
                        console.log(isAlone);
                        if (!isAlone) {
                            $("#leyenda").show();
                        }
                    }


                    $('.form-gridview-search').hide();

                },

                error: function (xhr, status, error) {

                }
            });


        }
       /* function getData() {
            var StrData = new Object;
            StrData.usuario = $('#cboVendedorTel').val();
            StrData.fecha = $('#txtFecha').val();
            return StrData;
        }*/
        function getDataWS() {
            var StrData = new Object;
            StrData.nombre = $("#usuarioSelected").val();
            StrData.usuario = $('#cboVendedorcod').val();
            StrData.fecha = $('#txtFecha').val();
            StrData.telefono = $('#numberSelected').val();
            return StrData;
        }



    </script>
</head>
<body onkeypress="return pulsar(event)">
    <form id="form1" runat="server">
        <input id="hAccion" type="hidden" />
        <input id="hNombre" type="hidden" />
        <input id="hCodigo" type="hidden" />
        <div id="map" class="cz-maps-full">
        </div>
        <div id="cz-util-hidden-right-ultpos-geo" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-left">
            <div class="cz-util-hidden-arrow">
            </div>
        </div>
        <div class="modalGeo hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="false" style="display: block;">
            <div id="myModalContent" class="modal-body">
                <div class="cz-form-box-content">
         
                    <div class="cz-form-content">
                        <p>
                         Vendedor
                        </p>
                        <asp:DropDownList  ID="cboVendedorcod"  runat="server" CssClass="cz-form-content-input-select" >
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                       <asp:TextBox ID="numberSelected" runat="server" type="hidden"/>
                        <asp:TextBox ID="usuarioSelected" runat="server" type="hidden"/>
           
              <div class="cz-form-content">
                        <p>
                            <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FECHA)%>
                        </p>
                        <input name="txtFecha" type="text" value="30/05/2014" maxlength="10" id="txtFecha"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                     </div>
               </div>

                  
                    <div class="cz-form-content">
                        <div class="form-gridview-search">
                            <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                            <p>
                                buscando resultados
                            </p>
                        </div>
                    </div>

                    <%--BOTONES--%>
                    <div class="cz-form-content">
                        <input type="button" id="btnGenerar" value="Generar" class="form-button cz-form-content-input-button" />
                        <input type="button" id="btnCancelar" value="Limpiar" class="form-button cz-form-content-input-button" />
                    </div>
                 <div class="cz-form-content" id="divvisible" style="display: none;">
                 <input type="button" id="btnpedidos" value="Resaltar" class="form-button cz-form-content-input-button" />
                </div>


                <div id="leyenda" style="display: none;" class="cz-form-box-content">
                    <div class="cz-form-content">

                        <ul>
                        </ul>
                    </div>

                </div>
                <div id="cz-modal-geo-option2">
                    <input name="address" type="text" id="address" style="width: 300px" placeholder="Ingresa Dirección" />
                    <div class="cz-modal-geo-back">
                    </div>
                    <div class="contentGeo">
                    </div>
                </div>
            </div>
        </div>
            <div id="mymodalInfoVendedor" class="modal-body">
                <div class="cz-form-content" style="height: 30px">
                            <span>Pedidos Realizados : </span>
                            <span id="lblPedidosRealizados"></span>
                        </div>
                        <div class="cz-form-content" style="height: 30px">
                            <span>No Pedidos : </span>
                            <span id="lblNopedidos"></span>
                        </div>
                              <div class="cz-form-content" style="height: 30px">
                            <span>Monto total de Venta : </span>
                            <span id="lblmontoventa"></span>
                        </div>
                          <div class="cz-form-content" style="height: 30px">
                            <span>Cantidad total de items : </span>
                            <span id="lblcantitems"></span>
                        </div>
                    <div class="cz-form-content" style="height: 30px">
                            <span>Monto cobrado : </span>
                            <span id="lblcobranza"></span>
                        </div>
                </div>
            </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
    </form>
    <div id="calendarwindow" style="z-index: 9000;" class="calendar-window">
</body>
</html>
