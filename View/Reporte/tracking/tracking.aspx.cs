﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using System.Web.Services;
using Controller.functions;
using Model;
using System.Configuration;

namespace Pedidos_Site.Reporte.tracking
{
    public partial class tracking : PageController
    {
        public string latitudIni;
        public string fecha;
        public string numero;
        public string longitudIni;
        public List<PerfilBean> listaPerfil = new List<PerfilBean>();
        public static String micodMenu = "RRU";
        public static String id_usu = "";

        public static String codigoSub = "";
        public static String pkusu = "";
        public static List<String> numeros = null;
        public static List<String> nombres = null;
        public string rutaws;
        protected override void initialize()
        {
            rutaws = ConfigurationManager.AppSettings["WSTracking"].ToString();
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }

            if (Session["lgn_id"] == null || Session["lgn_codsupervisor"] == null || !fnValidarPerfilMenu(micodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            if (!IsPostBack)
            {
                //  llenarComboBox();
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                if (codigoSub == "SUP")
                {
                    //  cboPerfil.SelectedValue = pkusu;
                    //  deshabilitarCboSupervisor = Operation.CSS.STYLE_DISPLAY_NONE;
                    // llenarComboBoxGrupo();
                    llenarComboBoxVendedor(pkusu);
                }
                else
                {
                    llenarComboBox();
                }
                //   llenarcoGrupo();
                //   llenarComboBoxVendedor(pkusu);
            }
            if (ManagerConfiguration.laltitudlongitud != "0")
            {
                String[] latlo = ManagerConfiguration.laltitudlongitud.Split(',');
                /*
                cboVendedor.DataSource = ReporteController.getUSuarios_Ruta();
                cboVendedor.DataValueField = "USR_CODIGO";
                cboVendedor.DataTextField = "USR_NOMBRE";

                try
                {
                    cboVendedor.DataBind();
                    cboVendedor.Items.Insert(0, "--" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TODOS) + "--");
                    cboVendedor.Items[0].Value = "-1";
                }


                catch (Exception) { }
                */
                latitudIni = latlo[0];
                longitudIni = latlo[1];

                txtFecha.Value = Utils.getFechaActual();
                fecha = txtFecha.Value;
            }
            else
            {
                latitudIni = "- 12.0462037802177";
                longitudIni = "-77.0656585693359";
            }
        }
        /* private void llenarcoGrupo()
         {
             List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
             if (loLstGrupoBean.Count != 0)
             {
                 cboGrupro.Items.Insert(0, "Todos");
                 cboGrupro.Items[0].Value = "0";
                 for (int i = 0; i < loLstGrupoBean.Count; i++)
                 {
                     cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                     cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                 }
             }
             else
             {
                 cboGrupro.Items.Insert(0, "Todos");
                 cboGrupro.Items[0].Value = "0";
             }
         }*/
        private void llenarComboBox()
        {
            //List<UsuarioBean> lista = UsuarioController.ListaSupervisores();
            List<UsuarioBean> lista = UsuarioController.ListaVendedores();
            numeros = new List<String>();
            nombres = new List<String>();
            if (lista.Count != 0)
            {

                cboVendedorcod.Items.Insert(0, "Seleccione usuario");
                cboVendedorcod.Items[0].Value = "";

                for (int i = 0; i < lista.Count; i++)
                {
                    cboVendedorcod.Items.Insert(i + 1, lista[i].nombre + " - " + lista[i].telefono);
                    // cboVendedor.Items[i + 1].Value = lista[i].id;
                    cboVendedorcod.Items[i + 1].Value = lista[i].id;
                    numeros.Add(lista[i].telefono);
                    nombres.Add(lista[i].nombre);
                }
            }
            else
            {
                cboVendedorcod.Items.Insert(0, "Todos");
                cboVendedorcod.Items[0].Value = "";

            }
        }
        /*private void llenarComboBoxGrupo(String codigo, String perfil)
          {
              List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), perfil);
              if (lista.Count != 0)
              {
                  cboGrupro.Items.Insert(0, "Todos");
                  cboGrupro.Items[0].Value = "0";
                  for (int i = 0; i < lista.Count; i++)
                  {
                      cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                      cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();

                  }
              }
              else
              {
                  cboGrupro.Items.Insert(0, "Todos");
                  cboGrupro.Items[0].Value = "0";
              }
          }*/
        private void llenarComboBoxVendedor(String codigo)
        {
            numeros = new List<String>();
            nombres = new List<String>();
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedorcod.Items.Insert(0, "Seleccione usuario");
                cboVendedorcod.Items[0].Value = "";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedorcod.Items.Insert(i + 1, lstGrupoBean[i].nombre + " - " + lstGrupoBean[i].telefono);
                    cboVendedorcod.Items[i + 1].Value = lstGrupoBean[i].id.ToString();
                    numeros.Add(lstGrupoBean[i].telefono);
                    nombres.Add(lstGrupoBean[i].nombre);
                }
            }
            else
            {
                cboVendedorcod.Items.Insert(0, "Todos");
                cboVendedorcod.Items[0].Value = "";
            }
        }
        /*    private void llenarCboGrupos(Int32 USUPK, String TIPO)
            {
                List<GrupoBean> loLstGrupoBean = UsuarioController.fnListarGrupoActivo(USUPK, TIPO);
                if (loLstGrupoBean.Count != 0)
                {
                    cboGrupro.Items.Insert(0, "Todos");
                    cboGrupro.Items[0].Value = "0";
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                        cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                    }
                }
            }*/
        [WebMethod]
        public static string infovendedorefi(String cod, String fec)
        {
            try
            {
                EficienciaBean infovendedor = ReporteController.infovendedor(cod, Utils.getStringFechaYYMMDD(fec));

                return JSONUtils.serializeToJSON(infovendedor);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }


        [WebMethod]
        public static UsuarioBean getSelectedIndex(int index)
        {
            int i = index;
            String numero = numeros[i - 1];
            String nombre = nombres[i - 1];
            UsuarioBean beanUsuario = new Model.bean.UsuarioBean();
            beanUsuario.telefono = numero;
            beanUsuario.nombre = nombre;

            return beanUsuario;
        }

        [WebMethod]
        public static string cargarTracking(String usuario, String fecha, String telefono)
        {
            try
            {
                List<MapBean> lst = ReporteController.RutaBuscarWSsdr(usuario, Utils.getStringFechaYYMMDD(fecha), "", "", "0", "");

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }


        }

        [WebMethod]
        public static string ultposicion(String perfil, String satelite)
        {

            try
            {

                return "";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }

        }

        [WebMethod]
        public static String MostrarVendedoresGrupoSupervisor(String codigoGrupo, String codigoSupervisor)
        {
            String Rpta = "";
            List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresGrupoSupervisor(codigoGrupo, codigoSupervisor);
            if (lstGrupoBean.Count > 0)
            {
                Rpta = ToolBox.Option("-1", "", "Todos").ToString();
                for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                {
                    Rpta += ToolBox.Option(lstGrupoBean[i].Usu_PK.ToString(), "", lstGrupoBean[i].nombre);
                }
            }
            else
            {
                Rpta = ToolBox.Option("-1", "", "No Hay Registro.").ToString();
            }
            return Rpta;
        }

        [WebMethod]
        public static String MostrarGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Length == 0)
            {
                codigo = "0";
            }
            if (codigo.Equals("0"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count != 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);

                    }
                }
                else
                {
                    Rpta = ToolBox.Option("0", "", "No Hay Registro.").ToString();
                }
            }
            else
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), "SUP");
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Grupo_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {


                }
            }
            return Rpta;
        }
    }
}