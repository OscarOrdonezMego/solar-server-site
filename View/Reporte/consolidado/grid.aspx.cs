﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Controller.functions;
using System.IO;
using System.Data;
using Model;

namespace Pedidos_Site.Reporte.consolidado
{
    public partial class Reporte_consolidado_grid : PageController
    {
        protected override void initialize()
        {
            string json = new StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
            String vendedor = dataJSON["vendedor"].ToString();
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }


            PaginateConsolidadoBean paginate = ReporteController.fnObtenerReporteConsolidado(fini, ffin, vendedor, pagina, filas, codperfil, codgrupo, id_usu);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                DataTable loConsolidado = paginate.loConsolidado;
                grdMant.DataSource = loConsolidado;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        protected void grdRepConsolidado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ReporteController.isReporteDinamicoGPS())
            {
                switch (e.Row.RowType)
                {
                    case DataControlRowType.Header:
                        break;
                    case DataControlRowType.DataRow:

                        int tamano = e.Row.Cells.Count;
                        string codigoPedido = e.Row.Cells[0].Text;

                        PedidoBean pedidoBean = ReporteController.obtenerGpsPedido(codigoPedido);

                        if (pedidoBean.LATITUD != null && pedidoBean.LONGITUD != null & !pedidoBean.LATITUD.Equals("0") && !pedidoBean.LONGITUD.Equals("0"))
                        {
                            e.Row.Cells[tamano - 1].Text = "<a role='button' data-toggle='modal' style='cursor: pointer; display:block' class='detGPS form-icon' cod='" + pedidoBean.LATITUD + "|" + pedidoBean.LONGITUD + "'><img src='../../imagery/all/icons/punto.png' border='0' title='GPS' alt='GPS'></a>";
                        }

                        break;
                    case DataControlRowType.Footer:

                        break;
                }
            }
        }
    }
}