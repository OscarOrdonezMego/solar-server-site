﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.consolidado.Reporte_consolidado_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:GridView ID="grdMant" runat="server" CssClass="grilla table table-bordered table-striped" GridLines="None"  Width="100%" BorderStyle="None" BorderWidth="1px" onrowdatabound="grdRepConsolidado_RowDataBound">
            <HeaderStyle  CssClass="orden,center" Width="40px"/>
            <AlternatingRowStyle CssClass="alt"/>
            <RowStyle /> 
        </asp:GridView>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server" >
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>