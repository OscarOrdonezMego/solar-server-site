﻿using Controller.functions;
using Model.bean;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;

namespace Pedidos_Site.Reporte.consolidado
{
    public partial class Reporte_consolidado_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";


        }


        [WebMethod]
        public static string cargarMapa(String codigo)

        {
            try
            {

                List<MapBean> lst = new List<MapBean>();

                MapBean be = new MapBean();

                string latitud = codigo.Split('|')[0];
                string longitud = codigo.Split('|')[1];

                be.latitud = latitud;
                be.longitud = longitud;
                be.img = "../../images/gps/s1.png";
                be.msg = "Latitud: " + latitud + "\n " + "Longitud: " + longitud;
                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);
            }

            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}