﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controller.functions;
using Model.bean;
using Controller;
using System.IO;
using Model;
using System.Text;
using System.Data;

namespace Pedidos_Site.Reporte
{
    public partial class Reporte_Exportar : PageController
    {
        public string Mbm = "";
        public Boolean bonificacion = false;
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        public Boolean flgMostrarFechaDespacho = false;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }

            String modulo = Request.QueryString["mod"].ToString();
            Mbm = ManagerConfiguration.nBonificacionManual;

            switch (modulo)
            {
                case "REP_PEDIDO":
                    String p_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String p_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String p_vend = Request.Params["fven"].ToString();
                    String p_cobertura = Request.Params["fcobertura"].ToString();
                    String p_bonificacion = Request.Params["fbonificacion"].ToString();
                    String P_TipoArticulo = "";
                    String p_codperfil = Request.Params["codperfil"].ToString();
                    String p_codgrupo = Request.Params["codgrupo"].ToString();
                    String p_flgFecReg = Request.Params["flagfecreg"].ToString();

                    if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                    {
                        P_TipoArticulo = "PRE";
                    }
                    else
                    {
                        P_TipoArticulo = "PRO";
                    }
                    List<ReportePedidoBean> datos = ReporteController.PedidoBuscarXLS(p_vend, p_fini, p_ffin,
                        p_cobertura, p_bonificacion, P_TipoArticulo, p_codperfil, p_codgrupo, id_usu, p_flgFecReg);

                    if (datos.Count > 0)
                    {
                        flgMostrarFechaDespacho = datos[0].FlgDireccionDespacho.ToString().Equals("T");
                    }

                    DataTable odtL = ReporteModel.findReportePedidoXLS(p_vend, p_fini, p_ffin, p_cobertura, p_bonificacion,
                        P_TipoArticulo, "-1", "-1", p_codperfil, p_codgrupo, id_usu, p_flgFecReg);

                    foreach (DataRow xrow in odtL.Rows)
                    {
                        String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
                        if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
                        {
                            bonificacion = true;
                        }
                        else
                        {
                            bonificacion = false;
                        }
                    }

                    Exportar<ReportePedidoBean>(datos, gvPedido, "Pedido");
                    break;

                case "REP_NOPEDIDO":
                    String a_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String a_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String a_vend = Request.Params["fven"].ToString();
                    String a_cobertura = Request.Params["fcobertura"].ToString();
                    String a_codperfil = Request.Params["codperfil"].ToString();
                    String a_codgrupo = Request.Params["codgrupo"].ToString();
                    String a_flgFecReg = Request.Params["flagfecreg"].ToString();
                    List<ReporteNoPedidoBean> ndatos = ReporteController.NoPedidoBuscarXLS(a_vend, a_fini, a_ffin, a_cobertura, a_codperfil, a_codgrupo, id_usu, a_flgFecReg);

                    Exportar<ReporteNoPedidoBean>(ndatos, gvNoPedido, "No Pedido");
                    break;

                case "REP_DEVOLUCION":
                    String d_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String d_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String d_vend = Request.Params["fven"].ToString();
                    String d_cobertura = Request.Params["fcobertura"].ToString();
                    String d_tipoArticulo = "";
                    String d_codperfil = Request.Params["codperfil"].ToString();
                    String d_codgrupo = Request.Params["codgrupo"].ToString();
                    List<ReporteDevolucionBean> ddatos = null;
                    if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                    {
                        d_tipoArticulo = "PRE";
                        ddatos = ReporteController.DevolucionBuscarXLS(d_tipoArticulo, d_vend, d_fini, d_ffin, d_cobertura, d_codperfil, d_codgrupo, id_usu);
                        Exportar<ReporteDevolucionBean>(ddatos, gvDevolucion, "Devolucion");
                    }
                    else
                    {
                        d_tipoArticulo = "PRO";
                        ddatos = ReporteController.DevolucionBuscarSimpleXLS(d_tipoArticulo, d_vend, d_fini, d_ffin, d_cobertura, d_codperfil, d_codgrupo, id_usu);
                        Exportar<ReporteDevolucionBean>(ddatos, gvDevolucionSimple, "Devolucion");
                    }
                    break;

                case "REP_CANJE":
                    String c_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String c_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String c_vend = Request.Params["fven"].ToString();
                    String c_cobertura = Request.Params["fcobertura"].ToString();
                    String c_tipoArtituclo = "";
                    String c_codperfil = Request.Params["codperfil"].ToString();
                    String c_codgrupo = Request.Params["codgrupo"].ToString();
                    if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                    {
                        c_tipoArtituclo = "PRE";
                        List<ReporteCanjeBean> cdatos = ReporteController.CanjeBuscarPresentacionXLS(c_tipoArtituclo, c_vend, c_fini, c_ffin, c_cobertura, c_codperfil, c_codgrupo, id_usu);
                        Exportar<ReporteCanjeBean>(cdatos, gvCanjepresentacion, "Canje");
                    }
                    else
                    {
                        c_tipoArtituclo = "PRO";
                        List<ReporteCanjeBean> cdatos = ReporteController.CanjeBuscarXLS(c_tipoArtituclo, c_vend, c_fini, c_ffin, c_cobertura, c_codperfil, c_codgrupo, id_usu);
                        Exportar<ReporteCanjeBean>(cdatos, gvCanje, "Canje");
                    }
                    break;

                case "REP_PEDIDOTOTAL":
                    String pt_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String pt_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String pt_vend = Request.Params["fven"].ToString();
                    String pt_cobertura = Request.Params["fcobertura"].ToString();
                    String pt_bonificacion = Request.Params["fbonificacion"].ToString();
                    String pt_codperfil = Request.Params["codperfil"].ToString();
                    String pt_codgrupo = Request.Params["codgrupo"].ToString();
                    String fTipoPedido = Request.Params["FlagTipoPedido"].ToString();
                    String pagina = Request.Params["pagina"].ToString();
                    String filas = Request.Params["filas"].ToString();

                    if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                    {
                        List<ReportePedidoTotalBean> ptdatos = ReporteController.paginarPedidopresetacionTotalBuscar(fTipoPedido, pt_vend, pt_fini, pt_ffin, pt_cobertura, pt_bonificacion, pagina, filas, pt_codperfil, pt_codgrupo, id_usu).lstResultados;
                        if (ptdatos.Count > 0) bonificacion = ptdatos[0].tieneBonificacion.Equals("0") ? false : true;
                        Exportar<ReportePedidoTotalBean>(ptdatos, gvPedidoTotalPresentacion, "Pedido Total");
                    }
                    else
                    {
                        List<ReportePedidoTotalBean> ptdatos = ReporteController.paginarPedidoTotalBuscar(fTipoPedido, pt_vend, pt_fini, pt_ffin, pt_cobertura, pt_bonificacion, pagina, filas, pt_codperfil, pt_codgrupo, id_usu).lstResultados;
                        if (ptdatos.Count > 0) bonificacion = ptdatos[0].tieneBonificacion.Equals("0") ? false : true;
                        Exportar<ReportePedidoTotalBean>(ptdatos, gvPedidoTotal, "Pedido Total");
                    }
                    break;

                case "REP_QUIEBRESTOCK":
                    String qs_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String qs_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String qs_vend = Request.Params["fven"].ToString();
                    String qs_cobertura = Request.Params["fcobertura"].ToString();
                    String qs_codigopro = Request.Params["fcodigoproducto"].ToString();
                    String qs_bonificacion = Request.Params["fbonificacion"].ToString();
                    String qs_codperfil = Request.Params["codperfil"].ToString();
                    String qs_codgrupo = Request.Params["codgrupo"].ToString();
                    List<ReporteQuiebreStockBean> qsdatos = ReporteController.PedidoStockQuiebreXLS(qs_vend, qs_fini, qs_ffin, qs_cobertura, qs_codigopro, qs_bonificacion, qs_codperfil, qs_codgrupo, id_usu);


                    DataTable odtqs = ReporteModel.findReporteStockQuiebreXLS(qs_vend, qs_fini, qs_ffin, qs_cobertura, qs_codigopro, qs_bonificacion, "-1", "-1", qs_codperfil, qs_codgrupo, id_usu);
                    foreach (DataRow xrow in odtqs.Rows)
                    {

                        String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
                        if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
                        {
                            bonificacion = true;
                        }
                        else
                        {
                            bonificacion = false;
                        }
                    }


                    Exportar<ReporteQuiebreStockBean>(qsdatos, gvQuiebreStock, "Quiebre Stock");
                    break;

                case "REP_COBRANZA":
                    String co_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String co_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String co_vend = Request.Params["fven"].ToString();
                    String co_codperfil = Request.Params["codperfil"].ToString();
                    String co_codgrupo = Request.Params["codgrupo"].ToString();

                    List<ReporteCobranzaBean> codatos = ReporteController.CobranzaBuscarXLS(co_vend, co_fini, co_ffin, co_codperfil, co_codgrupo, id_usu);

                    Exportar<ReporteCobranzaBean>(codatos, gvCobranza, "Cobranza");
                    break;

                case "REP_RESERVA":
                    String re_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String re_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String re_vend = Request.Params["fven"].ToString();
                    String re_bonificacion = Request.Params["fbonificacion"].ToString();
                    String re_codigopro = Request.Params["fcodigoproducto"].ToString();
                    String re_codperfil = Request.Params["codperfil"].ToString();
                    String re_codgrupo = Request.Params["codgrupo"].ToString();
                    List<ReporteReservaBean> redatos = ReporteController.ReservaBuscarXLS(re_vend, re_fini, re_ffin, re_codigopro, re_bonificacion, re_codperfil, re_codgrupo, id_usu);

                    DataTable odtre = ReporteModel.findReporteReservaXLS(re_vend, re_fini, re_ffin, re_codigopro, re_bonificacion, "-1", "-1", re_codperfil, re_codgrupo, id_usu);
                    foreach (DataRow xrow in odtre.Rows)
                    {
                        String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
                        if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
                        {
                            bonificacion = true;
                        }
                        else
                        {
                            bonificacion = false;
                        }
                    }

                    Exportar<ReporteReservaBean>(redatos, gvReserva, "Reserva");
                    break;

                case "REP_CONSOLIDADO":
                    String rc_fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String rc_ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String rc_vend = Request.Params["fven"].ToString();
                    String rc_codperfil = Request.Params["codperfil"].ToString();
                    String rc_codgrupo = Request.Params["codgrupo"].ToString();
                    DataTable loReporteConsolidado = ReporteModel.fnObtenerReporteConsolidadoXLS(rc_fini, rc_ffin, rc_vend, rc_codperfil, rc_codgrupo, id_usu);


                    if (ReporteController.isReporteDinamicoGPS())
                    {
                        loReporteConsolidado.Columns.Add("LATITUD", typeof(string));
                        loReporteConsolidado.Columns.Add("LONGITUD", typeof(string));

                        foreach (DataRow dr in loReporteConsolidado.Rows) // search whole table
                        {

                            string codigoPedido = dr["PEDIDO"].ToString();
                            PedidoBean pedidoBean = ReporteController.obtenerGpsPedido(codigoPedido);
                            dr["LATITUD"] = pedidoBean.LATITUD;
                            dr["LONGITUD"] = pedidoBean.LONGITUD;
                        }
                    }

                    ExportarDataTable(loReporteConsolidado, gvConsolidado, "Consolidado");
                    break;

                case "REP_PROSPECTO":

                    String fini = Utils.getStringFechaYYMMDD(Request.Params["fini"].ToString());
                    String ffin = Utils.getStringFechaYYMMDD(Request.Params["ffin"].ToString());
                    String codvendedor = Request.Params["fven"].ToString();
                    String prospecto = Request.Params["fpro"].ToString();

                    List<ClienteProspeccionBean> datosPros = ReporteController.ProspectoBuscarXLS(codvendedor, prospecto, fini, ffin);

                    Exportar<ClienteProspeccionBean>(datosPros, gvProspecto, "Prospecto");
                    break;
            }
        }
        private static void ValidarFraccionamiento(List<ConfiguracionBean> poLstConfiguracionBean)
        {
            FuncionBean loFuncionBean = new FuncionBean();
            loFuncionBean.CodigoFuncion = Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO;
            FuncionBean loFuncionBeanres = Operation.ValidacionSession.fnExtraerCodigoFuncion(poLstConfiguracionBean, loFuncionBean);
            ioRespuesta = Operation.ValidacionSession.fnValidarFuncion(loFuncionBeanres);
            ioRespuestaString = Operation.ValidacionSession.fnValidarFuncionOcultar(loFuncionBeanres);
        }

        private void ExportarDataTable(DataTable poDataTable, GridView poGridView, String psNombreXls)
        {

            poGridView.DataSource = poDataTable;
            poGridView.DataBind();

            // render the DataGrid control to a file

            MemoryStream mystream = new MemoryStream();
            StreamWriter sw = new StreamWriter(mystream);
            sw.AutoFlush = true;
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            poGridView.RenderControl(hw);

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + psNombreXls + ".xls");

            // La longitud del contenido es del tamano de la longitud del Stream (Nuestro documento) 
            // Response.AppendHeader("Content-Length", mystream.Length.ToString());

            //' Se define el tipo de contenido 
            //Response.ContentType = "application/octet-stream";

            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "UTF-8";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentEncoding = System.Text.Encoding.Default;
            string style = @"<style> .text { mso-number-format:\@; } </style> ";
            Response.Write(style);
            Response.BinaryWrite(mystream.ToArray());

            Response.End();

        }

        private void Exportar<T>(List<T> lista, GridView Grid, String NombreXLS)
        {
            Grid.DataSource = lista;
            Grid.DataBind();

            // render the DataGrid control to a file
            MemoryStream mystream = new MemoryStream();
            StreamWriter sw = new StreamWriter(mystream);
            sw.AutoFlush = true;
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Grid.RenderControl(hw);
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + NombreXLS + ".xls");

            // La longitud del contenido es del tamano de la longitud del Stream (Nuestro documento) 
            // Response.AppendHeader("Content-Length", mystream.Length.ToString());

            //' Se define el tipo de contenido 
            //Response.ContentType = "application/octet-stream";

            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "UTF-8";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentEncoding = System.Text.Encoding.Default;
            string style = @"<style> .text { mso-number-format:\@; } </style> ";
            Response.Write(style);
            Response.BinaryWrite(mystream.ToArray());

            Response.End();

        }

        private string camposGrid(String CODIGO)
        {


            switch (CODIGO)
            {
                case "1":
                    return "FECHAINICIO";
                case "2":
                    return "FECHAFIN";
                case "3":
                    return "PERFIL";
                case "4":
                    return "CLIENTE";
                case "5":
                    return "USUARIO";
                case "6":
                    return "FECHAESTADO";

                case "8":
                    return "NOVISITA";
                case "9":
                    return "ESTADOASIG";
                case "10":
                    return "ESTADOREAL";
                case "11":
                    return "TIPOACTIVIDAD";
                case "12":
                    return "ESTADO";
                case "13":
                    return "ORDEN";
                case "14":
                    return "CODIGO";
                case "15":
                    return "SITUACIONRUTA";
                default: return "";


            }
        }

        private void XLSExport(DataTable dt, String[] cabeceras, String[] campos)
        {
            // render the DataGrid control to a file
            MemoryStream mystream = new MemoryStream();
            StreamWriter sw = new StreamWriter(mystream);
            sw.AutoFlush = true;
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            StringBuilder str = new StringBuilder();
            //Inicializa el HTML

            str.Append("<table border=\"1\" cellspacing=\"0\" rules=\"all\" style=\"border-collapse:collapse;\" >");
            str.Append("<thead><tr>");

            for (int i = 0; i < cabeceras.Length; i++)
            {
                str.Append("<td>" + cabeceras[i].ToString() + "</td>");
            }

            str.Append("</tr></thead>");
            str.Append("<tbody>");

            foreach (DataRow dr in dt.Rows)
            {
                str.Append("<tr>");
                for (int i = 0; i < campos.Length; i++)
                {
                    str.Append("<td class=\"text\">" + dr[campos[i].ToString()] + "</td>");
                }
                str.Append("</tr>\n");

            }

            //Cierra el HTML
            str.Append("</tbody>");
            str.Append("</table>");

            hw.Write(str);

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + dt.TableName + ".xls");

            // La longitud del contenido es del tamano de la longitud del Stream (Nuestro documento) 
            // Response.AppendHeader("Content-Length", mystream.Length.ToString());

            //' Se define el tipo de contenido 
            //Response.ContentType = "application/octet-stream";

            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "UTF-8";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentEncoding = System.Text.Encoding.Default;
            string style = @"<style> .text { mso-number-format:\@; } </style> ";
            Response.Write(style);
            Response.BinaryWrite(mystream.ToArray());

            Response.End();

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void gvPedido_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INICIO);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FIN);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DIRECCION);

                    e.Row.Cells[8].Text = "Dir. Despacho";
                    if (!flgMostrarFechaDespacho)
                    {
                        e.Row.Cells[8].Visible = false;
                    }

                    e.Row.Cells[9].Text = "Tipo";

                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CONDICION);
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[10].Visible = false;
                    }

                    //e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO);
                    e.Row.Cells[11].Text = "Monto Soles";
                    e.Row.Cells[12].Text = "Monto Dolares";
                    e.Row.Cells[13].Text = "Total Flete";
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DISTANCIA);
                    e.Row.Cells[16].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_HABILITADO);
                    e.Row.Cells[17].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[18].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
                case DataControlRowType.DataRow:

                    if (!flgMostrarFechaDespacho)
                    {
                        e.Row.Cells[8].Visible = false;
                    }

                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[10].Visible = false;
                    }
                    break;
            }
        }

        protected void gvPedido_RowDataBound_2(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INICIO);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FIN);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DIRECCION);
                    e.Row.Cells[8].Text = "Tipo";
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CONDICION);
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[9].Visible = false;
                    }
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DISTANCIA);
                    e.Row.Cells[13].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_HABILITADO);
                    break;
                case DataControlRowType.DataRow:
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[8].Visible = false;
                    }
                    break;
            }
        }

        protected void gvNoPedido_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INICIO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FIN);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DIRECCION);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MOTIVO_VALOR);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_HABILITADO);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
            }
        }

        protected void gvDevolucion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ALMACEN);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRESENTACION);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD_PRESENTACION);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_UNID_FRACIONAMIENTO);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FRACCION);
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRECIO);
                    e.Row.Cells[13].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MOTIVO);
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA_VENCIMIENTO);
                    e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[16].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[17].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
            }
        }

        protected void gvDevolucion_RowDataBound_simple(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ALMACEN);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MOTIVO);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA_VENCIMIENTO);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
            }
        }

        protected void gvCanje_RowDataBound_simple(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ALMACEN);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MOTIVO);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA_VENCIMIENTO_CANJE);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
            }
        }

        protected void gvCanje_RowDataBound_presentacion(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ALMACEN);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRESENTACION);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD_PRESENTACION);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_UNID_FRACIONAMIENTO);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FRACCION);
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MOTIVO);
                    e.Row.Cells[13].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA_VENCIMIENTO_CANJE);
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[16].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);
                    break;
            }
        }

        protected void gvPedidoTotal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CONDICION);
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA);
                    e.Row.Cells[7].Text = "total soles";
                    e.Row.Cells[8].Text = "total dolares";
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_PRODUCTO);
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[12].Text = "precio soles";
                    e.Row.Cells[13].Text = "precio dolares";
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DESCPORCENTAJE);
                    if (Mbm == "1" || bonificacion == true)
                    {
                        e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_BONIFICACIONREP);
                    }

                    e.Row.Cells[16].Text = "monto soles";
                    e.Row.Cells[17].Text = "monto dolares";
                    e.Row.Cells[18].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DESCRIPCION_PRODUCTO);
                    e.Row.Cells[19].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    break;
                case DataControlRowType.DataRow:
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                    break;
            }
        }

        protected void gvPedidoTotal_presentacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CONDICION);
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA);
                    e.Row.Cells[7].Text = "Tipo";
                    e.Row.Cells[8].Text = "total soles";
                    e.Row.Cells[9].Text = "total dolares";
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_PRODUCTO);
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);

                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MANT_PRESENTACION);
                    e.Row.Cells[13].Text = "Cantidad Presentacion";
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_UNID_FRACIONAMIENTO);



                    e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
                    e.Row.Cells[16].Text = "Fraccion";

                    e.Row.Cells[17].Text = "precio soles";
                    e.Row.Cells[18].Text = "precio dolares";
                    e.Row.Cells[19].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DESCPORCENTAJE);
                    if (Mbm == "1" || bonificacion == true)
                    {
                        e.Row.Cells[20].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_BONIFICACIONREP);
                    }

                    e.Row.Cells[21].Text = "monto soles";
                    e.Row.Cells[22].Text = "monto dolares";
                    e.Row.Cells[23].Text = "Total Flete";
                    e.Row.Cells[24].Text = "Monto Total";
                    e.Row.Cells[25].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DESCRIPCION_PRODUCTO);

                    e.Row.Cells[26].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_OBSERVACION);
                    break;
                case DataControlRowType.DataRow:
                    if (ManagerConfiguration.mostrar_condvta.Trim() == "0")
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                    break;
            }
        }

        protected void gvQuiebreStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_NAV_PEDIDO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INICIO);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FIN);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);

                    if (Mbm == "1" || bonificacion == true)
                    {
                        e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_BONIFICACIONREP);
                    }

                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_STOCK);

                    break;
            }
        }

        protected void gvCobranza_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_NAV_COBRANZA);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO_CLIENTE);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TIPO);
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TIPO_DOCUMENTO);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TOTAL) + "S/.";
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PAGADO) + "S/.";
                    e.Row.Cells[10].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_SALDO) + "S/.";
                    e.Row.Cells[11].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TOTAL) + "$.";
                    e.Row.Cells[12].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PAGADO) + "$.";
                    e.Row.Cells[13].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_SALDO) + "$.";
                    e.Row.Cells[14].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VOUCHER);
                    e.Row.Cells[15].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_BANCO);
                    e.Row.Cells[16].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TIPO);
                    e.Row.Cells[17].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FECHA);
                    e.Row.Cells[18].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO) + "S/.";
                    e.Row.Cells[19].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO) + "$.";
                    e.Row.Cells[20].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LATITUD);
                    e.Row.Cells[21].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_LONGITUD);

                    break;

            }
        }

        protected void gvReserva_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_NAV_PEDIDO);
                    e.Row.Cells[1].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INICIO);
                    e.Row.Cells[2].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CLIENTE);
                    e.Row.Cells[3].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CODIGO);
                    e.Row.Cells[4].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PRODUCTO);
                    e.Row.Cells[5].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);

                    if (Mbm == "1" || bonificacion == true)
                    {
                        e.Row.Cells[6].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_BONIFICACIONREP);
                    }
                    e.Row.Cells[7].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_STOCK);
                    e.Row.Cells[8].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);
                    e.Row.Cells[9].Text = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRUPO);

                    break;

            }
        }

        protected void gvProspecto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    e.Row.Cells[0].Text = "Codigo Prospecto";
                    e.Row.Cells[1].Text = "Nombre Prospecto";
                    e.Row.Cells[2].Text = "Direccion Prospecto";
                    e.Row.Cells[3].Text = "Codigo Vendedor";
                    e.Row.Cells[4].Text = "Nombre Vendedor";
                    e.Row.Cells[5].Text = "Cod Tipo Cliente";
                    e.Row.Cells[6].Text = "Tipo Cliente";
                    e.Row.Cells[7].Text = "Giro";
                    e.Row.Cells[8].Text = "Latitud";
                    e.Row.Cells[9].Text = "Longitud";
                    e.Row.Cells[10].Text = "Fecha Registro";
                    e.Row.Cells[11].Text = "Fecha Movil";
                    e.Row.Cells[12].Text = "Observacion";
                    e.Row.Cells[13].Text = "Campo Adicional 1";
                    e.Row.Cells[14].Text = "Campo Adicional 2";
                    e.Row.Cells[15].Text = "Campo Adicional 3";
                    e.Row.Cells[16].Text = "Campo Adicional 4";
                    e.Row.Cells[17].Text = "Campo Adicional 5";

                    break;
            }
        }
    }
}