﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using System.Data;
using Model;

namespace Pedidos_Site.Reporte.reserva
{
    public partial class Reporte_reserva_grid : PageController
    {
        public string Mbm = "";
        public Boolean bonificacion = false;
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = dataJSON["fini"].ToString();
            String ffin = dataJSON["ffin"].ToString();
            String fcodigoproducto = dataJSON["codigoproducto"].ToString();
            String fBonificacion = dataJSON["Bonificacion"].ToString();
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;

            String vendedor = dataJSON["vendedor"].ToString();
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();

            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }

            Mbm = ManagerConfiguration.nBonificacionManual;

            PaginateReporteReservaBean paginate = ReporteController.paginarPedidoBuscarReserva(vendedor, fini, ffin, fcodigoproducto, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReporteReservaBean> lst = paginate.lstResultados;

                DataTable odtL = ReporteModel.findReporteReservaBean(vendedor, fini, ffin, fcodigoproducto, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);
                foreach (DataRow xrow in odtL.Rows)
                {

                    String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
                    if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
                    {
                        bonificacion = true;
                    }
                    else
                    {
                        bonificacion = false;
                    }
                }


                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }
    }
}
