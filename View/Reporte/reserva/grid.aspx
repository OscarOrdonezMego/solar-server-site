﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.reserva.Reporte_reserva_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            
                            <th   nomb="PEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NAV_PEDIDO)%>
                            </th>
                            
                            <th nomb="FECHA">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHA)%>
                            </th>
                            <th nomb="CLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                             <th  nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th  nomb="PRODUCTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                            </th>
                             <th  nomb="CANTIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                           
                            <% if (Mbm == "1" || bonificacion == true)
                              { %>
                            <th  class=" orden" nomb="BONIFICACION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                            </th>
                              <% } %>
                            <th  nomb="STOCK">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%>
                            </th>

                            <th   nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                            <th   nomb="NOMBREGRUPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th style="width:40px;">
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
 
                   <td>
                        <%# Eval("idped ")%>
                    </td>
                     <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <td>
                        <%# Eval("pro_codigo")%>
                    </td>
                      <td>
                        <%# Eval("pro_nombre")%>
                    </td>
                   
                    <td>
                        <%# Eval("det_cantidad")%>
                    </td>
                    
                   
                    <% if (Mbm == "1" || bonificacion == true)
                              { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>
                      <% } %>

                       <td>
                        <%# Eval("pro_stock")%>
                    </td>

                     <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                     <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemRegDel form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar Usuario" /></a>
                    </td>
              
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">

                   <td>
                        <%# Eval("idped ")%>
                    </td>
                     <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <td>
                        <%# Eval("pro_codigo")%>
                    </td>
                      <td>
                        <%# Eval("pro_nombre")%>
                    </td>
                   
                    <td>
                        <%# Eval("det_cantidad")%>
                    </td>
                  
                    <% if (Mbm == "1" || bonificacion == true)
                              { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>
                      <% } %>

                       <td>
                        <%# Eval("pro_stock")%>
                    </td>
                     <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                       <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                      <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemRegDel form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar Usuario" /></a>
                    </td>           
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
