﻿using Controller.functions;
using Model.bean;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;

namespace Pedidos_Site.Reporte.pagosanulado
{
    public partial class GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');
            var tipo = (dDato[1].ToString().ToUpper() == "PAGO" ? true : false);
            var subtitle = (tipo ? "Anulado" : "Modificado");
            htxtTipoDocumento.Text = dDato[1].ToString();
            titleTipoDocumento.Text = dDato[1].ToString() + " " + subtitle;
            lblTipoDocumento.Text = dDato[1].ToString();
            lblNomUsuario.Text = dDato[2].ToString();
            lblFechaRegistro.Text = dDato[3].ToString();
            lblCliente.Text = dDato[4].ToString();
            lblMonto.Text = (tipo ? dDato[5].ToString() : dDato[7].ToString());
            lblMontoDolares.Text = (tipo ? dDato[6].ToString() : dDato[8].ToString());
            lblTipoDoc1.Text = (tipo ? (IdiomaCultura.getMensaje(IdiomaCultura.WEB_MONTO) + " S/: ") : "Serie: ");
            lblTipoDoc2.Text = (tipo ? (IdiomaCultura.getMensaje(IdiomaCultura.WEB_MONTO) + " $: ") : "Correlativo: ");
        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {
            try
            {

                String[] xdatos = codigo.Split('|');

                String xlat = xdatos[9].ToString();
                String xlon = xdatos[10].ToString();
                String xusu = xdatos[2].ToString();
                String xfec = xdatos[3].ToString();
                String xidd = xdatos[0].ToString();
                String xcli = xdatos[4].ToString();
                String xmons = xdatos[5].ToString().Trim();
                String xmond = xdatos[6].ToString().Trim();
                String xser = xdatos[8].ToString();
                String xcor = xdatos[7].ToString();
                String xtip = xdatos[1].ToString();
                var tipo = (xdatos[1].ToString().ToUpper() == "PAGO" ? true : false);
                var subtitle = (tipo ? "Anulado" : "Modificado");

                List<MapBean> lst = new List<MapBean>();


                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;

                be.msg = "<h4>" + xtip + " " + subtitle + ":" + "</h4>Nro Doc: " + xidd + "</br> " +
                    IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + xusu + "</br>" +
                    IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHAREGISTRO) + ": " + xfec + "</br>" +
                    IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE) + ": " + xcli + "</br>";
               
                    if (tipo) {
                        if (Convert.ToDouble(xmons == "" ? "0" : xmons) > 0)
                        {   
                            be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_MONTO) + " S/ : " + xmons + "</br>";
                        }
                    if (Convert.ToDouble(xmond == "" ? "0" : xmond) > 0)
                    {
                            be.msg += IdiomaCultura.getMensaje(IdiomaCultura.WEB_MONTO) + " $ : " + xmond + "</br>";
                        }
                    }
                    else
                    {
                        be.msg += "Serie : " + xser + "</br>" +
                            "Correlativo : " + xcor + "</br>";
                    }

                be.img = "../../images/gps/pedido.png";

                be.titulo = "Documento" + xtip;

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }


        }
    }
}