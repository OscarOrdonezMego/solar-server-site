﻿using Controller;
using Controller.functions;
using Model;
using Model.bean;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Pedidos_Site.Reporte.pagosanulado
{
    public partial class grid : PageController
    {
        public string Mcv = "";
        private String valorDireccionDespacho = "F";
        public String visibleDireccionDespacho = "";
        private String valorConfigMostrarAlmacen = "F";
        public String visibleColumnaAlmacen = "";
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        protected override void initialize()
        {
            

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String fTipoDocumento = dataJSON["tipodoc"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String vendedor = dataJSON["vendedor"].ToString();
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;

            PaginateReporteDocumentoBean paginate = ReporteController.paginarDocumentoBuscar(fini, ffin,
                fTipoDocumento, codgrupo, vendedor, pagina, filas);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReporteDocumentoBean> lst = paginate.lstResultados;

                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }
    }
}