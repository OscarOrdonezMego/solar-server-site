﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="grid.aspx.cs" Inherits="Pedidos_Site.Reporte.pagosanulado.grid" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                           <th nomb="FECHA_REGISTRO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAREGISTRO)%>
                            </th>
                            <th nomb="TIPO">Tipo</th>
                            <th nomb="NOMGRUPO">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBREGRUPO)%>
                            </th>
                            <th nomb="NOMUSR">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FILENAME_VENDEDOR)%>
                            </th>
                            <th nomb="CODCLI">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_CLIENTE)%>
                            </th>
                            <th nomb="NOMCLI">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE_CLIENTE)%>
                            </th>
                           <th nomb="SERIE">
                               Serie
                            </th>
                            <th nomb="CORR">
                               Correlativo
                            </th>
                            <th  nomb="MONTO_SOLES">
                                Monto Soles
                            </th>
                            <th  nomb="MONTO_DOLARES">
                                Monto Dólares
                            </th>
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("doc_fecregistro")%>
                    </td>
                    <td>
                        <%# Eval("doc_tipo")%>
                    </td>
                    <td>
                        <%# Eval("gru_nombre")%>
                    </td>
                    <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                     <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <td>
                        <%# Eval("serie")%>
                    </td>
                    <td>
                        <%# Eval("correlativo")%>
                    </td>
                   
                    <td>
                        <%# Eval("pag_montosoles")%>
                    </td>
                    <td>
                        <%# Eval("pag_montodolares")%>
                    </td>
                    <td>
                          <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("id") %>|<%# Eval("doc_tipo") %>|<%# Eval("nom_usuario") %>|<%# Eval("doc_fecregistro") %>|<%# Eval("cli_nombre") %>|<%# Eval("pag_montosoles") %>|<%# Eval("pag_montodolares") %>|<%# Eval("serie") %>|<%# Eval("correlativo") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <%# Eval("doc_fecregistro")%>
                    </td>
                    <td>
                        <%# Eval("doc_tipo")%>
                    </td>
                    <td>
                        <%# Eval("gru_nombre")%>
                    </td>
                    <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                     <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <td>
                        <%# Eval("serie")%>
                    </td>
                    <td>
                        <%# Eval("correlativo")%>
                    </td>
                   
                    <td>
                        <%# Eval("pag_montosoles")%>
                    </td>
                    <td>
                        <%# Eval("pag_montodolares")%>
                    </td>
                    <td>
                          <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("id") %>|<%# Eval("doc_tipo") %>|<%# Eval("nom_usuario") %>|<%# Eval("doc_fecregistro") %>|<%# Eval("cli_nombre") %>|<%# Eval("pag_montosoles") %>|<%# Eval("pag_montodolares") %>|<%# Eval("serie") %>|<%# Eval("correlativo") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>
                    </td>    
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" alt="" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>