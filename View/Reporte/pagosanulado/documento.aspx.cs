﻿using Controller;
using Controller.functions;
using Model;
using Model.bean;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace Pedidos_Site.Reporte.pagosanulado
{
    public partial class documento : PageController
    {
        public static String lsCodMenu = "RDA";
        public static String codigoSub = "";
        public static String pkusu = "";
        public static Int32 ioRespuesta;
        public static String ioRespuestaString = "";
        public static String id_usu = "";

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || Session["lgn_codsupervisor"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            if (!IsPostBack)
            {
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                llenarcoTipo();
                llenarcoGrupo();
                llenarComboBoxVendedor(pkusu);
                txtFechaIni.Value = Utils.getFechaActual();
                txtFechaFin.Value = Utils.getFechaActual();
            }

            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }
        }
        private void llenarcoTipo()
        {
            cboTipoDocumento.Items.Insert(0, "Todos");
            cboTipoDocumento.Items[0].Value = " ";

            cboTipoDocumento.Items.Insert(1, "Anulados");
            cboTipoDocumento.Items[1].Value = "P";

            cboTipoDocumento.Items.Insert(2, "Modificados");
            cboTipoDocumento.Items[2].Value = "C";
        }
        private void llenarcoGrupo()
        {
            List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxGrupo(String codigo, String perfil)
        {
            List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), perfil);
            if (lista.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                    cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();
                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();

                }
            }
            else
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
            }
        }

        private void llenarCboGrupos(Int32 USUPK, String TIPO)
        {
            List<GrupoBean> loLstGrupoBean = UsuarioController.fnListarGrupoActivo(USUPK, TIPO);
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
        }

        [WebMethod]
        public static String cargarMapaPago(String fini, String ffin, String vendedor, String FlagEnCobertura, String Bonificacion, String codperfil, String codgrupo, String FlagTipoPedido)
        {
            String tipoArticulo = "";
            List<ReportePedidoBean> list = ReporteController.ListaCordenadaPedido(vendedor, Utils.getStringFechaYYMMDD(fini),
                Utils.getStringFechaYYMMDD(ffin), FlagEnCobertura, Bonificacion,
                FlagTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);

            return JSONUtils.serializeToJSON(list);
        }

        [WebMethod]
        public static String MostarGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Length == 0)
            {
                codigo = "0";
            }
            if (codigo.Equals("0"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count != 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);

                    }
                }
                else
                {
                    Rpta = ToolBox.Option("0", "", "No Hay Registro.").ToString();
                }
            }
            else
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), "SUP");
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Grupo_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
            }
            return Rpta;
        }
        [WebMethod]
        public static String MostarVendedoresPorGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Equals("0"))
            {
                List<UsuarioBean> lstUsuarioBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(pkusu));
                if (lstUsuarioBean.Count != 0)
                {
                    Rpta += ToolBox.Option("-1", "", "Todos");
                    for (int i = 0; i < lstUsuarioBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstUsuarioBean[i].id.ToString(), "", lstUsuarioBean[i].nombre);

                    }
                }
            }
            else
            {
                List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresPorGrupo(Int32.Parse(codigo));
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("-1", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Usu_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    Rpta = ToolBox.Option("-1", "", "No Hay Registro.").ToString();
                }
            }

            return Rpta;
        }


        [WebMethod]
        public static List<ListItem> ComboMultGrupo()
        {
            try
            {
                List<ListItem> lstComboBean = GrupoController.fnListarGrupo().Select(x => new ListItem()
                {
                    Text = x.nombre.ToString(),
                    Value = x.Grupo_PK.ToString(),
                    Selected = true,
                }).ToList();

                return lstComboBean;
            }
            catch (Exception ex)
            {
                return new List<ListItem>();
            }
        }

        [WebMethod]
        public static List<ListItem> ComboMultVendedor(String grupos)
        {
            try
            {
                int cod = Convert.ToInt32(HttpContext.Current.Session["lgn_id"].ToString());
                List<ListItem> lstComboBean = UsuarioController.ListaVendedorPorSupervisorMultiple(cod, grupos).Select(x => new ListItem()
                {
                    Text = x.nombre.ToString(),
                    Value = x.id.ToString(),
                    Selected = true,
                }).ToList();

                return lstComboBean;
            }
            catch (Exception ex)
            {
                return new List<ListItem>();
            }
        }


    }
}