﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.graficos.graficos" Codebehind="graficos.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <script>
      google.load('visualization', '1', { packages: ['gauge'] });
        google.load("visualization", "1", { packages: ["corechart"] });

        

        $(document).ready(function () {

            //            busReg();
            //            $('#buscar').trigger("click");
            //            exportarReg(".xlsReg", "../exportar.aspx", '.grilla'); //funcion encarga de la exportacion
            //            detReg(".detGPS", "GMAPS.aspx");
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });



            $("#cboOperacionr2").change(function () {

                if ($("#cboOperacionr2").val() == "P") { //PEDIDO
                    $('#cboTipor2').val('C');
                    $("#cboTipor2").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor2").prop("disabled", false)
                }
                if ($("#cboOperacionr2").val() == "CO") { //COBRANZA
                    $('#cboTipor2').val('M');
                    $("#cboTipor2").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor2").prop("disabled", true)
                }
                if ($("#cboOperacionr2").val() == "C" || $("#cboOperacionr2").val() == "D") { //PEDIDO
                    $('#cboTipor2').val('C');
                    $("#cboTipor2").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor2").prop("disabled", true)
                }

            });

            $("#cboOperacionr3").change(function () {

                if ($("#cboOperacionr3").val() == "P") { //PEDIDO
                    $('#cboTipor3').val('C');
                    $("#cboTipor3").each(function () {
                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor3").prop("disabled", false)
                }
                 if ($("#cboOperacionr3").val() == "CO") { //COBRANZA
                    $('#cboTipor3').val('M');
                    $("#cboTipor3").each(function () {
                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor3").prop("disabled", true)
                }
                  if ($("#cboOperacionr3").val() == "C") { //CANJE
                    $('#cboTipor3').val('C');
                    $("#cboTipor3").each(function () {
                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor3").prop("disabled", true)
                }
                if ($("#cboOperacionr3").val() == "D") { //CANJE
                    $('#cboTipor3').val('C');
                    $("#cboTipor3").each(function () {
                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor3").prop("disabled", true)
                }
                });


            $("#cboOperacionr6").change(function () {

                if ($("#cboOperacionr6").val() == "P") { //PEDIDO
                    $('#cboTipor6').val('C');
                    $("#cboTipor6").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor6").prop("disabled", false)
                }
                if ($("#cboOperacionr6").val() == "C") { //CANJE
                    $('#cboTipor6').val('C');
                    $("#cboTipor6").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor6").prop("disabled", true)
                }
                if ($("#cboOperacionr6").val() == "D") { //DEVOLUCION
                    $('#cboTipor6').val('C');
                    $("#cboTipor6").each(function () {

                        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                    });
                    $("#cboTipor6").prop("disabled", true)
                }
            });

            $("#cboTipoReporte").change(function () {

                if ($("#cboTipoReporte").val() == "1") { //INDICADOR PRODUCTO
                    $("#r1").show();
                    $("#r2").hide();
                    $("#r3").hide();
                    $("#r4").hide();
                    $("#r5").hide();
                    $("#r6").hide();
                }
                if ($("#cboTipoReporte").val() == "2") { //TOP CLIENTES
                    $("#r1").hide();
                    $("#r2").show();
                    $("#r3").hide();
                    $("#r4").hide();
                    $("#r5").hide();
                    $("#r6").hide();
                }
                if ($("#cboTipoReporte").val() == "3") { //TOP VENDEDORES
                    $("#r1").hide();
                    $("#r2").hide();
                    $("#r3").show();
                    $("#r4").hide();
                    $("#r5").hide();
                    $("#r6").hide();
                }
                if ($("#cboTipoReporte").val() == "4") { //AVANCE
                    $("#r1").hide();
                    $("#r2").hide();
                    $("#r3").hide();
                    $("#r4").show();
                    $("#r5").hide();
                    $("#r6").hide();
                }
                if ($("#cboTipoReporte").val() == "5") { //INDICADOR
                    $("#r1").hide();
                    $("#r2").hide();
                    $("#r3").hide();
                    $("#r4").hide();
                    $("#r5").show();
                    $("#r6").hide();
                }
                if ($("#cboTipoReporte").val() == "6") { //TOP PRODUCTOS
                    $("#r1").hide();
                    $("#r2").hide();
                    $("#r3").hide();
                    $("#r4").hide();
                    $("#r5").hide();
                    $("#r6").show();
                }
            });

            $('#btnChar1').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoIndicadorProducto',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataIndicadorProducto()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);

                        $('#LFr1').html(jObject.LabelFoot);
//                        $('#LTr1').html(jObject.Titulo);
                
                         var dataChar1= new google.visualization.DataTable();
                       dataChar1.addColumn('string', 'Label');
                       dataChar1.addColumn('number', 'Value');
                       dataChar1.addRows(1);
                       dataChar1.setCell(0, 0, jObject.Titulo);
                       dataChar1.setCell(0, 1, jObject.porc);

              
                        var options = {
                           width: 500, height: 300,
                           redFrom: 0, redTo: jObject.li,
                           yellowFrom: jObject.li, yellowTo: jObject.ls,
                           greenFrom: jObject.ls, greenTo: 100,
                           minorTicks: 5
                       };
 
                        var chart = new google.visualization.Gauge(document.getElementById('Char1'));
                        chart.draw(dataChar1, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });
//            top clientes

            $('#btnChar2').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoTopClientes',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataTopClientes()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                        $('#LTr2').html(jObject.Titulo);
                        var dataChar2 = new google.visualization.DataTable();
                        dataChar2.addColumn('string', 'Clientes');
                        dataChar2.addColumn('number', 'Total');
                       dataChar2.addColumn({ type: 'string', role: 'style' });
                        dataChar2.addColumn({ type: 'string', role: 'annotation' });
                        
                        $.each(jObject.Valor, function (i, item) {
                            dataChar2.addRows(1);
                            dataChar2.setCell(i, 0, item.cliente);
                            dataChar2.setCell(i, 1, item.total);
                            dataChar2.setCell(i, 2, item.color);
                            dataChar2.setCell(i, 3, item.total);

                        });

                         var options = {
                           width: 500, height: 300,
                          is3D: true,
                          legend: 'none',
                        };
                       
 
                        var chart = new google.visualization.PieChart(document.getElementById('Char2'));
                        chart.draw(dataChar2, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });

        // Top Productos
          $('#btnChar6').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoTopProductos',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataTopProductos()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                         $('#LTr6').html(jObject.Titulo);
                        var dataChar6 = new google.visualization.DataTable();
                        dataChar6.addColumn('string', 'Productos');
                        dataChar6.addColumn('number', 'Total');
                       dataChar6.addColumn({ type: 'string', role: 'style' });
                        dataChar6.addColumn({ type: 'string', role: 'annotation' });
                        
                        $.each(jObject.Valor, function (i, item) {
                            dataChar6.addRows(1);
                            dataChar6.setCell(i, 0, item.producto);
                            dataChar6.setCell(i, 1, item.total);
                            dataChar6.setCell(i, 2, item.color);
                            dataChar6.setCell(i, 3, item.total);

                        });



                         var options = {
                           width: 500, height: 300,
                          is3D: true,
                          legend: 'none',
                        };
                       
 
                        var chart = new google.visualization.PieChart(document.getElementById('Char6'));
                        chart.draw(dataChar6, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });
            //AVANCE
             $('#btnChar4').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoAvance',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataAvance()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                         $('#LTr4').html(jObject.Titulo);
                        var dataChar4 = new google.visualization.DataTable();
                        dataChar4.addColumn('string', 'Fechas');
                        dataChar4.addColumn('number', 'Total');
                       dataChar4.addColumn({ type: 'string', role: 'style' });
                        dataChar4.addColumn({ type: 'string', role: 'annotation' });
                        
                        $.each(jObject.Valor, function (i, item) {
                            dataChar4.addRows(1);
                            dataChar4.setCell(i, 0, item.fecha);
                       dataChar4.setCell(i, 1, item.total);
                            dataChar4.setCell(i, 2, item.color);
                            dataChar4.setCell(i, 3, item.total);

                        });



                         var options = {
                          legend: 'none'
                        };
                       
 
                        var chart = new google.visualization.ColumnChart(document.getElementById('Char4'));
                        chart.draw(dataChar4, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });
          
            //TOP VENDEDORES
   $('#btnChar3').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoTopVendedores',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataTopVendedores()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                          $('#LTr3').html(jObject.Titulo);
                        var dataChar3 = new google.visualization.DataTable();
                        dataChar3.addColumn('string', 'Vendedor');
                        dataChar3.addColumn('number', 'Total');
                       dataChar3.addColumn({ type: 'string', role: 'style' });
                        dataChar3.addColumn({ type: 'string', role: 'annotation' });
                        
                        $.each(jObject.Valor, function (i, item) {
                            dataChar3.addRows(1);
                            dataChar3.setCell(i, 0, item.vendedor);
                            dataChar3.setCell(i, 1, item.total);
                            dataChar3.setCell(i, 2, item.color);
                            dataChar3.setCell(i, 3, item.total);
                        });

                       console.log(dataChar3);
                         var options = {
                          hAxis: {title: jObject.x, titleTextStyle: {color: 'black'}},
                            vAxis: {title: jObject.y, titleTextStyle: {color: 'black'}},
                          is3D: true,
                          legend: 'none',
                        };
                       
 
                        var chart = new google.visualization.ColumnChart(document.getElementById('Char3'));
                        chart.draw(dataChar3, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });

             //AVANCE
   $('#btnChar4').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoAvance',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataAvance()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                        
                        var dataChar4 = new google.visualization.DataTable();
                        dataChar4.addColumn('string', 'Fecha');
                        dataChar4.addColumn('number', 'Total');
                       dataChar4.addColumn({ type: 'string', role: 'style' });
                        dataChar4.addColumn({ type: 'string', role: 'annotation' });
                        
                        $.each(jObject.Valor, function (i, item) {
                            dataChar4.addRows(1);
                            dataChar4.setCell(i, 0, item.fecha);
                            dataChar4.setCell(i, 1, item.monto);
                            dataChar4.setCell(i, 2, item.color);
                            dataChar4.setCell(i, 3, item.monto);

                        });



                         var options = {
                          hAxis: {title: jObject.x, titleTextStyle: {color: 'black'}},
                            vAxis: {title: jObject.y, titleTextStyle: {color: 'black'}},
                          is3D: true,
                          legend: 'none',
                        };
                       
 
                        var chart = new google.visualization.LineChart(document.getElementById('Char4'));
                        chart.draw(dataChar4, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });
              //INDICADOR
             $('#btnChar5').live('click', function (e) {
                e.preventDefault();


                $.ajax({
                    type: 'POST',
                    url: 'graficos.aspx/getGraficoIndicador',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(getDataIndicador()),
                    success: function (data) {

                        jObject = jQuery.parseJSON(data.d);
                        $('#LFr5').html(jObject.LabelFoot);
                         var dataChar5= new google.visualization.DataTable();
                       dataChar5.addColumn('string', 'Label');
                       dataChar5.addColumn('number', 'Value');
                       dataChar5.addRows(1);
                       dataChar5.setCell(0, 0, jObject.Titulo);
                       dataChar5.setCell(0, 1, jObject.porc);

//                       console.log(dataChar5);
//                        var options = {
//                           width: 400, height: 180,
//                           redFrom: 0, redTo: 20,
//                           yellowFrom: 20, yellowTo: 80,
//                           greenFrom: 80, greenTo: 100,
//                           minorTicks: 5
//                       };
                            var options = {
                           width: 500, height: 300,
                           redFrom: 0, redTo: jObject.li,
                           yellowFrom: jObject.li, yellowTo: jObject.ls,
                           greenFrom: jObject.ls, greenTo: 100,
                           minorTicks: 5
                       };
 
                        var chart = new google.visualization.Gauge(document.getElementById('Char5'));
                        chart.draw(dataChar5, options);

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alertChart");

                    }
                });


            });
        });

        function getDataTopClientes() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.operacion = $('#cboOperacionr2').val();
            strData.tipo = $('#cboTipor2').val();

            return strData;
        }
         function getDataAvance() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.operacion = $('#cboOperacionr4').val();
            

            return strData;
        }
         function getDataTopProductos() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.operacion = $('#cboOperacionr6').val();
            strData.tipo = $('#cboTipor6').val();

            return strData;
        }
      
        function getDataTopVendedores() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.operacion = $('#cboOperacionr3').val();
            strData.tipo = $('#cboTipor3').val();

            return strData;
        }
         function getDataIndicador() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.li=$('#txtLimiteInferiorr5').val();
            strData.ls=$('#txtLimiteSuperiorr5').val();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.meta = $('#txtMetar5').val();
            strData.operacion = $('#cboOperacionr5').val();

            return strData;
        }
         function getDataIndicadorProducto() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.li=$('#txtLimiteInferiorr1').val();
            strData.ls=$('#txtLimiteSuperiorr1').val();
            strData.desde = $('#txtFechaI').val();
            strData.hasta = $('#txtFechaF').val(); // + ' ' + $('#txtHoraFin').val();
            strData.meta = $('#txtMetar1').val();
            strData.tipo = $('#cboTipor1').val();
            strData.codprod = $('#cboProductor1').val();

            return strData;
        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon_report.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NAV_GRAFICOS)%></p>
                    </div>
                </div>
                <%-- <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
                <input type="button" id="cz-form-box-exportar" class="cz-form-content-input-button cz-form-content-input-button-image form-button xlsReg cz-util-right"
                    value="Exportar" />--%>
            </div>
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO)%></p>
                    <asp:DropDownList ID="cboTipoReporte" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>

                    <div class="cz-form-content">
                            <p>
                                <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_DESDE)%></p>
                            <input name="txtFechaI" type="text" value="30/05/2014" maxlength="10" id="txtFechaI"
                                runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                                onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                            <div class="cz-form-content-input-calendar-visible">
                                <div class="cz-form-content-input-calendar-visible-button">
                                    <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                        src="../../images/icons/calendar.png" />
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HASTA)%></p>
                            <input name="txtFechaF" type="text" value="30/05/2014" maxlength="10" id="txtFechaF"
                                runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                                onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                            <div class="cz-form-content-input-calendar-visible">
                                <div class="cz-form-content-input-calendar-visible-button">
                                    <img alt="<>" id="Img2" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                        src="../../images/icons/calendar.png" />
                                </div>
                            </div>
                        </div>

                <div style="display: none;" class="cz-form-content cz-util-right cz-util-right-text">
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                </div>
            </div>
            <%--INDICADOR PRODUCTO--%>
            <div id="r1" style="display: block; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="titleGauge">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_INDICADORPRODUCTO) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                    
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_LIMITE_INFERIOR) %></p>
                            <asp:TextBox ID="txtLimiteInferiorr1" MaxLength="2" runat="server" class="cz-form-content-input-text" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                               
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_LIMITE_SUPERIOR) %></p>
                            <asp:TextBox ID="txtLimiteSuperiorr1" MaxLength="3" runat="server" class="cz-form-content-input-text" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%--meta--%>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_META) %></p>
                            <asp:TextBox ID="txtMetar1" runat="server" MaxLength="10" class="cz-form-content-input-text" Style="width: 70px;" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%--Tipo--%>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO) %></p>
                            <asp:DropDownList ID="cboTipor1" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <%--Producto--%>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PRODUCTO) %></p>
                            <asp:DropDownList ID="cboProductor1" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar1" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr1"></span>
                        </div>
                        <div id="Char1" style="padding-left: 50%; margin-left: -150px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr1"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--TOP CLIENTES--%>
            <div id="r2" style="display: none; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="P1">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_TOP_CLIENTES) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                    
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERACION) %></p>
                            <asp:DropDownList ID="cboOperacionr2" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO) %></p>
                            <asp:DropDownList ID="cboTipor2" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar2" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr2"></span>
                        </div>
                        <div id="Char2" style="padding-left: 50%; margin-left: -250px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr2"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--TOP VENDEDORES--%>
            <div id="r3" style="display: none; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="P2">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_TOP_VENDEDORES) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                  
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERACION) %></p>
                            <asp:DropDownList ID="cboOperacionr3" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO) %></p>
                            <asp:DropDownList ID="cboTipor3" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar3" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr3"></span>
                        </div>
                        <div id="Char3" style="padding-left: 50%; margin-left: -547px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr3"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--AVANCE--%>
            <div id="r4" style="display: none; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="P3">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_AVANCE) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                   
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERACION) %></p>
                            <asp:DropDownList ID="cboOperacionr4" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar4" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr4"></span>
                        </div>
                        <div id="Char4" style="padding-left: 50%; margin-left: -558px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr4"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--INDICADOR--%>
            <div id="r5" style="display: none; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="P4">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_INDICADOR) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                     
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_LIMITE_INFERIOR) %></p>
                            <asp:TextBox ID="txtLimiteInferiorr5" runat="server" MaxLength="2" class="cz-form-content-input-text" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                                %
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_LIMITE_SUPERIOR) %></p>
                            <asp:TextBox ID="txtLimiteSuperiorr5" runat="server" MaxLength="3" class="cz-form-content-input-text" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%--meta--%>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_META) %></p>
                            <asp:TextBox ID="txtMetar5" runat="server" MaxLength="10" class="cz-form-content-input-text" Style="width: 70px;" onkeypress="fc_PermiteNumeros()"></asp:TextBox>
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%--Operacion--%>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERACION) %></p>
                            <asp:DropDownList ID="cboOperacionr5" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar5" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr5"></span>
                        </div>
                        <div id="Char5" style="padding-left: 50%; margin-left: -150px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr5"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--TOP PRODUCTOS--%>
            <div id="r6" style="display: none; width: 100%; padding-left: 0px; background: white;"
                class="cz-form-content cz-content-extended">
                <div class="cz-form-subcontent">
                    <div class="cz-form-subcontent-title">
                        <p id="P5">
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_TOP_PRODUCTOS) %></p>
                    </div>
                    <div class="cz-form-subcontent-content">
                     
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERACION) %></p>
                            <asp:DropDownList ID="cboOperacionr6" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO) %></p>
                            <asp:DropDownList ID="cboTipor6" runat="server" CssClass="cz-form-content-input-select">
                            </asp:DropDownList>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <input type="button" id="btnChar6" class="cz-form-content-input-button" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OBTENER)%>">
                        </div>
                    </div>
                    <div class="cz-form-subcontent-content">
                        <div style="text-align: center">
                            <span id="LTr6"></span>
                        </div>
                        <div id="Char6" style="padding-left: 50%; margin-left: -250px;">
                        </div>
                        <div style="text-align: center">
                            <span id="LFr6"></span>
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="cz-form-box-content">
                <div class="cz-other-box-center">
                    <div class="cz-other-box-center-content">
                        <%--aqui iban--%>
        </div>
        <!--Hidden Fields to control pagination-->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
    </div>
    </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>
