﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller.functions;
using System.Data;
using Model.bean;
using System.Text;
using Controller;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Reporte.graficos
{
    public partial class graficos : PageController
    {
        public static String lsCodMenu = "RGR";
        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            txtFechaI.Value = Utils.getFechaActual();
            txtFechaF.Value = Utils.getFechaActual();


            cboProductor1.DataSource = ReporteController.getProductos();
            cboProductor1.DataValueField = "PRO_PK";
            cboProductor1.DataTextField = "PRO_NOMBRE";
            cboProductor1.DataBind();

            List<BECbx> loLista = new List<BECbx>();
            loLista.Add(new BECbx("C", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_OPERACIONCANTIDAD)));
            loLista.Add(new BECbx("M", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_OPERACIONMONTO)));
            cboTipor1.DataTextField = "Texto";
            cboTipor1.DataValueField = "Valor";
            cboTipor1.DataSource = loLista;
            cboTipor1.DataBind();
            cboTipor2.DataTextField = "Texto";
            cboTipor2.DataValueField = "Valor";
            cboTipor2.DataSource = loLista;
            cboTipor2.DataBind();
            cboTipor3.DataTextField = "Texto";
            cboTipor3.DataValueField = "Valor";
            cboTipor3.DataSource = loLista;
            cboTipor3.DataBind();

            cboTipor6.DataTextField = "Texto";
            cboTipor6.DataValueField = "Valor";
            cboTipor6.DataSource = loLista;
            cboTipor6.DataBind();

            List<BECbx> tiporeporte = new List<BECbx>();
            tiporeporte.Add(new BECbx("1", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_INDICADORPRODUCTO)));
            tiporeporte.Add(new BECbx("2", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_TOP_CLIENTES)));
            tiporeporte.Add(new BECbx("3", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_TOP_VENDEDORES)));
            tiporeporte.Add(new BECbx("4", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_AVANCE)));
            tiporeporte.Add(new BECbx("5", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_INDICADOR)));
            tiporeporte.Add(new BECbx("6", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_TOP_PRODUCTOS)));
            cboTipoReporte.DataTextField = "Texto";
            cboTipoReporte.DataValueField = "Valor";
            cboTipoReporte.DataSource = tiporeporte;
            cboTipoReporte.DataBind();

            List<BECbx> loOperacion = new List<BECbx>();
            loOperacion.Add(new BECbx("P", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_PEDIDO)));
            loOperacion.Add(new BECbx("CO", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_COBRANZA)));
            loOperacion.Add(new BECbx("C", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_CANJE)));
            loOperacion.Add(new BECbx("D", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_DEVOLUCION)));
            cboOperacionr2.DataTextField = "Texto";
            cboOperacionr2.DataValueField = "Valor";
            cboOperacionr2.DataSource = loOperacion;
            cboOperacionr2.DataBind();

            cboOperacionr3.DataTextField = "Texto";
            cboOperacionr3.DataValueField = "Valor";
            cboOperacionr3.DataSource = loOperacion;
            cboOperacionr3.DataBind();

            List<BECbx> loOperacionR45 = new List<BECbx>();
            loOperacionR45.Add(new BECbx("P", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_PEDIDO)));
            loOperacionR45.Add(new BECbx("CO", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_COBRANZA)));

            cboOperacionr4.DataTextField = "Texto";
            cboOperacionr4.DataValueField = "Valor";
            cboOperacionr4.DataSource = loOperacionR45;
            cboOperacionr4.DataBind();

            cboOperacionr5.DataTextField = "Texto";
            cboOperacionr5.DataValueField = "Valor";
            cboOperacionr5.DataSource = loOperacionR45;
            cboOperacionr5.DataBind();

            List<BECbx> loOperacionr6 = new List<BECbx>();
            loOperacionr6.Add(new BECbx("P", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_PEDIDO)));
            loOperacionr6.Add(new BECbx("C", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_CANJE)));
            loOperacionr6.Add(new BECbx("D", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_NAV_DEVOLUCION)));

            cboOperacionr6.DataTextField = "Texto";
            cboOperacionr6.DataValueField = "Valor";
            cboOperacionr6.DataSource = loOperacionr6;
            cboOperacionr6.DataBind();

            DataTable odtL = ReporteController.getLimiteGraficos();

            foreach (DataRow xrow in odtL.Rows)
            {
                if (xrow["CodIndicador"].ToString() == "1") //CIRCULAR
                {
                    txtLimiteInferiorr5.Text = xrow["LIMITE_INFERIOR"].ToString();
                    txtLimiteSuperiorr5.Text = xrow["LIMITE_SUPERIOR"].ToString();
                    txtMetar5.Text = xrow["META"].ToString();
                }
                if (xrow["CodIndicador"].ToString() == "3") //TERMOMETRO
                {
                    txtLimiteInferiorr1.Text = xrow["LIMITE_INFERIOR"].ToString();
                    txtLimiteSuperiorr1.Text = xrow["LIMITE_SUPERIOR"].ToString();
                    txtMetar1.Text = xrow["META"].ToString();
                }
            }
        }



        [WebMethod(EnableSession = true)]
        public static String getGraficoTopClientes(String desde, String hasta, String operacion, String tipo)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoTopClientesBean> lista;

            lista = ReporteController.GraficoTopClientes(fini, ffin, operacion, tipo);
            StringBuilder sb = new StringBuilder();

            Int32 VAL = 0;


            String lTIT = "";
            if (operacion == "P" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD);
            }
            if (operacion == "P" && tipo == "M")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_MONTO);
            }
            if (operacion == "CO" && tipo == "M")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_COBRANZA_VS_MONTO);
            }
            if (operacion == "C" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_CANJE_VS_CANTIDAD);
            }
            if (operacion == "D" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD);
            }


            String lFoot = "";

            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");
            sb.Append("\"Valor\":[");
            int i = 0;
            foreach (GraficoTopClientesBean bean in lista)
            {
                if (i > 0)
                {
                    sb.Append(",");

                }
                sb.Append("{\"cliente\":\"" + bean.cliente + "\",\"total\":\"" + bean.total + "\",\"color\":\"" + coloress(i) + "\"}");
                i++;
            }

            sb.Append("]}");
            return sb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static String getGraficoTopProductos(String desde, String hasta, String operacion, String tipo)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoTopProductosBean> lista;

            lista = ReporteController.GraficoTopProductos(fini, ffin, operacion, tipo);
            StringBuilder sb = new StringBuilder();

            String lFoot = "";
            String lTIT = "";
            if (operacion == "P" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_CANTIDAD);
            }
            if (operacion == "P" && tipo == "M")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_PEDIDO_VS_MONTO);
            }
            if (operacion == "CO" && tipo == "M")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_COBRANZA_VS_MONTO);
            }
            if (operacion == "C" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_CANJE_VS_CANTIDAD);
            }
            if (operacion == "D" && tipo == "C")
            { //PEDIDO VS CANTIDAD
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRCLIENTE_DEVOLUCION_VS_CANTIDAD);
            }

            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");
            sb.Append("\"Valor\":[");
            int i = 0;
            foreach (GraficoTopProductosBean bean in lista)
            {
                if (i > 0)
                {
                    sb.Append(",");

                }
                sb.Append("{\"producto\":\"" + bean.producto + "\",\"total\":\"" + bean.total + "\",\"color\":\"" + coloress(i) + "\"}");
                i++;
            }

            sb.Append("]}");
            return sb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static String getGraficoTopVendedores(String desde, String hasta, String operacion, String tipo)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoTopVendedoresBean> lista;

            lista = ReporteController.GraficoTopVendedores(fini, ffin, operacion, tipo);
            StringBuilder sb = new StringBuilder();

            String lFoot = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENDEDOR);// LabelFoot + " = " + ValReal + " - " + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PROGRAMADAS) + "= " + bean.programado;
            String lFoot2 = "";
            String lTIT = "";
            if (tipo == "C")
            {
                lFoot2 = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_CANTIDAD);
            }
            if (tipo == "M")
            {
                lFoot2 = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO);
            }

            if (operacion == "P" && tipo == "C")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_CANTIDAD);
            }
            if (operacion == "P" && tipo == "M")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_PEDIDO_VS_MONTO);
            }
            if (operacion == "CO")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_COBRANZA_VS_MONTO);
            }
            if (operacion == "C")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_CANJE_VS_CANTIDAD);
            }
            if (operacion == "D")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_GRVENDEDOR_DEVOLUCION_VS_CANTIDAD);
            }

            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");
            sb.Append("\"x\":\"" + lFoot + "\",");
            sb.Append("\"y\":\"" + lFoot2 + "\",");
            sb.Append("\"Valor\":[");
            int i = 0;
            foreach (GraficoTopVendedoresBean bean in lista)
            {
                if (i > 0)
                {
                    sb.Append(",");

                }
                sb.Append("{\"vendedor\":\"" + bean.vendedor + "\",\"total\":\"" + bean.total + "\",\"color\":\"" + coloress(i) + "\"}");
                i++;
            }

            sb.Append("]}");
            return sb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static String getGraficoIndicador(String li, String ls, String desde, String hasta, String meta, String operacion)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoIndicadorBean> lista;

            lista = ReporteController.GraficoIndicador(li, ls, "1", fini, ffin, meta, "0", operacion);
            StringBuilder sb = new StringBuilder();



            GraficoIndicadorBean bean = lista[0];
            String lTIT = "";
            String lFoot = "";
            if (operacion == "P")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENTAS_TOTALES);

            }
            else { lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_COBRANZAS_TOTALES); }


            lFoot = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO) + ' ' + bean.total.ToString();
            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");

            sb.Append("\"li\":\"" + li + "\",");
            sb.Append("\"ls\":\"" + ls + "\",");

            sb.Append("\"Valor\":\"" + bean.total.ToString() + "\",");
            if (meta == "0")
            {
                sb.Append("\"porc\":\"" + "0" + "\"");
            }
            else
            {
                sb.Append("\"porc\":\"" + ((Double.Parse(bean.total) * 100) / Double.Parse(meta)).ToString() + "\"");
            }


            sb.Append("}");
            return sb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static String getGraficoIndicadorProducto(String li, String ls, String desde, String hasta, String meta, String tipo, String codprod)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoIndicadorProductoBean> lista;

            lista = ReporteController.GraficoIndicadorProducto(li, ls, "3", fini, ffin, meta, codprod, tipo);
            StringBuilder sb = new StringBuilder();

            GraficoIndicadorProductoBean bean = lista[0];
            String lTIT = "";
            String lFoot = "";
            if (tipo == "M")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENTAS_TOTALES_PRODUCTO);
                lFoot = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO) + " " + bean.total.ToString();
            }
            if (tipo == "C")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_UNIDADES_VENDIDAS);
                lFoot = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_UNIDADES) + " " + bean.total.ToString();
            }

            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");
            sb.Append("\"li\":\"" + li + "\",");
            sb.Append("\"ls\":\"" + ls + "\",");
            sb.Append("\"y\":\"porcentaje\",");

            sb.Append("\"Valor\":\"" + bean.total.ToString() + "\",");
            if (meta == "0")
            {
                sb.Append("\"porc\":\"" + "0" + "\"");
            }
            else
            {
                sb.Append("\"porc\":\"" + ((Double.Parse(bean.total) * 100) / Double.Parse(meta)).ToString() + "\"");
            }

            sb.Append("}");
            return sb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static String getGraficoAvance(String desde, String hasta, String operacion)
        {
            String fini = Utils.getStringFechaYYMMDD(desde) + " 00:00:00";
            String ffin = Utils.getStringFechaYYMMDD(hasta) + " 23:59:59";
            List<GraficoAvanceBean> lista;

            lista = ReporteController.GraficoAvance(fini, ffin, operacion);
            StringBuilder sb = new StringBuilder();

            String lFoot = "";

            String lTIT = "";
            String xx = "";
            String xy = "";
            if (operacion == "P")
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_VENTAS_TOTALES);
                xx = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MESES);
            }
            else
            {
                lTIT = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_COBRANZAS_TOTALES);
                xx = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_DIAS);
            }

            xy = IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_MONTO);

            sb.Append("{");
            sb.Append("\"Titulo\":\"" + lTIT + "\",");
            sb.Append("\"LabelFoot\":\"" + lFoot + "\",");
            sb.Append("\"x\":\"" + xx + "\",");
            sb.Append("\"y\":\"" + xy + "\",");
            sb.Append("\"Valor\":[");
            int i = 0;
            foreach (GraficoAvanceBean bean in lista)
            {
                if (i > 0)
                {
                    sb.Append(",");
                }
                sb.Append("{\"fecha\":\"" + bean.fecha + "\",\"monto\":\"" + bean.monto + "\",\"color\":\"" + coloress(i) + "\"}");

                i++;
            }

            sb.Append("]}");
            return sb.ToString();
        }

        private static String coloress(Int32 lis)
        {
            List<String> Lista = new List<String>();
            String color = "";

            Lista.Add("#AFD8F8");
            Lista.Add("#F6BD0F");
            Lista.Add("#8BBA00");
            Lista.Add("#FF8E46");
            Lista.Add("#008E8E");
            Lista.Add("#D64646");
            Lista.Add("#8E468E");
            Lista.Add("#588526");
            Lista.Add("#B3AA00");
            Lista.Add("#008ED6");

            int i = Convert.ToInt16(lis) + 1;
            color = Lista[i % 10];

            return color;
        }
    }
}