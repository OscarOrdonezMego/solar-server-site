﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller.functions;
using Model.bean;
using Controller;
using System.Data;
using Model;

namespace Pedidos_Site.Reporte.quiebrestock
{
    public partial class Reporte_quiebrestock_grid : PageController
    {
        public string Mbm = "";
        public Boolean bonificacion = false;
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
            String fcobertura = dataJSON["FlagEnCobertura"].ToString();
            String fcodigoproducto = dataJSON["codigoproducto"].ToString();
            String fBonificacion = dataJSON["Bonificacion"].ToString();

            String vendedor = dataJSON["vendedor"].ToString();
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }


            Mbm = ManagerConfiguration.nBonificacionManual;

            PaginateReporteQuiebreStockBean paginate = ReporteController.paginarPedidoBuscarQuiebre(vendedor, fini, ffin, fcobertura, fcodigoproducto, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReporteQuiebreStockBean> lst = paginate.lstResultados;
                DataTable odtL = ReporteModel.findReporteQuiebreStockBean(vendedor, fini, ffin, fcobertura, fcodigoproducto, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);
                foreach (DataRow xrow in odtL.Rows)
                {

                    String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
                    if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
                    {
                        bonificacion = true;
                    }
                    else
                    {
                        bonificacion = false;
                    }
                }
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                const string htmlNoData = "<div class='gridNoData'>" +
                                          "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                          "<p>No se encontraron datos para mostrar</p>" +
                                          "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }
    }
}