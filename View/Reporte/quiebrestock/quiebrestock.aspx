﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.quiebrestock.Reporte_quiebrestock_quiebrestock" Codebehind="quiebrestock.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
      <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <script>


        var urlbus = 'grid.aspx';
        $(document).ready(function () {
            BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE();
        });
        $(document).ready(function () {
            clickCBOSupervisor();
            BUSCARGRUPOSCUANDOHACEMOSCHANGE();
            busReg();
            $('#buscar').trigger("click");
            exportarReg(".xlsReg", "../exportar.aspx", '.grilla'); //funcion encarga de la exportacion
            

        });
        function BUSCARGRUPOSCUANDOHACEMOSCHANGE() {
            $('#cboPerfil').change(function () {

                var ulsdata = "quiebrestock.aspx/MostarGrupos";
                var objt = new Object();
                objt.codigo = $(this).val();
                $.ajax({
                    url: ulsdata,
                    data: JSON.stringify(objt),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        $('#cboGrupro option').remove();
                        $('#cboGrupro').append(msg.d);
                        $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });


            });

        }


        function BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE() {
            $('#cboGrupro').change(function () {
                var ulsdatae = "quiebrestock.aspx/MostarVendedoresPorGrupos";
                var objtd = new Object();
                objtd.codigo = $(this).val();
                $.ajax({
                    url: ulsdatae,
                    data: JSON.stringify(objtd),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        $('#cboVendedor option').remove();
                        $('#cboVendedor').append(msg.d);
                        $('#cboVendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cboVendedor').find("option:selected").text());
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });


            });

        }

        function clickCBOSupervisor() {
            var codusu = '<%=codigoSub%>';
            var usupk = '<%=pkusu%>';
            if (codusu == 'SUP') {
                Supervisor(usupk);
            }
        }

        function Supervisor(codigo) {
            var ulsdata = "quiebrestock.aspx/MostarGrupos";
            var objt = new Object();
            objt.codigo = codigo;
            $.ajax({
                url: ulsdata,
                data: JSON.stringify(objt),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $('#cboGrupro option').remove();
                    $('#cboGrupro').append(msg.d);
                    $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.fini = $('#txtFechaIni').val();
            strData.ffin = $('#txtFechaFin').val(); // + ' ' + $('#txtHoraFin').val();
            strData.vendedor = $('#cboVendedor').val();
            strData.FlagEnCobertura = $('#cbocobertura').val();
            strData.codigoproducto = $('#txtNombre').val();
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.Bonificacion = $('#cbobonificacion').val();
            if ($('#cbobonificacion').val() == null) {
                strData.Bonificacion = "-1";

            }
            strData.codperfil = $('#cboPerfil').val();
            strData.codgrupo = $('#cboGrupro').val();


            return strData;
        }
     
      
        function getParametrosXLS() {
            var fini = $('#txtFechaIni').val(); //  + ' ' + $('#txtHoraIni').val();
            var ffin = $('#txtFechaFin').val(); //  + ' ' + $('#txtHoraFin').val();
            var fven = $('#cboVendedor').val();
            var fcobertura = $('#cbocobertura').val();
            var fcodigoproducto = $('#txtNombre').val();
            var fbonificacion = $('#cbobonificacion').val();
            if ($('#cbobonificacion').val() == null) {
               fbonificacion = "-1";

            }
            var codperfil = $('#cboPerfil').val();
            var codgrupo = $('#cboGrupro').val();
            var mod = "REP_QUIEBRESTOCK";
            //return 'mod=' + mod + '&fini=' + fini + '&ffin=' + ffin + '&fven=' + fven + '&fcobertura=' + fcobertura + '&fcodigoproducto=' + fcodigoproducto;
            return 'mod=' + mod + '&fini=' + fini + '&ffin=' + ffin + '&fven=' + fven + '&fcobertura=' + fcobertura + '&fcodigoproducto=' + fcodigoproducto + '&fbonificacion=' + fbonificacion + '&codperfil=' + codperfil + '&codgrupo=' + codgrupo;
        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon_report.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_REPORTEQUIEBRESTOCK)%></p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EXPORTAR))
                      { %>
                <input type="button" id="cz-form-box-exportar" class="cz-form-content-input-button cz-form-content-input-button-image form-button xlsReg cz-util-right"
                    value="Exportar" />
                    <%} %>
            </div>
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAINICIO)%></p>
                    <input name="txtFechaIni" type="text" value="30/05/2014" maxlength="10" id="txtFechaIni"
                        runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                        onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                    <div class="cz-form-content-input-calendar-visible">
                        <div class="cz-form-content-input-calendar-visible-button">
                            <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                src="../../images/icons/calendar.png" />
                        </div>
                    </div>
                </div>
               
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAFIN)%></p>
                    <input name="txtFechaFin" type="text" value="30/05/2014" maxlength="10" id="txtFechaFin"
                        runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                        onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                    <div class="cz-form-content-input-calendar-visible">
                        <div class="cz-form-content-input-calendar-visible-button">
                            <img alt="<>" id="Img2" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                src="../../images/icons/calendar.png" />
                        </div>
                    </div>
                </div>
               
                
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CODIGOPRODUCTO)%></p>
                    <asp:TextBox ID="txtNombre" runat="server" class="cz-form-content-input-text" maxlength="20"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>

                 <% if (Mbm == "1")
                              { %>

                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%></p>

                     <asp:DropDownList ID="cbobonificacion" runat="server" CssClass=" requerid cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>

                <%} %>

                 <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_COBERTURA)%>
                        </p>
                    <asp:DropDownList ID="cbocobertura" runat="server" CssClass=" requerid cz-form-content-input-select">
                    </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
                </div>
                </div>
                <div class="cz-form-content" <%=deshabilitarCboSupervisor %>>
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SUPERVISOR)%></p>
                    <asp:DropDownList ID="cboPerfil" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRUPO)%></p>
                    <asp:DropDownList ID="cboGrupro" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>

                   <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VENDEDOR)%></p>
                    <asp:DropDownList ID="cboVendedor" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>


                <div class="cz-form-content cz-util-right cz-util-right-text">
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                </div>
            </div>


            
                


            <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div class="form-gridview-data" id="divGridViewData" runat="server">
                            </div>
                            <div class="form-gridview-error" id="divGridViewError" runat="server">
                            </div>
                            <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                <p>
                                    buscando resultados</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Hidden Fields to control pagination-->
            <div id="paginator-hidden-fields">
                <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_RESTAURARVARIOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ELIMINARVARIOS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ELIMINAR)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_RESTAURAR)%>" />
            </div>
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
            </div>
        </div>
    </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>