﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;

public partial class Reporte_ruta_icono : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Color[] arrayColores = new Color[] { Color.Yellow,Color.Green,Color.Blue   , Color.Gray  ,Color.Violet, Color.Bisque , 
                                             Color.YellowGreen,Color.Tomato,Color.Aqua,Color.SkyBlue,Color.Orange,Color.Olive  ,
                                             Color.Brown ,Color.LawnGreen,Color.Fuchsia,Color.Khaki  ,Color.LightCoral  ,Color.Gold  ,
                                             Color.Navy  ,Color.PaleTurquoise,Color.Thistle};
        try
        {
            int indice = Int32.Parse(Request.QueryString["indice"].ToString());
            string proceso = Request.QueryString["proceso"].ToString();
            changeColor(arrayColores[(indice % arrayColores.Length)], proceso);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static Bitmap changeColor(Color colores, String proceso)
    {

        string ruta = HttpContext.Current.Server.MapPath("~/Images/gps/" + proceso);

        byte[] fileImagen;
        fileImagen = System.IO.File.ReadAllBytes(ruta);
        System.IO.MemoryStream stream;
        stream = new System.IO.MemoryStream(fileImagen);
        Bitmap bmpOLD = new Bitmap(stream);

        //////////////captura color y cambia el color al que se asigno

        Bitmap bmap = (Bitmap)bmpOLD.Clone();
        Color c;
        for (int i = 0; i < bmap.Width; i++)
        {
            for (int j = 0; j < bmap.Height; j++)
            {
                c = bmap.GetPixel(i, j);
                int nPixelR = 0;
                int nPixelG = 0;
                int nPixelB = 0;



                if (c.A != 0 && c.R == 255 && c.G == 0 && c.B == 0) //captura color para realizar el cambio
                {
                    bmap.SetPixel(i, j, colores);
                }
                else
                {
                    nPixelR = c.R;
                    nPixelG = c.G;
                    nPixelB = c.B;

                    bmap.SetPixel(i, j, Color.FromArgb((byte)c.A, (byte)nPixelR, (byte)nPixelG, (byte)nPixelB));
                }


            }
        }
        bmpOLD = (Bitmap)bmap.Clone();

        HttpContext.Current.Response.ContentType = "image/png";

        System.IO.MemoryStream mems = new System.IO.MemoryStream();
        bmpOLD.Save(mems, ImageFormat.Png);
        HttpContext.Current.Response.OutputStream.Write(mems.GetBuffer(), 0, (int)mems.Length);

        return bmpOLD;
    }

}