﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.ruta.ruta" Codebehind="ruta.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="<%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es&libraries=drawing,places&client=<%=(ConfigurationManager.AppSettings["MAPS_API_CLIENT"])%>"></script>--%>
    
    <script src="../../js/alertify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../css/alertify.core.css" />
    <link rel="stylesheet" href="../../css/alertify.default.css" />

     <script type="text/javascript" src="<%
        string url = "&";
        string client = (ConfigurationManager.AppSettings["MAPS_API_CLIENT"]);
        if (!client.Equals("")){
            client = "client=" + client;
            url += client;
        }
        string key = (ConfigurationManager.AppSettings["MAPS_API_KEY"]);
        if (!key.Equals("")){
            key = "key=" + key;
            url += key;
        }
        %><%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es&libraries=drawing,places<%=url%>"></script>

    <title></title>
    <style type="text/css">
        .modalGeo {
            right: 0%;
            left: inherit;
            width: 300px;
        }

        #map img {
            max-width: none;
        }

        #map label {
            width: auto;
            display: inline;
        }
    </style>
    <script>
        function pulsar(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13) return false;
        }
    </script>
    <script>
        var markersArray = [];
        var urlLoad = 'ruta.aspx/cargarRuta';

        $(document).ready(function () {
            BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE();
        });
        $(document).ready(function () {
            clickCBOSupervisor();
            BUSCARGRUPOSCUANDOHACEMOSCHANGE();

            initialize('<%=latitudIni %>', '<%=longitudIni %>');

            $('#btnGenerar').click(function (e) {
                deleteMarkerRuta();
                cargaPointsRuta(urlLoad, true)

            });
            $('#btnCancelar').click(function (e) {
                deleteMarkerRuta();
            });
        });
        
        function BUSCARGRUPOSCUANDOHACEMOSCHANGE() {
            $('#cboPerfil').change(function () {

                var ulsdata = "ruta.aspx/MostrarGrupos";
                var objt = new Object();
                objt.codigo = $(this).val();
                $.ajax({
                    url: ulsdata,
                    data: JSON.stringify(objt),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        $('#cboGrupro option').remove();
                        $('#cboGrupro').append(msg.d);
                        $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                        $('#cboGrupro').trigger("change");
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });


            });

        }


        function BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE() {
            $('#cboGrupro').change(function () {
                var ulsdatae = "ruta.aspx/MostrarVendedoresGrupoSupervisor";
                var objtd = new Object();
                objtd.codigoGrupo = $(this).val();
                objtd.codigoSupervisor = $('#cboPerfil').val();
                console.log(objtd);
                $.ajax({
                    url: ulsdatae,
                    data: JSON.stringify(objtd),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        $('#cboVendedor option').remove();
                        $('#cboVendedor').append(msg.d);
                        $('#cboVendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cboVendedor').find("option:selected").text());
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });


            });

        }

        function clickCBOSupervisor() {
            var codusu = '<%=codigoSub%>';
            var usupk = '<%=pkusu%>';
            if (codusu == 'SUP') {
                Supervisor(usupk);
            }
        }

        function Supervisor(codigo) {
            var ulsdata = "ruta.aspx/MostarGrupos";
            var objt = new Object();
            objt.codigo = codigo;
            $.ajax({
                url: ulsdata,
                data: JSON.stringify(objt),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $('#cboGrupro option').remove();
                    $('#cboGrupro').append(msg.d);
                    $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }

        function cargaPointsRuta(urlM, polilyne) {
           // deleteMarker();

            $.ajax({
                type: 'POST',
                url: urlM,
                data: JSON.stringify(getData()),
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                dataType: "json",
                beforeSend: function () {
                    $(".form-gridview-search").show();
                    $("#leyenda").find('ul').html('');
                    $("#leyenda").hide();
                },
                success: function (data) {
                    
                    if (data.d == "[]") {
                        alertify.log("No existe Puntos de ventas en esta fecha.", 10000);
                    } else {
                        var items = 0;
                        var isAlone = true;
                        $.each(jQuery.parseJSON(data.d), function (index, objPoint) {
                            crearPoint(objPoint, jQuery.parseJSON(data.d));

                            if (objPoint.tipo != "") {
                                items++;

                                if (objPoint.tipo != '-1') {
                                    isAlone = false;
                                    $("#leyenda").find('ul').append('<li style="list-style: none;"><img style="padding-right: 10px;" src="icono.aspx?indice=' + objPoint.tipo + '&proceso=leyenda.png" border="0"/>' + objPoint.vendedor + '</li>');
                                }
                                else {

                                    $("#leyenda").find('ul').append('<li style="list-style: none;"><img style="padding-right: 10px;" src="../../images/gps/leyenda.png" border="0"/>' + objPoint.vendedor + '</li>');
                                }
                            }
                        });
                       
                    }
                    if (items > 1) {
                        console.log(isAlone);
                        if (!isAlone) {
                            $("#leyenda").show();
                        }
                    }


                    $('.form-gridview-search').hide();
                    
                },

                error: function (xhr, status, error) {

                }
            });


        }

        function getData() {

            var StrData = new Object;
            StrData.usuario = $('#cboVendedor').val();
            StrData.fecha = $('#txtFecha').val();
            StrData.satelite = $('#chkSatelite').attr("checked") ? 'S' : '';
            StrData.codperfil = $('#cboPerfil').val();
            StrData.codgrupo = $('#cboGrupro').val();
            return StrData;

        }

    </script>
</head>
<body onkeypress="return pulsar(event)">
    <form id="form1" runat="server">
        <input id="hAccion" type="hidden" />
        <input id="hNombre" type="hidden" />
        <input id="hCodigo" type="hidden" />
        <div id="map" class="cz-maps-full">
        </div>
        <div id="cz-util-hidden-right-ultpos-geo" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-left">
            <div class="cz-util-hidden-arrow">
            </div>
        </div>
        <div class="modalGeo hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="false" style="display: block;">
            <div id="myModalContent" class="modal-body">
                <div class="cz-form-box-content">

                    <div class="cz-form-content" <%=deshabilitarCboSupervisor %>>
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SUPERVISOR)%>
                        </p>
                        <asp:DropDownList ID="cboPerfil" runat="server" CssClass="cz-form-content-input-select">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                        </p>
                        <asp:DropDownList ID="cboGrupro" runat="server" CssClass="cz-form-content-input-select">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_USUARIO)%>
                        </p>
                        <asp:DropDownList ID="cboVendedor" runat="server" CssClass="cz-form-content-input-select">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FECHA)%>
                        </p>
                        <input name="txtFecha" type="text" value="30/05/2014" maxlength="10" id="txtFecha"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content" style="width: auto; height: 30px">
                        <span>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SATELITE)%></span>
                        <input type="checkbox" id="chkSatelite" />
                    </div>

                    <div class="cz-form-content">
                        <div class="form-gridview-search">
                            <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                            <p>
                                buscando resultados
                            </p>
                        </div>
                    </div>

                    <%--BOTONES--%>
                    <div class="cz-form-content">
                        <input type="button" id="btnGenerar" value="Generar" class="form-button cz-form-content-input-button" />
                        <input type="button" id="btnCancelar" value="Limpiar" class="form-button cz-form-content-input-button" />
                    </div>

                </div>


                <div id="leyenda" style="display: none;" class="cz-form-box-content">
                    <div class="cz-form-content">

                        <ul>
                        </ul>
                    </div>

                </div>
                <div id="cz-modal-geo-option2">
                    <input name="address" type="text" id="address" style="width: 300px" placeholder="Ingresa Dirección" />
                    <div class="cz-modal-geo-back">
                    </div>
                    <div class="contentGeo">
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
    </form>
    <div id="calendarwindow" style="z-index: 9000;" class="calendar-window">
</body>
</html>
