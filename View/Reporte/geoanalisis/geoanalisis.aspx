﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.geoanalisis.geoanalisis" Codebehind="geoanalisis.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3&sensor=false&language=es&libraries=drawing,places"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <title></title>
    <style type="text/css">
        .modalGeo
        {
            right: 0%;
            left: inherit;
            width: 300px;
        }
        #map img
        {
            max-width: none;
        }
        #map label
        {
            width: auto;
            display: inline;
        }
    </style>
    <script>
        function pulsar(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13) return false;
        }
    </script>
      <script>
        var urlLoad = 'geoanalisis.aspx/cargarGeoAnalisis';
      var markersArray = [];
        $(document).ready(function () {


            initialize(<%=latitudIni %>,<%=longitudIni %> );

            $('#btnObtener').click(function(e){
                cargaPointsGeoAnalisis(urlLoad,true)
            
            });
        });



        function cargaPointsGeoAnalisis(urlM,polilyne)
        { 
           deleteMarker(); 
   
           $.ajax({
                    type: 'POST',
                    url: urlM,
                    data:JSON.stringify(getData()),
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    dataType: "json",
                    beforeSend: function () {
                        $(".form-gridview-search").show();
                    },
                    success: function (data) {
              
                     var items=0;
                     var isAlone=true;
                      $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
                      
                          crearPoint(objPoint,jQuery.parseJSON(data.d));
                         
                      });
                      setTimeout(function(){
                      map.setCenter(setZoom());},1000);
                      $('.form-gridview-search').hide();
                    },
            
                    error: function (xhr, status, error) {
              
                    }
                }); 
        
     
        }

       function getData()
        {

        var StrData = new Object;
        StrData.operacion = $('#cboDato').val();
        StrData.tipo=$('#cboTipoFiltro').val();
        StrData.fechaini = $('#txtDesde').val(); //DESDE
        StrData.fechafin = $('#txtHasta').val(); //HASTA
        StrData.asistencia = $('#chkSatelite').attr("checked") ? 'S' : '';

        return StrData;

        }

    </script>
</head>
<body onkeypress="return pulsar(event)">
    <form id="form1" runat="server">
    <input id="hAccion" type="hidden" />
    <input id="hNombre" type="hidden" />
    <input id="hCodigo" type="hidden" />
    <div id="map" class="cz-maps-full">
    </div>
    <div id="cz-util-hidden-right-ultpos-geo" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-left">
        <div class="cz-util-hidden-arrow">
        </div>
    </div>
    <div class="modalGeo hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="false" style="display: block;">
        <div id="myModalContent" class="modal-body">
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MAPA_GEOANALISIS_DATO)%></p>
                    <asp:DropDownList ID="cboDato" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                 <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRP_FILTROTIPO)%></p>
                    <asp:DropDownList ID="cboTipoFiltro" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                 <div class="cz-form-content" style="width: auto; height: 30px">
                <span>
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SATELITE)%></span>
                <input type="checkbox" id="chkSatelite" />
            </div>
                <div class="cz-form-content">
                    <p>
                        <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_DESDE)%>
                    </p>
                    <input name="txtDesde" type="text" value="30/05/2014" maxlength="10" id="txtDesde"
                        runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                        onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                    <div class="cz-form-content-input-calendar-visible">
                        <div class="cz-form-content-input-calendar-visible-button">
                            <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                src="../../images/icons/calendar.png" />
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                        <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_HASTA)%>
                    </p>
                    <input name="txtHasta" type="text" value="30/05/2014" maxlength="10" id="txtHasta"
                        runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                        onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                    <div class="cz-form-content-input-calendar-visible">
                        <div class="cz-form-content-input-calendar-visible-button">
                            <img alt="<>" id="Img2" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                src="../../images/icons/calendar.png" />
                        </div>
                    </div>
                </div>
            <%--BOTONES--%>
            <div class="cz-form-content">
                <input type="button" id="btnObtener" value="Obtener" class="form-button cz-form-content-input-button" />
            </div>
            </div>
           
        </div>
        <div id="cz-modal-geo-option2">
            <%--<input name="address" type="text" id="address" style="width: 300px" placeholder="Ingresa Dirección" />--%>
            <div class="cz-modal-geo-back">
            </div>
            <div class="contentGeo">
                <div class="form-gridview-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                    <p>
                        buscando resultados</p>
                </div>
                <ul>
                </ul>
            </div>
        </div>
    </div>
    </div>
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
    </div>
    </form>
    <div id="calendarwindow" style="z-index: 9000;" class="calendar-window">
</body>
</html>
