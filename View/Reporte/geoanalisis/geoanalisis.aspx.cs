﻿using System;
using System.Collections.Generic;
using Model.bean;
using Controller;
using System.Web.Services;
using Controller.functions;

namespace Pedidos_Site.Reporte.geoanalisis
{
    public partial class geoanalisis : PageController
    {
        public string latitudIni;
        public string longitudIni;
        public List<PerfilBean> listaPerfil = new List<PerfilBean>();
        protected override void initialize()
        {
            if (ManagerConfiguration.laltitudlongitud != "0")
            {
                String[] latlo = ManagerConfiguration.laltitudlongitud.Split(',');


                try
                {
                    List<BECbx> loLista = new List<BECbx>();
                    loLista.Add(new BECbx("C", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_CLIENTE)));
                    loLista.Add(new BECbx("P", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_PRODUCTO)));
                    cboDato.DataTextField = "Texto";
                    cboDato.DataValueField = "Valor";
                    cboDato.DataSource = loLista;
                    cboDato.DataBind();

                    loLista.Clear();
                    loLista.Add(new BECbx("T", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_OPERACIONCANTIDAD)));
                    loLista.Add(new BECbx("S", IdiomaCultura.getMensajeEncodeJS(IdiomaCultura.WEB_GRP_OPERACIONMONTO)));

                    cboTipoFiltro.DataTextField = "Texto";
                    cboTipoFiltro.DataValueField = "Valor";
                    cboTipoFiltro.DataSource = loLista;
                    cboTipoFiltro.DataBind();
                }


                catch (Exception) { }

                latitudIni = latlo[0];
                longitudIni = latlo[1];

                txtDesde.Value = Utils.getFechaActual();
                txtHasta.Value = Utils.getFechaActual();

            }
            else
            {
                latitudIni = "- 12.0462037802177";
                longitudIni = "-77.0656585693359";
            }
        }



        [WebMethod]
        public static string cargarGeoAnalisis(String operacion, String tipo, String fechaini, String fechafin, String asistencia)
        {

            try
            {
                fechaini = Utils.getStringFechaYYMMDD(fechaini);
                fechafin = Utils.getStringFechaYYMMDD(fechafin);

                List<MapBean> lst = ReporteController.GeoAnalisisBuscar(operacion, tipo, fechaini, fechafin, asistencia);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}