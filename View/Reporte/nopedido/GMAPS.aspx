﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.nopedido.Reporte_asistencia_GMAPS" Codebehind="GMAPS.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <style>
        #map img
        {
            max-width: none;
        }
        #map label
        {
            width: auto;
            display: inline;
        }
    </style>
    <script>
          var markersArray = [];
        $(document).ready(function () {
            $(document).ajaxStop(function () {
                /* your code here ?*/
            });
            initialize(-12.044189, -77.061737);
           
            cargaPoints('GMAPS.aspx/cargarMapa',false)

        });

        function getData() {

            var strData = '{"codigo": "' + <%= codigo %>+ '" }';
            return jQuery.parseJSON(strData);

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="H1" runat="server">
            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TRACKINGNOPEDIDO)%></h3>
    </div>

      <br/>
     
    <div id="myModalContent" class="modal-body" style="padding: 0px;">
     <div class="cz-form-content" style="height:30px" >
            <span style="font-weight:500;">
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_USUARIO)%>
                : </span>
            <asp:Label ID="lblNomUsuario" runat="server"></asp:Label>
        </div>
        <div class="cz-form-content" style="height:30px">
            <span style="font-weight:500;">
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAINICIO)%>
                :</span>
            <asp:Label ID="lblFechaInicio" runat="server"></asp:Label>
        </div>
        <div class="cz-form-content" style="height:30px">
            <span style="font-weight:500;">
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAFIN)%>
                :</span>
            <asp:Label ID="lblFechaFin" runat="server"></asp:Label>
        </div>
      
        <div class="cz-form-content" style="height:30px">
            <span style="font-weight:500;">
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                :</span>
            <asp:Label ID="lblCliente" runat="server"></asp:Label>
        </div>
        <div class="cz-form-content" style="height:30px">
            <span style="font-weight:500;">
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MOTIVO_VALOR)%>
                :</span>
            <asp:Label ID="lblMotivo" runat="server"></asp:Label>
        </div>
          
        <div id="map" style="width: 100%; height: 400px">
        </div>
    </div>
    <div class="modal-footer">
        <button id="cancelReg" class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button>
    </div>
    </form>
</body>
</html>
