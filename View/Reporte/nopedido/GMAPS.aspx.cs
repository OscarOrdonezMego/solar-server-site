﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;

namespace Pedidos_Site.Reporte.nopedido
{
    public partial class Reporte_asistencia_GMAPS : System.Web.UI.Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');

            lblNomUsuario.Text = dDato[1].ToString();
            lblFechaInicio.Text = dDato[2].ToString();
            lblFechaFin.Text = dDato[3].ToString();
            lblMotivo.Text = dDato[5].ToString();
            lblCliente.Text = dDato[4].ToString();

        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {

                String[] xdatos = codigo.Split('|');

                String xped = xdatos[0].ToString();
                String xusu = xdatos[1].ToString();
                String xini = xdatos[2].ToString();
                String xfin = xdatos[3].ToString();
                String xcli = xdatos[4].ToString();
                String xmot = xdatos[5].ToString();

                String xlat = xdatos[6].ToString();
                String xlon = xdatos[7].ToString();


                List<MapBean> lst = new List<MapBean>();

                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;
                be.msg = "<h4>" + "No Pedido" + "</h4> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUMNOPEDIDOUPPERCASE) + ": " + xped + "</br> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + xusu + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHAINICIO) + ": " + xini + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHAFIN) + ": " + xfin + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE) + ": " + xcli + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO_VALOR) + ": " + xmot;

                be.img = "../../images/gps/no-pedido.png";

                be.titulo = "No Pedido";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }

        }
    }
}