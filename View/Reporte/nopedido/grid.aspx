﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.nopedido.Reporte_asistencia_grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 16/04/2015 Se muestra Dirección
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th  nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                            <th  nomb="NOMBREGRUPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th nomb="FECHAINICIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_INICIO)%>
                            </th>
                            <th  nomb="FECHAFIN">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FIN)%>
                            </th>
                            <th nomb="CODIGOCLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_CLIENTE)%>
                            </th>
                            <th  nomb="CLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                            <%--@001 I--%>
                            <th  nomb="DIRECCION">
                                Dirección
                            </th>
                            <%--@001 F--%>
                            <th  nomb="MOTIVO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MOTIVO_VALOR)%>
                            </th>
                          
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("usr_nombre")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecfin")%>
                    </td>
                    <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("PED_DIRECCION")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <%# Eval("mot_nombre")%>
                    </td>
                    
                    <td>
                          <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("usr_nombre") %>|<%# Eval("ped_fecinicio") %>|<%# Eval("ped_fecfin") %>|<%# Eval("cli_nombre") %>|<%# Eval("mot_nombre") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                    </td>
                   
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <%# Eval("usr_nombre")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecfin")%>
                    </td>
                    <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("PED_DIRECCION")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <%# Eval("mot_nombre")%>
                    </td>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("usr_nombre") %>|<%# Eval("ped_fecinicio") %>|<%# Eval("ped_fecfin") %>|<%# Eval("cli_nombre") %>|<%# Eval("mot_nombre") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
