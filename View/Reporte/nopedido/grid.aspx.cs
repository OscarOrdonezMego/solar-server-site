﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Controller.functions;
using Model;

namespace Pedidos_Site.Reporte.nopedido
{
    public partial class Reporte_asistencia_grid : PageController
    {
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String fcobertura = dataJSON["FlagEnCobertura"].ToString();
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
            String vendedor = dataJSON["vendedor"].ToString();
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            String flgfecreg = dataJSON["Flagfecreg"].ToString();

            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }

            PaginateReporteNoPedidoBean paginate = ReporteController.paginarNoPedidoBuscar(vendedor,
                fini, ffin, fcobertura, pagina, filas, codperfil, codgrupo, id_usu, flgfecreg);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReporteNoPedidoBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }
    }
}