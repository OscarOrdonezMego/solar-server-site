﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.canje.Reporte_asistencia_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                             <th  nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                            <th nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th nomb="WEB_CODIGO_CLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_CLIENTE)%>
                            </th>
                            <th nomb="WEB_CLIENTE">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                              <th  nomb="WEB_ALMACEN" <%=liValidarVisibleAlmacen %>>
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ALMACEN)%>                            
                            </th>
                            <th nomb="WEB_PRODUCTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                            </th>
                           <th  nomb="WEB_PRESENTACION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRESENTACION)%>
                            </th>
                           <th  nomb="WEB_CANTIDAD_PRESENTACION" <%=liValidarVisible %>>
                             <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD_PRESENTACION)%>
                            </th>
                             <th nomb="WEB_CANTIDAD_PRESENTACION" <%=liValidarVisible %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%>
                            </th>
                               <th nomb="WEB_CANTIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                            <th nomb="WEB_FRACCION" <%=liValidarVisible %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FRACCION)%>
                            </th>
                               <th  nomb="WEB_PRECIO" <%=liValidarVisible %>>
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIO)%>
                            </th>
                             <th nomb="DIRECCION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MOTIVO)%>
                            </th>
                            
                             <th nomb="WEB_FECHA_VENCIMIENTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHA_VENCIMIENTO)%>
                            </th>
                           <th nomb="WEB_OBSERVACION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_OBSERVACION)%>
                            </th>
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>

                <tr>
                     <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("USR_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("CABC_CODCLIENTE")%>
                    </td>
                    <td>
                        <%# Eval("CLI_NOMBRE")%>
                    </td>
                      <td <%=liValidarVisibleAlmacen %>>
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DETC_CANTIDAD_PRE")%>
                    </td>
                     <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                     <td>
                        <%# Eval("DETC_CANTIDAD")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DETC_CANTIDAD_FRAC")%>
                    </td>
                       <td <%=liValidarVisible %>>
                        <%# Eval("PRECIO_PRESENTACION")%>
                    </td>
                    
                      <td>
                        <%# Eval("MOT_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("DETC_FECVCMTO")%>
                    </td>
                    <td>
                        <%# Eval("DETC_OBSERVACION")%>
                    </td>
                    
                   
                    <td>
                          <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("USR_NOMBRE") %>|<%# Eval("CLI_NOMBRE") %>|<%# Eval("PRO_NOMBRE") %>|<%# Eval("DETC_CANTIDAD") %>|<%# Eval("MOT_NOMBRE") %>|<%# Eval("DETC_OBSERVACION") %>|<%# Eval("DETC_FECVCMTO") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                    </td>
                   
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                     <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("USR_NOMBRE")%>
                    </td>
                       <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("CABC_CODCLIENTE")%>
                    </td>
                    <td>
                        <%# Eval("CLI_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisibleAlmacen %>>
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                     <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DETC_CANTIDAD_PRE")%>
                    </td>
                      <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                     <td>
                        <%# Eval("DETC_CANTIDAD")%>
                    </td>
                    <td  <%=liValidarVisible %>>
                        <%# Eval("DETC_CANTIDAD_FRAC")%>
                    </td> 
                        <td <%=liValidarVisible %>>
                        <%# Eval("PRECIO_PRESENTACION")%>
                    </td>
                      <td>
                        <%# Eval("MOT_NOMBRE")%>
                    </td>
                       <td>
                        <%# Eval("DETC_FECVCMTO")%>
                    </td>
                    <td>
                        <%# Eval("DETC_OBSERVACION")%>
                    </td>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("USR_NOMBRE") %>|<%# Eval("CLI_NOMBRE") %>|<%# Eval("PRO_NOMBRE") %>|<%# Eval("DETC_CANTIDAD") %>|<%# Eval("MOT_NOMBRE") %>|<%# Eval("DETC_OBSERVACION") %>|<%# Eval("DETC_FECVCMTO") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
