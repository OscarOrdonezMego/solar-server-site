﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.pedidototal.Reporte_asistencia_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divGridView" runat="server">
            <asp:Repeater ID="grdMant" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%" class="grilla table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class=" orden" nomb="ID">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                                </th>

                                <th class=" orden" nomb="VENDEDOR">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                                </th>

                                <th class=" orden" nomb="GRUPO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                                </th>

                                <th class="orden" nomb="CLIENTE">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                                </th>

                                <% if (Mcv == "1")
                                { %>
                                <th class="orden" nomb="CONDICION">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CONDICION)%>
                                </th>
                                <% } %>

                                <th class=" orden" nomb="FECHA">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHA)%>
                                </th>
                                <th class=" orden" nomb="TOTAL_SOLES">
                                    Total Soles
                                </th>
                                <th class=" orden" nomb="TOTAL_DOLARES">
                                    Total Dólares
                                </th>
                                <th class=" orden" nomb="TIPO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                                </th>
                                <th class=" orden" nomb="PRODUCTO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                                </th>
                                
                                <% if (liRespuestaint == 1)
                                { %>
                                <th class="orden" nomb="NOMBRE_PRESENTACION">
                                    Presentación
                                </th>
                                <% } %>

                                <% if (liRespuestaint == 1)
                                { %>
                                <th class="orden" nomb="CANTIDAD_PRESENTACION">
                                    Cantidad Presentación
                                </th>
                                <% } %>

                                <% if (liRespuestaint == 1)
                                { %>
                                <th class="orden" nomb="UNIDAD_FRACCIONAMIENTO">
                                    Unid Fraccionamiento
                                </th>
                                <% } %>

                                <th class=" orden" nomb="CANTIDAD">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                                </th>

                                <% if (liRespuestaint == 1)
                                { %>
                                <th class="orden" nomb="FRACCION">
                                    Fracción
                                </th>
                                <% } %>

                                <th class=" orden" nomb="PRECIO_SOLES">
                                    Precio Soles
                                </th>
                                <th class=" orden" nomb="PRECIO_DOLARES">
                                    Precio Dólares
                                </th>
                                <th class=" orden" nomb="PORCENTAJE">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCPORCENTAJE)%>
                                </th>
                                <% if (bonificacion == true)
                                   { %>
                                <th class=" orden" nomb="BONIFICACION">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                                </th>
                                <% } %>
                                <th class=" orden" nomb="MONTO_SOLES">
                                    Monto Soles
                                </th>
                                <th class=" orden" nomb="MONTO_DOLARES">
                                    Monto Dólares
                                </th>
                                <th class=" orden" nomb="DESCRIPCION">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                                </th>

                                <th class=" orden" nomb="DISTANCIA">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DISTANCIA)%>
                                </th>

                                <th style="width: 40px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# Eval("ID")%>
                        </td>

                        <td>
                            <%# Eval("NOM_USUARIO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBREGRUPO")%>
                        </td>
                        <td>
                            <%# Eval("CLI_NOMBRE")%>
                        </td>
                        <% if (Mcv == "1")
                           { %>
                        <td>
                            <%# Eval("CONDVTA_NOMBRE")%>
                        </td>
                        <% } %>
                        <td>
                            <%# Eval("PED_FECINICIO")%>
                        </td>
                        <td>
                            <%# Eval("PED_MONTOTOTAL_SOLES")%>
                        </td>
                        <td>
                            <%# Eval("PED_MONTOTOTAL_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("TIPO_NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("PRO_NOMBRE")%>
                        </td>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("NOMBRE_PRESENTACION")%>
                            </td>
                        <% } %>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("DET_CANTIDAD_PRESENTACION")%>
                            </td>
                        <% } %>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                            </td>
                        <% } %>

                        <td>
                            <%# Eval("DET_CANTIDAD")%>
                        </td>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("DET_CANTIDAD_FRACCION")%>
                            </td>
                        <% } %>

                        <td>
                            <%# Eval("DET_PRECIOBASE_SOLES")%>
                        </td>
                        <td>
                            <%# Eval("DET_PRECIOBASE_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("DESCUENTO")%>
                        </td>
                        <% if (bonificacion == true)
                           { %>
                        <td>
                            <%# Eval("DET_BONIFICACION")%>
                        </td>
                        <% } %>
                        <td>
                            <%# Eval("DET_MONTO_SOLES")%>
                        </td>
                         <td>
                            <%# Eval("DET_MONTO_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("DESCRIPCION")%>
                        </td>

                        <td>
                            <%# Eval("DISTANCIA")%>
                        </td>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                                cod="<%# Eval("ID") %>|<%# Eval("NOM_USUARIO") %>|<%# Eval("PED_FECINICIO") %>|<%# Eval("PED_FECFIN") %>|<%# Eval("CLI_NOMBRE") %>|<%# Eval("CONDVTA_NOMBRE") %>|<%# Eval("DET_MONTO_SOLES") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>|<%# Eval("CLI_LAT") %>|<%# Eval("CLI_LON") %>">
                                <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                        </td>


                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt">
                        <td>
                            <%# Eval("ID")%>
                        </td>
                        <td>
                            <%# Eval("NOM_USUARIO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBREGRUPO")%>
                        </td>
                        <td>
                            <%# Eval("CLI_NOMBRE")%>
                        </td>
                        <% if (Mcv == "1")
                           { %>
                        <td>
                            <%# Eval("CONDVTA_NOMBRE")%>
                        </td>
                        <% } %>
                        <td>
                            <%# Eval("PED_FECINICIO")%>
                        </td>
                        <td>
                            <%# Eval("PED_MONTOTOTAL_SOLES")%>
                        </td>
                        <td>
                            <%# Eval("PED_MONTOTOTAL_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("TIPO_NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("PRO_NOMBRE")%>
                        </td>
                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("NOMBRE_PRESENTACION")%>
                            </td>
                        <% } %>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("DET_CANTIDAD_PRESENTACION")%>
                            </td>
                        <% } %>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                            </td>
                        <% } %>

                        <td>
                            <%# Eval("DET_CANTIDAD")%>
                        </td>

                        <% if (liRespuestaint == 1)
                        { %>
                            <td >
                                <%# Eval("DET_CANTIDAD_FRACCION")%>
                            </td>
                        <% } %>

                        <td>
                            <%# Eval("DET_PRECIOBASE_SOLES")%>
                        </td>
                         <td>
                            <%# Eval("DET_PRECIOBASE_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("DESCUENTO")%>
                        </td>
                        <% if (bonificacion == true)
                           { %>
                        <td>
                            <%# Eval("DET_BONIFICACION")%>
                        </td>
                        <% } %>
                        <td>
                            <%# Eval("DET_MONTO_SOLES")%>
0                        <td>
                            <%# Eval("DET_MONTO_DOLARES")%>
                        </td>
                        <td>
                            <%# Eval("DESCRIPCION")%>
                        </td>

                        <td>
                            <%# Eval("DISTANCIA")%>
                        </td>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer; display: <%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                                cod="<%# Eval("ID") %>|<%# Eval("NOM_USUARIO") %>|<%# Eval("PED_FECINICIO") %>|<%# Eval("PED_FECFIN") %>|<%# Eval("CLI_NOMBRE") %>|<%# Eval("CONDVTA_NOMBRE") %>|<%# Eval("DET_MONTO_SOLES") %>|<%# Eval("LATITUD") %>|<%# Eval("LONGITUD") %>|<%# Eval("CLI_LAT") %>|<%# Eval("CLI_LON") %>">
                                <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" /></a>
                        </td>


                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Pagina</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <p>
                                <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            buscando resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">
                    ×
                </div>
            </div>
        </div>
    </form>
</body>
</html>
