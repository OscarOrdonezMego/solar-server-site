﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Model;
using Controller;
using Controller.functions;

namespace Pedidos_Site.Reporte.pedidototal
{
    public partial class Reporte_asistencia_grid : PageController
    {
        public string Mcv = "";
        public string Mbm = "";
        public Boolean bonificacion = false;
        public static String liValidarVisible;
        public static Int32 liRespuestaint;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
            String vendedor = dataJSON["vendedor"].ToString();
            String fcobertura = dataJSON["FlagEnCobertura"].ToString();
            String fBonificacion = dataJSON["Bonificacion"].ToString();
            String fTipoPedido = dataJSON["FlagTipoPedido"].ToString();
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();

            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }
            PaginateReportePedidoTotalBean paginate = null;
            Mcv = ManagerConfiguration.mostrar_condvta;
            Mbm = ManagerConfiguration.nBonificacionManual;

            if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                paginate = ReporteController.paginarPedidopresetacionTotalBuscar(fTipoPedido, vendedor,
                    fini, ffin, fcobertura, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);
            }
            else
            {
                paginate = ReporteController.paginarPedidoTotalBuscar(fTipoPedido, vendedor, fini,
                    ffin, fcobertura, fBonificacion, pagina, filas, codperfil, codgrupo, id_usu);
            }

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReportePedidoTotalBean> lst = paginate.lstResultados;
                if (lst.Count > 0) bonificacion = lst[0].tieneBonificacion.Equals("0") ? false : true;

                grdMant.DataSource = lst;
                grdMant.DataBind();
            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }
    }
}
