﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.Reporte_Exportar" Codebehind="Exportar.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:GridView ID="gvPedido" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvPedido_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="nom_usuario" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecinicio" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecfin" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="cli_codigo" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="cli_nombre" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="DIRECCION" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="PED_DIRECCION_DESPACHO" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="TIPO_NOMBRE" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="condvta_nombre" HeaderText="10"></asp:BoundField>
                    <%--<asp:BoundField DataField="ped_montototal" HeaderText="11"></asp:BoundField>--%>
                    <asp:BoundField DataField="MontoTotalSoles" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="MontoTotalDolares" HeaderText="12"></asp:BoundField>
                    <asp:BoundField DataField="TOTAL_FLETE" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="observacion" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="distancia" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="HABILITADO" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="latitud" HeaderText="17"></asp:BoundField>
                    <asp:BoundField DataField="longitud" HeaderText="18"></asp:BoundField>

                </Columns>
            </asp:GridView>
            
            <asp:GridView ID="gvPedido2" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvPedido_RowDataBound_2">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="nom_usuario" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecinicio" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecfin" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="cli_codigo" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="cli_nombre" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="DIRECCION" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="condvta_nombre" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="TIPO_NOMBRE" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="ped_montototal" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="PED_OBSERVACION" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="distancia" HeaderText="12"></asp:BoundField>
                    <asp:BoundField DataField="HABILITADO" HeaderText="13"></asp:BoundField>

                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvNoPedido" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvNoPedido_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="usr_nombre" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecinicio" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecfin" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="cli_codigo" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="cli_nombre" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="direccion" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="mot_nombre" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="habilitado" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="latitud" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="longitud" HeaderText="11"></asp:BoundField>

                </Columns>
            </asp:GridView>
           
            <asp:GridView ID="gvDevolucion" runat="server"  AutoGenerateColumns="false"
                OnRowDataBound="gvDevolucion_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="USR_NOMBRE" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="CDEV_CODCLIENTE" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="ALMACEN" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="6"></asp:BoundField>
                     <asp:BoundField DataField="NOMBRE_PRESENTACION" HeaderText="7"></asp:BoundField>
                     <asp:BoundField DataField="CANTIDA_PRESENTACION" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="UNIDAD_FRACCIONAMIENTO" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="DDEV_CANTIDAD" HeaderText="10"></asp:BoundField> 
                    <asp:BoundField DataField="DDEV_CANTIDAD_FRAC" HeaderText="11"></asp:BoundField>
                     <asp:BoundField DataField="PRECIO_PRESENTACION" HeaderText="12"></asp:BoundField>                                  
                    <asp:BoundField DataField="MOT_NOMBRE" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="FECVCMTO" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="CDEV_OBSERVACION" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="LATITUD" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="LONGITUD" HeaderText="17"></asp:BoundField>

                </Columns>
            </asp:GridView>
            
            <asp:GridView ID="gvDevolucionSimple" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvDevolucion_RowDataBound_simple">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="USR_NOMBRE" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="CDEV_CODCLIENTE" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="ALMACEN" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="DDEV_CANTIDAD" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="MOT_NOMBRE" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="FECVCMTO" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="CDEV_OBSERVACION" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="LATITUD" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="LONGITUD" HeaderText="12"></asp:BoundField>

                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvCanje" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvCanje_RowDataBound_simple">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="USR_NOMBRE" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="CABC_CODCLIENTE" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="ALM_NOMBRE" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="DETC_CANTIDAD" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="MOT_NOMBRE" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="DETC_FECVCMTO" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="DETC_OBSERVACION" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="LATITUD" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="LONGITUD" HeaderText="12"></asp:BoundField>
          

                </Columns>
            </asp:GridView>
            
            <asp:GridView ID="gvCanjepresentacion" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvCanje_RowDataBound_presentacion">
                <Columns>
                     <asp:BoundField DataField="id" HeaderText="0"></asp:BoundField>
                    <asp:BoundField DataField="USR_NOMBRE" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="CABC_CODCLIENTE" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="ALM_NOMBRE" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="6"></asp:BoundField>
                     <asp:BoundField DataField="NOMBRE_PRESENTACION" HeaderText="7"></asp:BoundField>
                     <asp:BoundField DataField="DETC_CANTIDAD_PRE" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="UNIDAD_FRACCIONAMIENTO" HeaderText="9"></asp:BoundField>
                     <asp:BoundField DataField="DETC_CANTIDAD" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="DETC_CANTIDAD_FRAC" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="MOT_NOMBRE" HeaderText="12"></asp:BoundField>
                     <asp:BoundField DataField="DETC_FECVCMTO" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="DETC_OBSERVACION" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="LATITUD" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="LONGITUD" HeaderText="16"></asp:BoundField>

                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvPedidoTotal" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvPedidoTotal_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="PED_PK" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOM_USUARIO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_CODIGO" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="CONDVTA_NOMBRE" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="PED_FECINICIO" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="PED_MONTOTOTAL_SOLES" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="PED_MONTOTOTAL_DOLARES" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="PRO_CODIGO" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="DET_CANTIDAD" HeaderText="12"></asp:BoundField>
                    <asp:BoundField DataField="DET_PRECIOBASE_SOLES" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="DET_PRECIOBASE_DOLARES" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="DESCUENTO" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="BONIFICACION" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="DET_MONTO_SOLES" HeaderText="17"></asp:BoundField>
                    <asp:BoundField DataField="DET_MONTO_DOLARES" HeaderText="18"></asp:BoundField>
                    <asp:BoundField DataField="DESCRIPCION" HeaderText="19"></asp:BoundField>
                    <asp:BoundField DataField="PED_OBSERVACION" HeaderText="20"></asp:BoundField>
                </Columns>
            </asp:GridView>
            
            <asp:GridView ID="gvPedidoTotalPresentacion" runat="server" AutoGenerateColumns="false"
                OnRowDataBound="gvPedidoTotal_presentacion_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="PED_PK" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="NOM_USUARIO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="CLI_CODIGO" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="CONDVTA_NOMBRE" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="PED_FECINICIO" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="TIPO_NOMBRE" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="PED_MONTOTOTAL_SOLES" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="PED_MONTOTOTAL_DOLARES" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="PRO_CODIGO" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="PRO_NOMBRE" HeaderText="12"></asp:BoundField>                    
                    <asp:BoundField DataField="NOMBRE_PRESENTACION" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="DET_CANTIDAD_PRESENTACION" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="UNIDAD_FRACCIONAMIENTO" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="DET_CANTIDAD" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="DET_CANTIDAD_FRACCION" HeaderText="17"></asp:BoundField>
                    <asp:BoundField DataField="DET_PRECIOBASE_SOLES" HeaderText="18"></asp:BoundField>
                    <asp:BoundField DataField="DET_PRECIOBASE_DOLARES" HeaderText="19"></asp:BoundField>
                    <asp:BoundField DataField="DESCUENTO" HeaderText="20"></asp:BoundField>
                    <asp:BoundField DataField="BONIFICACION" HeaderText="21"></asp:BoundField>
                    <asp:BoundField DataField="DET_MONTO_SOLES" HeaderText="22"></asp:BoundField>
                    <asp:BoundField DataField="DET_MONTO_DOLARES" HeaderText="23"></asp:BoundField>
                    <asp:BoundField DataField="FLETE_TOTAL" HeaderText="24"></asp:BoundField>
                    <asp:BoundField DataField="MONTO_TOTAL_FLETE" HeaderText="25"></asp:BoundField>
                    <asp:BoundField DataField="DESCRIPCION" HeaderText="26"></asp:BoundField>
                    <asp:BoundField DataField="PED_OBSERVACION" HeaderText="27"></asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvQuiebreStock" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvQuiebreStock_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="nom_usuario" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecinicio" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecfin" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="cli_nombre" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="pro_codigo" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="pro_nombre" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="det_cantidad" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="det_bonificacion" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="pro_stock" HeaderText="11"></asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvCobranza" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvCobranza_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="COB_CODIGO" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="USR_NOMBRE" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="CLI_CODIGO" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="CLI_NOMBRE" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="COB_TIPODOCUMENTO" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="TIPO_DOCUMENTO" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="COB_MONTOTOTAL" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="COB_MONTOPAGADO" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="SALDO" HeaderText="11"></asp:BoundField>
                     <asp:BoundField DataField="COB_MONTOTOTALDOLARES" HeaderText="12"></asp:BoundField>
                    <asp:BoundField DataField="COB_MONTOPAGADODOLARES" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="SALDODOLARES" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="PAG_VOUCHER" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="BAN_NOMLARGO" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="PAG_TIPOPAGO" HeaderText="17"></asp:BoundField>
                    <asp:BoundField DataField="PAG_FECREGISTRO" HeaderText="18"></asp:BoundField>
                    <asp:BoundField DataField="PAG_MONTOPAGO" HeaderText="19"></asp:BoundField>
                    <asp:BoundField DataField="PAG_MONTOPAGODOLARES" HeaderText="20"></asp:BoundField>
                    <asp:BoundField DataField="LATITUD" HeaderText="21"></asp:BoundField>
                    <asp:BoundField DataField="LONGITUD" HeaderText="22"></asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvReserva" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvReserva_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="idped" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="ped_fecinicio" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="cli_nombre" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="pro_codigo" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="pro_nombre" HeaderText="5"></asp:BoundField>
                    <asp:BoundField DataField="det_cantidad" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="DET_BONIFICACION" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="pro_stock" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="nom_usuario" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="NOMBREGRUPO" HeaderText="10"></asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gvConsolidado" runat="server">
            </asp:GridView>

            <asp:GridView ID="gvProspecto" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvProspecto_RowDataBound">
                <Columns>                    
                    <asp:BoundField DataField="codigo" HeaderText="1"></asp:BoundField>
                    <asp:BoundField DataField="nombre" HeaderText="2"></asp:BoundField>
                    <asp:BoundField DataField="direccion" HeaderText="3"></asp:BoundField>
                    <asp:BoundField DataField="usercod" HeaderText="4"></asp:BoundField>
                    <asp:BoundField DataField="vendedor" HeaderText="5"></asp:BoundField>      
                    <asp:BoundField DataField="tclicod" HeaderText="6"></asp:BoundField>
                    <asp:BoundField DataField="tipocliente" HeaderText="7"></asp:BoundField>
                    <asp:BoundField DataField="giro" HeaderText="8"></asp:BoundField>
                    <asp:BoundField DataField="latitud" HeaderText="9"></asp:BoundField>
                    <asp:BoundField DataField="longitud" HeaderText="10"></asp:BoundField>
                    <asp:BoundField DataField="fecha" HeaderText="11"></asp:BoundField>
                    <asp:BoundField DataField="fechaMovil" HeaderText="12"></asp:BoundField>          
                    <asp:BoundField DataField="observacion" HeaderText="13"></asp:BoundField>
                    <asp:BoundField DataField="CampoAdicional1" HeaderText="14"></asp:BoundField>
                    <asp:BoundField DataField="CampoAdicional2" HeaderText="15"></asp:BoundField>
                    <asp:BoundField DataField="CampoAdicional3" HeaderText="16"></asp:BoundField>
                    <asp:BoundField DataField="CampoAdicional4" HeaderText="17"></asp:BoundField>
                    <asp:BoundField DataField="CampoAdicional5" HeaderText="18"></asp:BoundField>                    
                </Columns>
            </asp:GridView>

        </div>

    </form>
</body>
</html>
