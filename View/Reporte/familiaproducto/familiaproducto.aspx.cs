﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller.functions;
using Model.bean;
using Controller;
using Model;
using System.Web.Services;

/// <summary>
/// @001 GMC 16/04/2015 Se agrega filtro Tipo Pedido
/// </summary>
namespace Pedidos_Site.Reporte.familiaproducto
{
    public partial class familiaproducto : PageController
    {
        public string Mbm = "";
        public static String lsCodMenu = "RPE";
        public static String deshabilitarCboSupervisor;
        public static String codigoSub = "";
        public static String pkusu = "";
        public static Int32 ioRespuesta;
        public static String ioRespuestaString = "";
        public static String id_usu = "";

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || Session["lgn_codsupervisor"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            if (!IsPostBack)
            {
                llenarComboBox();
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                if (codigoSub == "SUP")
                {
                    cboPerfil.SelectedValue = pkusu;
                    deshabilitarCboSupervisor = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                else
                {
                    deshabilitarCboSupervisor = "";
                }
                llenarcoGrupo();
                llenarComboBoxVendedor(pkusu);
                txtFechaIni.Value = Utils.getFechaActual();
                txtFechaFin.Value = Utils.getFechaActual();
            }
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;

            }

            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }



            llenarCboFamilia();
        }
        private void llenarcoGrupo()
        {
            List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBox()
        {
            List<UsuarioBean> lista = UsuarioController.ListaSupervisores();
            if (lista.Count != 0)
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboPerfil.Items.Insert(i + 1, lista[i].nombre);
                    cboPerfil.Items[i + 1].Value = lista[i].id;

                }
            }
            else
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
            }
        }

        private void llenarComboBoxGrupo(String codigo, String perfil)
        {
            List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), perfil);
            if (lista.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                    cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();

                }
            }
            else
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
            }
        }

        private void llenarCboGrupos(Int32 USUPK, String TIPO)
        {
            List<GrupoBean> loLstGrupoBean = UsuarioController.fnListarGrupoActivo(USUPK, TIPO);
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
        }




        private void llenarCboFamilia()
        {
            List<FamiliaProductoBean> loLstFamiliaBean = ProductoFamiliaController.fnListarFamiliaProductos();
            if (loLstFamiliaBean.Count != 0)
            {
                cboFamilia.Items.Insert(0, "Todos");
                cboFamilia.Items[0].Value = "0";
                for (int i = 0; i < loLstFamiliaBean.Count; i++)
                {
                    cboFamilia.Items.Insert(i + 1, loLstFamiliaBean[i].nombre);
                    cboFamilia.Items[i + 1].Value = loLstFamiliaBean[i].id.ToString();

                }
            }
            else
            {
                cboFamilia.Items.Insert(0, "Todos");
                cboFamilia.Items[0].Value = "0";
            }
        }
        [WebMethod]
        public static String cargarMapaPedido(String fini, String ffin, String vendedor, String FlagEnCobertura, String Bonificacion, String codperfil, String codgrupo, String FlagTipoPedido)
        {
            String tipoArticulo = "";
            if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                tipoArticulo = "PRE";
            }
            else
            {
                tipoArticulo = "PRO";
            }
            List<ReportePedidoBean> list = ReporteController.ListaCordenadaPedido(vendedor, Utils.getStringFechaYYMMDD(fini),
                Utils.getStringFechaYYMMDD(ffin), FlagEnCobertura, Bonificacion,
                FlagTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);

            return JSONUtils.serializeToJSON(list);
        }
        [WebMethod]
        public static String MostarVendedoresPorGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Equals("0"))
            {
                List<UsuarioBean> lstUsuarioBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(pkusu));
                if (lstUsuarioBean.Count != 0)
                {
                    Rpta += ToolBox.Option("-1", "", "Todos");
                    for (int i = 0; i < lstUsuarioBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstUsuarioBean[i].id.ToString(), "", lstUsuarioBean[i].nombre);

                    }
                }
            }
            else
            {
                List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresPorGrupo(Int32.Parse(codigo));
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("-1", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Usu_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    Rpta = ToolBox.Option("-1", "", "No Hay Registro.").ToString();
                }
            }

            return Rpta;
        }
        [WebMethod]
        public static String MostarGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Length == 0)
            {
                codigo = "0";
            }
            if (codigo.Equals("0"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count != 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);

                    }
                }
                else
                {
                    Rpta = ToolBox.Option("0", "", "No Hay Registro.").ToString();
                }
            }
            else
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), "SUP");
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Grupo_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {
                }
            }
            return Rpta;
        }
    }
}