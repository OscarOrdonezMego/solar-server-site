﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using Model;
using Controller;
using Model.bean;
using Controller.functions;

public partial class Reporte_pedido_GMAPSPEDIDO : PageController
{
    public String codigo;
    public static Int32 ioRespuesta;
    public String ioRespuestaString="";
    public static String id_usu = "";
    protected override void initialize()
    {
        if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
        {
            ioRespuesta = Operation.flagACIVADO.ACTIVO;
            ioRespuestaString = Operation.VACIO.TEXT_VACIO;
        }
        else
        {
            ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
            ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;

        }
       
        String perfilUsu = Session["lgn_codsupervisor"].ToString();
        if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
        {
            id_usu = Session["lgn_id"].ToString();
        }
    }
    [WebMethod]
    public static String cargarMapaPedido(String fini, String ffin, String vendedor, String FlagEnCobertura, String Bonificacion, String codperfil, String codgrupo, String FlagTipoPedido)
    {
        String tipoArticulo = "";
        if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
        {
            tipoArticulo = "PRE";
        }
        else
        {
            tipoArticulo = "PRO";
        }
        List<ReportePedidoBean> list = ReporteController.ListaCordenadaPedido(vendedor, Utils.getStringFechaYYMMDD(fini),
            Utils.getStringFechaYYMMDD(ffin), FlagEnCobertura, Bonificacion, FlagTipoPedido, 
            tipoArticulo, codperfil, codgrupo, id_usu);
        
        return JSONUtils.serializeToJSON(list); 
    }

    [WebMethod]
    public static String cargarMapaPedidoLine(String fini, String ffin, String vendedor, String FlagEnCobertura, String Bonificacion, String codperfil, String codgrupo, String FlagTipoPedido)
    {
        String tipoArticulo = "";
        if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
        {
            tipoArticulo = "PRE";
        }
        else
        {
            tipoArticulo = "PRO";
        }

        List<ReportePedidoBean> list = ReporteController.ListaVendedorPedido(vendedor, Utils.getStringFechaYYMMDD(fini),
            Utils.getStringFechaYYMMDD(ffin), FlagEnCobertura, Bonificacion, FlagTipoPedido, 
            tipoArticulo, codperfil, codgrupo, id_usu);

        ReportePedidoBean bean;

        List<ReportePedidoBean> listaCoordenada=new List<ReportePedidoBean>();

        for (Int32 i = 0; i < list.Count; i++)
        {
            bean = new ReportePedidoBean();
            bean.Coordenda = BuscarCoordenadaVendedor(fini, ffin, list[i].usr_pk, FlagEnCobertura, Bonificacion, codperfil, codgrupo, FlagTipoPedido);
            if (bean.Coordenda.Equals("[{lat:0,lng:0}]"))
            {

            }
            else
            {
                listaCoordenada.Add(bean);
            }         
        }

        return JSONUtils.serializeToJSON(listaCoordenada);
    }
    private static String BuscarCoordenadaVendedor(String fini, String ffin, String vendedor, String FlagEnCobertura, String Bonificacion, String codperfil, String codgrupo, String FlagTipoPedido)
    {
        String tipoArticulo = "";
        if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
        {
            tipoArticulo = "PRE";
        }
        else
        {
            tipoArticulo = "PRO";
        }
        List<ReportePedidoBean> listaCordenada = ReporteController.ListaCoordenadaVendedorPedido(vendedor, 
            Utils.getStringFechaYYMMDD(fini),
            Utils.getStringFechaYYMMDD(ffin), 
            FlagEnCobertura, Bonificacion, FlagTipoPedido, tipoArticulo, codperfil, codgrupo, id_usu);
        
        String coordenada = "[";
        String coordenadaFinal = "";
        for (Int32 i = 0; i < listaCordenada.Count; i++)
        {
            if (listaCordenada[i].latitud.Equals("0") && listaCordenada[i].longitud.Equals("0"))
            {

            }
            else
            {
                coordenada += "{lat:" + listaCordenada[i].latitud + ",lng:" + listaCordenada[i].longitud + "},";
            }
        }
        if (coordenada.Equals("["))
        {

        }
        else
        {
            coordenadaFinal = coordenada.Substring(0, coordenada.Length - 1) + "]";
        }
        
        return coordenadaFinal;
    }
  

}