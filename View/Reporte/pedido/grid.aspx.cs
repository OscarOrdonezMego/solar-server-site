﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;
using Controller.functions;

namespace Pedidos_Site.Reporte.pedido
{
    public partial class Reporte_asistencia_grid : PageController
    {
        public string Mcv = "";
        private String valorDireccionDespacho = "F";
        public String visibleDireccionDespacho = "";
        private String valorConfigMostrarAlmacen = "F";
        public String visibleColumnaAlmacen = "";
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;

            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            String fcobertura = dataJSON["FlagEnCobertura"].ToString();
            String fBonificacion = dataJSON["Bonificacion"].ToString();
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;

            String vendedor = dataJSON["vendedor"].ToString();
            String fTipoPedido = dataJSON["FlagTipoPedido"].ToString();
            String tipoArticulo = "";
            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();

            String flgfecreg = dataJSON["Flagfecreg"].ToString();

            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }

            Mcv = ManagerConfiguration.mostrar_condvta;

            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_DESPACHO, out this.valorDireccionDespacho);
            this.visibleDireccionDespacho = (this.valorDireccionDespacho == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN, out this.valorConfigMostrarAlmacen);
            this.visibleColumnaAlmacen = (this.valorConfigMostrarAlmacen == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

            if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                tipoArticulo = "PRE";
            }
            else
            {
                tipoArticulo = "PRO";
            }

            PaginateReportePedidoBean paginate = ReporteController.paginarPedidoBuscar(vendedor, fini, ffin,
                fcobertura, fBonificacion, pagina, filas, fTipoPedido, tipoArticulo, codperfil,
                codgrupo, id_usu, flgfecreg);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ReportePedidoBean> lst = paginate.lstResultados;

                if (lst[0].FlgDireccionDespacho == Model.Enumerados.FlagHabilitado.T.ToString())
                    this.visibleDireccionDespacho = "";
                if (lst[0].FLG_MOSTRAR_ALMACEN == Model.Enumerados.FlagHabilitado.T.ToString())
                    this.visibleColumnaAlmacen = "";

                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }


        protected String isExiste(String lat, String lng)
        {
            if (lat != "0" || lng != "0")
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
    }
}