﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;

namespace Pedidos_Site.Reporte.pedido
{
    public partial class Reporte_asistencia_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');

            lblNomUsuario.Text = dDato[1].ToString();
            lblFechaInicio.Text = dDato[2].ToString();
            lblFechaFin.Text = dDato[3].ToString();
            lblCondicionVta.Text = dDato[5].ToString();
            lblCliente.Text = dDato[4].ToString();
            lblMonto.Text = dDato[6].ToString();
            lblObservacion.Text = dDato[12].ToString();
        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {

                String[] xdatos = codigo.Split('|');

                String xlat = xdatos[7].ToString();
                String xlon = xdatos[8].ToString();
                String xusu = xdatos[1].ToString();
                String xini = xdatos[2].ToString();
                String xfin = xdatos[3].ToString();
                String xped = xdatos[0].ToString();
                String xcli = xdatos[4].ToString();
                String xcvta = xdatos[5].ToString();
                String xmon = xdatos[6].ToString();
                String xobs = xdatos[12].ToString();


                List<MapBean> lst = new List<MapBean>();


                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;

                be.msg = "<h4>" + "Pedido Total:" + "</h4>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUMPEDIDOUPPERCASE) + ": " + xped + "</br> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + xusu + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHAINICIO) + ": " + xini + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHAFIN) + ": " + xfin + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE) + ": " + xcli + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CONDVENTAUPPERCASE) + ": " + xcvta + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_MONTO) + ": " + xmon + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_OBSERVACION) + ": " + xobs;

                be.img = "../../images/gps/pedido.png";

                be.titulo = "Pedido Total";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }


        }
    }
}