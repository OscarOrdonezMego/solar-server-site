﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Controller.functions;
using System.Data;
using Model;

public partial class pedidodet : PageController
{
    public Boolean bonificacion = false;
    public string Mbm = "";
    public static Int32 ioRespuesta;
    public static String ioRespuestaString;

    protected override void initialize()
    {
        if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
        {
            ioRespuesta = Operation.flagACIVADO.ACTIVO;
            ioRespuestaString = Operation.VACIO.TEXT_VACIO;
        }
        else
        {
            ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
            ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
        }

        String xPedido = "";
        xPedido = Request.QueryString["idpedido"].ToString();
        Mbm = ManagerConfiguration.nBonificacionManual;
        DataTable odt ;
        if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
        {
            odt = ReporteController.PedidoDetallePresentacionList(xPedido);
        }
        else
        {
            odt = ReporteController.PedidoDetalleList(xPedido);
        }        

        foreach (DataRow xrow in odt.Rows)
        {          
            String valuesBon = xrow["TIENE_BONIFICACION"].ToString();
            if (!xrow["TIENE_BONIFICACION"].ToString().Equals("0"))
            {
                bonificacion = true;
            }
            else
            {
                bonificacion = false;
            }
        }      

        grdMant.DataSource = odt;
        grdMant.DataBind();
    }    
}