﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Reporte_pedido_GMAPSPEDIDO" Codebehind="GMAPSPEDIDO.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        #map img
        {
            max-width: none;
        }
        #map label
        {
            width: auto;
            display: inline;
        }
    </style>
    <script>

        var markersArray = [];
        $(document).ready(function () {
            $(document).ajaxStop(function () {
                /* your code here ?*/
            });
            initializePedido(-12.044189, -77.061737);
            //cargaPointsPedido('GMAPSPEDIDO.aspx/cargarMapaPedido', false);
            cargaPointsWithLabelPedido('GMAPSPEDIDO.aspx/cargarMapaPedido', false);

            $('#VER_MAPA_RUTA').click(function () {
                var urlMp = 'GMAPSPEDIDO.aspx/cargarMapaPedidoLine';
                $.ajax({
                    type: 'POST',
                    url: urlMp,
                    data: JSON.stringify(getDataPedido()),
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    dataType: "json",

                    success: function (data) {
                        var obj = jQuery.parseJSON(data.d);  
                        if (obj.length != 0) {                          
                            for (i = 0; i < obj.length; i++) {                                
                                addPolilynePedido(obj[i].Coordenda, makeColor());
                            }
                            setTimeout(function () { map.setCenter(setZoom()); }, 1000);
                            setTimeout(function () { setZoom(); }, 1000);
                        }
                    },

                    error: function (xhr, status, error) {
                    }
                });              
            });
        });

        function getDataPedido() {
            var strData = getParametros();
            return strData;
        }


    </script>
   
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalContent2" runat="server">VENTAS 
            </h3>
        </div>
        <div id="map2" style="width: 100%; height: 400px">
        </div>
        <input type="button" id="VER_MAPA_RUTA" class="cz-form-content-input-button  cz-util-right"
                        value="VER RUTA" />
    </form>

</body>
</html>
