﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="pedidodet" Codebehind="pedidodet.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="<%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es&client=<%=(ConfigurationManager.AppSettings["MAPS_API_CLIENT"])%>"></script>--%>

     <script type="text/javascript" src="<%
        string url = "&";
        string client = (ConfigurationManager.AppSettings["MAPS_API_CLIENT"]);
        if (!client.Equals("")){
            client = "client=" + client;
            url += client;
        }
        string key = (ConfigurationManager.AppSettings["MAPS_API_KEY"]);
        if (!key.Equals("")){
            key = "key=" + key;
            url += key;
        }
        %><%=(ConfigurationManager.AppSettings["URL_GMAPS"])%>&sensor=false&language=es<%=url%>"></script>

</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon_report.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_REPORTEDEPEDIDOS)%></p>
                    </div>
                </div>
                <div class="cz-form-content cz-util-right cz-util-right-text">
                    <a href="javascript: history.go(-1)">
                        <img src="../../imagery/all/icons/arrow-left.png" border="0" style="margin-top: 10px;"
                            alt="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VOLVER)%>"></a>
                </div>
            </div>
            <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div class="form-gridview-data" id="divGridView" runat="server">
                                <asp:Repeater ID="grdMant" runat="server">
                                    <HeaderTemplate>
                                        <table style="width: 100%" class="grilla table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th nomb="VENDEDOR">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                                                    </th>
                                                    <%if(fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())){ %>
                                                    <th nomb="ALMACEN">
                                                        Almacen
                                                    </th>
                                                    <%} %>
                                                    <th nomb="PRODUCTO">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                                                    </th>
                                                    <th nomb="NOMBRE_PRESENTACION" <%=ioRespuestaString %>>
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                                                    </th>
                                                    <th nomb="CANTIDAD_PRESENTACION" <%=ioRespuestaString %>>
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD_PRESENTACION)%>
                                                    </th>
                                                    <th nomb="UNIDAD_FRACCIONAMIENTO" <%=ioRespuestaString %>>
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%>
                                                    </th>
                                                    <th nomb="CANTIDAD">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                                                    </th>
                                                    <th nomb="FRACCION" <%=ioRespuestaString %>>
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FRACCION)%>
                                                    </th>
                                                    <%--<th nomb="PRECIO">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIO)%>
                                                    </th>--%>

                                                    <th nomb="PRECIO_SOLES">
                                                        Precio Soles
                                                    </th>
                                                    <th nomb="PRECIO_DOLARES">
                                                        Precio Dólares
                                                    </th>
                                                    <th nomb="DESC">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESC)%>
                                                    </th>
                                                    <%--<th nomb="MONTO">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%>
                                                    </th>--%>
                                                    <th nomb="MONTO_SOLES">
                                                        Monto Soles
                                                    </th>
                                                    <th nomb="MONTO_DOLARES">
                                                        Monto Dólares
                                                    </th>
                                                    <th nomb="FLETE" <%=ioRespuestaString %>>
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FLETE)%>
                                                    </th>
                                                    <th nomb="DESCRIPCION">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                                                    </th>
                                                     <% if (bonificacion == true) 
                                                    { %>
                                                     <th nomb="BONIFICACION">
                                                        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                                                    </th>
                                                     <% } %>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%# Eval("PED_PK")%>
                                            </td>
                                             <%if(fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())){ %>
                                            <td >
                                                <%# Eval("ALM_NOMBRE")%>
                                            </td>
                                            <%} %>
                                            <td>
                                                <%# Eval("pro_nombre")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("NOMBRE_PRESENTACION")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("CANTIDAD_PRESENTACION")%>
                                            </td >
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_cantidad")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("det_cantidad_fraccion")%>
                                            </td>                                            
                                            <%--<td>
                                                <%# Eval("det_preciobase")%>
                                            </td>--%>
                                            <td>
                                                <%# Eval("det_preciobase_soles")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_preciobase_dolares")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_descuento")%>
                                            </td>
                                            <%--<td>
                                                <%# Eval("det_monto")%>
                                            </td>--%>
                                            <td>
                                                <%# Eval("det_monto_soles")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_monto_dolares")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("DET_FLETE")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_descripcion")%>
                                            </td>
                                              <% if (bonificacion == true) 
                                                    { %>
                                           <td>
                                                 <%# Eval("det_bonificacion")%>
                                            </td>
                                             <% } %>
                                        </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr class="alt">
                                            <td>
                                                <%# Eval("PED_PK")%>
                                            </td>

                                            <%if(fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())){ %>
                                            <td >
                                                <%# Eval("ALM_NOMBRE")%>
                                            </td>
                                            <%} %>
                                            <td >
                                                <%# Eval("pro_nombre")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("NOMBRE_PRESENTACION")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("CANTIDAD_PRESENTACION")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_cantidad")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("det_cantidad_fraccion")%>
                                            </td>
                                            <%--<td>
                                                <%# Eval("det_preciobase")%>
                                            </td>--%>
                                            <td>
                                                <%# Eval("det_preciobase_soles")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_preciobase_dolares")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_descuento")%>
                                            </td>
                                            <%--<td>
                                                <%# Eval("det_monto")%>
                                            </td>--%>
                                            <td>
                                                <%# Eval("det_monto_soles")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_monto_dolares")%>
                                            </td>
                                            <td <%=ioRespuestaString %>>
                                                <%# Eval("DET_FLETE")%>
                                            </td>
                                            <td>
                                                <%# Eval("det_descripcion")%>
                                            </td>
                                              <% if (bonificacion == true) 
                                                    { %>
                                            <td>
                                                 <%# Eval("det_bonificacion")%>
                                            </td>
                                             <% } %>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </tbody> </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>