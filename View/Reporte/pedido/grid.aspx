﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Reporte.pedido.Reporte_asistencia_grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 16/04/2015 Se muestra Tipo Pedido y Dirección Despacho
@002 GMC 17/04/2015 Ajustes para validar visualización de Dirección Despacho
@003 GMC 05/05/2015 Ajustes para validar visualización de Almacén
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                             <th nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th nomb="FECHAINICIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_INICIO)%>
                            </th>
                            <th nomb="FECHAFIN">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FIN)%>
                            </th>
                            <th  nomb="CODIGOCLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_CLIENTE)%>
                            </th>
                            <th nomb="CLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                             <th  nomb="DIRECCION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%>
                            </th>
                            <%--@001 I--%>
                            <th nomb="DIR_DESPACHO" <%= visibleDireccionDespacho %> > <%--@002 I/F--%>
                                Dirección Despacho
                            </th>
                            <%--@001 F--%>
                            <%--@003 I--%>
                            <th nomb="ALM_NOMBRE" <%= visibleColumnaAlmacen %> >
                                Almacén
                            </th>
                            <%--@003 F--%>
                            <% if(Mcv == "1")
                              { %>
                             <th  nomb="CONDICION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CONDICION)%>
                            </th>
                            <% } %>
                            <%--@001 I--%>
                            <th  nomb="TIPO_PEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                            </th>
                            <%--@001 F--%>
                            <%--<th  nomb="MONTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%>
                            </th>--%>
                            <th  nomb="MONTO _SOLES">
                                Monto Soles
                            </th>
                            <th  nomb="MONTO_DOLARES">
                                Monto Dólares
                            </th>
                            <th  nomb="MONTO">
                                Total Flete
                            </th>
                             <th  nomb="OBS">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_OBSERVACION)%>
                            </th>
                             <th  nomb="DISTANCIA">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DISTANCIA)%>
                            </th>
                          
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GPS)%>
                            </th>
                             <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DETALLE)%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecfin")%>
                    </td>
                    <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                    <td>
                        <%# Eval("direccion")%>
                    </td>
                    <%--@001 I--%>
                    <td <%= visibleDireccionDespacho %> > <%--@002 I/F--%>
                        <%# Eval("PED_DIRECCION_DESPACHO")%>
                    </td>
                    <%--@001 F--%>
                    <%--@003 I--%>
                    <td <%= visibleColumnaAlmacen %> >
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <%--@003 F--%>
                    <% if(Mcv == "1"){ %>
                    <td>
                        <%# Eval("condvta_nombre")%>
                    </td>
                    <% } %>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("TIPO_NOMBRE")%>
                    </td>
                    <%--@001 F--%>
                    <%--<td>
                        <%# Eval("ped_montototal")%>
                    </td>--%>
                    <td>
                        <%# Eval("MontoTotalSoles")%>
                    </td>
                    <td>
                        <%# Eval("MontoTotalDolares")%>
                    </td>
                    <td>
                        <%# Eval("TOTAL_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("observacion")%>
                    </td>
                     <td>
                        <%# Eval("distancia")%>
                    </td>

                    <td>
                          <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("nom_usuario") %>|<%# Eval("ped_fecinicio") %>|<%# Eval("ped_fecfin") %>|<%# Eval("cli_nombre") %>|<%# Eval("condvta_nombre") %>|<%# Eval("ped_montototal") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>|<%# Eval("cli_lat") %>|<%# Eval("cli_lon") %>|<%# Eval("direccion") %>|<%# Eval("observacion") %>|<%# Eval("totalBonificacion") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>
                    </td>
                    <td>
                      
                             <a  style="cursor: pointer;" 
                             href="pedidodet.aspx?idpedido=<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle" alt="Detalle" /></a>

                    </td>                   
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("nom_usuario")%>
                    </td>
                       <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecinicio")%>
                    </td>
                    <td>
                        <%# Eval("ped_fecfin")%>
                    </td>
                    <td>
                        <%# Eval("cli_codigo")%>
                    </td>
                    <td>
                        <%# Eval("cli_nombre")%>
                    </td>
                      <td>
                        <%# Eval("direccion")%>
                    </td>
                    <%--@001 I--%>
                    <td <%= visibleDireccionDespacho %> > <%--@002 I/F--%>
                        <%# Eval("PED_DIRECCION_DESPACHO")%>
                    </td>
                    <%--@001 F--%>
                    <%--@003 I--%>
                    <td <%= visibleColumnaAlmacen %> >
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <%--@003 F--%>
                    <% if (Mcv == "1")
                       { %>
                    <td>
                        <%# Eval("condvta_nombre")%>
                    </td>
                    <% } %>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("TIPO_NOMBRE")%>
                    </td>
                    <%--@001 F--%>
                    <%--<td>
                        <%# Eval("ped_montototal")%>
                    </td>--%>
                    <td>
                        <%# Eval("MontoTotalSoles")%>
                    </td>
                    <td>
                        <%# Eval("MontoTotalDolares")%>
                    </td>
                    <td>
                        <%# Eval("TOTAL_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("observacion")%>
                    </td>

                     <td>
                        <%# Eval("distancia")%>
                    </td>
                 
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer; display:<%# isExiste(Eval("LATITUD").ToString(), Eval("LONGITUD").ToString())%>" class="detGPS form-icon"
                            cod="<%# Eval("ID") %>|<%# Eval("nom_usuario") %>|<%# Eval("ped_fecinicio") %>|<%# Eval("ped_fecfin") %>|<%# Eval("cli_nombre") %>|<%# Eval("condvta_nombre") %>|<%# Eval("ped_montototal") %>|<%# Eval("latitud") %>|<%# Eval("longitud") %>|<%# Eval("cli_lat") %>|<%# Eval("cli_lon") %>|<%# Eval("direccion") %>|<%# Eval("observacion") %>|<%# Eval("totalBonificacion") %>">
                            <img src="../../imagery/all/icons/punto.png" border="0" title="GPS" alt="GPS" /></a>

                           
                    </td>
                     <td>
                       <a  style="cursor: pointer;" 
                             href="pedidodet.aspx?idpedido=<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle" alt="Detalle" /></a>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" alt="" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>