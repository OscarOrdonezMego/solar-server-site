﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.Services;
using Model.bean;
using Controller.functions;

namespace Pedidos_Site.Reporte.devolucion
{
    public partial class Reporte_asistencia_GMAPS : Page
    {
        public string codigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            codigo = "'" + dataJSON["codigo"] + "'";
            String[] dDato = codigo.Split('|');

            lblNomUsuario.Text = dDato[1].ToString();
            lblCliente.Text = dDato[2].ToString();
            lblProducto.Text = dDato[3].ToString();
            lblCantidad.Text = dDato[4].ToString();
            lblMotivo.Text = dDato[5].ToString();
            lblVencimiento.Text = dDato[6].ToString();
        }

        [WebMethod]
        public static string cargarMapa(String codigo)
        {

            try
            {

                String[] xdatos = codigo.Split('|');

                String xdev = xdatos[0].ToString();
                String xusu = xdatos[1].ToString();
                String xcli = xdatos[2].ToString();
                String xpro = xdatos[3].ToString();
                String xcan = xdatos[4].ToString();
                String xmot = xdatos[5].ToString();
                String xven = xdatos[6].ToString();
                String xobs = xdatos[7].ToString();
                String xlat = xdatos[8].ToString();
                String xlon = xdatos[9].ToString();

                List<MapBean> lst = new List<MapBean>();

                MapBean be = new MapBean();
                be.latitud = xlat;
                be.longitud = xlon;
                be.msg = "<h4>" + "Devolución" + "</h4> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUMDEVOLUCIONUPPERCASE) + ": " + xdev + "</br> " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_USUARIO) + ": " + xusu + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE) + ": " + xcli + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_FECHA_VENCIMIENTO) + ": " + xven + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO) + ": " + xmot + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_PRODUCTO) + ": " + xpro + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CANTIDAD) + ": " + xcan + "</br>" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_OBSERVACION) + ": " + xobs;

                be.img = "../../images/gps/devolucion.png";

                be.titulo = "Devolución";

                lst.Add(be);

                return JSONUtils.serializeToJSON(lst);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}