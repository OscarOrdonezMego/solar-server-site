﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Reporte.marcaproducto.Reporte_marcaproducto" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 16/04/2015 Se muestra Tipo Pedido y Dirección Despacho
@002 GMC 17/04/2015 Ajustes para validar visualización de Dirección Despacho
@003 GMC 05/05/2015 Ajustes para validar visualización de Almacén
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <div class="cz-form-box-content">
           <div class="cz-form-box-content-title">
                <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/marca.png"
                    alt="<>" />
                <div id="cz-form-box-content-title-text">
                <p> 
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MARCA)%> : <span id="totalmarca" runat="server"></span><span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </p>
            </div>
        </div>
        <div class="cz-form-box-content-title">
            <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/monto_soles.png"
                alt="<>" />
            <div id="cz-form-box-content-title-text">
                <p>
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MONTO)%> (S/.) : <span id="totalmontosoles" runat="server"></span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </p>
            </div>
      </div>
      <div class="cz-form-box-content-title">
            <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/monto.png"
                alt="<>" />
            <div id="cz-form-box-content-title-text">
                <p>
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MONTO)%> ($) :  <span id="totalmontodolares" runat="server"></span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </p>
            </div>
       </div>
      <div class="cz-form-box-content-title">
            <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/cantidad.png"
                alt="<>" />
            <div id="cz-form-box-content-title-text">
            <p>
                <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CANTIDAD)%> : <span id="totalcantidad" runat="server"></span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </p>        
        </div>
      </div>
     </div>
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MARCA)%>
                            </th>
                            <th nomb="MONTO_SOLES">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%> (S/.)
                            </th>
                            <th nomb="MONTO_DOLARES">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%> ($.)
                            </th>
                             <th nomb="CANTIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("MONTOSOLES")%>
                    </td>
                    <td>
                        <%# Eval("MONTODOLARES")%>
                    </td>
                    <td>
                        <%# Eval("CANTIDAD")%>
                    </td>
                   
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("MONTOSOLES")%>
                    </td>
                    <td>
                        <%# Eval("MONTODOLARES")%>
                    </td>
                    <td>
                        <%# Eval("CANTIDAD")%>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" alt="" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
