﻿$(function () {
    var ul = $('ul');
    var array = new Array();
    var arrayErrorFilas = new Array();
    var arrayArchivosGuardados = new Array();
    var arrayColumnasConError = new Array();

    Array.prototype.remove = function (from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };
    $('#upload a').click(function () {
        $(this).parent().find('input').click();
    });
    ArrayArchivo();

    $('#Examinar').fileupload({
        dropZone: $('#upload'),
        add: function (e, data) {
            var fila = $('<li class="working"><input class="progres" type="text" value="0" data-width="48" data-height="48"' +
                'data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p class="archivo"><p class="tamanios"></p>' +
                '<p id="progressbar" class="progreso"><i class="label">Loading...</i>' +
                '</p><img class="correcto" src="../images/icons/grid/ico_grid_select.png" />' +
                '<img class="alerta" src="../images/icons/grid/ico_grid_detalle.png" />' +
                ' <img class="eliminar" src="../../imagery/all/icons/eliminar.png"/>' +
                '<div class="menalerta" style="display: none;"><div class="content-popup"><div class="cerrar"><a href="#" id="close"><img src="../images/icons/header/ico_header_salir.png"/></a></div>' +
                '<div><h1 class="data-fila">Error Columnas</h1><div class="contenedor-fila"></div><h1 class="data-error">data error</h1><div class="contenedor-data"></div></div></div></div></li>');

            var TipoArchivo = data.files[0].name;
            var txt = TipoArchivo.substring(TipoArchivo.length - 3, TipoArchivo.length);
            var tipo = "";
            if ($('[id="btnt"][name="grupo1"]').is(':checked')) {
                tipo = $('#btnt').val();
            }
            if ($('[id="btne"][name="grupo1"]').is(':checked')) {
                tipo = $('#btne').val();
            }
            var validarArchivo = BuscarNombreArchivoValido(TipoArchivo);
            if (validarArchivo == "F") {
                if (tipo == txt) {
                    fila.find('p.archivo').text(data.files[0].name);
                    fila.find('p.tamanios').text(formatFileSize(data.files[0].size));
                    data.context = fila.appendTo(ul);
                    fila.find('input.progres').knob();
                    fila.find('input.progres').trigger('configure', { 'fgColor': '#FF6702', 'inputColor': '#080808' });
                    var progressbar = fila.find("input.progres");
                    var progressLabel = fila.find("i.label");
                    var stop = 0;
                    var cambio = 0;
                    var val = "";
                    var valdata = "";
                    progressbar.progressbar({
                        value: true,
                        change: function () {
                            // progressLabel.text(progressbar.progressbar("value") + "%");
                            fila.find('input.progres').val(progressbar.progressbar("value") + "%").change();
                            /* if (progressbar.progressbar("value") == 50) {
                                 stop = 1;
                                 progressLabel.text(progressbar.progressbar("value") + "%");
                             }*/

                            var fr = new FileReader();
                            fr.onload = function (evt) {
                                var resultadoPrimeraFila = "";
                                var Resultado = evt.target.result;
                                if (cambio == 0) {
                                    progressLabel.text("Validando columnas");
                                }
                               
                                if (progressbar.progressbar("value") == 2) {
                                    val= ValidarNumeroFilas(Resultado, TipoArchivo);
                                }
                                if (progressbar.progressbar("value") == 2) {
                                    valdata = validarData(Resultado, TipoArchivo);
                                }
                                //alert(val);
                                if (val != "F") {
                                    if (progressbar.progressbar("value") == 40) {
                                        progressLabel.text("");
                                        progressLabel.append("<p>Nro de Columnas</p><b><p> Correctas</p>");
                                        cambio = 1;
                                    }
                                    if (progressbar.progressbar("value") == 46) {
                                        progressLabel.text("");
                                        progressLabel.append("<p>Validando data</p>");
                                    }
                                    if (valdata == "F") {
                                        if (progressbar.progressbar("value") == 86) {
                                            stop = 1;
                                            cambio = 1;
                                            progressLabel.text("");
                                            progressLabel.append("<p><b><i style='color:#cd0a0a;'>Error en la data</i></b></p>");
                                            fila.find('input.progres').trigger('configure', { 'fgColor': '#C00', 'inputColor': '#C00' });
                                            fila.find('img.alerta').css('display', 'block');
                                        }
                                    }

                                } else {
                                    //if (progressbar.progressbar("value") == 10) {
                                    if (progressbar.progressbar("value") == 20) {
                                        stop = 1;
                                    }                                   
                                    cambio = 1;
                                    progressLabel.text("");
                                    progressLabel.append("<p><b><i style='color:#cd0a0a;'>Error Nro de columnas</i></b></p>");
                                    fila.find('input.progres').trigger('configure', { 'fgColor': '#C00', 'inputColor': '#C00' });
                                    fila.find('img.alerta').css('display', 'block');
                                    //}
                                    //  progressbar.css({ 'background': 'red' });
                                    //progressbar.progressbar.find('.ui-widget-header').css({ 'background-color': 'red' });
                                    // fila.find('input.progres').trigger('configure', { 'fgColor': '#C00', 'inputColor': '#C00' });
                                    /* tpl.find('#detalle').css('display', 'block');
                                     data.context.addClass('error');*/
                                }

                                /*ValidarNumeroFilas(Resultado, TipoArchivo);
                                //  alert(arrayFilas[0].archivo + " " + arrayFilas[0].fila);
                                // alert(arrayErrorFilas[0].archivo + " " + arrayErrorFilas[0].filaCorrecta + " " + arrayErrorFilas[0].filaError);
                                resultadoPrimeraFila = Resultado.substr(1, Resultado.indexOf("\n"));
                                var columnas = 0;
                                if (resultadoPrimeraFila == "") {
                                    columnas = Resultado.split("|");
                                    validarColumnas(Resultado);
                                } else {
                                    columnas = resultadoPrimeraFila.split("|");
                                }
                                if (cambio == 0) {
                                    progressLabel.text("Validando columnas");
                                }
                                var val = validarNombreTamColumna(TipoArchivo, columnas.length)
                                if (val == "T") {
                                    if (progressbar.progressbar("value") == 20) {
                                        progressLabel.text("");
                                        progressLabel.append("<p>Nro de Columnas</p><b><p> Correctas</p>");
                                        cambio = 1;
                                    }
                                    //alert(Resultado.substr(1, Resultado.indexOf("\n")));

                                    data.context.addClass('success');
                                    tpl.find('#detalle').css('display', 'none');
                                } else {
                                    stop = 1;
                                    //  progressbar.css({ 'background': 'red' });
                                    progressLabel.text("Error ");
                                    //progressbar.progressbar.find('.ui-widget-header').css({ 'background-color': 'red' });
                                    fila.find('input.progres').trigger('configure', { 'fgColor': '#C00', 'inputColor': '#C00' });
                                    /* tpl.find('#detalle').css('display', 'block');
                                     data.context.addClass('error');
                                }*/
                            }
                            fr.readAsText(data.files[0]);

                        },
                        complete: function () {
                            progressLabel.text("");
                            progressLabel.append("<p><b><i style='color:lawngreen;'>Archivo correcto.</i></b></p>");
                            fila.find('img.correcto').css('display', 'block');
                        }
                    });

                    function progress() {
                        var val = progressbar.progressbar("value") || 0;
                        progressbar.progressbar("value", val + 2);

                        if (stop == 0) {
                            if (val < 99) {
                                setTimeout(progress, 80);
                            }
                        }
                    }

                    setTimeout(progress, 1000);
                }
            } else {
                alertify.log('El Archivo ' + data.files[0].name + ' ya existe.', 10000);
            }


            fila.find('img.eliminar').click(function () {
                fila.fadeOut(function () {
                    BorrarArchivoGuardados(fila.find('p.archivo').text());
                    BorrarArchivoInvalidos(fila.find('p.archivo').text());
                    BorrarDataInvalida(fila.find('p.archivo').text());
                    fila.remove();
                });
            });
            fila.find('img.alerta').click(function () {
                fila.fadeIn(function () {
                    fila.find('div.menalerta').fadeIn('slow');
                    var dataFilaError = ""
                    var archivoExiste = BuscarArchivoNoValido(fila.find('p.archivo').text());
                    if (archivoExiste == "T") {
                        var i = 0;
                        arrayErrorFilas.forEach(function (data) {
                            if (data.archivo == fila.find('p.archivo').text()) {
                                i = i + 1;
                                dataFilaError += "<p class='data-conte'>" + i + ": " + data.archivo + " <i style='color: #FF6702'>"+data.NombreColumna+"</i>= " + data.data + " <i style='color: #FF6702'>Tiene</i>= " + data.FilaIncorrecta + " <i style='color: #FF6702'>Columnas y debe tener=</i> " + data.FilaCorrecta + "</p>";
                            }
                        });
                    } else {
                        dataFilaError += "<p class='data-conte'>Nro de columnas correctas</p>";
                    }
                    fila.find('div.contenedor-fila').append(dataFilaError);
                    var dataColumnaError = ""
                    var columnaErrorExiste = BuscarColumnasConError(fila.find('p.archivo').text());
                    if (columnaErrorExiste == "T") {
                        var t = 0;
                        arrayColumnasConError.forEach(function (data) {
                            if (data.archivo == fila.find('p.archivo').text()) {
                                t = t + 1;
                                dataColumnaError += "<p class='data-conte'>" + t + ": " + data.archivo + " => " + data.filaerror + "</p>";
                            }
                        });
                    } else {
                        dataColumnaError += "<p class='data-conte'>La data es correcta.</p>"
                    }
                    
                    fila.find('div.contenedor-data').append(dataColumnaError);
                });
            });
            fila.find('div.cerrar').click(function () {
                fila.find('div.menalerta').fadeOut('slow');
                fila.find('p.data-conte').remove();
            });
            /* for (i = 0; i <= 100; i++) {
                 
 
                 fila.find('input.progres').val(i).change();
             }*/




        },
        progress: function (e, data) {
            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);
            //  alert(data[0].name);
            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input.progres').val('50').change();

            if (progress == 100) {
                data.context.removeClass('working');
            }
        },
        fail: function (e, data) {

        }
    });
    function validarColumnas(Resultado) {
        var resul = Resultado.split("|");
        for (i = 0; i < resul.length; i++) {
            alert(resul[i]);
        }
    }
    function validarCaracteresExtraños(texto) {
        var patt = new RegExp("$%");
        var res = patt.test(texto);
        return res;
    }
    function ValidarNumeroFilas(Resultado, Archivo) {
        var val = "";
        var filas = Resultado.split('\n');
        AddArchivos(Archivo);
        var arrayFilas = new Array();
        for (i = 0; i < filas.length; i++) {

            var obj = new Object();
            obj.archivo = Archivo;
            obj.fila = filas[i];
            if (filas[i].length != 1) {
                if (filas[i].length != 0) {
                    arrayFilas.push(obj);
                }
            }
        }

        for (t = 0; t < arrayFilas.length; t++) {
            var objeto = BuscarNombreArchivo(arrayFilas[t].archivo);
            //alert(objeto.nom + " " + arrayFilas[t].archivo + " " + objeto.tam + " " + arrayFilas[t].fila.split('|').length);
            if (objeto.nom == arrayFilas[t].archivo && objeto.tam == arrayFilas[t].fila.split('|').length) {

            } else {
                var objError = new Object();
                objError.archivo = objeto.nom;
                objError.NombreColumna = objeto.NomCol[0];
                objError.FilaCorrecta = objeto.tam;
                objError.FilaIncorrecta = arrayFilas[t].fila.split('|').length;
                objError.data = arrayFilas[t].fila.split("|")[0];
                arrayErrorFilas.push(objError);
                val = "F";
            }

        }

        return val;
    }
    function validarData(Resultado, Archivo) {
        var val = "T";
        var filas = Resultado.split('\n');
        var arrayFilas = new Array();
        for (i = 0; i < filas.length; i++) {
            var obj = new Object();
            obj.archivo = Archivo;
            obj.fila = filas[i];
            if (filas[i].length != 1) {
                if (filas[i].length != 0) {
                    arrayFilas.push(obj);
                }
            }
        }
        //recorro el total de filas 
        for (var r = 0; r < arrayFilas.length; r++) {
            var obj = BuscarNombreArchivo(arrayFilas[r].archivo);
            //recorro las columnas
            var columna = "";
            var columnaInvalida = "";
            for (var t = 0; t < obj.tam; t++) {
                columna += validarTipo(obj.Tipodata[t],obj.NomCol[t], arrayFilas[r].fila.split('|')[t]);
                columnaInvalida += validarTipoDatoMostrar(obj.Tipodata[t], arrayFilas[r].fila.split('|')[t]);
            }
            var valor = columna.split("");
            var valerror = validarColumna(valor);
            if (valerror == 1) {
                var objColErr = new Object();
                objColErr.archivo = arrayFilas[r].archivo;
                objColErr.filaerror = columnaInvalida;
                arrayColumnasConError.push(objColErr);
                val = "F";
            }
                      
        }
        
        return val;
    }
    function validarColumna(valor) {
        for (var i = 0; i < valor.length; i++) {
            if (valor[i] == 1) {
                return valor[i];
            }
        }        
    }
    function validarTipo(tipoDato, NombreCol,Dato) {
        var resultado;
        if (tipoDato == "Alfanumerico") {
            resultado = caracteres_extraños(Dato);
        }
        if (tipoDato == "Numerico") {
            if (NombreCol == "Longitud" || NombreCol == "Latitud") {
                resultado = numericoLonLat(Dato.toUpperCase());
            } else {
                resultado = numerico(Dato.toUpperCase());
            }            
        }
        if (tipoDato == "Decimal") {
            resultado = Decimal(Dato.toUpperCase());
        }
        if (tipoDato == "Date") {
            resultado = validarFormatoFecha(Dato);
        }
        return resultado;
    }
    function validarTipoDatoMostrar(tipoDato,Dato) {
        var resultado="";
        if (tipoDato == "Alfanumerico") {
            if (caracteres_extraños(Dato) == 1) {
                resultado = "<b><i style='color: #cd0a0a'>" + Dato + "</i></b>" + " <i style='color: #FF6702'>Tiene Caracteres extraños.</i><b><i style='color:#0154A0'>|</i></b>";
            } else {
                resultado = Dato + "<b><i style='color:#0154A0'>|</i></b>";
            }
        }
        if (tipoDato == "Numerico") {
            if (numerico(Dato.toUpperCase()) == 1) {
                resultado = "<b><i style='color: #cd0a0a'>" + Dato + "</i></b>" + " <i style='color: #FF6702'>Tiene que ser numerico.</i><b><i style='color:#0154A0'>|</i></b>";
            } else {
                resultado = Dato + "<b><i style='color:#0154A0'>|</i></b>";
            }
        }
        if (tipoDato == "Decimal") {
            if (Decimal(Dato.toUpperCase()) == 1) {
                resultado = "<b><i style='color: #cd0a0a'>" + Dato + "</i></b>" + " <i style='color: #FF6702'>Tiene que ser decimal.</i><b><i style='color:#0154A0'>|</i></b>";
            } else {
                resultado = Dato + "<b><i style='color:#0154A0'>|</i></b>";
            }
            
        }
        if (tipoDato == "Date") {
            if (validarFormatoFecha(Dato) == 1) {
                resultado = "<b><i style='color: #cd0a0a'>" + Dato + "</i></b>" + " <i style='color: #FF6702'>Error en el formato.</i><b><i style='color:#0154A0'>|</i></b>";
            } else {
                resultado = Dato + "<b><i style='color:#0154A0'>|</i></b>";
            }
            
        }
        return resultado;
    }
    function caracteres_extraños(texto) {
        var letras = "$#&%/'?¿!°+*{}[]<>";
        for (i = 0; i < texto.length; i++) {
            if (letras.indexOf(texto.charAt(i), 0) != -1) {
                return 1;
            }
        }
        return 0;
    }
    function numerico(texto) {
        var letras = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ#&%/'?¿!°+*{}[]$<>.-";
        for (i = 0; i < texto.length; i++) {
            if (letras.indexOf(texto.charAt(i), 0) != -1) {
                return 1;
            }
        }
        return 0;
    }
    function numericoLonLat(texto) {
        var letras = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ#&%/'?¿!°+*{}[]$<>";
        for (i = 0; i < texto.length; i++) {
            if (letras.indexOf(texto.charAt(i), 0) != -1) {
                return 1;
            }
        }
        return 0;
    }
    function Decimal(texto) {
        var letras = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ#&%/'?¿!°+*{}[]$<>";
        for (i = 0; i < texto.length; i++) {
            if (letras.indexOf(texto.charAt(i), 0) != -1) {
                return 1;
            }
        }
        return 0;
    }
    function validarFormatoFecha(campo) {
        var RegExPattern = /^\d{2,4}\-\d{1,2}\-\d{1,2}$/;
        if ((campo.match(RegExPattern)) && (campo != '')) {
            return 0;
        } else {
            return 1;
        }
    }
    function AddArchivos(Nombre) {
        var obj = new Object();
        obj.archivo = Nombre;
        arrayArchivosGuardados.push(obj);
    }
    function BuscarNombreArchivo(Nombre) {
        for (i = 0; i < array.length; i++) {
            if (array[i].nombre == Nombre) {
                var obj = new Object();
                obj.nom = array[i].nombre;
                obj.tam = array[i].columnas;
                obj.Tipodata = array[i].TipoDato;
                obj.NomCol = array[i].NombreColumna;
                return obj;
            }
        }
    }
    function BuscarNombreArchivoValido(Nombre) {
        var val = "F";
        arrayArchivosGuardados.forEach(function (data) {
            if (data.archivo == Nombre) {
                val = "T";
            }
        });
        return val;
    }
    function BorrarArchivoGuardados(Nombre) {

        for (var i = arrayArchivosGuardados.length - 1; i >= 0; i--) {
            if (arrayArchivosGuardados[i].archivo == Nombre) {
                arrayArchivosGuardados.splice(i, 1);
            }
        };
    }
    function BorrarArchivoInvalidos(Nombre) {
        for (var i = arrayErrorFilas.length - 1; i >= 0; i--) {
            if (arrayErrorFilas[i].archivo == Nombre) {
                arrayErrorFilas.splice(i, 1);
            }
        };
    }
    function BorrarDataInvalida(Nombre) {
        for (var i = arrayColumnasConError.length - 1; i >= 0; i--) {
            if (arrayColumnasConError[i].archivo == Nombre) {
                arrayColumnasConError.splice(i, 1);
            }
        };
    }
    function BuscarArchivoNoValido(Nombre) {
        var val = "F";
        arrayErrorFilas.forEach(function (data) {
            if (data.archivo == Nombre) {
                val = "T";
            }
        });
        return val;
    }
    function BuscarColumnasConError(Nombre) {
        var val = "F";
        arrayColumnasConError.forEach(function (data) {
            if (data.archivo == Nombre) {
                val = "T";
            }
        });
        return val;
    } 
    function BuscarCantColumna(Nombre) {
        for (i = 0; i < array.length; i++) {
            if (array[i].nombre == Nombre) {
                return respuesta = array[i].columnas;
            }
        }
    }
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
    function validarNombreTamColumna(Nombre, tamanio) {
        var respuesta = "F";
        for (i = 0; i < array.length; i++) {
            if (array[i].nombre == Nombre && array[i].columnas == tamanio) {
                return respuesta = "T";
            }
        }
        return respuesta;
    }
    function ArrayArchivo() {

        var obj = new Object();
        obj.nombre = "ALMACEN.txt";
        obj.columnas = 3;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Alfanumerico"];
        obj.NombreColumna = ["Codigo", "Nombre", "Dirección"];
        array[0] = obj;

        obj = new Object();
        obj.nombre = "ALMACEN_PRODUCTO_PRESENTACION.txt";
        obj.columnas = 3;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Numerico"];
        obj.NombreColumna = ["Codigo Almacen", "Codigo Pro/Pre", "Stock"];
        array[1] = obj;

        obj = new Object();
        obj.nombre = "CLIENTE.txt";
        obj.columnas = 14;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Alfanumerico",
            "Alfanumerico", "Alfanumerico", "Numerico",
            "Numerico", "Alfanumerico", "Alfanumerico",
            "Alfanumerico", "Alfanumerico", "Alfanumerico",
            "Alfanumerico", "Alfanumerico",];
        obj.NombreColumna = ["Codigo", "Nombre", "Dirección",
                             "Codigo Tipo Cliente", "Giro",
                             "Latitud", "Longitud", "Adicional 1",
                             "Adicional 2", "Adicional 3", "Adicional 4",
                             "Adicional 5","Limite de credito","Credito Utilizado"];
        array[2] = obj;

        obj = new Object();
        obj.nombre = "COBRANZA.txt";
        obj.columnas = 10;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Alfanumerico",
            "Decimal", "Decimal", "Date",
            "Alfanumerico", "Alfanumerico", "Alfanumerico","Date"];
        obj.NombreColumna = ["Codigo", "Nro: Documento", "Tipo Documento",
                             "Monto Pagado", "Monto Total", "Fecha Vencimiento",
                             "Serie de Cobranza", "Codigo Cliente",
                             "Codigo Vendedor", "Fecha de Registro"];
        array[3] = obj;

        obj = new Object();
        obj.nombre = "GENERAL.txt";
        obj.columnas = 6;
        obj.TipoDato = ["Numerico", "Alfanumerico", "Alfanumerico",
            "Alfanumerico", "Alfanumerico", "Alfanumerico"];
        obj.NombreColumna = ["Tipo de Carga", "Codigo", "Descripción",
                             "Abreviatura", "Fl_banco",
                             "Nro Documento"];
        array[4] = obj;

        obj = new Object();
        obj.nombre = "DIRECCION.txt";
        obj.columnas = 6;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Alfanumerico",
            "Numerico", "Numerico", "Alfanumerico"];
        obj.NombreColumna = ["Codigo Cliente", "Codigo Dirección", "Dirección",
                             "Latitud", "Longitud",
                             "Tipo"];
        array[5] = obj;

        obj = new Object();
        obj.nombre = "PRESENTACION.txt";
        obj.columnas = 9;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Alfanumerico",
            "Numerico", "Decimal", "Decimal",
            "Numerico", "Numerico", "Numerico"];
        obj.NombreColumna = ["Codigo Producto", "Codigo Presentación", "Nombre",
                             "Cantidad", "Unidad Francionamiento", "Precio",
                             "Stock", "Descuento Min", "Descuento Max", ""];
        array[6] = obj;

        obj = new Object();
        obj.nombre = "RUTA.txt";
        obj.columnas = 3;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Numerico"];
        obj.NombreColumna = ["Codigo Cliente", "Codigo Vendedor", "Ruta Cliente"];
        array[7] = obj;

        obj = new Object();
        obj.nombre = "VENDEDOR.txt";
        obj.columnas = 6;
        obj.TipoDato = ["Alfanumerico", "Numerico", "Alfanumerico",
                       "Numerico", "Alfanumerico", "Alfanumerico"];
        obj.NombreColumna = ["Codigo", "Login", "Nombre",
                             "Clave", "Codigo Grupo",
                             "Codigo Prefil"];
        array[8] = obj;

        obj = new Object();
        obj.nombre = "LISTAPRECIOS.txt";
        obj.columnas = 4;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Decimal",
                       "Alfanumerico"];
        obj.NombreColumna = ["Codigo Producto", "Codigo Canal", "Precio",
                             "Codigo condición de Venta"];
        array[9] = obj;

        obj = new Object();
        obj.nombre = "PRODUCTO.txt";
        obj.columnas = 4;
        obj.TipoDato = ["Alfanumerico", "Alfanumerico", "Decimal",
                       "Numerico"];
        obj.NombreColumna = ["Codigo Pro/Pre", "Nombre", "Precio",
                             "Stock"];
        array[10] = obj;
    }
});