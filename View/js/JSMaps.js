﻿var r=0;
var offset = { 'dashed': 0, 'dotted': 0 };
var markers = new Array();
var routes;
var polyline;
var infowindow= new Array();
var map;
var geocoder = null;
var polilinea = [];
var flightPath;
var infowindows = [];
var allposiciones = [];
var markersArrayTransaccioens = [];
var prev_infowindow = false;
var markersArrayWSsdr = [];

var ordenadosmarker = [];
function initialize(lat,lng)
 {

    var latlng = new google.maps.LatLng(lat,lng);
    var myOptions = {
        navigationControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        scaleControl: true,
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map"), myOptions);

    routes = new google.maps.MVCArray();
    polyline = new google.maps.Polyline({

    map: map
    , strokeColor: '#ff0000'
    , strokeWeight: 3
    , strokeOpacity: 0.2
    , clickable: false
    , icons: [{ icon: { path: 'M 0,-2 0,2', strokeColor: 'ff0000', strokeOpacity: 0.6, }, repeat: '25px' }]
    ,path: routes
    });
    
    google.maps.event.addListener(map, 'click', function(){
        for (var i = 0; i < infowindow.length-1; i++) {
            infowindow[i].close();
        }
    });
}

function markerWSsdr(data, obj) {
    allposiciones = [];
    infowindows = [];
   
    var repetidos = new Array();
    var marker = null;
    var i = 0;

    for (var j = 0; j < data.General.length; j++) {
        var objeto = {
            latitud: parseFloat(data.General[j].Latitud),
            longitud: parseFloat(data.General[j].Longitud),
            fecha: (data.General[j].Fecha),
            nombre: obj.nombre,
            telefono : obj.telefono
        };
        allposiciones.push(objeto);
      
    }
    for (i; i < data.General.length; i++) {
        var lineas = {
            lat: parseFloat(data.General[i].Latitud),
            lng: parseFloat(data.General[i].Longitud)
        };
        polilinea.push(lineas);
        var orden = i + 1;
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(data.General[i].Latitud, data.General[i].Longitud),
            label: orden.toString(),
            map: map
        });
     
        var infogeneral = '<div id="contentGeneral">' +
    '<div id="infoGeneral">' +
    '</div>' +
    '<div id="bodyContentGeneral">' +
    '<table style="width:280px;">' +
    '<tbody>' +
    '<tr>' +
    '<td>N\u00famero :</td><td>' + obj.telefono + '</td></tr>' +
    '<tr><td>Usuario :</td><td> ' + obj.nombre + '</td></tr>' +
    '<tr><td>Fecha de Captura :</td><td>' + data.General[i].Fecha + '</td></tr>' +
    '</tbody>' +
    '</table>' +
    '</div>' +
    '</div>';
        var infowindow = new google.maps.InfoWindow({ content: infogeneral });
        repetidos = buscarRepetidos(data.General[i].Latitud, data.General[i].Longitud, allposiciones);
        if (repetidos.length > 1) {
            var inf = infowindowsRepWSsdr(repetidos, allposiciones);
            infowindow = new google.maps.InfoWindow({ content: inf });
        }
        markersArrayWSsdr.push(marker);
        google.maps.event.addListener(marker, 'click', (function (marker, infogeneral, infowindow) {
            return function () {
                if( prev_infowindow ) {
                    prev_infowindow.close();
                }
                prev_infowindow = infowindow;
                infowindow.open(map, marker);
            };
        })(marker, infogeneral, infowindow));
    }
    printtracking(polilinea);
//    ordenarMarkerFecha(polilineasmarker);
}



        function printtracking(Coordenadas){
            var lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
                strokeOpacity: 1,
                scale: 2.2
            };
         flightPath = new google.maps.Polyline({
            path: Coordenadas,
            geodesic : true,
            strokeColor : '#FF0000',
            strokeOpacity : 1.0,
            strokeWeight : 2,
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '100px'
            }],
        }); 
   
        flightPath.setMap(map);
        }

        function clearFlighpath() {
            if (flightPath === undefined) {
                polilinea = [];
            } else {
                polilinea = [];
                flightPath.setMap(null);
            }
        }
        function clearinfowindow() {
            infowindows = [];
        }
    


function initializeCliente(lat,lng)
{

geocoder = new google.maps.Geocoder();
 var latlng = new google.maps.LatLng(lat,lng);
    var myOptions = {
        navigationControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        scaleControl: true,
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), myOptions);

    markerCliente= new google.maps.Marker({
			draggable: true,
			animation: google.maps.Animation.DROP,
			position: latlng,
			title: 'Punto',
            icon: '../../imagery/all/icons/punto.png'
			});

            markerCliente.setMap(map);
    google.maps.event.addListener(markerCliente, 'drag', function() {
	
        $('#txtLatitud').val(markerCliente.getPosition().lat().toString());
        $('#txtLongitud').val(markerCliente.getPosition().lng().toString());


	});

	google.maps.event.addListener(markerCliente, 'dragend', function() {
		//updateMarkerStatus('Arraste finalizado');
	//	geocodePosition(markerCli.getPosition(), markerArr[MapDiv].eolDireccion);
	});


   google.maps.event.addListener(map, 'click', function(e) {
	
    if(!markerCliente.getVisible())
    {
    
        markerCliente.setPosition(e.latLng);
        map.setCenter(e.latLng);
        markerCliente.setVisible(true);
         $('#txtLatitud').val(e.latLng.lat().toString());
        $('#txtLongitud').val(e.latLng.lng().toString());

    }

	});
	
	$("#txtLatitud" ).keyup(function( event ) {
     if($('#txtLatitud').val()!='')
     {
        markerCliente.setVisible(true);
        markerCliente.setPosition(new google.maps.LatLng($('#txtLatitud').val(),$('#txtLongitud').val()));
        map.setCenter(new google.maps.LatLng($('#txtLatitud').val(),$('#txtLongitud').val()));
     }
     if($('#txtLatitud').val()=='' && $('#txtLongitud').val()==''){
        markerCliente.setVisible(false);
     }
     })
     
     $("#txtLongitud" ).keyup(function( event ) {
     if($('#txtLongitud').val()!='')
     {
        markerCliente.setVisible(true);
        markerCliente.setPosition(new google.maps.LatLng($('#txtLatitud').val(),$('#txtLongitud').val()));
        map.setCenter(new google.maps.LatLng($('#txtLatitud').val(),$('#txtLongitud').val()));
     }
     if($('#txtLatitud').val()=='' && $('#txtLongitud').val()==''){
        markerCliente.setVisible(false);
     }
     
     })

}


 function codeAddress() {
         
        //obtengo la direccion del formulario
        var address = document.getElementById("txtDireccion").value;
        //hago la llamada al geodecoder
        geocoder.geocode( { 'address': address}, function(results, status) {
         
        //si el estado de la llamado es OK
        if (status == google.maps.GeocoderStatus.OK) {
            //centro el mapa en las coordenadas obtenidas
            map.setCenter(results[0].geometry.location);
            //coloco el marcador en dichas coordenadas
            markerCliente.setPosition(results[0].geometry.location);
           
          
                  $('#txtLatitud').val(markerCliente.getPosition().lat().toString());
                $('#txtLongitud').val(markerCliente.getPosition().lng().toString());
          
      } else {
          //si no es OK devuelvo error
          alert("No podemos encontrar la direcci&oacute;n, error: " + status);
      }
    });
  }

//function cargaPoints(urlM,polilyne)
//{ 
//   deleteMarker(); 
//   
//   $.ajax({
//            type: 'POST',
//            url: urlM,
//            data:JSON.stringify(getData()),
//            contentType: "application/json; charset=utf-8",
//            async: true,
//            cache: false,
//            dataType: "json",
//           
//            success: function (data) {
//              
//              
//              $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
//                crearPoint(objPoint,jQuery.parseJSON(data.d));
//                
//              });
//              setTimeout(function(){
//              map.setCenter(setZoom());},1000);


//            },
//            
//            error: function (xhr, status, error) {
//              
//            }
//        }); 
//        
//     
//}
function cargaPoints(urlM,polilyne)
{ 
   deleteMarker(); 
   
   $.ajax({
            type: 'POST',
            url: urlM,
            data:JSON.stringify(getData()),
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            dataType: "json",
           
            success: function (data) {                           
              $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
                crearPoint(objPoint,jQuery.parseJSON(data.d));                
              });
              
              //setTimeout(function(){ map.setCenter(setZoom()); },1000);
              setTimeout(function(){ setZoom(); },1000);        
            },
            
            error: function (xhr, status, error) {              
            }
        }); 
}



function cargaPointsUlt(urlM,polilyne)
{ 
   deleteMarker(); 
   
   $.ajax({
            type: 'POST',
            url: urlM,
            data:JSON.stringify(getData()),
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            dataType: "json",
            beforeSend: function () {

                $(".form-gridview-search").show();
            },
            success: function (data) {
              
               $(".form-gridview-search").hide();
               if(jQuery.parseJSON(data.d).length>0)
               {
                  $.each(jQuery.parseJSON(data.d), function(index, objPoint) {
                  if(objPoint.latitud!="0.0")
                  {
                    crearPoint(objPoint,jQuery.parseJSON(data.d));
                  }
                
                  });
                  setTimeout(function(){
                  map.setCenter(setZoom());},1000);

              }
              else
               {
                   addnotify("notify", "No existen puntos", "divError");
              }

            },
            
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        }); 
        
     
}

function transacciones() {
    for (var x = 0; x < markersArrayTransaccioens.length; x++) {
        var marker = markersArrayTransaccioens[x];
        marker.setAnimation(google.maps.Animation.BOUNCE);
        
    }
}
  
function crearPoint(objPoint,Data)
{
    addMarker(objPoint.latitud,objPoint.longitud,objPoint.msg,objPoint.img,Data);
}

function deleteMarker() {
    markersArrayTransaccioens = [];

if (markersArray.length>0) {
    for (var i= 0;i < markersArray.length; i++) {

         markersArray[i].setMap(null);
    }

     for (var i= 0;i < markers.length; i++) {

         markers[i].setMap(null);
    }
    
    routes = new google.maps.MVCArray();
    polyline.setPath(routes);

    while(markersArray.length > 0) {
        markersArray.pop();
     }
   while(markers.length > 0) {
        markers.pop();
    }

    markersArray.length = 0;
    markers.length=0;
    s= new Array();
    
}

}

function deleteMarkerRutaWSDR() {

    if (markersArrayWSsdr.length > 0) {
        for (var i = 0; i < markersArrayWSsdr.length; i++) {

            markersArrayWSsdr[i].setMap(null);
        }
        for (var i = 0; i < markers.length; i++) {
            if (markers[i] != undefined) {
                markers[i].setMap(null);
            }
        }


        routes = new google.maps.MVCArray();
        polyline.setPath(routes);

        while (markersArrayWSsdr.length > 0) {
            markersArrayWSsdr.pop();
        }
        while (markers.length > 0) {
            markers.pop();
        }

        markersArrayWSsdr.length = 0;
        markers.length = 0;
        infowindow = new Array();

    }
}

function deleteMarkerRuta() {

    if (markersArray.length > 0) {
        for (var i = 0; i < markersArray.length; i++) {

            markersArray[i].setMap(null);
        }
            for (var i = 0; i < markers.length; i++) {
                if (markers[i] != undefined) {
                    markers[i].setMap(null);
                }                
            }
                

        routes = new google.maps.MVCArray();
        polyline.setPath(routes);

        while (markersArray.length > 0) {
            markersArray.pop();
        }
        while (markers.length > 0) {
            markers.pop();
        }

        markersArray.length = 0;
        markers.length = 0;
        infowindow = new Array();

    }
}
function addMarker(lat,lng,info,image,obj) {


    var repetidos = new Array(); 
    var newLocation = new google.maps.LatLng(lat, lng);
    var marker = new google.maps.Marker({
        icon:image,
        position: newLocation,
        animation: google.maps.Animation.DROP,
        map: map});

    markersArrayTransaccioens.push(marker);
    markersArray.push(marker);

    var markerTransacciones = {
        latitud: lat,
        longitud: lng
    }

    markers[r++]=marker;
    map.setCenter(newLocation);
    var text = info ;
    var infowindow = new google.maps.InfoWindow({ content: text });
    repetidos = buscarRepetidos(lat,lng,obj)

    if(repetidos.length > 1 ){        
            var inf = infowindowsRep(repetidos, obj);      
        
            infowindow = new google.maps.InfoWindow({ content: inf });   
    }
        
    google.maps.event.addListener(marker, 'click', function() {
        for (var i = 0; i < infowindow.length-1; i++) {
            infowindow[i].close();
        }
        if (prev_infowindow) {
            prev_infowindow.close();
        }
        prev_infowindow = infowindow;
        infowindow.open(map,marker);
    });
    
}


function addPolilyne(lat,lng) {
    var newLocation = new google.maps.LatLng(lat, lng);
    map.setCenter(newLocation);
    routes.push(newLocation);
    polyline.setPath(routes);
    polylineTimer = setInterval(function () { animateDashed(); }, 200);

}

function buscarRepetidos(lat,lon, data){
    var arregloPuntosRepetidos = new Array(); 
    var j = 0;  
    $.each(data, function(index, objPoint) {
        if(lat==objPoint.latitud && lon== objPoint.longitud)
        {
         arregloPuntosRepetidos[j++] = index;
        }
    });
    
    
    return arregloPuntosRepetidos; 
}

function infowindowsRep(arrRep, data)
{

    var infow = ""; 
    setdataIw(data);
    infow += "<table  cellpadding='0'height='100%' align='center'><thead><tr><th>Puntos</th></tr></thead><tbody>";
    for(i=0; i < arrRep.length; i++)
    {
        var sec = arrRep[i];
         infow += "<tr><td>";
         infow += "<a href='javascript:markerRept("+  sec  +");'>"+ data[sec].titulo +"</a></td></tr>"
    }

    infow += "</tbody></table>";     
    return infow;
}
function infowindowsRepWSsdr(arrRep, allposiciones) {

    var infow = "";
    setdataIw(allposiciones);
    infow += "<table  cellpadding='0'height='100%' align='center'><thead><tr><th>Puntos</th></tr></thead><tbody>";
    for (i = 0; i < arrRep.length; i++) {

        var sec = arrRep[i];
        var orden = sec + 1;
        infow += "<tr><td>";
        infow += "<a href='javascript:markerReptWSsdr(" + sec + ");'> Puntos : " + orden + "</a></td></tr>"
    }

    infow += "</tbody></table>";

    return infow;
}
function markerReptWSsdr(sec) {

    var mnsg =  '<div id="contentGeneral">' +
    '<div id="infoGeneral">' +
    '</div>' +
    '<div id="bodyContentGeneral">' +
    '<table style="width:280px;">' +
    '<tbody>' +
    '<tr>' +
    '<td>N\u00famero :</td><td>' + allposiciones[sec].telefono + '</td></tr>' +
    '<tr><td>Usuario :</td><td> ' + allposiciones[sec].nombre + '</td></tr>' +
    '<tr><td>Fecha de Captura :</td><td>' + allposiciones[sec].fecha + '</td></tr>' +
    '</tbody>' +
    '</table>' +
    '</div>' +
    '</div>';
    var lalo = new google.maps.LatLng(allposiciones[sec].latitud, allposiciones[sec].longitud);
    var infowindow;
    infowindow = new google.maps.InfoWindow({ content: mnsg });
    if (prev_infowindow) {
        prev_infowindow.close();
    }
    prev_infowindow = infowindow;
    infowindow.open(map, markersArrayWSsdr[sec]);

}

function markerRept(sec){
    var dato = new Array();
    dato = getDataIw();
    var mnsg = dato[sec].msg;   
    var lalo = new google.maps.LatLng(dato[sec].latitud, dato[sec].longitud);    
    var infowindow;  
    infowindow = new google.maps.InfoWindow({ content: mnsg  });
        if (prev_infowindow) {
        prev_infowindow.close();
    }
    prev_infowindow = infowindow;
    infowindow.open(map,markersArray[sec]);
       
}

	

function setZoom() {
    
   //var boundbox = new google.maps.LatLngBounds();
    
   for ( var i = 0; i < markersArray.length; i++ ){
        map.setCenter(new google.maps.LatLng(markersArray[i].position.lat(), markersArray[i].position.lng()));
        //boundbox.extend(new google.maps.LatLng(markersArray[i].position.lat(),markersArray[i].position.lng()));
        }
   // map.fitBounds(boundbox);
   //return boundbox.getCenter();
 }




 
var datosIW;
function setdataIw(obj){
    datosIW =  obj;
    
}
function getDataIw(){
return datosIW;
} 



function animateDashed() {
    if (offset['dashed'] > 23) {
      offset['dashed'] = 0;
    } else {
      offset['dashed'] += 2;
    }
    var icons = polyline.get('icons');
    icons[0].offset = offset['dashed'] + 'px';
    polyline.set('icons', icons); 
    
    
}


function resizeMapModal() { 
	var center = map.getCenter(); 
	google.maps.event.trigger(map, "resize"); 
	map.setCenter(center); 
}
function setMapOnAll(map) {
    for (var i = 0; i < markersArrayWSsdr.length; i++) {
        markersArrayWSsdr[i].setMap(map);
    }
}
