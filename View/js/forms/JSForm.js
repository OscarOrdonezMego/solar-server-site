﻿function grupos() {
    $('#opciones').find('tr').live('dblclick', function (event) {

        if ($(this).hasClass('disable')) {
            console.log('tr disable dblclick');
            $(this).removeClass('disable');
            $(this).addClass('editable');
            $(this).find("input").attr('disabled', null);
        }
    });

    $(".newItem").live('click', function (event) {

        strData = new Object();
        strData.codigo = "";

        $.ajax({
            type: 'POST',
            url: 'formulario.aspx/listOpcion',
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {

                $('.okItem').hide();
                $('.newItem').hide();
                $('.editItem').hide();
                $('.deleteItem').hide();
                $('.cancelItem').hide();
                $('.saveItem').show();
                $('.cancelarItem').show();

                $('.tblOpciones').html(data.d);
                $('#txtgrupo').val('');
                $('#hvalGrupo').val('');
                $('#cbogroup').hide();
                $('#txtgroup').show();
            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

            }
        });

    });

    $(".editItem").live('click', function (event) {

        if ($('#cboGrupo').val() != null && $('#cboGrupo').val() != '-1') {

            strData = new Object();
            strData.codigo = $('#cboGrupo').val();

            $.ajax({
                type: 'POST',
                url: 'formulario.aspx/listOpcion',
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('.tblOpciones').html(data.d);

                    $('.okItem').hide();
                    $('.newItem').hide();
                    $('.editItem').hide();
                    $('.deleteItem').hide();
                    $('.cancelItem').hide();
                    $('.saveItem').show();
                    $('.cancelarItem').show();

                    $('#txtgrupo').val($('#cboGrupo option:selected').text());
                    $('#hvalGrupo').val($('#cboGrupo').val());
                    $("#opciones").tableDnD();
                    $('#cbogroup').hide();
                    $('#txtgroup').show();


                },
                error: function (xhr, status, error) {
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                }
            });



        }
    });


    $(".deleteItem").live('click', function (event) {

        if ($('#cboGrupo').val() != null && $('#cboGrupo').val() != '-1') {


            strData = new Object();
            strData.codigo = $('#cboGrupo').val();

            $.ajax({
                type: 'POST',
                url: 'formulario.aspx/delGrupos',
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#cboGrupo').html(data.d);
                    updteCombo();
                    $('#cbogroup').show();
                    $('#txtgroup').hide();

                },
                error: function (xhr, status, error) {
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                }
            });



        }
    });


    $(".cancelItem").live('click', function (event) {

        $('#dvGrupo').hide();
        $('#divControles').show();
        clearOptions();

    });


    $(".cancelarItem").live('click', function (event) {

        clearOptions();

    });


    $(".okItem").click(function (event) {
        if ($('#cboGrupo').val() != null) {

            $('#dvGrupo').hide();
            $('#divControles').show();

            $('#txtCGrupo').val($('#cboGrupo option:selected').text());
            $('#hCodGrupo').val($('#cboGrupo').val());

            var idcontrol = $('#hidControl').val();
            var ctrlControl = $('#' + idcontrol);
            ctrlControl.attr('cidgrupo', $('#hCodGrupo').val());
            ctrlControl.attr('cgrupo', $('#txtCGrupo').val());
        }
    });



    $("#btngrupo").live('click', function (event) {

        $('#dvGrupo').show();
        $('#divControles').hide();
        clearOptions();


        $.ajax({
            type: 'POST',
            url: 'formulario.aspx/listGrupos',
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,

            success: function (data) {
                $('#cboGrupo').html(data.d);
                updteCombo();
            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

            }
        });







    });

    $('#btnCancelar').live('click', function (event) {

        event.preventDefault();
        location.reload();
    });
    $("#saveReg").live('click', function (event) {
        event.preventDefault();
        guardarFormulario();
    });

    $(".saveItem").live('click', function (event) {

        var numRows = $('#opciones').find('.disable').length;

        if ($('#txtgrupo').val() != "") {
            if (numRows > 0) {

                var opciones = new Array();
                $("#opciones").find('.disable').each(function () {
                    opciones.push({ codigo: $(this).find('.txtOPCodigo').val(), nombre: $(this).find('.txtOPNombre').val() });
                });

                var strData = new Object();
                strData.codigo = $('#hvalGrupo').val();
                strData.nombre = $('#txtgrupo').val();
                strData.opciones = opciones;

                $.ajax({
                    type: 'POST',
                    url: 'formulario.aspx/saveGrupo',
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#cboGrupo').html(data.d);
                        updteCombo();
                        addnotify("notify", "Grupo Almacenado", "alert");
                        clearOptions();

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                    }
                });


                //  });
            }
            else {
                addnotify("notify", "Ingresar Opciones", "alert");

            }
        }
        else {
            addnotify("notify", "Ingresar el nombre del Grupo", "alert");

        }
    });

    $(".delRowOPC").live('click', function (event) {
        var x = $(this).parent().parent().parent().parent();
        if ($(x).find(' >tbody >tr').length > 1) {
            $(this).parent().parent().remove();
        }


    });



}


function guardarFormulario() {
    var validateItems = true;
    var vRel = true;

    $('.requerid').each(function () {
        if ($(this).val() == null || $(this).val() == "") {
            validateItems = false;
        }
    });

    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    }
    else {


        var strData = new Object();

        strData.estados = [];
        var xEstados = $('.window');
        if (xEstados.size() > 0) {




            xEstados.each(function (i) {
                var mEstado = $(this);

                var idEstado = mEstado.attr('id');
                var tituloEstado = mEstado.attr('title');
                var tipoEstado = mEstado.attr('codestado');
                console.log(idEstado.substr(1, 1));
                console.log(xEstados.size());

                var strDataEstado = new Object();
                strDataEstado.codigo = idEstado.substr(1, 1);
                strDataEstado.controles = [];

                var xControles = $(mEstado).find('.itemcontrol');

                if (xControles.size() > 0) {

                    //console.log(xControles);
                    $(xControles).each(function (i) {
                        var mControl = $(this);
                        console.log(mControl);
                        var idControl = mControl.attr('id');
                        var cetiqueta = mControl.attr('cetiqueta');
                        var cobligatorio = mControl.attr('cobligatorio');
                        var ccontrol = mControl.attr('ccontrol');

                        var cmaxcaracter = "";
                        var cidgrupo = "";
                        var cactualizar = "";
                        var ceditable = "";

                        if (ccontrol == "3") {
                            cidgrupo = mControl.attr('cidgrupo');
                        }
                        if (ccontrol == "1" || ccontrol == "4" || ccontrol == "6") {
                            cmaxcaracter = mControl.attr('cmaxcaracter');
                        }

                        if (ccontrol == "5" || ccontrol == "7") {
                            ceditable = mControl.attr('cEditable');
                        }


                        var strDataControles = new Object();
                        strDataControles.tipo = ccontrol;
                        strDataControles.max = cmaxcaracter;
                        strDataControles.obligatorio = cobligatorio;
                        strDataControles.grupo = cidgrupo;
                        strDataControles.etiqueta = cetiqueta;
                        strDataControles.editable = ceditable;
                        strDataEstado.controles.push(strDataControles);

                        //  console.log(strDataEstado.controles);

                    });
                    strData.estados.push(strDataEstado);
                    //console.log(strDataEstado);

                }
                else {
                    addnotify("notify", "Se debe agregar al menos un control al formulario " + idEstado.substr(1, 1), "registeruser");
                    vRel = false;

                }


            });




            if (vRel) {


                var etiqueta = [];
                var boolRepetido = false;
                console.log(strData.estados);
                $.each(strData.estados, function (ind, objE) {
                    $.each(objE.controles, function (index, objControl) {
                        etiqueta.push(objControl.etiqueta)


                    });

                });

                $.each(etiqueta, function (ind, etq) {

                    $.each(etiqueta, function (index, etq2) {
                        if (ind != index) {
                            if (etq == etq2) {
                                boolRepetido = true;
                            }
                        }
                    });
                });




                if (boolRepetido) {
                    addnotify("notify", "Existen etiquetas repetidas", "registeruser");
                }
                else {



                    $.ajax({
                        type: 'POST',
                        url: 'formulario.aspx/save',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        success: function (data) {
                            addnotify("notify", data.d, "registeruser");
                            $('#hidPk').val('');
                            location.reload();

                        },
                        error: function (xhr, status, error) {
                            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                        }
                    });
                }
            }



        } else {
            addnotify("notify", "Se debe crear al menos un estado", "registeruser");
        }
    }






    return "";
}


function ordenEstado(idEstado, Estados) {

    var orden = 0;
    var estado = $('.nodoInicio').attr('etapasiguiente');
    for (var i = 0; i < Estados; i++) {
        if (idEstado == estado) {
            return i + 1;
        }
        else {
            estado = $('#' + estado).attr('etapasiguiente');
        }
    }

}



function updteCombo() {
    $(".cz-form-content-input-select").change(function () {

        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });

    $(".cz-form-content-input-select").each(function () {

        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });

    $(".cz-form-content-input-select-custom").change(function () {
        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
        onChangeInputSelectCustom(this);
    });

    $(".cz-form-content-input-select-custom").each(function () {
        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });
}

function clearOptions() {
    $('.okItem').show();
    $('.newItem').show();
    $('.editItem').show();
    $('.deleteItem').show();
    $('.cancelItem').show();
    $('.saveItem').hide();
    $('.cancelarItem').hide();

    $('.tblOpciones').html('');
    $('#cbogroup').show();
    $('#txtgroup').hide();
    $('#hvalGrupo').val('');
    $('#txtgrupo').val('');
    $('#cboGrupo').val('-1');
}

function eventsForm() {

    $(".cz-menu-lateral-suboption").unbind('click')

    $('#contenedorEtapa').droppable({
        accept: ".toolboxEtapa",
        drop: function (event, ui) {


            var sel = $(this);
            var ny = event.pageY;
            var nx = event.pageX;
            var idE = ui.draggable.attr('ide');
            var titulo = "Estado" + " " + (nextEtapa + 1);
            var id = generaCod();
            if (nodoAnterior == "0")
                nodoAnterior = "-1";

            agregarEtapa(sel, id, titulo, nodoAnterior, '-2', nx, ny, idE);

            var objAnterior = $('#' + nodoAnterior);

            if (objAnterior.attr('etapasiguiente') == '-2') {
                if (nodoAnterior != '0') {
                    crearRelaciones('cn' + nodoAnterior, nodoAnterior, id, 'N', '', '');
                }
                $('#' + nodoAnterior).attr('etapasiguiente', id);
            }
            nodoAnterior = id;
            nextEtapa++;
            $('.itempropEtapa').removeClass('novisible');
        }
    });
    $('.toolboxItem').draggable({
        connectToSortable: ".item",
        helper: "clone",
        revert: "invalid"
    });

    $('.lstControles').draggable({
        /*appendTo: "body"*/

        /*  connectToSortable: "#contenedorEtapa",
           helper: "clone",
            revert: "invalid"
             containment: "#contenedorEtapa"*/
    });




    $('.removeControl').live('click', function (e) {
        e.preventDefault();
        var control = $(this).parent().parent();

        control.remove();
        $('#tControl').hide();
        $('#hidControl').val('');
    });




    $('#contenedorEtapa').delegate(".itemcontrol", "click", function () {

        $(".itemcontrol").removeClass('selected');
        $(this).addClass('selected');

        $('#dvGrupo').hide();
        $('#divControles').show();
        clearOptions();

        $('#hidControl').val($(this).attr('id'));
        var control = $(this).parent();

        setPropControl(control);

        $('#tControl').show();
        if ($('#tControl').css('height') == '42px') {

            $('#tControl').find('.cz-menu-lateral-title').trigger('click');
        }

        if ($('#plusControl').css('height') != '42px') {

            $('#plusControl').find('.cz-menu-lateral-title').trigger('click');
        }

    });



    $('#plusControl').find('.cz-menu-lateral-title').live('click', function (e) {
        console.log('x');
        if ($('#tControl').css('height') != '42px') {

            $('#tControl').find('.cz-menu-lateral-title').trigger('click');
        }
    });


    $('#tControl').find('.cz-menu-lateral-title').live('click', function (e) {
        if ($('#plusControl').css('height') != '42px') {

            $('#plusControl').find('.cz-menu-lateral-title').trigger('click');
        }
    });

    $('#txtCEtiqueta').keyup(function (e) {
        var control = $(this);
        var titulo = control.val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        ctrlControl.attr('cetiqueta', titulo);
        ctrlControl.find('.spanTitulo').text(abreviarCadena(titulo, 16));
    });

    $('#txtCMaxCaracter').keyup(function (e) {
        var control = $(this);
        var titulo = control.val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        ctrlControl.attr('cmaxcaracter', titulo);

    });

    $('#chkCObligatorio').change(function (e) {
        var s = $(this).attr('checked');
        var etiqueta = $('#txtCEtiqueta').val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        // alert(s+" "+idcontrol+" "+ctrlControl+" "+etiqueta+" "+ctrlControl.attr('cetiqueta'));
        if (s == 'checked') {
            ctrlControl.attr("cobligatorio", 'T');
        }
        else {
            ctrlControl.attr("cobligatorio", 'F');
        }
    });
    $('#chkCEditable').change(function (e) {
        var s = $(this).attr('checked');
        var etiqueta = $('#txtCEtiqueta').val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        // alert(s+" "+idcontrol+" "+ctrlControl+" "+etiqueta+" "+ctrlControl.attr('cetiqueta'));
        if (s == 'checked') {
            ctrlControl.attr("ceditable", 'T');
        }
        else {
            ctrlControl.attr("ceditable", 'F');
        }
    });
}

/*functiones*/

function setPropControl(control) {

    var controls = $(control).find('.itemcontrol').attr('ccontrol');
    $('#CActualizar').hide();


    var etiqueta = $(control).find('.itemcontrol').attr('cetiqueta');
    var maxcaracter = $(control).find('.itemcontrol').attr('cmaxcaracter');
    var obligatorio = $(control).find('.itemcontrol').attr('cobligatorio');
    var grupo = $(control).find('.itemcontrol').attr('cgrupo');
    var idgrupo = $(control).find('.itemcontrol').attr('cidgrupo');
    var editable = $(control).find('.itemcontrol').attr('ceditable');



    $('#CEtiqueta').show();
    $('#txtCEtiqueta').val(etiqueta);
    $('#CObligatorio').show();
    if (obligatorio == "T") {
        $('#chkCObligatorio').attr('checked', true);
    }
    else {
        $('#chkCObligatorio').attr('checked', false);
    }
    console.log('setPropControl ceditable=' + editable + ' controls=' + controls);
    if (editable == "T") {
        $('#chkCEditable').attr('checked', true);
    }
    else {
        $('#chkCEditable').attr('checked', false);
    }
    if (controls == "1" || controls == "4" || controls == "6") {

        $('#CMaxCaracter').show();

        $('#CGrupo').hide();

        $('#txtCMaxCaracter').val(maxcaracter);
        $('#CEditable').hide();
    }
    else if (controls == "3") {

        $('#CMaxCaracter').hide();

        $('#CGrupo').show();

        $('#hCodGrupo').val(idgrupo);
        $('#txtCGrupo').val(grupo);
        $('#CEditable').hide();
    }

    else if (controls == "2" || controls == "8") {
        $('#CMaxCaracter').hide();
        $('#CGrupo').hide();
        $('#CEditable').hide();
    }
    else if (controls == "5" || controls == "7") {
        $('#CMaxCaracter').hide();
        $('#CEditable').show();
        $('#CGrupo').hide();
    }
    else if (controls == "9") {
        $('#CMaxCaracter').hide();
        $('#CGrupo').hide();
        $('#CObligatorio').hide();
        $('#CEditable').hide();
    }
}




function insertandoControl(control) {
    ctrlSel = "1";
    var h = '';
    h = '<div class="itemcontrol" id="' + GeneradorCodigo() + '" cetiqueta="' + "Ingrese Titulo" + '" cmaxcaracter="" cobligatorio="T" ceditable="T" cactualizar="T" cidgrupo="" cgrupo="" ccontrol="' + control + '" >';
    h += '<a title="Eliminar control" class="removeControl"><img src="../../imagery/all/layout/eraser.png" /></a><div class="nombreCampo" ><span class="spanTitulo">' + "Ingrese Titulo" + '</span></div>';
    h += '<div  class="nombreCampo movilControles C' + control + '" ></div>';
    return h;
}
function insertandoControlE(control, titulo, max, obligatorio, actualizar, idgrupo, grupo,editable) {
    ctrlSel = "1";
    var h = '<div class="toolboxItem  ui-draggable" control="' + control + '" "="" style="background:white;display: block;margin-bottom: 5px;">';
    h += '<div class="itemcontrol" id="' + GeneradorCodigo() + '" cetiqueta="' + titulo + '" cmaxcaracter="' + max + '" cobligatorio="' + obligatorio + '" cactualizar="' + actualizar + '" cidgrupo="' + idgrupo + '" cgrupo="' + grupo + '" ccontrol="' + control + '" ceditable="' + editable + '" >';
    h += '<a title="Eliminar control" class="removeControl"><img src="../../imagery/all/layout/eraser.png" /></a><div class="nombreCampo" ><span class="spanTitulo">' + titulo + '</span></div>';
    h += '<div  class="nombreCampo movilControles C' + control + '" ></div>';
    h += '</div>';
    return h;
}
function agregarEtapa(sel, id, titulo, anterior, siguiente, posx, posy, idE) {

    ctrlSel = "0";

    var eAnterior = $('#' + anterior);

    var attAnterior = eAnterior.attr('etapasiguiente');

    var contenedor = $('<div class="cz-form-subcontent component window" style="z-index:-1;top:' + posy + 'px;left:' + posx + 'px;min-width: 80px;max-width: 600px;"></div>');

    var itemId = 'item' + id;
    var item = $('<div class="cz-form-subcontent-content item" style="height:auto;min-width: 80px;max-width: 600px;min-height: 70px;">&nbsp;</div>');

    contenedor.append('<div class="cz-form-subcontent-title tituloPantalla"><span class="txtEtapa">' + escapeHTML(abreviarCadena(titulo, 16)) + '</span></div></div>');
    contenedor.append(item);
    contenedor.attr("id", id);
    contenedor.attr("title", titulo);
    contenedor.attr("etapaanterior", anterior);
    contenedor.attr("etapasiguiente", siguiente);

    contenedor.attr("codEstado", idE);

    sel.append(contenedor);
    setItemDropeable(item);
    $('.txtTituloEtapa').val(titulo);
    $('#txtIdEtapa').val(id);
    agregarEndPoint(id, 1, true);
    $('.itemprop').hide();
    $('.ui-autocomplete').css("display", "none");
    contenedor.trigger('click');


}

function setItemDropeable(i) {

    i.sortable({
        placeholder: 'control-hover',
        stop: function (event, ui) {

            var s = ui.item;
            var ic;
            s.removeClass('cz-menu-lateral-suboption');
            if (s.hasClass('toolboxItem')) {
                console.log('toolboxItem sortable');
                console.log(s.children().hasClass('itemcontrol'));
                if (!s.children().hasClass('itemcontrol')) {
                    ic = $(insertandoControl(s.attr("control")));
                    s.html(ic);
                }
                else {
                    ic = $(s.children());
                }
                console.log(ic);
                ic.trigger('click');

            }




        }
    });

}




function cargarFormulario() {


    $.ajax({
        type: 'POST',
        url: 'formulario.aspx/load',
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        dataType: "json",
        success: function (data) {

            var tAct = jQuery.parseJSON(data.d);

            console.log(tAct);

            $.each(tAct, function (index, objEstado) {


                $.each(objEstado.controles, function (idx, objControl) {

                    $('#f' + objEstado.codigo).find('.ui-sortable').append(insertandoControlE(objControl.tipo, objControl.etiqueta, objControl.max, objControl.obligatorio, objControl.actControl, objControl.grupo, objControl.grupoDesc, objControl.editable));
                });


            });

        },

        error: function (xhr, status, error) {

        }
    });

}

/*function generales*/

function generaCod() {
    var myDate = new Date(); var myDate = new Date(); var dia = padL(myDate.getDate(), 2); var mes = padL((myDate.getMonth() + 1), 2); var anio = (myDate.getFullYear());
    var hora = myDate.getHours(); var minuto = myDate.getMinutes(); var segundo = myDate.getSeconds(); var milisegundo = myDate.getMilliseconds();
    alert(anio + mes + dia + hora + minuto + segundo + milisegundo);
    return (anio + mes + dia + hora + minuto + segundo + milisegundo);
}
function GeneradorCodigo() {
    var hexVal = "0123456789ABCDEF".split("");
    return hexVal.sort(function () {
        return (Math.round(Math.random()) - 0.5);
    }).slice(0, 6).join('');
}
function padL(number, length) { var str = '' + number; while (str.length < length) { str = '0' + str; } return str; }

function escapeHTML(value) {
    return value.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}
function abreviarCadena(cadena, longitud) {
    return (cadena.length > longitud) ? cadena.substring(0, longitud) + '...' : cadena;
}


function escapeComillaDoble(value) {
    return value.replace(/"/g, '&quot;');
}

/*jsplump*/
function inicioJPlumb() {

    jsPlumb.bind("ready", function () {

        jsPlumb.reset();
        document.onselectstart = function () { return false; }; 			// chrome fix.
        var resetRenderMode = function (desiredMode) {// render mode
            var newMode = jsPlumb.setRenderMode(desiredMode);
            jsPlumb.importDefaults({
                Endpoint: "Blank",
                PaintStyle: { lineWidth: 3, strokeStyle: "#ffa500", "dashstyle": "2 4" },
                HoverPaintStyle: { strokeStyle: "#056", lineWidth: 4 },
                ConnectionOverlays: [["Arrow", { location: 1, id: "arrow", length: 14, foldback: 0.8 }]]
            });

            /*   jsPlumb.bind("click", function (params) {
                   var cns = jsPlumb.getConnections({ source: params.sourceId });
                   var eTarget = $('#' + params.targetId);
                   var eSource = $('#' + params.sourceId);
                   var elementos = parseInt(eSource.attr('lista'));
                   var idEtapaSiguiente = eSource.attr('etapaanterior');
                   if (elementos > 0) {
                       elementos = elementos - 1;
                       eSource.attr('lista', elementos);
                   }
                   //console.log(params.overlays[1].id);
                   console.log(idEtapaSiguiente);
                   if (idEtapaSiguiente != "-2") {
                       eTarget.attr('etapaanterior', '-1');
                       eTarget.attr('ligado', 'F');
   
                       if (params.sourceId != 'Pantallainicio') {
                           var eSource = $('#' + params.sourceId);
                           eSource.attr('etapasiguiente', '-2');
                       }
   
                       if (params.overlays.length > 2) {
                           var idover = params.overlays[1].id;
   
                           var ctrlos = $('[inputidopcion="' + idover + '"]');
                           var divos = $('[idopcion="' + idover + '"]');
                           ctrlos.attr('inputdestinoetapa', '-2');
                           ctrlos.val('--Ir a--');
                           divos.attr('codetapa', '-2');
                           divos.attr('txtetapa', '--Ir a--');
                       }
                   }
                   jsPlumb.detach(params);
                   //for(h= 0;h<params.overlays.length;h++){
                   //   console.log(params.overlays[h].type);   
                   //}
   
               });*/
            jsPlumb.bind("jsPlumbConnection", function (conn) {
                conn.connection.setPaintStyle({ strokeStyle: "#F3641D" });
            });
        };
        resetRenderMode(jsPlumb.SVG);
    });
}


function agregarEndPoint(itemId, max, isTarget) {
    jsPlumb.draggable(jsPlumb.getSelector("#" + itemId));
    var selector = $('#' + itemId).find('.ep');
    EndPoints(selector, itemId, max);
    if (isTarget) {
        jsPlumb.makeTarget(jsPlumb.getSelector("#" + itemId), {
            beforeDrop: function (params) {
                var hecho = true;
                if (params.sourceId == 'Pantallainicio') {
                    if (params.targetId == 'Pantallafin') {
                        hecho = false;
                    }
                }
                console.log(hecho);
                if (hecho) {
                    var obj = jsPlumb.getConnections({ source: params.sourceId });
                    console.log(obj.overlays);
                    for (h = 0; h < obj.length; h++) {
                        if (obj[h].overlays.length < 3)
                            jsPlumb.detach(obj[h]);
                    }
                    //if(obj.length > 0){
                    //    jsPlumb.detach(obj[0]);
                    //}
                    var eTarget = $('#' + params.targetId);
                    eTarget.attr('etapaanterior', params.sourceId);
                    //console.log('desde: ' + params.sourceId);
                    //console.log('hasta: ' + params.targetId);
                    //if(params.sourceId != 'Pantallainicio'){
                    var eSource = $('#' + params.sourceId);
                    eSource.attr('etapasiguiente', params.targetId);
                    //}
                    return true;
                } else {
                    return false;
                }
            },
            dropOptions: { hoverClass: "dragHover" },
            anchor: "Continuous"
        });
    }
}
function EndPoints(selector, contenedor, mxconn) {
    var cod = "cn" + contenedor;
    //console.log(cod);
    jsPlumb.makeSource(selector, {
        parent: contenedor,
        anchor: "Continuous",
        connector: ["StateMachine", { curviness: 1 }],
        connectorStyle: { strokeStyle: "#F3641D", lineWidth: 4 },
        Scope: cod
    },
	    { anchor: "RightMiddle", uuid: cod }

    );
}
/*conection*/
var itemConeccion = function (d, h, o, l, i) { this.desde = d; this.hasta = h; this.esopcion = o; this.etiqueta = l; this.idopcion = i }

function getTipoConector(o, l, mid) {
    var undef;
    var stateMachineConnector;
    //console.log("getTipoConector.o="+o+"|mid="+mid+"|label="+l + " undef: " + undef);
    if (o == "N") {
        // console.log("getTipoConector.stateMachineConnector");
        stateMachineConnector = {
            connector: ["StateMachine", { curviness: 1 }],
            paintStyle: { lineWidth: 3, strokeStyle: "#056" },
            hoverPaintStyle: { strokeStyle: "#F46523" },
            endpoint: "Blank",
            anchor: "Continuous",
            curviness: 1,
            overlays: [["PlainArrow", { location: 1, width: 20, length: 12 }]]
        };
    } else if (mid != undef) {
        // console.log(mid);
        // console.log("getTipoConector.stateMachineConnector.l="+l+"|id="+mid);
        // mid = mid ? mid : escapeHTML(l);
        //console.log("stateMachineConnector.l="+l+"|id="+mid);
        stateMachineConnector = {
            connector: "StateMachine",
            paintStyle: { lineWidth: 3, strokeStyle: "#056" },
            hoverPaintStyle: { strokeStyle: "#F46523" },
            endpoint: "Blank",
            anchor: "Continuous",
            overlays: [
		            ["Label", { cssClass: "nodecomponent label", id: mid, label: l, location: 0.3 }],
		            "PlainArrow"
            ]
        };
    }
    //console.log("FIN.stateMachineConnector="+stateMachineConnector);
    return stateMachineConnector;
}
function crearRelaciones(key, anterior, siquiente, esopcion, rotulo, id) {
    //console.log(">>>crearRelaciones");
    //console.log("d: " + anterior + " h: " + siquiente);
    //console.log("<<<");
    jsPlumb.connect({
        source: anterior,
        target: siquiente//,
        //scope: key
    }, getTipoConector(esopcion, rotulo, id)
		);
}
