﻿function grupos() {
    $('#opciones').find('tr').live('dblclick', function (event) {

        if ($(this).hasClass('disable')) {
            console.log('tr disable dblclick');
            $(this).removeClass('disable');
            $(this).addClass('editable');
            $(this).find("input").attr('disabled', null);
        }
    });

    $(".newItem").live('click', function (event) {

        strData = new Object();
        strData.codigo = "";

        $.ajax({
            type: 'POST',
            url: 'formulario.aspx/listOpcion',
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {

                $('.okItem').hide();
                $('.newItem').hide();
                $('.editItem').hide();
                $('.deleteItem').hide();
                $('.cancelItem').hide();
                $('.saveItem').show();
                $('.cancelarItem').show();

                $('.tblOpciones').html(data.d);
                $('#txtgrupo').val('');
                $('#hvalGrupo').val('');
                $('#cbogroup').hide();
                $('#txtgroup').show();
            },
            error: function (xhr, status, error) {
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

            }
        });

    });

    $(".editItem").live('click', function (event) {

        if ($('#cboGrupo').val() != null && $('#cboGrupo').val() != '-1') {

            strData = new Object();
            strData.codigo = $('#cboGrupo').val();

            $.ajax({
                type: 'POST',
                url: 'formulario.aspx/listOpcion',
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('.tblOpciones').html(data.d);
                    
                    $('.okItem').hide();
                    $('.newItem').hide();
                    $('.editItem').hide();
                    $('.deleteItem').hide();
                    $('.cancelItem').hide();
                    $('.saveItem').show();
                    $('.cancelarItem').show();

                    $('#txtgrupo').val($('#cboGrupo option:selected').text());
                    $('#hvalGrupo').val($('#cboGrupo').val());
                    $("#opciones").tableDnD();
                    $('#cbogroup').hide();
                    $('#txtgroup').show();


                },
                error: function (xhr, status, error) {
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                }
            });



        }
    });


    $(".deleteItem").live('click', function (event) {

        if ($('#cboGrupo').val() != null && $('#cboGrupo').val() != '-1') {


            strData = new Object();
            strData.codigo = $('#cboGrupo').val();

            $.ajax({
                type: 'POST',
                url: 'formulario.aspx/delGrupos',
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#cboGrupo').html(data.d);
                    updteCombo();
                    $('#cbogroup').show();
                    $('#txtgroup').hide();

                },
                error: function (xhr, status, error) {
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                }
            });



        }
    });


    $(".cancelItem").live('click', function (event) {

        $('#dvGrupo').hide();
        $('#divControles').show();
        clearOptions();

    });


    $(".cancelarItem").live('click', function (event) {

        clearOptions();

    });


    $(".okItem").click(function (event) {
        if ($('#cboGrupo').val() != null) {

            $('#dvGrupo').hide();
            $('#divControles').show();

            $('#txtCGrupo').val($('#cboGrupo option:selected').text());
            $('#hCodGrupo').val($('#cboGrupo').val());

            var idcontrol = $('#hidControl').val();
            var ctrlControl = $('#' + idcontrol);
            ctrlControl.attr('cidgrupo', $('#hCodGrupo').val());
            ctrlControl.attr('cgrupo',  $('#txtCGrupo').val());
        }
    });



    $("#btngrupo").live('click', function (event) {

        $('#dvGrupo').show();
        $('#divControles').hide();
         clearOptions();


            $.ajax({
                type: 'POST',
                url: 'formulario.aspx/listGrupos',
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                
                success: function (data) {
                    $('#cboGrupo').html(data.d);
                    updteCombo();
                },
                error: function (xhr, status, error) {
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                }
            });







    });

    $("#saveReg").live('click', function (event) {
        event.preventDefault();
        guardarFormulario();
    });

    $(".saveItem").live('click', function (event) {

        var numRows = $('#opciones').find('.disable').length;

        if ($('#txtgrupo').val() != "") {
            if (numRows > 0) {

                var opciones = new Array();
                $("#opciones").find('.disable').each(function () {
                    opciones.push({ codigo: $(this).find('.txtOPCodigo').val(), nombre: $(this).find('.txtOPNombre').val() });
                });

                var strData = new Object();
                strData.codigo = $('#hvalGrupo').val();
                strData.nombre = $('#txtgrupo').val();
                strData.opciones = opciones;

                $.ajax({
                    type: 'POST',
                    url: 'formulario.aspx/saveGrupo',
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#cboGrupo').html(data.d);
                        updteCombo();
                        addnotify("notify", "Grupo Almacenado", "alert");
                        clearOptions();

                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "alert");

                    }
                });


                //  });
            }
            else {
                addnotify("notify", "Ingresar Opciones", "alert");

            }
        }
        else {
            addnotify("notify", "Ingresar el nombre del Grupo", "alert");

        }
    });

    $(".delRowOPC").live('click', function (event) {
        var x = $(this).parent().parent().parent().parent();
        if ($(x).find(' >tbody >tr').length > 1) {
            $(this).parent().parent().remove();
        }


    });


    var btnAddText = 0;

    $(".delRowAd").live('click', function (event) {
        var h= $('#div4').height();
        if (btnAddText > 0) {
          $('.adi' + btnAddText).hide();
           $('#txtAdicional' + btnAddText).removeClass('requerid');
            btnAddText = btnAddText - 1;

            
            $('#adicionales').animate({ height: (btnAddText *32) + "px" }, 300);
          //  $('#div4').animate({ height: (h-32) + "px" }, 300);
        }
    });

    $("#btnAddText").live('click', function (event) {
    var h= $('#div4').height();
        if (btnAddText < 5) {
            btnAddText = btnAddText + 1;
            $('.adi' + btnAddText).show();
            $('#txtAdicional' + btnAddText).addClass('requerid');
            console.log(h+(btnAddText *20));
            //$('#div4').animate({ height: (h+32)  + "px" }, 300);
            $('#adicionales').animate({ height: (btnAddText*32) + "px" }, 300);
        }
    });

}


function guardarFormulario() {
    var validateItems = true;
	var vRel=true;
    if ($('.cz-menu-lateral-suboption2').is(':visible')) {

        $('.requerid').each(function () {
            if ($(this).val() == null || $(this).val() == "") {
                validateItems = false;
            }
        });

        if (!validateItems) {
            addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
        }
        else {


            var strData = new Object();

            strData.id = $('#hidPk').val();
            strData.codigo = $('#txtCodigoR').val();
            strData.nombre = $('#txtNombre').val();
            strData.perfil = $('#cboPerfil').val();
            strData.adicional1 = $('#txtAdicional1').val();
            if ($('.adi2').is(':visible')) {
                strData.adicional2 = $('#txtAdicional2').val();
            }
            else {
                strData.adicional2 = "";
            }
            if ($('.adi3').is(':visible')) {
                strData.adicional3 = $('#txtAdicional3').val();
            }
            else {
                strData.adicional3 = "";
            }
            if ($('.adi4').is(':visible')) {
                strData.adicional4 = $('#txtAdicional4').val();
            }
            else {
                strData.adicional4 = "";
            }
            if ($('.adi5').is(':visible')) {
                strData.adicional5 = $('#txtAdicional5').val();
            }
            else {
                strData.adicional5 = "";
            }

            strData.estados = [];
            var xEstados = $('.window');
            if (xEstados.size() > 0) {



                var xmsj = verificarRelacionesIniFin('Pantallainicio', "Inicio","I");
                    if (xmsj != "") {

                        addnotify("notify", xmsj, "registeruser");
                        vRel= false;
						
                    }
                    else
                    {
						
						
							var xmsj = verificarRelacionesIniFin('Pantallafin', "Fin","F");
						if (xmsj != "") {

							addnotify("notify", xmsj, "registeruser");
							vRel= false;
							
						}
						else						
						{

                xEstados.each(function (i) {
                    var mEstado = $(this);

                    var idEstado = mEstado.attr('id');
                    var tituloEstado = mEstado.attr('title');
                    var tipoEstado = mEstado.attr('codestado');
                    var orden = ordenEstado(idEstado, xEstados.size());
                    console.log(idEstado);
                    console.log(xEstados.size());
                    console.log(orden);
                   

                    

                    var xmsj = verificarRelaciones(idEstado, tituloEstado);
                    if (xmsj != "") {

                        addnotify("notify", xmsj, "registeruser");
                        vRel= false;
						return false;
                    }
					else
					{
                     
                        var strDataEstado = new Object();
                        strDataEstado.codigo = idEstado;
                        strDataEstado.nombre = tituloEstado;
                        strDataEstado.orden = orden;
                        strDataEstado.tipo = tipoEstado;
                        if (tipoEstado == "1") {
                            strDataEstado.flgActEstado = "F";
                            strDataEstado.flgNoVisita = "F";
                            strDataEstado.flgAsociar = "F";
                        }
                        else if (tipoEstado == "2") {
                            strDataEstado.flgActEstado = "T";
                            strDataEstado.flgNoVisita = "F";
                            strDataEstado.flgAsociar = "F";
                        }
                        else if (tipoEstado == "3") {
                            strDataEstado.flgActEstado = "F";
                            strDataEstado.flgNoVisita = "F";
                            strDataEstado.flgAsociar = "T";
                        }
                        else if (tipoEstado == "4") {
                            strDataEstado.flgActEstado = "F";
                            strDataEstado.flgNoVisita = "T";
                            strDataEstado.flgAsociar = "F";
                        }
                        strDataEstado.controles = [];

                        var xControles = $(mEstado).find('.itemcontrol');
                        //console.log(xControles);
                        $(xControles).each(function (i) {
                            var mControl = $(this);
                            var idControl = mControl.attr('id');
                            var cetiqueta = mControl.attr('cetiqueta');
                            var cobligatorio = mControl.attr('cobligatorio');
                            var ccontrol = mControl.attr('ccontrol');

                            var cmaxcaracter = "";
                            var cidgrupo = "";
                            var cactualizar = "";

                            if (ccontrol == "CHG" || ccontrol == "COM" || ccontrol == "AUC") {
                                cidgrupo = mControl.attr('cidgrupo');
                            }
                            if (ccontrol == "ALF" || ccontrol == "NUM" || ccontrol == "DEC") {
                                cmaxcaracter = mControl.attr('cmaxcaracter');
                            }
                            if (tipoEstado == "2") {
                                cactualizar = mControl.attr('cactualizar');
                            }

                            var strDataControles = new Object();
                            strDataControles.tipo = ccontrol;
                            strDataControles.max = cmaxcaracter;
                            strDataControles.obligatorio = cobligatorio;
                            strDataControles.grupo = cidgrupo;
                            strDataControles.etiqueta = cetiqueta;
                            strDataControles.actControl = cactualizar;

                            strDataEstado.controles.push(strDataControles);

                            //  console.log(strDataEstado.controles);

                        });
                        strData.estados.push(strDataEstado);
                        //console.log(strDataEstado);
                    }



                });

					}
					}

				if(vRel){
					$.ajax({
						type: 'POST',
						url: 'formulario.aspx/saveTipo',
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						async: true,
						cache: false,
						data: JSON.stringify(strData),
						success: function (data) {
							addnotify("notify", data.d, "registeruser");
                            $('#hidPk').val('');
							location.reload();

						},
						error: function (xhr, status, error) {
							addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
						}
					});
				}

            
           
            }else
            {
               addnotify("notify", "Se debe crear al menos un estado", "registeruser");
            }
        }

    }
    else {

        addnotify("notify", "Debe ingresar al menos un campo adicional", "registeruser");
    }




    return "";
}

function ordenEstado(idEstado, Estados){

    var orden=0;
    var estado=$('.nodoInicio').attr('etapasiguiente');
    for (var i = 0; i < Estados; i++) {
        if(idEstado == estado)
        {
            return i+1;
        }
        else
        {
        estado =$('#'+estado).attr('etapasiguiente');
        }
    }

}

function verificarRelaciones(codetapa, nombre)
{
    var oTarget = jsPlumb.getConnections({target: $("#" + codetapa) });
    var oSource = jsPlumb.getConnections({source: $("#" + codetapa) });
       
    var valor = "";
    
    if(oTarget.length == 0)
    {
        valor = escapeHTML(nombre) + " " + 'Sin relacion entrada';
    }
    if(oSource.length == 0)
    {
        valor = escapeHTML(nombre) + " " + 'Sin relacion salida';
    }
    return valor;
}

function verificarRelacionesIniFin(codetapa, nombre, tipo)
{
    var oTarget = jsPlumb.getConnections({target: $("#" + codetapa) });
    var oSource = jsPlumb.getConnections({source: $("#" + codetapa) });
       
    var valor = "";
    if(tipo=="I")
    {
    
        if(oSource.length == 0)
        {
            valor = escapeHTML(nombre) + " " + 'Sin relacion salida';
        }
    }
    else
    {
     if(oTarget.length == 0)
        {
            valor = escapeHTML(nombre) + " " + 'Sin relacion entrada';
        }
        
    }
    return valor;
}

function updteCombo() {
    $(".cz-form-content-input-select").change(function () {

        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });

    $(".cz-form-content-input-select").each(function () {

        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });

    $(".cz-form-content-input-select-custom").change(function () {
        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
        onChangeInputSelectCustom(this);
    });

    $(".cz-form-content-input-select-custom").each(function () {
        $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
    });
}

function clearOptions() {
    $('.okItem').show();
    $('.newItem').show();
    $('.editItem').show();
    $('.deleteItem').show();
    $('.cancelItem').show();
    $('.saveItem').hide();
    $('.cancelarItem').hide();

        $('.tblOpciones').html('');
        $('#cbogroup').show();
        $('#txtgroup').hide();
        $('#hvalGrupo').val('');
        $('#txtgrupo').val('');
        $('#cboGrupo').val('-1');
}

function eventsForm() {

    $(".cz-menu-lateral-suboption").unbind('click')

    $('#contenedorEtapa').droppable({
        accept: ".toolboxEtapa",
        drop: function (event, ui) {

            
            var sel = $(this);
            var ny = event.pageY;
            var nx = event.pageX;
            var idE = ui.draggable.attr('ide');
            var titulo = "Estado" + " " + (nextEtapa + 1);
            var id = generaCod();
            if (nodoAnterior == "0")
                nodoAnterior = "-1";

            agregarEtapa(sel, id, titulo, nodoAnterior, '-2', nx, ny, idE);

            var objAnterior = $('#' + nodoAnterior);

            if (objAnterior.attr('etapasiguiente') == '-2') {
                if (nodoAnterior != '0') {
                    crearRelaciones('cn' + nodoAnterior, nodoAnterior, id, 'N', '', '');
                }
                $('#' + nodoAnterior).attr('etapasiguiente', id);
            }
            nodoAnterior = id;
            nextEtapa++;
            $('.itempropEtapa').removeClass('novisible');
        }
    });
    $('.toolboxItem').draggable({
        connectToSortable: ".item",
        helper: "clone",
        revert: "invalid"
    });
    $('.toolboxEtapa').draggable({
        connectToSortable: "#contenedorEtapa",
        helper: "clone",
        revert: "invalid"
    });
    $('.lstControles').draggable({
    	/*appendTo: "body"*/
			
    /*  connectToSortable: "#contenedorEtapa",
       helper: "clone",
        revert: "invalid"
         containment: "#contenedorEtapa"*/
    });


    $('.removeEtapa').live('click', function () {
        var control = $(this).parent().parent();
        
        var id = control.attr('id');
        var etapasiguiente = control.attr('etapasiguiente');
        if (etapasiguiente == "0")
            nodoAnterior = control.attr('etapaanterior');
        jsPlumb.select({ source: id }).detach();
        jsPlumb.select({ target: id }).detach();
        control.remove();
        control.addClass('novisible');
        eliminando = true;
        $('.itemprop').hide();
        $('.ui-autocomplete').css("display", "none");
        var iOption = $('[inputdestinoetapa="' + id + '"]');
        iOption.attr('inputdestinoetapa', '-2');
        iOption.val('--' + "Ir a" + '--');
        $('#tEstado').hide();
        $('#tControl').hide();
        $('#hidControl').val('');
    });

    $('.removeControl').live('click', function (e) {
        e.preventDefault();
        var control = $(this).parent().parent();
        
        control.remove();
        $('#tControl').hide();
        $('#hidControl').val('');
    });


  /*  $('#contenedorEtapa').delegate(".window", "click", function () {
        
        
            var t = $(this).attr('title');
            var c = $(this).attr('id');
            if (ctrlSel == "0") {
                $('#tEstado').show();
                $('#tControl').hide();
                $('#hidControl').val('');
                if ($('#tEstado').css('height') == '42px') {

                    $('#tEstado').find('.cz-menu-lateral-title').trigger('click');
                } 
            }
            $('#txtEstadoCodigo').val(c);
            $('#txtEstadoNombre').val(t);
        
        ctrlSel = "0"
    });*/

    $('#contenedorEtapa').delegate(".itemcontrol", "click", function () {
       // ctrlSel = "1";


        $(".itemcontrol").removeClass('selected');
        $(this).addClass('selected');
        $('#hidControl').val($(this).attr('id'));
        var control = $(this).parent();

        setPropControl(control);
        
        $('#tControl').show();
        if ($('#tControl').css('height') == '42px') {

            $('#tControl').find('.cz-menu-lateral-title').trigger('click');
        }

    });


    $('#txtEstadoNombre').keyup(function (e) {
        var control = $(this);
        var titulo = control.val();
        var idetapa = $('#txtEstadoCodigo').val(); //control.attr('codigo');
        var ctrlEtapa = $('#' + idetapa);
        ctrlEtapa.attr('title', titulo);
        ctrlEtapa.find('.txtEtapa').text(abreviarCadena(titulo, 16));
        /*var lisSelEtapa = $('.seleccionEtapa');
        lisSelEtapa.each(function () {
            var itemIdEtapa = $(this).attr('inputdestinoetapa');
            if (idetapa == itemIdEtapa)
                $(this).val(titulo);
        });*/

    });


    $('#txtCEtiqueta').keyup(function (e) {
        var control = $(this);
        var titulo = control.val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        ctrlControl.attr('cetiqueta', titulo);
        ctrlControl.find('.spanTitulo').text(abreviarCadena(titulo, 16));
    });

    $('#txtCMaxCaracter').keyup(function (e) {
        var control = $(this);
        var titulo = control.val();
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);
        ctrlControl.attr('cmaxcaracter', titulo);

    });

    $('#chkCObligatorio').change(function (e) {
        var s = $(this).attr('checked');
        
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);


        if (s == 'checked') {
            ctrlControl.attr("cobligatorio", 'T');
        }
        else {
            ctrlControl.attr("cobligatorio", 'F');
        }


    });


    $('#chkCActualizar').change(function (e) {
        var s = $(this).attr('checked');
        var idcontrol = $('#hidControl').val();
        var ctrlControl = $('#' + idcontrol);


        if (s == 'checked') {
            ctrlControl.attr("cactualizar", 'T');
        }
        else {
            ctrlControl.attr("cactualizar", 'F');
        }


    });

}

/*functiones*/

function setPropControl(control) {
   
    var controls = $(control).find('.itemcontrol').attr('ccontrol');
    $('#CActualizar').hide();
    

    var etiqueta = $(control).find('.itemcontrol').attr('cetiqueta');
    var maxcaracter = $(control).find('.itemcontrol').attr('cmaxcaracter');
    var obligatorio = $(control).find('.itemcontrol').attr('cobligatorio');
    var grupo = $(control).find('.itemcontrol').attr('cgrupo');
    var idgrupo = $(control).find('.itemcontrol').attr('cidgrupo');
    var actualizar = $(control).find('.itemcontrol').attr('cactualizar');


    

    $('#CEtiqueta').show();
    $('#txtCEtiqueta').val(etiqueta);
    $('#CObligatorio').show();

    if (obligatorio == "T") {
        $('#chkCObligatorio').attr('checked', true);
    }
    else {
        $('#chkCObligatorio').attr('checked', false);
    }

    if ($(control).parent().parent().attr('codestado') == "2") {
        $('#CActualizar').show();
        if (actualizar == "T") {
            $('#chkCActualizar').attr('checked', true);
        }
        else {
            $('#chkCActualizar').attr('checked', false);
        }
    }

   
    else if (controls == "1" || controls == "4" || controls=="6") {
        
        $('#CMaxCaracter').show();
        
        $('#CGrupo').hide();

        $('#txtCMaxCaracter').val(maxcaracter);
    }
    else if ( controls == "3") {
        
        $('#CMaxCaracter').hide();

        $('#CGrupo').show();

        $('#hCodGrupo').val(idgrupo);
        $('#txtCGrupo').val(grupo);

    }

    else if (controls == "5" || controls == "7" || controls == "2" || controls == "8") {
        
        $('#CMaxCaracter').hide();
        
        $('#CGrupo').hide();
    }
}


function iniciarNodos() {
    var html = '';
    html = '<div class="component nodoInicio"  etapasiguiente="-2" id="Pantallainicio"><div class="texto">' + "Inicio" + '<div class="ep"></div></div></div>';
    html += '<div class="component nodoFin"  id="Pantallafin"><div class="texto">' + "Fin" + '</div>';
    $('#contenedorEtapa').append(html);
    agregarEndPoint("Pantallainicio", 1, false);
    agregarEndPoint("Pantallafin", -1, true);
}

function insertandoControl(control) {
    ctrlSel = "1";
    var h = '';
    h = '<div class="itemcontrol" id="' + generaCod() + '" cetiqueta="' + "Ingrese Titulo" + '" cmaxcaracter="" cobligatorio="T" cactualizar="T" cidgrupo="" cgrupo="" ccontrol="' + control + '" >';
    h += '<a title="Eliminar control" class="removeControl"><img src="../../imagery/all/layout/eraser.png" /></a><div class="nombreCampo" ><span class="spanTitulo">' + "Ingrese Titulo" + '</span></div>';
    h += '<div  class="nombreCampo movilControles C' + control + '" ></div>';
    return h;
}
function insertandoControlE(control,titulo, max, obligatorio,actualizar,idgrupo, grupo) {
    ctrlSel = "1";
    var h = '<div class="toolboxItem  ui-draggable" control="' + control + '" "="" style="background:white;display: block;margin-bottom: 5px;">';
    h += '<div class="itemcontrol" id="' + generaCod() + '" cetiqueta="' + titulo + '" cmaxcaracter="' + max + '" cobligatorio="' + obligatorio + '" cactualizar="' + actualizar + '" cidgrupo="' + idgrupo + '" cgrupo="' + grupo + '" ccontrol="' + control + '" >';
    h += '<a title="Eliminar control" class="removeControl"><img src="../../imagery/all/layout/eraser.png" /></a><div class="nombreCampo" ><span class="spanTitulo">' + titulo + '</span></div>';
    h += '<div  class="nombreCampo movilControles C' + control + '" ></div>';
    h += '</div>';
    return h;
}
function agregarEtapa(sel, id, titulo, anterior, siguiente, posx, posy, idE) {

    ctrlSel = "0";
    var tipoEstado="Normal";
    if (idE == 2) {
        tipoEstado = "Actualizar Estado";
    }
    else if (idE == 3) {
        tipoEstado = "Asociado a Item";
    }
    else if (idE == 4) {
        tipoEstado = "No Visita";
    }


    var eAnterior = $('#' + anterior);

    var attAnterior = eAnterior.attr('etapasiguiente');

    var contenedor = $('<div class="cz-form-subcontent component window" style="z-index:-1;top:' + posy + 'px;left:' + posx + 'px;min-width: 80px;max-width: 600px;"></div>');

    var itemId = 'item' + id;
    var item = $('<div class="cz-form-subcontent-content item" style="height:auto;min-width: 80px;max-width: 600px;min-height: 70px;">&nbsp;</div>');

    contenedor.append('<div class="cz-form-subcontent-title tituloPantalla"><a title="Eliminar Estado" class="removeEtapa" style="float: right;"><img src="../../imagery/all/icons/eli.png"></a><span class="txtEtapa">' + escapeHTML(abreviarCadena(titulo, 16)) + '</span></div></div>');
    contenedor.append(item);
    contenedor.attr("id", id);
    contenedor.attr("title", titulo);
    contenedor.attr("etapaanterior", anterior);
    contenedor.attr("etapasiguiente", siguiente);

    contenedor.attr("codEstado", idE);

    sel.append(contenedor);
    setItemDropeable(item);
    $('.txtTituloEtapa').val(titulo);
    $('#txtIdEtapa').val(id);
    agregarEndPoint(id, 1, true);
    $('.itemprop').hide();
    $('.ui-autocomplete').css("display", "none");
    contenedor.trigger('click');


}

function setItemDropeable(i) {

    i.sortable({
        placeholder: 'control-hover',
        stop: function (event, ui) {

            var s = ui.item;
            var ic;
            s.removeClass('cz-menu-lateral-suboption');
            if (s.hasClass('toolboxItem')) {
                console.log('toolboxItem sortable');
                console.log(s.children().hasClass('itemcontrol'));
                if (!s.children().hasClass('itemcontrol')) {
                    ic = $(insertandoControl(s.attr("control")));
                    s.html(ic);
                }
                else {
                    ic = $(s.children());
                }
                console.log(ic);
                ic.trigger('click');

            }




        }
    });

}

function iniciarNodos() {
    var html = '';
    html = '<div class="component nodoInicio"  etapasiguiente="-2" id="Pantallainicio"><div class="texto">' + "Inicio" + '<div class="ep"></div></div></div>';
    html += '<div class="component nodoFin"  id="Pantallafin"><div class="texto">' + "Fin" + '</div>';
    $('#contenedorEtapa').append(html);
    agregarEndPoint("Pantallainicio", 1, false);
    agregarEndPoint("Pantallafin", -1, true);
}


function cargarFormulario(codFormulario) {
    if (codFormulario != "") {
        var strData = new Object();
        strData.id = codFormulario;
        $.ajax({
            type: 'POST',
            url: 'formulario.aspx/load',
            data: JSON.stringify(strData),
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            dataType: "json",
            success: function (data) {

                var tAct = jQuery.parseJSON(data.d);
                
                $('#txtCodigoR').val(tAct.codigo);
                $('#txtNombre').val(tAct.nombre);
                if (tAct.perfil != null && tAct.perfil != "") {
                    $('#cboPerfil').val(tAct.perfil);
                    updteCombo();
                }
                $('#btnAddText').trigger('click');
                $('#txtAdicional1').val(tAct.adicional1);
                if (tAct.adicional2 != "") {
                    $('#btnAddText').trigger('click');
                    $('#txtAdicional2').val(tAct.adicional2);
                }
                if (tAct.adicional3 != "") {
                    $('#btnAddText').trigger('click');
                    $('#txtAdicional3').val(tAct.adicional3);
                }
                if (tAct.adicional4 != "") {
                    $('#btnAddText').trigger('click');
                    $('#txtAdicional4').val(tAct.adicional4);
                }
                if (tAct.adicional5 != "") {
                    $('#btnAddText').trigger('click');
                    $('#txtAdicional5').val(tAct.adicional5);
                }

                var posy = 100;
                var posx = 20;
                var conteo = 0;

                $.each(tAct.estados, function (index, objEstado) {

                    var totalEst = tAct.estados.length;
                    
                    nextEtapa++;
                    var x = (posx * (conteo + 1))*10;
                    var estadoAnt = 'Pantallainicio';
                    var estadoSgt = '';
                    if (index > 0) {
                       
                        estadoAnt = tAct.estados[index - 1].codigo;
                    }
                    if (index < totalEst - 1) {
                        estadoSgt = tAct.estados[index + 1].codigo;
                    }
                    if (index == totalEst - 1) {
                        estadoSgt = 'Pantallafin';
                    }

                    if(estadoAnt=="Pantallainicio")
                    {
                        $('.nodoInicio').attr('etapasiguiente',objEstado.codigo);
                    }

                    if(conteo>2)
                    {
                        posy=posy+260;
                        x = (posx * (1))*10;
                        conteo=0;
                    }

                    
                    agregarEtapa($('#contenedorEtapa'), objEstado.codigo, objEstado.nombre, estadoAnt, estadoSgt, x, posy, objEstado.tipo);
                    crearRelaciones('cn' + objEstado.codigo, estadoAnt, objEstado.codigo, 'N', '', '');
                    if (index == totalEst - 1) {
                        crearRelaciones('cn' + estadoSgt, objEstado.codigo, estadoSgt, 'N', '', '');
                    }
                    $.each(objEstado.controles, function (idx, objControl) {
                        
                        $('#' + objEstado.codigo).find('.ui-sortable').append(insertandoControlE(objControl.tipo, objControl.etiqueta, objControl.max, objControl.obligatorio, objControl.actControl, objControl.grupo, objControl.grupoDesc));
                    });

                    conteo++;
                });
                nodoAnterior = '0';
            },

            error: function (xhr, status, error) {

            }
        });
    }
}

/*function generales*/

function generaCod() {
    var myDate = new Date(); var myDate = new Date(); var dia = padL(myDate.getDate(), 2); var mes = padL((myDate.getMonth() + 1), 2); var anio = (myDate.getFullYear());
    var hora = myDate.getHours(); var minuto = myDate.getMinutes(); var segundo = myDate.getSeconds(); var milisegundo = myDate.getMilliseconds();
    return (anio + mes + dia + hora + minuto + segundo + milisegundo);
}

function padL(number, length) { var str = '' + number; while (str.length < length) { str = '0' + str; } return str; }

function escapeHTML(value) {
    return value.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}
function abreviarCadena(cadena, longitud) {
    return (cadena.length > longitud) ? cadena.substring(0, longitud) + '...' : cadena;
}


function escapeComillaDoble(value) {
    return value.replace(/"/g, '&quot;');
}

/*jsplump*/
function inicioJPlumb() {

    jsPlumb.bind("ready", function () {
        
        jsPlumb.reset();
        document.onselectstart = function () { return false; }; 			// chrome fix.
        var resetRenderMode = function (desiredMode) {// render mode
            var newMode = jsPlumb.setRenderMode(desiredMode);
            jsPlumb.importDefaults({
                Endpoint: "Blank",
                PaintStyle: { lineWidth: 3, strokeStyle: "#ffa500", "dashstyle": "2 4" },
                HoverPaintStyle: { strokeStyle: "#056", lineWidth: 4 },
                ConnectionOverlays: [["Arrow", { location: 1, id: "arrow", length: 14, foldback: 0.8}]]
            });

         /*   jsPlumb.bind("click", function (params) {
                var cns = jsPlumb.getConnections({ source: params.sourceId });
                var eTarget = $('#' + params.targetId);
                var eSource = $('#' + params.sourceId);
                var elementos = parseInt(eSource.attr('lista'));
                var idEtapaSiguiente = eSource.attr('etapaanterior');
                if (elementos > 0) {
                    elementos = elementos - 1;
                    eSource.attr('lista', elementos);
                }
                //console.log(params.overlays[1].id);
                console.log(idEtapaSiguiente);
                if (idEtapaSiguiente != "-2") {
                    eTarget.attr('etapaanterior', '-1');
                    eTarget.attr('ligado', 'F');

                    if (params.sourceId != 'Pantallainicio') {
                        var eSource = $('#' + params.sourceId);
                        eSource.attr('etapasiguiente', '-2');
                    }

                    if (params.overlays.length > 2) {
                        var idover = params.overlays[1].id;

                        var ctrlos = $('[inputidopcion="' + idover + '"]');
                        var divos = $('[idopcion="' + idover + '"]');
                        ctrlos.attr('inputdestinoetapa', '-2');
                        ctrlos.val('--Ir a--');
                        divos.attr('codetapa', '-2');
                        divos.attr('txtetapa', '--Ir a--');
                    }
                }
                jsPlumb.detach(params);
                //for(h= 0;h<params.overlays.length;h++){
                //   console.log(params.overlays[h].type);   
                //}

            });*/
            jsPlumb.bind("jsPlumbConnection", function (conn) {
                conn.connection.setPaintStyle({ strokeStyle: "#F3641D" });
            });
        };
        resetRenderMode(jsPlumb.SVG);
    });
}


function agregarEndPoint(itemId, max, isTarget) {
    jsPlumb.draggable(jsPlumb.getSelector("#" + itemId));
    var selector = $('#' + itemId).find('.ep');
    EndPoints(selector, itemId, max);
    if (isTarget) {
        jsPlumb.makeTarget(jsPlumb.getSelector("#" + itemId), {
            beforeDrop: function (params) {
                var hecho = true;
                if (params.sourceId == 'Pantallainicio') {
                    if (params.targetId == 'Pantallafin') {
                        hecho = false;
                    }
                }
                console.log(hecho);
                if (hecho) {
                    var obj = jsPlumb.getConnections({ source: params.sourceId });
                    console.log(obj.overlays);
                    for (h = 0; h < obj.length; h++) {
                        if (obj[h].overlays.length < 3)
                            jsPlumb.detach(obj[h]);
                    }
                    //if(obj.length > 0){
                    //    jsPlumb.detach(obj[0]);
                    //}
                    var eTarget = $('#' + params.targetId);
                    eTarget.attr('etapaanterior', params.sourceId);
                    //console.log('desde: ' + params.sourceId);
                    //console.log('hasta: ' + params.targetId);
                    //if(params.sourceId != 'Pantallainicio'){
                    var eSource = $('#' + params.sourceId);
                    eSource.attr('etapasiguiente', params.targetId);
                    //}
                    return true;
                } else {
                    return false;
                }
            },
            dropOptions: { hoverClass: "dragHover" },
            anchor: "Continuous"
        });
    }
}
function EndPoints(selector, contenedor, mxconn) {
    var cod = "cn" + contenedor;
    //console.log(cod);
    jsPlumb.makeSource(selector, {
        parent: contenedor,
        anchor: "Continuous",
        connector: ["StateMachine", { curviness: 1}],
        connectorStyle: { strokeStyle: "#F3641D", lineWidth: 4 },
        Scope: cod
    },
	    { anchor: "RightMiddle", uuid: cod }

    );
}
/*conection*/
var itemConeccion = function (d, h, o, l, i) { this.desde = d; this.hasta = h; this.esopcion = o; this.etiqueta = l; this.idopcion = i }

function getTipoConector(o, l, mid) {
    var undef;
    var stateMachineConnector;
    //console.log("getTipoConector.o="+o+"|mid="+mid+"|label="+l + " undef: " + undef);
    if (o == "N") {
        // console.log("getTipoConector.stateMachineConnector");
        stateMachineConnector = {
            connector: ["StateMachine", { curviness: 1}],
            paintStyle: { lineWidth: 3, strokeStyle: "#056" },
            hoverPaintStyle: { strokeStyle: "#F46523" },
            endpoint: "Blank",
            anchor: "Continuous",
            curviness: 1,
            overlays: [["PlainArrow", { location: 1, width: 20, length: 12}]]
        };
    } else if (mid != undef) {
        // console.log(mid);
        // console.log("getTipoConector.stateMachineConnector.l="+l+"|id="+mid);
        // mid = mid ? mid : escapeHTML(l);
        //console.log("stateMachineConnector.l="+l+"|id="+mid);
        stateMachineConnector = {
            connector: "StateMachine",
            paintStyle: { lineWidth: 3, strokeStyle: "#056" },
            hoverPaintStyle: { strokeStyle: "#F46523" },
            endpoint: "Blank",
            anchor: "Continuous",
            overlays: [
		            ["Label", { cssClass: "nodecomponent label", id: mid, label: l, location: 0.3}],
		            "PlainArrow"
		        ]
        };
    }
    //console.log("FIN.stateMachineConnector="+stateMachineConnector);
    return stateMachineConnector;
}
function crearRelaciones(key, anterior, siquiente, esopcion, rotulo, id) {
    //console.log(">>>crearRelaciones");
    //console.log("d: " + anterior + " h: " + siquiente);
    //console.log("<<<");
    jsPlumb.connect({
        source: anterior,
        target: siquiente//,
        //scope: key
    }, getTipoConector(esopcion, rotulo, id)
		);
}
