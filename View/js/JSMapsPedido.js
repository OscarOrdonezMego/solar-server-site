﻿var r = 0;
var offset = { 'dashed': 0, 'dotted': 0 };
var markers = new Array();
var routes;
var polyline;
var infowindow = new Array();
var map;
var geocoder = null;

function initializePedido(lat, lng) {

    var latlng = new google.maps.LatLng(lat, lng);
    var myOptions = {
        navigationControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
        scaleControl: true,
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map2"), myOptions);
    routes = new google.maps.MVCArray();

    google.maps.event.addListener(map, 'click', function () {
        for (var i = 0; i < infowindow.length - 1; i++) {
            infowindow[i].close();
        }
    });
}

function cargaPointsPedido(urlM, polilyne) {
    deleteMarker();
    $.ajax({
        type: 'POST',
        url: urlM,
        data: JSON.stringify(getDataPedido()),
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        dataType: "json",

        success: function (data) {
            var obj = jQuery.parseJSON(data.d);
            if (obj.length != 0) {

                for (i = 0; i < obj.length; i++) {
                    if (obj[i].latitud != 0 && obj[i].longitud != 0) {
                        crearPointPedido(obj[i], '');                        
                    }
                }
                setTimeout(function () { map.setCenter(setZoom()); }, 1000);
                setTimeout(function () { setZoom(); }, 1000);
            }
        },

        error: function (xhr, status, error) {
        }
    });
}

function cargaPointsWithLabelPedido(urlM, polilyne) {
    deleteMarker();
    $.ajax({
        type: 'POST',
        url: urlM,
        data: JSON.stringify(getDataPedido()),
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        dataType: "json",

        success: function (data) {
            var obj = jQuery.parseJSON(data.d);

            if (obj.length != 0) {

                for (i = 0; i < obj.length; i++) {
                    if (obj[i].latitud != 0 && obj[i].longitud != 0) {
                        crearPointWithLabelPedido(obj[i], '');
                    }
                }
                setTimeout(function () { map.setCenter(setZoom()); }, 1000);
                setTimeout(function () { setZoom(); }, 1000);
            }
        },

        error: function (xhr, status, error) {
        }
    });
}

function crearPointPedido(objPoint, Data) {
    addMarkerPedido(objPoint.latitud, objPoint.longitud, '<p>Pedido Total</p></br>Num. Pedido: ' + objPoint.id + '</br>Usuario: ' + objPoint.nom_usuario + '</br>Fecha Inicio: ' + objPoint.ped_fecinicio + '</br>Fecha Fin: ' + objPoint.ped_fecfin + '</br>Cliente: ' + objPoint.cli_nombre + '</br>Cond. Venta: ' + objPoint.condvta_nombre + '</br>Monto Total: ' + objPoint.ped_montototal + '</br>Observación: ' + objPoint.observacion + '', '../../images/gps/ico_black-dot.png', '');
}

function crearPointWithLabelPedido(objPoint, Data) {
    addMarkerLabelPedido(objPoint.latitud, objPoint.longitud, '<p>Pedido Total</p></br>Num. Pedido: ' + objPoint.id + '</br>Usuario: ' + objPoint.nom_usuario + '</br>Fecha Inicio: ' + objPoint.ped_fecinicio + '</br>Fecha Fin: ' + objPoint.ped_fecfin + '</br>Cliente: ' + objPoint.cli_nombre + '</br>Cond. Venta: ' + objPoint.condvta_nombre + '</br>Monto Total: ' + objPoint.ped_montototal + '</br>Observación: ' + objPoint.observacion + '', '../../images/gps/ico_black-dot.png', objPoint);
}

function addMarkerLabelPedido(lat, lng, info, image, obj) {

    var estiloLabel = {
        color: "red",
        background: "white",
        fontFamily: "Arial",
        fontSize: "11px",
        fontWeight: "bold",
        textAlign: "center",
        whiteSpace: "nowrap",
        width: "40px",
        border: "2px solid black",
        textAlign: "center"
    };

    var repetidos = new Array();
    var newLocation = new google.maps.LatLng(lat, lng);

    //console.log("Punto: " + obj.COLOR);

    var marker = new MarkerWithLabel({
        //icon: image,
        position: newLocation,
        animation: google.maps.Animation.DROP,
        map: map,
        labelStyle: estiloLabel,
        labelContent: obj.COLOR,
        labelInBackground: false
    });
   
    markersArray.push(marker);

    markers[r++] = marker;
    map.setCenter(newLocation);
    var text = info;
    var infowindow = new google.maps.InfoWindow({ content: text });

    //repetidos = buscarRepetidosPedido(lat, lng, obj)
    /*
    if (repetidos.length > 1) {
        var inf = infowindowsRepPedido(repetidos, obj);
        infowindow = new google.maps.InfoWindow({ content: inf });
    }
    */
    google.maps.event.addListener(marker, 'click', function () {
        for (var i = 0; i < infowindow.length - 1; i++) {
            infowindow[i].close();
        }
        infowindow.open(map, marker);
    });
}

function addMarkerPedido(lat, lng, info, image, obj) {
    
    var repetidos = new Array();
    var newLocation = new google.maps.LatLng(lat, lng);
    var marker = new google.maps.Marker({
        icon: image,
        position: newLocation,
        animation: google.maps.Animation.DROP,
        map: map
    });

    markersArray.push(marker);

    markers[r++] = marker;
    map.setCenter(newLocation);
    var text = info;
    var infowindow = new google.maps.InfoWindow({ content: text });
    repetidos = buscarRepetidosPedido(lat, lng, obj)

    if (repetidos.length > 1) {
        var inf = infowindowsRepPedido(repetidos, obj);
        infowindow = new google.maps.InfoWindow({ content: inf });
    }
    google.maps.event.addListener(marker, 'click', function () {
        for (var i = 0; i < infowindow.length - 1; i++) {
            infowindow[i].close();
        }
        infowindow.open(map, marker);
    });
}

function makeColor() {
    var hexVal = "0123456789ABCDEF".split("");
    /**
     * Otra forma de crear un color aleatoriamente:
     *
     * for(var color = Math.floor(Math.random()*0xffffff).toString(16); color.length < 6; color = '0'+color);
     * return '#' + color;
     */
    return '#' + hexVal.sort(function () {
        return (Math.round(Math.random()) - 0.5);
    }).slice(0, 6).join('');
}

function infowindowsRepPedido(arrRep, data) {

    var infow = "";
    setdataIw(data);
    infow += "<table  cellpadding='0'height='100%' align='center'><thead><tr><th>Puntos</th></tr></thead><tbody>";
    for (i = 0; i < arrRep.length; i++) {
        var sec = arrRep[i];
        infow += "<tr><td>";
        infow += "<a href='javascript:markerRept(" + sec + ");'>" + data[sec].titulo + "</a></td></tr>"
    }

    infow += "</tbody></table>";
    return infow;
}

function buscarRepetidosPedido(lat, lon, data) {

    var arregloPuntosRepetidos = new Array();
    var j = 0;
    $.each(data, function (index, objPoint) {
        if (lat == objPoint.latitud && lon == objPoint.longitud) {
            arregloPuntosRepetidos[j++] = index;
        }
    });

    return arregloPuntosRepetidos;
}
function addPolilynePedido(coordenada, color) {
    polyline = new google.maps.Polyline({
    strokeColor:color
    , strokeWeight: 3
    , strokeOpacity: 0.9
    , clickable: false
   // , icons: [{ icon: { path: 'M 0,-2 0,2', strokeColor: color, strokeOpacity: 0.0, }}]
    , path: eval(coordenada)
    });
    addLine();

}
function addLine() {
    polyline.setMap(map);
}
function removeLine() {
    polyline.setMap(null);
}

function animateDashedPedido(Color) {
    if (offset['dashed'] > 23) {
        offset['dashed'] = 0;
    } else {
        offset['dashed'] += 2;
    }
    var icons = [{ icon: { path: 'M 0,-2 0,2', strokeColor: Color, strokeOpacity: 0.6, }, repeat: '25px' }];
    icons[0].offset = offset['dashed'] + 'px';
    polyline.set('icons', icons);
}