﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.inicio" Codebehind="inicio.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TGPTPVM');</script>
    <!-- End Google Tag Manager -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Inicio</title>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TGPTPVM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe>
    <!-- End Google Tag Manager (noscript) -->
    <form id="form1" runat="server">
        <div class="cz-main cz-submain-form-backini">
            <%--<div id="cz-other-box-main">
                <h1>&nbsp;</h1>
            </div>--%>
            <div id="cz-other-box-content">
                <div class="cz-other-box-center">
                    <div class="cz-other-box-center-content">
                        <asp:Literal ID="MenuInicio" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
