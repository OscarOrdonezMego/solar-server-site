﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Model.bean;
using Controller;
using System.Configuration;
using Model;
using System.Linq;

/// <summary>
/// @001 GMC 03/05/2015 Validar visualización de menú Almacén y Producto por Almacén
/// </summary>
namespace Pedidos_Site
{
    public partial class inicio : PageController
    {
        protected override void initialize()
        {
            try
            {
                if (!IsPostBack)
                {
                    Int32 liIdPerfil = 0;
                    if (Session["lgn_id"] != null)
                    {
                        liIdPerfil = Convert.ToInt32(Session["lgn_perfil"].ToString());
                    }
                    else
                    {
                        string myScript = "parent.parent.document.location.href = '" + "/Default.aspx?acc=EXT';";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
                    }

                    StringBuilder lsMenuInicio = new StringBuilder();
                    List<MenuBean> loLstMenuBean = MenuController.subObtenerDatosMenu(liIdPerfil);
                    List<ManualBean> loLstManuales = MenuController.subObtenerDatosManuales(liIdPerfil);
                    string lsRutaManual = ConfigurationManager.AppSettings["urlDocs"] + "/";
                    string urlDashboard = "../Dashboard/Default.aspx";
                    if (loLstMenuBean.Count > 0)
                    {
                        List<MenuBean> loLstMenuBeanPadre = loLstMenuBean.Where(obj => obj.IdMenuPadre.Equals(String.Empty)).ToList();

                        for (int i = 0; i < loLstMenuBeanPadre.Count; i++)
                        {
                            List<MenuBean> loLstMenuBeanHijo = loLstMenuBean.Where(obj => obj.IdMenuPadre.Equals(loLstMenuBeanPadre[i].IdMenu)).ToList();
                            if (loLstMenuBeanHijo.Count > 0)
                            {
                                lsMenuInicio.Append("<div class=\"cz-other-box-center-box\">");
                                lsMenuInicio.Append("<div class=\"cz-other-box-center-box-title\">" + loLstMenuBeanPadre[i].Descripcion + "</div>");
                                lsMenuInicio.Append("<div class=\"cz-other-box-center-box-options\">");

                                for (int j = 0; j < loLstMenuBeanHijo.Count; j++)
                                {
                                    String lsDescripcionMenu = loLstMenuBeanHijo[j].Descripcion;

                                    if (lsDescripcionMenu.Length >= 32)
                                    {
                                        lsDescripcionMenu = lsDescripcionMenu.Substring(0, 32) + "...";
                                    }

                                    if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_MANT_ALMACEN || loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_MANT_PRODUCTO_ALMACEN)
                                    {
                                        if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                        {
                                            lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                        }
                                    }
                                    else
                                    {
                                        if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_DASHBOARD)
                                        {
                                            lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option-nojs\"> <a href=\"" + urlDashboard + "\" target=\"_blank\">" + lsDescripcionMenu + "</a> </div>");
                                        }
                                        else
                                        {
                                            if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_REP_TRACKING)
                                            {
                                                if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_REPORTE_TRACKING).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                {
                                                    lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                }
                                            }
                                            else
                                            {
                                                if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MANTENIMIENTO_FORMULARIO)
                                                {
                                                    if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CABECERA_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())
                                                           || fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_FIN_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                    {
                                                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                    }
                                                }
                                                else
                                                {
                                                    if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_MENU_REP_DINAMICO)
                                                    {
                                                        if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CABECERA_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()) ||
                                                            fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_FIN_PEDIDO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                        {
                                                            lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresFuncion.COD_FUNCION_BONIFICACION)
                                                        {
                                                            string valor = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor;
                                                            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                                            {
                                                                lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_DEVOLUCION)
                                                            {
                                                                if ((fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresModulo.COD_MODULO_DEVOLUCION).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())))
                                                                {
                                                                    lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                                }
                                                            }
                                                            else
                                                            {

                                                                if (loLstMenuBeanHijo[j].Codigo == Model.Constantes.CodigoValoresMenu.COD_CANJE)
                                                                {
                                                                    if ((fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresModulo.COD_MODULO_CANJE).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())))
                                                                    {
                                                                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + loLstMenuBeanHijo[j].Url + "\">" + lsDescripcionMenu + "</a> </div>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                lsMenuInicio.Append("</div></div>");
                            }
                        }
                    }

                    if (loLstManuales != null && loLstManuales.Count > 0)
                    {
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box\">");
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-title\">Ayuda</div>");
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-options\">");

                        foreach (ManualBean loManual in loLstManuales)
                        {
                            lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + lsRutaManual + loManual.Url + "\" >" + loManual.Descripcion + "</a> </div>");
                        }
                    }

                    lsMenuInicio.Append("</div></div>");

                    List<Tipo> tipos = Util.obtenerTipos();
                    //lsMenuInicio.Append("<div class=\"cz-other-box-main\">");
                    foreach (Tipo tipo in tipos)
                    {
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box\">");
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-title\">").Append(tipo.Nombre).Append("</div>");
                        lsMenuInicio.Append("<div class=\"cz-other-box-center-box-options\">");

                        foreach (Recurso recurso in tipo.Recursos)
                        {
                            lsMenuInicio.Append("<div class=\"cz-other-box-center-box-option-nojs\"> <a target='blank' href=\"" + recurso.URL + "\" >" + recurso.Descripcion + "</a> </div>");
                        }
                        lsMenuInicio.Append("</div></div>");
                    }

                    MenuInicio.Text = lsMenuInicio.ToString();
                }
            }
            catch (Exception ex)
            {
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
        }


        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
    }
}