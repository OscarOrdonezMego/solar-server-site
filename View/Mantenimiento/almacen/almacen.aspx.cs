﻿using System;
using System.Web.UI;
using System.Web.Services;
using Controller;
using Model.bean;
using Controller.functions;
using Model;

namespace Pedidos_Site.Mantenimiento.almacen
{
    public partial class Mantenimiento_almacen : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MENU_MANT_ALMACEN;

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
        }
        [WebMethod]
        public static void borrar(String codigos, String flag)
        {
            try
            {
                AlmacenController.borrar(codigos, flag);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public static String crear(String id, String codigo, String nombre, String direccion)
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }
                else
                {
                    AlmacenBean bean = new AlmacenBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.direccion = direccion;
                    bean.flag = "T";
                    AlmacenController.crear(bean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
        [WebMethod]
        public static String editar(String id, String codigo, String nombre, String direccion)
        {
            try
            {
                AlmacenBean bean = new AlmacenBean();
                bean.id = id;
                bean.codigo = codigo;
                bean.nombre = nombre;
                bean.direccion = direccion;
                bean.flag = "T";
                AlmacenController.crear(bean);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}