﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Mantenimiento.almacen.Mantenimiento_almacen_nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
<script src="<%=Request.ApplicationPath + "/js/Validacion.js"%>" type="text/javascript"></script>
    <script type="text/javascript">

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion
            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.codigo = $('#MtxtCodigo').val();
            strData.nombre = $('#MtxtNombre').val();
            strData.direccion = $('#MtxtDireccion').val();
            strData.accion = $('#MhAccion').val();
            return strData;
        }
        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtNombre').val('');
            $('#MtxtDireccion').val('');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
           </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%> *</p>
            <input type="text" id="MtxtCodigo" runat="server" class="requerid cz-form-content-input-text" maxlength="20" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="cz-form-content">
            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%> *</p>
            <input type="text" id="MtxtNombre" runat="server" class="requerid cz-form-content-input-text" maxlength="100" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="cz-form-content">
            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%></p>
            <input type="text" id="MtxtDireccion" runat="server" class="cz-form-content-input-text" maxlength="100" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
    </div>
    <div class="modal-footer">
        
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button" value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
     <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    </form>
</body>
</html>
