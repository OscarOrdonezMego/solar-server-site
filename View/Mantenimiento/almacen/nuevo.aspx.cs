﻿using System;
using System.Collections.Generic;
using Model.bean;
using Newtonsoft.Json;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.almacen
{
    public partial class Mantenimiento_almacen_nuevo : PageController
    {
        public bool GenCodigo;
        public static Int32 ioValidarInt;
        public static String ioValidarOcultar;
        protected override void initialize()
        {
            if (!IsPostBack)
            {
                if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                {
                    ioValidarInt = Operation.flagACIVADO.ACTIVO;
                    ioValidarOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                else
                {
                    ioValidarInt = Operation.flagACIVADO.DESACTIVADO;
                    ioValidarOcultar = String.Empty;
                }

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOALMACEN);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                if (dataJSON != null)
                {
                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARALMACEN);
                    AlmacenBean usuario = AlmacenController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"];

                    MtxtNombre.Value = usuario.nombre;
                    MtxtCodigo.Value = usuario.codigo;
                    MtxtDireccion.Value = usuario.direccion;
                    MtxtCodigo.Disabled = true;
                }
                else
                {
                    hidPk.Value = "";
                    MtxtCodigo.Disabled = GenCodigo;
                }

                if (GenCodigo)
                    MtxtNombre.Focus();
                else
                    MtxtCodigo.Focus();
            }

        }
        public static void ValidarSiExisteFrancionamiento(List<ConfiguracionBean> loLstConfiguracionBean, String liValidarVisible, Int32 liRespuestaint)
        {

            FuncionBean loFuncionBean = new FuncionBean();
            loFuncionBean.CodigoFuncion = Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO;
            FuncionBean loFuncionBeanres = Operation.ValidacionSession.fnExtraerCodigoFuncion(loLstConfiguracionBean, loFuncionBean);
            liValidarVisible = Operation.ValidacionSession.fnValidarFuncionOcultar(loFuncionBeanres);
            liRespuestaint = Operation.ValidacionSession.fnValidarFuncion(loFuncionBeanres);
        }
    }
}