﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.listaprecio
{
    public partial class grid : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MANTTENIMIENTO_LISTA_PRECIO;
        private String valorHabilitarDsctoPorProducto = Enumerados.FlagHabilitado.F.ToString();
        public String visibleHabilitarDsctoPorProducto = "";
        public static List<ConfiguracionBean> loLstConfiguracionBean;
        public static Int32 liRespuesta;
        public static String liRespuestastrin;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {

                liRespuestastrin = Operation.VACIO.TEXT_VACIO;
                liRespuesta = Operation.flagACIVADO.ACTIVO;
            }
            else
            {
                liRespuesta = Operation.flagACIVADO.DESACTIVADO;
                liRespuestastrin = Operation.CSS.STYLE_DISPLAY_NONE;

            }


            if (liRespuesta == Operation.flagACIVADO.ACTIVO)
            {

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                String pagina = dataJSON["pagina"].ToString();
                String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
                String producto = dataJSON["producto"].ToString();
                String canal = dataJSON["canal"].ToString();
                String condventa = dataJSON["condventa"].ToString();
                String presentacion = dataJSON["presentacion"].ToString();
                String flag = dataJSON["flag"].ToString();

                ListaPrecioBean loListaPrecioBean = new ListaPrecioBean();
                loListaPrecioBean.Pagina = Int32.Parse(pagina);
                loListaPrecioBean.TotalFila = Int32.Parse(filas);
                loListaPrecioBean.producto = producto;
                loListaPrecioBean.codigocanal = canal;
                loListaPrecioBean.condventa = condventa;
                loListaPrecioBean.Presentacion = presentacion;
                loListaPrecioBean.flag = flag;
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);

                this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

                PaginateListaPrecioBean paginate = ProductoPresentacionController.paginarBuscarListaPrecioPresentacion(loListaPrecioBean);

                if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
                {
                    this.lbTpagina.Text = paginate.totalPages.ToString();
                    this.lbPagina.Text = pagina;

                    if (Int32.Parse(pagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(pagina) == paginate.totalPages)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<ListaPrecioBean> lst = paginate.lstResultados;
                    Session[SessionManager.LISTA_SESSION_PRESENTACION] = lst;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }
            else
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                String producto = dataJSON["producto"].ToString();
                String canal = dataJSON["canal"].ToString();
                String condventa = dataJSON["condventa"].ToString();
                String flag = dataJSON["flag"].ToString();
                String pagina = dataJSON["pagina"].ToString();
                String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;

                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);

                this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

                PaginateListaPrecioBean paginate = ListaPrecioController.paginarBuscar(producto, canal, condventa, flag, pagina, filas);

                if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
                {
                    this.lbTpagina.Text = paginate.totalPages.ToString();
                    this.lbPagina.Text = pagina;

                    if (Int32.Parse(pagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(pagina) == paginate.totalPages)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<ListaPrecioBean> lst = paginate.lstResultados;

                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }

            }

        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
    }
}