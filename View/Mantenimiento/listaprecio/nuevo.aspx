﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.listaprecio.Mantenimiento_listaprecio_nuevo" Codebehind="nuevo.aspx.cs" %>

<%--
@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet"/>
    <script type="text/javascript">


        $(document).ready(function () {
            
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
            //$('#txtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione Producto--');
            $('#txtProducto').autocomplete('producto.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre
                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#txtProducto').result(function (event, item, formatted) {
                $('#mHProducto').val("-1");
                if (item) {
                    $('#mHProducto').val(item.Codigo);
                    var vali = '<%=liRespuestaint%>';
                    if (vali == 1) {
                        var url = "nuevo.aspx/fnBuscarPresentaciones";
                        var obj = new Object();
                        obj.codigo = item.Codigo;
                        enviarConAjax(url, obj);
                    }

                }
                     });

        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion
            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.codigoproducto = $('#mHProducto').val();
            strData.codigocanal = $('#cboTipoCliente').val();
            strData.precio = '0';//$('#txtPrecio').val();
            strData.condicionvta = $('#cboCondVenta').val();
            strData.presentacion = $('#txtpresentacion').val();
            strData.descmin = $('#MtxtDescuentoMin').val();
            strData.descmax = $('#MtxtDescuentoMax').val();
            strData.grupoeconomico = $('#cboGrupoEconomico').val();
            strData.preciosoles = $('#txtPrecioSoles').val();
            strData.preciodolares = $('#txtPrecioDolares').val();
            return strData;
        }
        function clearCampos() {//funcion encargada de limpiar los input
            $('#hidPk').val('');
            $('#mHProducto').val('');
            $('#cboTipoCliente').val($("#McboPerfil option:first").val());
            $('#txtPrecio').val('');
            $('#txtProducto').val('');
            $('#cboCondVenta').val('');
            $('#txtpresentacion').val('');
            $('#MtxtDescuentoMin').val('');
            $('#MtxtDescuentoMax').val('');
            $('#cboGrupoEconomico').val($("#McboPerfil option:first").val());
            $('#txtPrecioSoles').val('');
            $('#txtPrecioDolares').val('');

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
        }

        function enviarConAjax(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "S") {
                        $('#txtpresentacion option').remove();
                        $('#txtpresentacion').append("<option value=\"\">No hay Registro.</option>");
                        $('#txtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html($('#txtpresentacion').find("option:selected").text());
                    } else {
                        $('#txtpresentacion option').remove();
                        $('#txtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                        $('#txtpresentacion').append(msg.d);
                    }

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server"></h3>
        </div>
        <div id="myModalContent" class="modal-body">

            <div class="cz-form-content">
                <p>
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPOCLIENTE)%>
                </p>
                <asp:DropDownList ID="cboTipoCliente" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
            

            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                *
                </p>
                <asp:TextBox ID="txtProducto" CssClass="requerid cz-form-content-input-text" runat="server"></asp:TextBox>
                <asp:HiddenField ID="mHProducto" runat="server" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content" <%=liValidarVisible %>>
                <p><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%> *</p>
                <select id="txtpresentacion" runat="server" class="requerid cz-form-content-input-select"></select>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_COND_VTA)%>
                *
                </p>
                <asp:DropDownList ID="cboCondVenta" runat="server" CssClass=" requerid cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>

            <div class="cz-form-content">
                <p>
                    Grupo Económico
                </p>
                <asp:DropDownList ID="cboGrupoEconomico" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>

            <div class="cz-form-content" style="display:none;">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_PRECIO)%>
                *
                </p>
                <asp:TextBox ID="txtPrecio" onkeypress="fc_PermiteDecimal(this)" runat="server"></asp:TextBox>
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>

            <div class="cz-form-content">
                <p>
                    Precio Soles
                </p>
                <asp:TextBox ID="txtPrecioSoles" class="cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this)" runat="server"></asp:TextBox>
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>

            <div class="cz-form-content">
                <p>
                    Precio Dólares
                </p>
                <asp:TextBox ID="txtPrecioDolares" class="cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this)" runat="server"></asp:TextBox>
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>

            <!--@001 I-->

            <div class="cz-form-content" <%= visibleHabilitarDsctoPorProducto %> <%= visibleHabilitarDscto %>>
                <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMIN)%> *</p>
                <input type="text" id="MtxtDescuentoMin" runat="server" onkeypress="fc_PermiteDecimal(this)" maxlength="20" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content" <%= visibleHabilitarDsctoPorProducto %> <%= visibleHabilitarDscto %> >
                <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMAX)%> *</p>
                <input type="text" id="MtxtDescuentoMax" runat="server" onkeypress="fc_PermiteDecimal(this)" maxlength="20" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
          
            <!--@001 F-->

        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
        <div class="modal-footer">
            <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
                value="GUARDAR" />
        </div>
        <asp:HiddenField ID="MhAccion" runat="server" />
        <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
        <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="" runat="server" />
        <asp:HiddenField ID="hidClave" Value="" runat="server" />


    </form>


</body>
</html>