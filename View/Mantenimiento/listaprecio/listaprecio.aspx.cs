﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using Controller.functions;
using System.Web.Services;
using Model;

/// <summary>
/// @001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
/// </summary>
namespace Pedidos_Site.Mantenimiento.listaprecio
{
    public partial class listaprecio : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MANTTENIMIENTO_LISTA_PRECIO;
        //@001 I
        private static String valorHabilitarDsctoPorProducto = "F";
        //@001 F
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        public static String lsTipoArticulo = "";
        public static List<DescuentoVolumen> ioListaDsctoVol = new List<DescuentoVolumen>();
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                lsTipoArticulo = Operation.TipoArticulo.PRESENTACION;
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_BLOCK;
            }
            else
            {
                lsTipoArticulo = Operation.TipoArticulo.PRODUCTO;
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            cargarCombo();
        }



        private void cargarCombo()
        {

            //.Value = "-1";
        }



        [WebMethod]
        public static void borrar(String codigos, String flag)
        {

            try
            {
                if (ioRespuesta == 1)
                {
                    ProductoPresentacionController.subBorrarListaPrecioPresentacion(codigos, flag);
                }
                else
                {
                    ListaPrecioController.borrar(codigos, flag);
                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String presentacion, String id, String codigoproducto, String codigocanal, String precio, String condicionvta
     , String descmin, String descmax //@001 I/F
            , String grupoeconomico, String preciosoles, String preciodolares)
        {
            try
            {
                if ((preciosoles.Trim() == string.Empty || preciosoles.Trim() == "0") && (preciodolares.Trim() == string.Empty || preciodolares.Trim() == "0"))
                {
                    throw new Exception("Se debe ingresar al menos un precio");
                }
                if (ioRespuesta == 1)
                {
                    ProductoPresentacionBean loProductoPresentacionBean = new ProductoPresentacionBean();
                    loProductoPresentacionBean.CodigoPresentacion = presentacion;
                    loProductoPresentacionBean.Canal = codigocanal;
                    loProductoPresentacionBean.CodVenta = condicionvta;
                    loProductoPresentacionBean.PrecioPre = "0";

if (preciosoles.Trim() != string.Empty && preciosoles.Trim() != "0")
                    {
                        loProductoPresentacionBean.PrecioBaseSoles = preciosoles;
                    }
                    if (preciodolares.Trim() != string.Empty && preciodolares.Trim() != "0")
                    {
                        loProductoPresentacionBean.PrecioBaseDolares = preciodolares;
                    }

                    if (descmin.Length > 0)
                    {
                        loProductoPresentacionBean.Descmin = descmin;
                    }
                    else
                    {
                        loProductoPresentacionBean.Descmin = "0";
                    }
                    if (descmax.Length > 0)
                    {
                        loProductoPresentacionBean.Descmax = descmax;
                    }
                    else
                    {
                        loProductoPresentacionBean.Descmax = "0";
                    }
                    if (Convert.ToInt32(loProductoPresentacionBean.Descmin) > Convert.ToInt32(loProductoPresentacionBean.Descmax))
                    {
                        throw new Exception("Descuento minimo debe ser menor al descuento maximo");
                    }
                    else
                    {
                        ProductoPresentacionController.subCrearListaPrecionPresentacion(loProductoPresentacionBean);
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }

                }
                else
                {
                    if (Utils.tieneCaracteresReservados(codigoproducto))
                    {
                        throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigoproducto) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigoproducto));
                    }

                    else
                    {
                        ListaPrecioBean bean = new ListaPrecioBean();
                        bean.codigoproducto = codigoproducto;
                        bean.codigocanal = codigocanal;
                        bean.precio = precio;
                        bean.condventa = condicionvta;
                        bean.grupoeconomico = grupoeconomico;
                        //bean.preciosoles = preciosoles;
                        //bean.preciodolares = preciodolares;
 if (preciosoles.Trim() != string.Empty && preciosoles.Trim() != "0")
                        {
                            bean.preciosoles = preciosoles;
                        }
                        if (preciodolares.Trim() != string.Empty && preciodolares.Trim() != "0")
                        {
                            bean.preciodolares = preciodolares;
                        }
                        if (descmin.Length > 0)
                        {
                            bean.descmin = descmin;
                        }
                        else
                        {
                            bean.descmin = "0";
                        }
                        if (descmax.Length > 0)
                        {
                            bean.descmax = descmax;
                        }
                        else
                        {
                            bean.descmax = "0";
                        }
                        if (Convert.ToInt32(bean.descmin) > Convert.ToInt32(bean.descmax))
                        {
                            throw new Exception("Descuento minimo debe ser menor al descuento Maximo");
                        }
                        else
                        {
                            ListaPrecioController.crear(bean);
                            return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                        }
                    }

                }



            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
        [WebMethod]
        public static String ListaDescuentoXvolumen(String codigodesvol)
        {
            return codigodesvol;
        }

        [WebMethod]
        public static String editar(String presentacion, String id, String codigoproducto, String codigocanal, String precio, String condicionvta
            , String descmin, String descmax //@001 I/F
            , String grupoeconomico, String preciosoles, String preciodolares)
        {
            try
            {
if ((preciosoles.Trim() == string.Empty || preciosoles.Trim() == "0") && (preciodolares.Trim() == string.Empty || preciodolares.Trim() == "0"))
                {
                    throw new Exception("Se debe ingresar al menos un precio");
                }
                if (ioRespuesta == 1)
                {
                    ProductoPresentacionBean loProductoPresentacionBean = new ProductoPresentacionBean();
                    loProductoPresentacionBean.Codigo = id;
                    loProductoPresentacionBean.CodigoPresentacion = presentacion;
                    loProductoPresentacionBean.Canal = codigocanal;
                    loProductoPresentacionBean.CodVenta = condicionvta;
                    loProductoPresentacionBean.PrecioPre = "0";

if (preciosoles.Trim() != string.Empty && preciosoles.Trim() != "0")
                    {
                        loProductoPresentacionBean.PrecioBaseSoles = preciosoles;
                    }
                    if (preciodolares.Trim() != string.Empty && preciodolares.Trim() != "0")
                    {
                        loProductoPresentacionBean.PrecioBaseDolares = preciodolares;
                    }
                    if (descmin.Length > 0)
                    {
                        loProductoPresentacionBean.Descmin = descmin;
                    }
                    else
                    {
                        loProductoPresentacionBean.Descmin = "0";
                    }
                    if (descmax.Length > 0)
                    {
                        loProductoPresentacionBean.Descmax = descmax;
                    }
                    else
                    {
                        loProductoPresentacionBean.Descmax = "0";
                    }
                    if (Convert.ToInt32(descmin) > Convert.ToInt32(descmax))
                    {
                        throw new Exception("Descuento minimo debe ser menor al descuento Maximo");
                    }
                    else
                    {
                        ProductoPresentacionController.subEditarListaPrecioPresentacion(loProductoPresentacionBean);
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }

                }
                else
                {
                    if (Utils.tieneCaracteresReservados(codigoproducto))
                    {
                        throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigoproducto) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigoproducto));
                    }

                    else
                    {
                        ListaPrecioBean bean = new ListaPrecioBean();
                        bean.id = id;
                        bean.codigoproducto = codigoproducto;
                        bean.codigocanal = codigocanal;
                        bean.precio = precio;
                        bean.condventa = condicionvta;
                        bean.descmin = descmin; //@001 I/F
                        bean.descmax = descmax; //@001 I/F
                        bean.grupoeconomico = grupoeconomico;
                       //bean.preciosoles = preciosoles;
                        //bean.preciodolares = preciodolares;

                        if (preciosoles.Trim() != string.Empty && preciosoles.Trim() != "0")
                        {
                            bean.preciosoles = preciosoles;
                        }
                        if (preciodolares.Trim() != string.Empty && preciodolares.Trim() != "0")
                        {
                            bean.preciodolares = preciodolares;
                        }
                        if (Convert.ToInt32(descmin) > Convert.ToInt32(descmax))
                        {
                            throw new Exception("Descuento minimo debe ser menor al descuento Maximo");
                        }
                        else
                        {
                            ListaPrecioController.editar(bean);
                            return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                        }

                    }
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
        [WebMethod]
        public static String fncrearDescuentoVolumen(String rangominimo, String rangomaximo, String dsctominimo, String dsctomaximo, String idprecio, String codproducto)
        {
            try
            {
                List<DescuentoVolumen> listarangominimo = Operation.SplitVector.ListaRangoMin(rangominimo);
                List<DescuentoVolumen> listarangomaximo = Operation.SplitVector.ListaRangoMax(rangomaximo);
                List<DescuentoVolumen> listadsctominimo = Operation.SplitVector.Listadsctominimo(dsctominimo);
                List<DescuentoVolumen> listadsctomaximo = Operation.SplitVector.Listadsctomaximo(dsctomaximo);
                DescuentoVolumen loDescuentoVolumen = null;
                ListaPrecioController.BorrarDescuentoVolumen(Int32.Parse(idprecio));
                for (Int32 i = 0; i < listadsctomaximo.Count; i++)
                {
                    loDescuentoVolumen = new DescuentoVolumen();
                    loDescuentoVolumen.IDLISTAPRECIO = idprecio;
                    loDescuentoVolumen.CODIGOPROD = codproducto;
                    loDescuentoVolumen.RMIN = listarangominimo[i].RMIN;
                    loDescuentoVolumen.RMAX = listarangomaximo[i].RMAX;
                    loDescuentoVolumen.DSCTOMIN = listadsctominimo[i].DSCTOMIN;
                    loDescuentoVolumen.DSCTOMAX = listadsctomaximo[i].DSCTOMAX;
                    loDescuentoVolumen.TIPOARTICULO = lsTipoArticulo;
                    ListaPrecioController.crearDescuentoVolumen(loDescuentoVolumen);
                }
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String AgregarListaDescuentovol(String ranminnum, String ranmaxnum, String destominnum, String destomaxnum)
        {
            try
            {
                DescuentoVolumen obj = new DescuentoVolumen();
                obj.RMIN = ranminnum;
                obj.RMAX = ranmaxnum;
                obj.DSCTOMIN = destominnum;
                obj.DSCTOMAX = destomaxnum;
                Int32 rptaRango = Operation.Validacion.ValidacionRango(ioListaDsctoVol, ranminnum, ranmaxnum);
                Int32 rptaDescuento = Operation.Validacion.ValidacionDescuento(ioListaDsctoVol, destominnum, destomaxnum);

                if ((rptaRango == 1 && rptaDescuento == 1))
                {
                    ioListaDsctoVol.Add(obj);
                    return "1";
                }
                else
                {
                    return "0";

                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}