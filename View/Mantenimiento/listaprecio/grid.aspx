﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.listaprecio.grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 15/04/2015 Ajustes para validar visualización de Descuento Mínimo y Máximo
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <script>
        var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
        $(document).ready(function () {
            if (flag == "T") {
                $('#Tieliminar').append('Eliminar');
            } else {
                $('#Tieliminar').append('Restaurar');
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divGridView" runat="server">
            <asp:Repeater ID="grdMant" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%" class="grilla table table-bordered table-striped" id="Mygrid">
                        <thead>
                            <tr>
                                <th style="width: 40px;" class="center">
                                    <input id="ChkAll" name="chkSelectAll" value="<%# Eval("CODIGOPRODUCTO") %>" type="checkbox">
                                </th>
                                <th nomb="NOMBRE" >
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                                </th>

                                <th nomb="NOMBRE" id="nombre" <%=liRespuestastrin %> >
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                                </th>

                                <th nomb="TIPOACTIVIDAD">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPOCLIENTE)%>
                                </th>
                                <th nomb="FECHAINICIO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_COND_VTA)%>
                                </th>
                               <%-- <th nomb="FECHAFIN">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIO)%>
                                </th>--%>

                                <th nomb="PrecioSoles">
                                    Precio Soles
                                </th>
                                <th nomb="PrecioDolares">
                                    Precio Dólares
                                </th>

                                <th nomb="GrupoEconomico">
                                    Grupo Económico
                                </th>

                                <%--@001 I--%>
                                 <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())
                               )
                                  {%>
                                <th nomb="DESC_MIN" <%= visibleHabilitarDsctoPorProducto %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTO_MIN)%>
                                </th>
                                <%} %>
                                 <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                  {%>
                                <th nomb="DESC_MAX" <%= visibleHabilitarDsctoPorProducto %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTO_MAX)%>
                                </th>
                                 <%} %>
                                <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_VOLUMEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                  {%>
                                <th nomb="DESC_MAX">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESC_VOL)%>
                                </th>
                                <%} %>
                                <%--@001 F--%>
                                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                                  { %>
                                <th style="width: 40px;">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                                </th>
                                <%} %>
                                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                                  {%>
                                <th style="width: 40px;" id="Tieliminar"></th>
                                <%} %>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <input id="<%# Eval("CODIGOPRODUCTO") %>" value="<%# Eval("CODIGOPRODUCTO") %>" type="checkbox">
                        </td>

                        <td  <%=liRespuestastrin %>  >
                            <%# Eval("NombreProducto")%>
                        </td>
                        <td>
                            <%# Eval("producto")%>
                        </td>
                        <td>
                            <%# Eval("CODIGOCANAL")%>
                        </td>
                        <td>
                            <%# Eval("CONDVENTA")%>
                        </td>
                        <%--<td>
                            <%# Eval("PRECIO")%>
                        </td>--%>

                        <td>
                            <%# Eval("preciosoles")%>
                        </td>
                        <td>
                            <%# Eval("preciodolares")%>
                        </td>

                        <td>
                            <%# Eval("grupoeconomico")%>
                        </td>

                        <%--@001 I--%>
                              <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descmin")%>
                        </td>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descmax")%>
                        </td>
                           <%} %>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_VOLUMEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                        <td>
                           <center><a style="cursor: pointer;" data-toggle="modal" class="descuentoXvolumen" idlipre="<%# Eval("codigoproducto")%>" codpro="<%# Eval("CodProducto")%>" codpre="<%# Eval("Codpresentacion")%>">
                                <img src="../../imagery/all/icons/detalle.png" border="0" title="Dscto x Vol" /></a></center> 
                        </td>
                        <%} %>
                        <%--@001 F--%>

                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                          {%>
                        <td>
                            <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" condven="<%# Eval("CONDVENTA")%>" pre="<%# Eval("NombreProducto")%>" pro="<%# Eval("PRODUCTO")%>" canal="<%# Eval("CODIGOCANAL")%>" cod="<%# Eval("CODIGOPRODUCTO") %>">
                                <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                        </td>
                        <%} %>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                          { %>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                                cod="<%# Eval("CODIGOPRODUCTO") %>">
                                <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                        </td>
                        <%} %>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt">
                        <td>
                            <input id="<%# Eval("CODIGOPRODUCTO") %>" value="<%# Eval("CODIGOPRODUCTO") %>" type="checkbox">
                        </td>

                        <td  <%=liRespuestastrin %> >
                            <%# Eval("NombreProducto")%>
                        </td>
                        <td>
                            <%# Eval("PRODUCTO")%>
                        </td>
                        <td>
                            <%# Eval("CODIGOCANAL")%>
                        </td>
                        <td>
                            <%# Eval("CONDVENTA")%>
                        </td>
                        <%--<td>
                            <%# Eval("PRECIO")%>
                        </td>--%>

                        <td>
                            <%# Eval("preciosoles")%>
                        </td>
                        <td>
                            <%# Eval("preciodolares")%>
                        </td>

                        <td>
                            <%# Eval("grupoeconomico")%>
                        </td>

                        <%--@001 I--%>
                         <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descmin")%>
                        </td>
                            <%} %>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descmax")%>
                        </td>
                        <%} %>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_VOLUMEN).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                        <td>
                           <center><a style="cursor: pointer;" class="descuentoXvolumen" data-toggle="modal" idlipre="<%# Eval("codigoproducto")%>" codpro="<%# Eval("CodProducto")%>" codpre="<%# Eval("Codpresentacion")%>">
                                <img src="../../imagery/all/icons/detalle.png" border="0" title="Dscto x Vol" /></a></center> 
                        </td>
                        <%} %>
                        <%--@001 F--%>

                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                          { %>
                        <td>
                            <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" condven="<%# Eval("CONDVENTA")%>" pre="<%# Eval("NombreProducto")%>" pro="<%# Eval("PRODUCTO")%>" canal="<%# Eval("CODIGOCANAL")%>" cod="<%# Eval("CODIGOPRODUCTO") %>">
                                <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                        </td>
                        <%} %>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                          {%>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                                cod="<%# Eval("CODIGOPRODUCTO") %>">
                                <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                        </td>
                        <%} %>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Pagina</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            buscando resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">
                    ×
                </div>
            </div>
        </div>
    </form>
</body>
</html>