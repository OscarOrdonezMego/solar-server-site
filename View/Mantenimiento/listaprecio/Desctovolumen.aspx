﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.listaprecio.Mantenimiento_listaprecio_Desctovolumen" Codebehind="Desctovolumen.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        .fila-base {
            display: none;
        }

        .eliminar {
            cursor: pointer;
            color: #000;
        }

        input[type="text"] {
            width: 80px;
        }

        input#btnañadirlista {
            width: 104px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#tdañadir').css('background-color', '#0154A0');
            $('#tdañadir').mouseenter(function () {
                $(this).css('background-color', '#0154A0');
            });
            $('#textvisible').css('width', '85px');
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server"><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_TITULO_DESCUENTO_VOL)%></h3>
        </div>
        <div id="myModalContent" class="modal-body">
            <table id="miTable" class="mygrilla table table-bordered table-striped" style="width: 100%">
                <thead>
                    <tr>
                        <th scope="col">Rango Min</th>
                        <th scope="col">Rango Max</th>
                        <th scope="col">Dscto Min</th>
                        <th scope="col">Dscto Max</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <%=Fila %>
                    <tr class="fila-base">
                        <td>
                            <input type="text" id="rangomin1" name="rangomin1" /></td>
                        <td>
                            <input type="text" id="rangomax1" name="rangomax1" /></td>
                        <td>
                            <input type="text" id="destomin1" name="destomin1" /></td>
                        <td>
                            <input type="text" id="destomax1" name="destomax1" /></td>
                        <td class="eliminar">Eliminar</td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width: 116px" class="cz-form-content">
                                <input type="text" onkeypress="fc_PermiteNumeros()" class="add requerid cz-form-content-input-text" id="rangomin2" name="rangomin2" />
                                <div style="width: 85px" class="cz-form-content-input-text-visible">
                                    <div class="cz-form-content-input-text-visible-button">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="width: 116px" class="cz-form-content">
                                <input type="text" onkeypress="fc_PermiteNumeros()" class="add requerid cz-form-content-input-text" id="rangomax2" name="rangomax2" />
                                <div style="width: 85px" class="cz-form-content-input-text-visible">
                                    <div class="cz-form-content-input-text-visible-button">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>

                            <div style="width: 116px" class="cz-form-content">
                                <input onkeypress="fc_PermiteDecimalOld(this)" type="text" class="add requerid cz-form-content-input-text" id="destomin2" name="destomin2" />
                                <div style="width: 85px" class="cz-form-content-input-text-visible">
                                    <div class="cz-form-content-input-text-visible-button">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>

                            <div style="width: 116px" class="cz-form-content">
                                <input type="text" onkeypress="fc_PermiteDecimalOld(this)" class="add requerid cz-form-content-input-text" id="destomax2" name="destomax2" />
                                <div style="width: 85px" class="cz-form-content-input-text-visible">
                                    <div class="cz-form-content-input-text-visible-button">
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
        <div class="modal-footer">
            <input type="button" id="btnañadirlista" runat="server" class="addanadirlista form-button cz-form-content-input-button"
                value="AÑADIR" />
            <input type="button" id="saveRegdescvol" runat="server" class="form-button cz-form-content-input-button"
                value="GUARDAR" />

        </div>
        <asp:HiddenField ID="MhAccion" runat="server" />
        <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
        <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="" runat="server" />
        <asp:HiddenField ID="hidClave" Value="" runat="server" />
        <asp:HiddenField ID="idlistaprecio" Value="" runat="server" />
        <asp:HiddenField ID="codproducto" Value="" runat="server" />
        <asp:HiddenField ID="TotalParaEliminar" Value="" runat="server" />

    </form>
</body>
</html>
