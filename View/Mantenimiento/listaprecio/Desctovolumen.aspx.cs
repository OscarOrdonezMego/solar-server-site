﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web.Services;
using Model;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.listaprecio
{
    public partial class Mantenimiento_listaprecio_Desctovolumen : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MANTTENIMIENTO_LISTA_PRECIO;
        public static String lsTipoArticulo = "";
        public static String Fila = "";
        public static List<DescuentoVolumen> ioListaDsctoVol = new List<DescuentoVolumen>();
        protected override void initialize()
        {

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                lsTipoArticulo = Operation.TipoArticulo.PRESENTACION;
            }
            else
            {
                lsTipoArticulo = Operation.TipoArticulo.PRODUCTO;
            }
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                Fila = "";
                idlistaprecio.Value = dataJSON["idlistaprecio"].ToString();
                codproducto.Value = dataJSON["codproducto"].ToString();
                List<DescuentoVolumen> lista = ListaPrecioController.BuscarDescuentoVolumen(Int32.Parse(dataJSON["idlistaprecio"].ToString()));

                if (lista.Count > 0)
                {
                    TotalParaEliminar.Value = lista.Count().ToString();
                    ioListaDsctoVol = lista;
                    Int32 contador = 0;
                    for (Int32 i = 0; i < lista.Count; i++)
                    {
                        contador = contador + 1;
                        Fila += "<tr opc=\"2\"><td opcranmin=" + lista[i].RMIN + ">" + lista[i].RMIN + "</td><td opcranmax=" + lista[i].RMAX + ">" + lista[i].RMAX + "</td><td opcdestomin=" + lista[i].DSCTOMIN + ">" + lista[i].DSCTOMIN + "</td><td opcdestomax=" + lista[i].DSCTOMAX + ">" + lista[i].DSCTOMAX + "</td><td class=\"eliminar\" cod=" + contador + " ><center><a role=\"button\"  style=\"cursor: pointer;\"  ><img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" /></a></center></td></tr>";
                    }
                }
                else
                {
                    TotalParaEliminar.Value = lista.Count().ToString();
                    ioListaDsctoVol = lista;
                }

            }
        }
        [WebMethod]
        public static String AgregarListaDescuentovol(String ranminnum, String ranmaxnum, String destominnum, String destomaxnum, String codfila)
        {
            try
            {
                DescuentoVolumen obj = new DescuentoVolumen();
                obj.RMIN = ranminnum;
                obj.RMAX = ranmaxnum;
                obj.DSCTOMIN = destominnum;
                obj.DSCTOMAX = destomaxnum;
                obj.Codfila = codfila;
                if (ioListaDsctoVol.Count > 0)
                {
                    Int32 rptaRango = Operation.Validacion.ValidacionRango(ioListaDsctoVol, ranminnum, ranmaxnum);
                    Int32 rptaDescuento = Operation.Validacion.ValidacionDescuento(ioListaDsctoVol, destominnum, destomaxnum);

                    if ((rptaRango == 1 && rptaDescuento == 1))
                    {
                        ioListaDsctoVol.Add(obj);
                        return "1";
                    }
                    else
                    {

                        return "0";

                    }
                }
                else
                {
                    ioListaDsctoVol.Add(obj);
                    return "1";
                }


            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }



        }
        [WebMethod]
        public static String Eliminarfila(String codigo)
        {
            if (ioListaDsctoVol.Count > 0)
            {
                for (Int32 i = 0; i < ioListaDsctoVol.Count; i++)
                {
                    if (ioListaDsctoVol[i].Codfila == codigo)
                    {
                        ioListaDsctoVol.RemoveAt(i);
                    }
                }
            }
            return "Eliminado";
        }
    }
}