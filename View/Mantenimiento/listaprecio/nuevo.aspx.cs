﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Controller;
using Model.bean;
using Newtonsoft.Json;
using System.Web.Services;
using Model;

/// <summary>
/// @001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
/// </summary>
namespace Pedidos_Site.Mantenimiento.listaprecio
{
    public partial class Mantenimiento_listaprecio_nuevo : PageController
    {
        public bool GenCodigo;

        //@001 I
        private String valorHabilitarDscto = "F";
        private String valorHabilitarDsctoPorProducto = "F";
        private String obligatorioIngresar = "";
        public String visibleHabilitarDsctoPorProducto = "";
        public String visibleHabilitarDscto = "";

        //@001 F
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;

                liValidarVisible = String.Empty;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            if (!Page.IsPostBack)
            {
                cargarCombo();
                IniciarTipoCliente();
                IniciarGrupoEconomico();

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOPRECIO);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);
                if (liRespuestaint == 1)
                {
                    if (dataJSON != null)
                    {
                        String lsCodigoPresentacion = dataJSON["codigo"];
                        List<ListaPrecioBean> loLstListaPrecioBean = ProductoPresentacionController.BuscarListaPrecioPresentacion(Int32.Parse(lsCodigoPresentacion));
                        for (Int32 i = 0; i < loLstListaPrecioBean.Count; i++)
                        {
                            if (loLstListaPrecioBean[i].codigoproducto.Equals(lsCodigoPresentacion))
                            {
                                cboTipoCliente.SelectedValue = loLstListaPrecioBean[i].codigocanal;
                                txtProducto.Text = loLstListaPrecioBean[i].NombreProducto;
                                mHProducto.Value = loLstListaPrecioBean[i].CodProducto;
                                List<ProductoPresentacionBean> loLstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(loLstListaPrecioBean[i].CodProducto);
                                if (loLstProductoPresentacionBean.Count > 0)
                                {
                                    for (Int32 j = 0; j < loLstProductoPresentacionBean.Count; j++)
                                    {
                                        txtpresentacion.Items.Insert(j, loLstProductoPresentacionBean[j].Nombre);
                                        txtpresentacion.Items[j].Value = loLstProductoPresentacionBean[j].CodigoPresentacion;
                                    }
                                    for (Int32 s = 0; s < loLstProductoPresentacionBean.Count; s++)
                                    {
                                        if (txtpresentacion.Items[s].Value.Equals(loLstListaPrecioBean[i].Codpresentacion))
                                        {
                                            txtpresentacion.Items[s].Selected = true;
                                        }
                                    }
                                }
                                cboCondVenta.SelectedValue = loLstListaPrecioBean[i].condventa;
                                txtPrecio.Text = loLstListaPrecioBean[i].precio;
txtPrecioSoles.Text = loLstListaPrecioBean[i].preciosoles;
                                txtPrecioDolares.Text = loLstListaPrecioBean[i].preciodolares;
                                MtxtDescuentoMin.Value = loLstListaPrecioBean[i].descmin;
                                MtxtDescuentoMax.Value = loLstListaPrecioBean[i].descmax;
                                hidPk.Value = loLstListaPrecioBean[i].codigoproducto;
                            }
                        }
                    }
                }
                else
                {
                    if (dataJSON != null)
                    {

                        myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARPRECIO);
                        ListaPrecioBean usuario = ListaPrecioController.info(dataJSON["codigo"]);
                        hidPk.Value = usuario.id;

                        txtProducto.Text = usuario.producto;
                        mHProducto.Value = usuario.codigoproducto;
                        txtPrecio.Text = usuario.precio;
                        cboCondVenta.SelectedValue = usuario.condventa;
                        cboTipoCliente.SelectedValue = usuario.codigocanal;
                        MtxtDescuentoMin.Value = usuario.descmin;
                        MtxtDescuentoMax.Value = usuario.descmax;
                        cboGrupoEconomico.SelectedValue = usuario.grupoeconomico;
                        txtPrecioSoles.Text = usuario.preciosoles;
                        txtPrecioDolares.Text = usuario.preciodolares;
                    }
                    else
                    {
                        hidPk.Value = "";

                    }

                }

                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);

                valorHabilitarDscto = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor;
                this.visibleHabilitarDscto = (this.valorHabilitarDscto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.obligatorioIngresar = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.T.ToString() ? "cz-form-content-input-text" : "cz-form-content-input-text");

                this.MtxtDescuentoMin.Attributes.Add("class", obligatorioIngresar);
                this.MtxtDescuentoMax.Attributes.Add("class", obligatorioIngresar);

            }
        }

        private void cargarCombo()
        {
            cboCondVenta.DataSource = GeneralController.getGeneralBean2("USPS_COND_VENTA");
            cboCondVenta.DataValueField = "CODIGO";
            cboCondVenta.DataTextField = "NOMBRE";

            try
            {
                cboCondVenta.DataBind();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        [WebMethod]
        public static String fnBuscarPresentaciones(String codigo)
        {
            try
            {
                String nombre = "--Seleccione--";
                String option = "";
                List<ProductoPresentacionBean> lolstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(codigo);
                if (lolstProductoPresentacionBean.Count > 0)
                {
                    option = "<option value=\"\" >" + nombre + " </option>";
                    for (Int32 i = 0; i < lolstProductoPresentacionBean.Count; i++)
                    {
                        option += "<option value=" + lolstProductoPresentacionBean[i].CodigoPresentacion + " >" + lolstProductoPresentacionBean[i].Nombre + " " + lolstProductoPresentacionBean[i].Cantidad + "</option>";
                    }
                    return option;
                }
                else
                {
                    option = "S";
                    return option;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void IniciarTipoCliente()
        {
            cboTipoCliente.DataTextField = "Nombre";
            cboTipoCliente.DataValueField = "Id";
            cboTipoCliente.DataSource = ClienteController.fnListarTipoCliente();
            cboTipoCliente.DataBind();
            cboTipoCliente.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }

        private void IniciarGrupoEconomico()
        {
            cboGrupoEconomico.DataTextField = "nombre";
            cboGrupoEconomico.DataValueField = "codigo";
            cboGrupoEconomico.DataSource = MantGeneralController.paginarBuscar("", "", "11", "T", 1, 10000).lstResultados;
            cboGrupoEconomico.DataBind();
            cboGrupoEconomico.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }
    }
}