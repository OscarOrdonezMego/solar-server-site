﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.listaprecio.listaprecio" Codebehind="listaprecio.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <%--  <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>--%>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>

    <script src="../../js/alertify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../css/alertify.core.css" />
    <link rel="stylesheet" href="../../css/alertify.default.css" />
 
    <script type="text/javascript">
        var urldel = 'listaprecio.aspx/borrar';
        var urlins = 'nuevo.aspx'
        var urlbus = 'grid.aspx';
        var urlsavN = 'listaprecio.aspx/crear';
        var urlsavE = 'listaprecio.aspx/editar';

        $(document).ready(function () {

            deleteReg();
            addReg();
            modReg();
            busReg();
            CrearDesctoXvol();
            eliminarFila();
            textcampos();
            botonañadir();
            $('#buscar').trigger("click");
            
        });
        function CrearDesctoXvol() {
            
            $('.descuentoXvolumen').unbind();
            $('.descuentoXvolumen').live('click', function () {
                
                var codpro = $(this).attr('codpro');
                var idlpre = $(this).attr('idlipre');
                var codpre = $(this).attr('codpre');
                var objdesvol = new Object();
                var validar = '<%=ioRespuesta%>';
                if (validar == 1) {
                    objdesvol.codproducto = codpre;
                } else {
                    objdesvol.codproducto = codpro;
                }                
                objdesvol.idlistaprecio = idlpre;
                var urlir = "Desctovolumen.aspx";
                $.ajax({
                    type: "POST",
                    url: urlir,
                    data: JSON.stringify(objdesvol),
                    success: function (responding) {
                        $("#myModalDesctoVol").html(responding);                        
                        $('#myModalDesctoVol').modal('show');
                        $('#saveRegdescvol').unbind();

                        $('#saveRegdescvol').click(function () {
                            // $("#miTable tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#miTable tbody"); 
                            var tamaño = 0;
                            var tamaño2 = 0;
                            var contador = 0;
                            var objdescuentoVolumen;

                            var dsctomin="" ;
                            var dsctomax = "";
                            var rangomin = "";
                            var rangomax = "";
                            $('#miTable').find("tbody tr").each(function (i) {
                                tamaño = $(this).attr("opc");
                                //tamaño2 = $(this).find("td").length;
                                $(this).children("td").each(function (j) {
                                    switch (j) {
                                        case 0:
                                            if ($(this).attr("opcranmin") != undefined) {
                                                rangomin += $(this).attr("opcranmin") + "|";
                                            }
                                            break;
                                        case 1:
                                            if ($(this).attr("opcranmax") != undefined) {
                                                rangomax += $(this).attr("opcranmax") + "|";
                                            }
                                            break;
                                        case 2:
                                            if ($(this).attr("opcdestomin") != undefined) {
                                                dsctomin += $(this).attr("opcdestomin") + "|";
                                            }
                                            break;
                                        case 3:
                                            if ($(this).attr("opcdestomax") != undefined) {
                                                dsctomax += $(this).attr("opcdestomax") + "|";
                                            }
                                            break;

                                    }
                                });
                                if (tamaño == 2) {
                                    contador = contador + 1;
                                }
                               
                            });
                                var urlGrabar = "listaprecio.aspx/fncrearDescuentoVolumen";
                                var obj = new Object();
                                obj.rangominimo = rangomin;
                                obj.rangomaximo = rangomax;
                                obj.dsctominimo = dsctomin;
                                obj.dsctomaximo = dsctomax;
                                obj.idprecio = $('#idlistaprecio').val();
                                obj.codproducto = $('#codproducto').val();
                                $.ajax({
                                    url: urlGrabar,
                                    data: JSON.stringify(obj),
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (msg) {
                                        $('#myModalDesctoVol').modal('hide');
                                        addnotify("notify", msg.d, "registeruser");
                                    },
                                    error: function (result) {
                                        addnotify("notify", result.status, "registeruser");
                                    }
                                });                            
                            
                        });
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });
            });           
        }
        function textcampos() {
            $('.add').live("keypress", function (e) {
                if (e.which == 13) {
                    if (validarCampos()) {
                        AñadirFila($('#rangomin2').val(), $('#rangomax2').val(), $('#destomin2').val(),  $('#destomax2').val(), $('#rangomin2').val(), $('#rangomax2').val(), $('#destomin2').val(), $('#destomax2').val());
                    }
                }
                
            });               
        }
        function botonañadir() {
            $('.addanadirlista').live('click', function () {
                    if (validarCampos()) {
                        AñadirFila( $('#rangomin2').val(), $('#rangomax2').val(), $('#destomin2').val(), $('#destomax2').val(), $('#rangomin2').val(), $('#rangomax2').val(),$('#destomin2').val(),$('#destomax2').val());
                    }
            });
        }
        function validarCampos() {
            var rpta = true;
            $('.requerid').each(function () {
                $(this).parent().find('span').remove();
                if ($(this).val() == "" || $(this).val() == "0" || $(this).val() == "0.0") {

                    $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");

                    $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")


                    rpta = false;

                }
            });
            return rpta;
        }
        function eliminarFila() {
            $(document).on("click", ".eliminar", function () {
                var parent = $(this).parents().get(0);
                $(parent).remove();
                var par = $(this).attr('cod');
                var urlEliminar = "Desctovolumen.aspx/Eliminarfila";
                var obj = new Object();
                obj.codigo = par;
                $.ajax({
                    url: urlEliminar,
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                       
                            addnotify("notify",msg.d, "registeruser");
                        
                    },
                    error: function (result) {

                        addnotify("notify", result.status, "registeruser");
                    }
                });
            });
        }
        function limpiarTextos() {
            $('#rangomin2').val('');
            $('#rangomax2').val('');
            $('#destomin2').val('');
            $('#destomax2').val('');
        }
        function FocusTexto() {
            $('#rangomin2').focus();
        }
        function AñadirFila(ranmin, ranmax, destomin, destomax, ranminnum, ranmaxnum, destominnum, destomaxnum) {
            var resultado = 0;
            var numerofila = 0;
                numerofila=   $("#TotalParaEliminar").val();
            if (numerofila == "0") {
                resultado = 1;
                $("#TotalParaEliminar").val('1');
            } else {
                resultado = (parseInt(numerofila) + parseInt(1));
                $("#TotalParaEliminar").val(resultado);
            }

            var urlir = "Desctovolumen.aspx/AgregarListaDescuentovol";
            var objdescuento = new Object();
            objdescuento.ranminnum = ranminnum;
            objdescuento.ranmaxnum = ranmaxnum;
            objdescuento.destominnum = destominnum;
            objdescuento.destomaxnum = destomaxnum;
            objdescuento.codfila = resultado;
            $.ajax({
                url: urlir,
                data: JSON.stringify(objdescuento),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "1") {
                        var fila = "<tr opc=\"2\"><td opcranmin=" + ranminnum + ">" + ranmin + "</td><td opcranmax=" + ranmaxnum + ">" + ranmax + "</td><td opcdestomin=" + destominnum + ">" + destomin + "</td><td opcdestomax=" + destomaxnum + ">" + destomax + "</td><td  class=\"eliminar\" cod=" + resultado + " ><center><a role=\"button\"  style=\"cursor: pointer;\"  ><img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" /></a></center></td></tr>";
                        $('#miTable').find("tbody tr:eq(0)").after(fila);
                        limpiarTextos();
                        FocusTexto();
                        addnotify("notify", "Registro Exitoso", "registeruser");
                    } else {
                        addnotify("notify", "Esta dentro del Rango", "registeruser");
                    }
                },
                error: function (result) {

                    addnotify("notify", result.status, "registeruser");
                }
            });
            /*
            var $tr = $('#miTable').find('tbody tr:last').clone();
            $tr.find("input").attr("name", function () {
                //  separar el campo name y su numero en dos partes
                var parts =this.id.match(/(\D+)(\d+)$/);
                // crear un nombre nuevo para el nuevo campo incrementando el numero para los previos campos en 1
                return parts[1] + ++parts[2];
                // repetir los atributos ids
            }).attr("id", function () {
                var parts = this.id.match(/(\D+)(\d+)$/);
                return parts[1] + ++parts[2];
            });
           
            // añadir la nueva fila a la tabla
            */
        }
        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.producto = $('#MtxtProducto').val();
            strData.canal = $('#MtxtCanal').val();
            strData.condventa = $('#MtxtCondVenta').val();
            strData.flag = $('#chkflag').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.presentacion = $('#txtprestacionprecio').val();
            

            return strData;
        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/precio.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_LISTAPRECIO)%> </p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
               <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.CREAR))
                 {%>
                <input type="button" id="cz-form-box-nuevo" class="cz-form-content-input-button cz-form-content-input-button-image form-button addReg cz-form-box-content-button cz-util-right"
                    data-grid-id="divGridViewData" value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NUEVO) %>" />
                <%} %>
                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                  {%>
                <input type="button" id="cz-form-box-del" class="cz-form-content-input-button cz-form-content-input-button-image form-button delReg cz-util-right"
                    value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
               <%} %>
            </div>
            <div class="cz-form-box-content">
                <%if(fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())){ %>
                <div class="cz-form-content" >
                    <p>
                        <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%></p>
                    <asp:TextBox ID="txtprestacionprecio" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="hddlTurno" type="hidden" value="-1" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <%} %>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PRODUCTO)%></p>
                    <asp:TextBox ID="MtxtProducto" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="hddlTurno" type="hidden" value="-1" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CANAL)%></p>
                    <asp:TextBox ID="MtxtCanal" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="hcliente" type="hidden" value="-1" runat="server" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                 <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CONDVTA)%></p>
                    <asp:TextBox ID="MtxtCondVenta" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="Hidden1" type="hidden" value="-1" runat="server" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
               
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HABILITADO)%></p>
                        <input type="checkbox" class="cz-form-content-input-check" id="chkflag" name="habilitado"
                            checked="checked" />
                    </div>
                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                            value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server">
                                </div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server">
                                </div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>
                                        buscando resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hidden Fields to control pagination-->
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                    

                     <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />

                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
                <div id="myModalDesctoVol" class="modaldescvol hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>
