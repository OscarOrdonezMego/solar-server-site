﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.general
{
    public partial class Mantenimiento_usuario_grid : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MANTTENIMIENTO_GENERAL;
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String codigo = dataJSON["gencodigo"].ToString();
            String nombre = dataJSON["gennombre"].ToString();
            String grupo = dataJSON["gentipo"].ToString();
            Int32 pagina = Int32.Parse(dataJSON["pagina"].ToString());
            Int32 filas = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
            String flag = dataJSON["flag"].ToString();
            Session["general_flag"] = flag;

            PaginateMantGeneralBean paginate = MantGeneralController.paginarBuscar(codigo, nombre, grupo, flag, pagina, filas);

            if ((pagina > 0) && (pagina <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina.ToString();

                if (pagina == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (pagina == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<MantGeneralBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        public Boolean getHabilitado()
        {
            bool result = true;
            try
            {
                String flag = Session["general_flag"].ToString();
                if (flag != null)
                {
                    result = (flag == "T") ? true : false;
                }
            }
            catch (Exception)
            {
                result = true;
            }
            return result; 
        }
    }
}