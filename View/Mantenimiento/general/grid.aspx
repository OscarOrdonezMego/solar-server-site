﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.general.Mantenimiento_usuario_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("grupo") %>|<%# Eval("id") %>" type="checkbox">
                            </th>
                            <th  class="center" nomb="TIPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                            </th>
                            <th style="width: 100px;"  nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th  nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                              {%>
                             <th style="width: 40px;">
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                            <%} %>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                              { %>

                             <%if (getHabilitado())
                             {%>

                            <th style="width: 40px;">
                             <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                            </th>

                             <%} else { %>

                            <th style="width: 40px;">
                             Restaurar
                            </th>

                             <%} %>

                            <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("grupo") %>|<%# Eval("id") %>" value="<%# Eval("grupo") %>|<%# Eval("id") %>" type="checkbox">
                    </td>
                     <td>
                        <%# Eval("GRUPONOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                     {%>
                    <td style="text-align:center;">
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("grupo") %>|<%# Eval("CODIGO") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                  <%} %>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                    {%>

                    <%if (getHabilitado())
                    {%>

                     <td style="text-align:center;">
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("grupo") %>|<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " />
                            
                        </a>
                    </td>

                    

                    <%} else { %>

                   <td style="text-align:center;">
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("grupo") %>|<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/restaurar.png" border="0" title="Restaurar " />
                            
                        </a>
                    </td>

                     <%} %>

                 <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <input id="<%# Eval("grupo") %>|<%# Eval("id") %>" value="<%# Eval("grupo") %>|<%# Eval("id") %>" type="checkbox">
                    </td>
                  <td>
                        <%# Eval("GRUPONOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      { %>
                    <td style="text-align:center;">
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("grupo") %>|<%# Eval("CODIGO") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                  <%} %>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                    { %>

                    <%if (getHabilitado())
                    {%>

                    <td style="text-align:center;">
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("grupo") %>|<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>

                     <%} else { %>

                     <td style="text-align:center;">
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("grupo") %>|<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/restaurar.png" border="0" title="Restaurar " /></a>
                    </td>

                    <%} %>

                   <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>