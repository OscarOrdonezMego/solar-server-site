﻿using System;
using System.Web.UI;
using Model.bean;
using Controller;
using System.Web.Services;
using Controller.functions;
using Model;

namespace Pedidos_Site.Mantenimiento.general
{
    public partial class general : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MANTTENIMIENTO_GENERAL;
        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            cargarTipo();
        }

        private void cargarTipo()
        {
            int item = 0;

            cboGrupo.Items.Insert(item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_TODOS));
            cboGrupo.Items[item].Value = "-1";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO_NOPEDIDO));
            cboGrupo.Items[item].Value = "1";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_CONDVENTAUPPERCASE));
            cboGrupo.Items[item].Value = "2";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_BANCO));
            cboGrupo.Items[item].Value = "3";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_TIPO_DOCUMENTO));
            cboGrupo.Items[item].Value = "4";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO_CANJE));
            cboGrupo.Items[item].Value = "5";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVODEVOLUCION));
            cboGrupo.Items[item].Value = "6";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_TIPOCLIENTE));
            cboGrupo.Items[item].Value = "7";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_FAMILIA));
            cboGrupo.Items[item].Value = "9";

            cboGrupo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MARCA));
            cboGrupo.Items[item].Value = "10";

            cboGrupo.Items.Insert(++item, "Grupo Económico");
            cboGrupo.Items[item].Value = "11";

            cboGrupo.Items.Insert(++item, "Tipo Moneda");
            cboGrupo.Items[item].Value = "12";

            cboGrupo.Items.Insert(++item, "Motivo Anulación Cobranza");
            cboGrupo.Items[item].Value = "13";

            cboGrupo.Items.Insert(++item, "Motivo Cambio Correlativo");
            cboGrupo.Items[item].Value = "14";
        }


        [WebMethod]
        public static void borrar(String codigos, String flag)
        {
            try
            {
                MantGeneralController.borrar(codigos, flag);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String grupo, String id, String gencodigo, String gennombre, String gennombrecorto, String nuevo, String nroDocumento, String banco, String fechaDiferida)
        {
            try
            {

                if (Utils.tieneCaracteresReservados(grupo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, grupo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, grupo));
                }
                else if (Utils.tieneCaracteresReservados(gencodigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, gencodigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, gencodigo));
                }

                else if (Utils.tieneCaracteresReservados(gennombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LOGIN, gennombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, gennombre));
                }

                else
                {
                    MantGeneralBean bean = new MantGeneralBean();
                    bean.id = id;
                    bean.codigo = gencodigo;
                    bean.nombrecorto = gennombrecorto;
                    bean.nombre = gennombre;
                    bean.grupo = grupo;
                    bean.nuevo = nuevo;
                    bean.banco = banco;
                    bean.nroDocumento = nroDocumento;
                    bean.fechaDiferida = fechaDiferida;

                    MantGeneralController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String grupo, String id, String gencodigo, String gennombre, String gennombrecorto, String nuevo, String nroDocumento, String banco, String fechaDiferida)
        {
            try
            {

                if (Utils.tieneCaracteresReservados(grupo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, grupo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, grupo));
                }
                else if (Utils.tieneCaracteresReservados(gencodigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, gencodigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, gencodigo));
                }

                else if (Utils.tieneCaracteresReservados(gennombrecorto))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LOGIN, gennombrecorto) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, gennombrecorto));
                }


                else
                {
                    MantGeneralBean bean = new MantGeneralBean();
                    bean.id = id;
                    bean.codigo = gencodigo;
                    bean.nombrecorto = gennombrecorto;
                    bean.nombre = gennombre;
                    bean.grupo = grupo;
                    bean.nuevo = nuevo;
                    bean.banco = banco;
                    bean.nroDocumento = nroDocumento;
                    bean.fechaDiferida = fechaDiferida;
                    MantGeneralController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}