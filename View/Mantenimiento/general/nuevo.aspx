﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.general.nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script>

        $(document).ready(function () {
            ocultar();
            $("#McboTipo").change(function () {

                if ($("#McboTipo").val() == "3") {
                    console.log($("#McboTipo").val());
                    $("#MtxtNombreCorto").prop("disabled", false);
                    $("#MtxtNombreCorto").val('');
                }
                else {
                    $("#MtxtNombreCorto").prop("disabled", true);
                    $("#MtxtNombreCorto").val('');
                }
                if ($("#McboTipo").val() == "4") {
                    $("#chkflagNroDocumento").css("display", "block");
                    $("#chkflagBanco").css("display", "block");
                    $("#chkflagFechaDiferida").css("display", "block");
                    $("#div1").css("display", "block");
                    $("#div2").css("display", "block");
                    $("#div3").css("display", "block");
                    $("#p1").css("display", "block");
                    $("#p2").css("display", "block");
                    $("#p3").css("display", "block");
                }
                else {
                    $("#chkflagNroDocumento").css("display", "none");
                    $("#chkflagBanco").css("display", "none");
                    $("#chkflagFechaDiferida").css("display", "none");
                    $("#div1").css("display", "none");
                    $("#div2").css("display", "none");
                    $("#div3").css("display", "none");
                    $("#p1").css("display", "none");
                    $("#p2").css("display", "none");
                    $("#p3").css("display", "none");
                    $("#chkflagNroDocumento").prop("checked", false);
                    $("#chkflagBanco").prop("checked", false);
                    $("#chkflagFechaDiferida").prop("cheked", false);
                }
            });

            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

        });
        function ocultar() {
            $("#chkflagNroDocumento").css("display", "none");
            $("#chkflagBanco").css("display", "none");
            $("#chkflagFechaDiferida").css("display", "none");
            $("#div1").css("display", "none");
            $("#div2").css("display", "none");
            $("#div3").css("display", "none");
            $("#p1").css("display", "none");
            $("#p2").css("display", "none");
            $("#p3").css("display", "none");
        }
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.grupo = $('#McboTipo').val();
            strData.id = $('#hidPk').val();

            strData.gencodigo = $('#MtxtCodigo').val();
            strData.gennombre = $('#MtxtNombre').val();

            strData.gennombrecorto = $('#MtxtNombreCorto').val();

            if ($('#hidPk').val().length != 0)
            { strData.nuevo = "0"; }
            else { strData.nuevo = "1"; }
            //            strData.supervisor = $('#chkSupervisor').attr("checked") ? 'T' : 'F';
            strData.nroDocumento = $('#chkflagNroDocumento').attr("checked") ? 'T' : 'F'
            strData.banco = $('#chkflagBanco').attr("checked") ? 'T' : 'F';
            strData.fechaDiferida = $('#chkflagFechaDiferida').attr("checked") ? 'T' : 'F';
            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtNombre').val('');
            $('#MtxtLogin').val('');
            $('#MtxtClave').val('');
            $('#McboPerfil').val($("#McboPerfil option:first").val());
            $('#McboGeocerca').val($("#McboGeocerca option:first").val());
            $('#MtxtNombreCorto').val('');
        }

        function MostrarBloquesOcultos() {
            $("#chkflagNroDocumento").css("display", "block");
            $("#chkflagBanco").css("display", "block");
            $("#chkflagFechaDiferida").css("display", "block");
            $("#div1").css("display", "block");
            $("#div2").css("display", "block");
            $("#div3").css("display", "block");
            $("#p1").css("display", "block");
            $("#p2").css("display", "block");
            $("#p3").css("display", "block");
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server"></h3>
        </div>
        <div id="myModalContent" class="modal-body">
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                *
                </p>
                <asp:DropDownList ID="McboTipo" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>

            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *
                </p>
                <input type="text" id="MtxtCodigo" runat="server" class="requerid cz-form-content-input-text"
                    maxlength="10" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                *
                </p>
                <input type="text" id="MtxtNombre" runat="server" class="requerid cz-form-content-input-text"
                    maxlength="50" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE_CORTO)%>
                *(<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_SOLO_BANCO)%>)
                </p>
                <input type="text" id="MtxtNombreCorto" runat="server" class="cz-form-content-input-text"
                    maxlength="20" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content" id="div1">
                <p id="p1">
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BANCO)%>
                </p>
                <input type="checkbox" class="cz-form-content-input-check" id="chkflagBanco" name="habilitado" runat="server"
                     />
            </div>
            <div class="cz-form-content" id="div2">
                <p id="p2">
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NRO_DOCUMENTO)%>
                </p>
                <input type="checkbox" class="cz-form-content-input-check" id="chkflagNroDocumento" name="habilitado" runat="server"
                   />
            </div>

            <div class="cz-form-content" id="div3">
                <p id="p3">
                    <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHA_DIFERIDA)%>
                </p>
                <input type="checkbox" class="cz-form-content-input-check" id="chkflagFechaDiferida" name="habilitado" runat="server"
                   />
            </div>

        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
        <asp:HiddenField ID="MhAccion" runat="server" />
        <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="" runat="server" />
        <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>
</body>
</html>
