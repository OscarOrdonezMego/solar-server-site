﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.general
{
    public partial class nuevo : Page
    {
        private Boolean flagcomboPerfil = false;
        private String usr_aplication = "";
        private String usr_perfil_sesion = "";
        private String usr_perfil = "";
        private String usr_codigo = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOREGGENERAL);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                usr_aplication = ((Model.bean.UsuarioBean)(Session[SessionManager.USER_SESSION])).codigo.ToString();
                usr_perfil_sesion = ((Model.bean.UsuarioBean)(Session[SessionManager.USER_SESSION])).codigoperfil.ToString();
                cargarPerfiles();

                if (dataJSON != null)
                {
                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARGENERAL);
                    MantGeneralBean usuario = MantGeneralController.info(dataJSON["codigo"]);
                    hidPk.Value = usuario.id;

                    MtxtNombre.Value = usuario.gruponombre;
                    MtxtCodigo.Value = usuario.codigo;
                    McboTipo.SelectedValue = usuario.grupo;
                    MtxtNombreCorto.Value = usuario.nombrecorto;
                    MtxtCodigo.Disabled = true;
                    McboTipo.Enabled = false;

                    if (McboTipo.SelectedValue.ToString() != "3")
                    {
                        MtxtNombreCorto.Disabled = true;
                    }
                    else
                    { MtxtNombreCorto.Disabled = false; }

                    if (McboTipo.SelectedValue.ToString().Equals("4"))
                    {

                        chkflagBanco.Checked = usuario.banco == "T" ? true : false;
                        chkflagNroDocumento.Checked = usuario.nroDocumento == "T" ? true : false;
                        chkflagFechaDiferida.Checked = usuario.fechaDiferida == "T" ? true : false;

                        Page.ClientScript.RegisterStartupScript(this.GetType(), "MostrarBloquesOcultos", "MostrarBloquesOcultos()", true);
                    }
                }
                else
                {
                    hidPk.Value = "";
                    MtxtNombreCorto.Disabled = true;
                }

            }

        }

        private void cargarPerfiles()
        {
            int item = 0;
            McboTipo.Items.Insert(item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO_NOPEDIDO));
            McboTipo.Items[item].Value = "1";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_CONDVENTAUPPERCASE));
            McboTipo.Items[item].Value = "2";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_BANCO));
            McboTipo.Items[item].Value = "3";

            //cmbPerfil.Items.Insert(++item, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_PAGOSDECOBRANZA));
            //cmbPerfil.Items[item].Value = "4";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_TIPO_DOCUMENTO));
            McboTipo.Items[item].Value = "4";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVO_CANJE));
            McboTipo.Items[item].Value = "5";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MOTIVODEVOLUCION));
            McboTipo.Items[item].Value = "6";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_TIPOCLIENTE));
            McboTipo.Items[item].Value = "7";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_FAMILIA));
            McboTipo.Items[item].Value = "9";

            McboTipo.Items.Insert(++item, IdiomaCultura.getMensaje(IdiomaCultura.WEB_MARCA));
            McboTipo.Items[item].Value = "10";

            McboTipo.Items.Insert(++item, "Grupo Económico");
            McboTipo.Items[item].Value = "11";

            McboTipo.Items.Insert(++item, "Tipo Moneda");
            McboTipo.Items[item].Value = "12";
            
            McboTipo.Items.Insert(++item, "Motivo Anulación Cobranza");
            McboTipo.Items[item].Value = "13";
            
            McboTipo.Items.Insert(++item, "Motivo Cambio Correlativo");
            McboTipo.Items[item].Value = "14";
        }
    }
}