﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.formulario.actividad_tipo_tipo" Codebehind="formulario.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.8.22.custom.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <link href="../../css/formulary.css" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/jquery.jsPlumb-1.3.16-all-min.js" type="text/javascript"></script>
    
    <%--  <script src="../../js/form/jqFunciones.js" type="text/javascript"></script>
    <script src="../../js/form/jqEventos.js" type="text/javascript"></script>
    --%>
    <style>
        .cz-form-box-content
        {
            overflow: inherit;
        }
    </style>
    <script>
        var arrEtapas = [];
        var eliminando = false;
        var nodoAnterior = 'Pantallainicio';
        iM = 2000;
        var nextEtapa = 0;
        var nextControl = 0;
        var nextOpcion = 0;
        var delOpciones = [];
        var anchoVentana = 1024;
        var anchoPantalla = 160;
        var ctrlSel = "0";
       
       
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <input id="hidPk" runat="server" type="hidden"/>
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div id="contenedorEtapa" class="cz-form-box-content cz-util-nopad cz-content-full"
                style="margin-top: 0px">

                

                <div id="lstControles" class="lstControles cz-form-box-content cz-util-nopad" style="width: 360px;
                    float: right; margin-top: 0px; margin-right: 10px;">
                    <div class="cz-util-pad">

                   

                        <div id="cz-menu-lateral-options" style="width: 304px;">

                          

                            <div class="cz-menu-lateral-option" id="plusControl"  style="width: 304px;">
                                <div id="cz-menu-lateral-option-WEB_CONTROL" class="cz-menu-lateral-title">
                                    <div class="cz-menu-lateral-title-icon" style="overflow: hidden; width: 0px;">
                                    </div>
                                    <div class="cz-menu-lateral-title-text" style="overflow: hidden; width: 150px;">
                                        Controles</div>
                                    <div class="cz-menu-lateral-title-status">
                                        +</div>
                                </div>
                                <div id="divFil" class="cz-menu-lateral-suboptions" style="height: 0px;">
                                    <asp:Literal ID="ltrControles" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="cz-form-content-wall">
                            </div>


                            
                        </div>
                        <div class="cz-menu-lateral-option" id="tControl" style="display: none;">
                            <div id="idPropertyControl" class="cz-menu-lateral-title config-form">
                                <div class="cz-menu-lateral-title-icon" style="overflow: hidden; width: 0px;">
                                </div>
                                <div class="cz-menu-lateral-title-text" style="overflow: hidden; width: 150px;">
                                    Propiedades Control</div>
                                <div class="cz-menu-lateral-title-status">
                                    +</div>
                            </div>
                            <div id="divControles" class="cz-menu-lateral-suboptions" style="height: 0px;">
                                <input id="hidControl" class="hidControl" type="hidden"/>
                                <div id="CEtiqueta" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Etiqueta*</p>
                                    <input type="text" id="txtCEtiqueta" class="cz-form-content-input-text" maxlength="25" />
                                    <div class="cz-form-content-input-text-visible">
                                        <div class="cz-form-content-input-text-visible-button">
                                        </div>
                                    </div>
                                </div>
                                <div id="CMaxCaracter" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Max Caracteres</p>
                                    <input type="text" id="txtCMaxCaracter" onkeypress="fc_PermiteNumeros();" class=" cz-form-content-input-text"
                                        maxlength="25" />
                                    <div class="cz-form-content-input-text-visible">
                                        <div class="cz-form-content-input-text-visible-button">
                                        </div>
                                    </div>
                                </div>
                                <div id="CObligatorio" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Obligatorio</p>
                                    <input type="checkbox" class="cz-form-content-input-check" id="chkCObligatorio" checked="checked" />
                                </div>
                                <div id="CEditable" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Editable</p>
                                    <input type="checkbox" class="cz-form-content-input-check" id="chkCEditable" checked="checked" />
                                </div>
                                <div id="CActualizar" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Actualizar</p>
                                    <input type="checkbox" class="cz-form-content-input-check" id="chkCActualizar" checked="checked" />
                                </div>
                                <div id="CGrupo" class="cz-form-content cz-menu-lateral-suboption" style="height: 58px;
                                    background-image: none;">
                                    <p>
                                        Grupo</p>
                                    <input id="hCodGrupo" class="hCodGrupo" type="hidden"/>
                                    <input type="text" id="txtCGrupo" readonly disabled="disabled" class=" cz-form-content-input-calendar"
                                        maxlength="25" />
                                    <div class="cz-form-content-input-calendar-visible" style="position: inherit;">
                                        <div id="btngrupo" class="cz-form-content-input-calendar-visible-button" style="text-align: center;
                                            font-weight: bolder;">
                                            ...
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="dvGrupo" class="cz-menu-lateral-suboptions" style="height: 0px; display: none;">
                                <div class="cz-form-content cz-menu-lateral-suboption" style="height: 58px; background-image: none;">
                                    <div id="cbogroup">
                                        <p>
                                            Grupos</p>
                                        <asp:DropDownList ID="cboGrupo" runat="server" CssClass="cz-form-content-input-select">
                                        </asp:DropDownList>
                                        <div class="cz-form-content-input-select-visible" style="position: inherit;">
                                            <p class="cz-form-content-input-select-visible-text">
                                            </p>
                                            <div class="cz-form-content-input-select-visible-button">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="txtgroup" style="display: none;">
                                        <p>
                                            Grupo*</p>
                                        <input id="hvalGrupo" type="hidden" />
                                        <input name="txtgrupo" runat="server" type="text" id="txtgrupo" maxlength="100" class="cz-form-content-input-text"/>
                                        <div class="cz-form-content-input-text-visible">
                                            <div class="cz-form-content-input-text-visible-button">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cz-form-content cz-menu-lateral-suboption" style="height: 58px; background-image: none;">
                                    <div style="padding-top: 10px; text-align: center;">
                                        <a class="okItem" style="padding-left: 5px" title="OK">
                                            <img src="../../imagery/all/icons/detalle.png"></a> <a class="newItem" style="padding-left: 5px"
                                                title="Nuevo">
                                                <img src="../../imagery/all/icons/add.png"></a> <a class="editItem" style="padding-left: 5px"
                                                    title="Editar">
                                                    <img src="../../imagery/all/icons/modificar.png"></a> <a class="deleteItem" style="padding-left: 5px"
                                                        title="Eliminar">
                                                        <img src="../../imagery/all/icons/eliminar.png"></a> <a class="cancelItem" style="padding-left: 5px"
                                                            title="Cancelar">
                                                            <img src="../../imagery/all/icons/cancel.png"></a> <a class="saveItem" style="padding-left: 5px;
                                                                display: none;" title="Guardar">
                                                                <img src="../../imagery/all/icons/save.png"></a> <a class="cancelarItem" style="padding-left: 5px;
                                                                    display: none;" title="Guardar">
                                                                    <img src="../../imagery/all/icons/cancel.png"></a>
                                    </div>
                                </div>
                                <div class="tblOpciones" style="overflow: auto; height: 150px; float: left;width:100%;">
                                </div>
                            </div>
                        </div>

                        <div class="  cz-menu-lateral-suboption" style="background-image: none;">
                                    
                                    <input type="button" id="saveReg" class="cz-form-content-input-button " value="Guardar"/>
                                    <input type="button" id="btnCancelar" class="cz-form-content-input-button " value="Cancelar"/>
                                </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
    </div>
    </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            inicioJPlumb();
            //iniciarNodos();
            eventsForm();
            grupos();

            <% if(TPCP.Valor=="T"){ %>
            agregarEtapa($('#contenedorEtapa'), "f1", "Inicio", 0, 0, 20, 20, 5);
            <%} %>
            <% if(TPFP.Valor=="T"){ %>
            agregarEtapa($('#contenedorEtapa'), "f2", "Fin", "f1", 0, 200, 20, 5);
            <%} %>
             <% if(TPFP.Valor=="T" && TPCP.Valor=="T"){ %>
            crearRelaciones('cn' + 'f1', 'f1', 'f2', 'N', '', '');
            <%} %>

            cargarFormulario();

            
        });
    </script>
    <script src="../../js/forms/JSForm.js" type="text/javascript"></script>
    <script src="../../js/tablednd.js" type="text/javascript"></script>
</body>
</html>
