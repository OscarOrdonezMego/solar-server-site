﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using Controller;
using Controller.functions;
using Model.bean;
using System.Text;
using System.Data;

namespace Pedidos_Site.Mantenimiento.formulario
{
    public partial class actividad_tipo_tipo : PageController
    {
        public ConfiguracionBean TPCP = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPCP"];
        public ConfiguracionBean TPFP = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFP"];

        protected override void initialize()
        {

            if (!IsPostBack)
            {
                // ltrEstados.Text = cargaEstados();
                ltrControles.Text = cargaControl();
                cargarcombos();
                /*    hidPk.Value = "";
                    if (Request.QueryString["id"] != null)
                    {
                        hidPk.Value = Request.QueryString["id"];
                    }
                    */
            }
        }


        private void cargarcombos()
        {

            DataTable dt = EstadoController.findGRUPOS();
            cboGrupo.DataSource = dt;
            cboGrupo.DataValueField = "idgrupo";
            cboGrupo.DataTextField = "nombre";

            try
            {
                cboGrupo.DataBind();
                cboGrupo.Items.Insert(0, "--" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_SEL_DATA) + "--");
                cboGrupo.Items[0].Value = "-1";
            }
            catch (Exception) { }


        }

        private static string cargaControl()
        {

            StringBuilder sbFil = new StringBuilder();

            foreach (DataRow dr in GeneralController.findCONTROLES().Rows)
            {

                sbFil.Append(" <div id='C" + dr["IdTipoControl"].ToString() + "' class=\"toolboxItem cz-menu-lateral-suboption \"control='" + dr["IdTipoControl"].ToString() + "' style='background-color:white;display: block;margin-bottom: 5px;' \">");

                sbFil.Append("<span> " + Utils.FirstCharToUpper(dr["Nombre"].ToString()) + "</span>");

                sbFil.Append("</div>");
            }


            return sbFil.ToString();
        }


        private static String cargaDataOpc(DataTable dt)
        {

            StringBuilder t1 = new StringBuilder();

            t1.Append("<table style='width:100%;' id='opciones' class='grilla table table-bordered table-striped'>\n");
            t1.Append("<thead>\n");
            t1.Append("<tr><th>CODIGO</th><th>NOMBRE</th><th></th></tr>\n");
            t1.Append("</thead>\n");
            t1.Append("<tbody>\n");

            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                t1.Append("<tr class='disable'>\n");
                t1.Append("<td>\n");
                t1.Append("<input style='width: 50px;'  id='txtOPCodigo" + i.ToString() + "' disabled class='txtOPCodigo ' type='text' value='" + dr["CodGrupoDetalle"].ToString() + "'> \n");
                t1.Append("</td>\n");
                t1.Append("<td>\n");
                t1.Append("<input id='txtOPNombre" + i.ToString() + "' class='txtOPNombre ' disabled type='text' value='" + dr["Nombre"].ToString() + "' onblur='grabarTROPC(this);'> \n");
                t1.Append("</td>\n");
                t1.Append("<td>\n");
                t1.Append("<a class='delRowOPC' title='Eliminar'><img src='../../imagery/all/icons/eliminar.png'></a>\n");
                t1.Append("</td>\n");
                t1.Append("</tr>\n");
                i = i + 1;
            }

            t1.Append("<tr class='nodrop nodrag'>\n");
            t1.Append("<td>\n");
            t1.Append("<input style='width: 50px;'  id='txtOPCodigo" + i.ToString() + "' class='txtOPCodigo ' type='text'> \n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<input id='txtOPNombre" + i.ToString() + "' class='txtOPNombre ' type='text' onblur='grabarTROPC(this);'> \n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<a class='delRowOPC' title='Eliminar'><img src='../../imagery/all/icons/eliminar.png'></a>\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");

            t1.Append("</tbody>\n");
            t1.Append("</table>\n");

            return t1.ToString();
        }


        private static String cargaIniOpc()
        {

            StringBuilder t1 = new StringBuilder();

            t1.Append("<table style='width:100%;' id='opciones' class='grilla table table-bordered table-striped'>\n");
            t1.Append("<thead>\n");
            t1.Append("<tr><th>CODIGO</th><th>NOMBRE</th><th></th></tr>\n");
            t1.Append("</thead>\n");
            t1.Append("<tbody>\n");
            t1.Append("<tr class='nodrop nodrag'>\n");
            t1.Append("<td>\n");
            t1.Append("<input  id='txtOPCodigo1' style='width: 50px;' class='txtOPCodigo ' type='text'> \n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<input id='txtOPNombre1' class='txtOPNombre ' type='text' onblur='grabarTROPC(this);'> \n");
            t1.Append("</td>\n");
            t1.Append("<td>\n");
            t1.Append("<a class='delRowOPC' title='Eliminar'><img src='../../imagery/all/icons/eliminar.png'></a>\n");
            t1.Append("</td>\n");
            t1.Append("</tr>\n");
            t1.Append("</tbody>\n");
            t1.Append("</table>\n");

            return t1.ToString();
        }

        [WebMethod]
        public static string delGrupos(String codigo)
        {

            EstadoController.borrarGrupo(codigo);
            DataTable dt = EstadoController.findGRUPOS();
            StringBuilder resultado = new StringBuilder();

            foreach (DataRow dr in dt.Rows)
            {
                resultado.Append("<option value=" + dr["idgrupo"].ToString() + ">" + dr["nombre"].ToString() + "</option>\n");

            }
            return resultado.ToString();
        }

        [WebMethod]
        public static string saveGrupo(String codigo, String nombre, List<OpcionBean> opciones)
        {
            GrupoBean bean = new GrupoBean();
            bean.codigo = codigo;
            bean.nombre = nombre;
            bean.opciones = opciones;

            EstadoController.crearGrupo(bean);
            DataTable dt = EstadoController.findGRUPOS();
            StringBuilder resultado = new StringBuilder();

            foreach (DataRow dr in dt.Rows)
            {
                resultado.Append("<option value=" + dr["idgrupo"].ToString() + ">" + dr["nombre"].ToString() + "</option>\n");

            }
            return resultado.ToString();
        }

        [WebMethod]
        public static string listGrupos()
        {
            DataTable dt = EstadoController.findGRUPOS();
            StringBuilder resultado = new StringBuilder();

            foreach (DataRow dr in dt.Rows)
            {
                resultado.Append("<option value=" + dr["IdGrupo"].ToString() + ">" + dr["Nombre"].ToString() + "</option>\n");

            }
            return resultado.ToString();
        }

        [WebMethod]
        public static string listOpcion(String codigo)
        {

            String respuesta = "";
            if (codigo != "")
            {
                DataTable dt = EstadoController.findOPCIONES(codigo);
                respuesta = cargaDataOpc(dt);
            }
            else
            {
                respuesta = cargaIniOpc();
            }


            return respuesta;
        }


        [WebMethod]
        public static string save(List<BEEstados> estados)
        {
            try
            {

                EstadoController.crear(estados);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [WebMethod]
        public static string load()
        {
            try
            {

                return EstadoController.load();
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}