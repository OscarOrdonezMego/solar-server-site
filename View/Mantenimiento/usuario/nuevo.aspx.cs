﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using System.Text;

namespace Pedidos_Site.Mantenimiento.usuario
{
    public partial class Nuevo : Page
    {
        private Boolean flagcomboPerfil = false;
        private String usr_aplication = "";
        private String usr_perfil_sesion = "";
        private String usr_perfil = "";
        private String usr_codigo = "";
        public static StringBuilder codigogrupo = null;
        public static StringBuilder codigogrupos = null;
        private int item = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                codigogrupo = null;
                codigogrupos = null;

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOUSUARIO);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                usr_aplication = ((UsuarioBean)(Session[SessionManager.USER_SESSION])).codigo.ToString();
                usr_perfil_sesion = Session["lgn_codsupervisor"].ToString();

                //cargarPerfiles();

                llenarComboBox();
                if (dataJSON != null)
                {
                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARUSUARIO);
                    UsuarioBean usuario = UsuarioController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"];

                    MtxtNombre.Value = usuario.nombre;
                    MtxtCodigo.Value = usuario.codigo;
                    MtxtLogin.Value = usuario.login;
                    hidClave.Value = usuario.clave;
                    MtxtSerie.Value = usuario.serie;
                    MtxtCorrelativo.Value = usuario.correlativo;
                    MtxtClave.Attributes.Add("Value", usuario.clave);
                    McboPerfil.SelectedValue = usuario.codigoperfil.Trim();
                    GrupoBean bean = UsuarioController.BuscarGrupo(dataJSON["codigo"].ToString());
                    if (bean != null)
                    {
                        codigogrupo = new StringBuilder();
                        codigogrupo.Append(bean.codigo);
                    }
                    List<GrupoBean> lista = UsuarioController.ListaGrupoSupervisor(Int32.Parse(dataJSON["codigo"].ToString()));
                    if (lista.Count > 0)
                    {
                        codigogrupos = new StringBuilder();
                        codigogrupos.Append("[");
                        for (Int32 i = 0; i < lista.Count; i++)
                        {
                            codigogrupos.Append(lista[i].Grupo_PK + ",");

                        }
                        codigogrupos.Append("]");
                    }

                    if (usuario.codigo.Trim() == "DBO")
                    {
                        McboPerfil.Enabled = false;
                    }
                }
                else
                {
                    hidPk.Value = "";
                }

                if (!usr_aplication.Equals("IORDER") && !usr_aplication.Equals("ADMIN") && !usr_aplication.Equals(MtxtCodigo.Value)
                   && McboPerfil.SelectedValue != "2" && McboPerfil.SelectedValue != "3" && McboPerfil.SelectedValue != "1")
                {
                    saveReg.Disabled = false;
                }
            }
        }

        private void cargarPerfiles()
        {
            if (usr_aplication.Equals("DBO"))
            {
                McboPerfil.Items.Insert(item, "ADMINISTRADOR");
                McboPerfil.Items[item].Value = "ADM";
                item++;
                McboPerfil.Items.Insert(item, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_VENDEDOR));
                McboPerfil.Items[item].Value = "VEND";
            }
            else
            {
                McboPerfil.Items.Insert(0, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_VENDEDOR));
                McboPerfil.Items[item].Value = "VEND";
                McboPerfil.Items.Insert(item + 1, "super");
                McboPerfil.Items[item + 1].Value = "VEND";
            }
        }

        private void llenarComboBox()
        {
            List<PerfilBean> lista = UsuarioController.obtenerListaPerfilCrear(usr_perfil_sesion);
            if (lista.Count != 0)
            {
                for (int i = 0; i < lista.Count; i++)
                {
                    McboPerfil.Items.Insert(i, lista[i].nombre.ToString());
                    McboPerfil.Items[i].Value = lista[i].codigorol.ToString();
                }
            }
        }
    }
}