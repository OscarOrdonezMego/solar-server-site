﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.usuario.Grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <style type="text/css">
        .Hijo {
            margin: 14px;
            font-family: sans-serif;
            color: beige;
        }

        .subhijo {
            margin: 14px;
            font-size: 17px;
        }
    </style>
    <script type="text/javascript">

        $('.grupro').click(function () {

            var UrlGrupo = "gridGrupo.aspx";
            var obj = new Object();

            obj.usupk = $(this).attr('cod');
            obj.tipo = $(this).attr('tipo').trim();
            $.ajax({
                type: 'POST',
                url: UrlGrupo,
                data: JSON.stringify(obj),
                success: function (data) {
                    $("#myModal").html(data);
                    $('#myModal').modal('show');
                    
                   /* $('#vergrupo').click(function () {
                        buscarGrupo(obj.usupk, obj.tipo);
                    });
                    $('#vergrupo').trigger('click');*/
                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        });
        function buscarGrupo(codigo, tipo) {
            var UrlGrupo = "gridGrupo.aspx/listaGrupo";
            var obj = new Object();
            obj.cod = codigo;
            obj.tipo = tipo;
            $.ajax({
                url: UrlGrupo,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (tipo == "SUP") {
                        if (msg.d != "") {
                            $('.Hijo').append(msg.d);
                        } else {
                            $('.Hijo').append("<h2>No tiene Grupo.</h2>");
                            $('h2').css("color", "red");
                        }

                    } else {
                        if (msg.d != "") {
                            $('#subgrupo').append("<h2>" + msg.d + "</h2>");
                            $('h2').css("color", "rgb(102, 102, 102)");
                            $('h2').css("font-size", "16px");
                        } else {
                            $('#subgrupo').append("<h2>No tiene Grupo.</h2>");
                            $('h2').css("color", "red");
                            $('h2').css("font-size", "16px");
                        }
                    }


                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
    </script>
 
</head>
<body>
    <form id="form3" runat="server">
        <div id="divGridView" runat="server">
            <asp:Repeater ID="grdMant" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%" class="grilla table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 40px;" class="center">
                                    <input id="ChkAll" name="chkSelectAll" value="<%# Eval("ID") %>" type="checkbox">
                                </th>
                                <th style="width: 100px;" class="center" nomb="CODIGO">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                                </th>
                                <th nomb="NOMBRE">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                                </th>
                                <th nomb="LOGIN">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_LOGIN)%>
                                </th>
                                <th nomb="PERFIL">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PERFIL)%>
                                </th>
                                <th nomb="NOMBREPERFIL">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBREGRUPO)%>
                                </th>
                                <th nomb="SERIE">Serie</th>
                                <th nomb="CORRELATIVO">Correlativo</th>
                                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                                  { %>
                                <th style="width: 40px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                                </th>
                                <%} %>
                                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                                  { %>
                                <th style="width: 40px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                                </th>
                                <%} %>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                        </td>
                        <td>
                            <%# Eval("CODIGO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("LOGIN")%>
                        </td>
                        <td>
                            <%# Eval("NOMBREPERFIL")%>
                        </td>
                        <td>
                            <center>
                                <a class="grupro" style="cursor: pointer;" data-toggle="modal" tipo="<%# Eval("VEN_PERFIL_VALUE") %>" cod="<%# Eval("ID") %>">
                                    <img src="../../imagery/all/icons/detalle.png" border="0" title="Buscar Grupo " /></a></center>
                        </td>
                        <td>
                            <%# Eval("serie")%>
                        </td>
                         <td>
                            <%# Eval("correlativo")%>
                        </td>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                          { %>
                        <td>
                            <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                                <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                        </td>
                        <%} %>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                          { %>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                                cod="<%# Eval("ID") %>">
                                <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                        </td>
                        <%} %>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt">
                        <td>
                            <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                        </td>
                        <td>
                            <%# Eval("CODIGO")%>
                        </td>
                        <td>
                            <%# Eval("NOMBRE")%>
                        </td>
                        <td>
                            <%# Eval("LOGIN")%>
                        </td>
                        <td>
                            <%# Eval("NOMBREPERFIL")%>
                        </td>
                        <td>
                            <center>
                                <a class="grupro" style="cursor: pointer;" data-toggle="modal" tipo="<%# Eval("VEN_PERFIL_VALUE") %>" cod="<%# Eval("ID") %>">
                                    <img src="../../imagery/all/icons/detalle.png" border="0" title="Buscar Grupo " /></a></center>
                        </td>
                        <td>
                            <%# Eval("serie")%>
                        </td>
                         <td>
                            <%# Eval("correlativo")%>
                        </td>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                          { %>
                        <td>
                            <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                                <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                        </td>
                        <%} %>
                        <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                          { %>
                        <td>
                            <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                                cod="<%# Eval("ID") %>">
                                <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                        </td>
                        <%} %>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Pagina</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de</span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            buscando resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">
                    ×
                </div>
            </div>
        </div>
    </form>
</body>
</html>
