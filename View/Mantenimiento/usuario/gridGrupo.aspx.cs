﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller;
using Model.bean;

namespace Pedidos_Site.Mantenimiento.usuario
{
    public partial class Mantenimiento_usuario_gridGrupo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                List<GrupoBean> listaGrupoMostrar = listaGrupo(dataJSON["usupk"].ToString(), dataJSON["tipo"].ToString());
                if (listaGrupoMostrar.Count > 0)
                {
                    for (Int32 i = 0; i < listaGrupoMostrar.Count; i++)
                    {

                        dimostrarmsg.InnerHtml += "<h2>" + listaGrupoMostrar[i].nombre + "</h2>";


                    }

                }
                else
                {
                    dimostrarmsg.InnerHtml += "<h2>No Tiene Grupo</h2>";
                }


            }
        }

        public static List<GrupoBean> listaGrupo(String cod, String tipo)
        {

            try
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.ListaGrupoSupervisor(Int32.Parse(cod));
                List<GrupoBean> Rpta = lstGrupoBean;
                /*  if (lstGrupoBean.Count > 0)
                  {
                      var conta = 0;
                      for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                      {
                          Rpta += ToolBox.DIV("", "subhijo", (conta = conta + 1) + "°_ " + lstGrupoBean[i].nombre);
                      }
                  }*/

                /*
                else

                    Rpta=new List<GrupoBean>();
                    GrupoBean Obj = UsuarioController.BuscarGrupo(cod);
                    Rpta.Add(Obj);
                 /*   if (Obj != null)
                    {
                        Rpta = Obj.nombre;
                    }*/


                return Rpta;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}