﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Mantenimiento.usuario.Nuevo" CodeBehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .ms-parent {
            margin-top: 0px;
            margin-left: 12px;
        }

        .modal-body {
            overflow: initial;
        }

        .modal-footer {
            float: right;
            padding: 9px 9px 9px 464px;
        }

        .form-group {
            border: 0px solid;
            width: 232px;
            height: 60px;
            float: left;
        }

        p.mip {
            padding-left: 12px;
            font-family: sans-serif;
        }

        .ms-drop ul {
            height: 122px;
        }

        .ms-choice {
            margin-top: 0px;
        }

        .ms-drop ul {
            width: 184px;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#saveReg').removeAttr("disabled");
            var nu = 0;
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });


            $('#McboPerfil').change(function () {
                llenarCombo($(this).val());
            });

            $('#McboPerfil').trigger('change');

        });

        function deshabilitardiv() {
            $('#McboPerfil').change(function () {
                if ($(this).val() === 'DBO' || $(this).val() === 'ADM') {
                    $('#ven').css('display', 'none');
                }
            });
        }

        function llenarCombo(Tipo) {
            var URL = "usuario.aspx/ListaGrupoCbo";
            var obj = new Object();
            obj.tipo = Tipo;
            $.ajax({
                url: URL,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (Tipo == 'VEN' || Tipo == 'ADM' || Tipo == 'DBO') {
                        $('#cbovendedor option').remove();
                        if (msg.d == 'NR') {
                            $('#cbovendedor').append("<option value=\"\">No Hay Registro.</option>");
                            $('#cbovendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cbovendedor').find("option:selected").text());
                        } else {
                            $('#cbovendedor').append(msg.d);
                        }
                        $('#cbosupervisor option').remove();
                        $('#cbovendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cbovendedor').find("option:selected").text());

                        $('#sup').css('display', 'none');
                        if (Tipo == 'DBO' || Tipo == 'ADM') {
                            $('#ven').css('display', 'none');
                        } else {
                            $('#ven').css('display', 'block');
                        }
                        var cod = '<%=codigogrupo%>';
                        if (cod != null) {
                            buscarGrupo(cod);
                        }



                    }
                    if (Tipo == 'SUP') {
                        $('#cbovendedor option').remove();
                        $('#cbosupervisor option').remove();
                        if (msg.d == 'NR') {
                            $('#cbosupervisor').append("<option value=\"\">No Hay Registro.</option>");
                            $('#cbosupervisor').parent().find(".cz-form-content-input-select-visible-text").html($('#cbosupervisor').find("option:selected").text());
                        } else {
                            $('#cbosupervisor').append(msg.d);
                        }
                        $('#cbosupervisor').multipleSelect({
                            placeholder: "Seleccionar Grupo",
                            width: '205px',
                            multiple: true
                        });
                        $('#sup').css('display', 'block');
                        $('#ven').css('display', 'none');
                        var cod = '<%=codigogrupos%>'
                        if (cod != null) {
                            buscarGrupos(cod);
                        }


                    }

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function buscarGrupo(codigo) {
            $('#cbovendedor').val(codigo);
            $('#cbovendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cbovendedor').find("option:selected").text());
        }
        function buscarGrupos(codigos) {
            $('#cbosupervisor').multipleSelect("setSelects", codigos);
        }
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.codigo = $('#MtxtCodigo').val();
            strData.nombre = $('#MtxtNombre').val();
            strData.login = $('#MtxtLogin').val();
            strData.clave = $('#MtxtClave').val();
            strData.clave2 = $('#hidClave').val();
            strData.codigoperfil = $('#McboPerfil').val();
            strData.serie = $('#MtxtSerie').val();
            strData.correlativo = $('#MtxtCorrelativo').val();
            strData.accion = $('#MhAccion').val();
            if ($('#cbovendedor').val() != null) {
                strData.codgrupo = $('#cbovendedor').val();
            } else {
                strData.codgrupo = "" + $("#cbosupervisor").multipleSelect("getSelects");
            }
            strData.supervisor = $('#chkSupervisor').attr("checked") ? 'T' : 'F';

            return strData;
        }

        function data() {
            var sr = new getData();
            return sr.codigoperfil;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtNombre').val('');
            $('#MtxtLogin').val('');
            $('#MtxtClave').val('');
            $('#McboPerfil').val($("#McboPerfil option:first").val());
            $('#McboGeocerca').val($("#McboGeocerca option:first").val());
            $('#MtxtSerie').val('');
            $('#MtxtCorrelativo').val('');
        }
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server"></h3>
        </div>
        <div id="myModalContent" class="modal-body">
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *
                </p>
                <input type="text" id="MtxtCodigo" runat="server" class="requerid cz-form-content-input-text"
                    maxlength="20" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                *
                </p>
                <input type="text" id="MtxtNombre" runat="server" class="requerid cz-form-content-input-text"
                    maxlength="100" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_LOGIN)%>
                *
                </p>
                <input type="text" id="MtxtLogin" runat="server" class="requerid cz-form-content-input-text"
                    maxlength="10" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLAVE)%>
                *
                </p>
                <asp:TextBox ID="MtxtClave" TextMode="Password" CssClass="requerid cz-form-content-input-text" runat="server" MaxLength="10"></asp:TextBox>
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PERFIL)%>
                *
                </p>
                <asp:DropDownList ID="McboPerfil" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content" id="ven">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                *
                </p>
                <select id="cbovendedor" runat="server" class="requerid select1 cz-form-content-input-select">
                </select>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>

            <div class="form-group" id="sup" style="display: none">
                <p class="mip">
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%> *
                </p>
                <select id="cbosupervisor" class="select2" multiple="multiple">
                </select>
            </div>
            <div class="cz-form-content">
                <p>Serie:</p>
                <input type="text" id="MtxtSerie" runat="server" class="requerid cz-form-content-input-text" maxlength="5" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>Correlativo:</p>
                <input type="text" id="MtxtCorrelativo" runat="server" class="requerid cz-form-content-input-text" maxlength="10" />
                <div class="cz-form-content-input-text-visible">
                    <div class="cz-form-content-input-text-visible-button">
                    </div>
                </div>
            </div>
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
        <div class="modal-footer">
            <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
                value="GUARDAR" />
        </div>
        <asp:HiddenField ID="llenarCombo" runat="server" />
        <asp:HiddenField ID="llenarCombo2" runat="server" />
        <asp:HiddenField ID="MhAccion" runat="server" />
        <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
        <asp:HiddenField ID="vergrupo" Value="" runat="server" />

        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="" runat="server" />
        <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>
</body>
</html>
