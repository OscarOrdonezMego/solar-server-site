﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.usuario
{
    public partial class Grid : PageController
    {
        public static String lsCodMenu = "MUS";
        PaginateUsuarioBean paginate;
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);


            String codigo = dataJSON["codigo"].ToString();
            String nombre = dataJSON["nombre"].ToString();
            String login = dataJSON["login"].ToString();
            String perfil = dataJSON["perfil"].ToString();
            Int32 pagina = Int32.Parse(dataJSON["pagina"].ToString());
            Int32 filas = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
            String flag = dataJSON["flag"].ToString();
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String serie = dataJSON["serie"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }
            paginate = UsuarioController.paginarBuscar(codigo, login, nombre, perfil, flag, "", pagina, filas, perfilUsu, codgrupo, id_usu, serie);

            if ((pagina > 0) && (pagina <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina.ToString();

                if (pagina == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (pagina == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<UsuarioBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }
    }
}