﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using System.Web.Services;
using Controller.functions;
using System.Web.Security;
using Model;

namespace Pedidos_Site.Mantenimiento.usuario
{
    public partial class Usuario : PageController
    {
        private String usr_aplication = "";
        private String usr_sesion_perfil = "";
        public static String lsCodMenu = "MUS";
        public static Int32 USU_PK = 0;
        public static String Codigo_Usu = "";
        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            usr_sesion_perfil = Session["lgn_codsupervisor"].ToString();
            llenarComboBox(usr_sesion_perfil);
            llenarCboGrupos(Int32.Parse(Session["lgn_id"].ToString()), usr_sesion_perfil);
            USU_PK = Int32.Parse(Session["lgn_id"].ToString());
            usr_aplication = ((Model.bean.UsuarioBean)(Session["USER_SESSION"])).codigo;
            //usr_sesion_perfil = ((Model.bean.UsuarioBean)(Session["USER_SESSION"])).codigoperfil;

            //cargarPerfiles();
            Codigo_Usu = Session["lgn_codigo_usu"].ToString();

        }

        private void cargarPerfiles()
        {

            cboPerfil.Items.Insert(0, "VENDEDOR");
            cboPerfil.Items[0].Value = "VEND";
            int item = 1;
            if (usr_aplication.Equals("IORDER") || usr_sesion_perfil.Equals("SUP"))
            {
                cboPerfil.Items.Insert(item, "SUPERVISOR");
                cboPerfil.Items[item].Value = "SUP";
                item++;
            }

            cboPerfil.Items.Insert(item, "ADMINISTRADOR");
            cboPerfil.Items[item].Value = "ADM";

        }
        private void llenarComboBox(String perfil)
        {
            List<PerfilBean> lista = UsuarioController.obtenerListaPerfil(perfil);
            if (lista.Count != 0)
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = " ";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboPerfil.Items.Insert(i + 1, lista[i].nombre);
                    cboPerfil.Items[i + 1].Value = lista[i].codigo;
                }
            }
        }
        private void llenarCboGrupos(Int32 USUPK, String TIPO)
        {
            List<GrupoBean> loLstGrupoBean = UsuarioController.fnListarGrupoActivo(USUPK, TIPO);
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }

        [WebMethod]
        public static void borrar(String codigos, String flag)
        {
            try
            {
                UsuarioController.borrar(codigos, flag);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [WebMethod]
        public static String ListaGrupoCbo(String tipo)
        {
            String data = "";
            if (tipo.Equals("SUP"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count > 0)
                {
                    for (Int32 i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        data += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    data = "NR";
                }
            }
            else
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count > 0)
                {
                    for (Int32 i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        data += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    data = "NR";
                }
            }

            return data;
        }

        [WebMethod]
        public static String crear(String id, String codigo, String login, String nombre, String codigoperfil, String clave, String clave2, String codgrupo, String serie, String correlativo)
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }

                else if (Utils.tieneCaracteresReservados(login))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LOGIN, login) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, login));
                }

                else if (Utils.tieneCaracteresReservados(clave))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLAVE, clave) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, clave));
                }

                else
                {
                    UsuarioBean bean = new UsuarioBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.login = login.ToUpper();
                    bean.nombre = nombre.ToUpper();
                    bean.codigoperfil = codigoperfil;
                    bean.serie = serie;
                    bean.correlativo = correlativo;
                    bean.clave = clave2;
                    if (clave2 != clave)
                    {
                        bean.clave = FormsAuthentication.HashPasswordForStoringInConfigFile(clave, "sha1");
                    }
                    bean.flagmode = "1";
                    UsuarioController.crear(bean);
                    if (codigoperfil.Equals("ADM") || codigoperfil.Equals("DBO"))
                    {
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }
                    else
                    {
                        if (codigoperfil.Equals("SUP"))
                        {
                            List<GrupoBean> loLstGrupoBean = Operation.SplitVector.ListaCodigoGrupo(codgrupo);
                            for (Int32 i = 0; i < loLstGrupoBean.Count; i++)
                            {
                                //UsuarioController.AsignarUsuario(loLstGrupoBean[i].Grupo_PK, codigo);
                                bean.Codgrupo = loLstGrupoBean[i].Grupo_PK.ToString();
                                UsuarioController.crearGrupoDetalle(bean);
                            }

                        }
                        else
                        {
                            bean.Codgrupo = codgrupo;
                            UsuarioController.crearGrupoDetalle(bean);
                        }
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }
                }
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        [WebMethod]
        public static String editar(String id, String codigo, String login, String nombre, String codigoperfil, String clave, String clave2, String codgrupo, String serie, String correlativo)
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }

                else if (Utils.tieneCaracteresReservados(login))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_LOGIN, login) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, login));
                }
                else if (Utils.tieneCaracteresReservados(clave))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLAVE, clave) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, clave));
                }
                else
                {
                    UsuarioBean bean = new UsuarioBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.login = login.ToUpper();
                    bean.nombre = nombre.ToUpper();
                    bean.codigoperfil = codigoperfil;
                    bean.clave = clave2;
                    bean.flagmode = "0";
                    bean.serie = serie;
                    bean.correlativo = correlativo;
                    if (clave2 != clave)
                    {
                        bean.clave = FormsAuthentication.HashPasswordForStoringInConfigFile(clave, "sha1");
                        bean.flagmode = "1";
                    }
                    UsuarioController.crear(bean);
                    if (codigoperfil.Equals("ADM") || codigoperfil.Equals("DBO"))
                    {
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }
                    else
                    {
                        if (codigoperfil.Equals("SUP"))
                        {
                            List<GrupoBean> loLstGrupoBean = Operation.SplitVector.ListaCodigoGrupo(codgrupo);
                            UsuarioController.borrarSupervisor(Int32.Parse(id));
                            UsuarioController.EliminarGrupoDetalle(Int32.Parse(id));

                            for (Int32 i = 0; i < loLstGrupoBean.Count; i++)
                            {
                                //UsuarioController.AsignarUsuario(loLstGrupoBean[i].Grupo_PK, codigo);
                                bean = new UsuarioBean();
                                bean.codigo = codigo;
                                bean.Codgrupo = loLstGrupoBean[i].Grupo_PK.ToString();
                                UsuarioController.crearGrupoDetalle(bean);
                            }
                        }
                        else
                        {
                            bean.Codgrupo = codgrupo;
                            UsuarioController.EditarGrupoDetalle(bean);
                        }
                    }
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}