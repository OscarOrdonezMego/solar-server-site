﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Mantenimiento.usuario.Mantenimiento_usuario_gridGrupo" Codebehind="gridGrupo.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    
</head>
<body>
    <form id="form4" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
            </button>
            <h3 id="myModalLabel" runat="server"><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%> </h3>
        </div>
        <div id="myModalContent" class="modal-body">
            <div id="grupoMultiple">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                *
                </p>
                <div id="dimostrarmsg" runat="server">

                </div>
                <div class="padre">
                    <div class="Hijo">
                    </div>
                </div>
            </div>
            <div id="GrupoSimple">
                <div id="subgrupo">
                </div>
            </div>
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
        <div class="modal-footer">
            <input type="button" id="btncerrar" runat="server" data-dismiss="modal" class="form-button cz-form-content-input-button"
                value="CERRAR" />
        </div>
        <asp:HiddenField ID="llenarCombo" runat="server" />
        <asp:HiddenField ID="llenarCombo2" runat="server" />
        <asp:HiddenField ID="MhAccion" runat="server" />
        <asp:HiddenField ID="vergrupo" runat="server" />
        <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="" runat="server" />
        <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>

</body>
</html>
