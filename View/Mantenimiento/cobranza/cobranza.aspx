﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.cobranza.cobranza" Codebehind="cobranza.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
     <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet"/>
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
     <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>

    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
   
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>


 
    <script type="text/javascript">
        var urldel = 'cobranza.aspx/borrar';
        var urlins = 'nuevo.aspx';
        var urlbus = 'grid.aspx';
        var urlsavN = 'cobranza.aspx/crear';
        var urlsavE = 'cobranza.aspx/editar';
        $(document).ready(function () {
            BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE();
        });
        $(document).ready(function () {
            clickCBOSupervisor();
            BUSCARGRUPOSCUANDOHACEMOSCHANGE();
            deleteReg();
            addReg();
            modReg();
            busReg();
            $('#buscar').trigger("click");

        });
        function BUSCARGRUPOSCUANDOHACEMOSCHANGE() {
            $('#cboPerfil').change(function () {
            
                    var ulsdata = "cobranza.aspx/MostarGrupos";
                    var objt = new Object();
                    objt.codigo = $(this).val();
                    $.ajax({
                        url: ulsdata,
                        data: JSON.stringify(objt),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            $('#cboGrupro option').remove();
                            $('#cboGrupro').append(msg.d);
                            $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "registeruser");
                        }
                    });
                

            });

        }


        function BUSCARGRUPOSVENDEDORCUANDOHACEMOSCHANGE() {
            $('#cboGrupro').change(function () {
                   var ulsdatae = "cobranza.aspx/MostarVendedoresPorGrupos";
                    var objtd = new Object();
                    objtd.codigo = $(this).val();
                    $.ajax({
                        url: ulsdatae,
                        data: JSON.stringify(objtd),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            $('#cboVendedor option').remove();
                            $('#cboVendedor').append(msg.d);
                            $('#cboVendedor').parent().find(".cz-form-content-input-select-visible-text").html($('#cboVendedor').find("option:selected").text());
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "registeruser");
                        }
                    });
                

            });

        }

        function clickCBOSupervisor() {
            var codusu = '<%=codigoSub%>';
            var usupk = '<%=pkusu%>';
            if (codusu == 'SUP') {
                Supervisor(usupk);
            }
        }

        function Supervisor(codigo) {
            var ulsdata = "cobranza.aspx/MostarGrupos";
            var objt = new Object();
            objt.codigo = codigo;
            $.ajax({
                url: ulsdata,
                data: JSON.stringify(objt),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $('#cboGrupro option').remove();
                    $('#cboGrupro').append(msg.d);
                    $('#cboGrupro').parent().find(".cz-form-content-input-select-visible-text").html($('#cboGrupro').find("option:selected").text());
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.clicodigo = $('#hcliente').val();
            strData.vencodigo = $('#cboVendedor').val();
            strData.clientecodigo = $('#txtCodigo').val();
            strData.flag = $('#chkflag').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.codperfil = $('#cboPerfil').val();
            strData.codgrupo = $('#cboGrupro').val();
            strData.fini = $('#txtFechaIni').val();
            strData.ffin = $('#txtFechaFin').val();
            return strData;
        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/cobranzas.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NAV_COBRANZA)%></p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
               <% if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.CREAR))
                  {%>
                <input type="button" id="cz-form-box-nuevo" class="cz-form-content-input-button cz-form-content-input-button-image form-button addReg cz-form-box-content-button cz-util-right"
                    data-grid-id="divGridViewData" value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NUEVO) %>" />
               <%} %>
               <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                 { %>
                <input type="button" id="cz-form-box-del" class="cz-form-content-input-button cz-form-content-input-button-image form-button delReg cz-util-right"
                    value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
               <%} %>
            </div>
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAINICIO)%>
                        </p>
                        <input name="txtFechaIni" type="text" value="30/05/2014" maxlength="10" id="txtFechaIni"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img1" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAFIN)%>
                        </p>
                        <input name="txtFechaFin" type="text" value="30/05/2014" maxlength="10" id="txtFechaFin"
                            runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                            onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="Img2" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CODIGO)%> Cliente</p>
                    <asp:TextBox ID="txtCodigo" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="hddlTurno" type="hidden" value="-1" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CLIENTE)%></p>
                    <asp:TextBox ID="txtcliente" runat="server" class="cz-form-content-input-text"></asp:TextBox><input
                        id="hcliente" type="hidden" value="" runat="server" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content" <%=deshabilitarCboSupervisor %>>
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SUPERVISOR)%></p>
                    <asp:DropDownList ID="cboPerfil" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRUPO)%></p>
                    <asp:DropDownList ID="cboGrupro" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                    <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VENDEDOR)%></p>
                    <asp:DropDownList ID="cboVendedor" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
           
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HABILITADO)%></p>
                        <input type="checkbox" class="cz-form-content-input-check" id="chkflag" name="habilitado"
                            checked="checked" />
                    </div>
                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                            value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer" style="overflow:auto;">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server">
                                </div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server">
                                </div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>
                                        buscando resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hidden Fields to control pagination-->
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                 
                <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />

                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>

    <script>

        $(document).ready(function () {
            $('#txtcliente').autocomplete('cliente.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#txtcliente').result(function (event, item, formatted) {
                $('#hcliente').val("");
                if (item) {
                    $('#hcliente').val(item.Codigo);
                }
            });

            $('#txtVendedor').autocomplete('vendedor.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

                     $('#txtVendedor').result(function (event, item, formatted) {
                $('#hvendedor').val("");
                if (item) {
                    $('#hvendedor').val(item.Codigo);
                }
            });

        });

    </script>
</body>
</html>
