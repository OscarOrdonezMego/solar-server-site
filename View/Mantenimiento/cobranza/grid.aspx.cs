﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;
using Controller.functions;

namespace Pedidos_Site.Mantenimiento.cobranza
{
    public partial class actividad_actividad_grid : PageController
    {
        public static String lsCodMenu = "MCO";
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);


            String xclientecodigo = dataJSON["clientecodigo"].ToString();
            String xclicod = dataJSON["clicodigo"].ToString();
            String xvendcod = dataJSON["vencodigo"].ToString();
            String xflag = dataJSON["flag"].ToString();

            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }
            String xpagina = dataJSON["pagina"].ToString();
            String xfilas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;//dataJSON["filas"].ToString();
            String fini = Utils.getStringFechaYYMMDD(dataJSON["fini"].ToString());
            String ffin = Utils.getStringFechaYYMMDD(dataJSON["ffin"].ToString());
            PaginateCobranzaBean paginate = CobranzaController.paginarBuscar(fini, ffin, xclicod, xvendcod, xclientecodigo, xflag, xpagina, xfilas, codperfil, codgrupo, id_usu);

            if ((Int32.Parse(xpagina) > 0) && (Int32.Parse(xpagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = xpagina;

                if (Int32.Parse(xpagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(xpagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<CobranzaBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }
    }
}