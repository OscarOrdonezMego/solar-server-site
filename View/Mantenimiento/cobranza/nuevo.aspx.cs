﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller;
using Model.bean;
using Newtonsoft.Json;

namespace Pedidos_Site.Mantenimiento.cobranza
{
    public partial class nuevo : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOCOBRANZA);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                cargarTipoDocumento();
                MtxtFechaIni.Value = DateTime.Today.ToShortDateString();

                if (dataJSON != null)
                {
                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARCOBRANZA);
                    CobranzaBean usuario = CobranzaController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"].ToString();
                    MtxtCodigo.Value = usuario.cobcodigo.ToString();
                    MtxtCliente.Value = usuario.clinombre.ToString();
                    MtxtVendedor.Value = usuario.vennombre.ToString();
                    mhCliente.Value = usuario.clicodigo.ToString();
                    mhVendedor.Value = usuario.vencodigo.ToString();
                    MtxtNumero.Value = usuario.numdocumento.ToString();
                    cboTipoDocumento.SelectedValue = usuario.tipodocumento.ToString();
                    MtxtMontoPagado.Value = usuario.montopagadosoles.ToString();
                    MtxtMontoTotal.Value = usuario.montototalsoles.ToString();
                    MtxtMontoPagadoDolares.Value = usuario.montopagadodolares.ToString();
                    MtxtMontoTotalDolares.Value = usuario.montototaldolares.ToString();
                    MtxtFechaIni.Value = usuario.fecvencimiento.ToString();
                    MtxtSerie.Value = usuario.serie.ToString();
                    MtxtCliente.Disabled = true;
                    MtxtCliente.Disabled = true;
                    MtxtVendedor.Disabled = true;
                    MtxtCodigo.Disabled = true;

                }
                else
                {
                    hidPk.Value = "";

                    mhCliente.Value = "";
                    mhVendedor.Value = "";

                }

                MtxtVendedor.Focus();
            }
        }


        private void cargarTipoDocumento()
        {
            int item = 0;

            cboTipoDocumento.Items.Insert(item, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_FACTURA));
            cboTipoDocumento.Items[item].Value = "FAC";
            item++;

            cboTipoDocumento.Items.Insert(item, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_BOLETA));
            cboTipoDocumento.Items[item].Value = "BOL";
            item++;
        }
    }
}