﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.cobranza.nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script>

        $(document).ready(function () {
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-text-calendar2").click(function () {
                var calendar = new CalendarPopup("calendarwindow2");
                calendar.setCssPrefix("NEXTEL");
                var txtname = $(this).parent().parent().parent().find("input.cz-form-content-input-calendar").attr("id");
                console.log(txtname);
                calendar.select(eval("document.forms[1]." + txtname), $(this).attr("id"), 'dd/MM/yyyy');
                //var xtop = $('#calendarwindow2').position().top - 262;
                var xtop = $('#calendarwindow2').position().top - 87;
                console.log(xtop);

                if (txtname == "MtxtFechaIni") {
                    $('#calendarwindow2').css({ 'left': '64px', 'top': xtop + 'px' })
                }
                else {
                    $('#calendarwindow2').css({ 'left': '280px', 'top': xtop + 'px' })
                }
                return false;
            });

            $('#MtxtCliente').autocomplete('cliente.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtCliente').result(function (event, item, formatted) {
                $('#mhCliente').val("");
                if (item) {
                    $('#mhCliente').val(item.Codigo);
                }
            });

            $('#MtxtVendedor').autocomplete('vendedor.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtVendedor').result(function (event, item, formatted) {
                $('#mhVendedor').val("-1");
                if (item) {
                    $('#mhVendedor').val(item.Codigo);
                }
            });



            //            $('#chkClienteEspecial').click(function (e) {
            //                if ($(this).attr("checked")) {
            //                    $('#clienteespecial').hide();
            //                }
            //                else {
            //                    $('#clienteespecial').show();
            //                }
            //            });

        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();

            if (parseInt($('#MtxtMontoPagado').val()) > parseInt($('#MtxtMontoTotal').val())) {
                addnotify("notify", "MONTO PAGADO S/. MAYOR QUE MONTO TOTAL S/.", "registeruser");
            } else if (parseInt($('#MtxtMontoPagadoDolares').val()) > parseInt($('#MtxtMontoTotalDolares').val())) {
                addnotify("notify", "MONTO PAGADO $. MAYOR QUE MONTO TOTAL $.", "registeruser");
            } else {
                strData.pk = $('#hidPk').val();
                strData.cobcodigo = $('#MtxtCodigo').val(); //  $('#mhCliente').val();
                strData.clicodigo = $('#mhCliente').val();
                strData.vencodigo = $('#mhVendedor').val();
                strData.numero = $('#MtxtNumero').val();
                strData.tipodocto = $('#cboTipoDocumento').val();
                strData.pagado = ($.trim($('#MtxtMontoPagado').val()) == "" ? "0" : $('#MtxtMontoPagado').val());
                strData.total = ($.trim($('#MtxtMontoTotal').val()) == "" ? "0" : $('#MtxtMontoTotal').val());
                strData.pagadodolares = ($.trim($('#MtxtMontoPagadoDolares').val()) == "" ? "0" : $('#MtxtMontoPagadoDolares').val());
                strData.totaldolares = ($.trim($('#MtxtMontoTotalDolares').val()) == "" ? "0" : $('#MtxtMontoTotalDolares').val());
                strData.vencimiento = $('#MtxtFechaIni').val();
                strData.serie = $('#MtxtSerie').val();

                console.log(strData);
                //strData.accion = $('#MhAccion').val();
                //strData.supervisor = $('#chkSupervisor').attr("checked") ? 'T' : 'F';
                return strData;
            }            
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#hidPk').val('');
            $('#MtxtCodigo').val('');
            $('#mhCliente').val('');
            $('#MtxtCliente').val('');
            $('#mhVendedor').val('');
            $('#MtxtVendedor').val('');
            $('#MtxtNumero').val('');
            $('#cboTipoDocumento').val($("#cboTipoDocumento option:first").val());
            //$('#MtxtHasta').val($('#hFechaActual').val());
            $('#MtxtMontoPagado').val('');
            $('#MtxtMontoTotal').val('');
            $('#MtxtMontoPagadoDolares').val('');
            $('#MtxtMontoTotalDolares').val('');
            $('#MtxtSerie').val('');

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *</p>
            <input type="text" id="MtxtCodigo" runat="server" class="requerid cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                *</p>
            <input type="text" id="MtxtCliente" runat="server" class="requerid cz-form-content-input-text" />
            <asp:HiddenField ID="mhCliente" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                *</p>
            <input type="text" id="MtxtVendedor" runat="server" class="requerid cz-form-content-input-text" />
            <asp:HiddenField ID="mhVendedor" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_NUMERO)%>
                *</p>
            <input type="text" id="MtxtNumero" runat="server" class="requerid cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_TIPO_DOCUMENTO)%>
                *</p>
            <asp:DropDownList ID="cboTipoDocumento" runat="server" CssClass=" requerid cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_SERIE)%>
                *</p>
            <input type="text" id="MtxtSerie" runat="server" class="requerid cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MONTO_TOTAL)%>
                S/. *</p>
            <input type="text" id="MtxtMontoTotal" runat="server" onkeypress="fc_PermiteDecimalOld(this);"
                class="cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible"> 
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MONTO_PAGADO)%>
                S/. *</p>
            <input type="text" id="MtxtMontoPagado" runat="server" onkeypress="fc_PermiteDecimalOld(this);"
                class="cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>

        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MONTO_TOTAL)%>
                $. *</p>
            <input type="text" id="MtxtMontoTotalDolares" runat="server" onkeypress="fc_PermiteDecimalOld(this);"
                class="cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible"> 
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MONTO_PAGADO)%>
                $. *</p>
            <input type="text" id="MtxtMontoPagadoDolares" runat="server" onkeypress="fc_PermiteDecimalOld(this);"
                class="cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>

        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FECHA_VENCIMIENTO)%>
            </p>
            <input name="MtxtFechaIni" type="text" value="30/05/2014" maxlength="10" id="MtxtFechaIni"
                runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
            <div class="cz-form-content-input-calendar-visible">
                <div class="cz-form-content-input-calendar-visible-button">
                    <img alt="<>" id="Img1" style="position: absolute; z-index: 2; padding: 4px; margin-top: 1px;"
                        name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar2"
                        src="../../images/icons/calendar.png" />
                </div>
            </div>
            <div id="calendarwindow2" class="calendar-window">
            </div>
        </div>
    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>
</body>
</html>
