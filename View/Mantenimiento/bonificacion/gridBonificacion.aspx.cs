﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.bonificacion
{
    public partial class Mantenimiento_bonificacion_gridBinificacion : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_BONIFICACION_AUTOMATICA;
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = String.Empty;
            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                BonificacionBean loBonificacionBean = new BonificacionBean();
                loBonificacionBean.Pagina = dataJSON["pagina"].ToString();
                loBonificacionBean.TotalPaginas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
                loBonificacionBean.PRO_CODIGO = dataJSON["codigo"].ToString();
                if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    loBonificacionBean.TIPO_ARTICULO = Operation.TipoArticulo.PRESENTACION;
                }
                else
                {
                    loBonificacionBean.TIPO_ARTICULO = Operation.TipoArticulo.PRODUCTO;
                }

                PaginateBonificacionBean paginate = BonificacionController.ListaBonificacion(loBonificacionBean);
                if ((Int32.Parse(loBonificacionBean.Pagina) > 0) && (Int32.Parse(loBonificacionBean.Pagina) <= paginate.TotalFila))
                {
                    this.lbTpagina.Text = paginate.TotalFila.ToString();
                    this.lbPagina.Text = loBonificacionBean.Pagina;

                    if (Int32.Parse(loBonificacionBean.Pagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(loBonificacionBean.Pagina) == paginate.TotalFila)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<BonificacionBean> lst = paginate.LoLstBonificacionBean;
                    grdMant4.DataSource = lst;
                    grdMant4.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView3.InnerHtml = htmlNoData;
                    this.divGridViewPagintator3.Visible = false;
                }
            }
        }
    }
}