﻿<%@ WebHandler Language="C#" Class="presentacion" %>

using System;
using System.Web;
using Controller;
using Newtonsoft.Json;
using Model.bean;
using Model;
using System.Collections.Generic;
public class presentacion : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        String match = context.Request.Params["q"];
        String tipoArticulo = context.Request.Params["cod"];
        String output = "";
        List<Combo> result = ProductoController.matchProductoBeanBonificacion(match, tipoArticulo);
        output = Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented);
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}