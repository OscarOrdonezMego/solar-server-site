﻿using System;
using System.Collections.Generic;
using Model;
using Newtonsoft.Json;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.bonificacion
{
    public partial class Mantenimiento_bonificacion_grid : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_BONIFICACION_AUTOMATICA;

        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        public static List<BonificacionBean> ioLstBonificacionBean = new List<BonificacionBean>();
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = String.Empty;
            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                BonificacionBean loBonificacionBean = new BonificacionBean();
                loBonificacionBean.Pagina = dataJSON["pagina"].ToString();
                loBonificacionBean.TotalPaginas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;
                loBonificacionBean.CONPRO_CODIGO = dataJSON["codigo"].ToString();
                PaginateBonificacionBean paginate = BonificacionController.paginarBuscarBonificacionDetalle(loBonificacionBean, ioLstBonificacionBean);
                if ((Int32.Parse(loBonificacionBean.Pagina) > 0) && (Int32.Parse(loBonificacionBean.Pagina) <= paginate.TotalFila))
                {
                    this.lbTpagina.Text = paginate.TotalFila.ToString();
                    this.lbPagina.Text = loBonificacionBean.Pagina;

                    if (Int32.Parse(loBonificacionBean.Pagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(loBonificacionBean.Pagina) == paginate.TotalFila)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<BonificacionBean> lst = paginate.LoLstBonificacionBean;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }
        }
    }
}