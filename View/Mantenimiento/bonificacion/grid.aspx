﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.bonificacion.Mantenimiento_bonificacion_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        $(document).ready(function () {
            $('.chkgrid').each(function () {
                if ($(this).val() == 'T') {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });
    </script>
</head>
<body>
   <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width:100%"  class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th nomb="NUMPEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>
                           <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                              
                            <th style="width: 40px;">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EXISTE)%>
                            </th> 
                        
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                              {%>
                           <th style="width: 40px;">
                            <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                            </th>
                            <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("PRO_CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%> - <%# Eval("CONPRO_NOMBRE")%>  <%# Eval("BON_CANTIDAD ")%> X <%# Eval("BON_MAXIMO")%>
                    </td>
                    <td>
                        <%# Eval("CONBON_CANTIDAD")%>
                    </td>
                    <td>
                       <center><input id=""  class="chkgrid"   value="<%# Eval("Existe")%>" type="checkbox"></center> 
                    </td>
                   
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                   <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <%# Eval("PRO_CODIGO")%>
                    </td>
                    <td>
                      <%# Eval("PRO_NOMBRE")%> - <%# Eval("CONPRO_NOMBRE")%>  <%# Eval("BON_CANTIDAD ")%> X <%# Eval("BON_MAXIMO")%>
                    </td>
                    <td>
                        <%# Eval("CONBON_CANTIDAD")%>
                    </td>
                    <td>
                        <center><input id="" class="chkgrid"  value="<%# Eval("Existe")%>" type="checkbox"></center>
                    </td>
                   
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     { %>
                    <td>
                        <a  role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                   <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer-2">
            <div class="paginator-table-inner-2">
                <div class="paginator-data-2">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search-2">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
