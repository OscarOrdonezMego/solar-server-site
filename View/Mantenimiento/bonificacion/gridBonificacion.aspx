﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.bonificacion.Mantenimiento_bonificacion_gridBinificacion" Codebehind="gridBonificacion.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divGridView3" runat="server">
            <asp:Repeater ID="grdMant4" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%" class="grilla table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th nomb="NUMPEDIDO" style="width: 40px;"></th>
                                <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                  { %>
                                <th nomb="NOMBREGGG">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                                </th>
                                <%} %>
                                <th nomb="NOMBRE">
                                    <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                      { %>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                                    <%}
                                      else
                                      { %>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                                    <%} %>
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <center><input id="" class="chkgrid" cod="<%# Eval("PRO_CODIGO")%>" value="<%# Eval("CODIGO_BON_PK")%>" type="checkbox"></center>
                        </td>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          { %>
                        <td>
                            <%# Eval("CONPRO_NOMBRE")%>
                        </td>
                        <%} %>
                        <td>
                            <%# Eval("PRO_NOMBRE")%>
                        </td>

                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alt">
                        <td>
                            <center><input id="" class="chkgrid" cod="<%# Eval("PRO_CODIGO")%>" value="<%# Eval("CODIGO_BON_PK")%>" type="checkbox"></center>
                        </td>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          { %>
                        <td>
                            <%# Eval("CONPRO_NOMBRE")%>
                        </td>
                        <%} %>
                        <td>
                            <%# Eval("PRO_NOMBRE")%>
                        </td>

                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="divGridViewPagintator3" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Pagina</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            buscando resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">
                    ×
                </div>
            </div>
        </div>

        <div id="formu" class="formulario" style="display: none">
            <p>mensage que se quiera sdfsfsfsdffsdfsfsdfsfsdfsdfsfsdfsdfsdfsdfsdfsf</p>
            <div class="subFormulario-botones">
                <input type="button" class="parent" value="Si" id="BtnS" />
                <input type="button" class="parent" value="No" id="BtnN" />
            </div>
        </div>
    </form>
</body>
</html>
