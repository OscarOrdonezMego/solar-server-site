﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Services;
using Model.bean;
using Model;
using Controller;
using Newtonsoft.Json;

namespace Pedidos_Site.Mantenimiento.bonificacion
{
    public partial class Mantenimiento_bonificacion_bonificacion : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_BONIFICACION_AUTOMATICA;
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        public static List<BonificacionBean> ioLstBonificacionBean = new List<BonificacionBean>();
        public static Constantes cons;
        public static String Option = "";
        public static bool fraccionamientoActivado = false;
        public static Int32 ioFraccionamientoMaximoDecimales;

        protected override void initialize()
        {

            if (Session["lgn_id"] == null)
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }


            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                fraccionamientoActivado = true;
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;

                String loValorFraccionamientoDecimal = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES).Valor;
                ioFraccionamientoMaximoDecimales = Convert.ToInt32(loValorFraccionamientoDecimal);
            }
            else
            {
                fraccionamientoActivado = false;
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = String.Empty;
                ioFraccionamientoMaximoDecimales = 0;
            }
        }

        [WebMethod]
        public static String GuardarDataALista(String nombreproductoparagrid, String connombreproductoparagrid, String codigopro, String nombreproducto, String cantidad, String bonomax, String flag, String concodigopro, String connombreproducto, String concantidad, String conflag)
        {
            try
            {
                Int32 existe = 0;
                BonificacionBean loBonificacionBean = new BonificacionBean();
                loBonificacionBean.PRO_CODIGO = codigopro;
                loBonificacionBean.PRO_NOMBRE = nombreproducto;
                loBonificacionBean.BON_CANTIDAD = cantidad;
                loBonificacionBean.BON_MAXIMO = bonomax;
                loBonificacionBean.BON_EDITABLE = flag;
                loBonificacionBean.CONPRO_CODIGO = concodigopro;
                loBonificacionBean.CONPRO_NOMBRE = connombreproducto;
                loBonificacionBean.CONBON_CANTIDAD = concantidad;
                loBonificacionBean.NOM_PRO_MOSTRAR_BON = nombreproductoparagrid;
                loBonificacionBean.NOM_PRO_MOSTRAR_CONDI = connombreproductoparagrid;
                loBonificacionBean.Existe = conflag;
                if (ioLstBonificacionBean.Count > 0)
                {
                    for (Int32 i = 0; i < ioLstBonificacionBean.Count; i++)
                    {
                        if (ioLstBonificacionBean[i].CONPRO_CODIGO.Equals(loBonificacionBean.CONPRO_CODIGO))
                        {
                            existe = 1;
                            break;
                        }
                    }
                }
                if (existe == 0)
                {
                    ioLstBonificacionBean.Add(loBonificacionBean);
                    return "Se agregó correctamente.";
                }
                else
                {
                    return "El artículo ya esta Registrado.";
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String EliminarArticulo(String codigoarticulo)
        {
            if (ioLstBonificacionBean.Count > 0)
            {
                for (Int32 i = 0; i < ioLstBonificacionBean.Count; i++)
                {
                    if (ioLstBonificacionBean[i].CONPRO_CODIGO == codigoarticulo)
                    {
                        ioLstBonificacionBean.RemoveAt(i);
                    }
                }
            }
            return "";
        }
        [WebMethod]
        public static String ListaBonificaion(String tipoArticulo, String codigo)
        {
            if (codigo.Length == 0)
            {
                ioLstBonificacionBean.Clear();
            }
            String CodigoHTML = "";
            BonificacionBean obj = new BonificacionBean();
            obj.TIPO_ARTICULO = tipoArticulo;
            List<BonificacionBean> lista = ioLstBonificacionBean;

            if (lista.Count > 0)
            {
                if (tipoArticulo == "PRO")
                {
                    for (Int32 i = 0; i < lista.Count; i++)
                    {
                        if (lista[i].Existe.Equals("T"))
                        {
                            CodigoHTML += HTML.Table.TROpen + HTML.Table.TDOpen + lista[i].CONPRO_CODIGO + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + lista[i].PRO_NOMBRE + " - " + lista[i].CONPRO_NOMBRE + " " + lista[i].BON_CANTIDAD + " X " + lista[i].BON_MAXIMO + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + lista[i].CONBON_CANTIDAD + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + "<input id=\"\" checked=\"checked\" class=\"chkgridboni\" value=" + lista[i].Existe + " type=\"checkbox\">" + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + "<center><a role=\"button\" data-toggle=\"modal\" style=\"cursor: pointer;\" class=\"delItemRege form-icon\"  cod=" + lista[i].CONPRO_CODIGO + "> <img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" alt=\"Borrar\" /></a></center>" + HTML.Table.TDClose
                                                          + HTML.Table.TRClose;
                        }
                        else
                        {
                            CodigoHTML += HTML.Table.TROpen + HTML.Table.TDOpen + lista[i].CONPRO_CODIGO + HTML.Table.TDClose
                                                        + HTML.Table.TDOpen + lista[i].PRO_NOMBRE + " - " + lista[i].CONPRO_NOMBRE + " " + lista[i].BON_CANTIDAD + " X " + lista[i].BON_MAXIMO + HTML.Table.TDClose
                                                        + HTML.Table.TDOpen + lista[i].CONBON_CANTIDAD + HTML.Table.TDClose
                                                        + HTML.Table.TDOpen + "<input id=\"\"  class=\"chkgridboni\" value=" + lista[i].Existe + " type=\"checkbox\">" + HTML.Table.TDClose
                                                        + HTML.Table.TDOpen + "<center><a role=\"button\" data-toggle=\"modal\" style=\"cursor: pointer;\" class=\"delItemRege form-icon\"  cod=" + lista[i].CONPRO_CODIGO + "> <img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" alt=\"Borrar\" /></a></center>" + HTML.Table.TDClose
                                    + HTML.Table.TRClose;
                        }

                    }
                }
                else
                {
                    for (Int32 i = 0; i < lista.Count; i++)
                    {
                        if (lista[i].Existe.Equals("T"))
                        {
                            CodigoHTML += HTML.Table.TROpen + HTML.Table.TDOpen + lista[i].CONPRO_CODIGO + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + lista[i].NOM_PRO_MOSTRAR_BON + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + lista[i].PRO_NOMBRE + " - " + lista[i].CONPRO_NOMBRE + " " + lista[i].BON_CANTIDAD + " X " + lista[i].BON_MAXIMO + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + lista[i].CONBON_CANTIDAD + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + "<input id=\"\" checked=\"checked\" class=\"chkgridboni\" value=" + lista[i].Existe + " type=\"checkbox\">" + HTML.Table.TDClose
                                                                              + HTML.Table.TDOpen + "<center><a role=\"button\" data-toggle=\"modal\" style=\"cursor: pointer;\" class=\"delItemRege form-icon\"  cod=" + lista[i].CONPRO_CODIGO + "> <img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" alt=\"Borrar\" /></a></center>" + HTML.Table.TDClose
                                                          + HTML.Table.TRClose;
                        }
                        else
                        {
                            CodigoHTML += HTML.Table.TROpen + HTML.Table.TDOpen + lista[i].CONPRO_CODIGO + HTML.Table.TDClose
                                                                                + HTML.Table.TDOpen + lista[i].NOM_PRO_MOSTRAR_BON + HTML.Table.TDClose
                                                                                + HTML.Table.TDOpen + lista[i].PRO_NOMBRE + " - " + lista[i].CONPRO_NOMBRE + " " + lista[i].BON_CANTIDAD + " X " + lista[i].BON_MAXIMO + HTML.Table.TDClose
                                                                                + HTML.Table.TDOpen + lista[i].CONBON_CANTIDAD + HTML.Table.TDClose
                                                                                + HTML.Table.TDOpen + "<input id=\"\" checked=\"checked\" class=\"chkgridboni\" value=" + lista[i].Existe + " type=\"checkbox\">" + HTML.Table.TDClose
                                                                                + HTML.Table.TDOpen + "<center><a role=\"button\" data-toggle=\"modal\" style=\"cursor: pointer;\" class=\"delItemRege form-icon\"  cod=" + lista[i].CONPRO_CODIGO + "> <img src=\"../../imagery/all/icons/eliminar.png\" border=\"0\" title=\"Borrar\" alt=\"Borrar\" /></a></center>" + HTML.Table.TDClose
                                                            + HTML.Table.TRClose;
                        }

                    }
                }


            }

            return CodigoHTML;
        }

        [WebMethod]
        public static String crearBonificacion(String codigopro, String nombreproducto, String cantidad, String bonomax, String flag, String TipoArticulo)
        {
            try
            {

                BonificacionBean loBonificacionBean = new BonificacionBean();

                if (TipoArticulo.Equals("PRE"))
                {
                    loBonificacionBean.PRE_PK = codigopro;
                }
                else
                {
                    loBonificacionBean.PRE_PK = "";
                }

                loBonificacionBean.PRO_CODIGO = codigopro;
                loBonificacionBean.PRO_NOMBRE = nombreproducto;
                loBonificacionBean.BON_CANTIDAD = cantidad;
                loBonificacionBean.BON_MAXIMO = bonomax;
                loBonificacionBean.BON_ITEMS = "0";
                loBonificacionBean.BON_EDITABLE = flag;
                loBonificacionBean.TIPO_ARTICULO = TipoArticulo;
                RespondInsert result = BonificacionController.crearBonificacion(loBonificacionBean);
                if (result.Valor == 1)
                {
                    if (ioLstBonificacionBean.Count > 0)
                    {
                        BonificacionBean loBonificacionBeandet = null;
                        for (Int32 i = 0; i < ioLstBonificacionBean.Count; i++)
                        {
                            loBonificacionBeandet = new BonificacionBean();
                            loBonificacionBeandet.CODIGO_BON_PK = Int32.Parse(result.Codigo);
                            loBonificacionBeandet.CONPRO_CODIGO = ioLstBonificacionBean[i].CONPRO_CODIGO;
                            loBonificacionBeandet.CONPRO_NOMBRE = ioLstBonificacionBean[i].CONPRO_NOMBRE;
                            loBonificacionBeandet.CONBON_CANTIDAD = ioLstBonificacionBean[i].CONBON_CANTIDAD;
                            loBonificacionBeandet.Existe = ioLstBonificacionBean[i].Existe;
                            loBonificacionBeandet.TIPO_ARTICULO = TipoArticulo;
                            BonificacionController.crearBonificacionDetalle(loBonificacionBeandet);
                        }
                        ioLstBonificacionBean.Clear();
                    }
                }

                return "Se Grabo Correctamente.";

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        [WebMethod]
        public static String LimpiarLista(String tipoarticulo)
        {
            ioLstBonificacionBean.Clear();

            return "";
        }
        [WebMethod]
        public static String buscarBonificacion(String codigobonpk, String tipoArticulo)
        {
            try
            {
                BonificacionBean loparametro = new BonificacionBean();
                loparametro.CODIGO_BON_PK = Int32.Parse(codigobonpk);
                loparametro.TIPO_ARTICULO = tipoArticulo;
                BonificacionBean loBonificacionBean = BonificacionController.BuscarBonificacion(loparametro);
                Option = llenarCombo(loBonificacionBean.PRO_CODIGO);
                String Json = "";
                ioLstBonificacionBean = BonificacionController.BuscarBonificacionDeatalle(loparametro);
                Json = JsonConvert.SerializeObject(loBonificacionBean);
                return Json;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String EditarBonificacion(String bonpk, String TipoArticulo, String cantidad, String bonomax, String flag)
        {
            try
            {
                BonificacionBean loBonificacionBean = new BonificacionBean();
                loBonificacionBean.CODIGO_BON_PK = Int32.Parse(bonpk);
                loBonificacionBean.BON_CANTIDAD = cantidad;
                loBonificacionBean.BON_MAXIMO = bonomax;
                loBonificacionBean.BON_ITEMS = "0";
                loBonificacionBean.BON_EDITABLE = flag;
                loBonificacionBean.TIPO_ARTICULO = TipoArticulo;
                BonificacionController.EditarBonificacion(loBonificacionBean);
                if (ioLstBonificacionBean.Count > 0)
                {
                    BonificacionBean loBonificacionBeandet = null;
                    BonificacionController.EliminarBonificacionDetalle(Int32.Parse(bonpk));
                    for (Int32 i = 0; i < ioLstBonificacionBean.Count; i++)
                    {
                        loBonificacionBeandet = new BonificacionBean();
                        loBonificacionBeandet.CODIGO_BON_PK = Int32.Parse(bonpk);
                        loBonificacionBeandet.CONPRO_CODIGO = ioLstBonificacionBean[i].CONPRO_CODIGO;
                        loBonificacionBeandet.CONPRO_NOMBRE = ioLstBonificacionBean[i].CONPRO_NOMBRE;
                        loBonificacionBeandet.CONBON_CANTIDAD = ioLstBonificacionBean[i].CONBON_CANTIDAD;
                        loBonificacionBeandet.Existe = ioLstBonificacionBean[i].Existe;
                        loBonificacionBeandet.TIPO_ARTICULO = TipoArticulo;
                        BonificacionController.crearBonificacionDetalle(loBonificacionBeandet);
                    }
                    ioLstBonificacionBean.Clear();
                }
                else
                {
                    BonificacionController.EliminarBonificacionDetalle(Int32.Parse(bonpk));
                }
                return "Se Actualizó correctamente.";
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
        [WebMethod]
        public static String EliminarBonificacion(String bonpk, String TipoArticulo)
        {

            try
            {
                BonificacionController.EliminarBonificacion(Int32.Parse(bonpk), TipoArticulo);
                return "se elimino correctamente.";
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String fnBuscarPresentaciones(String codigo)
        {

            return llenarCombo(codigo);

        }
        private static String llenarCombo(String codigo)
        {
            try
            {
                String nombre = "--Seleccione--";
                String option = "";
                List<PresentacionBean> lolstProductoPresentacionBean = BonificacionController.fnBuscarPresentacionBonificacion(codigo);

                if (lolstProductoPresentacionBean.Count > 0)
                {
                    option = "<option value=\"\" >" + nombre + " </option>";
                    for (Int32 i = 0; i < lolstProductoPresentacionBean.Count; i++)
                    {
                        option += "<option codig='" + lolstProductoPresentacionBean[i].Nombre + "' value=" + lolstProductoPresentacionBean[i].CODIGOPRE + " >" + lolstProductoPresentacionBean[i].Nombre + "</option>";
                    }
                    return option;
                }
                else
                {
                    option = "S";
                    return option;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        private static class HTML
        {
            public static class Table
            {
                public const String TROpen = "<TR>";
                public const String TRClose = "</TR>";
                public const String TDOpen = "<TD>";
                public const String TDClose = "</TD>";
            }
            public static class Input
            {
                public const String InputTOpen = "<input ";
                public const String Close = " >";
                public const String InputTClose = " </input>";
            }
            public static class Propiedad
            {
                public const String IDOpen = " id= \"";
                public const String IDClose = " \" ";
                public const String ClassOpen = " class= \"";
                public const String ClassClose = " \" ";
                public const String ValueOpen = " value= \"";
                public const String ValueClose = "\" ";
                public const String TypeOpen = " type= \"";
                public const String TypeClose = " \" ";
            }
            public static class Tipo
            {
                public const String CHECKBOX = "checkbox";
            }
        }
    }
}