﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.bonificacion.Mantenimiento_bonificacion_bonificacion" Codebehind="bonificacion.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>

    <script src="../../js/alertify.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../css/alertify.core.css" />
    <link rel="stylesheet" href="../../css/alertify.default.css" />

    <script>
        var urldel = 'usuario.aspx/borrar';
        var urlins = 'nuevo.aspx'
        var urlbus = 'gridBonificacion.aspx';
        var urlSavL = 'bonificacion.aspx/GuardarDataALista';
        var urlsavN = 'bonificacion.aspx/crearBonificacion';
        // var urlsavE = 'bonificacion.aspx/editarBonificacion';
        var urlbusboni = 'grid.aspx';

        $(document).ready(function () {
            $('#btxtProducto').keyup(function () {
                $('#btxtcodigo').val('');
                $('#btxtCantidad').val('');
                $('#btxtBonoMax').val('');
                $('#chkeditable').attr("checked", false);
                cleanfieldCondicion();
                CleanList();
                $("#BuscarBonidetalle").trigger("click");
            });
            EnableTrueEdicionEliminar();
            deleteReg();
            addReg();
            modReg();
            busReg();
            ValidarExisteCantidad();
            $('#buscar').trigger("click");
            mostrarMensageCbo();
        });

        $(document).ready(function () {
            var fracAc = '<%=ioRespuesta%>';
            var tipoArticuloAuto = "";
            if (fracAc == 1) {
                tipoArticuloAuto = "PRE";
            } else {
                tipoArticuloAuto = "PRO";
            }
            MostrarNombrePresentacion(tipoArticuloAuto);
            $('#btxtProducto').autocomplete('producto.ashx?cod=' + tipoArticuloAuto,
                    {
                        multiple: false,
                        minChars: 0,
                        max: 12,
                        autoFill: false,
                        cacheLength: 0,
                        mustMatch: true,
                        matchContains: false,
                        selectFirst: false,
                        dataType: 'json',
                        parse: function (data) {
                            return $.map(data, function (row) {
                                return {
                                    data: row, value: row.Nombre, result: row.Nombre

                                }
                            });
                        },
                        formatItem: function (item) {

                            return item.Nombre;
                        }

                    });
            $('#btxtProducto').result(function (event, item, formatted) {
                $('#bhCodigo').val("");
                if (item) {
                    var valifrac = '<%=fraccionamientoActivado %>'
                            if (valifrac) {
                                var urlpresentacion = "bonificacion.aspx/fnBuscarPresentaciones";
                                var obj = new Object();
                                obj.codigo = item.Codigo;
                                $.ajax({
                                    url: urlpresentacion,
                                    data: JSON.stringify(obj),
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (msg) {
                                        if (msg.d == "S") {
                                            $('#bcboaPresentaciones option').remove();
                                            $('#bcboaPresentaciones').parent().find(".cz-form-content-input-select-visible-text").html('No hay Registros.');
                                        } else {
                                            $('#bcboaPresentaciones option').remove();
                                            $('#bcboaPresentaciones').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                                            $('#bcboaPresentaciones').append(msg.d);
                                        }

                                    },
                                    error: function (result) {
                                        addnotify("notify", result.status, "registeruser");
                                    }
                                });
                            }
                            if (valifrac.toString() == 'False') {
                                $('#bhCodigo').val(item.Codigo);
                                $('#btxtcodigo').val(item.Codigo);
                            }

                        }
            });

            $('#bctxtproducto').autocomplete('producto.ashx?cod=' + tipoArticuloAuto,
                    {
                        multiple: false,
                        minChars: 0,
                        max: 12,
                        autoFill: false,
                        cacheLength: 0,
                        mustMatch: true,
                        matchContains: false,
                        selectFirst: false,
                        dataType: 'json',
                        parse: function (data) {
                            return $.map(data, function (row) {
                                return {
                                    data: row, value: row.Nombre, result: row.Nombre

                                }
                            });
                        },
                        formatItem: function (item) {

                            return item.Nombre;
                        }

                    });
            $('#bctxtproducto').result(function (event, item, formatted) {
                $('#bccodigopro').val("");
                if (item) {

                    var valifrac = '<%=fraccionamientoActivado %>'
                            if (valifrac) {
                                var urlpresentacion = "bonificacion.aspx/fnBuscarPresentaciones";
                                var obj = new Object();
                                obj.codigo = item.Codigo;
                                $.ajax({
                                    url: urlpresentacion,
                                    data: JSON.stringify(obj),
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (msg) {
                                        if (msg.d == "S") {
                                            $('#bccbopresentacion option').remove();
                                            $('#bccbopresentacion').parent().find(".cz-form-content-input-select-visible-text").html('No hay Registros.');
                                        } else {
                                            $('#bccbopresentacion option').remove();
                                            $('#bccbopresentacion').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                                            $('#bccbopresentacion').append(msg.d);
                                        }

                                    },
                                    error: function (result) {
                                        addnotify("notify", result.status, "registeruser");
                                    }
                                });
                            }
                            if (valifrac.toString() == 'False') {
                                $('#bccodigopro').val(item.Codigo);
                                $('#bctxtCodigo').val(item.Codigo);
                            }
                        }
            });
            $('#bcboaPresentaciones').change(function () {
                if ($(this).val().length > 0) {
                    $('#btxtcodigo').val($(this).val());
                } else {
                    $('#btxtcodigo').val('');
                }
            });
            $('#bccbopresentacion').change(function () {
                if ($(this).val().length > 0) {
                    $('#bctxtCodigo').val($(this).val());
                } else {
                    $('#bctxtCodigo').val('');
                }

            });
        });
                function getData() {
                    var fracAc = '<%=ioRespuesta%>';
            var tipoArticuloCrear = "";
            if (fracAc == 1) {
                tipoArticuloCrear = "PRE";
            } else {
                tipoArticuloCrear = "PRO";
            }
            var strData = new Object();
            strData.codigopro = $('#btxtcodigo').val();
            var valifrac = '<%=fraccionamientoActivado %>'
            if (valifrac) {
                strData.nombreproducto = $('#bcboaPresentaciones option:selected').html();
                strData.connombreproducto = $('#bccbopresentacion option:selected').html();
                strData.nombreproductoparagrid = $('#btxtProducto').val();
                strData.connombreproductoparagrid = $('#bctxtproducto').val();
            }
            if (valifrac.toString() == 'False') {
                strData.nombreproducto = $('#btxtProducto').val();
                strData.connombreproducto = $('#bctxtproducto').val();
            }

            strData.cantidad = $('#btxtCantidad').val();
            strData.bonomax = $('#btxtBonoMax').val();
            strData.flag = $('#chkeditable').attr("checked") ? 'T' : 'F';
            strData.concodigopro = $('#bctxtCodigo').val();

            strData.concantidad = $('#bctxtCantidad').val();
            strData.conflag = $('#bchkexiste').attr("checked") ? 'T' : 'F';
            strData.bonpk = $('#BONPK').val();
            strData.TipoArticulo = tipoArticuloCrear;
            return strData;
        }
        function mostrarCodigo(idcbo, idtxt) {

            $(id).change(function () {
                $(idtxt).val($(this).val());
            });
        }
        function mostrarMensageCbo() {
            if ($('#bcboaPresentaciones').val() != "") {
                $('#bcboaPresentaciones option').remove();
                $('#bcboaPresentaciones').parent().find(".cz-form-content-input-select-visible-text").html('Buscar Producto.');
            }
            if ($('#bccbopresentacion').val() != "") {
                $('#bccbopresentacion option').remove();
                $('#bccbopresentacion').parent().find(".cz-form-content-input-select-visible-text").html('Buscar Producto.');
            }

        }
        function mostrarMensageCboDataLlenaaAgregar() {
            $('#bccbopresentacion option').remove();
            $('#bccbopresentacion').parent().find(".cz-form-content-input-select-visible-text").html('Buscar Producto.');
        }
        function mostrarMensageCboDataLlenaGuardar() {
            $('#bcboaPresentaciones option').remove();
            $('#bcboaPresentaciones').parent().find(".cz-form-content-input-select-visible-text").html('Buscar Producto.');

            $('#bccbopresentacion option').remove();
            $('#bccbopresentacion').parent().find(".cz-form-content-input-select-visible-text").html('Buscar Producto.');
        }
        function cleanfieldCondicion() {
            $('#bctxtCodigo').val('');
            $('#bctxtproducto').val('');
            $('#bctxtCantidad').val('');
            $('#bchkexiste').attr("checked", false);
        }
        function CleanList() {
            var urlSavcle = 'bonificacion.aspx/LimpiarLista';
            var strData = new Object();
            strData.tipoarticulo = "PRO";
            $.ajax({
                url: urlSavcle,
                data: JSON.stringify(strData),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (responding) {

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function cleanfield() {
            $('#btxtcodigo').val('');
            $('#btxtProducto').val('');
            $('#btxtCantidad').val('');
            $('#btxtBonoMax').val('');
            $('#chkeditable').attr("checked", false);
        }
        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.codigo = $('#txtbuscarPorducto').val();
            strData.nombre = $('#btxtProducto').val();
            strData.login = $('#btxtCantidad').val();
            strData.perfil = $('#btxtBonoMax').val();
            strData.flag = $('#chkeditable').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.orden = $('#hdnOrden').val();
            strData.busNombre = $('#hdnBNombre').val();
            return strData;
        }
        $(document).ready(function () {
            $("#saveReg").click(function () {
                var obj = getData();
                if ($('#opcEditar').val() == "0") {
                    $.ajax({
                        url: urlsavN,
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (responding) {
                            cleanfield();
                            addnotify("notify", responding.d, "registeruser");
                            $('#buscar').trigger("click");
                            $("#BuscarBonidetalle").trigger("click");
                            $('#saveReg').css('color', 'gray');
                            $('#saveReg').prop('disabled', true);
                            mostrarMensageCboDataLlenaGuardar();
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "registeruser");
                        }
                    });
                } else {
                    $('#btnmodificarbonificacion').trigger('click');
                }

            });
            $("#bcbtnagregar").click(function () {

                var validateItems = true;

                $('.requerid').each(function () {
                    $(this).parent().find('span').remove();
                    if ($(this).val() == "") {


                        $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");

                        $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")


                        validateItems = false;

                    }
                });
                if (validateItems) {
                    var obj = getData();
                    $.ajax({
                        url: urlSavL,
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (responding) {

                            if ($('#opcEditar').val() != "1") {
                                $('#saveReg').css('color', 'white');
                                $('#saveReg').prop('disabled', false);
                            }

                            $("#BuscarBonidetalle").trigger("click");
                            // validarChekbox();
                            cleanfieldCondicion();
                            habilitarCantidadCondicion();
                            mostrarMensageCboDataLlenaaAgregar();
                            addnotify("notify", responding.d, "registeruser");
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "registeruser");
                        }
                    });
                }
            });
        });

        function validarChekbox() {
            $('.chkgridboni').each(function () {
                alert("hola" + $(this).val());
            });
            /*if (nombrechk == "T") {
                $('.chkgridboni').prop('checked', true);
            } else {
                $('.chkgridboni').prop('checked', false);
            }*/
        }
        $(document).ready(function () {

            $("tbody tr").mouseenter(function () {
                $(this).css('background-color', '#CCC');
            });

            $("tbody tr").mouseleave(function () {
                $(this).css('background-color', '#6E6E6E');
            });
            $("tbody tr").click(function () {
                alert($(this).text());
            });

            $('#btnBuscar').click(function () {
                var theTable = $('#grdMant4');
                $.uiTableFilter(theTable, $('#txtbuscarPorducto').val());
            });
        });
        $(document).ready(function () {
            var theTable = $('.grilla');

            theTable.find("tbody > tr").find("td:eq(1)").mousedown(function () {
                $(this).prev().find(":checkbox").click();
            });

            /* $("#txtbuscarPorducto").keyup(function () {
                 var obj = $(this).val();
                 if (obj == "") {
                     $.uiTableFilter(theTable, obj);
                 }
 
             });*/

            $('#filter-form').submit(function () {
                theTable.find("tbody > tr:visible > td:eq(1)").mousedown();
                return false;
            }).focus(); //Give focus to input field
        });
        $(document).ready(function () {
            $('#gridnuevo').css('display', 'none');
            $("#BuscarBonidetalle").click(function () {
                var urlListaBonificacion = "bonificacion.aspx/ListaBonificaion";
                var strData = new Object();
                var valifrac = '<%=fraccionamientoActivado %>'
                if (valifrac) {
                    strData.tipoArticulo = "PRE";
                }
                if (valifrac.toString() == 'False') {
                    strData.tipoArticulo = "PRO";
                }

                strData.codigo = $('#btxtcodigo').val();
                $.ajax({
                    url: urlListaBonificacion,
                    data: JSON.stringify(strData),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (responding) {
                        if (responding.d == "") {
                            $('#gridnuevo').css('display', 'none');
                            $('#footer').css('display', 'block');
                            /*$('#gridnuevo').innerHeight("");*/
                        } else {
                            $('#gridnuevo').css('display', 'block');
                            $('#footer').css('display', 'none');
                            $("#domainsTable tbody").empty().append(responding.d);
                        }

                        /* $("#domainsTable").tablesorter({ widthFixed: true, widgets: ['zebra'] }).tablesorterPager({ container: $('#pager') });
                         $("#domainsTable").trigger('update');
                         var sorting = [[0, 0]];
                         $('#domainsTable').trigger('sorton', [sorting]);*/
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });

            });
            $("#BuscarBonidetalle").trigger("click");
            $('.content-image').mouseenter(function () {
                $(this).css('background-color', 'gray');
            });
            $('.content-image').mouseleave(function () {
                $(this).css('background-color', 'white');
            });

            $('#btncrearbonificacion').click(function () {
                DeshabilitarBotones();
                habilitarparaCrear();
                mostrarMensageCboDataLlenaGuardar();
                mostrarMensageCboDataLlenaaAgregar();
                habilitarCbo(false);
                $('#opcEditar').val('0');
            });
            $('#btnborrarbonificacion').click(function () {
                var contador = 0;
                $(".grilla INPUT[type='checkbox']").each(function (i) {
                    if ($(this).prop('checked') == true) {
                        if (i == 0) {
                            contador = 1;
                        } else {
                            contador += 1;
                        }

                    }
                });
                if (contador == 0) {
                    alertify.log("Seleccione un articulo para Eliminar.", 10000);
                } else {
                    if (contador > 1) {
                        alertify.log("Seleccione solo un articulo.", 10000);
                    } else {
                        if (contador == 1) {
                            alertify.confirm('Eliminar el Articulo Nro: ' + $(".grilla INPUT[type='checkbox']").attr('cod'), function (e) {
                                if (e) {
                                    var urlEliBon = "bonificacion.aspx/EliminarBonificacion";
                                    $.ajax({
                                        url: urlEliBon,
                                        data: JSON.stringify(getData()),
                                        dataType: "json",
                                        type: "POST",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (responding) {
                                            alertify.log(responding.d, 10000);
                                            cleanfield();
                                            cleanfieldCondicion();
                                            CleanList();
                                            $('#opcEditar').val('0');
                                            $('#buscar').trigger("click");
                                            $("#BuscarBonidetalle").trigger("click");
                                            EnableTrueEdicionEliminar();
                                            EnableFalseCrear();
                                            EnabletxtProducto(false);
                                            habilitarparaCrear();
                                            mostrarMensageCboDataLlenaGuardar();
                                            mostrarMensageCboDataLlenaaAgregar();
                                            habilitarCbo(false);
                                        },
                                        error: function (result) {
                                            addnotify("notify", result.status, "registeruser");
                                        }
                                    });
                                } else {

                                }
                            });
                        }
                    }
                }
            });
            $('#btnmodificarbonificacion').click(function (evento) {
                var contador = 0;
                $(".grilla INPUT[type='checkbox']").each(function (i) {
                    if ($(this).prop('checked') == true) {
                        if (i == 0) {
                            contador = 1;
                        } else {
                            contador += 1;
                        }

                    }
                });
                if (contador == 0) {
                    alertify.log("Seleccione un articulo para modificar.", 10000);
                } else {
                    if (contador > 1) {
                        alertify.log("Seleccione solo un articulo.", 10000);
                    } else {
                        if (contador == 1) {
                           
                            alertify.confirm('Modificar Articulo Nro: ' + $(".grilla INPUT[type='checkbox']").attr('cod'), function (e) {
                                if (e) {
                                    var urlEditBon = "bonificacion.aspx/EditarBonificacion";
                                    $.ajax({
                                        url: urlEditBon,
                                        data: JSON.stringify(getData()),
                                        dataType: "json",
                                        type: "POST",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (responding) {
                                            alertify.log(responding.d, 10000);
                                            cleanfield();
                                            cleanfieldCondicion();
                                            CleanList();
                                            DesSeleccionarCheckBox();
                                            $('#opcEditar').val('0');
                                            $("#BuscarBonidetalle").trigger("click");
                                            EnableTrueEdicionEliminar();
                                            EnableFalseCrear();
                                            EnabletxtProducto(false);
                                            habilitarparaCrear();
                                            mostrarMensageCboDataLlenaGuardar();
                                            mostrarMensageCboDataLlenaaAgregar();
                                            habilitarCbo(false);
                                        },
                                        error: function (result) {
                                            addnotify("notify", result.status, "registeruser");
                                        }
                                    });
                                } else {

                                }
                            });
                        }
                    }
                }
            });
            $(".grilla INPUT[type='checkbox']").live('click', function (e) {
                DesSeleccionarCheckBox();
                $(this).attr('checked', 'checked');

                AñadirPropiedadCheckbox();

                if ($(this).prop('checked') == true) {
                    EnableFalseEdicionEliminar();
                    // EnableTrueCrear();
                    EnabletxtProducto(true);
                    habilitarparaEditar();
                    $('#opcEditar').val('1');
                    var strdata = new Object();
                    strdata.codigobonpk = $(this).val();
                    strdata.tipoArticulo = "PRO";
                    var urlBusBon = 'bonificacion.aspx/buscarBonificacion';
                    $.ajax({
                        url: urlBusBon,
                        data: JSON.stringify(strdata),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (responding) {
                            var obj = JSON.parse(responding.d);
                            $('#btxtcodigo').val(obj.PRO_CODIGO);
                            var valifrac = '<%=fraccionamientoActivado %>'
                            if (valifrac) {
                                $('#btxtProducto').val(obj.PRO_NOMBRE);
                            }
                            if (valifrac.toString() == 'False') {
                                $('#btxtProducto').val(obj.CONPRO_NOMBRE);
                            }
                            $('#btxtCantidad').val(obj.BON_CANTIDAD);
                            $('#btxtBonoMax').val(obj.BON_MAXIMO);
                            $('#chkeditable').val(obj.BON_EDITABLE);
                            $('#BONPK').val(obj.CODIGO_BON_PK);
                            if (obj.BON_EDITABLE == "T") {

                                $('#chkeditable').attr("checked", true);
                            }
                            $('#bcboaPresentaciones option').remove();
                            $('#bcboaPresentaciones').parent().find(".cz-form-content-input-select-visible-text").html(obj.CONPRO_NOMBRE);
                            habilitarCbo(true);
                            $("#BuscarBonidetalle").trigger("click");
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "registeruser");
                        }
                    });
                } else {
                    cleanfield();
                    cleanfieldCondicion();
                    EnableTrueEdicionEliminar();
                    EnableFalseCrear();
                    CleanList();
                    DesSeleccionarCheckBox();
                    $('#opcEditar').val('0');
                    $("#BuscarBonidetalle").trigger("click");
                    EnabletxtProducto(false);
                    habilitarparaCrear();
                }


            });
            function habilitarCbo(truee) {
                if (truee) {
                    $('#bcboaPresentaciones').attr('disabled', truee);
                    $('#cboprimero').css('background-color', 'gainsboro');
                } else {
                    $('#bcboaPresentaciones').attr('disabled', truee);
                    $('#cboprimero').css('background-color', '');
                }

            }
            function AñadirPropiedadCheckbox() {
                $(".grilla INPUT[type='checkbox']").each(function (i) {
                    if ($(this).attr('CF') == 'T') {

                    } else {
                        $(this).attr('C', 'T');
                    }

                });
            }
            function AñadirPropiedadCheckboxFalse() {
                $(".grilla INPUT[type='checkbox']").each(function (i) {
                    if ($(this).attr('C') == 'F') {

                    } else {
                        $(this).prop('checked', false);
                    }

                });
            }
            function DesSeleccionarCheckBox() {
                $('.chkgrid').each(function () {
                    if ($(this).prop('C') == 'F') {

                    } else {
                        $(this).prop('checked', false);
                        $(this).attr('C', 'T');
                    }
                });
            }
            function AñadirPropiedadAUno() {
                $(this).attr('C', 'F');
            }
            function validar() {
                $('input[type=checkbox]').live('click', function () {
                    var parent = $(this).parent().attr('id');
                    $('#' + parent + ' input[type=checkbox]').removeAttr('checked');
                    $(this).attr('checked', 'checked');
                });
            }
            $('.delItemRege').live('click', function (e) {
                var codigo = $(this).attr('cod');
                var obj = new Object();
                obj.codigoarticulo = codigo;
                var urlEliProductoDetalle = "bonificacion.aspx/EliminarArticulo";
                $.ajax({
                    url: urlEliProductoDetalle,
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (responding) {
                        $("#BuscarBonidetalle").trigger("click");
                        EnabletxtProducto(false);
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "registeruser");
                    }
                });
            });
        });
        function reset() {
            $("#toggleCSS").attr("href", "../../css/alertify.default.css");
            alertify.set({
                labels: {
                    ok: "OK",
                    cancel: "Cancel"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }
        function DeshabilitarBotones() {

            cleanfield();
            cleanfieldCondicion();
            CleanList();
            $("#BuscarBonidetalle").trigger("click");
            DesSeleccionarCheckBox();
            EnableTrueEdicionEliminar();
            EnableFalseCrear();
            $('#opcionCrear').val('1');
            EnabletxtProducto(false);

        }
        function DesSeleccionarCheckBox() {
            $('.chkgrid').each(function () {
                $(this).prop('checked', false);
            });
        }

        function EnableTrueEdicionEliminar() {
            $('#btnmodificarbonificacion').attr("disabled", true);
            $('#btnmodificarbonificacion').css('background-color', 'gray');
            $('#btnborrarbonificacion').attr("disabled", true);
            $('#btnborrarbonificacion').css('background-color', 'gray');
        }
        function EnableFalseEdicionEliminar() {
            $('#btnmodificarbonificacion').attr("disabled", false);
            $('#btnmodificarbonificacion').css('background-color', '');
            $('#btnborrarbonificacion').attr("disabled", false);
            $('#btnborrarbonificacion').css('background-color', '');
        }
        function EnableTrueCrear() {
            $('#btncrearbonificacion').attr("disabled", true);
            $('#btncrearbonificacion').css('background-color', 'gray');
        }
        function EnableFalseCrear() {
            $('#btncrearbonificacion').attr("disabled", false);
            $('#btncrearbonificacion').css('background-color', '');
        }
        function EnabletxtProducto(truee) {
            $('#btxtProducto').prop("disabled", truee);
        }
        function MostrarNombrePresentacion(tipoarticulo) {
            if (tipoarticulo == 'PRO') {
                $('#titulo').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PROD_A_BON)%>:');
                $('#productoboni').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>');
                $('#productocondi').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>');
            }
            if (tipoarticulo == 'PRE') {
                $('#titulo').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRES_A_BON)%>:');
                $('#productoboni').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>');
                $('#productocondi').text('<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>');
            }
        }
        function ValidarExisteCantidad() {
            $('#bchkexiste').live('click', function (e) {
                if ($(this).attr('checked') == 'checked') {
                    deschabilitarCantidadCondicion();
                } else {
                    habilitarCantidadCondicion();
                }
            });
        }
        function habilitarCantidadCondicion() {
            $('#bctxtCantidad').attr('disabled', false);
            $('#bctxtCantidad').prop('class', 'requerid cz-form-content-input-text')
        }
        function deschabilitarCantidadCondicion() {
            $('#bctxtCantidad').prop('class', 'cz-form-content-input-text');
            $('#bctxtCantidad').val('');
            $('#bctxtCantidad').parent().find('span').remove();
            $('#bctxtCantidad').attr('disabled', true);
        }
        function habilitarparaEditar() {
            $('#saveReg').css('color', 'white');
            $('#saveReg').attr('disabled', false);
        }
        function habilitarparaCrear() {
            $('#saveReg').css('color', 'gray');
            $('#saveReg').attr('disabled', true);
        }


        function getMaximoDecimales() {
            return <%= ioFraccionamientoMaximoDecimales %>
        }

    </script>
    <style>
        input#bchkexiste {
            margin-top: 6px;
            margin-left: 9px;
            width: 18px;
        }

        div#pager {
            margin-left: 37px;
            padding-top: 10px;
        }

        .contenedor-mantenimiento {
            border: 0px solid;
            height: 49px;
            margin: 10px 10px 10px;
            padding-left: 72px;
        }

        .content-image {
            border: 0px solid;
            width: 45px;
            height: 45px;
            float: left;
            margin: 1px;
        }

        img.imagenico {
            width: 28px;
        }

        .imagenbot {
            margin: 0px;
            width: 45px;
            height: 45px;
        }

        .formulario {
            border: 0px solid;
            padding: 11px;
            box-shadow: 0px 1px 5px rgb(1, 84, 160);
            width: 254px;
            margin: 10px;
        }

        .subFormulario-botones {
            border: 0px solid;
            padding: 5px 48px;
        }

        .parent {
            width: 73px;
            height: 28px;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/bono.png"
                            alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_BONIFICACIONES)%></p>
                        </div>
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div class="cz-form-box-content-consultation">
                        <div class="cz-form-content">
                            <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>:
                            <input type="text" id="txtbuscarPorducto" runat="server" class=" cz-form-content-input-text"
                                maxlength="100" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer-bonus">
                            <input type="button" id="buscar" runat="server" style="color: white; background-color: #0154A0; height: 29px; background-image: url();"
                                value="BUSCAR" />
                        </div>
                        <div class="form-grid-box">
                            <div class="form-grid-table-outer">
                                   <div class="form-grid-table-right">
                                <div class="form-grid-table-inner">
                                    <div class="form-gridview-data" id="divGridViewData" runat="server">
                                    </div>
                                    <div class="form-gridview-error" id="divGridViewError" runat="server">
                                    </div>
                                    <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                        <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                        <p>
                                            buscando resultados
                                        </p>
                                    </div>
                                </div>
                                <div class="contenedor-mantenimiento">
                                    <div class="content-image" id="crear">
                                        <button type="button" id="btncrearbonificacion" class="imagenbot">
                                            <img src="../../imagery/all/icons/add.png" border="0" title="Nuevo" class="imagenico" /></button>
                                    </div>
                                    <div class="content-image" id="editar" style="display: none">
                                        <button type="button" id="btnmodificarbonificacion" class="imagenbot">
                                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" class="imagenico" /></button>
                                    </div>
                                    <div class="content-image" id="eliminar">
                                        <button type="button" id="btnborrarbonificacion" class="imagenbot">
                                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Eliminar" class="imagenico" /></button>
                                    </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="cz-form-box-content-bonus">
                        <p id="titulo" style="font-size: 18px; padding-bottom: 10px"></p>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *
                            </p>
                            <input type="text" id="btxtcodigo" runat="server" class="requerid cz-form-content-input-text"
                                maxlength="100" disabled="disabled" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>

                        <div class="cz-form-content">
                            <p id="productoboni">
                                *
                            </p>
                            <input type="text" id="btxtProducto" runat="server" class="requerid cz-form-content-input-text"
                                maxlength="100" />
                            <asp:HiddenField ID="bhCodigo" runat="server" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          { %>
                        <div class="cz-form-content">
                            <!--@002 F-->
                            <p id="pAlmacen"><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>: </p>
                            <select id="bcboaPresentaciones" runat="server" class="requerid cz-form-content-input-select"></select>
                            <div class="cz-form-content-input-select-visible">
                                <p id="cboprimero" class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <%} %>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                *
                            </p>
                            <input type="text" id="btxtCantidad" runat="server" onfocus="this.oldvalue = this.value;" class="decimal-limit requerid cz-form-content-input-text"
                                maxlength="100" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_BONOMAX)%>:
                *
                            </p>
                            <input type="text" id="btxtBonoMax" runat="server" onkeypress="javascript: return fc_PermiteNumeros(event, this)" class="requerid cz-form-content-input-text"
                                maxlength="100" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>

                        <div class="cz-form-content">
                            <!--contenedor-bonusc-->
                            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_EDITABLE)%>:</p>
                            <input type="checkbox" id="chkeditable" runat="server" class="chec-box-my" />
                        </div>
                        <div class="alert fade" id="divError">
                            <strong id="tituloMensajeError"></strong>
                            <p id="mensajeError">
                            </p>
                        </div>
                        <div class="modal-footer-bonus">
                            <input type="button" id="saveReg" runat="server" style="color: gray; background-color: #0154A0; height: 29px;"
                                value="GUARDAR" disabled="disabled" />
                        </div>
                    </div>
                    <div class="cz-form-box-content-bonus-2">
                        <p style="font-size: 18px; padding-bottom: 10px"><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_AGREGAR_CONDICION)%>:</p>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *
                            </p>
                            <input type="text" id="bctxtCodigo" runat="server" class="requerid cz-form-content-input-text"
                                maxlength="100" disabled="disabled" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>

                        <div class="cz-form-content">
                            <p id="productocondi">
                                *
                            </p>
                            <input type="text" id="bctxtproducto" runat="server" class="requerid cz-form-content-input-text"
                                maxlength="100" />
                            <asp:HiddenField ID="bccodigopro" runat="server" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>
                        <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          { %>
                        <div class="cz-form-content">
                            <!--@002 F-->
                            <p id="Almacen"><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>: </p>
                            <select id="bccbopresentacion" runat="server" class="requerid cz-form-content-input-select"></select>
                            <div class="cz-form-content-input-select-visible">
                                <p class="cz-form-content-input-select-visible-text">
                                </p>
                                <div class="cz-form-content-input-select-visible-button">
                                </div>
                            </div>
                        </div>
                        <%} %>
                        <div class="cz-form-content">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                *
                            </p>
                            <input type="text" id="bctxtCantidad" runat="server" onfocus="this.oldvalue = this.value;" class="decimal-limit requerid cz-form-content-input-text"
                                maxlength="100" />
                            <div class="cz-form-content-input-text-visible">
                                <div class="cz-form-content-input-text-visible-button">
                                </div>
                            </div>
                        </div>

                        <div class="cz-form-content">
                            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EXISTE)%>:</p>
                            <input type="checkbox" id="bchkexiste" runat="server" class="chec-box-my" />
                        </div>

                        <div class="modal-footer-bonus">
                            <input type="button" id="bcbtnagregar" runat="server" style="color: white; background-color: #0154A0; height: 29px;"
                                value="Agregar" />
                        </div>
                    </div>
                    <div class="cz-form-box-content-bonus-3" id="gridnuevo">
                        <table id="domainsTable" class="grilla2 table table-bordered table-striped" style="width: 100%; border-radius: 12px">
                            <thead>
                                <tr>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%></th>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%></th>
                                    <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                      { %>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%></th>
                                    <%} %>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%></th>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EXISTE)%></th>
                                    <th><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                    <div id="footer" class="cz-form-box-content-bonus-3">
                        <center><img src='../../images/icons/grid/ico_grid_nodata.png' /> 
                                          <p>No se encontraron datos para mostrar</p> </center>
                    </div>
                </div>

                <!--Hidden Fields to control pagination-->
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnActualPage2" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="5" runat="server" />
                    <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
                    <asp:HiddenField ID="BuscarBonidetalle" Value="" runat="server" />
                    <asp:HiddenField ID="opcionCrear" Value="" runat="server" />
                    <asp:HiddenField ID="BONPK" Value="" runat="server" />
                    <asp:HiddenField ID="opcEditar" Value="0" runat="server" />
                    <asp:HiddenField ID="codigopro" Value="0" runat="server" />

                    <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                    <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                    <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                    <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                    <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                    <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>

    <script>
        $(".decimal-limit").on('input', function () {
            fc_PermiteDecimalConLimite(this, getMaximoDecimales()); this.oldvalue = this.value;
        });
    </script>
</body>
</body>
</html>
