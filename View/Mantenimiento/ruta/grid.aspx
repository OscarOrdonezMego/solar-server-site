﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.ruta.actividad_actividad_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <script>
    var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
        $(document).ready(function () {
            if (flag == "T") {
                $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>');
           } else {
               $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_RESTAURA)%>');
           }
        });
      </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("RUTA_PK") %>" type="checkbox">
                            </th>
                           <th nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%> Cliente
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                            <th nomb="TIPOACTIVIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                            <th nomb="NOMBREGRUPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th  nomb="FECHAINICIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAVISITA)%>
                            </th>
                           
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                              { %>
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                          <% }%>
                          <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                            {%>
                            <th style="width: 40px;" id="Tieliminar"> 
                            </th>
                           <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("clicodigo")%>
                    </td>
                    <td>
                        <%# Eval("clinombre")%>
                    </td>
                    <td>
                        <%# Eval("usrnombre")%>
                    </td>
                  <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("PROGRAMACIONFORMAT")%>
                    </td>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                    {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                   <%} %>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     {%>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                   <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                      <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("clicodigo")%>
                    </td>
                    <td>
                        <%# Eval("clinombre")%>
                    </td>
                    <td>
                        <%# Eval("usrnombre")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("PROGRAMACIONFORMAT")%>
                    </td>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      { %>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                <%} %>
                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                  { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                  <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
