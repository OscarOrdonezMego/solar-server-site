﻿<%@ WebHandler Language="C#" Class="cliente" %>

using System;
using System.Web;
using Controller;
using Newtonsoft.Json;
using Model.bean;
using Model;
using System.Collections.Generic;

public class cliente : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        String match = context.Request.Params["q"];
        String output = "";

        List<Combo> result = ClienteController.matchClienteBean(match);

        output = Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented);
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}