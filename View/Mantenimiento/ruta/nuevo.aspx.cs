﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller;
using Model.bean;
using Newtonsoft.Json;

namespace Pedidos_Site.Mantenimiento.ruta
{
    public partial class nuevo : Page
    {
        public bool GenCodigo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVARUTA);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);


                if (dataJSON != null)
                {

                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARRUTA);
                    RutaBean usuario = RutaController.info(dataJSON["codigo"]);
                    hidPk.Value = usuario.id;
                    MtxtCliente.Value = usuario.clinombre.ToString();
                    MtxtVendedor.Value = usuario.usrnombre.ToString();
                    mhCliente.Value = usuario.clinombre.ToString();
                    mhVendedor.Value = usuario.vendcodigo.ToString();
                    MtxtCliente.Disabled = true;


                    if (usuario.programacion != null && usuario.programacion.Length > 6)
                    {
                        idL.Checked = (usuario.programacion.Substring(0, 1) == "1") ? true : false;
                        idMa.Checked = (usuario.programacion.Substring(1, 1) == "1") ? true : false;
                        idMi.Checked = (usuario.programacion.Substring(2, 1) == "1") ? true : false;
                        idJ.Checked = (usuario.programacion.Substring(3, 1) == "1") ? true : false;
                        idV.Checked = (usuario.programacion.Substring(4, 1) == "1") ? true : false;
                        idS.Checked = (usuario.programacion.Substring(5, 1) == "1") ? true : false;
                        idD.Checked = (usuario.programacion.Substring(6, 1) == "1") ? true : false;
                        // programacion
                    }

                }
                else
                {
                    hidPk.Value = "";
                    mhCliente.Value = "";
                    mhVendedor.Value = "";

                }

                MtxtVendedor.Focus();

            }
        }
    }
}