﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.ruta
{
    public partial class ruta : PageController
    {
        public static String lsCodMenu = "MRU";

        public static String deshabilitarCboSupervisor;
        public static String codigoSub = "";
        public static String pkusu = "";
        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            if (!IsPostBack)
            {
                llenarComboBox();
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                if (codigoSub == "SUP")
                {
                    cboPerfil.SelectedValue = pkusu;
                    deshabilitarCboSupervisor = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                else
                {
                    deshabilitarCboSupervisor = "";
                }
                llenarcoGrupo();
                llenarComboBoxVendedor(pkusu);
            }
            //cargarCombo();
            //txtFecha.Value = Utils.getFechaActual();
        }
        private void llenarcoGrupo()
        {
            List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBox()
        {
            List<UsuarioBean> lista = UsuarioController.ListaSupervisores();
            if (lista.Count != 0)
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboPerfil.Items.Insert(i + 1, lista[i].nombre);
                    cboPerfil.Items[i + 1].Value = lista[i].id;

                }
            }
            else
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
            }
        }
        private void llenarComboBoxGrupo(String codigo, String perfil)
        {
            List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), perfil);
            if (lista.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                    cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();

                }
            }
            else
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
            }
        }
        [WebMethod]
        public static String MostarVendedoresPorGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Equals("0"))
            {
                List<UsuarioBean> lstUsuarioBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(pkusu));
                if (lstUsuarioBean.Count != 0)
                {
                    Rpta += ToolBox.Option("-1", "", "Todos");
                    for (int i = 0; i < lstUsuarioBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstUsuarioBean[i].id.ToString(), "", lstUsuarioBean[i].nombre);

                    }
                }
            }
            else
            {
                List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresPorGrupo(Int32.Parse(codigo));
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("-1", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Usu_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    Rpta = ToolBox.Option("-1", "", "No Hay Registro.").ToString();
                }
            }
            return Rpta;
        }
        [WebMethod]
        public static String MostarGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Length == 0)
            {
                codigo = "0";
            }
            if (codigo.Equals("0"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count != 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);

                    }
                }
                else
                {
                    Rpta = ToolBox.Option("0", "", "No Hay Registro.").ToString();
                }
            }
            else
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), "SUP");
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Grupo_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {


                }
            }

            return Rpta;
        }





        private void cargarCombo()
        {
        }



        [WebMethod]
        public static void borrar(String codigos, String flag)
        {

            try
            {

                RutaController.borrar(codigos, flag);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String did, String dclicod, String dvencod, String idL, String idMa, String idMi, String idJ, String idV, String idS, String idD)
        {
            try
            {

                RutaBean bean = new RutaBean();

                bean.vendcodigo = dvencod;

                if (dclicod != "" && dclicod != "-1")
                {
                    bean.clicodigo = dclicod;
                }
                else
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CLIENTE_INVALIDO));

                }

                string programa = "";
                if (idL == "T") programa = programa + "1"; else programa = programa + "0";
                if (idMa == "T") programa = programa + "1"; else programa = programa + "0";
                if (idMi == "T") programa = programa + "1"; else programa = programa + "0";
                if (idJ == "T") programa = programa + "1"; else programa = programa + "0";
                if (idV == "T") programa = programa + "1"; else programa = programa + "0";
                if (idS == "T") programa = programa + "1"; else programa = programa + "0";
                if (idD == "T") programa = programa + "1"; else programa = programa + "0";

                if (programa != "0000000")
                {
                    bean.programacion = programa;
                }
                else
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_SELECCIONAR_RUTA));
                }


                RutaController.crear(bean);


                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);


            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String did, String dclicod, String dvencod, String idL, String idMa, String idMi, String idJ, String idV, String idS, String idD)
        {
            try
            {

                RutaBean bean = new RutaBean();
                bean.id = did;

                bean.vendcodigo = dvencod;

                string programa = "";
                if (idL == "T") programa = programa + "1"; else programa = programa + "0";
                if (idMa == "T") programa = programa + "1"; else programa = programa + "0";
                if (idMi == "T") programa = programa + "1"; else programa = programa + "0";
                if (idJ == "T") programa = programa + "1"; else programa = programa + "0";
                if (idV == "T") programa = programa + "1"; else programa = programa + "0";
                if (idS == "T") programa = programa + "1"; else programa = programa + "0";
                if (idD == "T") programa = programa + "1"; else programa = programa + "0";

                if (programa != "0000000")
                {
                    bean.programacion = programa;
                }
                else
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_SELECCIONAR_RUTA));
                }


                RutaController.editar(bean);

                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
    }
}