﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.ruta.nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script>

        $(document).ready(function () {

            $('#MtxtCliente').autocomplete('cliente.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtCliente').result(function (event, item, formatted) {
                $('#mhCliente').val("");
                if (item) {
                    $('#mhCliente').val(item.Codigo);
                }
            });

            $('#MtxtVendedor').autocomplete('vendedor.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

                $('#MtxtVendedor').result(function (event, item, formatted) {
                $('#mhVendedor').val("-1");
                if (item) {
                    $('#mhVendedor').val(item.Codigo);
                }
            });



//            $('#chkClienteEspecial').click(function (e) {
//                if ($(this).attr("checked")) {
//                    $('#clienteespecial').hide();
//                }
//                else {
//                    $('#clienteespecial').show();
//                }
//            });

        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();

            
  

            strData.did = $('#hidPk').val();
            strData.dclicod = $('#mhCliente').val();
            strData.dvencod = $('#mhVendedor').val();
            strData.idL = $('#idL').attr("checked") ? 'T' : 'F';
            strData.idMa = $('#idMa').attr("checked") ? 'T' : 'F';
            strData.idMi = $('#idMi').attr("checked") ? 'T' : 'F';
            strData.idJ = $('#idJ').attr("checked") ? 'T' : 'F';
            strData.idV = $('#idV').attr("checked") ? 'T' : 'F';
            strData.idS = $('#idS').attr("checked") ? 'T' : 'F';
            strData.idD = $('#idD').attr("checked") ? 'T' : 'F';


            strData.accion = $('#MhAccion').val();
            //strData.supervisor = $('#chkSupervisor').attr("checked") ? 'T' : 'F';

            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtCliente').val('');
            $('#MtxtDesde').val($('#hFechaActual').val());
            $('#MtxtVendedor').val('');
            $('#MtxtHasta').val($('#hFechaActual').val());
             $('#McboUsuario').val($("#McboUsuario option:first").val());
            $('#MtxtDescripcion').val('');
            $('#McboTipo').val($("#McboTipo option:first").val());
             $('#mHCliente').val('-1');
            $('#MtxtOrden').val('');
            $('#chkDiaSemana').attr("checked", null);
            $('#chkRangoFechas').attr("checked", null);
            $('#chkClienteEspecial').attr("checked", null);
            $('#idL').attr("checked", null);
            $('#idMa').attr("checked", null);
            $('#idMi').attr("checked", null);
            $('#idJ').attr("checked", null);
            $('#idV').attr("checked", null);
            $('#idS').attr("checked", null);
            $('#idD').attr("checked", null);
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
      
          

             <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                *</p>
            <input type="text" id="MtxtCliente" runat="server" class="requerid cz-form-content-input-text" />
             <asp:HiddenField ID="mhCliente" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        
         
          
     
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                *</p>
            <input type="text" id="MtxtVendedor" runat="server" class="requerid cz-form-content-input-text" />
             <asp:HiddenField ID="mhVendedor" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
       
            <div class="cz-form-content" style="width: 380px; height: 27px;">
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_LU)%><asp:CheckBox
                    ID="idL" Checked="true" runat="server" Text="" TabIndex='11' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_MA)%><asp:CheckBox
                    ID="idMa" Checked="true" runat="server" Text="" TabIndex='12' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_MI)%><asp:CheckBox
                    ID="idMi" Checked="true" runat="server" Text="" TabIndex='13' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_JU)%><asp:CheckBox
                    ID="idJ" Checked="true" runat="server" Text="" TabIndex='14' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_VI)%><asp:CheckBox
                    ID="idV" Checked="true" runat="server" Text="" TabIndex='15' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_SA)%><asp:CheckBox
                    ID="idS" Checked="true" runat="server" Text="" TabIndex='16' />&nbsp;&nbsp;
                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIASEMANA_DO)%><asp:CheckBox
                    ID="idD" Checked="true" runat="server" Text="" TabIndex='17' />
            </div>
         

    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidClave" Value="" runat="server" />

      
    </form>

   
</body>
</html>
