﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller;
using Model.bean;

namespace Pedidos_Site.Mantenimiento.tipocambio
{
    public partial class Mantenimiento_tipocambio_nuevo : PageController
    {
        protected override void initialize()
        {
            TipoCambioBean loTipoCambioBean = TipoCambioController.buscarTipoCambio(cboTipox.SelectedValue.ToString());
            if (loTipoCambioBean != null)
            {
                txtValor.Value = loTipoCambioBean.valor;
            }
            else
            {
                txtValor.Value = string.Empty;
            }
        }
    }
}