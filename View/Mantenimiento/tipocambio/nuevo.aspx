﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.tipocambio.Mantenimiento_tipocambio_nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script>

        $(document).ready(function () {
            $('#saveReg').removeAttr("disabled");
            $(".cz-form-content-input-select").change(function () {
                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {
                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $('#cboTipox').change(function () {
                llenarValor($(this).val());
            });

            $('#cboTipox').trigger('change');

            function llenarValor(Tipo){
                var URL = "tipocambio.aspx/LlenaValor";
                var obj = new Object();
                obj.tipo = Tipo;
                $.ajax({
                    url: URL,
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        $('#txtValor').val(msg.d);
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "findtipocambio");
                    }
                });
            }

        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.tipox = $('#cboTipox').val();
            strData.valor = $('#txtValor').val();

            return strData;
        }

        function data() {
            var sr = new getData();
            return sr.codigoperfil;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#cboTipo').val($("#McboPerfil option:first").val());
            $('#txtValor').val('');
            $('#MtxtLogin').val('');
            $('#MtxtClave').val('');
            $('#McboPerfil').val($("#McboPerfil option:first").val());
            $('#McboGeocerca').val($("#McboGeocerca option:first").val());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
            Crear Nuevo Tipo Cambio
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO)%></p>
                    <asp:DropDownList ID="cboTipox" runat="server" CssClass="cz-form-content-input-select">
                        <asp:ListItem Value="0" Text="De Soles a Dólares para Pedidos"></asp:ListItem>
                        <asp:ListItem Value="1" Text="De Dólares a Soles para Pedidos"></asp:ListItem>
                        <asp:ListItem Value="2" Text="De Soles a Dólares para Cobranzas"></asp:ListItem>
                        <asp:ListItem Value="3" Text="De Dólares a Soles para Cobranzas"></asp:ListItem>
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
        <div class="cz-form-content">
            <p>
                Valor
                *</p>
            <input type="text" id="txtValor" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class="requerid cz-form-content-input-text"
                maxlength="100" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>

    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>
</body>
</html>
