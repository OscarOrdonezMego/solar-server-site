﻿using System;
using Controller;
using Model.bean;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.tipocambio
{
    public partial class Mantenimiento_tipocambio_tipocambio : PageController
    {
        public static String lsCodMenu = "MPG";
        protected override void initialize()
        {

        }
        [WebMethod]
        public static String CrearTipoCambio(String tipox, String valor)
        {
            try
            {
                TipoCambioBean loTipoCambio = new TipoCambioBean();
                loTipoCambio.codigo = tipox;
                loTipoCambio.valor = valor;
                Int32 respuesta = TipoCambioController.crearTipoCambio(loTipoCambio);
                return Operation.Condicion.If(respuesta);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [WebMethod]
        public static String LlenaValor(string tipo)
        {
            TipoCambioBean loTipoCambioBean = TipoCambioController.buscarTipoCambio(tipo);
            if (loTipoCambioBean != null)
            {
                return loTipoCambioBean.valor;
            }
            else
            {
                return string.Empty;
            }
        }

    }
}