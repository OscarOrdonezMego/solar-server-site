﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.tipocambio
{
    public partial class Mantenimiento_tipocambio_grid : PageController
    {
        public static String lsCodMenu = "MTC";
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);


            if (dataJSON != null)
            {
                Int32 pagina = Int32.Parse(dataJSON["pagina"].ToString());
                Int32 totalpagina = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
                String tipo = dataJSON["tipo"].ToString();
                String flag = dataJSON["flag"].ToString();
                
                TipoCambioBean tc = new TipoCambioBean();
                tc.codigo = tipo;
                tc.flag = flag;
                
                PaginateTipoCambioBean paginate = (new TipoCambioController()).findTipoCambioBean(tc, pagina, totalpagina);

                //PaginateGrupoBeans paginate = new GrupoController.fnBuscarListarGrupo(pagina, totalpagina, codigo, nombre, flag);

                if ((pagina > 0) && pagina <= paginate.totalPages)
                {
                    this.lbTpagina.Text = paginate.totalPages.ToString();
                    this.lbPagina.Text = pagina.ToString();

                    if (pagina == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if ((pagina == paginate.totalPages))
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<TipoCambioBean> lst = paginate.lstResultados;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }
        }
    }
}