﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.tipocambio.Mantenimiento_tipocambio_tipocambio" Codebehind="tipocambio.aspx.cs"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <script>
        var urlins = 'nuevo.aspx';
        var urlbus = 'grid.aspx';
        var urlsavN = 'tipocambio.aspx/CrearTipoCambio';

        $(document).ready(function () {
            addReg();
            busReg();
            $('#buscar').trigger("click");

        });

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.tipo = $('#cboTipo').val();
            strData.login = $('#txtlogin').val();
            strData.perfil = $('#cboPerfil').val();
            strData.flag = $('#chkflag').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.orden = $('#hdnOrden').val();
            strData.busNombre = $('#hdnBNombre').val();
            return strData;
        }
    </script>
</head>
<body>
 <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/icon-users.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            Mantenimiento Tipo Cambio</p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
              <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.CREAR))
                { %>
                <input type="button" id="cz-form-box-nuevo" class="cz-form-content-input-button cz-form-content-input-button-image form-button addReg cz-form-box-content-button cz-util-right"
                    data-grid-id="divGridViewData" value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NUEVO) %>" />
              <%} %>
             
            </div>
            <div class="cz-form-box-content">

                 <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_TIPO)%></p>
                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="cz-form-content-input-select">
                        <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                        <asp:ListItem Value="0" Text="De Soles a Dólares para Pedidos"></asp:ListItem>
                        <asp:ListItem Value="1" Text="De Dólares a Soles para Pedidos"></asp:ListItem>
                        <asp:ListItem Value="2" Text="De Soles a Dólares para Cobranzas"></asp:ListItem>
                        <asp:ListItem Value="3" Text="De Dólares a Soles para Cobranzas"></asp:ListItem>
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>

                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HABILITADO)%></p>
                    <input type="checkbox" class="cz-form-content-input-check" id="chkflag" name="habilitado"
                        checked="checked" />
                </div>

                <div class="cz-form-content cz-util-right cz-util-right-text">
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                </div>
            </div>
            <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div class="form-gridview-data" id="divGridViewData" runat="server">
                            </div>
                            <div class="form-gridview-error" id="divGridViewError" runat="server">
                            </div>
                            <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                <p>
                                    buscando resultados</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Hidden Fields to control pagination-->
            <div id="paginator-hidden-fields">
                <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />
            </div>
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
            </div>
        </div>
    </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>
