﻿
using System;
using System.Web.UI;
using Controller;
using Model.bean;
using System.Web.Services;

namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class Presentacion : System.Web.UI.Page
    {
        public static String ioIdProducto;
        public static String ioProducto;

        protected void Page_Load(object sender, EventArgs e)
        {
            ioIdProducto = Request.QueryString["idcliente"].ToString();
            ioProducto = Request.QueryString["cliente"].ToString();
        }

        [WebMethod]
        public static String fnCrearPresentacion(String codigopre, String codigopro, String nombrepre, String cantidad, String unidadfracionamiento, String presiopre, String presioSoles, String presioDolares, String stock, String descmin, String descmax)
        {
            try
            {
                if ((presioSoles.Trim() == string.Empty || presioSoles.Trim() == "0") && (presioDolares.Trim() == string.Empty || presioDolares.Trim() == "0"))
                {
                    throw new Exception("Se debe ingresar al menos un precio presentación");
                }
                ProductoPresentacionBean loProductoPresentacionBean = null;
                loProductoPresentacionBean = new ProductoPresentacionBean();
                loProductoPresentacionBean.Descmin = (descmin.Length > 0) ? descmin : "0";
                loProductoPresentacionBean.Descmax = (descmax.Length > 0) ? descmax : "0";

                loProductoPresentacionBean.CodigoPresentacion = codigopre;
                loProductoPresentacionBean.CodigoProducto = codigopro;
                loProductoPresentacionBean.Nombre = nombrepre;
                loProductoPresentacionBean.Cantidad = cantidad;
                loProductoPresentacionBean.UnidaddeFrancionamieno = unidadfracionamiento;
                loProductoPresentacionBean.PrecioPre = "0";//presiopre;

                if (presioSoles.Trim() != string.Empty && presioSoles.Trim() != "0") loProductoPresentacionBean.PrecioBaseSoles = presioSoles;
                if (presioDolares.Trim() != string.Empty && presioDolares.Trim() != "0") loProductoPresentacionBean.PrecioBaseDolares = presioDolares;

                loProductoPresentacionBean.Stock = (stock.Length > 0) ? stock : "0";
                if (Convert.ToInt32(loProductoPresentacionBean.Descmin) > Convert.ToInt32(loProductoPresentacionBean.Descmax))
                {
                    throw new Exception("Descuento minimo debe ser menor al descuento maximo");
                }

                loProductoPresentacionBean.Condicion = 1;
                Int32 multiplo = fnMultiplos(Int32.Parse(cantidad), Int32.Parse(unidadfracionamiento));
                if (multiplo == 1)
                {
                    throw new Exception("No puede fraccionar " + cantidad + " en " + unidadfracionamiento);
                }
                else
                {
                    String respuesta = ProductoPresentacionController.fnVerSiExiste(loProductoPresentacionBean);
                    if (respuesta.Equals("0") || respuesta.Equals(""))
                    {
                        ProductoPresentacionController.subCrearPresentacion(loProductoPresentacionBean);
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }
                    else
                    {
                        throw new Exception("El codigo " + codigopre + " ya esta registrado.");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [WebMethod]
        public static String fnEditarPresentacion(String codigopre, String codigopro, String nombrepre, String cantidad, String unidadfracionamiento, String presiopre, String presioSoles, String presioDolares, String stock, String descmin, String descmax)
        {
            try
            {
                if ((presioSoles.Trim() == string.Empty || presioSoles.Trim() == "0") && (presioDolares.Trim() == string.Empty || presioDolares.Trim() == "0"))
                {
                    throw new Exception("Se debe ingresar al menos un precio presentación");
                }
                ProductoPresentacionBean loProductoPresentacionBean = null;
                loProductoPresentacionBean = new ProductoPresentacionBean();
                loProductoPresentacionBean.CodigoPresentacion = codigopre;
                loProductoPresentacionBean.CodigoProducto = codigopro;
                loProductoPresentacionBean.Nombre = nombrepre;
                loProductoPresentacionBean.Cantidad = cantidad;
                loProductoPresentacionBean.UnidaddeFrancionamieno = unidadfracionamiento;
                loProductoPresentacionBean.PrecioPre = "0"; //presiopre;
                if (presioSoles.Trim() != string.Empty && presioSoles.Trim() != "0") loProductoPresentacionBean.PrecioBaseSoles = presioSoles;
                if (presioDolares.Trim() != string.Empty && presioDolares.Trim() != "0") loProductoPresentacionBean.PrecioBaseDolares = presioDolares;
                loProductoPresentacionBean.Stock = (stock.Length > 0) ? stock : "0";
                if (Convert.ToInt32(descmin) > Convert.ToInt32(descmax))
                {
                    throw new Exception("Descuento minimo debe ser menor al descuento maximo");
                }
                else
                {
                    loProductoPresentacionBean.Descmin = (descmin.Length > 0) ? descmin : "0";
                    loProductoPresentacionBean.Descmax = (descmax.Length > 0) ? descmax : "0";
                }
                ProductoPresentacionController.subEditarPresentacion(loProductoPresentacionBean);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [WebMethod]
        public static String BorrarPresentacion(String codigos, String flag)
        {
            try
            {
                ProductoPresentacionController.subBorrarPresentacion(codigos, flag);
                return "Se eliminaron con Exito";
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private static Int32 fnMultiplos(Int32 poCantidad, Int32 poNumerofracionar)
        {
            Int32 liTotal = 0;
            liTotal = poCantidad % poNumerofracionar;
            return liTotal;
        }
    }
}