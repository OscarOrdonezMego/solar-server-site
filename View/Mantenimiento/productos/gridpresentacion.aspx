﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Mantenimiento.productos.gridpresentacion" Codebehind="gridpresentacion.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
    var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
        $(document).ready(function () {
            if (flag == "T") {
                $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>');
            } else {
                $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_RESTAURA)%>');
            }
        });
      </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="" type="checkbox">
                            </th>
                          
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>
                             <th  nomb="">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                             <th  nomb="">
                               <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%>
                            </th>
                             <%-- <th  nomb="">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIOBASE)%>
                            </th>--%>
                            <th  nomb="PrecioSoles">
                                Precio Soles
                            </th>
                            <th  nomb="PrecioDolares">
                                Precio Dólares
                            </th>
                             <th  nomb="" <%=liValidarAlmacenOcultar %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%>
                            </th>
                              <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                                  {%>
                            <th  nomb=""  <%= visibleHabilitarDsctoPorProducto %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMIN)%>
                            </th>
                            <th  nomb=""  <%= visibleHabilitarDsctoPorProducto %>> 
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMAX)%>
                            </th>
                            <%}%>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                              { %>
                            <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                          <% }%>
                          <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                            {%>
                            <th style="width: 40px;" id="Tieliminar"> 
                            </th>
                           <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("CodigoPresentacion")%>" value="<%# Eval("CodigoPresentacion")%>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("Codigo")%>
                    </td>
                    <td>
                        <%# Eval("Nombre")%>
                    </td>
                    
                    <td>
                        <%# Eval("Cantidad")%>
                    </td>
                    <td>
                        <%# Eval("UnidaddeFrancionamieno")%>
                    </td>
                    <%--<td>
                        <%# Eval("PrecioPre")%>
                    </td>--%>
                    <td>
                        <%# Eval("PrecioBaseSoles")%>
                    </td>
                    <td>
                        <%# Eval("PrecioBaseDolares")%>
                    </td>
                    <td <%=liValidarAlmacenOcultar %>>
                        <%# Eval("Stock")%>
                    </td>
                      <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                          {%>
                   <td <%= visibleHabilitarDsctoPorProducto %>>
                        <%# Eval("Descmin")%>
                    </td>
                    <td <%= visibleHabilitarDsctoPorProducto %>>
                        <%# Eval("Descmax")%>
                    </td>
                    <%} %>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                    {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("CodigoPresentacion")%>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                   <%} %>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     {%>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("CodigoPresentacion")%>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                   <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                      <td>
                        <input id="<%# Eval("CodigoPresentacion")%>" value="<%# Eval("CodigoPresentacion")%>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("Codigo")%>
                    </td>
                     <td>
                        <%# Eval("Nombre")%>
                    </td>
                   
                    <td>
                        <%# Eval("Cantidad")%>
                    </td>
                    <td>
                        <%# Eval("UnidaddeFrancionamieno")%>
                    </td>
                    <%--<td>
                        <%# Eval("PrecioPre")%>
                    </td>--%>
                    <td>
                        <%# Eval("PrecioBaseSoles")%>
                    </td>
                    <td>
                        <%# Eval("PrecioBaseDolares")%>
                    </td>
                    <td <%=liValidarAlmacenOcultar %>>
                        <%# Eval("Stock")%>
                    </td>
                       <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                           {%>
                    <td <%= visibleHabilitarDsctoPorProducto %>>
                        <%# Eval("Descmin")%>
                    </td>
                    <td <%= visibleHabilitarDsctoPorProducto %>>
                        <%# Eval("Descmax")%>
                    </td>
                    <%} %>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      { %>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("CodigoPresentacion")%>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                <%} %>
                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                  { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("CodigoPresentacion")%>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                  <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
