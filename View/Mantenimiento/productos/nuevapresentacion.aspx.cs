﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller;
using Model.bean;
using Model;

namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class nuevapresentacion : PageController
    {
        public static String ioIdProducto = "";
        public static String liValidarAlmacenOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        private String valorHabilitarDscto = "F";
        private String valorHabilitarDsctoPorProducto = "F";
        public String visibleHabilitarDsctoPorProducto = "";
        public String visibleHabilitarDscto = "";
        private String obligatorioIngresar = "";

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            else
            {
                liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                liValidarAlmacenOcultar = String.Empty;
            }
            ioIdProducto = Request.QueryString["idproducto"].ToString();
            hidPKPRO.Value = ioIdProducto;
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                Int32 liCodigo = Int32.Parse(dataJSON["codigo"].ToString());
                ProductoPresentacionBean loProductoPresentacionBean = ProductoPresentacionController.buscarPresentacion(liCodigo);
                MPPtxtcodigo.Value = loProductoPresentacionBean.Codigo;
                MPPtxtnombre.Value = loProductoPresentacionBean.Nombre;
                MPPtxtcantidad.Value = loProductoPresentacionBean.Cantidad;
                MPPtxtunidadfraccionamiento.Value = loProductoPresentacionBean.UnidaddeFrancionamieno;
                MPPtxtpreciopresentacion.Value = loProductoPresentacionBean.PrecioPre;
                MPPtxtprecioSoles.Value = loProductoPresentacionBean.PrecioBaseSoles;
                MPPtxtprecioDolares.Value = loProductoPresentacionBean.PrecioBaseDolares;
                MPPtxtstock.Value = loProductoPresentacionBean.Stock;
                MPPtxtdescuentomin.Value = loProductoPresentacionBean.Descmin;
                MPPtxtdescuentomax.Value = loProductoPresentacionBean.Descmax;
                hidPk.Value = loProductoPresentacionBean.CodigoPresentacion;
            }

            valorHabilitarDscto = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor;
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);

            this.visibleHabilitarDscto = (this.valorHabilitarDscto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            this.obligatorioIngresar = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.T.ToString() ? "cz-form-content-input-text" : "cz-form-content-input-text");

            if (liValidarAlmacenInt != 0)
            {
                this.MPPtxtdescuentomin.Attributes.Add("class", obligatorioIngresar);
                this.MPPtxtdescuentomax.Attributes.Add("class", obligatorioIngresar);
            }
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
    }
}
