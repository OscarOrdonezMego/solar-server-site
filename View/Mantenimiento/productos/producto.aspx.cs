﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Services;
using Controller;
using Model.bean;
using Controller.functions;
using Model;

/// <summary>
/// @001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
/// </summary>
namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class producto : PageController
    {
        public static String lsCodMenu = "MPR";

        //@001 I
        private static String valorHabilitarDsctoPorProducto = "F";
        //@001 F

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            llenarCboFamilia();
            llenarCboMarca();
        }
        private void llenarCboFamilia()
        {
            List<FamiliaProductoBean> loLstFamiliaBean = ProductoFamiliaController.fnListarFamiliaProductos();
            if (loLstFamiliaBean.Count != 0)
            {
                cboFamilia.Items.Insert(0, "Todos");
                cboFamilia.Items[0].Value = "0";
                for (int i = 0; i < loLstFamiliaBean.Count; i++)
                {
                    cboFamilia.Items.Insert(i + 1, loLstFamiliaBean[i].nombre);
                    cboFamilia.Items[i + 1].Value = loLstFamiliaBean[i].id.ToString();

                }
            }
            else
            {
                cboFamilia.Items.Insert(0, "Todos");
                cboFamilia.Items[0].Value = "0";
            }
        }
        private void llenarCboMarca()
        {
            List<MarcaProductoBean> lolstMarcaBean = ProductoMarcaController.fnlistaMarcaProducto();
            if (lolstMarcaBean.Count != 0)
            {
                cboMarca.Items.Insert(0, "Todos");
                cboMarca.Items[0].Value = "0";
                for (int i = 0; i < lolstMarcaBean.Count; i++)
                {
                    cboMarca.Items.Insert(i + 1, lolstMarcaBean[i].nombre);
                    cboMarca.Items[i + 1].Value = lolstMarcaBean[i].id.ToString();

                }
            }
            else
            {
                cboMarca.Items.Insert(0, "Todos");
                cboMarca.Items[0].Value = "0";
            }
        }

        [WebMethod]
        public static void borrar(String codigos, String flag)
        {

            try
            {

                ProductoController.borrar(codigos, flag);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String id, String codigo, String nombre, String preciobasesoles, String preciobasedolares, String unidad, String stock
            , String descmin, String descmax, String familia, String marca //@001 I/F
            )
        {
            try
            {

                descmin = descmin.Equals("") ? "0" : descmin;
                descmax = descmax.Equals("") ? "0" : descmax;

                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }
                else
                {
                    if ((preciobasesoles.Trim() == string.Empty || preciobasesoles.Trim() == "0") && (preciobasedolares.Trim() == string.Empty || preciobasedolares.Trim() == "0"))
                    {
                        throw new Exception("Se debe ingresar al menos un precio");
                    }
                    ProductoBean bean = new ProductoBean();
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.flag = "T";
                    bean.familia = familia;
                    bean.marca = marca;
                    bean.preciobase = "0";
                    if (preciobasesoles.Trim() != string.Empty && preciobasesoles.Trim() != "0")
                    {
                        bean.preciobasesoles = preciobasesoles;
                    }
                    else
                    {
                        //bean.preciobasesoles = "0";
                    }
                    if (preciobasedolares.Trim() != string.Empty && preciobasedolares.Trim() != "0")
                    {
                        bean.preciobasedolares = preciobasedolares;
                    }
                    else
                    {
                        //bean.preciobasedolares = "0";
                    }
                    bean.unidad = unidad;
                    if (stock.Length > 0)
                    {
                        bean.stock = stock;
                    }
                    else
                    {
                        bean.stock = "0";
                    }

                    bean.color = "$COLB";
                    bean.id = id;
                    if (descmin.Length > 0)
                    {
                        bean.descuentomin = descmin;
                    }
                    else
                    {
                        bean.descuentomin = "0";
                    }
                    if (descmax.Length > 0)
                    {
                        bean.descuentomax = descmax;
                    }
                    else
                    {
                        bean.descuentomax = "0";
                    }
                    if (Convert.ToInt32(descmin) > Convert.ToInt32(descmax))
                    {
                        throw new Exception("Descuento minimo debe ser menor al descuento Maximo");
                    }
                    else
                    {
                        ProductoController.crear(bean);
                        return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                    }

                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String id, String codigo, String nombre, String precio, String preciobasesoles, String preciobasedolares, 
            String unidad, String stock , String descmin, String descmax, String familia, String marca //@001 I/F
            )
        {
            try
            {
                if ((preciobasesoles.Trim() == string.Empty || preciobasesoles.Trim() == "0") && (preciobasedolares.Trim() == string.Empty || preciobasedolares.Trim() == "0"))
                {
                    throw new Exception("Se debe ingresar al menos un precio");
                }
                ProductoBean bean = new ProductoBean();
                bean.codigo = codigo;
                bean.nombre = nombre;
                bean.familia = familia;
                bean.marca = marca;
                bean.flag = "T";
                //if (precio.Length > 0)
                //{
                //    bean.preciobase = precio;
                //}
                //else
                //{
                    bean.preciobase = "0";
                //}
                if (preciobasesoles.Trim() != string.Empty && preciobasesoles.Trim() != "0")
                {
                    bean.preciobasesoles = preciobasesoles;
                }
                else
                {
                    //bean.preciobasesoles = "0";
                }
                if (preciobasedolares.Trim() != string.Empty && preciobasedolares.Trim() != "0")
                {
                    bean.preciobasedolares = preciobasedolares;
                }
                else
                {
                    //bean.preciobasedolares = "0";
                }
                bean.unidad = unidad;
                if (stock.Length > 0)
                {
                    bean.stock = stock;
                }
                else
                {
                    bean.stock = "0";
                }

                bean.color = "$COLB";
                bean.id = id;
                if (descmin.Length > 0)
                {
                    bean.descuentomin = descmin;
                }
                else
                {
                    bean.descuentomin = "0";
                }
                if (descmax.Length > 0)
                {
                    bean.descuentomax = descmax;
                }
                else
                {
                    bean.descuentomax = "0";
                }
                if (Convert.ToInt32(descmin) > Convert.ToInt32(descmax))
                {
                    throw new Exception("Descuento minimo debe ser menor al descuento Maximo");
                }
                else
                {
                    ProductoController.crear(bean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }


            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}