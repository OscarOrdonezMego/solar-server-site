﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

/// <summary>
/// @001 GMC 15/04/2015 Ajustes para validar visualización de Descuento Mínimo y Máximo
/// </summary>
namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class grid : PageController
    {
        public static String lsCodMenu = "MPR";
        //@001 I
        private String valorHabilitarDsctoPorProducto = "F";
        public String visibleHabilitarDsctoPorProducto = "";
        public static String liValidarOcultar;
        public static Int32 liRespuestaint;
        public static String liValidarMostrar;
        public static String liValidarAlmacenOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        //@001 F
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarOcultar = String.Empty;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarOcultar = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            else
            {
                if (liRespuestaint == 0)
                {
                    liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                    //  liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_BLOCK;
                }
                else
                {
                    liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                    liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
                }

            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String codigo = dataJSON["codigo"].ToString();
            String nombre = dataJSON["nombre"].ToString();
            String pagina = dataJSON["pagina"].ToString();
            String filas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;// dataJSON["filas"].ToString();
            String flag = dataJSON["flag"].ToString();
            String familia = dataJSON["familia"].ToString();
            String marca = dataJSON["marca"].ToString();
            //@001 I
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);
            this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            //@001 F

            PaginateProductoBean paginate = ProductoController.paginarBuscar(codigo, nombre, flag, pagina, filas, familia, marca);

            if ((Int32.Parse(pagina) > 0) && (Int32.Parse(pagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina;

                if (Int32.Parse(pagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(pagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<ProductoBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();

            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }

        //@001 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        //@001 F
    }
}