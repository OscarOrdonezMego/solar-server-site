﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits=" Pedidos_Site.Mantenimiento.productos.grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 15/04/2015 Ajustes para validar visualización de Descuento Mínimo y Máximo
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
       <script>
           var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
           $(document).ready(function () {
               if (flag == "T") {
                   $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>');
               } else {
                   $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_RESTAURA)%>');
               }
           });
      </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width:40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("ID") %>" type="checkbox">
                            </th>
                            <th style="width:100px;" class="center" nomb="CODIGO">
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>                            
                            <th nomb="PRESOLES" <%=liValidarMostrar %>>
                                Precio Soles 
                            </th>
                            <th nomb="PREDOLARES" <%=liValidarMostrar %>>
                                Precio Dólares 
                            </th>
                            <th nomb="UNM">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNIDADMEDIDA)%>
                            </th>
                             <th nomb="STOCK" <%=liValidarAlmacenOcultar%> >
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%>
                            </th>
                              <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()) && fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.F.ToString()))
                                  {%>
                                <th nomb="DESC_MIN" <%= visibleHabilitarDsctoPorProducto %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTO_MIN)%>
                                </th>              
                                <th nomb="DESC_MAX" <%= visibleHabilitarDsctoPorProducto %>>
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTO_MAX)%>
                                </th>
                             <%} %>
                              <th nomb="Familia">
                                   <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FAMILIA)%>
                                </th>
                                  <th nomb="Marca">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MARCA)%>
                                </th>
                             <th  <%=liValidarOcultar %>>
                               <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                            </th>
                            <%--@001 F--%>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                              {%>
                            <th style="width:40px;">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                          <%} %>
                          <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                            { %>
                            <th style="width:40px;" id="Tieliminar">
                                 
                            </th>
                            <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>                  
                    <td <%=liValidarMostrar %>>
                        <%# Eval("preciobasesoles")%>
                    </td>
                    <td <%=liValidarMostrar %>>
                        <%# Eval("preciobasedolares")%>
                    </td>
                    <td>
                        <%# Eval("unidad")%>
                    </td>
                    <td <%=liValidarAlmacenOcultar %> >
                        <%# Eval("stock")%>
                    </td>
                          <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()) &&
                                  fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.F.ToString()))
                          {%>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descuentomin")%>
                        </td>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descuentomax")%>
                        </td>
                     <%} %>
                     <td >
                            <%# Eval("Familia")%>
                        </td>
                           <td >
                            <%# Eval("Marca")%>
                        </td>
                   <td <%=liValidarOcultar %>>
                             <a  style="cursor: pointer;" 
                             href="presentacion.aspx?idcliente=<%# Eval("ID") %>&cliente=<%# Eval("NOMBRE") %>">
                            <center><img src="../../imagery/all/icons/detalle.png" border="0" title="Presentacion" /></center></a>
                    </td>

                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      { %>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" /></a>
                    </td>
                <%} %>
                <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                  { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" /></a>
                    </td>
                   <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                   <td <%=liValidarMostrar %>>
                        <%# Eval("preciobasesoles")%>
                    </td>
                    <td <%=liValidarMostrar %>>
                        <%# Eval("preciobasedolares")%>
                    </td>
                    <td>
                        <%# Eval("unidad")%>
                    </td>
                   <td <%=liValidarAlmacenOcultar %> <%=liValidarMostrar %>>
                        <%# Eval("stock")%>
                    </td>
                          <%if (fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString())
                                  && fnObtenerConfiguracionPorCodigo(Model.Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Model.Enumerados.FlagHabilitado.F.ToString())
                           )
                          {%>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descuentomin")%>
                        </td>
                        <td <%= visibleHabilitarDsctoPorProducto %>>
                            <%# Eval("descuentomax")%>
                        </td>
                     <%} %>
                      <td >
                            <%# Eval("Familia")%>
                        </td>
                           <td >
                            <%# Eval("Marca")%>
                        </td>
                    <%--@001 F--%>
                     <td <%=liValidarOcultar %>>
                             <a  style="cursor: pointer;" 
                             href="presentacion.aspx?idcliente=<%# Eval("ID") %>&cliente=<%# Eval("NOMBRE") %>">
                            <center><img src="../../imagery/all/icons/detalle.png" border="0" title="Presentacion" /></center></a>
                    </td>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" /></a>
                    </td>
                    <%} %>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                      { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" /></a>
                    </td>
                   <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
