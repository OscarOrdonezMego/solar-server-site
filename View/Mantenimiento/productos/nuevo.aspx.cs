﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Model.bean;
using Newtonsoft.Json;
using Controller;
using Model;

/// <summary>
/// @001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
/// </summary>
namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class Mantenimiento_productos_nuevo : PageController
    {
        public bool GenCodigo;

        //@001 I
        private String valorHabilitarDscto = "F";
        private String valorHabilitarDsctoPorProducto = "F";
        private String obligatorioIngresar = "";
        public String visibleHabilitarDsctoPorProducto = "";
        public String visibleHabilitarDscto = "";
        public static String liValidarAlmacenOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        public String visiblePresentacion = "";
        public String valorPresentacion = "F";

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            else
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_BLOCK;
            }
            if (!IsPostBack)
            {

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOPRODUCTO);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);


                cboListaFamiliaProducto();
                cboListaMarcaProducto();
                if (dataJSON != null)
                {
                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARPRODUCTO);
                    ProductoBean usuario = ProductoController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"];
                    cboFamiliaProducto.SelectedValue = usuario.familia;
                    cboMarcaProducto.SelectedValue = usuario.marca;
                    MtxtNombre.Value = usuario.nombre;
                    MtxtCodigo.Value = usuario.codigo;
                    MtxtPrecio.Value = usuario.preciobase;
                    MtxtPrecioSoles.Value = usuario.preciobasesoles;
                    MtxtPrecioDolares.Value = usuario.preciobasedolares;
                    MtxtUnidad.Value = usuario.unidad;
                    MtxtStock.Value = usuario.stock;
                    MtxtCodigo.Disabled = true;
                    MtxtDescuentoMinimo.Value = usuario.descuentomin; //@001 I/F
                    MtxtDescuentoMaximo.Value = usuario.descuentomax; //@001 I/F
                }
                else
                {
                    hidPk.Value = "";
                    MtxtCodigo.Disabled = GenCodigo;
                }

                if (GenCodigo)
                    MtxtNombre.Focus();
                else
                    MtxtCodigo.Focus();

                //@001 Iz

                //this.ObtenerDatosFuncion(Model.Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO, out this.valorHabilitarDscto);
                valorHabilitarDscto = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.CFG_HABILITAR_DESCUENTO).Valor;
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);
                valorPresentacion = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor;

                this.visiblePresentacion = (this.valorPresentacion == Model.Enumerados.FlagHabilitado.T.ToString() ? " style='display:none'" : "");
                this.visibleHabilitarDscto = (this.valorHabilitarDscto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                this.obligatorioIngresar = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.T.ToString() ? "cz-form-content-input-text" : "cz-form-content-input-text");
                if (liValidarAlmacenInt != 0)
                {
                    this.MtxtDescuentoMinimo.Attributes.Add("class", obligatorioIngresar);
                    this.MtxtDescuentoMaximo.Attributes.Add("class", obligatorioIngresar);
                }

                //@001 F
            }
        }

        //@001 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        private void cboListaFamiliaProducto()
        {
            cboFamiliaProducto.DataTextField = "Nombre";
            cboFamiliaProducto.DataValueField = "Id";
            cboFamiliaProducto.DataSource = ProductoFamiliaController.fnListarFamiliaProductos();
            cboFamiliaProducto.DataBind();
            cboFamiliaProducto.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }
        private void cboListaMarcaProducto()
        {
            cboMarcaProducto.DataTextField = "Nombre";
            cboMarcaProducto.DataValueField = "Id";
            cboMarcaProducto.DataSource = ProductoMarcaController.fnlistaMarcaProducto();
            cboMarcaProducto.DataBind();
            cboMarcaProducto.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }
        //@001 F
    }
}