﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.productos.Mantenimiento_productos_nuevo" Codebehind="nuevo.aspx.cs" %>

<%--
@001 GMC 15/04/2015 Ajustes para actualizar Descuento Minimo y Maximo
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
     <script type="text/javascript">
     
        $(document).ready(function () {
            
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });


     

        });
    </script>
    <script type="text/javascript">
       
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.codigo = $('#MtxtCodigo').val();
            strData.nombre = $('#MtxtNombre').val();
            strData.precio = $('#MtxtPrecio').val();
            strData.preciobasesoles = $('#MtxtPrecioSoles').val();
            strData.preciobasedolares = $('#MtxtPrecioDolares').val();
            strData.unidad = $('#MtxtUnidad').val();
            strData.stock = $('#MtxtStock').val();
            strData.accion = $('#MhAccion').val();
            //@001 I
            strData.familia = $('#cboFamiliaProducto').val();
            strData.marca = $('#cboMarcaProducto').val();
            strData.descmin = $('#MtxtDescuentoMinimo').val();
            strData.descmax = $('#MtxtDescuentoMaximo').val();
            //@001 F
            return strData;
        }
        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtNombre').val('');
            $('#MtxtPrecio').val('');
            $('#MtxtPrecioSoles').val('');
            $('#MtxtPrecioDolares').val('');
            $('#MtxtStock').val('');
            $('#MtxtUnidad').val('');
            $('#cboFamiliaProducto').val($("#McboPerfil option:first").val());
            $('#cboMarcaProducto').val($("#McboPerfil option:first").val());
            //@001 I
            $('#MtxtDescuentoMinimo').val('');
            $('#MtxtDescuentoMaximo').val('');
            //@001 F
        }
       /* $(document).ready(function () {
            $('#MtxtPrecio').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9.]/g, '');
            });
            $('#MtxtDescuentoMin').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9.]/g, '');
            });
            $('#MtxtDescuentoMax').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9.]/g, '');
            });
        });*/
        $(document).ready(function () {
           var valor = '<%=liValidarAlmacenInt%>';
            if (valor == 0) {
                $('#desmin').prop('display', 'none');
                $('#desmax').prop('display', 'none');
            } else {
                $('#desmin').prop('display', 'block');
                $('#desmax').prop('display', 'block');
            }
            var valor = '<%=liRespuestaint%>';
            if (valor == 1) {
                $('#oculstock').css('display', 'none');
                $('#oculprecio').css('display', 'none');
                $('#desmin').css('display', 'none');
                $('#desmax').css('display', 'none');
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
           </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p>  <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%> *</p>
            <input type="text" id="MtxtCodigo" runat="server"  maxlength="20" class="requerid cz-form-content-input-text"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
             <div class="cz-form-content">
                <p>
                   Familia
                </p>
                <asp:DropDownList ID="cboFamiliaProducto" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
            <div class="cz-form-content">
                <p>
                   Marca
                </p>
                <asp:DropDownList ID="cboMarcaProducto" runat="server" CssClass="cz-form-content-input-select">
                </asp:DropDownList>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
        <div class="cz-form-content">
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%> *</p>
            <input type="text" id="MtxtNombre" runat="server" class="requerid cz-form-content-input-text" maxlength="100"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="cz-form-content" <%=liValidarAlmacenOcultar %> id="oculstock">
            <p> <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%> *</p>
            <input type="text" id="MtxtStock" runat="server" onkeypress="fc_PermiteNumeros()" class="cz-form-content-input-text" maxlength="8"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
       </div>
      <div class="cz-form-content" >
            <p>Precio Soles</p>
            <input type="hidden" id="MtxtPrecio" runat="server"/>
            <input type="text" id="MtxtPrecioSoles" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class=" cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
       </div>
       <div class="cz-form-content">
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNIDADMEDIDA)%> *</p>
            <input type="text" id="MtxtUnidad" runat="server"  class="cz-form-content-input-text " maxlength="8"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
       <div class="cz-form-content">
            <p>Precio Dólares</p>
            <input type="text" id="MtxtPrecioDolares" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class=" cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
       </div>
        <!--@001 I-->
        <div class="cz-form-content" <%= visibleHabilitarDsctoPorProducto %>  <%= visibleHabilitarDscto %>  <%= visiblePresentacion %>>
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMIN)%> *</p>
            <input type="text" id="MtxtDescuentoMinimo" runat="server" onkeypress="fc_PermiteDecimalOld(this)"  maxlength="20" class=" cz-form-content-input-text"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="cz-form-content" <%= visibleHabilitarDsctoPorProducto %> <%= visibleHabilitarDscto %>  <%= visiblePresentacion %>>
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMAX)%> *</p>
            <input type="text" id="MtxtDescuentoMaximo" runat="server" onkeypress="fc_PermiteDecimalOld(this)" maxlength="20" class="cz-form-content-input-text"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <!--@001 F-->

        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
    </div>
    <div class="modal-footer">
        
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button" value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
     <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    </form>
</body>
</html>
