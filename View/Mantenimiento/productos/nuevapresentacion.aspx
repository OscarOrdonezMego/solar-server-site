﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.productos.nuevapresentacion" Codebehind="nuevapresentacion.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
     <script type="text/javascript">
         $(document).ready(function () {
             var enable = $("#hidPk").val();
             if (enable == '') {
                 $("#MPPtxtcodigo").prop("disabled", false);
             } else if (enable.length > 0) {
                 $("#MPPtxtcodigo").prop("disabled", true);
             }
             $('#MPPtxtcantidad').keyup(function () {
                 var cantidad = $(this).val();
                 $('#MPPtxtunidadfraccionamiento').val(cantidad);
             });
         });
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.codigopre = $('#MPPtxtcodigo').val();
            strData.codigopro = $('#hidPKPRO').val();
            strData.nombrepre = $('#MPPtxtnombre').val();
            strData.cantidad = $('#MPPtxtcantidad').val();
            strData.unidadfracionamiento = $('#MPPtxtunidadfraccionamiento').val();            
            strData.presiopre = $('#MPPtxtpreciopresentacion').val();
            strData.presioSoles = $('#MPPtxtprecioSoles').val();
            strData.presioDolares = $('#MPPtxtprecioDolares').val();
            strData.stock = $('#MPPtxtstock').val();
            strData.descmin = $('#MPPtxtdescuentomin').val();
            strData.descmax = $('#MPPtxtdescuentomax').val();
            return strData;
        }
        function clearCampos() {//funcion encargada de limpiar los input
            $('#MPPtxtcodigo').val('');
            $('#MPPtxtnombre').val('');
            $('#MPPtxtcantidad').val('');
            $('#MPPtxtunidadfraccionamiento').val('');
            $('#MPPtxtpreciopresentacion').val('');
            $('#MPPtxtprecioSoles').val('');
            $('#MPPtxtprecioDolares').val('');
            $('#MPPtxtstock').val('');
            $('#MtxtUnidad').val(''); 
            
            //@001 I
            $('#MPPtxtdescuentomin').val('');
            $('#MPPtxtdescuentomax').val('');
            //@001 F
        }
        
        $(document).ready(function () {
            if (($("#MPPtxtcantidad").val().length) == 0) {
                $("#MPPtxtunidadfraccionamiento").prop('disabled', true);
            }
            $("#MPPtxtcantidad").keyup(function () {
                if (($(this).val().length) == 0) {
                    $("#MPPtxtunidadfraccionamiento").prop('disabled', true);
                } else {
                    $("#MPPtxtunidadfraccionamiento").prop('disabled', false);
                }
                if ($(this).val() == "0") {
                    $(this).val('');
                    $("#MPPtxtunidadfraccionamiento").val('');
                    $("#MPPtxtunidadfraccionamiento").prop('disabled', true);
                }
            });
            
            $("#MPPtxtunidadfraccionamiento").keyup(function () {
                if ($(this).val() == "0") {
                    $(this).val('');
                }
                var cantidad = $("#MPPtxtcantidad").val();
                var unidadfrac = $(this).val();
                var total;
                total = cantidad % unidadfrac;
                if (unidadfrac.length == 0) {
                    $("#MPPtxtunidadfraccionamiento").parent().find('span').remove();
                } else { 
                    if (total == 0 ) {
                        $("#MPPtxtunidadfraccionamiento").parent().find('span').remove();
                        $("#MPPtxtunidadfraccionamiento").parent().find('.cz-form-content-input-text-visible').after("<span style='color:#00FF00'>" + "EL " + unidadfrac + $('#hPorcentage').val() + "</span>");
                       
                    } else {
                        $("#MPPtxtunidadfraccionamiento").parent().find('span').remove();
                        $("#MPPtxtunidadfraccionamiento").parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hnoPorcentage').val() + " " + unidadfrac + "</span>");
                        $("#saveReg").prop('disable', true);
                    }
                }
                
            });
        });

    </script>
</head>
<body>
   <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
            Producto Presentación
           </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p>  <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%> *</p>
            <input type="text" id="MPPtxtcodigo" runat="server"  maxlength="20" class="requerid cz-form-content-input-text"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>  <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%> *</p>
            <input type="text" id="MPPtxtnombre" runat="server"  maxlength="20" class="requerid cz-form-content-input-text"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
       

           <div class="cz-form-content">
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%> *</p>
            <input type="text" id="MPPtxtcantidad" runat="server" onkeypress="fc_PermiteNumeros()" class="requerid cz-form-content-input-text" maxlength="8"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
         <div class="cz-form-content">
            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%> *</p>
            <input type="text" id="MPPtxtunidadfraccionamiento" runat="server" onkeypress="fc_PermiteNumeros()"  class="requerid cz-form-content-input-text" maxlength="100"/>
            <asp:HiddenField ID="midproducto" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
          <div class="cz-form-content" style="display:none;">
            <p><%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIO_PRESENTACION)%></p>
            <input type="text" id="MPPtxtpreciopresentacion" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class="cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>

        <div class="cz-form-content">
            <p>Precio Presentación Soles</p>
            <input type="text" id="MPPtxtprecioSoles" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class="cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>

        <div class="cz-form-content">
            <p>Precio Presentación Dólares</p>
            <input type="text" id="MPPtxtprecioDolares" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class="cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>

          <div class="cz-form-content" <%=liValidarAlmacenOcultar %>>
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%> *</p>
            <input type="text" id="MPPtxtstock" runat="server" onkeypress="fc_PermiteNumeros()" class=" cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
          <div class="cz-form-content"  <%=visibleHabilitarDsctoPorProducto %>  <%= visibleHabilitarDscto %>>
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMIN)%> *</p>
            <input type="text" id="MPPtxtdescuentomin" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class=" cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
          <div class="cz-form-content" <%=visibleHabilitarDsctoPorProducto %>  <%= visibleHabilitarDscto %>>
            <p>      <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCUENTOMAX)%> *</p>
            <input type="text" id="MPPtxtdescuentomax" runat="server" onkeypress="fc_PermiteDecimalOld(this)" class=" cz-form-content-input-text" maxlength="20"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button"></div>
            </div>
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
    </div>
    <div class="modal-footer">
        
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button" value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
     <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
       <input id="hPorcentage" type="hidden" value=" Se puede Fraccionar " />
       <input id="hnoPorcentage" type="hidden" value=" No puedes fraccionar" />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
       <asp:HiddenField ID="hidPKPRO" Value="" runat="server" />
    </form>
</body>
</html>
