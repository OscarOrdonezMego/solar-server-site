﻿using System;
using System.Collections.Generic;
using Controller;
using Model.bean;
using Newtonsoft.Json;
using Model;

namespace Pedidos_Site.Mantenimiento.productos
{
    public partial class gridpresentacion : PageController
    {
        public static String lsCodMenu = "MPR";
        public static String liValidarAlmacenOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        public static Int32 ioFraccionamientoMaximoDecimales;
        public String visibleHabilitarDsctoPorProducto = "";
        private String valorHabilitarDsctoPorProducto = Enumerados.FlagHabilitado.F.ToString();
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;

                //Obtener Fraccionamiento Decimal
                String loValorFraccionamientoDecimal = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES).Valor;
                ioFraccionamientoMaximoDecimales = Convert.ToInt32(loValorFraccionamientoDecimal);
            }
            else
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = String.Empty;
                ioFraccionamientoMaximoDecimales = 0;
            }
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                ProductoPresentacionBean loProductoPresentacionBean = new ProductoPresentacionBean();
                loProductoPresentacionBean.Pagina = Int32.Parse(dataJSON["pagina"].ToString());
                loProductoPresentacionBean.TotalFila = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
                loProductoPresentacionBean.CodigoProducto = dataJSON["id"].ToString();
                loProductoPresentacionBean.Estado = dataJSON["flag"].ToString();
                loProductoPresentacionBean.Nombre = dataJSON["nombre"].ToString();
                loProductoPresentacionBean.CodigoPresentacion = dataJSON["codigo"].ToString();

                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO, out this.valorHabilitarDsctoPorProducto);
                this.visibleHabilitarDsctoPorProducto = (this.valorHabilitarDsctoPorProducto == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
                PaginateProductoPresentacionBean paginate = ProductoPresentacionController.paginarBuscar(loProductoPresentacionBean, ioFraccionamientoMaximoDecimales);

                if ((loProductoPresentacionBean.Pagina > 0) && (loProductoPresentacionBean.Pagina <= paginate.TotalFila))
                {
                    this.lbTpagina.Text = paginate.TotalFila.ToString();
                    this.lbPagina.Text = loProductoPresentacionBean.Pagina.ToString();

                    if (loProductoPresentacionBean.Pagina == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (loProductoPresentacionBean.Pagina == paginate.TotalFila)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<ProductoPresentacionBean> lst = paginate.LoLstListaProductoPresentacionBean;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }
        }

        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
    }
}