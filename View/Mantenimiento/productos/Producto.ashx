﻿<%@ WebHandler Language="C#" Class="Producto" %>

using System;
using System.Web;
using System.Collections;
using Model.bean;
using Controller;
using System.Data;
using System.Collections.Generic;

public class Producto : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        String match = context.Request.Params["q"];
        String output = "";

        List<ProductoPresentacionBean> result = ProductoPresentacionController.fnListProductoPaqueteBean();

        output = Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented);
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}