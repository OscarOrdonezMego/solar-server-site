﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.cliente.direcciongrid" Codebehind="direcciongrid.aspx.cs" %>

<%--
@001 GMC 14/04/2015 Se muestra campo DIR_TIPO (P: Pedido, D: Despacho)
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                           <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("dir_pk") %>" type="checkbox">
                            </th>
                            <th style="width:100px;"  nomb="CODIGO">
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>
                            <%--@001 I--%>
                            <th nomb="TIPO_DIR_DES">
                                TIPO
                            </th>
                            <%--@001 F--%>
                            <th style="width:40px;">
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                            <th style="width:40px;">
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("dir_pk") %>" value="<%# Eval("dir_pk") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("dir_tipo_des")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("dir_pk") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar Dirección" /></a>
                    </td>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemRegD form-icon"
                           idcliente="<%# Eval("cli_pk") %>"  cod="<%# Eval("dir_pk") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar Dirección" /></a>
                    </td>
                 
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                   <td>
                        <input id="<%# Eval("dir_pk") %>" value="<%# Eval("dir_pk") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("dir_tipo_des")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("dir_pk") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar Dirección" /></a>
                    </td>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemRegD form-icon"
                              idcliente="<%# Eval("cli_pk") %>"  cod="<%# Eval("dir_pk") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar Dirección" /></a>
                    </td>
                  
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
       <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
