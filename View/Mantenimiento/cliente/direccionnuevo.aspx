﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.cliente.direccionnuevo" Codebehind="direccionnuevo.aspx.cs" %>

<%--
@001 GMC 14/04/2015 Ajustes para actualizar Campo DIR_TIPO (P: Pedido, D: Despacho)
@002 GMC 18/04/2015 Ajustes para visualizar Campo DIR_TIPO
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <style type"text/css">
        #map img
        {
            max-width: none;
        }
        #map label
        {
            width: auto;
            display: inline;
        }
    </style>
    <script type="text/javascript">
        var markerCliente;
        $(document).ready(function () {
            //@001 I
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
            //@001 F

            $(document).ajaxStop(function () {
                
            });
            initializeCliente('<%=latitud %>', '<%= longitud %>');

            setTimeout(function () {
                resizeMapModal()
            }, 1000);


            $("#cz-form-box-verMas").click(function (e) {
                e.preventDefault();
                $('#map').toggle();
                resizeMapModal();
            });

        });
    </script>
    <script type="text/javascript">
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.idcliente = '<%= xIDcliente %>'; // $('#hidPk').val();
            strData.iddir = $('#hidPk').val(); ;
            strData.codigo = $('#MtxtCodigo').val();
            strData.nombre = $('#MtxtNombre').val();
            strData.latitud = $('#txtLatitud').val();
            strData.longitud = $('#txtLongitud').val();
            strData.accion = $('#MhAccion').val();
            strData.dir_tipo = $('#cboDirTipo').val(); //@001 I/F

            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#MtxtCodigo').val('');
            $('#MtxtNombre').val('');
            $('#txtDireccion').val('');
            $('#txtGiro').val('');
            $('#txtCanal').val('');
            $('#txtLatitud').val('');
            $('#txtLongitud').val('');
            $('#txtLongitud').trigger('keyup');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *</p>
            <input type="text" id="MtxtCodigo" runat="server" class="requerid cz-form-content-input-text"
                maxlength="10" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                *</p>
            <input type="text" id="MtxtNombre" runat="server" class="requerid cz-form-content-input-text"
                maxlength="100" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_LATITUD)%>
                </p>
            <input type="text" id="txtLatitud" runat="server" onkeypress="fc_PermiteDecimalNegativo(this);"
                class=" cz-form-content-input-text" maxlength="25" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_LONGITUD)%>
                </p>
            <input type="text" id="txtLongitud" runat="server" onkeypress="fc_PermiteDecimalNegativo(this);"
                class=" cz-form-content-input-text" maxlength="25" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <!--@001 I-->
        <!--<div class="cz-form-content" >-->
        <div class="cz-form-content" <%= visibleDireccionDespacho %> >
            <p>
                Tipo
                *</p>
            <asp:DropDownList ID="cboDirTipo" runat="server" CssClass=" requerid cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
        <!--@001 F-->
        <div class="cz-form-content">
            <p>
                
                </p>
             <input type="button" id="cz-form-box-verMas" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="map" value="Ver Mapa" />
        </div>
        <div id="map" style="display:none; width: 100%; height: 200px">
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidSecuencia" Value="" runat="server" />
    </form>
</body>
</html>
