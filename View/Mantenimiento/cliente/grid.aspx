﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.cliente.Mantenimiento_cliente_grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 16/04/2015 Ajustes para validar visualización de Campos Adicionales
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <script>
        var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
        $(document).ready(function () {
            if (flag == "T") {
                $('#Tieliminar').append('Eliminar');
            } else {
                $('#Tieliminar').append('Restaurar');
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("ID") %>" type="checkbox">
                            </th>
                            <th style="width:100px;"  nomb="CODIGO">
                              <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE)%>
                            </th>
                            <th  nomb="CANAL">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPOCLIENTE)%>
                            </th>
                            <th  nomb="DIRECCION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%>
                            </th>
                            <th  nomb="GIRO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GIRO)%>
                            </th>

                            <th  nomb="GrupoEconomico">
                                Grupo Económico
                            </th>

                            <%--@001 I--%>
                            <th nomb="CAMPO_1" <%= visibleCampoAdicional1 %> >
                                <%=DescripcionCampoAdicional1%>
                            </th>
                            <th nomb="CAMPO_2" <%= visibleCampoAdicional2 %> >
                                <%=DescripcionCampoAdicional2%>
                            </th>
                            <th nomb="CAMPO_3" <%= visibleCampoAdicional3 %> >
                                <%=DescripcionCampoAdicional3%>
                            </th>
                            <th nomb="CAMPO_4" <%= visibleCampoAdicional4 %> >
                                <%=DescripcionCampoAdicional4%>
                            </th>
                            <th nomb="CAMPO_5" <%= visibleCampoAdicional5 %> >
                                <%=DescripcionCampoAdicional5%>
                            </th>
                            <th nomb="CAMPO_LIMTE" <%= mostrarCredito %> >
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_LIMITE_CREDITO)%>
                            </th>
                            <th nomb="CAMPO_CREDITOUTIL" <%= mostrarCredito%> >
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CREDITO_UTILIZADO)%>
                            </th>
                            <th style="width:40px;">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%>
                            </th>
                            
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                            {%>
                            
                            <th style="width:40px;">
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                            
                            <%} %>
                            
                             <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                            {%>
                            <th style="width:40px;" id="Tieliminar">
                            </th>
                              <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("CANAL")%>
                    </td>
                    <td>
                        <%# Eval("DIRECCION")%>
                    </td>
                    <td>
                        <%# Eval("GIRO")%>
                    </td>

                    <td>
                        <%# Eval("grupoeconomico")%>
                    </td>

                    <%--@001 I--%>
                    <td <%= visibleCampoAdicional1 %> >
                        <%# Eval("CampoAdicional1")%>
                    </td>
                    <td <%= visibleCampoAdicional2 %> >
                        <%# Eval("CampoAdicional2")%>
                    </td>
                    <td <%= visibleCampoAdicional3 %> >
                        <%# Eval("CampoAdicional3")%>
                    </td>
                    <td <%= visibleCampoAdicional4 %> >
                        <%# Eval("CampoAdicional4")%>
                    </td>
                    <td <%= visibleCampoAdicional5 %> >
                        <%# Eval("CampoAdicional5")%>
                    </td>
                    <td <%= mostrarCredito %> >
                        <%# Eval("limiteCredito")%>
                    </td>
                    <td <%= mostrarCredito %> >
                        <%# Eval("creditoUtilizado")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                             <a  style="cursor: pointer;" 
                             href="direccion.aspx?idcliente=<%# Eval("ID") %>&cliente=<%# Eval("NOMBRE") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Direcciones" /></a>
                    </td>

                     <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                     {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar Usuario" /></a>
                    </td>
                    <%} %>
                    
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     {%>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar Usuario" /></a>
                    </td>
                    <%} %>

                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                   <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("CANAL")%>
                    </td>
                    <td>
                        <%# Eval("DIRECCION")%>
                    </td>
                    <td>
                        <%# Eval("GIRO")%>
                    </td>

                    <td>
                        <%# Eval("grupoeconomico")%>
                    </td>

                    <%--@001 I--%>
                    <td <%= visibleCampoAdicional1 %> >
                        <%# Eval("CampoAdicional1")%>
                    </td>
                    <td <%= visibleCampoAdicional2 %> >
                        <%# Eval("CampoAdicional2")%>
                    </td>
                    <td <%= visibleCampoAdicional3 %> >
                        <%# Eval("CampoAdicional3")%>
                    </td>
                    <td <%= visibleCampoAdicional4 %> >
                        <%# Eval("CampoAdicional4")%>
                    </td>
                    <td <%= visibleCampoAdicional5 %> >
                        <%# Eval("CampoAdicional5")%>
                    </td>
                    <td <%= mostrarCredito %> >
                        <%# Eval("limiteCredito")%>
                    </td>
                    <td <%= mostrarCredito %> >
                        <%# Eval("creditoUtilizado")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                             <a  style="cursor: pointer;" 
                             href="direccion.aspx?idcliente=<%# Eval("ID") %>&cliente=<%# Eval("NOMBRE") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Direcciones" /></a>
                    </td>
                    
                     <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                     {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                   <%} %>
                     
                     <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     {%>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                   <%} %>

                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
       <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
