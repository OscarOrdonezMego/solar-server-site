﻿using System;
using System.Web.Services;
using Controller.functions;
using Model.bean;
using Controller;
using Model;

/// <summary>
/// @001 GMC 12/04/2015 Ajustes para actualizar Campos Adicionales Cliente
/// </summary>
namespace Pedidos_Site.Mantenimiento.cliente
{
    public partial class Mantenimiento_cliente_cliente : PageController
    {
        public static String lsCodMenu = "MCL";

        private static String valorCampoAdicional1 = "F";
        private static String valorCampoAdicional2 = "F";
        private static String valorCampoAdicional3 = "F";
        private static String valorCampoAdicional4 = "F";
        private static String valorCampoAdicional5 = "F";

        protected override void initialize()
        {

            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
        }

        [WebMethod]
        public static void borrar(String codigos, String flag)
        {

            try
            {

                ClienteController.borrar(codigos, flag);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String id, String codigo, String nombre, String direccion, String canal, String giro, String xSec, String latitud, String longitud
            , String campoAdicional1, String campoAdicional2, String campoAdicional3, String campoAdicional4, String campoAdicional5 //@001 I/F
            , String limiteCredito, String creditoUtilizado, String grupoeconomico)
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }

                else if (Utils.tieneCaracteresReservados(direccion))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIRECCION, direccion) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, direccion));
                }

                else if (latitud.Length > 0 && longitud.Length == 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD, longitud));
                }
                else if (latitud.Length == 0 && longitud.Length > 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD, latitud));
                }

                else
                {

                    ClienteBean bean = new ClienteBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.direccion = direccion;
                    bean.flag = "T";
                    bean.latitud = latitud;
                    bean.secuencia = xSec;
                    bean.giro = giro;
                    bean.canal = canal;
                    bean.longitud = longitud;
                    bean.CampoAdicional1 = campoAdicional1;
                    bean.CampoAdicional2 = campoAdicional2;
                    bean.CampoAdicional3 = campoAdicional3;
                    bean.CampoAdicional4 = campoAdicional4;
                    bean.CampoAdicional5 = campoAdicional5;
                    bean.limiteCredito = limiteCredito;
                    bean.creditoUtilizado = creditoUtilizado;
                    bean.grupoeconomico = grupoeconomico;
                    ClienteController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String id, String codigo, String nombre, String direccion, String canal, String giro, String xSec, String latitud, String longitud
            , String campoAdicional1, String campoAdicional2, String campoAdicional3, String campoAdicional4, String campoAdicional5, String limiteCredito, String creditoUtilizado, String grupoeconomico
            )
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }

                else if (Utils.tieneCaracteresReservados(direccion))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DIRECCION, direccion) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, direccion));
                }

                else if (latitud.Length > 0 && longitud.Length == 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD, longitud));
                }
                else if (latitud.Length == 0 && longitud.Length > 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD, latitud));
                }


                else
                {
                    ClienteBean bean = new ClienteBean();
                    bean.id = id;
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.direccion = direccion;
                    bean.flag = "T";
                    bean.latitud = latitud;
                    bean.longitud = longitud;
                    bean.secuencia = xSec;
                    bean.giro = giro;
                    bean.canal = canal;
                    bean.grupoeconomico = grupoeconomico;
                    //@001 I
                    ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_1, out valorCampoAdicional1);
                    ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_2, out valorCampoAdicional2);
                    ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_3, out valorCampoAdicional3);
                    ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_4, out valorCampoAdicional4);
                    ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_5, out valorCampoAdicional5);

                    bean.CampoAdicional1 = (valorCampoAdicional1 == Model.Enumerados.FlagHabilitado.T.ToString() ? campoAdicional1 : "");
                    bean.CampoAdicional2 = (valorCampoAdicional2 == Model.Enumerados.FlagHabilitado.T.ToString() ? campoAdicional2 : "");
                    bean.CampoAdicional3 = (valorCampoAdicional3 == Model.Enumerados.FlagHabilitado.T.ToString() ? campoAdicional3 : "");
                    bean.CampoAdicional4 = (valorCampoAdicional4 == Model.Enumerados.FlagHabilitado.T.ToString() ? campoAdicional4 : "");
                    bean.CampoAdicional5 = (valorCampoAdicional5 == Model.Enumerados.FlagHabilitado.T.ToString() ? campoAdicional5 : "");
                    bean.limiteCredito = limiteCredito;
                    bean.creditoUtilizado = creditoUtilizado;
                    //@001 F

                    ClienteController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        //@001 I
        private static void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    if (configBean.Valor.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                    {
                        valorCampo = configBean.Valor;
                    }
                }
            }
        }
        //@001 F
    }
}