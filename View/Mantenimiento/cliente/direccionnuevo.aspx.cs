﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using Model.bean;
using Controller;

/// <summary>
/// @001 GMC 14/04/2015 Ajustes para actualizar Campos DIR_TIPO (P: Pedido, D: Despacho)
/// @002 GMC 18/04/2015 Ajustes para visualización de Campo DIR_TIPO
/// </summary>
namespace Pedidos_Site.Mantenimiento.cliente
{
    public partial class direccionnuevo : Page
    {
        public bool GenCodigo;
        public float latitud;
        public float longitud;
        String secuencia = "1";
        public String xIDcliente = "";
        //@002 I
        private String valorDireccionDespacho = "F";
        public String visibleDireccionDespacho = "";
        //@002 F

        protected void Page_Load(object sender, EventArgs e)
        {
            hidSecuencia.Value = secuencia;

            //@001 I
            cboDirTipo.DataSource = ReporteController.getTipoDireccion();
            cboDirTipo.DataValueField = "COD";
            cboDirTipo.DataTextField = "DEN";
            cboDirTipo.DataBind();
            //@001 F

            if (!IsPostBack)
            {
                xIDcliente = Request.QueryString["idcliente"].ToString();
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVADIRECCION);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                if (ManagerConfiguration.laltitudlongitud != "0")
                {
                    String[] latlo = ManagerConfiguration.laltitudlongitud.Split(',');

                    latitud = float.Parse(latlo[0]);
                    longitud = float.Parse(latlo[1]);
                }
                else
                {
                    latitud = float.Parse("-12.0462037802177");
                    longitud = float.Parse("-77.0656585693359");
                }


                if (dataJSON != null)
                {

                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARDIRECCION);
                    ClienteDireccionBean usuario = ClienteController.infoDireccion(Int32.Parse(xIDcliente), Int32.Parse(dataJSON["codigo"]));

                    hidPk.Value = usuario.dir_pk;

                    MtxtNombre.Value = usuario.nombre;
                    MtxtCodigo.Value = usuario.codigo;
                    txtLatitud.Value = usuario.latitud;
                    txtLongitud.Value = usuario.longitud;

                    if (txtLatitud.Value.Trim().ToString() != "")
                    {
                        latitud = float.Parse(txtLatitud.Value);
                    }
                    if (txtLongitud.Value.Trim().ToString() != "")
                    {
                        longitud = float.Parse(txtLongitud.Value);
                    }
                    MtxtCodigo.Disabled = true;

                    //@001 I
                    if (this.cboDirTipo.Items.FindByValue(usuario.dir_tipo) != null)
                        this.cboDirTipo.SelectedValue = usuario.dir_tipo;
                    //@001 F

                }
                else
                {
                    hidPk.Value = "";
                    MtxtCodigo.Disabled = GenCodigo;
                }

                //@002 I
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_DESPACHO, out this.valorDireccionDespacho);
                this.visibleDireccionDespacho = (this.valorDireccionDespacho == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

                if (this.valorDireccionDespacho == Model.Enumerados.FlagHabilitado.F.ToString())
                {
                    if (this.cboDirTipo.Items.FindByValue("P") != null)
                        this.cboDirTipo.SelectedValue = "P";
                }
                //@002 F

                if (GenCodigo)
                    MtxtNombre.Focus();
                else
                    MtxtCodigo.Focus();

            }
        }

        //@002 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        //@002 F
    }
}