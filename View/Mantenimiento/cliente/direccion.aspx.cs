﻿using System;
using System.Web.Services;
using Controller.functions;
using Model.bean;
using Controller;

/// <summary>
/// @001 GMC 14/04/2015 Ajustes para actualizar Campos DIR_TIPO (P: Pedido, D: Despacho) y FECCREACION en Mant Direcciones
/// </summary>
namespace Pedidos_Site.Mantenimiento.cliente
{
    public partial class direccion : PageController
    {
        public String cIDcliente = "";
        public string cCliente = "";
        protected override void initialize()
        {
            cIDcliente = Request.QueryString["idcliente"].ToString();
            cCliente = Request.QueryString["cliente"].ToString();
            hfIDcliente.Value = cIDcliente;
        }

        [WebMethod]
        public static void borrar(String codigos, String idcliente, String flag)
        {
            try
            {
                ClienteController.borrarDirecciones(idcliente, codigos, flag);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public static String crear(String idcliente, String iddir, String codigo, String nombre, String latitud, String longitud
            , String dir_tipo //@001 I/F
            )
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }
                else if (latitud.Length > 0 && longitud.Length == 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD, longitud));
                }
                else if (latitud.Length == 0 && longitud.Length > 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD, latitud));
                }
                else
                {
                    ClienteDireccionBean bean = new ClienteDireccionBean();
                    bean.cli_pk = idcliente;
                    bean.dir_pk = "";
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.latitud = latitud;
                    bean.longitud = longitud;
                    bean.dir_tipo = dir_tipo; //@001 I/F
                    ClienteController.creardireccion(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String idcliente, String iddir, String codigo, String nombre, String latitud, String longitud
            , String dir_tipo //@001 I/F
            )
        {
            try
            {
                if (Utils.tieneCaracteresReservados(codigo))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_CODIGO, codigo) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, codigo));
                }
                else if (Utils.tieneCaracteresReservados(nombre))
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_NOMBRE, nombre) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_CARACTER_RESERVADO, nombre));
                }
                else if (latitud.Length > 0 && longitud.Length == 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LONGITUD, longitud));
                }
                else if (latitud.Length == 0 && longitud.Length > 0)
                {
                    throw new Exception(IdiomaCultura.getMensaje(IdiomaCultura.WEB_DATOS_INVALIDOS, latitud) + ' ' + IdiomaCultura.getMensaje(IdiomaCultura.WEB_LATITUD, latitud));
                }
                else
                {

                    ClienteDireccionBean bean = new ClienteDireccionBean();
                    bean.cli_pk = idcliente;
                    bean.dir_pk = iddir;
                    bean.codigo = codigo;
                    bean.nombre = nombre;
                    bean.latitud = latitud;
                    bean.longitud = longitud;
                    bean.dir_tipo = dir_tipo; //@001 I/F

                    ClienteController.creardireccion(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}