﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using Controller;
using Model.bean;
using System.Web.Script.Serialization;
using System.Text;
using Model;

namespace Pedidos_Site.Mantenimiento.config
{
    public partial class Mantenimiento_config_config : PageController
    {
        public static String lsCodMenu = "MCF";

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            else
            {
                try
                {
                    if (!IsPostBack)
                    {
                        List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
                        loLstConfiguracionBean = ConfiguracionController.subObtenerDatosConfiguracion();
                        this.subEscribirConfiguracionModulo(loLstConfiguracionBean);
                        this.subEscribirConfiguracionFunciones(loLstConfiguracionBean);
                        this.subEscribirConfiguracionValores(loLstConfiguracionBean);
                        List<ManualBean> loLstManuales = MenuController.subObtenerDatosManuales(Convert.ToInt32(Session["lgn_id"]));

                    }
                }
                catch (Exception ex)
                {
                    String lsRutaDefault = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsRutaDefault, true);
                }
            }
        }

        private void subEscribirConfiguracionModulo(List<ConfiguracionBean> poLstConfiguracionBean)
        {
            StringBuilder loModuloConfiguracion = new StringBuilder();
            List<ConfiguracionBean> loLstConfiguracionBeanModulo = new List<ConfiguracionBean>();
            loLstConfiguracionBeanModulo = this.subObtenerListaConfiguracionPorCodigo(poLstConfiguracionBean, Enumerados.CodigoConfiguracion.MOD);
            //String lsDisplayNone = " style=\"display: none;\" ";
            //String lsStyle;
            if (loLstConfiguracionBeanModulo != null && loLstConfiguracionBeanModulo.Count > 0)
            {
                loModuloConfiguracion.Append("<div class=\"cz-form-content cz-content-extended\">");
                loModuloConfiguracion.Append("<div class=\"cz-form-subcontent\">");
                loModuloConfiguracion.Append("<div class=\"cz-form-subcontent-title\">");
                loModuloConfiguracion.Append("<p>Módulo</p>");
                loModuloConfiguracion.Append("</div>");
                loModuloConfiguracion.Append("<div class=\"cz-form-subcontent-content\">");
                loModuloConfiguracion.Append("<div class=\"cz-form-content-input-check\">");
                for (int i = 0; i < loLstConfiguracionBeanModulo.Count; i++)
                {
                    if (loLstConfiguracionBeanModulo[i].FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()) && !loLstConfiguracionBeanModulo[i].Codigo.Equals(Constantes.MOD_GENERAL))
                    {

                        String lsInput = String.Empty;
                        String lsIdElementoHtml = loLstConfiguracionBeanModulo[i].Codigo.Trim() + "_" + loLstConfiguracionBeanModulo[i].Tipo.Trim();
                        lsInput = (loLstConfiguracionBeanModulo[i].Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                   ? "<input type=\"checkbox\" class=\"cz-form-content-input-check\" id=\"" + lsIdElementoHtml + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\" onclick=\"javascript:modifiacarValorCheckedModulo('" + lsIdElementoHtml + "');\" checked=\"checked\" />"
                                   : "<input type=\"checkbox\" class=\"cz-form-content-input-check\" id=\"" + lsIdElementoHtml + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\" onclick=\"javascript:modifiacarValorCheckedModulo('" + lsIdElementoHtml + "');\" />";
                        loModuloConfiguracion.Append(lsInput);
                        loModuloConfiguracion.Append("<a class=\"cz-form-content-input-check-title\" >" + loLstConfiguracionBeanModulo[i].Descripcion.Trim() + "<a/>");
                        loModuloConfiguracion.Append("<br />");
                        loModuloConfiguracion.Append("<br />");
                    }
                }
                loModuloConfiguracion.Append("</div>");
                loModuloConfiguracion.Append("</div>");
                loModuloConfiguracion.Append("</div>");
                loModuloConfiguracion.Append("</div>");
            }

            litModulo.Text = loModuloConfiguracion.ToString();
        }

        private void subEscribirConfiguracionFunciones(List<ConfiguracionBean> poLstConfiguracionBean)
        {
            StringBuilder loFuncionesConfiguracion = new StringBuilder();
            List<ConfiguracionBean> loLstConfiguracionBeanFunciones = new List<ConfiguracionBean>();
            loLstConfiguracionBeanFunciones = this.subObtenerListaConfiguracionPorCodigo(poLstConfiguracionBean, Enumerados.CodigoConfiguracion.FUN);
            Session[SessionManager.LISTA_SESSION] = loLstConfiguracionBeanFunciones;
            List<ConfiguracionBean> loLstConfiguracionBeanModulo = new List<ConfiguracionBean>();
            loLstConfiguracionBeanModulo = this.subObtenerListaConfiguracionPorCodigo(poLstConfiguracionBean, Enumerados.CodigoConfiguracion.MOD);
            for (int i = 0; i < loLstConfiguracionBeanModulo.Count; i++)
            {
                if (loLstConfiguracionBeanModulo[i].FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()))
                {
                    List<ConfiguracionBean> loLstConfiguracionBeanFuncionesPorModulo = loLstConfiguracionBeanFunciones.FindAll(obj => obj.TipoPadre.Equals(loLstConfiguracionBeanModulo[i].Codigo));
                    String lsClaseIndentificador = loLstConfiguracionBeanModulo[i].Codigo.Trim() + "_" + loLstConfiguracionBeanModulo[i].Tipo.Trim();
                    if (loLstConfiguracionBeanFuncionesPorModulo != null && loLstConfiguracionBeanFuncionesPorModulo.Count > 1)
                    {
                        loFuncionesConfiguracion.Append("<div class=\"cz-form-content cz-content-extended\" >");
                        loFuncionesConfiguracion.Append("<div class=\"cz-form-subcontent\">");
                        loFuncionesConfiguracion.Append("<div class=\"cz-form-subcontent-title\">");
                        loFuncionesConfiguracion.Append("<p>" + "Función " + loLstConfiguracionBeanModulo[i].Descripcion.Trim() + "</p>");
                        loFuncionesConfiguracion.Append("</div>");
                        loFuncionesConfiguracion.Append("<div class=\"cz-form-subcontent-content\">");
                        loFuncionesConfiguracion.Append("<div class=\"cz-form-content-input-check\">");

                        for (int j = 0; j < loLstConfiguracionBeanFuncionesPorModulo.Count; j++)
                        {
                            if (loLstConfiguracionBeanFuncionesPorModulo[j].FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()) && !loLstConfiguracionBeanFuncionesPorModulo[j].Codigo.Equals(Constantes.FUN_GENERAL))
                            {

                                String lsInput = String.Empty;
                                String lsIdElementoHtml = loLstConfiguracionBeanFuncionesPorModulo[j].Codigo.Trim() + "_" + loLstConfiguracionBeanFuncionesPorModulo[j].Tipo.Trim();

                                lsInput = (loLstConfiguracionBeanFuncionesPorModulo[j].Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                                           ? "<input type=\"checkbox\" class=\"cz-form-content-input-check " + lsClaseIndentificador + "\" id=\"" + lsIdElementoHtml + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\"  onclick=\"javascript:modificarValoresFuncion('" + lsIdElementoHtml + "');\" checked=\"checked\"  />"
                                           : "<input type=\"checkbox\" class=\"cz-form-content-input-check " + lsClaseIndentificador + "\" id=\"" + lsIdElementoHtml + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\"  onclick=\"javascript:modificarValoresFuncion('" + lsIdElementoHtml + "');\" />";
                                loFuncionesConfiguracion.Append(lsInput);
                                loFuncionesConfiguracion.Append("<a class=\"cz-form-content-input-check-title\">" + loLstConfiguracionBeanFuncionesPorModulo[j].Descripcion.Trim() + "<a/>");
                                loFuncionesConfiguracion.Append("</br>");
                                loFuncionesConfiguracion.Append("</br>");
                            }
                        }
                        loFuncionesConfiguracion.Append("</div>");
                        loFuncionesConfiguracion.Append("</div>");
                        loFuncionesConfiguracion.Append("</div>");
                        loFuncionesConfiguracion.Append("</div>");
                    }
                }
            }
            litFunciones.Text = loFuncionesConfiguracion.ToString();

        }

        private void subEscribirConfiguracionValores(List<ConfiguracionBean> poLstConfiguracionBean)
        {
            StringBuilder loConfiguraciones = new StringBuilder();
            List<ConfiguracionBean> loLstConfiguracionBeanFunciones = new List<ConfiguracionBean>();
            loLstConfiguracionBeanFunciones = this.subObtenerListaConfiguracionPorCodigo(poLstConfiguracionBean, Enumerados.CodigoConfiguracion.FUN);
            List<ConfiguracionBean> loLstConfiguracionBeanValoresConfiguracion = new List<ConfiguracionBean>();
            loLstConfiguracionBeanValoresConfiguracion = this.subObtenerListaConfiguracionPorCodigo(poLstConfiguracionBean, Enumerados.CodigoConfiguracion.COF);
            Boolean lbEsGeneral = true;
            int i = 0;
            while (i < loLstConfiguracionBeanFunciones.Count)
            {

                List<ConfiguracionBean> loLstConfiguracionBeanConfiguracionesPorFuncion = null;
                if (lbEsGeneral)
                {
                    loLstConfiguracionBeanConfiguracionesPorFuncion = loLstConfiguracionBeanValoresConfiguracion.FindAll(obj => obj.TipoPadre.Equals(Constantes.FUN_GENERAL));
                    if (loLstConfiguracionBeanConfiguracionesPorFuncion != null && loLstConfiguracionBeanConfiguracionesPorFuncion.Count > 1)
                    {
                        loConfiguraciones.Append("<div class=\"cz-form-content cz-content-extended\" >");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent\">");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent-title\">");
                        loConfiguraciones.Append("<p>" + "Configuración " + "General" + "</p>");
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent-content\">");

                        for (int j = 0; j < loLstConfiguracionBeanConfiguracionesPorFuncion.Count; j++)
                        {
                            if (loLstConfiguracionBeanConfiguracionesPorFuncion[j].FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()))
                            {

                                String lsInput = subEscribirControlConfiguracion(loLstConfiguracionBeanConfiguracionesPorFuncion[j], null, null, String.Empty);
                                loConfiguraciones.Append(lsInput);
                            }
                        }
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("</div>");
                    }
                    lbEsGeneral = !lbEsGeneral;
                }
                else
                {
                    loLstConfiguracionBeanConfiguracionesPorFuncion = loLstConfiguracionBeanValoresConfiguracion.FindAll(obj => obj.TipoPadre.Equals(loLstConfiguracionBeanFunciones[i].Codigo));

                    if (loLstConfiguracionBeanConfiguracionesPorFuncion != null && loLstConfiguracionBeanConfiguracionesPorFuncion.Count > 1)
                    {
                        loConfiguraciones.Append("<div class=\"cz-form-content cz-content-extended\" >");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent\">");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent-title\">");
                        loConfiguraciones.Append("<p>" + "Configuración " + loLstConfiguracionBeanFunciones[i].Descripcion.Trim() + "</p>");
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("<div class=\"cz-form-subcontent-content\">");
                        for (int j = 0; j < loLstConfiguracionBeanConfiguracionesPorFuncion.Count; j++)
                        {
                            if (loLstConfiguracionBeanConfiguracionesPorFuncion[j].FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()))
                            {
                                ConfiguracionBean loConfiguracionBeanSegundo = null;
                                String lsClaseIndentificador = String.Empty;
                                if (loLstConfiguracionBeanConfiguracionesPorFuncion[j].Codigo.Equals(Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_GENERAL))
                                {
                                    lsClaseIndentificador = lsClaseIndentificador + "_" + Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_GENERAL;
                                    loConfiguracionBeanSegundo = loLstConfiguracionBeanValoresConfiguracion.Find(obj => obj.Codigo.Trim().Equals(Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PRODUCTO));
                                }
                                else if (loLstConfiguracionBeanConfiguracionesPorFuncion[j].Codigo.Equals(Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PORCENTAJE))
                                {
                                    lsClaseIndentificador = lsClaseIndentificador + "_" + Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PORCENTAJE;
                                    loConfiguracionBeanSegundo = loLstConfiguracionBeanValoresConfiguracion.Find(obj => obj.Codigo.Trim().Equals(Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_MONTO));

                                }

                                String lsInput = subEscribirControlConfiguracion(loLstConfiguracionBeanConfiguracionesPorFuncion[j], loLstConfiguracionBeanFunciones[i], loConfiguracionBeanSegundo, lsClaseIndentificador);
                                loConfiguraciones.Append(lsInput);
                            }
                        }
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("</div>");
                        loConfiguraciones.Append("</div>");
                    }
                    i++;
                }
            }
            litConfiguraciones.Text = loConfiguraciones.ToString();
        }

        private String subEscribirControlConfiguracion(ConfiguracionBean poConfiguracionBean, ConfiguracionBean poConfiguracionFuncion, ConfiguracionBean loConfiguracionBeanSegundo, String psClaseIdentificador)
        {
            StringBuilder loConfiguracionValoresConfiguracion = new StringBuilder();
            if (poConfiguracionBean.FlagHabilitado.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {

                ConfiguracionBean loConfiguracionBean = poConfiguracionFuncion;//poLstConfiguracionBean.Find(obj => obj.Codigo.Trim().Equals(loLstConfiguracionBeanValoresConfiguracion[i].TipoPadre.Trim()) && obj.Tipo.Trim().Equals(Enumerados.CodigoConfiguracion.FUN.ToString()));
                String lsCodigoModulo = String.Empty;
                lsCodigoModulo = (loConfiguracionBean == null) ? Constantes.FUN_GENERAL.ToString() : loConfiguracionBean.TipoPadre.Trim();
                String lsClaseIdentificadorFuncion = poConfiguracionBean.TipoPadre.Trim() + "_" + Enumerados.CodigoConfiguracion.FUN.ToString();
                String lsClaseIdentificadorModulo = lsCodigoModulo + "_" + Enumerados.CodigoConfiguracion.MOD.ToString() + "_" + Enumerados.CodigoConfiguracion.FUN.ToString();
                String lsClaseIndentificador = lsClaseIdentificadorFuncion + " " + lsClaseIdentificadorModulo;

                String lsCodigoValorConfiguracion = poConfiguracionBean.Codigo.Trim();
                String lsControlHtml = String.Empty;

                switch (lsCodigoValorConfiguracion)
                {
                    case Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_GENERAL:
                        psClaseIdentificador = lsClaseIndentificador + "_" + Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_GENERAL;
                        lsControlHtml = subEscribirControlConfiguracionRadioButton("Descuento Por:",
                                                                                    loConfiguracionBeanSegundo.Descripcion.Trim(),
                                                                                    loConfiguracionBeanSegundo.Codigo.Trim(),
                                                                                    loConfiguracionBeanSegundo.Tipo.Trim(),
                                                                                    loConfiguracionBeanSegundo.Valor,
                                                                                    poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    psClaseIdentificador, false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PORCENTAJE:
                        psClaseIdentificador = lsClaseIndentificador + "_" + Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_PORCENTAJE;
                        lsControlHtml = subEscribirControlConfiguracionRadioButton("Descuento Por:",
                                                                                   loConfiguracionBeanSegundo.Descripcion.Trim(),
                                                                                   loConfiguracionBeanSegundo.Codigo.Trim(),
                                                                                   loConfiguracionBeanSegundo.Tipo.Trim(),
                                                                                   loConfiguracionBeanSegundo.Valor,
                                                                                   poConfiguracionBean.Descripcion.Trim(),
                                                                                   lsCodigoValorConfiguracion,
                                                                                   poConfiguracionBean.Tipo.Trim(),
                                                                                   poConfiguracionBean.Valor,
                                                                                   psClaseIdentificador, true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_RANGO_DESCUENTO_MINIMO:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador, true, 2);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_RANGO_DESCUENTO_MAXIMO:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador, false, 2);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_STOCK:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_VALIDAR_STOCK:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_DESCONTAR_STOCK:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_CONSULTA_STOCk_LINEA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_STOCK_RESTRICTIVO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_VALIDAR_PRECIO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_PRECIO_EDITABLE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_PRECIO_TIPO_CLIENTE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_PRECIO_CONDICION_VENTA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_MANUAL:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CONDICION_VENTA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_COBRANZA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_DEVOLUCION:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_CANJE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_GPS:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_EFICIENCIA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_SEGUIMIENTO_RUTA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_ES_EMBEBIDO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_NUMEROS_DECIMALES_VISTA:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true, 1);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_PAGINACION:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false, 2);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;


                    case Constantes.CodigoValoresConfiguracion.CFG_TECLADO_ALFANUMERICO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_1:
                        lsControlHtml = subEscribirControlConfiguracionTextBoxCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_2:
                        lsControlHtml = subEscribirControlConfiguracionTextBoxCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_3:
                        lsControlHtml = subEscribirControlConfiguracionTextBoxCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_4:
                        lsControlHtml = subEscribirControlConfiguracionTextBoxCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_CAMPO_5:
                        lsControlHtml = subEscribirControlConfiguracionTextBoxCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_PEDIDO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_DESPACHO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_NO_VISITA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_COTIZACION:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_FRACCIONAMIENTO_SIMPLE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_FRACCIONAMIENTO_MULTIPLE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConLimites(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true, 0, 5);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;


                    case Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CABECERA_PEDIDO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_FIN_PEDIDO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_CONSULTA_PRODUCTO_WS:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;



                    case Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_DESCUENTO_VOLUMEN:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    false);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_CONSULTA_SEGUIMIENTO_PEDIDO_WS:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_VERIFICACION_PEDIDO_WS:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_CLIENTE_FUERA_RUTA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_DEUDA_VENCIDA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_LIMITE_CREDITO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MAX_ITEMS_PEDIDO:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador, true, 3);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MONTO_MAX_PEDIDO:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true, 5);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MONTO_MIN_PEDIDO:
                        lsControlHtml = subEscribirControlnConfiguracionTextBoxConTamano(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true, 5);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_BUSQUEDA_NUM_CLIENTE:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;

                    case Constantes.CodigoValoresConfiguracion.CFG_BUSQUEDA_NUM_ARTICULO:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_MODO_FUERA_DE_COBERTURA:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                    case Constantes.CodigoValoresConfiguracion.CFG_REPORTE_TRACKING:
                        lsControlHtml = subEscribirControlConfiguracionCheck(poConfiguracionBean.Descripcion.Trim(),
                                                                                    lsCodigoValorConfiguracion,
                                                                                    poConfiguracionBean.Tipo.Trim(),
                                                                                    poConfiguracionBean.Valor,
                                                                                    lsClaseIndentificador,
                                                                                    true);
                        loConfiguracionValoresConfiguracion.Append(lsControlHtml);
                        break;
                }
            }
            return loConfiguracionValoresConfiguracion.ToString();
        }

        private List<ConfiguracionBean> subObtenerListaConfiguracionPorCodigo(List<ConfiguracionBean> poLstConfiguracionBean, Enumerados.CodigoConfiguracion peCodigoConfiguracion)
        {
            List<ConfiguracionBean> loLstConfiguracionBean = new List<ConfiguracionBean>();
            loLstConfiguracionBean = poLstConfiguracionBean.FindAll(obj => obj.Tipo.Equals(peCodigoConfiguracion.ToString()));
            return loLstConfiguracionBean;
        }

        private String subEscribirControlConfiguracionCheck(String psDescripcionConfiguracion, String psCodigoValorConfiguracion, String psTipo, String psValor, String psClaseIdentificador, Boolean pbSatoLinea)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSatoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            loStringControl.Append("<p>" + psDescripcionConfiguracion + "</p>");
            loStringControl.Append("<div class=\"cz-form-content-input-check\">");
            String lsInput = String.Empty;
            String lsIdElementoHtml = psCodigoValorConfiguracion + "_" + psTipo;
            lsInput = (psValor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                        ? "<input type=\"checkbox\" id=\"" + lsIdElementoHtml + "\" class=\"cz-form-content-input-check " + psClaseIdentificador + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\" onclick=\"javascript:modifiacarValorChecked('" + lsIdElementoHtml + "');\" Checked />"
                        : "<input type=\"checkbox\" id=\"" + lsIdElementoHtml + "\" class=\"cz-form-content-input-check " + psClaseIdentificador + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\" onclick=\"javascript:modifiacarValorChecked('" + lsIdElementoHtml + "');\" />";
            loStringControl.Append(lsInput);
            loStringControl.Append("</div>");
            return loStringControl.ToString();
        }

        private String subEscribirControlConfiguracionRadioButton(String psTitulo, String psPrimerDescripcionConfiguracion, String psCodigoPrimerValorConfiguracion, String psPrimerTipo, String psPrimerValor, String psSegundoDescripcionConfiguracion, String psCodigoSegundoValorConfiguracion, String psSegundoTipo, String psSegundoValor, String psClaseIdentificador, Boolean pbSaltoLinea)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSaltoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            loStringControl.Append("<p>" + psTitulo + "</p>");
            loStringControl.Append("</br>");
            loStringControl.Append("<div class=\"cz-form-content-input-radio\">");

            String lsInput = String.Empty;
            String lsIdPrimerElementoHtml = psCodigoPrimerValorConfiguracion + "_" + psPrimerTipo;
            String lsIdSegundoElementoHtml = psCodigoSegundoValorConfiguracion + "_" + psSegundoTipo;

            loStringControl.Append("<p>" + psPrimerDescripcionConfiguracion + "</p>");
            lsInput = (psPrimerValor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                        ? "<input type=\"radio\" id=\"" + lsIdPrimerElementoHtml + "\" class=\"cz-form-content-input-radio " + psClaseIdentificador + "\" name=\"" + psClaseIdentificador + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\" onclick=\"javascript:modifiacarValorRadio('" + lsIdPrimerElementoHtml + "','" + lsIdSegundoElementoHtml + "');\" Checked />"
                        : "<input type=\"radio\" id=\"" + lsIdPrimerElementoHtml + "\" class=\"cz-form-content-input-radio " + psClaseIdentificador + "\" name=\"" + psClaseIdentificador + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\" onclick=\"javascript:modifiacarValorRadio('" + lsIdPrimerElementoHtml + "','" + lsIdSegundoElementoHtml + "');\" />";
            loStringControl.Append(lsInput);

            loStringControl.Append("</br></br>");

            lsInput = String.Empty;
            loStringControl.Append("<p>" + psSegundoDescripcionConfiguracion + "</p>");
            lsInput = (psSegundoValor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                ? "<input type=\"radio\" id=\"" + lsIdSegundoElementoHtml + "\" class=\"cz-form-content-input-radio " + psClaseIdentificador + "\" name=\"" + psClaseIdentificador + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\" onclick=\"javascript:modifiacarValorRadio('" + lsIdSegundoElementoHtml + "','" + lsIdPrimerElementoHtml + "');\" Checked />"
                : "<input type=\"radio\" id=\"" + lsIdSegundoElementoHtml + "\" class=\"cz-form-content-input-radio " + psClaseIdentificador + "\" name=\"" + psClaseIdentificador + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\" onclick=\"javascript:modifiacarValorRadio('" + lsIdSegundoElementoHtml + "','" + lsIdPrimerElementoHtml + "');\" />";
            loStringControl.Append(lsInput);

            loStringControl.Append("</div>");
            loStringControl.Append("</br></br></br></br>");
            return loStringControl.ToString();
        }

        private String subEscribirControlnConfiguracionTextBoxConTamano(String psDescripcionConfiguracion, String psCodigoValorConfiguracion, String psTipo, String psValor, String psClaseIdentificador, Boolean pbSaltoLinea, int maximo)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSaltoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            loStringControl.Append("<p>" + psDescripcionConfiguracion + "</p>");
            loStringControl.Append("<input type=\"text\" id=\"" + psCodigoValorConfiguracion + "_" + psTipo + "\"  class=\"requerid cz-form-content-input-text " + psClaseIdentificador + "\" onkeypress=\"javascript:fc_PermiteNumeros();\" placeholder=\"Ingrese un número\" maxlength=\"" + maximo + "\" value=\"" + psValor + "\" />");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible\" >");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible-button\"></div>");
            loStringControl.Append("</div>");
            return loStringControl.ToString();
        }

        private String subEscribirControlnConfiguracionTextBox(String psDescripcionConfiguracion, String psCodigoValorConfiguracion, String psTipo, String psValor, String psClaseIdentificador, Boolean pbSaltoLinea)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSaltoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            loStringControl.Append("<p>" + psDescripcionConfiguracion + "</p>");
            loStringControl.Append("<input type=\"text\" id=\"" + psCodigoValorConfiguracion + "_" + psTipo + "\"  class=\"requerid cz-form-content-input-text " + psClaseIdentificador + "\" onkeypress=\"javascript:fc_PermiteNumeros();\" placeholder=\"Ingrese un número\" maxlength=\"2\" value=\"" + psValor + "\" />");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible\" >");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible-button\"></div>");
            loStringControl.Append("</div>");
            return loStringControl.ToString();
        }

        private String subEscribirControlnConfiguracionTextBoxConLimites(String psDescripcionConfiguracion, String psCodigoValorConfiguracion, String psTipo, String psValor, String psClaseIdentificador, Boolean pbSaltoLinea, Int32 piMinimoValor, Int32 piMaximoValor)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSaltoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            loStringControl.Append("<p>" + psDescripcionConfiguracion + "</p>");
            loStringControl.Append("<input type=\"text\" id=\"" + psCodigoValorConfiguracion + "_" + psTipo + "\"  onfocus=\"this.oldvalue = this.value;\" class=\"number-limit requerid cz-form-content-input-text " + psClaseIdentificador + "\" placeholder=\"Número entre " + piMinimoValor + " y " + piMaximoValor + " \" maxlength=\"1\" value=\"" + psValor + "\" />");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible\" >");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible-button\"></div>");
            loStringControl.Append("</div>");
            return loStringControl.ToString();
        }

        private String subEscribirControlConfiguracionTextBoxCheck(String psDescripcionConfiguracion, String psCodigoValorConfiguracion, String psTipo, String psValor, String psClaseIdentificador, Boolean pbSatoLinea)
        {
            StringBuilder loStringControl = new StringBuilder();
            if (pbSatoLinea)
            {
                loStringControl.Append("</br></br>");
            }
            String lsInput = String.Empty;
            String lsIdElementoHtml = "";
            lsIdElementoHtml = psCodigoValorConfiguracion + "_" + "TXT";
            loStringControl.Append("<p>" + "<input type=\"text\" id=\"" + lsIdElementoHtml + "\" class=\"requerid cz-form-content-input-text " + psClaseIdentificador + "\" placeholder=\"Ingrese una descripción\" value=\"" + psDescripcionConfiguracion + "\" />" + "</p>");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible\" >");
            loStringControl.Append("<div class=\"cz-form-content-input-text-visible-button\"></div>");
            loStringControl.Append("</div>");
            loStringControl.Append("<div class=\"cz-form-content-input-check\">");
            lsIdElementoHtml = psCodigoValorConfiguracion + "_" + psTipo;
            lsInput = (psValor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                        ? "<input type=\"checkbox\" id=\"" + lsIdElementoHtml + "\" class=\"cz-form-content-input-check " + psClaseIdentificador + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.T.ToString() + "\" onclick=\"javascript:modifiacarValorChecked('" + lsIdElementoHtml + "');\" Checked />"
                        : "<input type=\"checkbox\" id=\"" + lsIdElementoHtml + "\" class=\"cz-form-content-input-check " + psClaseIdentificador + "\" name=\"" + lsIdElementoHtml + "\" value=\"" + Enumerados.FlagHabilitado.F.ToString() + "\" onclick=\"javascript:modifiacarValorChecked('" + lsIdElementoHtml + "');\" />";
            loStringControl.Append(lsInput);
            loStringControl.Append("</div>");
            return loStringControl.ToString();
        }

        [WebMethod]
        public static String guardarConfig(String json)
        {
            try
            {
                JavaScriptSerializer loJssSerializador = new JavaScriptSerializer();
                IDictionary<String, String> loLstDiccionarioConfiguracion = new Dictionary<String, String>();
                loLstDiccionarioConfiguracion = loJssSerializador.Deserialize<IDictionary<String, String>>(json);

                foreach (KeyValuePair<String, String> lsConfiguracionValor in loLstDiccionarioConfiguracion)
                {
                    String lsIdConfiguracion = lsConfiguracionValor.Key;
                    String[] laConfiguracion = lsIdConfiguracion.Split('_');
                    if (laConfiguracion.Length == 2)
                    {
                        ConfiguracionController.subInsertarDatosConfiguracion(laConfiguracion[1], laConfiguracion[0], lsConfiguracionValor.Value.ToString());
                    }
                }
                GeneralController.subInicializarConfiguracion();
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}