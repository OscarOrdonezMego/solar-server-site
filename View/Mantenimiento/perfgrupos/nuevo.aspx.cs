﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Controller;
using Model.bean;

namespace Pedidos_Site.Mantenimiento.perfgrupos
{
    public partial class Mantenimiento_perfgrupos_nuevo : PageController
    {
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {
                String codigo = dataJSON["codigo"].ToString();
                GrupoBean loGrupoBean = GrupoController.fnBuscarGrupo(codigo);
                txtcodigogrupo.Value = loGrupoBean.codigo;
                txtnombregrupo.Value = loGrupoBean.nombre;
                hidPk.Value = loGrupoBean.codigo;
                txtcodigogrupo.Disabled = true;
            }
        }
    }
}