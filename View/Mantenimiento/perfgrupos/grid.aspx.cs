﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model;
using Model.bean;
using Controller;

namespace Pedidos_Site.Mantenimiento.perfgrupos
{
    public partial class Mantenimiento_perfgrupos_grid : PageController
    {
        public static String lsCodMenu = "MPG";
        protected override void initialize()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);


            if (dataJSON != null)
            {
                Int32 pagina = Int32.Parse(dataJSON["pagina"].ToString());
                Int32 totalpagina = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
                String codigo = dataJSON["codigo"].ToString();
                String nombre = dataJSON["nombre"].ToString();
                String flag = dataJSON["flag"].ToString();

                PaginateGrupoBeans paginate = GrupoController.fnBuscarListarGrupo(pagina, totalpagina, codigo, nombre, flag);

                if ((pagina > 0) && pagina <= paginate.TotalFila)
                {
                    this.lbTpagina.Text = paginate.TotalFila.ToString();
                    this.lbPagina.Text = pagina.ToString();

                    if (pagina == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if ((pagina == paginate.TotalFila))
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<GrupoBean> lst = paginate.LsListaGrupoBean;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }
        }
    }
}