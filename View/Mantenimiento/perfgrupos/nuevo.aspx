﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.perfgrupos.Mantenimiento_perfgrupos_nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script>

        $(document).ready(function () {
            $('#saveReg').removeAttr("disabled");
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.id = $('#hidPk').val();
            strData.pscodigo = $('#txtcodigogrupo').val();
            strData.psnombre = $('#txtnombregrupo').val();

            return strData;
        }

        function data() {
            var sr = new getData();
            return sr.codigoperfil;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#txtcodigogrupo').val('');
            $('#txtnombregrupo').val('');
            $('#MtxtLogin').val('');
            $('#MtxtClave').val('');
            $('#McboPerfil').val($("#McboPerfil option:first").val());
            $('#McboGeocerca').val($("#McboGeocerca option:first").val());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
            <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_NUEVO_GRUPO)%>
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *</p>
            <input type="text" id="txtcodigogrupo" runat="server" class="requerid cz-form-content-input-text"
                maxlength="5" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOMBRE_GRUPO)%>
                *</p>
            <input type="text" id="txtnombregrupo" runat="server" class="requerid cz-form-content-input-text"
                maxlength="100" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>

    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidClave" Value="" runat="server" />
    </form>
</body>
</html>
