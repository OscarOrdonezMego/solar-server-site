﻿using System;
using Controller;
using Model.bean;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.perfgrupos
{
    public partial class Mantenimiento_perfgrupos_perfgrupos : PageController
    {
        public static String lsCodMenu = "MPG";
        protected override void initialize()
        {

        }
        [WebMethod]
        public static String CrearGrupo(String pscodigo, String psnombre)
        {
            try
            {
                GrupoBean loGrupoBean = new GrupoBean();
                loGrupoBean.codigo = pscodigo;
                loGrupoBean.nombre = psnombre;
                Int32 respuesta = GrupoController.crearGrupo(loGrupoBean);
                return Operation.Condicion.If(respuesta);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String EditarGrupo(String pscodigo, String psnombre)
        {
            try
            {
                GrupoBean loGrupoBean = new GrupoBean();
                loGrupoBean.codigo = pscodigo;
                loGrupoBean.nombre = psnombre;
                GrupoController.EditarGrupo(loGrupoBean);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        [WebMethod]
        public static String EliminarGrupo(String codigos, String flag)
        {
            try
            {
                GrupoController.EliminarGrupo(codigos, flag);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_ELIMINO_EXITO);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}