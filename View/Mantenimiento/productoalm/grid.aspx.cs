﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.productoalm
{
    public partial class Mantenimiento_productoalm_grid : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MENU_MANT_PRODUCTO_ALMACEN;
        PaginateAlmacenProductoBean paginate;
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        //@001 F
        public static Int32 ioFraccionamientoMaximoDecimales;

        protected override void initialize()
        {

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;

                //Obtener Fraccionamiento Decimal
                String loValorFraccionamientoDecimal = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES).Valor;
                ioFraccionamientoMaximoDecimales = Convert.ToInt32(loValorFraccionamientoDecimal);
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String pro_codigo = dataJSON["pro_codigo"].ToString();
            String pro_nombre = dataJSON["pro_nombre"].ToString();
            String alm_codigo = dataJSON["alm_codigo"].ToString();
            String flag = dataJSON["flag"].ToString();
            String presentacion = dataJSON["presentacion"].ToString();

            Int32 pagina = Int32.Parse(dataJSON["pagina"].ToString());
            Int32 filas = Int32.Parse(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor);
            if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                paginate = AlmacenProductoController.paginarBuscarAlmacen(pro_codigo, pro_nombre, alm_codigo, flag, presentacion, pagina, filas, ioFraccionamientoMaximoDecimales);
            }
            else
            {
                paginate = AlmacenProductoController.paginarBuscar(pro_codigo, pro_nombre, alm_codigo, flag, pagina, filas, ioFraccionamientoMaximoDecimales);
            }


            if ((pagina > 0) && (pagina <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina.ToString();

                if (pagina == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (pagina == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<AlmacenProductoBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();
            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }



            if ((pagina > 0) && (pagina <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = pagina.ToString();

                if (pagina == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (pagina == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<AlmacenProductoBean> lst = paginate.lstResultados;
                grdMant.DataSource = lst;
                grdMant.DataBind();
            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }
    }
}