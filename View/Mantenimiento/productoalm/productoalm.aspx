﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.productoalm.Mantenimiento_productoalm_productoalm" Codebehind="productoalm.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="Stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script> 
    <script type="text/javascript" language="javascript">
        var urldel = 'productoalm.aspx/borrar';
        var urlins = 'nuevo.aspx'
        var urlbus = 'grid.aspx';
        var urlsavN = 'productoalm.aspx/crear';
        var urlsavE = 'productoalm.aspx/editar';

        $(document).ready(function () {
            var val = '<%=liRespuestaint %>';
            if (val == 1) {
                $("#tituloalmacen").text("Mantenimiento Presentación Almacén");
            } else {
                $("#tituloalmacen").text("Mantenimiento Producto Almacén");
            }
            deleteReg();
            addReg();
            modReg();
            busReg();
            $('#buscar').trigger("click");
           
        });

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.pro_codigo = $('#txtCodigoProducto').val();
            strData.pro_nombre = $('#txtNombreProducto').val();
            strData.alm_codigo = $('#ddlAlmacen').val();
            strData.flag = $('#chkflag').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.orden = $('#hdnOrden').val();
            strData.presentacion = $('#PAtxtPresentacion').val();
            return strData;
        }
    </script>
</head>
<body class="formularyW">
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/almacen.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p id="tituloalmacen">
                       h
                        </p>
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
              <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.CREAR))
                { %>
                <input type="button" id="cz-form-box-nuevo" class="cz-form-content-input-button cz-form-content-input-button-image form-button addReg cz-form-box-content-button cz-util-right"
                    data-grid-id="divGridViewData" value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NUEVO) %>" />
              <%} %>
              <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                { %>
                <input type="button" id="cz-form-box-del" class="cz-form-content-input-button cz-form-content-input-button-image form-button delReg cz-util-right"
                    value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
               <%} %>
            </div>
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                    <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_COD_PRODUCTO)%>
                    </p>
                    <asp:TextBox ID="txtCodigoProducto" runat="server" class="cz-form-content-input-text" maxLength="20"></asp:TextBox><input
                        id="hddlTurno" type="hidden" value="-1" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NOM_PRODUCTO)%>
                    </p>
                    <asp:TextBox ID="txtNombreProducto" runat="server" class="cz-form-content-input-text"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content" <%=liValidarVisible %>>
                    <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                    </p>
                    <asp:TextBox ID="PAtxtPresentacion" runat="server" class="cz-form-content-input-text"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <p>
                    <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ALMACEN)%>
                    </p>
                    <asp:DropDownList ID="ddlAlmacen" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content" style="display:block">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HABILITADO)%></p>
                    <input type="checkbox" class="cz-form-content-input-check" id="chkflag" name="habilitado"
                        checked="checked" />
                </div>

                <div class="cz-form-content cz-util-right cz-util-right-text">
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>" />
                </div>
            </div>
            <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div class="form-gridview-data" id="divGridViewData" runat="server">
                            </div>
                            <div class="form-gridview-error" id="divGridViewError" runat="server">
                            </div>
                            <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" alt="" />
                                <p>
                                    buscando resultados</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Hidden Fields to control pagination-->
            <div id="paginator-hidden-fields">
                <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
            </div>
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true">
            </div>
        </div>
    </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
</body>
</html>