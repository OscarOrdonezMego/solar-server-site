﻿<%@ WebHandler Language="C#" Class="productoalm_producto" %>

using System;
using System.Web;
using Controller;
using Newtonsoft.Json;
using Model.bean;
using Model;
using System.Collections.Generic;

public class productoalm_producto : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        String match = context.Request.Params["q"];
        String output = "";

        List<Combo> result = ProductoController.matchProductoBean(match);

        output = Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented);
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}