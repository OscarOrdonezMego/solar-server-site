﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.productoalm.Mantenimiento_productoalm_grid" Codebehind="grid.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
    <script>
        var flag = $('#chkflag').attr("checked") ? 'T' : 'F';
        $(document).ready(function () {
            if (flag == "T") {
                $('#Tieliminar').append('<%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>');
            } else {
                $('#Tieliminar').append('Restaurar');
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("id_pro_pre") %>" type="checkbox">
                            </th>
                            <th nomb="ALMACEN">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ALMACEN)%>
                            </th>
                            <th nomb="COD_PRODUCTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_COD_PRODUCTO)%>
                            </th>
                            <th nomb="NOM_PRODUCTO" <%=liValidarVisible %>>
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                            </th>
                            <th nomb="NOM_PRODUCTO">
                                 <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                            </th>
                            <th nomb="STOCK">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_STOCK)%>
                            </th>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                              { %>
                             <th style="width: 40px;"> <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                            <%} %>
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                              { %>
                            <th style="width: 40px;" id="Tieliminar"> 
                            </th>
                            <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("id_pro_pre") %>" value="<%# Eval("id_pro_pre") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PRO_CODIGO")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("ALM_PRO_STOCK")%>
                    </td>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                      { %>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" pkpre="<%# Eval("CODIGOPK_PRESENTACION") %>" pkpro="<%# Eval("PRO_CODIGO") %>" cod="<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" alt="Editar" /></a>
                    </td>
                  <%} %>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                    { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("id_pro_pre") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                 <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <input id="<%# Eval("id_pro_pre") %>" value="<%# Eval("id_pro_pre") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PRO_CODIGO")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("ALM_PRO_STOCK")%>
                    </td>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                    { %>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" pkpre="<%# Eval("CODIGOPK_PRESENTACION") %>" pkpro="<%# Eval("PRO_CODIGO") %>"  cod="<%# Eval("id") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" alt="Editar" /></a>
                    </td>
                  <%} %>
                  <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                    { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("id_pro_pre") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                   <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de</span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" alt="" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
