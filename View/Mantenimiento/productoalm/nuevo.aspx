﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.productoalm.Mantenimiento_productoalm_nuevo" Codebehind="nuevo.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <style type="text/css">
        #divAlmacenes {
            margin-left:10px;
            margin-top: -20px;
            margin-left: 23px;
            width: 508px;
            height: 251px;
            overflow: scroll;
            box-shadow: 0 0 5px #B6B6B6;
        }

        .linea-on {
            border-bottom: 0px solid;
            width: 529px;
            height: 48px;
            box-shadow: 0 0 2px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        var urlsavN = 'productoalm.aspx/crear';
        var urlsavE = 'productoalm.aspx/editar';

        $(document).ready(function () {
            var editar = '<%=Editar%>';
            if (editar = 0) {
                $('#PAtxtpresentacion').prop('class', 'requerid cz-form-content-input-select');
                $('#PAtxtpresentacion option').remove();
                $('#PAtxtpresentacion').append('<option value="">Seleccione Producto</option>');
            } else {
                $('#PAtxtpresentacion').prop('class', 'cz-form-content-input-select');
            }
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $('#btnSave').click(function () { fc_RegistrarProductoAlmacen(); });

            $('#MtxtProducto').autocomplete('producto.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtProducto').result(function (event, item, formatted) {

                $('#mhCodigoProducto').val("");

                var vali = '<%=ioValidarInt %>';
                if (vali == 1) {
                    $("#PAtxtpresentacion").prop('class', 'requerid cz-form-content-input-select');
                    var url = "nuevo.aspx/fnBuscarPresentaciones";
                    var obj = new Object();
                    obj.codigo3 = item.Codigo;
                    enviarConAjax(url, obj);
                } else {
                    fc_LoadAlmacenes(false, '', '');
                    $("#PAtxtpresentacion").prop('class', '');
                    $('#mhCodigoProducto').val(item.Codigo)

                    var urlBuscarByProducto = 'nuevo.aspx/buscarByProducto';

                    var strData = new Object();
                    strData.pro_codigo = item.Codigo;

                    $.ajax({
                        type: 'POST',
                        url: urlBuscarByProducto,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        success: function (response) {

                            var items = response.d;

                            $.each(items, function (index, item) {
                                $(".chkAlmacen").each(function () {

                                    var alm_codigo = $(this).attr("cod");

                                    if (item.ALM_CODIGO == alm_codigo) {
                                        $(this).prop("checked", true);
                                        var idTxt = $('#' + $(this).attr("id").replace("chkAlmacen", "txtStock"));
                                        $(idTxt).val(item.ALM_PRO_STOCK);
                                        $(idTxt).prop("disabled", false);
                                    }

                                });


                            });

                        },
                        error: function (xhr, status, error) {


                        }
                    });


                }
            });

            var id = $('#hidPk').val();
            if (id == '0') {
                fc_LoadAlmacenes(false, '', '');

            } else {
                var validacionfrac = '<%=ioValidarInt%>';
                var codpre = "";
                if (validacionfrac == 1) {
                    codpre = $("#HidpkPresentacion").val();
                    fc_ObtenerInfo(id, codpre);
                } else {
                    codpre = $("#HidpkProducto").val();
                    fc_ObtenerInfo(id, codpre);

                }

            }


        });

        function fc_SeleccionarAlmacen(indice) {
            var idChecked = "#chkAlmacen" + indice;
            var idTxt = "#txtStock" + indice;
            if ($(idChecked).is(':checked')) {
                $(idTxt).prop("disabled", false);
                $(idTxt).focus();
            } else {
                $(idTxt).prop("disabled", true);
            }
        }

        function fc_GetXmlAlmacenSeleccionado() {
            var str_xml_almacen_sel = '';
            str_xml_almacen_sel += '<ROOT>';

            $(".chkAlmacen").each(function () {
                if (($(this).is(':checked'))) {

                    var alm_codigo = $(this).attr("cod");
                    var nu_stock = $('#' + $(this).attr("id").replace("chkAlmacen", "txtStock")).val();

                    str_xml_almacen_sel += '<doc ';
                    str_xml_almacen_sel += 'alm_codigo = "' + alm_codigo + '" ';
                    str_xml_almacen_sel += 'alm_pro_stock = "' + nu_stock + '" ';
                    str_xml_almacen_sel += '/>';

                }
            });

            str_xml_almacen_sel += '</ROOT>';
            return str_xml_almacen_sel;
        }

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();

            var str_xml_almacen_sel = '';
            str_xml_almacen_sel += fc_GetXmlAlmacenSeleccionado();

            strData.id = $('#hidPk').val();
            strData.pro_codigo = $('#mhCodigoProducto').val();
            strData.accion = $('#MhAccion').val();
            strData.XmlAlmacenSel = str_xml_almacen_sel;
            strData.presentacion = $('#PAtxtpresentacion').val();
            strData.stock = buscarStockAlmacen();
            strData.idalmacen = buscarCodigoAlmacen();

            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#hidPk').val('0');
            $('#MtxtProducto').val('');

            $(".chkAlmacen").each(function () {
                $(this).prop("checked", false);
                var idTxt = $('#' + $(this).attr("id").replace("chkAlmacen", "txtStock"));
                $(idTxt).val('');
                $(idTxt).prop("disabled", true);
            });
            $('#PAtxtpresentacion').val('');
            $('#PAtxtpresentacion option').remove();
            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

        }

        function fc_LoadAlmacenes(accionEditar, alm_codigo, alm_pro_stock) {

            var urlLoadAlmacenes = 'nuevo.aspx/LoadAlmacenes';
            var strData = new Object();
            var flagVisible = false;
            var txtChecked = '';
            var txtDisabled = '';
            var txtDisabledCheck = '';

            $.ajax({
                type: 'POST',
                url: urlLoadAlmacenes,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (response) {

                    var items = response.d;
                    var html_almacen = '';
                    html_almacen = '<div class="contenedor-almacen">';
                    $.each(items, function (index, item) {

                        flagVisible = false;
                        txtChecked = '';
                        txtDisabled = '';
                        txtDisabledCheck = '';

                        if (accionEditar) {
                            if (item.codigo == alm_codigo) {
                                flagVisible = true;
                                txtChecked = 'checked="checked"';
                                txtDisabledCheck = 'disabled';
                            }
                        } else {
                            flagVisible = true;
                            txtDisabled = 'disabled';
                        }

                        if (flagVisible) {

                            html_almacen += '<div class="cz-form-content" style="width:300px; height:20px; margin-top:15px;">';
                            html_almacen += '<input type="checkbox" id="chkAlmacen' + index + '" onclick="javascript: fc_SeleccionarAlmacen(' + index + ')" cod="' + item.codigo + '" class="cz-form-content-input-check chkAlmacen" ' + txtChecked + ' ' + txtDisabledCheck + ' />';
                            html_almacen += item.nombre;
                            html_almacen += '</div>';
                            html_almacen += '<div class="cz-form-content" style="width:350px; height:40px;margin-left: 201px;margin-top: -30px;">';
                            html_almacen += '<div style="margin-top:15px;">Stock:</div>';
                            html_almacen += '<div style="margin-left:50px; margin-top:-22px;">';
                            html_almacen += '<input type="text"  id="txtStock' + index + '" maxlength="5" onkeypress="javascript: return fc_PermiteNumeros(event, this)" class="almacen cz-form-content-input-text" ' + txtDisabled + ' value="' + alm_pro_stock + '" />';
                            html_almacen += '<div class="cz-form-content-input-text-visible">';
                            html_almacen += '<div class="cz-form-content-input-text-visible-button">';
                            html_almacen += '</div>';
                            html_almacen += '</div>';
                            html_almacen += '</div>';
                            html_almacen += '</div>';
                            html_almacen += '<div class="linea-on"></div>';


                        }

                    });
                    html_almacen += '</div>';
                    $('#divAlmacenes').text("");
                    $('#divAlmacenes').append(html_almacen);

                },
                error: function (xhr, status, error) {


                }
            });


        }

        function fc_ObtenerInfo(id, codpre) {

            var alm_codigo = '';
            var pro_codigo = '';
            var pro_nombre = '';
            var alm_pro_stock = '';

            var urlObtenerInfo = 'nuevo.aspx/info';
            var strData = new Object();
            strData.id = id;
            strData.codigopre = codpre;

            $.ajax({
                type: 'POST',
                url: urlObtenerInfo,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (response) {

                    var item = response.d;

                    $('#mhCodigoProducto').val(item.PRO_CODIGO);
                    $('#MtxtProducto').val(item.PRO_NOMBRE);

                    fc_LoadAlmacenes(true, item.ALM_CODIGO, item.ALM_PRO_STOCK);

                },
                error: function (xhr, status, error) {


                }
            });
        }

        function fc_RegistrarProductoAlmacen() {
            var validate0 = false, validate1 = false, validate2 = false;

            if ($.trim($('#MtxtProducto').val()) != '')
                validate0 = true;

            $(".chkAlmacen").each(function () {
                if (($(this).is(':checked'))) {
                    validate1 = true;

                    var txtid = $(this).attr("id").replace("chkAlmacen", "txtStock");
                    txtid = "#" + txtid;

                    validate2 = false;
                    if ($(txtid).val() != '') {
                        validate2 = true;
                    }

                }
            });

            if (!validate0)
                addnotify("notify", "Debe ingresar Producto.", "registeruser");
            else if (!validate1)
                addnotify("notify", "Debe seleccionar al menos un Almacén.", "registeruser");
            else if (!validate2)
                addnotify("notify", "Debe ingresar Stock para Almacén seleccionado.", "registeruser");
            else {

                var validateItems = true;
  
                $('.requerid').each(function () {
                    $(this).parent().find('span').remove();
                    if ($(this).val() == "") {
                        $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
                        $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")
                        validateItems = false;
                    }
                });

                var url = '';
                var id = $('#hidPk').val();
                if (id == '0') {
                    url = urlsavN;
                }
                else {
                    url = urlsavE;
                }

                if (validateItems) {
                    $('#divError').attr('class', "alert fade");
                    var hModal = $("#myModal").height();
                    $.ajax({
                        type: 'POST',
                        url: url,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        data: JSON.stringify(getData()),
                        success: function (data) {
                            addnotify("notify", data.d, "registeruser");

                            if (id == '0') {
                                clearCampos();
                            } else {
                                $('#myModal').modal('hide');
                            }
                            $('#buscar').trigger("click");

                        },
                        error: function (xhr, status, error) {
                            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                        }
                    });

                }

            }

        }

        function fc_PermiteNumeros(e, objTextBox) {
            if (e == null) { e = window.event; }
            if (e != null) {
                var intKeyPress = 0;
                if (e.which) // Netscape/Firefox/Opera
                    intKeyPress = e.which;
                else
                    intKeyPress = e.keyCode;

                if (intKeyPress < 48 || intKeyPress > 57) return false;
            }
            return true;
        }

        function enviarConAjax(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "S") {
                        $('#PAtxtpresentacion option').remove();
                        $('#PAtxtpresentacion').append('<option value="">No hay Registros.</option>');
                        $(".cz-form-content-input-select").each(function () {

                            $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                        });
                    } else {
                        $('#PAtxtpresentacion option').remove();
                        $('#PAtxtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                        $('#PAtxtpresentacion').append(msg.d);
                    }
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        $(document).ready(function () {
            $("#PAtxtpresentacion").change(function () {
                var codigo = $(this).val();
                var url = "nuevo.aspx/fnBuscarAlmacen";
                var obj = new Object();
                obj.prepk = codigo;
                limpiarCheked();
                fc_LoadAlmacenes(false, '', '');
                BuscarAlmacenes(url, obj);
            });
        });
        function BuscarAlmacenes(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (resultado) {
                    var obj2 = JSON.parse(resultado.d);
                    for (i = 0; i < obj2.length; i++) {
                        var codalmacen = obj2[i].ALM_CODIGO;
                        buscarAlmacen(codalmacen);
                    }

                },
                error: function (resultado) {
                    addnotify("notify", resultado.status, "registeruser");
                }
            });
        }
        function buscarCodigoAlmacen() {
            var codAlm = "";
            $('.chkAlmacen').each(function () {
                if (($(this).is(':checked'))) {
                    codAlm += $(this).attr('cod') + "|";
                }
            });
            return codAlm;
        }
        function buscarStockAlmacen() {
            var stock = "";
            $('.almacen').each(function () {
                if ($(this).val() != "") {
                    stock += $(this).val() + "|";
                }
            });
            return stock;
        }
        function limpiarCheked() {
            $('.chkAlmacen').each(function () {
                if (($(this).is(':checked'))) {
                    $(this).attr("checked", false);
                }
            });
        }
        $(document).ready(function () {
            $('#PAtxtpresentacion').change(function () {
                var urlBuscarByProducto = 'nuevo.aspx/buscarByPresentacion';

                var strData = new Object();
                strData.pre_pk = $(this).val();

                $.ajax({
                    type: 'POST',
                    url: urlBuscarByProducto,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (response) {

                        var items = response.d;

                        $.each(items, function (index, item) {

                            $(".chkAlmacen").each(function () {

                                var alm_codigo = $(this).attr("cod");

                                if (item.ALM_CODIGO == alm_codigo) {
                                    $(this).prop("checked", true);
                                    var idTxt = $('#' + $(this).attr("id").replace("chkAlmacen", "txtStock"));
                                    $(idTxt).val(item.ALM_PRO_STOCK);
                                    $(idTxt).prop("disabled", false);
                                }



                            });


                        });

                    },
                    error: function (xhr, status, error) {


                    }
                });


            });
        });

        function validarAlmacen(validateItems) {

            $('.almacen').each(function () {
                $(this).parent().find('span').remove();
                if ($(this).prop('disabled') == false && $(this).val() == "") {


                    $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");

                    $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>")


                    validateItems = false;

                } else {
                    validateItems = true;
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server"></h3>
        </div>
        <div id="myModalContent" class="modal-body">

            <div class="cz-form-content" style="width: 520px; height: 50px;">
                <div style="margin-top: 15px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>:</div>
                <div style="margin-left: 80px; margin-top: -22px;">
                    <input type="text" id="MtxtProducto" runat="server" class="requerid cz-form-content-input-text" />
                    <asp:HiddenField ID="mhCodigoProducto" runat="server" />
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
            </div>
            <div class="cz-form-content" <%=ioValidarOcultar %>>
                <div style="margin-top: 15px;"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>:</div>
                <div style="margin-left: 80px; margin-top: -22px;">
                    <select id="PAtxtpresentacion" runat="server" class="requerid cz-form-content-input-select"></select>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
            </div>

            <div class="cz-form-content" style="width: 520px; height: 10px;">
            </div>
        </div>
       
        <div id="divAlmacenes">
        </div>
        <div class="alert fade" id="divError">
            <strong id="tituloMensajeError"></strong>
            <p id="mensajeError">
            </p>
        </div>
        <div class="modal-footer">
            <%--<input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />--%>
            
            <input type="button" id="btnSave" runat="server" class="form-button cz-form-content-input-button"
                value="GUARDAR" />

        </div>
        <asp:HiddenField ID="MhAccion" runat="server" />
        <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
        <asp:HiddenField ID="hidPk" Value="0" runat="server" />
        <asp:HiddenField ID="HidpkPresentacion" Value="0" runat="server" />
        <asp:HiddenField ID="HidpkProducto" Value="0" runat="server" />
    </form>
</body>
</html>
