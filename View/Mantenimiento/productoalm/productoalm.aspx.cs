﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model.bean;
using Controller;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.productoalm
{
    public partial class Mantenimiento_productoalm_productoalm : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MENU_MANT_PRODUCTO_ALMACEN;
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        protected override void initialize()
        {

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;

            }
            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            cargarAlmacen();
        }

        private void cargarAlmacen()
        {
            ddlAlmacen.DataSource = AlmacenController.getAlmacen();
            ddlAlmacen.DataValueField = "codigo";
            ddlAlmacen.DataTextField = "nombre";
            ddlAlmacen.DataBind();
            ddlAlmacen.Items.Insert(0, new ListItem("--Todos--", ""));
        }
        [WebMethod]
        public static void borrar(String codigos, String flag)
        {
            try
            {
                if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    AlmacenProductoController.borrar(codigos, flag);
                }
                else
                {
                    AlmacenProductoController.borrarProducto(codigos, flag);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public static String crear(String presentacion, String stock, String idalmacen, String id, String pro_codigo, String XmlAlmacenSel)
        {
            try
            {
                if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    AlmacenBean loAlmacenBean = null;
                    List<AlmacenBean> loLstAlmacenBeanCodAlm = Operation.SplitVector.fnGetCodigoAlmacen(idalmacen);
                    List<AlmacenBean> loLstAlmacenBeanStock = Operation.SplitVector.fnGetStockAlmacen(stock);

                    AlmacenProductoController.subBorrarAlmacenPrsentacion(presentacion);

                    for (Int32 i = 0; i < loLstAlmacenBeanCodAlm.Count; i++)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.ALM_CODIGO = loLstAlmacenBeanCodAlm[i].ALM_CODIGO;
                        loAlmacenBean.PrePK = presentacion;
                        loAlmacenBean.Stock = loLstAlmacenBeanStock[i].Stock;
                        AlmacenProductoController.subCrearAlmacenPresentacion(loAlmacenBean);
                    }
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
                else
                {

                    AlmacenProductoBean bean = new AlmacenProductoBean();
                    bean.id = "0";
                    bean.PRO_CODIGO = pro_codigo;
                    bean.xml_almacen = XmlAlmacenSel;
                    AlmacenProductoController.crear(bean);

                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }


            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
        [WebMethod]
        public static String editar(String presentacion, String stock, String idalmacen, String id, String pro_codigo, String XmlAlmacenSel)
        {
            try
            {
                if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    AlmacenBean loAlmacenBean = null;
                    List<AlmacenBean> loLstAlmacenBeanCodAlm = Operation.SplitVector.fnGetCodigoAlmacen(idalmacen);
                    List<AlmacenBean> loLstAlmacenBeanStock = Operation.SplitVector.fnGetStockAlmacen(stock);
                    for (Int32 i = 0; i < loLstAlmacenBeanCodAlm.Count; i++)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.ALM_CODIGO = loLstAlmacenBeanCodAlm[i].ALM_CODIGO;
                        loAlmacenBean.PrePK = presentacion;
                        loAlmacenBean.Stock = loLstAlmacenBeanStock[i].Stock;
                        AlmacenProductoController.subEditarAlmacenPresentacion(loAlmacenBean);
                    }
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
                else
                {
                    AlmacenBean loAlmacenBean = null;
                    List<AlmacenBean> loLstAlmacenBeanCodAlm = Operation.SplitVector.fnGetCodigoAlmacen(idalmacen);
                    List<AlmacenBean> loLstAlmacenBeanStock = Operation.SplitVector.fnGetStockAlmacen(stock);
                    for (Int32 i = 0; i < loLstAlmacenBeanCodAlm.Count; i++)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.ALM_CODIGO = loLstAlmacenBeanCodAlm[i].ALM_CODIGO;
                        loAlmacenBean.PrePK = pro_codigo;
                        loAlmacenBean.Stock = loLstAlmacenBeanStock[i].Stock;
                        AlmacenProductoController.subEditarAlmacenProducto(loAlmacenBean);
                    }
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }


            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}