﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.productoalm
{
    public partial class Mantenimiento_productoalm_nuevo : PageController
    {
        public static Int32 ioValidarInt;
        public static String ioValidarOcultar;
        public static Int32 Editar = 0;
        protected override void initialize()
        {
            Editar = 0;
            if (!IsPostBack)
            {
                List<ConfiguracionBean> loLstConfiguracionBean = (List<ConfiguracionBean>)Session[SessionManager.LISTA_SESSION];
                ValidarFraccionamiento(loLstConfiguracionBean);

                if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
                {
                    ioValidarInt = Operation.flagACIVADO.ACTIVO;
                    ioValidarOcultar = Operation.VACIO.TEXT_VACIO;
                }
                else
                {
                    ioValidarInt = Operation.flagACIVADO.DESACTIVADO;
                    ioValidarOcultar = Operation.CSS.STYLE_DISPLAY_NONE;

                }

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = "NUEVO PRODUCTO ALMACÉN";
                btnSave.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                if (dataJSON != null)
                {
                    Editar = 1;
                    myModalLabel.InnerText = "EDITAR PRODUCTO ALMACÉN";
                    hidPk.Value = dataJSON["codigo"];
                    MtxtProducto.Disabled = true;
                    HidpkPresentacion.Value = dataJSON["pkpre"].ToString();
                    HidpkProducto.Value = dataJSON["pkpro"].ToString();
                    PAtxtpresentacion.Disabled = true;
                    List<ProductoPresentacionBean> lolstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(dataJSON["pkpro"]);
                    if (lolstProductoPresentacionBean.Count > 0)
                    {

                        for (Int32 j = 0; j < lolstProductoPresentacionBean.Count; j++)
                        {
                            PAtxtpresentacion.Items.Insert(j, lolstProductoPresentacionBean[j].Nombre);
                            PAtxtpresentacion.Items[j].Value = lolstProductoPresentacionBean[j].CodigoPresentacion;
                        }
                        for (Int32 s = 0; s < lolstProductoPresentacionBean.Count; s++)
                        {
                            if (PAtxtpresentacion.Items[s].Value.Equals(dataJSON["pkpre"].ToString()))
                            {
                                PAtxtpresentacion.Items[s].Selected = true;
                            }
                        }

                    }

                }
                else
                {
                    hidPk.Value = "0";
                }
            }

        }
        private static void ValidarFraccionamiento(List<ConfiguracionBean> poLstConfiguracionBean)
        {
            if (poLstConfiguracionBean != null)
            {
                FuncionBean loFuncionBean = new FuncionBean();
                loFuncionBean.CodigoFuncion = Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO;
                FuncionBean loFuncionBeanres = Operation.ValidacionSession.fnExtraerCodigoFuncion(poLstConfiguracionBean, loFuncionBean);
                ioValidarInt = Operation.ValidacionSession.fnValidarFuncion(loFuncionBeanres);
                ioValidarOcultar = Operation.ValidacionSession.fnValidarFuncionOcultar(loFuncionBeanres);
            }

        }
        [WebMethod]
        public static List<AlmacenBean> LoadAlmacenes()
        {
            try
            {
                List<AlmacenBean> list = AlmacenController.getAlmacen();
                return list;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
        [WebMethod]
        public static List<AlmacenProductoBean> buscarByProducto(String pro_codigo)

        {
            try
            {
                return AlmacenProductoController.infoByProducto(pro_codigo);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
        [WebMethod]
        public static List<AlmacenProductoBean> buscarByPresentacion(Int32 pre_pk)
        {
            try
            {
                return AlmacenProductoController.infoByPresentacion(pre_pk);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
        [WebMethod]
        public static AlmacenProductoBean info(String id, String codigopre)
        {
            try
            {
                AlmacenProductoBean almacenProducto = null;
                if (ioValidarInt == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    almacenProducto = AlmacenProductoController.info(id, codigopre);
                }
                else
                {
                    almacenProducto = AlmacenProductoController.info2(id, codigopre);
                }

                return almacenProducto;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
        [WebMethod]
        public static String fnBuscarPresentaciones(String codigo3)
        {
            try
            {
                String nombre = "--Seleccione--";
                String option = "";
                List<ProductoPresentacionBean> lolstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(codigo3);
                if (lolstProductoPresentacionBean.Count > 0)
                {
                    option = "<option value=\"\" >" + nombre + " </option>";
                    for (Int32 i = 0; i < lolstProductoPresentacionBean.Count; i++)
                    {
                        option += "<option value=" + lolstProductoPresentacionBean[i].CodigoPresentacion + " >" + lolstProductoPresentacionBean[i].Nombre + " " + lolstProductoPresentacionBean[i].Cantidad + "</option>";
                    }
                    return option;
                }
                else
                {
                    option = "S";
                    return option;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }
        [WebMethod]
        public static String fnBuscarAlmacen(String prepk)
        {
            try
            {
                List<AlmacenBean> lolstAlmacenBean = PedidoDetalleController.fnBuscarAlmacen(prepk);
                List<AlmacenBean> lolstAlmacenBeanReturn = new List<AlmacenBean>();
                AlmacenBean loAlmacenBean = null;

                if (lolstAlmacenBean.Count > 0)
                {
                    foreach (AlmacenBean current in lolstAlmacenBean)
                    {
                        loAlmacenBean = new AlmacenBean();
                        loAlmacenBean.codigo = current.codigo;
                        loAlmacenBean.nombre = current.nombre;
                        loAlmacenBean.PrePK = current.PrePK;
                        loAlmacenBean.Stock = current.Stock;
                        loAlmacenBean.ALM_CODIGO = current.ALM_CODIGO;
                        lolstAlmacenBeanReturn.Add(loAlmacenBean);
                    }

                }
                String json = JsonConvert.SerializeObject(lolstAlmacenBeanReturn);
                return json;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}