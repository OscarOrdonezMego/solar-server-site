﻿<%@ WebHandler Language="C#" Class="vendedor" %>

using System;
using System.Web;
using Controller;
using Newtonsoft.Json;
using Model.bean;
using Model;
using System.Collections.Generic;

public class vendedor : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        String match = context.Request.Params["q"];
        String output = "";
        String codUsu = context.Request.Params["codusupk"];
        List<Combo> result = VendedorController.matchVendedorBean2(match,codUsu);

        output = Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented);
        context.Response.Write(output);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}