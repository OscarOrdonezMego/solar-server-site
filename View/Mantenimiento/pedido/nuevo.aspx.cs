﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller;
using Model.bean;
using Newtonsoft.Json;
using Model;

/// <summary>
/// @001 GMC 13/04/2015 Se graba/actualiza campo Tipo
/// @002 GMC 15/04/2015 Se setean valores de Cond Venta y Tipo para actualizar
/// @003 GMC 22/04/2015 Ajustes para visualización de campo Tipo
/// </summary>
namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class nuevo : PageController
    {
        public string Mcv = "";
        public string Mcv2 = "";

        //@003 I
        private String valorConfigCotizacion = "F";
        public String visibleTipo = "";
        public static String liValidarAlmacenOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        public static String pkusu = "";
        //@003 F
        public String visibleProcesado = "";

        protected override void initialize()
        {
            pkusu = Session["lgn_id"].ToString();
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                liValidarAlmacenOcultar = String.Empty;
            }
            else
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            subCboAlmacen();
            cboCondicionVenta.DataSource = ReporteController.getCondicionVenta();
            cboCondicionVenta.DataValueField = "COD";
            cboCondicionVenta.DataTextField = "DEN";
            cboCondicionVenta.DataBind();

            //@001 I
            cboTipo.DataSource = ReporteController.getTipoPedido();
            cboTipo.DataValueField = "COD";
            cboTipo.DataTextField = "DEN";
            cboTipo.DataBind();
            //@001 F

            cargarComboEstado();

            Mcv = ManagerConfiguration.mostrar_condvta;
            Mcv2 = (ManagerConfiguration.mostrar_condvta == "0" ? " style='display:none'" : "");
            if (!Page.IsPostBack)
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVOPEDIDO);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);
                MtxtFechaIni.Value = DateTime.Today.ToShortDateString();
                if (dataJSON != null)
                {

                    myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARPEDIDO);
                    PedidoBean usuario = PedidoController.info(dataJSON["codigo"]);
                    hidPk.Value = dataJSON["codigo"].ToString();
                    MtxtCodigo.Value = usuario.PED_CODIGO.ToString();
                    MtxtCliente.Value = usuario.CLI_NOMBRE.ToString();
                    MtxtVendedor.Value = usuario.NOM_USUARIO.ToString();
                    mhCliente.Value = usuario.CLI_CODIGO.ToString();
                    mhVendedor.Value = usuario.USR_PK.ToString();
                    MtxtFechaIni.Value = usuario.PED_FECINICIO.ToString().Split(' ')[0];
                    txtHoraIni.Value = usuario.PED_FECINICIO.ToString().Split(' ')[1].Substring(0, 5);
                    MtxtCliente.Disabled = true;
                    MtxtVendedor.Disabled = true;
                    MtxtCodigo.Disabled = true;
                    PcboaAlmacen.SelectedValue = usuario.ALM_PK;
                    //@002 I
                    if (this.cboCondicionVenta.Items.FindByValue(usuario.PED_CONDVENTA) != null)
                        this.cboCondicionVenta.SelectedValue = usuario.PED_CONDVENTA;
                    if (this.cboTipo.Items.FindByValue(usuario.FLGTIPO) != null)
                        this.cboTipo.SelectedValue = usuario.FLGTIPO;
                    //@002 F
                    cboEstadoProcesado.SelectedValue = usuario.ESTADO_PROCESADO;
                }
                else
                {
                    hidPk.Value = "";
                    mhCliente.Value = "";
                    mhVendedor.Value = "";
                    this.visibleProcesado = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                MtxtVendedor.Focus();

                //@003 I
                this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_COTIZACION, out this.valorConfigCotizacion);
                this.visibleTipo = (this.valorConfigCotizacion == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");

                if (this.valorConfigCotizacion == Model.Enumerados.FlagHabilitado.F.ToString())
                {
                    if (this.cboTipo.Items.FindByValue("P") != null)
                        this.cboTipo.SelectedValue = "P";
                }
                //@003 F
            }
        }


        //@003 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        private void subCboAlmacen()
        {
            List<AlmacenBean> loLstPedidoBean = PedidoController.fnListaAmacen();
            if (loLstPedidoBean.Count != 0)
            {
                PcboaAlmacen.Items.Insert(0, IdiomaCultura.getMensaje(IdiomaCultura.WEB_SELECCIONAR));
                PcboaAlmacen.Items[0].Value = "0";
                for (int i = 0; i < loLstPedidoBean.Count; i++)
                {
                    PcboaAlmacen.Items.Insert(i + 1, loLstPedidoBean[i].nombre);
                    PcboaAlmacen.Items[i + 1].Value = loLstPedidoBean[i].codigo;
                }
            }
        }

        private void cargarComboEstado()
        {
            cboEstadoProcesado.Items.Insert(0, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_ETIQUETA_PROCESADO));
            cboEstadoProcesado.Items[0].Value = Constantes.EstadoFlag.TRUE;
            cboEstadoProcesado.Items.Insert(1, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_ETIQUETA_NO_PROCESADO));
            cboEstadoProcesado.Items[1].Value = Constantes.EstadoFlag.FALSE;
        }
    }
}