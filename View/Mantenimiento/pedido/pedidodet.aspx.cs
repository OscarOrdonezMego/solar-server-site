﻿using System;
using Model.bean;
using Controller;
using System.Web.Services;
using Model;

namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class pedidodet : PageController
    {
        public String xPedido = "";
        public String ioCodalmacen = "";

        public static String lsCodMenu = "MPE";
        public static Int32 ioValidacionInt;
        public static String ioValidacionVisible;
        public static Int32 ioFraccionamientoMaximoDecimales;
        public static Int32 valorMaxItemsPedido;
        public static Double valorMontoMinPedido;
        public static Double valorMontoMaxPedido;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioValidacionInt = Operation.flagACIVADO.ACTIVO;
                ioValidacionVisible = Operation.VACIO.TEXT_VACIO;

                String loValorFraccionamientoDecimal = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES).Valor;
                ioFraccionamientoMaximoDecimales = Convert.ToInt32(loValorFraccionamientoDecimal);
            }
            else
            {
                ioValidacionInt = Operation.flagACIVADO.DESACTIVADO;
                ioValidacionVisible = Operation.CSS.STYLE_DISPLAY_NONE;
                ioFraccionamientoMaximoDecimales = 0;
            }

            valorMaxItemsPedido = Convert.ToInt32(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAX_ITEMS_PEDIDO).Valor);
            valorMontoMinPedido = Convert.ToDouble(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MONTO_MIN_PEDIDO).Valor);
            valorMontoMaxPedido = Convert.ToDouble(fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MONTO_MAX_PEDIDO).Valor);

            this.vMaxItemsPedido.Value = valorMaxItemsPedido.ToString();
            this.vMontoMinPedido.Value = valorMontoMinPedido.ToString();
            this.vMontoMaxPedido.Value = valorMontoMaxPedido.ToString();

            //IDPEDIDO
            if (Request.QueryString["idpedido"] != "" && Request.QueryString["idpedido"] != null)
            {
                idpedido.Value = Request.QueryString["idpedido"].ToString();
                xPedido = Request.QueryString["idpedido"].ToString();
            }

            if (Request.QueryString["idalmacen"] != "" && Request.QueryString["idalmacen"] != null)
            {
                ioCodalmacen = Request.QueryString["idalmacen"].ToString();
            }
            //IDPEDIDO
            //idpedido.Value = Request.QueryString["idpedido"].ToString();
            //xPedido = Request.QueryString["idpedido"].ToString();
            //ioCodalmacen = Request.QueryString["idalmacen"].ToString();

        }
        private void cargarCombo()
        {

        }



        [WebMethod]
        public static void borrar(String codigos, String flag)
        {

            try
            {
                if (ioValidacionInt == 1)
                {
                    String lscodDetalle = Operation.Operaciones.fnObtenerCodigoPedidoDetalle(codigos);
                    ProductoPresentacionController.subBorrarPedidoDetallePresentacion(codigos, flag);
                }
                else
                {

                    PedidoDetalleController.borrar(codigos, flag);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        [WebMethod]
        public static String crear(String cantidafraccionada, String idprecio, String almacen, String flete, String codpresntacion, String pk, String pkDET, String codigoprod, String cantidad, String preciosoles, String preciodolares, String descuento, String montosoles, String montodolares, String descripcion, String debonificacion, String tipo, String precio, String moneda)
        {
if (precio.Trim() == "--seleccione--")
            {
                return "Debe seleccionar un precio.";
            }
            if (moneda == "S")
            {
                montodolares = "0";
                preciodolares = "0";
                montosoles = (Convert.ToDouble(cantidad) * Convert.ToDouble(precio)).ToString();
                preciosoles = precio;
            }
            if (moneda == "D")
            {
                montosoles = "0";
                preciosoles = "0";
                montodolares = (Convert.ToDouble(cantidad) * Convert.ToDouble(precio)).ToString();
                preciodolares = precio;
            }
            var cantItems = griddet.itemsPedido;
            var montoTotalfinalSoles = griddet.totalMontoPedidoSoles + Convert.ToDouble(montosoles);
            var montoTotalfinalDolares = griddet.totalMontoPedidoDolares + Convert.ToDouble(montodolares);
            
            if (valorMaxItemsPedido != 0 && valorMaxItemsPedido == cantItems)
            {
                return "Supera maximo de items por pedido : " + cantItems;
            }
            if ((preciosoles.Trim() == string.Empty || preciosoles.Trim() == "0") && (preciodolares.Trim() == string.Empty || preciodolares.Trim() == "0"))
            {
                throw new Exception("Se debe ingresar al menos un precio");
            }
            if (valorMontoMaxPedido != 0.0 && montoTotalfinalSoles > valorMontoMaxPedido)
            {
                return "Supera el monto maximo por pedido de : S/." + valorMontoMaxPedido;
            }

            try
            {
                if (tipo.Equals("1"))
                {
                    ProductoPresentacionBean loProductoPresentacionBean = PedidoDetalleController.fnBuscarCantidadPresentacion(codpresntacion);
                    PedidoDetalleBean loPedidoDetalleBean = new PedidoDetalleBean();
                    String loAlmacen = "";
                    loPedidoDetalleBean.PED_PK = pk;
                    loPedidoDetalleBean.CODIGO_PRE_PK = codpresntacion;
                    if (almacen != null)
                    {
                        loPedidoDetalleBean.ALM_PK = Int32.Parse(loAlmacen = (almacen != null) ? almacen : "0");
                    }
                    else
                    {
                        loPedidoDetalleBean.ALM_PK = 0;
                    }
                    loPedidoDetalleBean.PRE_CANTIDAD = loProductoPresentacionBean.Cantidad;

                    /*FRACCIONAMIENTO DECIMAL com máximo número de decimales*/
                    Decimal loCantidadDecimal = Convert.ToDecimal(cantidad);
                    cantidad = Convert.ToString(Decimal.Round(loCantidadDecimal, ioFraccionamientoMaximoDecimales));

                    loPedidoDetalleBean.TipoMoneda = moneda;
                    loPedidoDetalleBean.DET_CANTIDAD = cantidad;
                    loPedidoDetalleBean.DET_PRECIO = "0";
                    loPedidoDetalleBean.DET_PRECIO_SOLES = preciosoles;
                    loPedidoDetalleBean.DET_PRECIO_DOLARES = preciodolares;
                    if (descuento.Length > 0)
                    {
                        loPedidoDetalleBean.DET_DESCUENTO = descuento;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_DESCUENTO = "0";
                    }
loPedidoDetalleBean.DET_MONTO = "0";
                    loPedidoDetalleBean.DET_MONTO_SOLES = Operation.Operaciones.fnMontoTotalNeto(preciosoles, cantidad, loPedidoDetalleBean.DET_DESCUENTO);
                    loPedidoDetalleBean.DET_MONTO_DOLARES = Operation.Operaciones.fnMontoTotalNeto(preciodolares, cantidad, loPedidoDetalleBean.DET_DESCUENTO);
                    loPedidoDetalleBean.DET_CANTIDAD_FRAC = cantidafraccionada;
                    loPedidoDetalleBean.DET_DESCRIPCION = descripcion;
                    if (debonificacion.Length > 0)
                    {
                        loPedidoDetalleBean.DET_BONIFICACION = debonificacion;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_BONIFICACION = "0";
                    }

                    if (flete.Length > 0)
                    {
                        loPedidoDetalleBean.DET_FLETE = flete;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_FLETE = "0";
                    }
                    loPedidoDetalleBean.DET_TIPODESC = "p";
                    if (idprecio == null)
                    {
                        loPedidoDetalleBean.CodListaPrecio = "0";

                    }
                    else
                    {
                        loPedidoDetalleBean.CodListaPrecio = idprecio;
                    }

                    PedidoDetalleController.crearPedidoDetalle(loPedidoDetalleBean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
                else
                {
                    PedidoDetalleBean bean = new PedidoDetalleBean();
                    bean.TipoMoneda = moneda;
                    bean.id = pk;
                    bean.idDET = "0";
                    bean.PRO_CODIGO = codigoprod;
                    bean.DET_CANTIDAD = cantidad;
                    bean.DET_PRECIO = "0";
                    bean.DET_PRECIO_SOLES = preciosoles;
                    bean.DET_PRECIO_DOLARES = preciodolares;
                    bean.DET_MONTO = "0";
                    bean.DET_MONTO_SOLES = montosoles;
                    bean.DET_MONTO_DOLARES = montodolares;
                    bean.DET_DESCRIPCION = descripcion;
                    bean.DET_BONIFICACION = debonificacion;
                    bean.DET_DESCUENTO = descuento;
                    PedidoDetalleController.crear(bean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        [WebMethod]
        public static String editar(String cantidafraccionada, String idprecio, String almacen, String flete, String codpresntacion, String pk, String pkDET, String codigoprod, String cantidad, String preciosoles, String preciodolares, String descuento, String montosoles, String montodolares, String descripcion, String debonificacion, String precio, String moneda)
        {
if (precio.Trim() == "--seleccione--")
            {
                return "Debe seleccionar un precio.";
            }
            if (moneda == "S")
            {
                montodolares = "0";
                preciodolares = "0";
                montosoles = (Convert.ToDouble(cantidad) * Convert.ToDouble(precio)).ToString();
                preciosoles = precio;
            }
            if (moneda == "D")
            {
                montosoles = "0";
                preciosoles = "0";
                montodolares = (Convert.ToDouble(cantidad) * Convert.ToDouble(precio)).ToString();
                preciodolares = precio;
            }
            var cantItems = griddet.itemsPedido;
            var montoTotalfinalSoles = griddet.totalMontoPedidoSoles + Convert.ToDouble(montosoles);
            var montoTotalfinalDolares = griddet.totalMontoPedidoDolares + Convert.ToDouble(montodolares);

            if (valorMaxItemsPedido != 0 && valorMaxItemsPedido == cantItems)
            {
                return "Supera maximo de items por pedido :" + cantItems;
            }
            if ((preciosoles.Trim() == string.Empty || preciosoles.Trim() == "0") && (preciodolares.Trim() == string.Empty || preciodolares.Trim() == "0"))
            {
                throw new Exception("Se debe ingresar al menos un precio");
            }
            if (valorMontoMaxPedido != 0.0 && montoTotalfinalSoles > valorMontoMaxPedido)
            {
                return "Supera el monto maximo por pedido de :" + valorMontoMaxPedido;
            }

            try
            {
                if (ioValidacionInt == 1)
                {
                    ProductoPresentacionBean loProductoPresentacionBean = PedidoDetalleController.fnBuscarCantidadPresentacion(codpresntacion);
                    PedidoDetalleBean loPedidoDetalleBean = new PedidoDetalleBean();
                    loPedidoDetalleBean.idDET = pkDET;
                    loPedidoDetalleBean.PED_PK = pk;
                    loPedidoDetalleBean.CODIGO_PRE_PK = codpresntacion;
                    if (almacen != null)
                    {
                        loPedidoDetalleBean.ALM_PK = Int32.Parse(almacen);
                    }
                    else
                    {
                        loPedidoDetalleBean.ALM_PK = 0;
                    }
                    loPedidoDetalleBean.PRE_CANTIDAD = loProductoPresentacionBean.Cantidad;

                    /*FRACCIONAMIENTO DECIMAL com máximo número de decimales*/
                    Decimal loCantidadDecimal = Convert.ToDecimal(cantidad);
                    cantidad = Convert.ToString(Decimal.Round(loCantidadDecimal, ioFraccionamientoMaximoDecimales));

                    loPedidoDetalleBean.TipoMoneda = moneda;
                    loPedidoDetalleBean.DET_CANTIDAD = cantidad;
                    loPedidoDetalleBean.DET_PRECIO = "0";
                    loPedidoDetalleBean.DET_PRECIO_SOLES = preciosoles;
                    loPedidoDetalleBean.DET_PRECIO_DOLARES = preciodolares;
                    if (descuento.Length > 0)
                    {
                        loPedidoDetalleBean.DET_DESCUENTO = descuento;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_DESCUENTO = "0";
                    }
loPedidoDetalleBean.DET_MONTO = "0";
                    loPedidoDetalleBean.DET_MONTO_SOLES = Operation.Operaciones.fnMontoTotalNeto(preciosoles, cantidad, loPedidoDetalleBean.DET_DESCUENTO);
                    loPedidoDetalleBean.DET_MONTO_DOLARES = Operation.Operaciones.fnMontoTotalNeto(preciodolares, cantidad, loPedidoDetalleBean.DET_DESCUENTO);
                    loPedidoDetalleBean.DET_CANTIDAD_FRAC = cantidafraccionada;
                    loPedidoDetalleBean.DET_DESCRIPCION = descripcion;
                    if (debonificacion.Length > 0)
                    {
                        loPedidoDetalleBean.DET_BONIFICACION = debonificacion;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_BONIFICACION = "0";
                    }

                    if (flete.Length > 0)
                    {
                        loPedidoDetalleBean.DET_FLETE = flete;
                    }
                    else
                    {
                        loPedidoDetalleBean.DET_FLETE = "0";
                    }
                    loPedidoDetalleBean.DET_TIPODESC = "p";
                    if (idprecio == null)
                    {
                        loPedidoDetalleBean.CodListaPrecio = "0";

                    }
                    else
                    {
                        loPedidoDetalleBean.CodListaPrecio = idprecio;
                    }
                    PedidoDetalleController.editarPedidoDetalle(loPedidoDetalleBean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }
                else
                {
                    PedidoDetalleBean bean = new PedidoDetalleBean();
                    bean.id = pk;
                    bean.idDET = pkDET;
                    bean.PRO_CODIGO = codigoprod;
                    bean.DET_CANTIDAD = cantidad;
                    bean.DET_PRECIO = "0";
                    bean.DET_PRECIO_SOLES = preciosoles;
                    bean.DET_PRECIO_DOLARES = preciodolares;
                    bean.DET_DESCUENTO = descuento;
                    bean.DET_MONTO = "0";
                    bean.DET_MONTO_SOLES = montosoles;
                    bean.DET_MONTO_DOLARES = montodolares;
                    bean.DET_DESCRIPCION = descripcion;
                    bean.DET_BONIFICACION = debonificacion;
                    bean.TipoMoneda = moneda;
                    PedidoDetalleController.editar(bean);
                    return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
                }


            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
    }
}