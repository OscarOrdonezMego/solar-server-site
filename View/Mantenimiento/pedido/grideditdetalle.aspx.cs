﻿using System;
using Model.bean;
using Controller;
using Model;
using System.Data;

namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class grideditdetalle : PageController
    {
        public Boolean bonificacion = false;
        public string Mbm = "";
        public static Int32 ioRespuesta;
        public static String liValidarVisible;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
            }


            String xPedido = "", xEdicion = "";
            xPedido = Request.QueryString["idpedido"].ToString();
            xEdicion = Request.QueryString["idedicion"].ToString();
            Mbm = ManagerConfiguration.nBonificacionManual;
            DataTable odt;
            if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                odt = ReporteController.PedidoDetalleEdicionList(xPedido, xEdicion);
            }
            else
            {
                odt = ReporteController.PedidoDetalleEdicionList(xPedido, xEdicion);
            }

            grdMant.DataSource = odt;
            grdMant.DataBind();
        }
    }
}