﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;
using System.Web.UI.HtmlControls;

/// <summary>
/// @001 GMC 18/04/2015 Ajustes para validar visualización de Dirección Despacho
/// @002 GMC 05/05/2015 Ajustes para validar visualización de Almacén
/// </summary>
namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class actividad_actividad_grid : PageController
    {
        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MENU_MANT_PEDIDO;
        //@001 I
        public String visibleCondicion = "";
        private String valorDireccionDespacho = "F";
        private String valorCondicion = "F";
        public String visibleDireccionDespacho = "";
        //@001 F
        //@002 I
        private String valorConfigMostrarAlmacen = "F";
        public String visibleColumnaAlmacen = "";
        //@002 F
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String xfecha = dataJSON["fecha"].ToString();
            String xclicod = dataJSON["clicodigo"].ToString();
            String xvendcod = dataJSON["vencodigo"].ToString();
            String xflag = dataJSON["flag"].ToString();

            String xpagina = dataJSON["pagina"].ToString();
            String fTipoPedido = dataJSON["FlagTipoPedido"].ToString();
            String procesado = dataJSON["procesado"].ToString();
            String xTipoArticulo = "";
            if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                xTipoArticulo = Operation.TipoArticulo.PRESENTACION;
            }
            else
            {
                xTipoArticulo = Operation.TipoArticulo.PRODUCTO;
            }
            String xfilas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;

            String codperfil = dataJSON["codperfil"].ToString();
            String codgrupo = dataJSON["codgrupo"].ToString();
            String id_usu = "";
            String perfilUsu = Session["lgn_codsupervisor"].ToString();
            if (Operation.ValidacionSession.ValidarPerfil(perfilUsu))
            {
                id_usu = Session["lgn_id"].ToString();
            }
            //@001 I
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_DIRECCION_DESPACHO, out this.valorDireccionDespacho);
            this.visibleDireccionDespacho = (this.valorDireccionDespacho == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            //@001 F
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_CONDICION_VENTA, out this.valorCondicion);
            this.visibleCondicion = (this.valorCondicion == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            //@002 I
            this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN, out this.valorConfigMostrarAlmacen);
            this.visibleColumnaAlmacen = (this.valorConfigMostrarAlmacen == Model.Enumerados.FlagHabilitado.F.ToString() ? " style='display:none'" : "");
            //@002 F

            PaginatePedidoBean paginate = PedidoController.paginarBuscar(xclicod, xvendcod, xfecha, xflag, xpagina, xfilas, xTipoArticulo, codperfil, codgrupo, id_usu, fTipoPedido, procesado);

            if ((Int32.Parse(xpagina) > 0) && (Int32.Parse(xpagina) <= paginate.totalPages))
            {
                this.lbTpagina.Text = paginate.totalPages.ToString();
                this.lbPagina.Text = xpagina;

                if (Int32.Parse(xpagina) == 1)
                { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                if (Int32.Parse(xpagina) == paginate.totalPages)
                { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                List<PedidoBean> lst = paginate.lstResultados;

                //@001 I

                //@001 F

                //@002 I

                //@002 F

                grdMant.DataSource = lst;
                grdMant.DataBind();


            }
            else
            {
                String htmlNoData = "<div class='gridNoData'>" +
                                    "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                    "<p>No se encontraron datos para mostrar</p>" +
                                    "</div>";

                this.divGridView.InnerHtml = htmlNoData;
                this.divGridViewPagintator.Visible = false;
            }
        }


        //@001 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        //@001 F

        protected void GridViewRowEventHandler(Object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var estado = ((PedidoBean)e.Item.DataItem).ESTADO_PROCESADO.ToString();
                //var fecha_edicion = ((PedidoBean)e.Item.DataItem).PED_FECEDICION.ToString(); 
                if (!estado.Equals("Procesado"))
                {
                    ((HtmlTableCell)e.Item.FindControl("procesado")).Style.Add("background-color", "#5caa55");
                }
                // if (fecha_edicion.Equals(String.Empty)) {
                //     ((HtmlLink)e.Item.FindControl("ediciones")).Style.Add("alt", "sin ediciones");
                //}
            }
        }

        public String grabarjson()
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            String xpagina = dataJSON["pagina"].ToString();

            return xpagina;
        }
    }
}