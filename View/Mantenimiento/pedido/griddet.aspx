﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.griddet" Codebehind="griddet.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml id="grilladetallepedido"">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <script>
        $(document).ready(function () {
            var res=0;
            res='<%=liValidarVisible %>';
            if (res.length === 0) {
                $(".columna").css("display","none");
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="mhIdPedido" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="montoPedido" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="numeroItems" runat="server"></asp:HiddenField>
    
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server">
            
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center"  >
                                <input id="ChkAll" name="chkSelectAll" value="<%= xIDpedido %>|<%# Eval("ID") %>" type="checkbox">
                            </th>
                          
                            <th nomb="CODIGOPEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO)%>
                            </th>
                            <th nomb="CODIGODETALLE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_DETALLE)%>
                            </th>
                            <th nomb="PRODUCTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                            </th>
                            <th nomb="NOMBRE_PRESENTACION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                            </th>
                            <th nomb="CANTIDAD_PRESENTACION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD_PRESENTACION)%>
                            </th>
                            <th nomb="UNIDAD_FRACCIONAMIENTO" <%=liValidarVisible %>>
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%>
                            </th>
                            <th nomb="CANTIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                           <th nomb="FRACCION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FRACCION)%>
                            </th>
                            <th nomb="PRECIO_SOLES">
                                Precio Soles
                            </th>
                            <th nomb="PRECIO_DOLARES">
                                Precio Dólares
                            </th>
                            <th  nomb="DESC">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESC)%>
                            </th>
                            <th nomb="MONTO_SOLES">
                                Monto Soles
                            </th>
                            <th nomb="MONTO_DOLARES">
                                Monto Dólares
                            </th>
                            <th  nomb="FLETE" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FLETE)%>
                            </th>
                            <th nomb="DESCRIPCION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                            </th>
                             <% if (bonificacion == true)
                              { %>
                            <th nomb="BONIFICACION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                            </th>
                            
                            <%} %>
                            <%if(fnValidarPerfilMenu(lsCodMenu,Model.Enumerados.FlagPermisoPerfil.EDITAR)){%>
                            <th style="width: 40px;">
                             <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                            </th>
                          <%} %>
                          <%if(fnValidarPerfilMenu(lsCodMenu,Model.Enumerados.FlagPermisoPerfil.ELIMINAR)){ %>
                            <th style="width: 40px;">
                             <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                            </th>
                           <%} %>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%= xIDpedido %>|<%# Eval("ID") %>" value="<%= xIDpedido %>|<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("IDPED")%>
                    </td>
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>

                    <td <%=liValidarVisible %>>
                        <%# Eval("CANTIDAD_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                    <td>
                        <%# Eval("DET_CANTIDAD")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DET_CANTIDAD_FRACCION")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIO_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIO_DOLARES")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCUENTO")%>
                    </td>
                    <td>
                        <%# Eval("DET_MONTO_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("DET_MONTO_DOLARES")%>
                    </td>
                      <td <%=liValidarVisible %>>
                        <%# Eval("DET_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCRIPCION")%>
                    </td>
                    <% if (bonificacion == true)
                              { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>

                     <%} %>
                     <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                       { %>
                    <td style="">
                         <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%= xIDpedido %>|<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                   <%} %>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%= xIDpedido %>|<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                   <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <input id="<%= xIDpedido %>|<%# Eval("ID") %>" value="<%= xIDpedido %>|<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("IDPED")%>
                    </td>
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>

                    <td <%=liValidarVisible %>>
                        <%# Eval("CANTIDAD_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                    <td>
                        <%# Eval("DET_CANTIDAD")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DET_CANTIDAD_FRACCION")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIO_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIO_DOLARES")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCUENTO")%>
                    </td>                    
                    <td>
                        <%# Eval("DET_MONTO_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("DET_MONTO_DOLARES")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DET_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCRIPCION")%>
                    </td>
                    <% if (bonificacion == true)
                              { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>


                     <%} %>
                     <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                       { %>
                    <td style="">
                         <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%= xIDpedido %>|<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar " /></a>
                    </td>
                   <%} %>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%= xIDpedido %>|<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar " /></a>
                    </td>
                   <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label></p>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>
