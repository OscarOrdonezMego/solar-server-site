﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Model.bean;
using Controller;
using Controller.functions;
using System.Web.Services;
using Model;

/// <summary>
/// @001 GMC 13/04/2015 Se graba/actualiza campo Tipo
/// </summary>
namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class pedido : PageController
    {

        public static String lsCodMenu = Constantes.CodigoValoresMenu.COD_MENU_MANT_PEDIDO;
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;
        public static String deshabilitarCboSupervisor;
        public static String codigoSub = "";
        public static String pkusu = "";
        protected override void initialize()
        {

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }

            if (Session["lgn_id"] == null || !fnValidarPerfilMenu(lsCodMenu, Enumerados.FlagPermisoPerfil.VER))
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }

            if (!IsPostBack)
            {
                llenarComboBox();
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                if (codigoSub == "SUP")
                {
                    cboPerfil.SelectedValue = pkusu;
                    deshabilitarCboSupervisor = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                else
                {
                    deshabilitarCboSupervisor = "";
                }
                llenarcoGrupo();
                llenarComboBoxVendedor(pkusu);
            }

            txtFechaIni.Value = Utils.getFechaActual();
            cargarComboTIpoPedido();
            cargarComboEstado();

        }
        private void cargarComboTIpoPedido()
        {
            cboTipoPedido.Items.Insert(0, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_TODOS));
            cboTipoPedido.Items[0].Value = "";
            cboTipoPedido.Items.Insert(1, "Pedido");
            cboTipoPedido.Items[1].Value = "P";
            cboTipoPedido.Items.Insert(2, "Cotización");
            cboTipoPedido.Items[2].Value = "C";
        }
        private void cargarComboEstado()
        {
            cboProcesado.Items.Insert(0, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_TODOS));
            cboProcesado.Items[0].Value = String.Empty;
            cboProcesado.Items.Insert(1, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_ETIQUETA_PROCESADO));
            cboProcesado.Items[1].Value = Constantes.EstadoFlag.TRUE;
            cboProcesado.Items.Insert(2, IdiomaCultura.getMensajeEncodeHTML_Upper(IdiomaCultura.WEB_ETIQUETA_NO_PROCESADO));
            cboProcesado.Items[2].Value = Constantes.EstadoFlag.FALSE;
        }
        private void llenarComboBox()
        {
            List<UsuarioBean> lista = UsuarioController.ListaSupervisores();
            if (lista.Count != 0)
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboPerfil.Items.Insert(i + 1, lista[i].nombre);
                    cboPerfil.Items[i + 1].Value = lista[i].id;

                }
            }
            else
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "";
            }
        }
        private void llenarcoGrupo()
        {
            List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxGrupo(String codigo, String perfil)
        {
            List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), perfil);
            if (lista.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                    cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();

                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }
        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();

                }
            }
            else
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
            }
        }
        [WebMethod]
        public static String MostarVendedoresPorGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Equals("0"))
            {
                List<UsuarioBean> lstUsuarioBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(pkusu));
                if (lstUsuarioBean.Count != 0)
                {
                    Rpta += ToolBox.Option("-1", "", "Todos");
                    for (int i = 0; i < lstUsuarioBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstUsuarioBean[i].id.ToString(), "", lstUsuarioBean[i].nombre);

                    }
                }
            }
            else
            {
                List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresPorGrupo(Int32.Parse(codigo));
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("-1", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Usu_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {
                    Rpta = ToolBox.Option("-1", "", "No Hay Registro.").ToString();
                }
            }
            return Rpta;
        }
        [WebMethod]
        public static String MostarGrupos(String codigo)
        {
            String Rpta = "";
            if (codigo.Length == 0)
            {
                codigo = "0";
            }
            if (codigo.Equals("0"))
            {
                List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
                if (loLstGrupoBean.Count != 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (int i = 0; i < loLstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(loLstGrupoBean[i].Grupo_PK.ToString(), "", loLstGrupoBean[i].nombre);

                    }
                }
                else
                {
                    Rpta = ToolBox.Option("0", "", "No Hay Registro.").ToString();
                }
            }
            else
            {
                List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), "SUP");
                if (lstGrupoBean.Count > 0)
                {
                    Rpta = ToolBox.Option("0", "", "Todos").ToString();
                    for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                    {
                        Rpta += ToolBox.Option(lstGrupoBean[i].Grupo_PK.ToString(), "", lstGrupoBean[i].nombre);
                    }
                }
                else
                {


                }
            }

            return Rpta;
        }




        [WebMethod]
        public static void borrar(String codigos, String flag)
        {
            try
            {
                PedidoController.borrar(codigos, flag);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public static String crear(String almacen, String id, String codigo, String fecha, String condicionvta, String clicodigo, String clivendedor, String tipo, String procesado)
        {
            try
            {
                TipoCambioBean tcs = TipoCambioController.buscarTipoCambio("0");
                TipoCambioBean tcd = TipoCambioController.buscarTipoCambio("1");
                if (tcs == null && tcd == null) return "No existe tipo de cambio vigente.";
                else if (tcs == null) return "No existe tipo de cambio Soles vigente.";
                else if (tcd == null) return "No existe tipo de cambio Dólares vigente.";

                PedidoBean bean = new PedidoBean();
                bean.TipoCambioSoles = tcs.id;
                bean.TipoCambioDolares = tcd.id;

                bean.id = "0";
                bean.PED_CODIGO = codigo;
                bean.PED_FECINICIO = Utils.getStringFechaYYMMDDHHMM(fecha);
                if (ManagerConfiguration.mostrar_condvta == "1")
                {
                    bean.PED_CONDVENTA = condicionvta;
                }
                else
                {
                    bean.PED_CONDVENTA = "";
                }
                bean.CLI_CODIGO = clicodigo;
                bean.USR_PK = clivendedor;
                bean.FLGTIPO = tipo;
                bean.ALM_PK = almacen;
                if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    bean.TipoArticulo = Enumerados.TipoArticulo.PRE.ToString();
                }
                else
                {
                    bean.TipoArticulo = Enumerados.TipoArticulo.PRO.ToString();
                }
                bean.ESTADO_PROCESADO = procesado;
                PedidoController.crear(bean);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
        [WebMethod]
        public static String editar(String almacen, String id, String codigo, String fecha, String condicionvta, String clicodigo, String clivendedor, String tipo, String procesado)
        {
            try
            {

                PedidoBean bean = new PedidoBean();
                bean.id = id;
                bean.PED_CODIGO = codigo;
                bean.PED_FECINICIO = Utils.getStringFechaYYMMDDHHMM(fecha);
                if (ManagerConfiguration.mostrar_condvta == "1")
                {
                    bean.PED_CONDVENTA = condicionvta;
                }
                else
                {
                    bean.PED_CONDVENTA = "";
                }
                bean.CLI_CODIGO = clicodigo;
                bean.USR_PK = clivendedor;
                bean.FLGTIPO = tipo; //@001 I/F
                bean.ALM_PK = almacen;
                if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    bean.TipoArticulo = Enumerados.TipoArticulo.PRE.ToString();
                }
                else
                {
                    bean.TipoArticulo = Enumerados.TipoArticulo.PRO.ToString();
                }
                bean.ESTADO_PROCESADO = procesado;
                PedidoController.editar(bean);
                return IdiomaCultura.getMensaje(IdiomaCultura.WEB_MENSAJE_EXITO);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
    }
}