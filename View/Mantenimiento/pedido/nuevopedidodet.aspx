﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.nuevopedidodet" Codebehind="nuevopedidodet.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script>

        $(document).ready(function () {
            var flete = '<%=flete%>';
            if (flete == 0) {
                $('#divflete').css('display', 'none');
            } else {
                $('#divflete').css('display', 'block');
            }
            var frac = '<%=liRespuestaint%>';
            if (frac == 1) {
                $("#PDcboaAlmacenes").attr("class", "requerid cz-form-content-input-select");
            } else {
                $("#PDcboaAlmacenes").attr("class", "cz-form-content-input-select");
            }
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $('#MtxtProducto').autocomplete('producto.ashx',
            {
                multiple: false,
                minChars: 0,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.Nombre, result: row.Nombre

                        }
                    });
                },
                formatItem: function (item) {

                    return item.Nombre;
                }

            });

            $('#MtxtProducto').result(function (event, item, formatted) {
                debugger;
                $('#mhCodigo').val("");
                if (item) {
                    $('#mhCodigo').val(item.Codigo);
                    console.log(item.Precio);
                    var vali = '<%=liRespuestaint%>';
                    if (vali == 1) {
                        var valalmacen = '<%=liValidarAlmacenInt%>';
                        var url = "nuevopedidodet.aspx/fnBuscarPresentaciones";
                        var obj = new Object();
                        obj.codigo = item.Codigo;
                        if (valalmacen == 1) {
                            obj.tipo = "ALM";
                        } else {
                            obj.tipo = "PRE";
                        }
                        enviarConAjax(url, obj);
                    } else {
                        //document.ready = document.getElementById("cboMoneda").value = 'D';
                        //$("#cboMoneda").find('option:contains("'+'Dólares'+'")').prop('selected', true);
                        if (item.PrecioSoles == null) $('#cboMoneda').val('D'); else $('#cboMoneda').val('S');
                        $('#cboMoneda').trigger('change');

                        $('#MtxtPrecioSoles').val(item.PrecioSoles);
                        $('#MtxtPrecioDolares').val(item.PrecioDolares);
                        var obj11 = new Object();
                        obj11.codigo = item.Codigo;
                        var url12 = "nuevopedidodet.aspx/fnBuscarAlmacen";
                        enviarConAjax3(url12, obj11);
                    }
                }
            });

            function tipocambio(moneda)
            {
                var obj = new Object();
                obj.moneda = moneda;
                obj.pedido = $('#hIDpedido').val();
                $.ajax({
                    url: 'nuevopedidodet.aspx/fnBuscarTipoCambio',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        if (moneda == "S") $('#hdSoles').val(msg.d.valor);
                        if (moneda == "D") $('#hdDolares').val(msg.d.valor);
                    },
                    error: function (result) {
                        addnotify("notify", result.status, "buscatipocambio");
                    }
                });
            };

            tipocambio('S');
            tipocambio('D');

            $('#cboMoneda').change(function () {
                debugger;
                var vali = '<%=liRespuestaint%>';
                if (vali == 1) {
                    var obj = new Object();
                    obj.codigo = $('#PDtxtpresentacion').val();
                    if (obj.codigo != null) {
                        if ($('#ValidadPrecio').val() == "T")
                        {
                            var urllp = "nuevopedidodet.aspx/fnBuscarListaPrecion";
                            BuscarListaPrecio(urllp, obj);
                        }
                        else
                        {
                            var url = "nuevopedidodet.aspx/fnBuscarPrecioPresentacion";
                            enviarConAjax2(url, obj);
                        } 
                    }
                }
                else {

                var tcSoles = $('#hdSoles').val();
                var tcDolares = $('#hdDolares').val();
                if (tcSoles == "" && tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio.", "buscatipocambio");
                else if (tcSoles == "") addnotify("notify", "El pedido no tiene tipo de cambio Soles.", "buscatipocambio");
                else if (tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio Dólares.", "buscatipocambio");
                else if ($('#mhCodigo').val() != '')
                {
                    var obj = new Object();
                    obj.codigo = $('#mhCodigo').val();
                    $.ajax({
                        url: 'nuevopedidodet.aspx/fnBuscarProducto',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            if ($('#cboMoneda').val() == 'S')
                            {
                                if (msg.d.preciobasesoles == null)
                                {
                                    if (tcDolares == null) $('#MtxtPrecioBase').val('');
                                    else {
                                        $('#MtxtPrecioBase').val(msg.d.preciobasedolares * tcDolares);
                                        addnotify("notify", "Se calculó el precio Soles: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcDolares + " y precio Dólares " + msg.d.preciobasedolares + ").", "Cálculo Dólares");
                                    }
                                } else
                                $('#MtxtPrecioBase').val(msg.d.preciobasesoles);
                            }
                            else if ($('#cboMoneda').val() == 'D')
                            {
                                if (msg.d.preciobasedolares == null)
                                {
                                    if (tcSoles == null) $('#MtxtPrecioBase').val('');
                                    else {
                                        $('#MtxtPrecioBase').val(msg.d.preciobasesoles * tcSoles);
                                        addnotify("notify", "Se calculó el precio Dólares: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcSoles + " y precio Soles " + msg.d.preciobasesoles + ").", "Cálculo Soles");
                                    }
                                } else
                                $('#MtxtPrecioBase').val(msg.d.preciobasedolares);
                            }
                        },
                        error: function (result) {
                            addnotify("notify", result.status, "buscaproducto");
                        }
                    }); 
                }

                }
            });

            $('#PDtxtpresentacion').change(function () {
                debugger;
                if ($(this).val() == "") {
                    $('#ppresenacion').text("Presentación *");
                    //$('#MtxtPrecio').val('');
                    $('#MtxtPrecioSoles').val('');
                    $('#MtxtPrecioDolares').val('');
                    $('#PDcboFraccionamiento option').remove();
                    $("#PDcboFraccionamiento").append("<option value=\"0\">Sin Fraccionamiento </option>");
                    $('#PDcboFraccionamiento').parent().find(".cz-form-content-input-select-visible-text").html($('#PDcboFraccionamiento').find("option:selected").text());
                }else{
                var obj = new Object();
                obj.codigo = $(this).val();
                var valAlmacenDesactivado = '<%=liValidarAlmacenInt%>';
                if (valAlmacenDesactivado == 1) {
                    var url2 = "nuevopedidodet.aspx/fnBuscarAlmacen";
                    enviarConAjax3(url2, obj);
                } else {
                    var url5 = "nuevopedidodet.aspx/fnBuscarStockPresentacion";
                    buscarStockPresentacion(url5, obj);
                }
                
                var urllp = "nuevopedidodet.aspx/fnBuscarListaPrecion";
                BuscarListaPrecio(urllp, obj);
                //if ($('#ValidadPrecio').val() == "F")
                //{
                    var url = "nuevopedidodet.aspx/fnBuscarPrecioPresentacion";                
                    enviarConAjax2(url, obj);
                //}
                var urllpre = "nuevopedidodet.aspx/fnBuscarCantidadPresentacion";
                buscarCantidadPresentacion(urllpre, obj);
                $('#divflete').css('display', 'none');
                }
            });
            $('#PDcboaAlmacenes').change(function () {
                var url2 = "nuevopedidodet.aspx/fnBuscarStockAlmacen";
                var obj = new Object();
                obj.codigo = $(this).val();
                obj.codpre = $('#PDtxtpresentacion').val();
                enviarConAjax4(url2, obj);
                var codpedido = $('#CODIGOPEDIDO').val();
                if (codpedido == obj.codigo) {
                    $('#pAlmacen').text("Almacen * Stock: 0");
                    $('#divflete').css('display', 'none');
                } else {
                    $('#pAlmacen').text("Almacen * Stock: 0");
                    $('#divflete').css('display', 'block');
                }

            });
        });
        function buscarCantidadPresentacion(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var obj2 = JSON.parse(msg.d);
                    $('#PDcboFraccionamiento option').remove();
                    for (i = 0; i < obj2.length; i++) {
                        if (obj2[i].Total == 0) {
                            $("#PDcboFraccionamiento").append("<option value=" + obj2[i].Total + ">Sin Fraccionamiento </option>");
                        } else {
                            $("#PDcboFraccionamiento").append("<option value=\"" + obj2[i].Total + "\">" + obj2[i].Resulado + "</option>");
                        }                        
                    }
                    $('#PDcboFraccionamiento').parent().find(".cz-form-content-input-select-visible-text").html($('#PDcboFraccionamiento').find("option:selected").text());
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function enviarConAjax(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "S") {
                        $('#PDtxtpresentacion option').remove();
                        $('#PDtxtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html('No hay Registros.');
                    } else {
                        $('#PDtxtpresentacion option').remove();
                        $('#PDtxtpresentacion').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                        $('#PDtxtpresentacion').append(msg.d);
                    }

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function enviarConAjax2(ulrs, obj) {
            //obj.moneda = $('#cboMoneda').val();
            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    debugger;
                    var tcSoles = $('#hdSoles').val();
                    var tcDolares = $('#hdDolares').val();
                    if (tcSoles == "" && tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio.", "buscatipocambio");
                    else if (tcSoles == "") addnotify("notify", "El pedido no tiene tipo de cambio Soles.", "buscatipocambio");
                    else if (tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio Dólares.", "buscatipocambio");
                    else {
                        if ($('#ValidadPrecio').val() == "F")
                        {
                            if ($('#cboMoneda').val() == 'S') {
                                if (msg.d.PRO_PRECIOBASE_SOLES == null) {
                                    if (tcDolares == null) $('#MtxtPrecioBase').val('');
                                    else {
                                        $('#MtxtPrecioBase').val(msg.d.PRO_PRECIOBASE_DOLARES * tcDolares);
                                        addnotify("notify", "Se calculó el precio Soles: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcDolares + " y precio Dólares " + msg.d.PRO_PRECIOBASE_DOLARES + ").", "Cálculo Dólares");
                                    }
                                } else
                                    $('#MtxtPrecioBase').val(msg.d.PRO_PRECIOBASE_SOLES);
                            }
                            else if ($('#cboMoneda').val() == 'D') {
                                if (msg.d.PRO_PRECIOBASE_DOLARES == null) {
                                    if (tcSoles == null) $('#MtxtPrecioBase').val('');
                                    else {
                                        $('#MtxtPrecioBase').val(msg.d.PRO_PRECIOBASE_SOLES * tcSoles);
                                        addnotify("notify", "Se calculó el precio Dólares: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcSoles + " y precio Soles " + msg.d.PRO_PRECIOBASE_SOLES + ").", "Cálculo Soles");
                                    }
                                } else
                                    $('#MtxtPrecioBase').val(msg.d.PRO_PRECIOBASE_DOLARES);
                            }
                        }
                    }
                    //$("#MtxtPrecio").val(msg.d);

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function BuscarListaPrecio(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var obj2 = JSON.parse(msg.d);
                    if (obj2 == "") {
                        $('#divPrecio').css("display", "block");
                        $("#MtxtPrecioBase").attr("class", "requerid cz-form-content-input-text");
                        //$("#PDcboListaPrecio").attr("class", "cz-form-content-input-select-visible-text");
                        $('#divlistaprecio').css("display", "none");
                        $('#ValidadPrecio').val("F");
                    } else {
                        $('#ValidadPrecio').val("T");
                        $('#divlistaprecio').css("display", "block");
                        $('#PDcboListaPrecio option').remove();
                        $("#PDcboListaPrecio").append("<option value=\"\"> --seleccione-- </option>");
                        for (i = 0; i < obj2.length; i++) {
                            debugger;
                            var tcSoles = $('#hdSoles').val();
                            var tcDolares = $('#hdDolares').val();
                            if (tcSoles == "" && tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio.", "buscatipocambio");
                            else if (tcSoles == "") addnotify("notify", "El pedido no tiene tipo de cambio Soles.", "buscatipocambio");
                            else if (tcDolares == "") addnotify("notify", "El pedido no tiene tipo de cambio Dólares.", "buscatipocambio");
                            else
                            {
                                if ($('#cboMoneda').val() == 'S') {
                                    if (obj2[i].preciosoles == null || obj2[i].preciosoles == '') {
                                        if (tcDolares == null) $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + '' + "</option>");
                                        else {
                                            $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + (obj2[i].preciodolares * tcDolares) + "</option>");
                                            //addnotify("notify", "Se calculó el precio Soles: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcDolares + " y precio Dólares " + msg.d.preciobasedolares + ").", "Cálculo Dólares");
                                        }
                                    } else
                                        $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + obj2[i].preciosoles + "</option>");
                                }
                                else if ($('#cboMoneda').val() == 'D') {
                                    if (obj2[i].preciodolares == null || obj2[i].preciodolares == '') {
                                        if (tcSoles == null) $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + '' + "</option>");
                                        else {
                                            $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + (obj2[i].preciosoles * tcSoles) + "</option>");
                                            //addnotify("notify", "Se calculó el precio Dólares: " + $('#MtxtPrecioBase').val() + " (tipo de cambio " + tcSoles + " y precio Soles " + msg.d.preciobasesoles + ").", "Cálculo Soles");
                                        }
                                    } else
                                        $("#PDcboListaPrecio").append("<option value=\"" + obj2[i].id + "\">" + obj2[i].preciodolares + "</option>");
                                }
                            }
                        }
                        $('#PDcboListaPrecio').parent().find(".requerid cz-form-content-input-select-visible-text").html($('#PDcboListaPrecio').find("option:selected").text());
                        $('#divPrecio').css("display", "none");
                        //$("#PDcboListaPrecio").attr("class", "requerid cz-form-content-input-select-visible-text");
                        $("#MtxtPrecioBase").attr("class", "cz-form-content-input-text");
                        $('#PDcboListaPrecio').trigger('change');
                    }              
                    
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function enviarConAjax3(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "S") {
                        $('#PDcboaAlmacenes option').remove();
                        $("#PDcboaAlmacenes").append("<option value=\"0\"> --seleccione-- </option>");
                        $('#pAlmacen').text("Almacen * Stock: 0");
                        $('#PDcboaAlmacenes').parent().find(".cz-form-content-input-select-visible-text").html('No hay Registros.');
                    } else {
                        
                        $('#PDcboaAlmacenes option').remove();
                        $('#PDcboaAlmacenes').parent().find(".cz-form-content-input-select-visible-text").html('--Seleccione--');
                        $('#PDcboaAlmacenes').append(msg.d);
                    }

                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }

        function buscarStockPresentacion(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d.length > 0) {
                        $('#ppresenacion').text("Presentación * Stock: " + msg.d);
                    } else {
                        $('#ppresenacion').text("Presentación *");
                    }
                    
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function enviarConAjax4(ulrs, obj) {

            $.ajax({
                url: ulrs,
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d == "") {
                        $('#pAlmacen').text("Almacen * Stock: 0");
                    } else {
                        $('#pAlmacen').text(msg.d);
                    }
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();
            strData.preciosoles = 0;
            strData.preciodolares = 0;
            strData.pk = $('#hIDpedido').val();
            strData.pkDET = $('#hIDpedidoDET').val();
            strData.codpresntacion = $('#PDtxtpresentacion').val();
            strData.codigoprod = $('#mhCodigo').val(); //  $('#mhCliente').val();
            strData.cantidad = $('#MtxtCantidad').val();
            if ($('#ValidadPrecio').val() == "T") {
                //strData.preciosoles = ($('#cboMoneda').val() == 'S') ? $("#PDcboListaPrecio option:selected").html() : null;
                //strData.preciodolares = ($('#cboMoneda').val() == 'D') ? $("#PDcboListaPrecio option:selected").html() : null;
                strData.precio = $("#PDcboListaPrecio option:selected").html();
            } else {
                //strData.preciosoles = $('#MtxtPrecioSoles').val();
                //strData.preciodolares = $('#MtxtPrecioDolares').val();
                strData.precio = $('#MtxtPrecioBase').val();
            }            
            strData.descuento = $('#MtxtDescuento').val();
            strData.montosoles = $('#MtxtPrecioSoles').val() * $('#MtxtCantidad').val();
            strData.montodolares = $('#MtxtPrecioDolares').val() * $('#MtxtCantidad').val();
            strData.descripcion = $('#MtxtDescripcion').val();
            
            if ($('#MtxtBonificacion').val() == "") {
                strData.debonificacion = "";
                //$("#cbobonificacion option[value='-1']").hide();
            }else{
                strData.debonificacion = $('#MtxtBonificacion').val();
            }

            strData.almacen = $('#PDcboaAlmacenes').val();
            strData.flete = $('#pdTxtFlete').val();
            strData.idprecio = $('#PDcboListaPrecio').val();
            strData.cantidafraccionada = $('#PDcboFraccionamiento').val();
            //strData.debonificacion = $('#MtxtBonificacion').val();
            //            strData.accion = $('#MhAccion').val();
            //strData.supervisor = $('#chkSupervisor').attr("checked") ? 'T' : 'F';
            strData.tipo = '<%=liRespuestaint%>';
            
            strData.moneda = $('#cboMoneda').val();
            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#hIDpedidoDET').val('0');
            $('#MtxtProducto').val('');
            $('#MtxtCantidad').val('');
            $('#MtxtPrecioSoles').val('');
            $('#MtxtPrecioDolares').val('');
            $('#MtxtDescuento').val('');
            $('#MtxtBonificacion').val('');
            //          $('#MtxtHasta').val($('#hFechaActual').val());
            $('#MtxtDescripcion').val('');
            $('#PDtxtpresentacion').val('');
            $('#PDcboaAlmacenes').val('');
            $('#PDcboListaPrecio').val('');
            $('#pdTxtFlete').val('');
            $('#PDcboFraccionamiento').val('');

            $('#PDcboaAlmacenes option').remove();
            $('#PDtxtpresentacion option').remove();
            $('#PDcboListaPrecio option').remove();
            $('#PDcboFraccionamiento option').remove();

            $('#pAlmacen').text("Almacen * Stock: 0");

            $('#divPrecio').css("display", "block");
            $("#MtxtPrecioBase").attr("class", "requerid cz-form-content-input-text");
            $('#divlistaprecio').css("display", "none");

            $('#MtxtPrecioBase').val('');
            $('#cboMoneda').val();

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
        }

        function obtenerMaximoDecimales(){
            var maximoDecimales = "<%=ioFraccionamientoMaximoDecimales%>";
            return maximoDecimales
        }

        function fc_PermiteDecimalConLimite(e, intMaxDecimales) {
            var character = String.fromCharCode(window.event.keyCode)
            var newValue = e.value + character;

            if (newValue.length == 1 && character != '-') {
                return true;
            }

            if (isNaN(newValue) || decimalPlaces(newValue) > intMaxDecimales) {
                window.event.preventDefault();
                return false;
            }
        }

        function decimalPlaces(num) {
            var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if (!match) { return 0; }
            return Math.max(
                 0,
                 // Number of digits right of decimal point.
                 (match[1] ? match[1].length : 0)
                 // Adjust for scientific notation.
                 - (match[2] ? +match[2] : 0));
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
        </h3>
    </div>
    <div id="myModalContent" class="modal-body">
      
          <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                *</p>
            <input type="text" id="MtxtProducto" runat="server" class="requerid cz-form-content-input-text" />
            <asp:HiddenField ID="mhCodigo" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        <div class="cz-form-content" <%=liValidarVisible %>>
                <p id="ppresenacion"> <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%> *</p>
                <select id="PDtxtpresentacion" runat="server" class="requerid cz-form-content-input-select"></select>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
        <div class="cz-form-content" style="display:none" id="divlistaprecio">
                <p><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_LISTA_PRECIO)%>*</p>
                <select id="PDcboListaPrecio" runat="server" class=" cz-form-content-input-select"></select>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
        <div class="cz-form-content"  <%=liValidarAlmacenOcultar %>>
        <!--@002 F-->
            <asp:Label id="pAlmacen" runat="server">Stock * </asp:Label>
            <select id="PDcboaAlmacenes" runat="server" class="requerid cz-form-content-input-select"></select>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
                 <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                *</p>
            <input type="text" id="MtxtCantidad" runat="server" class="requerid cz-form-content-input-text" onkeypress="fc_PermiteDecimalConLimite(this, '3')" />
             
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdSoles" runat="server" />
            <asp:HiddenField ID="hdDolares" runat="server" />
        <div class="cz-form-content" id="divPrecio">
            <p> Precio</p>
            <%--<input type="hidden" id="MtxtPrecio" runat="server" />--%>
            
            <input type="text" id="MtxtPrecioBase" runat="server" class="requerid cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>

          <div class="cz-form-content">
            <p>
                Tipo Moneda
            </p>
            <asp:DropDownList ID="cboMoneda" runat="server" CssClass="cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>

        <div class="cz-form-content" id="divPrecioSoles" style="display:none;">
            <p> Precio Soles</p>
            <input type="text" id="MtxtPrecioSoles" runat="server" class="cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        <div class="cz-form-content" id="divPrecioDolares" style="display:none;">
            <p> Precio Dólares</p>
            <input type="text" id="MtxtPrecioDolares" runat="server" class="cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);" />
             
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>

      

        <div id="lblporcentaje" runat="server" class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_DESCPORCENTAJE)%>
                </p>
            <input type="text" id="MtxtDescuento" runat="server" class=" cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);"/>
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
       
            <div  class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                </p>
            <input type="text" id="MtxtDescripcion" runat="server" class=" cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>

          <div class="cz-form-content" <%=liValidarBonificacionOcultar%>>
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                </p>
            <input type="text" id="MtxtBonificacion" runat="server" class=" cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);"/>
             
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>

       <div class="cz-form-content" id="divflete" style="display:none">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FLETE)%> *
                </p>
            <input type="text" id="pdTxtFlete" runat="server" class=" cz-form-content-input-text" onkeypress="fc_PermiteDecimal(this);"/>
             
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        <div class="cz-form-content" <%=liValidarVisible %>>
                <p><%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FRACCIONAMIENTO)%> *</p>
                <select id="PDcboFraccionamiento" runat="server" class="requerid cz-form-content-input-select"></select>
                <div class="cz-form-content-input-select-visible">
                    <p class="cz-form-content-input-select-visible-text">
                    </p>
                    <div class="cz-form-content-input-select-visible-button">
                    </div>
                </div>
            </div>
    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hIDpedido" Value="" runat="server" />
    <asp:HiddenField ID="hIDpedidoDET" Value="" runat="server" />
    <asp:HiddenField ID="CODIGOPEDIDO" Value="" runat="server" />
    <asp:HiddenField ID="ValidadPrecio" Value="" runat="server" />

      
    </form>

</body>
</html>
