﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.gridedit" Codebehind="gridedit.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
     <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <title></title>
 
</head>
<body>
    <form id="form1" runat="server">
        <div id="cz-form-box">
         <div class="cz-form-box-content">
               <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/pedidoeditados.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PEDIDOS_EDITADOS)%></p>
                                                     
                    </div>
                </div>

              <input type="button"   id="cz-form-box-atras" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-util-right"
                    value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VOLVER)%>"  />
                            
            </div>
     <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div id="divGridView" runat="server">
                                 <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>  
                              <th  nomb="CODIGOEDICION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO_EDITADO)%>
                            </th>
                            <th  nomb="CODIGOPEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO)%>
                            </th>
                            <th  nomb="CODIGOVENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_VENDEDOR)%>
                            </th>
                            <th  nomb="VENDEDOR">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                            <th  nomb="CLIENTE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                              <th  nomb="MONTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%>
                            </th>
                            <th  nomb="FECHAREGISTRO" >
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAREGISTRO)%>
                            </th>
                            <th  nomb="FECHAEDICION">
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAEDICION)%>
                            </th>
                      <th style="width: 40px;">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DETALLE)%>
                             </th>
                     
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                     <td>
                        <%# Eval("ID_EDICION")%>
                    </td>
                    <td>
                        <%# Eval("PED_PK")%>
                    </td>
                    <td>
                        <%# Eval("USR_CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("USR_NOMBRE")%>
                    </td>
                    <td >
                        <%# Eval("CLI_NOMBRE")%>
                    </td>

                    <td>
                        <%# Eval("PED_MONTOTOTAL")%>
                    </td>
                    <td>
                        <%# Eval("PED_FECREGISTRO")%>
                    </td>
                    <td>
                        <%# Eval("PED_FECREGISTROEDICION")%>
                    </td>                         
                    <td>
                        <a  style="cursor: pointer;" 
                             href="grideditdetalle.aspx?idpedido=<%# Eval("PED_PK")%>&idedicion=<%# Eval("ID_EDICION") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle" alt="Detalle" /></a>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                     <td>
                        <%# Eval("ID_EDICION")%>
                    </td>
                    <td>
                        <%# Eval("PED_PK")%>
                    </td>
                    <td>
                        <%# Eval("USR_CODIGO")%>
                    </td>
                    <td>
                        <%# Eval("USR_NOMBRE")%>
                    </td>
                    <td >
                        <%# Eval("CLI_NOMBRE")%>
                    </td>

                    <td>
                        <%# Eval("PED_MONTOTOTAL")%>
                    </td>
                    <td>
                        <%# Eval("PED_FECREGISTRO")%>
                    </td>
                    <td>
                        <%# Eval("PED_FECREGISTROEDICION")%>
                    </td>           
                     <td>
                        <a  style="cursor: pointer;" 
                              href="grideditdetalle.aspx?idpedido=<%# Eval("PED_PK")%>&idedicion=<%# Eval("ID_EDICION") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle" alt="Detalle" /></a>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
          </div>
   
 
    </form>
    <script>

        $(document).ready(function () {

            function obtenerDatosGrilla() {
                return $.ajax({
                    type: "POST",
                    data: JSON.stringify(getParametros()),
                    url: "gridedit.aspx",
                    dataType: "html",
                    async: false
                }).responseText;
            }

            $('#cz-form-box-atras').click(function () {
     
                    parent.history.back();
             
            });

        });
        
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  $('#cz-form-box-atras').click(function () {

      parent.history.back();

  });
}

    </script>
</body>
</html>
