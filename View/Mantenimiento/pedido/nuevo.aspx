﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.nuevo" Codebehind="nuevo.aspx.cs" %>

<%--
@001 GMC 13/04/2015 Se graba/actualiza campo Tipo
@002 GMC 22/04/2015 Ajustes para visualización de campo Tipo
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>    
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>

    <script>
     $(document).ready(function () {
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
         
            $(".cz-form-content-input-text-calendar2").click(function () {
                var calendar = new CalendarPopup("calendarwindow2");
                calendar.setCssPrefix("NEXTEL");
                var txtname = $(this).parent().parent().parent().find("input.cz-form-content-input-calendar").attr("id");
                console.log(txtname);
                calendar.select(eval("document.forms[1]." + txtname), $(this).attr("id"), 'dd/MM/yyyy');
                //var xtop = $('#calendarwindow2').position().top - 262;
                var xtop = $('#calendarwindow2').position().top - 87;
                console.log(xtop);

                if (txtname == "MtxtFechaIni") {
                    $('#calendarwindow2').css({ 'left': '64px', 'top': xtop + 'px' })
                }
                else {
                    $('#calendarwindow2').css({ 'left': '280px', 'top': xtop + 'px' })
                }
                return false;
            });

            $('#MtxtCliente').autocomplete('cliente.ashx',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtCliente').result(function (event, item, formatted) {
                $('#mhCliente').val("");
                if (item) {
                    $('#mhCliente').val(item.Codigo);
                }
            });

            $('#MtxtVendedor').autocomplete('vendedor.ashx?codusupk='+'<%=pkusu%>',
                     {
                         multiple: false,
                         minChars: 0,
                         max: 12,
                         autoFill: false,
                         cacheLength: 0,
                         mustMatch: true,
                         matchContains: false,
                         selectFirst: false,
                         dataType: 'json',
                         parse: function (data) {
                             return $.map(data, function (row) {
                                 return {
                                     data: row, value: row.Nombre, result: row.Nombre

                                 }
                             });
                         },
                         formatItem: function (item) {

                             return item.Nombre;
                         }

                     });

            $('#MtxtVendedor').result(function (event, item, formatted) {
                $('#mhVendedor').val("-1");
                if (item) {
                    $('#mhVendedor').val(item.Codigo);
                }
            });


        });

        function getData() {//funcion encargada de enviar los parametros para la insercion o edicion

            var strData = new Object();

            strData.id = $('#hidPk').val();
            strData.codigo = $('#MtxtCodigo').val();//  $('#mhCliente').val();
            strData.fecha = $('#MtxtFechaIni').val() + ' ' + $('#txtHoraIni').val();
            strData.condicionvta = $('#cboCondicionVenta').val();
            strData.clicodigo = $('#mhCliente').val();
            strData.clivendedor = $('#mhVendedor').val();
            //@001 I
            strData.tipo = $('#cboTipo').val();
            strData.almacen = $('#PcboaAlmacen').val();
            strData.procesado = $('#cboEstadoProcesado').val();
            //@001 F            

            return strData;
        }

        function clearCampos() {//funcion encargada de limpiar los input
            $('#hidPk').val('');
            $('#MtxtCodigo').val('');
            $('#MtxtVendedor').val('');
            $('#MtxtCliente').val('');
            $('#cboCondicionVenta').val($("#cboTipoDocumento option:first").val());
            $('#mhVendedor').val('');
            $('#mhCliente').val('');
            $('#txtHoraIni').val('00:00');
            $('#PcboaAlmacen').val('');

            $(".cz-form-content-input-select").each(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
            
        }
        $(document).ready(function () {
            var valor = '<%=liValidarAlmacenInt%>';
            if (valor == 1) {
                $('#oculalmacen').css('display', 'none');
            }
        });
    </script>
</head>
<body>
     <form id="form1" runat="server">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <h3 id="myModalLabel" runat="server">
        </h3>
    </div>
    <div id="myModalContent" class="modal-body" style="height:200px">
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CODIGO)%>
                *</p>
            <input type="text" id="MtxtCodigo" runat="server"  maxlength="20" class="requerid cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">     
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                *</p>
            <input type="text" id="MtxtCliente" runat="server" class="requerid cz-form-content-input-text" />
             <asp:HiddenField ID="mhCliente" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                *</p>
            <input type="text" id="MtxtVendedor" runat="server" class="requerid cz-form-content-input-text" />
             <asp:HiddenField ID="mhVendedor" runat="server" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                   
                </div>
            </div>
        </div>
        <div class="cz-form-content" <%= Mcv2  %> >
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_COND_VTA)%>
                *</p>
            <asp:DropDownList ID="cboCondicionVenta" runat="server" CssClass=" requerid cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_FECHA_VENCIMIENTO)%>
            </p>
            <input name="MtxtFechaIni" type="text" value="30/05/2014" maxlength="10" id="MtxtFechaIni"
                runat="server" class="cz-form-content-input-calendar" onkeypress="javascript:fc_Slash(this.id, '/');"
                onblur="javascript:fc_ValidaFechaOnblur(this.id);" />
            <div class="cz-form-content-input-calendar-visible">
                <div class="cz-form-content-input-calendar-visible-button">
                    <img alt="<>" id="Img1" style="position: absolute; z-index: 2; padding: 4px; margin-top: 1px;"
                        name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar2"
                        src="../../images/icons/calendar.png" />
                </div>
            </div>
            <div id="calendarwindow2" class="calendar-window">
            </div>
        </div>
        <div class="cz-form-content">
            <p>
                &nbsp;
            </p>
            <input type="text" id="txtHoraIni" value="00:00" runat="server" maxlength="5" size="6"
                onblur="javascript:fc_ValidaHoraOnblur(this.id);" class="cz-form-content-input-text" />
            <div class="cz-form-content-input-text-visible">
                <div class="cz-form-content-input-text-visible-button">
                </div>
            </div>
        </div>
        <!--@001 I-->
        <!--@002 I-->
        <!--<div class="cz-form-content" >-->
        <div class="cz-form-content" <%= visibleTipo %> >
        <!--@002 F-->
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_TIPO)%>
                *</p>
            <asp:DropDownList ID="cboTipo" runat="server" CssClass=" requerid cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
         <div class="cz-form-content" id="oculalmacen" >
        <!--@002 F-->
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_ALMACEN)%>
                         *</p>
            <asp:DropDownList ID="PcboaAlmacen" runat="server" CssClass="cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>
        <!--@001 F-->

        <div class="cz-form-content" <%= visibleProcesado %> >
        <!--@002 F-->
            <p>
                <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_ESTADO)%>
                *</p>
            <asp:DropDownList ID="cboEstadoProcesado" runat="server" CssClass=" requerid cz-form-content-input-select">
            </asp:DropDownList>
            <div class="cz-form-content-input-select-visible">
                <p class="cz-form-content-input-select-visible-text">
                </p>
                <div class="cz-form-content-input-select-visible-button">
                </div>
            </div>
        </div>


    </div>
    <div class="alert fade" id="divError">
        <strong id="tituloMensajeError"></strong>
        <p id="mensajeError">
        </p>
    </div>
    <div class="modal-footer">
        <input type="button" id="saveReg" runat="server" class="form-button cz-form-content-input-button"
            value="GUARDAR" />
    </div>
    <asp:HiddenField ID="MhAccion" runat="server" />
    <asp:HiddenField ID="HidAutoGen" Value="" runat="server" />
    <asp:HiddenField ID="hFechaActual" Value="" runat="server" />
    <input id="hObligatorio" type="hidden" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_INGRESEVALOR)%> " />
    <asp:HiddenField ID="hidPk" Value="" runat="server" />
    <asp:HiddenField ID="hidClave" Value="" runat="server" />

      
    </form>

   
   
</body>
</html>
