﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.pedidodet" Codebehind="pedidodet.aspx.cs" %>
<%@ Reference Page="~/Mantenimiento/pedido/griddet.aspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
 <script>
     var urldel = 'pedidodet.aspx/borrar';
     var urlins = 'nuevopedidodet.aspx?idpedido=<%=xPedido %>&idAlmacen=<%=ioCodalmacen %> '
        var urlbus = 'griddet.aspx';
        var urlsavN = 'pedidodet.aspx/crear';
        var urlsavE = 'pedidodet.aspx/editar';

        $(document).ready(function () {

            deleteReg();
            //addReg();
            modReg();
            busReg();
            $('#buscar').trigger("click");

        });

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.id = $('#idpedido').val();
            strData.codigo = $('#txtCodigo').val();
            strData.producto = $('#txtProducto').val();
            strData.flag = $('#chkflag').attr("checked") ? 'T' : 'F';
            strData.pagina = $('#hdnActualPage').val();
            strData.filas = $('#hdnShowRows').val();
            strData.presentacion = $('#txtpresentacion').val();

            return strData;
        }
    </script>
</head>
<body >
    <form id="form1" runat="server">
    <div class="cz-submain cz-submain-form-background">
        <div id="cz-form-box">
            <div class="cz-form-box-content">
                <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/pedidos.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_DETALLEPEDIDO)%></p>
                                                     
                    </div>
                </div>
                <input type="button" id="cz-form-box-vertabla" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-form-box-content-button cz-u-expand-table cz-util-right"
                    data-grid-id="divGridViewData" value="Ver Tabla" />
               
                <input type="button" id="cz-form-box-nuevo" class="cz-form-content-input-button cz-form-content-input-button-image form-button addReg cz-form-box-content-button cz-util-right"
                    data-grid-id="divGridViewData" value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NUEVO) %>" />
             
                <input type="button" id="cz-form-box-del" class="cz-form-content-input-button cz-form-content-input-button-image form-button delRegD cz-util-right"
                    value="<%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
              &nbsp;&nbsp
              <input type="button" id="cz-form-box-atras" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-util-right"
                    value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VOLVER)%>" />
                            
            </div>
             
            <div class="cz-form-box-content">
              
                <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CODIGO)%></p>
                    <asp:TextBox ID="txtCodigo" runat="server"  maxlength="20" class="cz-form-content-input-text"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                   <div class="cz-form-content">
                    <p>
                        <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PRODUCTO)%> </p>
                    <asp:TextBox ID="txtProducto" runat="server" class="cz-form-content-input-text"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content" <%=ioValidacionVisible %>>
                    <p>
                       <%=Model.bean.IdiomaCultura.getMensaje_Upper(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%></p>
                    <asp:TextBox ID="txtpresentacion" runat="server" class="cz-form-content-input-text"></asp:TextBox>
                    <div class="cz-form-content-input-text-visible">
                        <div class="cz-form-content-input-text-visible-button">
                        </div>
                    </div>
                </div>
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_HABILITADO)%></p>
                        <input type="checkbox" class="cz-form-content-input-check" id="chkflag" name="habilitado"
                            checked="checked" />
                    </div>
                    <div class="cz-form-content cz-util-right cz-util-right-text">
                        <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                            value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_BUSCAR)%>">
                    </div>
                </div>
                <div class="cz-form-box-content" id="grillaDetallePedido">
                    <div class="form-grid-box">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData" runat="server">
                                </div>
                                <div class="form-gridview-error" id="divGridViewError" runat="server">
                                </div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>
                                        buscando resultados</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Hidden Fields to control pagination-->
                <div id="paginator-hidden-fields">
                    <asp:HiddenField ID="vMontoMinPedido" Value="" runat="server" />
                    <asp:HiddenField ID="vMaxItemsPedido" Value="" runat="server" />
                    <asp:HiddenField ID="vMontoMaxPedido" Value="" runat="server" />
                    <asp:HiddenField ID="montoTotal" Value="" runat="server" />
                    <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                    <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    <asp:HiddenField ID="hdnOrden" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnBNombre" Value="" runat="server" />

                          <input type="hidden" id="hidRestaurar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />
                <input type="hidden" id="hidEliminar" value="<%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>" />
                <input type="hidden" id="hidSMSRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR_TODO_DATOS)%>" />
                <input type="hidden" id="hidSMSEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTROS)%>" />
                <input type="hidden" id="hidSimpleEliminar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ELIMINAR)%> <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MENSAJE_ACCIONREGISTRO)%>" />
                <input type="hidden" id="hidSimpleRestaurar" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_RESTAURAR)%>" />

                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                </div>
            </div>
        </div>
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
    <input runat="server" type="hidden" id="idpedido" value="0" />
    <script>

        $(document).ready(function () {

            function obtenerDatosGrilla() {
                return $.ajax({
                    type: "POST",
                    data: JSON.stringify(getParametros()),
                    url: "griddet.aspx",
                    dataType: "html",
                    async: false
                }).responseText;
            }

            $('#cz-form-box-atras').click(function () {
                var montoPedido = parseFloat($(obtenerDatosGrilla()).find("#montoPedido").val());
                var valorMinimoPedido = parseFloat($("#vMontoMinPedido").val());
                var flagHabilitado = $('#chkflag').attr("checked") ? 'T' : 'F';

                if (flagHabilitado == 'T' && valorMinimoPedido != 0.0 && montoPedido < valorMinimoPedido) {
                    $('#myModal').html(alertHtml('alertValidacion', "Aun no llega al monto minimo de S/." + valorMinimoPedido + " por pedido."));
                    $('#myModal').modal('show');
                } else {
                    parent.history.back();
                }
            });

            $('.addReg').click(function () {
                var valorItemsPedido = parseFloat($("#vMaxItemsPedido").val());

                if (valorItemsPedido == parseInt($(obtenerDatosGrilla()).find("#numeroItems").val())) {
                    $('#myModal').html(alertHtml('alertValidacion', "Ya no puede agregar más items a su pedido!"));
                    $('#myModal').modal('show');
                } else {
                    $.ajax({
                        type: 'POST',
                        url: urlins,
                        success: function (data) {
                            $("#myModal").html(data);
                            $('#myModal').modal('show');

                            $('#saveReg').click(function (e) {
                                e.preventDefault();
                                var validateItems = true;

                                $('.requerid').each(function () {
                                    $(this).parent().find('span').remove();
                                    if ($(this).val() == "") {
                                        $(this).parent().find('.cz-form-content-input-text-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
                                        $(this).parent().find('.cz-form-content-input-select-visible').after("<span style='color:#b94a48'>" + $('#hObligatorio').val() + "</span>");
                                        validateItems = false;
                                    }
                                });


                                if (validateItems) {

                                    $('#divError').attr('class', "alert fade");
                                    var hModal = $("#myModal").height();
                                    $.ajax({
                                        type: 'POST',
                                        url: urlsavN,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        async: true,
                                        cache: false,
                                        data: JSON.stringify(getData()),
                                        success: function (data) {
                                            addnotify("notify", data.d, "registeruser");
                                            clearCampos();
                                            $('#buscar').trigger("click");
                                        },
                                        error: function (xhr, status, error) {
                                            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                                        }
                                    });

                                }
                            });

                        },
                        error: function (xhr, status, error) {
                            $("#myModal").html(alertHtml('error', xhr.responseText));
                            $('#myModal').modal('show');
                        }
                    });
                }
            });

            $('#txtcliente').autocomplete('cliente.ashx',
            {
                multiple: false,
                minChars: 0,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.Nombre, result: row.Nombre

                        }
                    });
                },
                formatItem: function (item) {

                    return item.Nombre;
                }
            });

            $('#txtcliente').result(function (event, item, formatted) {
                $('#hcliente').val("");
                if (item) {
                    $('#hcliente').val(item.Codigo);
                }
            });

            $('#txtVendedor').autocomplete('vendedor.ashx',
            {
                multiple: false,
                minChars: 0,
                max: 12,
                autoFill: false,
                cacheLength: 0,
                mustMatch: true,
                matchContains: false,
                selectFirst: false,
                dataType: 'json',
                parse: function (data) {
                    return $.map(data, function (row) {
                        return {
                            data: row, value: row.Nombre, result: row.Nombre

                        }
                    });
                },
                formatItem: function (item) {

                    return item.Nombre;
                }

            });

            $('#txtVendedor').result(function (event, item, formatted) {
                $('#hvendedor').val("");
                if (item) {
                    $('#hvendedor').val(item.Codigo);
                }
            });
        });

    </script>
</body>
</html>
