﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Pedidos_Site.Mantenimiento.pedido.grideditdetalle" Codebehind="grideditdetalle.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
     <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <script src="../../js/JSMaps.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <title></title>
    <script>
        $(document).ready(function () {
            var res=0;
            res='';
            if (res.length === 0) {
                $(".columna").css("display","none");
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="cz-form-box">
         <div class="cz-form-box-content">
               <div id="cz-form-box-content-title">
                    <img id="cz-form-box-content-title-icon" src="../../imagery/all/icons/pedidoeditados.png"
                        alt="<>" />
                    <div id="cz-form-box-content-title-text">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_DETALLE_PEDIDOS_EDITADOS)%></p>
                                                     
                    </div>
                </div>
                
              <input type="button" id="cz-form-box-atras" class="cz-form-content-input-button cz-form-content-input-button-image form-button cz-util-right"
                    value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VOLVER)%>" />
                            
            </div>
     <div class="cz-form-box-content">
                <div class="form-grid-box">
                    <div class="form-grid-table-outer">
                        <div class="form-grid-table-inner">
                            <div id="divGridView" runat="server">
                                 <asp:Repeater ID="grdMant" runat="server">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>  
                            <th  nomb="CODIGOEDICION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO_EDITADO)%>
                            </th>
                          <th  nomb="CODIGOPEDIDO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO)%>
                            </th>
                            <th  nomb="CODIGODETALLE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_DETALLE)%>
                            </th>
                            <th  nomb="PRODUCTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRODUCTO)%>
                            </th>
                            <th  nomb="NOMBRE_PRESENTACION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MANT_PRESENTACION)%>
                            </th>
                            <th  nomb="CANTIDAD_PRESENTACION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD_PRESENTACION)%>
                            </th>
                            <th  nomb="UNIDAD_FRACCIONAMIENTO" <%=liValidarVisible %>>
                               <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_UNID_FRACIONAMIENTO)%>
                            </th>
                            <th  nomb="CANTIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CANTIDAD)%>
                            </th>
                           <th  nomb="FRACCION" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FRACCION)%>
                            </th>
                            <th  nomb="PRECIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PRECIO)%>
                            </th>
                            <th  nomb="DESC">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESC)%>
                            </th>
                            <th  nomb="MONTO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_MONTO)%>
                            </th>
                            <th  nomb="FLETE" <%=liValidarVisible %>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FLETE)%>
                            </th>
                            <th  nomb="DESCRIPCION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DESCRIPCION)%>
                            </th>
                             <% if (Mbm == "1")
                              { %>
                            <th  nomb="BONIFICACION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BONIFICACIONREP)%>
                            </th>
                            
                            <%} %>
                           
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                   <td>
                        <%# Eval("ID_EDICION")%>
                    </td>
                    <td>
                        <%# Eval("PED_PK")%>
                    </td>
                    <td>
                        <%# Eval("DET_PK")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>

                    <td <%=liValidarVisible %>>
                        <%# Eval("CANTIDAD_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                    <td>
                        <%# Eval("DET_CANTIDAD")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DET_CANTIDAD_FRACCION")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIOBASE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCUENTO")%>
                    </td>
                    
                    <td>
                        <%# Eval("DET_MONTO")%>
                    </td>
                     
                      <td <%=liValidarVisible %>>
                        <%# Eval("DET_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCRIPCION")%>
                    </td>
                    <% if (Mbm == "1")
                        { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>
                     <%} %>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                     <td>
                        <%# Eval("ID_EDICION")%>
                    </td>
                      <td>
                        <%# Eval("PED_PK")%>
                    </td>
                    <td>
                        <%# Eval("DET_PK")%>
                    </td>
                    <td>
                        <%# Eval("PRO_NOMBRE")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("NOMBRE_PRESENTACION")%>
                    </td>

                    <td <%=liValidarVisible %>>
                        <%# Eval("CANTIDAD_PRESENTACION")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("UNIDAD_FRACCIONAMIENTO")%>
                    </td>
                    <td>
                        <%# Eval("DET_CANTIDAD")%>
                    </td>
                    <td <%=liValidarVisible %>>
                        <%# Eval("DET_CANTIDAD_FRACCION")%>
                    </td>
                    <td>
                        <%# Eval("DET_PRECIOBASE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCUENTO")%>
                    </td>
                    
                    <td>
                        <%# Eval("DET_MONTO")%>
                    </td>
                     
                      <td <%=liValidarVisible %>>
                        <%# Eval("DET_FLETE")%>
                    </td>
                    <td>
                        <%# Eval("DET_DESCRIPCION")%>
                    </td>
                    <% if (Mbm == "1")
                        { %>
                    <td>
                        <%# Eval("DET_BONIFICACION")%>
                    </td>
                     <%} %>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
          </div>
    </div>
 
    </form>
    <script>

        $(document).ready(function () {

            function obtenerDatosGrilla() {
                return $.ajax({
                    type: "POST",
                    data: JSON.stringify(getParametros()),
                    url: "griddet.aspx",
                    dataType: "html",
                    async: false
                }).responseText;
            }

            $('#cz-form-box-atras').click(function () {
     
                    parent.history.back();
             
            });

        });

    </script>
</body>
</html>
