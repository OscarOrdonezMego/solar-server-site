﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Mantenimiento.pedido.actividad_actividad_grid" Codebehind="grid.aspx.cs" %>

<%--
@001 GMC 13/04/2015 Se muestra Tipo Pedido
@002 GMC 14/04/2015 Se muestra Dirección de Despacho
@003 GMC 18/04/2015 Ajustes para validar visualización de Dirección Despacho
@004 GMC 05/05/2015 Ajustes para validar visualización de Almacén
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet" />
    <title></title>
    <script>
       
    </script>
</head>
<body>
        <form id="form1" runat="server">
    <div id="divGridView" runat="server">
        <asp:Repeater ID="grdMant" runat="server" OnItemDataBound="GridViewRowEventHandler">
            <HeaderTemplate>
                <table style="width: 100%" class="grilla table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 40px;" class="center">
                                <input id="ChkAll" name="chkSelectAll" value="<%# Eval("ID") %>" type="checkbox">
                            </th>
                            <th nomb="CODIGO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CODIGO_PEDIDO)%>
                            </th>
                            <th nomb="NOMBRE">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                            </th>
                             <th nomb="NOMBREGRUPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                            </th>
                            <th nomb="TIPOACTIVIDAD">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CLIENTE)%>
                            </th>
                            <th nomb="FECHAINICIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DIRECCION)%>
                            </th>
                            <%--@002 I--%>
                            <th nomb="DIR_DESPACHO" <%=visibleDireccionDespacho%> >
                                Dirección Despacho
                            </th>
                            <%--@002 F--%>
                            <%--@004 I--%>
                            <th nomb="ALM_NOMBRE" <%=visibleColumnaAlmacen%> >
                                Almacén
                            </th>
                            <%--@004 F--%>
                            <th nomb="FECHAINICIO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAREGISTRO)%>
                            </th>
                             <th nomb="FECHAEDICION">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_FECHAEDICION)%>
                            </th>
                            <th nomb="CONDICION" <%=visibleCondicion%>>
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_CONDICION)%>
                            </th>
                            <%--@001 I--%>
                            <th nomb="TIPO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TIPO)%>
                            </th>
                            <%--@001 F--%>
                            <th nomb="MONTO_SOLES">
                                Monto Soles
                            </th>
                            <th nomb="MONTO_DOLARES">
                                Monto Dólares
                            </th>
                            <th nomb="FECHAINICIO"  <%=visibleColumnaAlmacen%>>
                                Total Flete
                            </th>
                              
                            <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                            {%>
                                <th style="width: 40px;">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_EDITAR)%>
                                </th>
                            <%} %>
                             <th style="width: 40px;">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_DETALLE)%>
                             </th>
                             <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                             {%>
                                <th style="width: 40px;">
                                    <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ELIMINAR)%>
                                </th>
                             <%} %>
                            

                            <th nomb="ESTADO">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_ESTADO)%>
                            </th>
                            <th  style="width: 40px;" class="centrarimagen">
                                <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_PEDIDOS_EDITADOS)%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("NOM_USUARIO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("CLI_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PED_DIRECCION")%>
                    </td>
                    <%--@002 I--%>
                    <td <%=visibleDireccionDespacho%> > <%--@003 I/F--%>
                        <%# Eval("PED_DIRECCION_DESPACHO")%>
                    </td>

                    <td <%=visibleColumnaAlmacen%> >
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <%--@004 F--%>
                    <td>
                        <%# Eval("PED_FECREGISTRO")%>
                    </td>
                        <td>
                        <%# Eval("PED_FECEDICION")%>
                    </td>
                    <td <%=visibleCondicion%>>
                        <%# Eval("CONDVTA_NOMBRE")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("TIPO_NOMBRE")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <%# Eval("PED_MONTOTOTAL_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("PED_MONTOTOTAL_DOLARES")%>
                    </td>
                     <td <%=visibleColumnaAlmacen%>>
                        <%# Eval("TOTAL_FLETE")%>
                    </td>

                     
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                       { %>
                   <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" alt="Editar" /></a>
                    </td>
                    <%} %>
                    <td>
                        <a  style="cursor: pointer;" 
                             href="pedidodet.aspx?idpedido=<%# Eval("ID") %>&idalmacen=<%# Eval("ALM_PK") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle" alt="Detalle" /></a>
                    </td>
                   <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                     { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                   <%} %>
               
                    <td id="procesado" runat="server">
                        <%# Eval("ESTADO_PROCESADO")%>                        
                    </td>
                     <td> 
                        <a id="ediciones"  style="cursor: pointer;" 
                             href="gridedit.aspx?idpedido=<%# Eval("ID") %>">
                            <center><img src="../../imagery/all/icons/pedidoeditados.png" border="0"title="Pedidos Editados" alt="Pedidos Editados"/></center>
                       </a>
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alt">
                    <td>
                        <input id="<%# Eval("ID") %>" value="<%# Eval("ID") %>" type="checkbox">
                    </td>
                    <td>
                        <%# Eval("id")%>
                    </td>
                    <td>
                        <%# Eval("NOM_USUARIO")%>
                    </td>
                    <td>
                        <%# Eval("NOMBREGRUPO")%>
                    </td>
                    <td>
                        <%# Eval("CLI_NOMBRE")%>
                    </td>
                    <td>
                        <%# Eval("PED_DIRECCION")%>
                    </td>
                    <%--@002 I--%>
                    <td <%=visibleDireccionDespacho%> > <%--@003 I/F--%>
                        <%# Eval("PED_DIRECCION_DESPACHO")%>
                    </td>
                    <%--@002 F--%>
                    <%--@004 I--%>
                    <td <%=visibleColumnaAlmacen%> >
                        <%# Eval("ALM_NOMBRE")%>
                    </td>
                    <%--@004 F--%>
                    <td>
                        <%# Eval("PED_FECREGISTRO")%>
                    </td>
                    <td>
                        <%# Eval("PED_FECEDICION")%>
                    </td>
                    <td <%=visibleCondicion%>>
                        <%# Eval("CONDVTA_NOMBRE")%>
                    </td>
                    <%--@001 I--%>
                    <td>
                        <%# Eval("TIPO_NOMBRE")%>
                    </td>
                    <%--@001 F--%>
                    <td>
                        <%# Eval("PED_MONTOTOTAL_SOLES")%>
                    </td>
                    <td>
                        <%# Eval("PED_MONTOTOTAL_DOLARES")%>
                    </td>
                    <td <%=visibleColumnaAlmacen%>>
                        <%# Eval("TOTAL_FLETE")%>
                    </td>                  
                     
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.EDITAR))
                       {%>
                    <td>
                        <a class="editItemReg" style="cursor: pointer;" data-toggle="modal" cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar" alt="Editar" /></a>
                    </td>
                    <%} %>
                    <td>
                             <a  style="cursor: pointer;" 
                             href="pedidodet.aspx?idpedido=<%# Eval("ID") %>&idalmacen=<%# Eval("ALM_PK") %>">
                            <img src="../../imagery/all/icons/detalle.png" border="0" title="Detalle " /></a>
                    </td>
                    <%if (fnValidarPerfilMenu(lsCodMenu, Model.Enumerados.FlagPermisoPerfil.ELIMINAR))
                      { %>
                    <td>
                        <a role="button" data-toggle="modal" style="cursor: pointer;" class="delItemReg form-icon"
                            cod="<%# Eval("ID") %>">
                            <img src="../../imagery/all/icons/eliminar.png" border="0" title="Borrar" alt="Borrar" /></a>
                    </td>
                  <%} %>
               
                    <td id="procesado" runat="server">
                        <%# Eval("ESTADO_PROCESADO")%>                        
                    </td>
                     <td>
                        <a  style="cursor: pointer;" 
                             href="gridedit.aspx?idpedido=<%# Eval("ID") %>">
                          <center><img src="../../imagery/all/icons/pedidoeditados.png" border="0" title="Pedidos Editados" alt="Pedidos Editados" /></center> </a>
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div id="divGridViewPagintator" class="paginator-table" runat="server">
        <div class="paginator-table-outer">
            <div class="paginator-table-inner">
                <div class="paginator-data">
                    <div class="cz-page-ant">
                        <p class="pagina-direccion">
                            <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="anterior"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-now">
                        <a>Pagina</a>
                        <p class="pagina-actual">
                            <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                        </p>
                        <p class="pagina-direccion">
                            <span class="pagina-data">de </span>
                            <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                        </p>
                    </div>
                    <div class="cz-page-des">
                        <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="siguiente"></asp:Label>
                    </div>
                </div>
                <div class="paginator-data-search">
                    <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                    <p>
                        buscando resultados</p>
                </div>
            </div>
        </div>
        <div class="cz-table-expand">
            <div class="cz-table-expand-close-x">
                ×</div>
        </div>
    </div>
    </form>
</body>
</html>