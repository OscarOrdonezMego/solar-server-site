﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Model.bean;
using Controller;
using Model;

namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class griddet : PageController
    {
        public String xIDpedido = "";
        public string Mbm = "";
        public static String lsCodMenu = "MPE";
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        public static Int32 itemsPedido;
        public static Double totalMontoPedidoSoles;
        public static Double totalMontoPedidoDolares;
        public Boolean bonificacion = false;
        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            String xid = dataJSON["id"].ToString();
            String xcodigo = dataJSON["codigo"].ToString();
            String xproducto = dataJSON["producto"].ToString();
            String xflag = dataJSON["flag"].ToString();
            String xPresentacion = dataJSON["presentacion"].ToString();

            String xpagina = dataJSON["pagina"].ToString();
            String xfilas = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_PAGINACION).Valor;// dataJSON["filas"].ToString();

            Mbm = ManagerConfiguration.nBonificacionManual;
            xIDpedido = xid;
            if (liRespuestaint == 1)
            {
                PedidoDetalleBean loPedidoDetalleBean = new PedidoDetalleBean();
                loPedidoDetalleBean.Pagina = Int32.Parse(xpagina);
                loPedidoDetalleBean.TotalFila = Int32.Parse(xfilas);
                loPedidoDetalleBean.id = xcodigo;
                loPedidoDetalleBean.idDET = xid;
                loPedidoDetalleBean.PRO_NOMBRE = xproducto;
                loPedidoDetalleBean.NOMBRE_PRESENTACION = xPresentacion;
                loPedidoDetalleBean.Flag = xflag;
                PaginatePedidoDetalleBean paginate1 = ProductoPresentacionController.paginarBuscarListaPedidoPresentacion(loPedidoDetalleBean);

                var totalpedido = 0.0;

                foreach (var detalle in paginate1.lstResultados)
                {
                    totalpedido = totalpedido + Convert.ToDouble(detalle.DET_MONTO_SOLES);
                }

                itemsPedido = Convert.ToInt32(paginate1.lstResultados.Count.ToString());
                totalMontoPedidoSoles = Convert.ToDouble(totalpedido.ToString());

                this.numeroItems.Value = itemsPedido.ToString();
                this.montoPedido.Value = totalMontoPedidoSoles.ToString();

                if ((Int32.Parse(xpagina) > 0) && (Int32.Parse(xpagina) <= paginate1.totalPages))
                {
                    this.lbTpagina.Text = paginate1.totalPages.ToString();
                    this.lbPagina.Text = xpagina;

                    if (Int32.Parse(xpagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(xpagina) == paginate1.totalPages)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<PedidoDetalleBean> lst = paginate1.lstResultados;
                    if (lst.Count > 0) bonificacion = lst[0].DET_BONIFICACION.Equals("0") ? false : true;

                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }

            }
            else
            {
                //PaginatePedidoBean paginate = PedidoController.paginarBuscar
                PaginatePedidoDetalleBean paginate = PedidoDetalleController.paginarBuscar(xid, xcodigo, xproducto, xflag, xpagina, xfilas);

                var totalpedido = 0.0;

                foreach (var detalle in paginate.lstResultados)
                {
                    totalpedido = totalpedido + Convert.ToDouble(detalle.DET_MONTO_SOLES);
                }

                itemsPedido = Convert.ToInt32(paginate.lstResultados.Count.ToString());
                totalMontoPedidoSoles = Convert.ToDouble(totalpedido.ToString());

                this.numeroItems.Value = itemsPedido.ToString();
                this.montoPedido.Value = totalMontoPedidoSoles.ToString();

                if ((Int32.Parse(xpagina) > 0) && (Int32.Parse(xpagina) <= paginate.totalPages))
                {
                    this.lbTpagina.Text = paginate.totalPages.ToString();
                    this.lbPagina.Text = xpagina;

                    if (Int32.Parse(xpagina) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(xpagina) == paginate.totalPages)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    List<PedidoDetalleBean> lst = paginate.lstResultados;

                    if (lst.Count > 0) bonificacion = lst[0].DET_BONIFICACION.Equals("0") ? false : true;
                    grdMant.DataSource = lst;
                    grdMant.DataBind();

                }
                else
                {
                    String htmlNoData = "<div class='gridNoData'>" +
                                        "<img src='../../images/icons/grid/ico_grid_nodata.png' />" +
                                        "<p>No se encontraron datos para mostrar</p>" +
                                        "</div>";

                    this.divGridView.InnerHtml = htmlNoData;
                    this.divGridViewPagintator.Visible = false;
                }
            }

        }
    }
}