﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Controller;
using Model.bean;
using Newtonsoft.Json;
using Model;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class nuevopedidodet : PageController
    {
        public string Mbm = "";
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        public static List<AlmacenBean> lolstAlmacenBean = null;
        public static String liValidarAlmacenOcultar = "";
        public static String liValidarBonificacionOcultar = "";
        public static Int32 liValidarAlmacenInt = 0;
        public static Int32 liValidarBonificacion = 0;
        public static Int32 flete = 0;
        public static Int32 ioFraccionamientoMaximoDecimales = 0;

        protected override void initialize()
        {
            CODIGOPEDIDO.Value = Request.QueryString["idAlmacen"].ToString();
            flete = 0;

            IniciarTipoMoneda();
            MtxtPrecioBase.Disabled = true;

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;

                String loValorFraccionamientoDecimal = fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MAXIMO_NUMERO_DECIMALES).Valor;
                ioFraccionamientoMaximoDecimales = Convert.ToInt32(loValorFraccionamientoDecimal);
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
                ioFraccionamientoMaximoDecimales = 0;
            }

            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarAlmacenInt = Operation.flagACIVADO.ACTIVO;
                liValidarAlmacenOcultar = String.Empty;
            }
            else
            {
                liValidarAlmacenInt = Operation.flagACIVADO.DESACTIVADO;
                liValidarAlmacenOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_AUTOMATICA).Valor.Equals(Enumerados.FlagHabilitado.T.ToString())
                || fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresConfiguracion.CFG_BONIFICACION_MANUAL).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                liValidarBonificacion = Operation.flagACIVADO.ACTIVO;
                liValidarBonificacionOcultar = String.Empty;
            }
            else
            {
                liValidarBonificacion = Operation.flagACIVADO.DESACTIVADO;
                liValidarBonificacionOcultar = Operation.CSS.STYLE_DISPLAY_NONE;
            }
            if (ManagerConfiguration.descuento == "1")
            {
                lblporcentaje.Visible = true;
            }
            else
            {
                lblporcentaje.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_NUEVODETALLE);
                saveReg.Value = IdiomaCultura.getMensaje(IdiomaCultura.WEB_GRABAR);

                Mbm = ManagerConfiguration.nBonificacionManual;
                cargarTipoDocumento();

                if (dataJSON != null)
                {
                    if (liRespuestaint == 1)
                    {
                        myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARDETALLE);

                        String Ids;
                        Ids = dataJSON["codigo"].ToString();
                        List<PedidoDetalleBean> loLstPedidoDetalleBean = ProductoPresentacionController.fnBuscarPedidoDetallePresentacion(Int32.Parse(Ids.Split('|')[1].ToString()));
                        for (Int32 i = 0; i < loLstPedidoDetalleBean.Count; i++)
                        {

                            hIDpedido.Value = loLstPedidoDetalleBean[i].PED_PK;
                            hIDpedidoDET.Value = loLstPedidoDetalleBean[i].id;
                            MtxtProducto.Value = loLstPedidoDetalleBean[i].PRO_NOMBRE;
                            mhCodigo.Value = loLstPedidoDetalleBean[i].PRO_CODIGO;
                            List<ProductoPresentacionBean> loLstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(loLstPedidoDetalleBean[i].PRO_CODIGO);
                            if (loLstProductoPresentacionBean.Count > 0)
                            {
                                for (Int32 j = 0; j < loLstProductoPresentacionBean.Count; j++)
                                {
                                    PDtxtpresentacion.Items.Insert(j, loLstProductoPresentacionBean[j].Nombre);
                                    PDtxtpresentacion.Items[j].Value = loLstProductoPresentacionBean[j].CodigoPresentacion;
                                }
                                for (Int32 s = 0; s < loLstProductoPresentacionBean.Count; s++)
                                {
                                    if (PDtxtpresentacion.Items[s].Value.Equals(loLstPedidoDetalleBean[i].Cod_Presentacion))
                                    {
                                        PDtxtpresentacion.Items[s].Selected = true;
                                    }
                                }
                            }
                            List<AlmacenBean> lolstAlmacenBeanEditar = PedidoDetalleController.fnBuscarAlmacen(loLstPedidoDetalleBean[i].Cod_Presentacion);
                            if (lolstAlmacenBeanEditar.Count > 0)
                            {
                                for (Int32 j = 0; j < lolstAlmacenBeanEditar.Count; j++)
                                {
                                    PDcboaAlmacenes.Items.Insert(j, lolstAlmacenBeanEditar[j].nombre);
                                    PDcboaAlmacenes.Items[j].Value = lolstAlmacenBeanEditar[j].codigo;
                                }
                                for (Int32 s = 0; s < lolstAlmacenBeanEditar.Count; s++)
                                {
                                    if (PDcboaAlmacenes.Items[s].Value.Equals(loLstPedidoDetalleBean[i].ALM_PK.ToString()))
                                    {
                                        PDcboaAlmacenes.Items[s].Selected = true;
                                    }
                                }

                            }
                            List<PresentacionBean> loPresentacionBean2 = PedidoDetalleController.fnBuscarCantidadPresentacionLista(Int32.Parse(loLstPedidoDetalleBean[i].Cod_Presentacion));
                            for (Int32 p = 0; p < loPresentacionBean2.Count; p++)
                            {
                                Int32 cantidad = Int32.Parse(loPresentacionBean2[p].Cantidad);
                                Int32 fraccionamiento = Int32.Parse(loPresentacionBean2[p].UnidadFrancionamiento);
                                Int32 Div = cantidad / fraccionamiento;
                                List<PresentacionBean> lis = new List<PresentacionBean>();
                                PresentacionBean obj = null;
                                for (Int32 t = 0; t < Div; t++)
                                {
                                    obj = new PresentacionBean();
                                    obj.Total = fraccionamiento * t;
                                    obj.Resulado = obj.Total + "" + obj.PRO_UNIDADDEFECTO + "(" + obj.Total + "/" + cantidad + ")";
                                    if (cantidad != obj.Total)
                                    {
                                        PDcboFraccionamiento.Items.Insert(t, obj.Resulado);
                                        PDcboFraccionamiento.Items[t].Value = obj.Total.ToString();
                                        if (PDcboFraccionamiento.Items[t].Value.Equals(loLstPedidoDetalleBean[i].Cant_fraccionada))
                                        {
                                            PDcboFraccionamiento.Items[t].Selected = true;
                                        }
                                    }

                                }

                            }
                            if (loLstPedidoDetalleBean[i].DET_FLETE.Equals("0"))
                            {
                                flete = 0;
                            }
                            else
                            {
                                flete = 1;
                            }
                            pdTxtFlete.Value = loLstPedidoDetalleBean[i].DET_FLETE;
                            MtxtCantidad.Value = loLstPedidoDetalleBean[i].DET_CANTIDAD;
                            MtxtPrecioSoles.Value = loLstPedidoDetalleBean[i].PrecioPresentacionSoles;
                            MtxtPrecioDolares.Value = loLstPedidoDetalleBean[i].PrecioPresentacionDolares;

                            cboMoneda.SelectedValue = loLstPedidoDetalleBean[i].TipoMoneda;
                            MtxtPrecioBase.Value = (loLstPedidoDetalleBean[i].TipoMoneda == "S") ? loLstPedidoDetalleBean[i].PrecioPresentacionSoles : loLstPedidoDetalleBean[i].PrecioPresentacionDolares;
                            
                            MtxtDescuento.Value = loLstPedidoDetalleBean[i].DET_DESCUENTO;
                            MtxtDescripcion.Value = loLstPedidoDetalleBean[i].DET_DESCRIPCION;
                            MtxtBonificacion.Value = loLstPedidoDetalleBean[i].DET_BONIFICACION;
                            MtxtProducto.Disabled = true;


                        }
                    }
                    else
                    {
                        myModalLabel.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_EDITARDETALLE);
                        PedidoDetalleBean usuario = PedidoDetalleController.info(dataJSON["codigo"]);

                        String Ids;
                        Ids = dataJSON["codigo"].ToString();

                        hIDpedido.Value = Ids.Split('|')[0].ToString();
                        hIDpedidoDET.Value = Ids.Split('|')[1].ToString();
                        MtxtProducto.Value = usuario.PRO_NOMBRE.ToString();
                        mhCodigo.Value = usuario.PRO_CODIGO.ToString();
                        MtxtCantidad.Value = usuario.DET_CANTIDAD.ToString();
                        MtxtPrecioSoles.Value = usuario.DET_PRECIO_SOLES.ToString();
                        MtxtPrecioDolares.Value = usuario.DET_PRECIO_DOLARES.ToString();

                        cboMoneda.SelectedValue = usuario.TipoMoneda;
                        MtxtPrecioBase.Value = (usuario.TipoMoneda == "S") ? usuario.DET_PRECIO_SOLES.ToString() : usuario.DET_PRECIO_DOLARES.ToString();
                        
                        MtxtDescuento.Value = usuario.DET_DESCUENTO.ToString();
                        MtxtDescripcion.Value = usuario.DET_DESCRIPCION.ToString();
                        MtxtBonificacion.Value = usuario.DET_BONIFICACION.ToString();
                        MtxtProducto.Disabled = true;
                        // List<AlmacenBean> lolstAlmacenBeanEditar2 = PedidoDetalleController.fnListaAlmacenActivo();
                        List<AlmacenBean> lolstAlmacenBeanEditar = PedidoDetalleController.fnBuscarAlmacen(Convert.ToString(usuario.Pro_pk));
                        if (lolstAlmacenBeanEditar.Count > 0)
                        {
                            for (Int32 j = 0; j < lolstAlmacenBeanEditar.Count; j++)
                            {
                                PDcboaAlmacenes.Items.Insert(j, lolstAlmacenBeanEditar[j].nombre);
                                PDcboaAlmacenes.Items[j].Value = lolstAlmacenBeanEditar[j].codigo;
                            }
                            for (Int32 s = 0; s < lolstAlmacenBeanEditar.Count; s++)
                            {
                                if (PDcboaAlmacenes.Items[s].Value.Equals(CODIGOPEDIDO.Value))
                                {
                                    PDcboaAlmacenes.Items[s].Selected = true;
                                    pAlmacen.Text = String.Empty;
                                    pAlmacen.Text = "Almacen * " + Math.Round(double.Parse(lolstAlmacenBeanEditar[s].Stock.ToString()), 0).ToString() + " UND";
                                }
                            }

                        }
                    }
                }
                else
                {
                    hIDpedido.Value = Request.QueryString["idpedido"].ToString();
                    hIDpedidoDET.Value = "";

                }

            }
        }


        private void cargarTipoDocumento()
        {
            int item = 0;

            item++;
        }
        [WebMethod]
        public static String fnBuscarPresentaciones(String codigo, String tipo)
        {
            try
            {
                String nombre = "--Seleccione--";
                String option = "";
                List<ProductoPresentacionBean> lolstProductoPresentacionBean = null;
                if (tipo.Equals("PRE"))
                {
                    lolstProductoPresentacionBean = ProductoPresentacionController.buscarPresentacionProducto(codigo);
                }
                if (tipo.Equals("ALM"))
                {
                    lolstProductoPresentacionBean = PedidoDetalleController.fnBuscarPresentacionesConStock(codigo);
                }
                if (lolstProductoPresentacionBean.Count > 0)
                {
                    option = "<option value=\"\" >" + nombre + " </option>";
                    for (Int32 i = 0; i < lolstProductoPresentacionBean.Count; i++)
                    {
                        option += "<option cant=" + lolstProductoPresentacionBean[i].Cantidad + " value=" + lolstProductoPresentacionBean[i].CodigoPresentacion + " >" + lolstProductoPresentacionBean[i].Nombre + " " + lolstProductoPresentacionBean[i].Cantidad + "</option>";
                    }
                    return option;
                }
                else
                {
                    option = "S";
                    return option;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }
        [WebMethod]
        public static PedidoDetalleBean fnBuscarPrecioPresentacion(String codigo)
        {
            //return Math.Round(decimal.Parse(ProductoPresentacionController.fnBuscarPrecionPresentacion(codigo,moneda)), 2).ToString();

            return ProductoPresentacionController.fnBuscarPrecionPresentacion(codigo);
        }

        [WebMethod]
        public static String fnBuscarAlmacen(String codigo)
        {
            try
            {
                String nombre = "--Seleccione--";
                String option = "";
                lolstAlmacenBean = PedidoDetalleController.fnBuscarAlmacen(codigo);
                if (lolstAlmacenBean.Count > 0)
                {
                    option = "<option value=\"\" >" + nombre + " </option>";
                    for (Int32 i = 0; i < lolstAlmacenBean.Count; i++)
                    {
                        option += "<option value=" + lolstAlmacenBean[i].codigo + " >" + lolstAlmacenBean[i].nombre + "</option>";
                    }
                    return option;
                }
                else
                {
                    option = "S";
                    return option;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


        }
        [WebMethod]
        public static String fnBuscarStockAlmacen(String codigo, String codpre)
        {
            String lsRpta = "";
            PresentacionBean loPresentacionBean = null;
            if (codpre != null)
            {
                Int32 liPrePk = Int32.Parse(codpre);
                loPresentacionBean = PedidoDetalleController.fnBuscarCantidadPresentacion(liPrePk);
            }
            if (lolstAlmacenBean != null)
            {
                for (Int32 i = 0; i < lolstAlmacenBean.Count; i++)
                {
                    if (lolstAlmacenBean[i].codigo.Equals(codigo))
                    {
                        if (codpre == null)
                        {
                            lsRpta = "Almacen * " + lolstAlmacenBean[i].Stock + " UND";
                        }
                        else
                        {
                            lsRpta = "Almacen * " + Transformar(loPresentacionBean.Cantidad, lolstAlmacenBean[i].Stock, loPresentacionBean.PRO_UNIDADDEFECTO);
                        }
                    }
                }
            }

            return lsRpta;
        }
        private static String Transformar3(String cantidad, String stock, String nombrepre)
        {
            decimal locant = decimal.Parse(cantidad);
            decimal lostock = decimal.Parse(stock);
            decimal totaldiv = Math.Round(lostock / locant, 2);
            String[] separa = totaldiv.ToString().Split('.', ' ', ',');
            String paquete = "";
            String unidades = "";
            for (Int32 i = 0; i < separa.Length; i++)
            {
                paquete = separa[0].ToString();
                unidades = separa[1].ToString();
            }
            String respuesta = paquete + " - " + unidades + " " + nombrepre;
            return respuesta;
        }
        private static String Transformar(String cantidad, String stock, String PRO_UNIDADDEFECTO)
        {
            String resultado = "";
            long PAQUETE = 0;
            long UNID = 0;
            long CANTIDAD = long.Parse(cantidad);
            long STOCK = long.Parse(Math.Round(decimal.Parse(stock)).ToString());
            if (STOCK == 0)
            {
                resultado = "0";
            }
            else
            {
                if (CANTIDAD < 1)
                {
                    resultado = "1";
                }
                else if (CANTIDAD == 1)
                {
                    resultado = "1 " + PRO_UNIDADDEFECTO;
                }
                else
                {
                    PAQUETE = STOCK / CANTIDAD;
                    UNID = STOCK % CANTIDAD;
                    resultado = PAQUETE + "-" + UNID + " " + PRO_UNIDADDEFECTO;
                }
            }

            return resultado;


        }
        [WebMethod]
        public static String fnBuscarListaPrecion(String codigo)
        {
            Int32 liPrePk = Int32.Parse(codigo);
            List<ListaPrecioBean> loLstListaPrecioBean = PedidoDetalleController.fnBuscarListaPrecio(liPrePk);
            String json = JsonConvert.SerializeObject(loLstListaPrecioBean);
            return json;
        }
        [WebMethod]
        public static String fnBuscarCantidadPresentacion(String codigo)
        {
            return GETBuscarCantidadPresentacion(codigo);
        }
        private static String GETBuscarCantidadPresentacion(String codigo)
        {
            Int32 liPrePk = Int32.Parse(codigo);
            PresentacionBean loPresentacionBean = PedidoDetalleController.fnBuscarCantidadPresentacion(liPrePk);
            Int32 cantidad = Int32.Parse(loPresentacionBean.Cantidad);
            Int32 fraccionamiento = Int32.Parse(loPresentacionBean.UnidadFrancionamiento);
            Int32 Div = cantidad / fraccionamiento;
            List<PresentacionBean> lis = new List<PresentacionBean>();
            PresentacionBean obj = null;
            for (Int32 i = 0; i < Div; i++)
            {
                obj = new PresentacionBean();
                obj.Total = fraccionamiento * i;
                obj.Resulado = obj.Total + "" + obj.PRO_UNIDADDEFECTO + "(" + obj.Total + "/" + cantidad + ")";
                if (cantidad != obj.Total)
                {
                    lis.Add(obj);
                }

            }
            String json = JsonConvert.SerializeObject(lis);
            return json;
        }
        [WebMethod]
        public static String fnBuscarStockPresentacion(String codigo)
        {
            PresentacionBean loPresentacionBean = PedidoDetalleController.fnBuscarStockPresentacion(Int32.Parse(codigo));
            return Transformar(loPresentacionBean.Cantidad, loPresentacionBean.Stock, loPresentacionBean.PRO_UNIDADDEFECTO);
        }

        [WebMethod]
        public static ProductoBean fnBuscarProducto(String codigo)
        {
            return ProductoController.infoxcod(codigo);
        }

        [WebMethod]
        public static TipoCambioBean fnBuscarTipoCambio(String moneda, String pedido)
        {
            TipoCambioBean tc = new TipoCambioBean();
            PedidoBean objped = PedidoController.info(pedido);
            if (objped != null)
            {
                if (moneda == "S")
                {
                    if (objped.ValorTipoCambioSoles != string.Empty)
                    {
                        tc.valor = objped.ValorTipoCambioSoles;
                    }
                }
                else if (moneda == "D")
                {
                    if (objped.ValorTipoCambioDolares != string.Empty)
                    {
                        tc.valor = objped.ValorTipoCambioDolares;
                    }
                }
            }
            //if (moneda == "S") moneda = "0";
            //if (moneda == "D") moneda = "1";
            //TipoCambioBean obj = TipoCambioController.buscarTipoCambio(moneda);
            //if (obj != null)
            //{
            //    tc.valor = obj.valor;
            //}
            return tc;
        }

        private void IniciarTipoMoneda()
        {
            cboMoneda.DataTextField = "nombre";
            cboMoneda.DataValueField = "codigo";
            cboMoneda.DataSource = MantGeneralController.paginarBuscar("", "", "12", "T", 1, 10000).lstResultados;
            cboMoneda.DataBind();
            //cboMoneda.Items.Insert(0, new ListItem("Ninguno", "-1"));
        }
    }
}