﻿using System;
using Model.bean;
using Controller;
using Model;
using System.Data;

namespace Pedidos_Site.Mantenimiento.pedido
{
    public partial class gridedit : PageController
    {
        public Boolean bonificacion = false;
        public string Mbm = "";
        public static Int32 ioRespuesta;
        public static String ioRespuestaString;

        protected override void initialize()
        {
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                ioRespuesta = Operation.flagACIVADO.ACTIVO;
                ioRespuestaString = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                ioRespuesta = Operation.flagACIVADO.DESACTIVADO;
                ioRespuestaString = Operation.CSS.STYLE_DISPLAY_NONE;
            }


            String xPedido = "";
            xPedido = Request.QueryString["idpedido"].ToString();

            Mbm = ManagerConfiguration.nBonificacionManual;
            DataTable odt;
            if (ioRespuesta == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                odt = ReporteController.PedidoEdicionList(xPedido);
            }
            else
            {
                odt = ReporteController.PedidoEdicionList(xPedido);
            }



            grdMant.DataSource = odt;
            grdMant.DataBind();
        }
    }
}