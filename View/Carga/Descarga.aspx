﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits=" Pedidos_Site.Carga.Carga_Descarga" Codebehind="Descarga.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../js/Validacion.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/jsmodule.js" type="text/javascript"></script>
    <link href="../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../js/jquery.autocomplete.js" type="text/javascript"></script>
    <link href="../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../js/PopupCalendar.js" type="text/javascript"></script>
    <script src="../js/JSMaps.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../js/cz_main.js" type="text/javascript"></script>
    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 60%;
        }

            #sortable li {
                margin: 0 3px 3px 3px;
                padding: 0.4em;
                padding-left: 1.5em;
                font-size: 1.4em;
                height: 18px;
            }

                #sortable li span {
                    position: absolute;
                    margin-left: -1.3em;
                }

        .cambiar-contenedor {
            border: gray;
            height: 263px;
            box-shadow: 0 0 5px #B6B6B6;
            float: left;
            margin: 6px;
            overflow: auto;
            width: 460px;
        }

        .sortable_hijo.ui-sortable-handle {
            height: 33px;
            border: 0px solid;
            border-bottom: 1px solid;
            width: 434px;
            margin: 5px;
            padding: 2px;
            background-color: white;
        }

        .tamaño-ancho-alto-on {
            width: 480px initial;
            height: 40px initial;
            box-shadow: 0 0 34px #ADA9A9;
        }

        .tamaño-ancho-alto-off {
            width: 434px initial;
            height: 30px initial;
        }

        .imgC {
            margin-top: 11px;
            width: 25px;
        }

        .imgD {
            margin-top: 11px;
            width: 25px;
        }

        .contenedor-Combo {
            box-shadow: 0 0 5px #B6B6B6;
            padding: 15px;
            margin: 6px;
        }

        .contenedor-boton {
            border: gray;
            width: 943px;
            height: 40px;
            box-shadow: 0 0 5px #B6B6B6;
            float: left;
            margin: 6px;
            overflow: auto;
        }

        #scroll2::-webkit-scrollbar-thumb {
            background-color: #ECE8E8;
            outline: 1px solid #ECE8E8;
        }

        #scroll2::-webkit-scrollbar {
            width: 5px;
        }

        #scroll1::-webkit-scrollbar-thumb {
            background-color: #ECE8E8;
            outline: 1px solid #ECE8E8;
        }

        #scroll1::-webkit-scrollbar {
            width: 5px;
        }

        .btncon {
            background-color: #003E77;
            height: 33px;
            width: 161px;
            font-family: sans-serif;
            border: 0;
            padding: 8px 20px;
            color: white;
            border-radius: 2px;
            cursor: pointer;
        }

        .config {
            border: 0px solid;
            width: 124px;
            height: 40px;
            float: right;
        }

        .btn {
            margin-top: 5px;
            background-color: #0154A0;
            border: 0;
            padding: 8px 20px;
            color: white;
            border-radius: 2px;
            cursor: pointer;
            width: 119px;
        }
    </style>
    <script type="text/javascript">

        function getData() {
            var strData = new Object();
            strData.txtFecini = $('#txtFecini').val();
            strData.txtHoraIni = $('#txtHoraIni').val();
            strData.txtFecfin = $('#txtFecfin').val();
            strData.txtHoraFin = $('#txtHoraFin').val();
            strData.cboVendedor = $('#cboVendedor').val();
            strData.cboPerfil = $('#cboPerfil').val();
            strData.cboGrupro = $('#cboGrupro').val();
            return strData;
        }
        $(document).ready(function () {

            changeCboArticulo();
            $("#sortable").sortable();
            $("#sortable").disableSelection();
            $("#sortabledetalle").sortable();
            $("#sortabledetalle").disableSelection();
            ClickCheked('checkC');
            ClickCheked('checkD');
            Guardar();
            eventoMouse();
            btn();
            mostrarOcultarConfiguracion();
            salir();
        });
        function mostrarOcultarConfiguracion() {
            $("#btnConfiguracion").click(function () {
                $("#configuracionocultar").css("display", "block");
                $('#cboArchivo').trigger("change");
            });
        }
        function salir() {
            $("#BtnSalir").click(function () {
                $("#configuracionocultar").css("display", "none");
            });
        }
        function btn() {
            $(".btn").mouseover(function () {
                $(this).css("background-color", "rgb(7, 54, 150)");
            });
            $(".btn").mouseout(function () {
                $(this).css("background-color", "#01549E");
            });
        }
        function eventoMouse() {
            $("div.sortable_hijo").live('mouseover', function () {
                $(this).removeClass("tamaño-ancho-alto-off");
                $(this).addClass("tamaño-ancho-alto-on");
            });
            $("div.sortable_hijo").live('mouseout', function () {
                $(this).removeClass("tamaño-ancho-alto-on");
                $(this).addClass("tamaño-ancho-alto-off");
            });
        }
        function changeCboArticulo() {
            $('#cboArchivo').change(function () {
                var URLS = "Descarga.aspx/BuscarNombreColumna";
                var OBJ = new Object();
                OBJ.codArchivo = $(this).val();
                OBJ.tipoColumna = "C";
                BuscarNombreColumnasCabecera(URLS, OBJ);
                OBJ.tipoColumna = "D";
                BuscarNombreColumnasDetalle(URLS, OBJ);

            });
        }
        function BuscarNombreColumnasCabecera(URL, OBJ) {
            $.ajax({
                url: URL,
                data: JSON.stringify(OBJ),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $('#sortable').empty();
                    $('#sortable').append(msg.d);
                    Cheked('checkC');
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function BuscarNombreColumnasDetalle(URL, OBJ) {
            $.ajax({
                url: URL,
                data: JSON.stringify(OBJ),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $('#sortabledetalle').empty();
                    $('#sortabledetalle').append(msg.d);
                    Cheked('checkD');
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function Cheked(Tipo) {
            $('.' + Tipo + '').each(function () {
                if ($(this).val() == 'T') {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
        }
        function ClickCheked(Tipo) {
            $('.' + Tipo + '').live('click', function () {
                if ($(this).prop('checked') == true) {
                    $(this).attr("value", "T");
                } else {
                    $(this).attr("value", "F");
                }
            });
        }
        function Guardar() {
            $('#BtnGuardar').click(function () {
                var cabecera = CheckBoxTrue('checkC');
                var detalle = CheckBoxTrue('checkD');
                var URL = "Descarga.aspx/GuardarConfiguracion";
                var COLUMNAS = new Object();
                COLUMNAS.cabecera = cabecera;
                COLUMNAS.detalle = detalle;

                var valorC = validarSinSeleccionar('checkC');
                var valorD = validarSinSeleccionar('checkD');
                var codigoArti = $("#cboArchivo").val();

                if (valorC == 0) {
                    addnotify("notify", "Seleccione por lo menos Un Item de la Cabecera.", "registeruser");
                } else {
                    if (codigoArti.trim() == "PGO" || codigoArti.trim() == "NVTA" || codigoArti.trim() == "PRT") {
                        EnvioALaBaseDeDatos(URL, COLUMNAS);
                    } else {
                        if (valorD == 0) {
                            addnotify("notify", "Seleccione por lo menos Un Item del Detalle.", "registeruser");
                        } else {
                            EnvioALaBaseDeDatos(URL, COLUMNAS);
                        }

                    }

                }

            });
        }
        function validarSinSeleccionar(Tipo) {
            var contador = 0;
            $('.' + Tipo + '').each(function () {
                if ($(this).prop('checked') == true) {
                    contador++;
                }
            });
            return contador;
        }
        function EnvioALaBaseDeDatos(URLSG, COLUMNAS) {
            $.ajax({
                url: URLSG,
                data: JSON.stringify(COLUMNAS),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    addnotify("notify", msg.d, "registeruser");
                },
                error: function (result) {
                    addnotify("notify", result.status, "registeruser");
                }
            });
        }
        function CheckBoxTrue(Tipo2) {
            var Nombre = "";
            var Habilitado = "";
            var Id = "";
            var nuevoOrden = 0;
            var ordenCadena = "";
            $('.' + Tipo2 + '').each(function (i) {
                nuevoOrden += 1;
                ordenCadena += nuevoOrden + '|';
                Nombre += $(this).attr('opc') + '|';
                Habilitado += $(this).attr('value') + '|';
                Id += $(this).attr('id') + '|';
            });
            return Id + ',' + Nombre + ',' + Habilitado + ',' + ordenCadena;
        }
    </script>

</head>
<body class="formularyW">
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../imagery/all/icons/config.png" alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p>
                                <%=Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_NAV_DESCARGA)%>
                            </p>
                        </div>

                    </div>
                    <div class="config">
                        <input type="button" id="btnConfiguracion" class="btn" runat="server"
                            value="Configuración" />
                    </div>
                </div>
                <div class="cz-form-box-content">
                    <div style="font-weight: bold; padding-left: 12px; margin-bottom: 10px;">
                        <p>
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NAV_DESCARGA)%>
                        </p>
                    </div>
                    <div class="cz-form-content">
                        <label>
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAINICIO)%></label>
                        <asp:TextBox ID="txtFecini" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="fecfin-img" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            &nbsp;
                        </p>
                        <input type="text" id="txtHoraIni" value="00:00" runat="server" maxlength="5" size="6"
                            onblur="javascript:fc_ValidaHoraOnblur(this.id);" class="cz-form-content-input-text" />
                        <div class="cz-form-content-input-text-visible">
                            <div class="cz-form-content-input-text-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <label>
                            <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_FECHAFIN)%></label>
                        <asp:TextBox ID="txtFecfin" class="cz-form-content-input-calendar" MaxLength="10"
                            onkeypress="javascript:fc_Slash(this.id, '/');" onblur="javascript:fc_ValidaFechaOnblur(this.id);"
                            runat="server"></asp:TextBox>
                        <div class="cz-form-content-input-calendar-visible">
                            <div class="cz-form-content-input-calendar-visible-button">
                                <img alt="<>" id="fecini-img" name="fecfin-img" class="form-input-date-image cz-form-content-input-text-calendar"
                                    src="../images/icons/calendar.png" />
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            &nbsp;
                        </p>
                        <input type="text" id="txtHoraFin" value="23:59" runat="server" maxlength="5" size="6"
                            class="cz-form-content-input-text" onblur="javascript:fc_ValidaHoraOnblur(this.id);" />
                        <div class="cz-form-content-input-text-visible">
                            <div class="cz-form-content-input-text-visible-button">
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-content" <%=deshabilitarCboSupervisor %>>
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_SUPERVISOR)%>
                        </p>
                        <asp:DropDownList ID="cboPerfil" runat="server" CssClass="cz-form-content-input-select" AutoPostBack="True" OnSelectedIndexChanged="cboPerfil_SelectedIndexChanged">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_GRUPO)%>
                        </p>
                        <asp:DropDownList ID="cboGrupro" runat="server" CssClass="cz-form-content-input-select" AutoPostBack="True" OnSelectedIndexChanged="cboGrupro_SelectedIndexChanged">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content">
                        <p>
                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_VENDEDOR)%>
                        </p>
                        <asp:DropDownList ID="cboVendedor" runat="server" CssClass="cz-form-content-input-select">
                        </asp:DropDownList>
                        <div class="cz-form-content-input-select-visible">
                            <p class="cz-form-content-input-select-visible-text">
                            </p>
                            <div class="cz-form-content-input-select-visible-button">
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content-wall">
                        <asp:Button ID="btnDescarga" class="form-button cz-form-content-input-button" runat="server"
                            Text="DESCARGA" OnClick="btnDescarga_Click" />
                    </div>
                </div>
                <div id="configuracionocultar" class="cz-form-box-content" style="display: none">

                    <center>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <div class="contenedor-Combo">
                                        <h1><%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MANT_CONFIG_DESCARGA)%></h1>
                                        <p>
                                            <%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MANT_ARCHIVO_DESCARGA)%>
                                        </p>
                                        <asp:DropDownList ID="cboArchivo" runat="server" CssClass="cz-form-content-input-select">
                                        </asp:DropDownList>
                                        <div class="cz-form-content-input-select-visible">
                                            <p class="cz-form-content-input-select-visible-text">
                                            </p>
                                            <div class="cz-form-content-input-select-visible-button">
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="color: rgb(105, 105, 109); font-weight: bold; padding-left: 8px;"><%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CAMPOS_CABECERA)%></p>
                                    <div id="scroll1" class="cambiar-contenedor">

                                        <div id="sortable">
                                        </div>
                                    </div>
                                </td>
                                <td>

                                    <p style="color: rgb(105, 105, 109); font-weight: bold; padding-left: 8px;"><%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_CAMPOS_DETALLE)%></p>
                                    <div id="scroll2" class="cambiar-contenedor">

                                        <div id="sortabledetalle">
                                        </div>

                                    </div>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <center>
                                        <input type="button" id="BtnGuardar" class="btncon" value="<%= Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_MANT_GRUARDA_CONFIGURACION)%>" />
                                        <input type="button" id="BtnSalir" value="Salir" class="btncon" />
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>

            </div>
            <asp:Literal ID="ltmensaje" runat="server"></asp:Literal>
            <div id="calendarwindow" class="calendar-window">
            </div>

        </div>
    </form>
</body>
</html>
