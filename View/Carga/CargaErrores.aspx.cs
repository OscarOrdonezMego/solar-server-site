﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Controller;
using Model.bean;
using System.Text;

///<summary>
///@001 GMC 05/05/2015 Se valida texto según configuración de carga de ALMACEN y ALMACE_PRODUCTO
///</summary>
namespace Pedidos_Site.Carga
{
    public partial class Carga_CargaErrores : System.Web.UI.Page
    {
        //@001 I
        private String valorConfigMostrarAlmacen = "F";
        //@001 F

        private char[] _separador = { '|' };
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            if (dataJSON != null)
            {

                myModalLabel.InnerText = dataJSON["codigo"];
                //@001 I
                //label1.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FORMATO_INVAL_ARCHIV) + " " + dataJSON["codigo"] + ". " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_PORFAVOR_REVISARLO);
                /* if (
                     dataJSON["codigo"] == "ALMACEN"
                     ||
                     dataJSON["codigo"] == "ALMACEN_PRODUCTO"
                     )
                 {

                     this.ObtenerDatosConfiguracion(Model.Constantes.CodigoValoresConfiguracion.CFG_MOSTRAR_ALMACEN, out this.valorConfigMostrarAlmacen);
                     if (valorConfigMostrarAlmacen == Model.Enumerados.FlagHabilitado.T.ToString())
                     {
                         label1.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FORMATO_INVAL_ARCHIV) + " " + dataJSON["codigo"] + ". " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_PORFAVOR_REVISARLO);
                     }
                     else 
                     {
                         label1.InnerText = "Campo Mostrar Almacén en el Mantenimiento de Configuración no está habilitado" + ". " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_PORFAVOR_REVISARLO);
                     }

                 }
                 else 
                 {*/
                label1.InnerText = IdiomaCultura.getMensaje(IdiomaCultura.WEB_FORMATO_INVAL_ARCHIV) + " " + dataJSON["codigo"] + ". " + IdiomaCultura.getMensaje(IdiomaCultura.WEB_PORFAVOR_REVISARLO);
                /*}*/
                //@001 F
                GridView1.DataSource = GeneralController.getErroresCarga(dataJSON["codigo"]);
                GridView1.DataBind();

            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int cantColumnas = e.Row.Cells.Count - 1;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int i = 0; i <= cantColumnas; i++)
                {
                    e.Row.Cells[i].Text = IdiomaCultura.getMensajeEncodeHTML(e.Row.Cells[i].Text);
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                String _errores = e.Row.Cells[cantColumnas].Text;
                string[] _arrErrores = _errores.Split(_separador);
                StringBuilder _listaErrores = new StringBuilder();

                foreach (string error in _arrErrores)
                {
                    if (error != string.Empty)
                    {
                        _listaErrores.Append(error).Append("<br/>");
                    }
                }

                //la ultima columna es de los errores
                e.Row.Cells[e.Row.Cells.Count - 1].Style.Value = "text-align:left;";
                e.Row.Cells[e.Row.Cells.Count - 1].Text = _listaErrores.ToString();
                //rowView["ERROR"] = listaErrores.ToString();
                _listaErrores = null;
            }
        }
        //@001 I
        private void ObtenerDatosConfiguracion(String codCampo, out String valorCampo)
        {
            valorCampo = "F";
            ConfiguracionBean configBean = ConfiguracionController.info(codCampo);
            if (configBean != null)
            {
                if (configBean.FlagHabilitado.Equals(Model.Enumerados.FlagHabilitado.T.ToString()))
                {
                    valorCampo = configBean.Valor;
                }
            }
        }
        //@001 F
    }
}