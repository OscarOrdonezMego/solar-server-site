﻿using Model.bean;
using Newtonsoft.Json;
using Pedidos_Site.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Pedidos_Site.Carga
{
    /// <summary>
    /// Descripción breve de Carga
    /// </summary>
    public class Carga : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                String json = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();
                Dictionary<string, List<string>> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(json);
                //Dictionary<string, List<Dictionary<string, string>>> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, string>>>>(json);
                List<ComboBean> lcol = new List<ComboBean>();
                if (dataJSON.Count() > 0)
                {
                    Dictionary<string, List<string>>.KeyCollection hojas = dataJSON.Keys;
                    var conHoja = 1;
                    foreach (var item in hojas)
                    {
                        if (conHoja <= 1)
                        {
                            if (dataJSON[item].Count() > 0)
                            {
                                if (dataJSON[item].Count() > 0)
                                {
                                    var indexCol = 0;
                                    foreach (var item2 in dataJSON[item])
                                    {
                                        if (item2 != "")
                                        {
                                            if (lcol.Where(x => x.nombre == item2).Count() > 0)
                                            {
                                                throw new Exception("Error: Verificar archivo, Existen Columnas con nombre duplicado: " + item2);
                                            }
                                            var asd = item2;
                                            ComboBean col = new ComboBean
                                            {
                                                codigo = indexCol.ToString(),
                                                nombre = item2
                                            };
                                            lcol.Add(col);
                                            indexCol++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception("Error: No se encontraron Columnas.");
                            }
                        }
                        conHoja++;
                    }
                }
                else
                {
                    throw new Exception("Error: No se encontraron Hojas.");
                }
                var res = new { tipo = "1", res = lcol, mensaje = "Procesado Correctamente" };
                String output = JsonConvert.SerializeObject(res, Formatting.Indented);
                context.Response.Write(output);
            }
            catch (Exception ex)
            {
                var res = new { tipo = "-1", mensaje = ex.Message };
                new LoggerHelper().Error("Error: carga.ashx", ex);
                //throw new Exception("Error: " + ex.Message);
                String output = JsonConvert.SerializeObject(res, Formatting.Indented);
                context.Response.Write(output);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}