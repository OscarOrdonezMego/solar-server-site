﻿using Controller;
using Model.bean;
using Newtonsoft.Json;
using Pedidos_Site.App_Code;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

namespace Pedidos_Site.Carga
{
    public partial class Carga_Carga : PageController
    {
        protected override void initialize()
        {
            if (!IsPostBack)
            {
                try
                {
                    String fileLocation = HttpContext.Current.Server.MapPath("~") + System.Configuration.ConfigurationManager.AppSettings["rutaTMP"].ToString();

                    DTSController.deleteDataFiles(fileLocation);
                    ltrLstCheckDeleteFiles.Text = GenerarFilasCheckBox();
                }
                catch (Exception e)
                {
                    throw new Exception("[ERROR] Se presentan errores al borrar el archivo");
                }
            }
        }

        private String GenerarFilasCheckBox()
        {
            StringBuilder stbCheckDeleteFiles = new StringBuilder();
            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Producto </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkProducto' name='Producto' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Vendedor </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkVendedor' name='Vendedor' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Cliente </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkCliente' name='Cliente' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Ruta </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkRuta' name='Ruta' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Cobranza </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkCobranza' name='Cobranza' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> General </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkGeneral' name='General' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Lista de Precios </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkListaPrecios' name='ListaPrecios' />" +
                                        "</div>");

            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                        "<p> Dirección </p>" +
                                        "<input type = 'checkbox' class='cz-form-content-input-check' id='chkDireccion' name='Direccion' />" +
                                        "</div>");
            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                       "<p> Familia </p>" +
                                       "<input type = 'checkbox' class='cz-form-content-input-check' id='chkFamilia' name='Familia'  />" +
                                       "</div>");
            stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                       "<p> Marca </p>" +
                                      "<input type = 'checkbox' class='cz-form-content-input-check' id='chkMarca' name='Marca'  />" +
                                      "</div>");

            ConfiguracionBean TPAL = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPAL"];
            ConfiguracionBean TPFM = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFM"];
            ConfiguracionBean TPFS = (ConfiguracionBean)ManagerConfiguration.HashConfig["TPFS"];

            if (TPFM.Valor == "T" || TPFS.Valor == "T")
            {
                stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                       "<p> Presentación </p>" +
                                       "<input type = 'checkbox' class='cz-form-content-input-check' id='chkPresentacion' name='Presentacion' />" +
                                       "</div>");
            }
            if (TPAL.Valor == "T") //Almacén
            {
                stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                       "<p> Almacén </p>" +
                                       "<input type = 'checkbox' class='cz-form-content-input-check' id='chkAlmacen' name='Almacen' />" +
                                       "</div>");

                stbCheckDeleteFiles.Append("<div class='cz-form-content'>" +
                                       "<p> Almacén Producto/Presentación</p>" +
                                       "<input type = 'checkbox' class='cz-form-content-input-check' id='chkAlmacenProductoPresentación' name='Almacen_Producto_Presentacion' />" +
                                       "</div>");
            }

            return stbCheckDeleteFiles.ToString();
        }

        private static String tableResult(List<FileCargaBean> lista)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<table class='grilla table table-bordered table-striped' cellspacing='0' id='grdMantUsuario' style='border-collapse:collapse;'><tbody>\n");
            sb.Append("<tr><th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ARCHIVO) + "</th>");
            sb.Append("<th scope='col' >" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ESTADOS) + "</th>\n");
            sb.Append("<th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_SUBIDOS) + "</th>\n");
            sb.Append("<th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_INSERTADOS) + "</th>\n");
            sb.Append("<th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ACTUALIZADOS) + "</th>\n");
            sb.Append("<th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TOTAL_CARGADOS) + "</th>\n");
            sb.Append("<th scope='col'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_TOTAL_NO_CARGADOS) + "</th></tr>\n");

            foreach (FileCargaBean BE in lista)
            {
                sb.Append("<tr>\n");

                sb.Append("<td align='center' valign='middle' >" + BE.archivo + "</td>\n");
                sb.Append("<td align='center' valign='middle' >" + BE.EstadoToHTML + "  <div style='display:none' class='errmsg'>" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_FORMATO_INVAL_ARCHIV) + " " + BE.archivo + ". " + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_PORFAVOR_REVISARLO) + " <br/>" + BE.errorExecute + "</div>   </td>\n");
                sb.Append("<td align='center' valign='middle' >" + BE.subidos + "</td>\n");
                sb.Append("<td align='center' valign='middle' >" + BE.insertados + "</td>\n");
                sb.Append("<td align='center' valign='middle' >" + BE.actualizados + "</td>\n");
                sb.Append("<td align='center' valign='middle' >" + (BE.actualizados + BE.insertados) + "</td>\n");
                sb.Append("<td align='center' valign='middle' >" + Convert.ToString(BE.subidos - (BE.actualizados + BE.insertados)) + "</td>\n");
                sb.Append("</tr>\n");
            }

            sb.Append("</tbody></table>\n");

            return sb.ToString();
        }

        private static String tableResultMapper(List<archivoExcelTablas> lista)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button><h3 id='myModalLabel' runat='server'>Formatos...</h3></div>\n");
            sb.Append("<div id='myModalContent' class='modal-body' style='height: 300px; '>\n");

            sb.Append("<div class='form-grid-table-outer'>\n");
            sb.Append("<div class='form-grid-table-inner'>\n");
            sb.Append("<div class='form-gridview-data' id='divGridViewData' runat='server'>\n");
            sb.Append("<div id='divGridView' runat='server'>\n");

            sb.Append("<table class='grilla table table-bordered table-striped' cellspacing='0' id='grdMantUsuario' style='border-collapse:collapse;'><tbody>\n");
            sb.Append("<tr><th scope='col'>Hojas Excel</th>");
            sb.Append("<th scope='col' >Estado</th>\n");
            sb.Append("<th scope='col'>Formato Cliente</th>\n");
            sb.Append("<th scope='col'>Formato Entel</th>\n");
            sb.Append("</tr>\n");
            Int16 row = 0;
            foreach (archivoExcelTablas BE in lista)
            {
                if (BE.tablas.Count <= 0)
                {
                    sb.Append("<tr>\n");
                    sb.Append("<td align='center' colspan=4 valign='middle' >No se encontraron tablas definidas por el sistema.</td>\n");
                    sb.Append("</tr>\n");
                }
                foreach (archivoTabla at in BE.tablas)
                {
                    sb.Append("<tr>\n");
                    sb.Append("<td align='center' valign='middle' >" + at.tabla + "</td>\n");
                    sb.Append("<td align='center' valign='middle' >" + (at.formatoCliente == "T" ? "Mapeado" : "Sin Mapeo") + " </td>\n");
                    sb.Append("<td align='center' valign='middle' ><input type='radio' name ='rCarga" + row + "' class='rCliente'  value ='" + at.tabla + "' " + (at.formatoCliente == "T" ? "checked='checked'" : "") + " " + (at.formatoCliente == "F" ? "disabled" : "") + " > " + "</td>\n");
                    sb.Append("<td align='center' valign='middle' ><input type='radio' name ='rCarga" + row + "' class='rEntel' value ='" + at.tabla + "' " + (at.formatoCliente == "F" ? "checked='checked'" : "") + " > " + "</td>\n");
                    sb.Append("</tr>\n");
                    row++;
                }
            }

            sb.Append("</tbody></table>\n");

            sb.Append("</div></div></div>\n");
            sb.Append("</div></div><div class='modal-footer'><button id='cancelReg' class='form-button cz-form-content-input-button' data-dismiss='modal' aria-hidden='true'>Cerrar</button>\n");
            sb.Append("<input type='button' id='saveMap' class='form-button cz-form-content-input-button' value='Guardar' /></div>\n");
            return sb.ToString();
        }

        [WebMethod]
        public static void eliminarDataAnteriorCarga(String listaArchivosEliminar)
        {
            listaArchivosEliminar = listaArchivosEliminar.TrimEnd(',');
            DTSController.eliminarDataAnteriorCarga(listaArchivosEliminar);
        }

        [WebMethod]
        public static String ejecutarArchivoDTS(String Tipo)
        {
            String fileLocation = HttpContext.Current.Server.MapPath("~") + System.Configuration.ConfigurationManager.AppSettings["rutaTMP"].ToString();
            String DTSLocation = HttpContext.Current.Server.MapPath("~") + System.Configuration.ConfigurationManager.AppSettings["rutaDTS"].ToString();

            //List<FileCargaBean> lst = DTSController.ejecutarArchivoDTS(fileLocation, DTSLocation, Tipo);
            List<archivoExcelTablas> sesMapTabla = (List<archivoExcelTablas>)HttpContext.Current.Session["sesMapTabla"];
            List<FileCargaBean> lst = DTSController.cargaDTS_XLS_BC(fileLocation, sesMapTabla);

            return tableResult(lst);
        }

        [WebMethod]
        public static String ValidaMapeoTablas()
        {
            List<String> tResult = new List<String>();
            tResult.Add("1");
            tResult.Add("Mensaje");
            tResult.Add("Result");
            HttpContext.Current.Session["sesMapTabla"] = null;
            try
            {
                String filesLocation = HttpContext.Current.Server.MapPath("~") + System.Configuration.ConfigurationManager.AppSettings["rutaTMP"].ToString();
                String[] extensions;
                extensions = new String[] { "*.xls", "*.xlsx" };

                List<String> arrArchivosCargados = new List<String>();
                foreach (String extension in extensions)
                {
                    String[] filesArr = Directory.GetFiles(filesLocation, extension, SearchOption.TopDirectoryOnly);
                    foreach (String file in filesArr)
                    {
                        new LoggerHelper().Debug("Debug", "Archivo Add: " + file);
                        if (arrArchivosCargados.Find(x => x == file) == null)
                        {
                            new LoggerHelper().Debug("Debug", "Archivo Add: " + file);
                            arrArchivosCargados.Add(file);
                        }
                    }
                }
                new LoggerHelper().Debug("Debug", "Archivos: " + arrArchivosCargados.Count().ToString());
                var mapeosTabla = MapeoController.ListarMaestrosMapeados();
                //var mapeosArchivos = MapeoController.ListarMaestrosMapeados();
                List<archivoExcelTablas> cargaTabla = new List<archivoExcelTablas>();
                if (arrArchivosCargados != null && arrArchivosCargados.Count > 0)
                {
                    foreach (string arch in arrArchivosCargados)
                    {
                        var tablasAr = new List<archivoTabla>();
                        foreach (String tabla in CargaExcelController.GetExcelSheetNames(arch))
                        {
                            tablasAr.Add(new archivoTabla { tabla = tabla.Replace("$", "") });
                        }
                        cargaTabla.Add(new archivoExcelTablas
                        {
                            archivo = arch,
                            tablas = tablasAr,
                        });
                    }

                }

                List<archivoExcelTablas> cargaTablaFinal = new List<archivoExcelTablas>();
                foreach (archivoExcelTablas item in cargaTabla)
                {
                    List<archivoTabla> tablasFinal = new List<archivoTabla>();
                    foreach (archivoTabla tabla in item.tablas)
                    {
                        if (mapeosTabla.Where(x => x.tabla.ToUpper() == tabla.tabla.ToUpper()).Count() <= 0)
                        {

                        }
                        else
                        {
                            archivoTabla tablaFinal = new archivoTabla
                            {
                                tabla = tabla.tabla,
                                formatoCliente = mapeosTabla.Where(x => x.tabla.ToUpper() == tabla.tabla.ToUpper() && x.tipoFormato == "C").Count() > 0 ? "T" : "F",
                                formatoEntel = mapeosTabla.Where(x => x.tabla.ToUpper() == tabla.tabla.ToUpper() && x.tipoFormato == "E").Count() > 0 ? "T" : "F",
                            };
                            tablasFinal.Add(tablaFinal);
                        }
                    }
                    new LoggerHelper().Debug("Debug", item.archivo);
                    cargaTablaFinal.Add(new archivoExcelTablas
                    {
                        archivo = item.archivo,
                        tablas = tablasFinal
                    });
                }

                tResult[0] = "1";
                tResult[1] = "Procesado Correctamente";
                tResult[2] = tableResultMapper(cargaTablaFinal);

                HttpContext.Current.Session["sesMapTabla"] = cargaTablaFinal;

                if (mapeosTabla == null && mapeosTabla.Count <= 0)
                {
                    tResult[0] = "-1";
                    tResult[1] = "No se encuntra Mapeo de Tablas(Cliente, Entel)";
                }
                if (cargaTabla == null && cargaTabla.Count <= 0)
                {
                    tResult[0] = "-1";
                    tResult[1] = "No se encuentran Tablas a cargas";
                }
            }
            catch (Exception e)
            {
                new LoggerHelper().Error("Error", e);
                tResult[0] = "-1";
                tResult[1] = "Errores al mapear tablas";
            }
            dynamic output = new { tipo = tResult[0], mensaje = tResult[1], tabla = tResult[2] };
            return JsonConvert.SerializeObject(output, Formatting.Indented); ;
        }

        [WebMethod]
        public static Boolean borrarArchivo(String filename, String fileext)
        {
            try
            {
                String fileLocation = HttpContext.Current.Server.MapPath("~") + System.Configuration.ConfigurationManager.AppSettings["rutaTMP"].ToString();
                FileInfo fileInfo = new FileInfo(fileLocation + filename + "." + fileext);
                fileInfo.Delete();
            }
            catch (Exception e)
            {
                throw new Exception("[ERROR] Se presentan errores al borrar el archivo");
            }

            return true;
        }

        [WebMethod]
        public static String GuardarMapeoTablas(String TablasEntel, String TablasCliente)
        {
            dynamic output = new { tipo = "1", mensaje = "Procesado Correctamente" };
            List<archivoExcelTablas> sesMapTabla = (List<archivoExcelTablas>)HttpContext.Current.Session["sesMapTabla"];
            var lstEntel = TablasEntel.Split(',');
            var lstCliente = TablasCliente.Split(',');
            try
            {
                if (sesMapTabla != null)
                {
                    foreach (archivoExcelTablas item in sesMapTabla)
                    {
                        foreach (archivoTabla tablas in item.tablas)
                        {
                            tablas.formatoCliente = "F";
                            tablas.formatoEntel = "F";
                            if (lstEntel.Where(x => x == tablas.tabla).Count() > 0)
                            {
                                tablas.formatoEntel = "T";
                            }
                            if (lstCliente.Where(x => x == tablas.tabla).Count() > 0)
                            {
                                tablas.formatoCliente = "T";
                            }
                        }
                    }
                    HttpContext.Current.Session["sesMapTabla"] = sesMapTabla;
                }
                else
                {
                    output = new { tipo = "-1", mensaje = "Error al realizar mapeo." };
                }
            }
            catch (Exception)
            {
                output = new { tipo = "-1", mensaje = "Error al realizar mapeo." };
            }
            return JsonConvert.SerializeObject(output, Formatting.Indented); ;
        }
    }
}