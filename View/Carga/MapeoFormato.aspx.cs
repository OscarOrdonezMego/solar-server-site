﻿using System;
using Model.bean;
using Controller;
using System.Web.Services;
using Model;
using business.functions;
using System.Collections.Generic;
using System.Linq;

namespace Pedidos_Site.Carga
{
    public partial class MapeoFormato : PageController
    {

        protected override void initialize()
        {
            if (!IsPostBack)
            {

                CargaCombo();
                litGrilla.Text = "<table class='grilla table table-bordered table-striped' cellspacing='0' border='0' id='Table1' style='border-collapse: collapse; width: 100%;'>" +
                            "<thead>" +
                             "   <tr>";
                litGrilla.Text += "     <th scope='col' >CAMPOS DEL ARCHIVO</th>";
                litGrilla.Text += "     <th scope='col' >CAMPOS DE PEDIDOS</th>";
                litGrilla.Text += "     <th scope='col' >FORMATOS DEL CAMPO</th>";
                litGrilla.Text += "</tr>";

                //foreach (var eRepor in lst)
                //{
                //    litGrilla.Text += "<tr>";
                //    foreach (var ezc in eRepor.ZonaCarga)
                //    {
                //        var Style = "'background-color: " + (ezc.valor == "0" ? "" : ezc.Color) + ";color: " + (ezc.valor == "0" ? "black" : (ezc.Color.ToUpper() == "RED" ? "#FFFF00" : (ezc.Color.ToUpper() == "GREEN" ? "#FFFFFF" : "#DD0000"))) + ";'";
                //        litGrilla.Text += "<td align='center' style=" + Style + " >" + (ezc.valor == "0" ? "" : ezc.valor) + "</td>";
                //    }
                //    litGrilla.Text += "</tr>";
                //}

                litGrilla.Text += "</thead><tbody></tbody>" +
    "                   </table>";
            }
        }
        public void CargaCombo()
        {
            try
            {
                Utility.ComboNuevo(ddlTabla, MapeoController.ListarMaestroTabla(), "id", "tabla");
            }
            catch (Exception ex)
            {
                //LoggerHelper.LogException(ex, "Error :" + this);
                throw new Exception("ERROR: " + ex.Message);
            }
        }
        [WebMethod]
        public static String CargaTablaColumna(String idTabla)
        {
            dynamic columnas, output;
            try
            {
                if (idTabla != "")
                {
                    columnas = MapeoController.ListarMaestroTablaColumnas(Int64.Parse(idTabla));

                }
                else
                {
                    columnas = new List<MapeoBean>();
                }
                output = Newtonsoft.Json.JsonConvert.SerializeObject(columnas, Newtonsoft.Json.Formatting.Indented);
                return output;
            }
            catch (Exception ex)
            {
                //LogHelper.LogException(ex, "Error :Zcarga_Insert : ");
                throw new Exception("ERROR: " + ex.Message);
            }
        }
        [WebMethod]
        public static String ListarMaestroTablaColumnasId(String idColumna)
        {
            try
            {
                dynamic columnas, output;
                if (idColumna != "")
                {
                    columnas = MapeoController.ListarMaestroTablaColumnasId(Int64.Parse(idColumna));
                }
                else
                {
                    columnas = new MapeoBean();
                }
                output = Newtonsoft.Json.JsonConvert.SerializeObject(columnas, Newtonsoft.Json.Formatting.Indented);
                return output;
            }
            catch (Exception ex)
            {
                //LogHelper.LogException(ex, "Error :Zcarga_Insert : ");
                throw new Exception("ERROR: " + ex.Message);
            }
        }
        [WebMethod]
        public static String GuardarMapeo(String idtabla,List<columnasMap> columna)
        {
            dynamic output;
            try
            {
                if (idtabla=="")
                {
                    throw new Exception("No se ha seleccionado la tabla.");
                }
                List<columnasMap> input= new List<columnasMap>();
                foreach (var item in columna.Where(x=>x.idColumna!=""))
                {
                    input.Add(new columnasMap
                        {
                            columnaxls = item.columnaxls,
                            idColumna = item.idColumna,
                            idColumnaxls = item.idColumnaxls,
                        });
                }
                MapeoController.GuardarMaestroTabla(Int64.Parse(idtabla), input);

                output = new { tipo = "1", mensaje = "El proceso se realizo con exito" };
                //output = "";// Newtonsoft.Json.JsonConvert.SerializeObject(columnas, Newtonsoft.Json.Formatting.Indented);
                //return output;
            }
            catch (Exception ex)
            {
                output = new { tipo = "-1", mensaje = ex.Message };
                //LogHelper.LogException(ex, "Error :Zcarga_Insert : ");
                throw new Exception("ERROR: " + ex.Message);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(output, Newtonsoft.Json.Formatting.Indented);
        }
    }
    
}