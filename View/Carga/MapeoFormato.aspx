﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Carga.MapeoFormato" CodeBehind="MapeoFormato.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <%--  <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>--%>
    <%--    <link href="../../css/jquery.autocomplete.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/PopupCalendar.css" type="text/css" rel="stylesheet" />
    <script src="../../js/PopupCalendar.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/alertHtml.ashx" type="text/javascript"></script>--%>
    <%--<script src="../js/JSModule.js" type="text/javascript"></script>--%>
    <%--<link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>--%>

    <style type="text/css">
        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

            .inputfile + label {
                font-size: 1.25em;
                font-weight: 700;
                color: white !important;
                background-color: black;
                display: inline-block;
            }

                .inputfile:focus + label,
                .inputfile + label:hover {
                    background-color: #003E77;
                }

            .inputfile + label {
                cursor: pointer; /* "hand" cursor */
                background-color: rgb(1, 84, 160);
                color: white;
            }

            .inputfile:focus + label {
                outline: 1px dotted #000;
                outline: -webkit-focus-ring-color auto 5px;
            }

            .inputfile + label svg {
                width: 1em;
                height: 1em;
                vertical-align: middle;
                fill: currentColor;
                margin-top: -0.25em;
                margin-right: 0.25em;
                color: white;
            }
            .inputfile:disabled + label{
                background-color: rgb(84, 84, 84);
            cursor: default;
        }
    </style>
    <script type="text/javascript">
        var urlCarga = "Carga.ashx";
        Object.filter = (obj, predicate) =>
            Object.keys(obj)
                  .filter(key => predicate(obj[key]))
                  .reduce((res, key) => (res[key] = obj[key], res), {});
        function ObjectLength(object) {
            var length = 0;
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    ++length;
                }
            }
            return length;
        };
        $(document).ready(function () {
            sessionStorage.removeItem("columnasTabla");
            sessionStorage.removeItem("columnasXls");
            $(".cz-form-content-input-select").change(function () {

                $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
            });
            $(".cz-form-content-input-select").trigger("change");

            //$("#xlf").click(function () {
            //    $("#cz-form-step-image-text-left").addClass("pasos");
            //    $("#paso1").addClass("pasos");
            //});
            $("#ddlTabla").change(function () {
                var strData = new Object();
                strData.idTabla = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: "MapeoFormato.aspx/CargaTablaColumna",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    beforeSend: function () {
                        sessionStorage.removeItem("columnasTabla");
                    },
                    success: function (result) {
                        sessionStorage.setItem("columnasTabla", result.d);
                        tablaMapeo($("#Table1").find("tbody"));
                        //localStorage.setItem("columnasTabla2", $.parseJSON(result.d));
                    },
                    error: function (xhr, status, error) {
                        sessionStorage.removeItem("columnasTabla");
                        addnotify("notify", error, "registeruser");
                    }
                });
            });
            $("#xlf").change(function () {
                var tipoCarga = "E";// $('input[name=tipoCarga]:checked').val();
                //alert(tipoCarga);
                var xlf = document.getElementById('xlf');
                rABS = true;
                use_worker = true;
                var files = xlf.files;
                if (files.length > 0) {
                    var f = files[0];
                    if (tipoCarga == "E") {
                        var ext = f.name.split(".").pop().toLowerCase();
                        if ($.inArray(ext, ['xls', 'xlsx']) == -1) {
                            addnotify("notify", "Extensión de archivo inválido", "registeruser");
                        } else {
                            //$("#cz-form-step-image-text-center").addClass("pasos");
                            //$("#paso2").addClass("pasos");
                            {
                                var reader = new FileReader();
                                var name = f.name;
                                reader.onload = function (e) {
                                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                                    var data = e.target.result;
                                    if (use_worker) {
                                        xw(data, process_wb);
                                    } else {
                                        var wb;
                                        if (rABS) {
                                            wb = X.read(data, { type: 'binary' });
                                        } else {
                                            var arr = fixdata(data);
                                            wb = X.read(btoa(arr), { type: 'base64' });
                                        } console.log("!");
                                        process_wb(wb);
                                    }
                                };
                                if (rABS) reader.readAsBinaryString(f);
                                else reader.readAsArrayBuffer(f);
                            }
                        }
                    } else {
                        addnotify("notify", "Extensión de archivo inválido", "registeruser");
                    }
                } else {
                    addnotify("notify", "Seleccione un archivo", "registeruser");
                }
            });
            $("#btnCarga").click(function () {
                var tipoCarga = $('input[name=tipoCarga]:checked').val();
                //alert(tipoCarga);
                var xlf = document.getElementById('xlf');
                rABS = true;
                use_worker = true;
                var files = xlf.files;
                if (files.length > 0) {
                    var f = files[0];
                    if (tipoCarga == "E") {
                        var ext = f.name.split(".").pop().toLowerCase();
                        if ($.inArray(ext, ['xls', 'xlsx']) == -1) {
                            addnotify("notify", "Extensión de archivo inválido", "registeruser");
                        } else {
                            $("#cz-form-step-image-text-center").addClass("pasos");
                            $("#paso2").addClass("pasos");
                            {
                                var reader = new FileReader();
                                var name = f.name;
                                reader.onload = function (e) {
                                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                                    var data = e.target.result;
                                    if (use_worker) {
                                        xw(data, process_wb);
                                    } else {
                                        var wb;
                                        if (rABS) {
                                            wb = X.read(data, { type: 'binary' });
                                        } else {
                                            var arr = fixdata(data);
                                            wb = X.read(btoa(arr), { type: 'base64' });
                                        }
                                        process_wb(wb);
                                    }
                                };
                                if (rABS) reader.readAsBinaryString(f);
                                else reader.readAsArrayBuffer(f);
                            }
                        }
                    } else {
                        addnotify("notify", "Extensión de archivo inválido", "registeruser");
                    }
                } else {
                    addnotify("notify", "Seleccione un archivo", "registeruser");
                }
            });
            $("#saveReg").click(function () {
                var result = validarMapeo();
                if (result) {
                    var strData = new Object();
                    var strColums = new Object();
                    
                    strColums.idtabla = $("#ddlTabla").val();
                    strColums.columna = [];
                    $(".filColumn").each(function (data) {
                        var strData = new Object();
                        strData.idColumnaxls = data;
                        strData.columnaxls = $(this).find("td").html();
                        strData.idColumna = $(this).find(".column").val();
                        strColums.columna.push(strData);
                    });

                    $.ajax({
                        type: 'POST',
                        url: "MapeoFormato.aspx/GuardarMapeo",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strColums),
                        beforeSend: function () {
                        },
                        success: function (result) {
                            console.log(result);
                            var res = $.parseJSON(result.d)
                            if (res.tipo=="1") {
                                $('#myModal').modal('hide');
                            }
                            addnotify("notify", res.mensaje, "registeruser");
                            //localStorage.setItem("columnasTabla2", $.parseJSON(result.d));
                        },
                        error: function (xhr, status, error) {
                        }
                    });
                }
            });
        });
        function buscarColumna(cbo) {
            var strData = new Object();
            strData.idColumna = cbo.val();
            $.ajax({
                type: 'POST',
                url: "MapeoFormato.aspx/ListarMaestroTablaColumnasId",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#xlf").prop('disabled', true);
                },
                success: function (result) {
                    var dato = $.parseJSON(result.d);
                    var formato = dato.formato +" "+ (dato.obligatorio == "T" ? "*" : "");
                    //JSON.stringify(result)
                    cbo.parent().parent().find(".lblColumn").html(formato);
                    $("#xlf").prop('disabled', false);
                },
                error: function (xhr, status, error) {
                    addnotify("notify", error, "registeruser");
                    $("#xlf").prop('disabled', false);
                }
            });
        };
        function validarMapeo() {
            var valido = true;
            var columnasTabla = $.parseJSON(sessionStorage.getItem("columnasTabla"));
            var strColums = [];
            $(".filColumn").each(function (data) {
                var strData = new Object();
                strData.idColumnaxls = data;
                strData.idColumna = $(this).find(".column").val();
                strColums.push(strData);
            });
            if (strColums.length<=0) {
                addnotify("notify", "No se existen columnas del archivo.", "registeruser");
            }
            if (ObjectLength(Object.filter(strColums, x => x.idColumna != "")) <= 0) {
                valido = false;
                addnotify("notify", "No se seleccionó ningun campo.", "registeruser");
            }
            /*Validar que lo seleccionado no se repita*/
            var repetido = false;
            if (columnasTabla) {
                columnasTabla.forEach(function (data) {
                    var filtered = Object.filter(strColums, x => x.idColumna == data.id);
                    if (ObjectLength(filtered) > 1) {
                        repetido = true;
                    }

                });

                /*Validar que esten seleccionado los obligatorios*/
                var noObligatorio = false;
                var noObligatorioVal = "";
                var filObligatorios = Object.filter(columnasTabla, x => x.obligatorio == "T");
                for (var prop in filObligatorios) {
                    // skip loop if the property is from prototype
                    if (!filObligatorios.hasOwnProperty(prop)) continue;
                    var columObl = filObligatorios[prop];
                    var fil = Object.filter(strColums, x => x.idColumna == columObl.id);
                    if (ObjectLength(fil) < 1) {
                        noObligatorio = true;
                        noObligatorioVal += " \n" + columObl.columna + ",";
                    }

                }
                if (noObligatorio) {
                    valido = false;
                    addnotify("notify", "No se han seleccionado los campos obligatorios: " + noObligatorioVal, "registeruser");
                }
            } else {
                valido = false;
                addnotify("notify", "No se a seleccionado la tabla.", "registeruser");
            }
            
            if (repetido) {
                valido = false;
                addnotify("notify", "Existen selecciones de campos repetidas.", "registeruser");
            }

            
            return valido;
        }
        function tablaMapeo(contenedor) {
            var columnasTabla = $.parseJSON(sessionStorage.getItem("columnasTabla"));
            var columnasXls = $.parseJSON(sessionStorage.getItem("columnasXls"));
            var control = '';//'<tr><td>CAMPOS DEL ARCHIVO</td>     <th>CAMPOS DE PEDIDOS</td>     <td>FORMATOS DEL CAMPO</td></tr>';
            $(contenedor).html("");
            if (columnasXls) {
                columnasXls.forEach(function (data) {
                    control += '<tr id ="' + data.codigo + '" class="filColumn">';
                    control += '<td>' + data.nombre;
                    control += '</td>';
                    control += '<td>';
                    control += '<select id="ddlColumna' + data.codigo + '" class="column cz-form-content-input-select">';
                    control += '</select>';
                    control += '<div class="cz-form-content-input-select-visible">';
                    control += '<p class="cz-form-content-input-select-visible-text"></p>';
                    control += '<div class="cz-form-content-input-select-visible-button">';
                    control += '</div>';
                    control += '</div>';

                    control += '</td>';
                    control += '<td>';
                    control += '<label class="lblColumn"></label>';
                    control += '</td>';
                    control += '</tr>';
                });
            }

            $(contenedor).append(control);
            if (columnasTabla) {
                $(".column").change(function () {
                    buscarColumna($(this));
                    $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                });
                $(".column").append($("<option selected ></option>").attr("value", "").text("Seleccione"));
                columnasTabla.forEach(function (data) {
                    $(".column").append($("<option></option>").attr("value", data.id).text(data.columna));
                });
                $(".column").parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());
                $(".column").change();
                //$(this).val("");
                //    //$(this).change();
                //$(".column").each(function () {
                //    var ddlCol = $(this);
                //    ddlCol.append($("<option></option>").attr("value", "").text("Seleccione"));
                //    columnasTabla.forEach(function (data) {
                //        ddlCol.append($("<option></option>").attr("value", data.id).text(data.columna));
                //    });
                //    $(this).parent().find(".cz-form-content-input-select-visible-text").html($(this).find("option:selected").text());

                //    //$(this).val("");
                //    //$(this).change();
                //});
            }
        };
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×</button>
            <h3 id="myModalLabel" runat="server">Mapear Formato...</h3>
        </div>
        <div id="myModalContent" class="modal-body" style="height: 500px;">
            <div class="cz-form-box-content">
                <div class="cz-form-content">
                    <label for="ddlTabla">Tabla</label>
                    <%--<label for="ddlTabla">Tabla</label>--%>
                    <asp:DropDownList ID="ddlTabla" runat="server" CssClass="cz-form-content-input-select">
                    </asp:DropDownList>
                    <div class="cz-form-content-input-select-visible">
                        <p class="cz-form-content-input-select-visible-text">
                        </p>
                        <div class="cz-form-content-input-select-visible-button">
                        </div>
                    </div>
                </div>
                <div class="cz-form-content">
                    <input type="file" name="xlfile" id="xlf" class="inputfile inputfile-2" />
                    <label for="xlf" class="cz-form-content-input-button">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" style="color: white;" />
                        </svg>
                        <span style="color: white;">Seleccione Archivo</span></label>
                    <select name="format" style="display: none;">
                        <option value="json" selected="selected">JSON</option>
                    </select>

                    <%--<input type="file" name="xlfile" id="xlf" />
                    <input type="button" id="buscar" class="cz-form-content-input-button cz-form-content-input-button-image"
                        value="Seleccione Archivo">--%>
                </div>
            </div>
            <div class="cz-form-box-content">
                <div id="divGridView" runat="server">
                    <asp:Literal runat="server" ID="litGrilla"></asp:Literal>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id="cancelReg" class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            <input type="button" id="saveReg" class="form-button cz-form-content-input-button" value="Guardar" />
        </div>
        <asp:HiddenField ID="MhAccion" runat="server" />
    </form>
    <div id="calendarwindow" class="calendar-window">
    </div>
    <input runat="server" type="hidden" id="idpedido" value="0" />

</body>
</html>
