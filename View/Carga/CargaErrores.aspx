﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Carga.Carga_CargaErrores" Codebehind="CargaErrores.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h3 id="myModalLabel" runat="server"></h3>
            </div>
            <div id="myModalContent" class="modal-body">
                <span id="label1" runat="server"></span>
                <br />
                <div style="overflow: auto; height: 450px;">
                    <asp:GridView ID="GridView1" runat="server" Width="100%" CssClass="grilla table table-bordered table-striped"
                        OnRowDataBound="GridView1_RowDataBound">
                    </asp:GridView>

                </div>
                <div class="alert fade" id="divError">
                    <strong id="tituloMensajeError"></strong>
                    <p id="mensajeError">
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button id="cancelReg" class="form-button cz-form-content-input-button" data-dismiss="modal"
                    aria-hidden="true">
                    CERRAR</button>
            </div>
        </div>
    </form>
</body>
</html>
