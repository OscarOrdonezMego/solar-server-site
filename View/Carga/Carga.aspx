﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Carga.Carga_Carga" CodeBehind="Carga.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title></title>
    <link href="../css/FileTemplate.css" type="text/css" rel="stylesheet" />
    <link href="../css/FileUploader.css" type="text/css" rel="stylesheet" />
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/alertHtml.ashx" type="text/javascript"></script>
    <script src="../js/JSModule.js" type="text/javascript"></script>
    <script src="../js/upload/fileuploader.js" type="text/javascript"></script>
    <link href="../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../js/cz_main.js" type="text/javascript"></script>

    <%-- CARGA --%>
    <script src="../js/carga/shim.js" type="text/javascript"></script>
    <script src="../js/carga/jszip.js" type="text/javascript"></script>
    <script src="../js/carga/xlsx.js" type="text/javascript"></script>
    <script src="../js/carga/carga.js" type="text/javascript"></script>
    <style>
        .modal-body ul {
            list-style: none;
        }
    </style>
    <script>

        $(document).ready(function () {
            $(".map-formt").hide();
            $('#chkFamilia').click(function () {
                if ($(this).is(':checked')) {
                    addnotify("notify", "Todos los productos asignados al maetros de familias se eliminaran", "chkFamilianotify");
                }
            });
            $('#chkMarca').click(function () {
                if ($(this).is(':checked')) {
                    addnotify("notify", "Todos los productos asignados al maetros de Marcas se eliminaran", "chkFamilianotify");
                }
            });

            accionesCarga();
            
            detReg(".btnData", "cargaErrores.aspx");
            detReg(".map-formt", "MapeoFormato.aspx");
            $('.map-formt').live('click', function (e) {
                
            });

            $('#rblTipoCarga_0').live('click', function (e) {
                $(".map-formt").hide();
                var uploader = new qq.FileUploader({
                    element: document.getElementById('file-uploader'),
                    action: 'Upload.ashx',
                    debug: false,
                    sizeLimit: <%= System.Configuration.ConfigurationManager.AppSettings["MAXSIZE_TXT"].ToString() %>,
                    allowedExtensions: ['txt'],
                    template: $("#file-upload-template").html(),
                    fileTemplate: '<li> ' +
                        '		<div class="file-info-space"> ' +
                        '    		<div class="file-info-box"> ' +
                        '				<div class="file-info-border"> ' +
                        '           		<div class="file-info-name"><span class="qq-upload-file"></span></div> ' +
                        '               	<div class="file-info-percentage"><span class="qq-upload-spinner"></span></div> ' +
                        '               	<div class="file-info-size"><span class="qq-upload-size"></span></div> ' +
                        '               	<div class="file-info-name"><a class="qq-upload-cancel" href="#">Cancel</a></div> ' +
                        '               	<div class="file-info-name"><span class="qq-upload-failed-text">Failed</span></div> ' +
                        '          	</div> ' +
                        '			</div> ' +
                        '		</div> ' +
                        '</li> ',

                    onSubmit: function (id, fileName) {

                        //  console.log('submit');
                    },
                    onProgress: function (id, fileName, loaded, total) {
                        // console.log('onProgress' + total);
                        $('#cz-carga-siguiente').hide();
                    },

                    onComplete: function (id, file, responseJSON) {
                        //  console.log('onComplete');
                        $(this.element).find('.qq-upload-list li').each(function () {
                            if ($(this).attr('qqFileId') == id) {
                                $(this).find('.file-info-space').attr('id', "fsp-" + str_validate(file));
                                $(this).find('.file-info-box').attr('id', "fbx-" + str_validate(file));
                                $(this).find('.file-info-border').attr('id', "fbd-" + str_validate(file));
                                //    $(this).find('.file-info-box').append("<div class='boton-ejecutar' id='exc-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "'><img src='../images/icons/carga/ico_carga_exc.png' /></div>");
                                $(this).find(".file-info-box").append("<div class='boton-borrar' id='del-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "' error='no'><img src='../images/icons/carga/ico_carga_del.png' /></div>");
                                $('.boton-ejecutar').show();
                                $('#cz-carga-siguiente').show();
                            }
                        });

                    }
                });
            });

            $('#rblTipoCarga_1').live('click', function (e) {
                $(".map-formt").show();
                var uploader = new qq.FileUploader({
                    element: document.getElementById('file-uploader'),
                    action: 'Upload.ashx',
                    debug: false,
                    sizeLimit: <%= System.Configuration.ConfigurationManager.AppSettings["MAXSIZE_XLS"].ToString() %>,
                    allowedExtensions: ['xls', 'xlsx'],
                    template: $("#file-upload-template").html(),
                    fileTemplate: '<li> ' +
                        '		<div class="file-info-space"> ' +
                        '    		<div class="file-info-box"> ' +
                        '				<div class="file-info-border"> ' +
                        '           		<div class="file-info-name"><span class="qq-upload-file"></span></div> ' +
                        '               	<div class="file-info-percentage"><span class="qq-upload-spinner"></span></div> ' +
                        '               	<div class="file-info-size"><span class="qq-upload-size"></span></div> ' +
                        '               	<div class="file-info-name"><a class="qq-upload-cancel" href="#">Cancel</a></div> ' +
                        '               	<div class="file-info-name"><span class="qq-upload-failed-text">Failed</span></div> ' +
                        '          	</div> ' +
                        '			</div> ' +
                        '		</div> ' +
                        '</li> ',
                    onSubmit: function (id, fileName) {

                          console.log('submit');
                        // $('#cz-carga-siguiente').hide();
                    },
                    onProgress: function (id, fileName, loaded, total) {
                         console.log('onProgress' + total);
                        $('#cz-carga-siguiente').hide();
                    },
                    onComplete: function (id, file, responseJSON) {
                        console.log('onComplete');
                        $(this.element).find('.qq-upload-list li').each(function () {
                            if ($(this).attr('qqFileId') == id) {
                                $(this).find('.file-info-space').attr('id', "fsp-" + str_validate(file));
                                $(this).find('.file-info-box').attr('id', "fbx-" + str_validate(file));
                                $(this).find('.file-info-border').attr('id', "fbd-" + str_validate(file));
                                //    $(this).find('.file-info-box').append("<div class='boton-ejecutar' id='exc-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "'><img src='../images/icons/carga/ico_carga_exc.png' /></div>");

                                $(this).find(".file-info-box").append("<div class='boton-borrar' id='del-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "' error='no'><img src='../images/icons/carga/ico_carga_del.png' /></div>");
                                $('.boton-ejecutar').show();
                                $('#cz-carga-siguiente').show();
                            }
                        });

                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="cz-submain cz-submain-form-background">
            <div id="cz-form-box">
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../imagery/all/icons/config.png" alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p>
                                Carga de Archivos
                            </p>

                            <div>
                                <span>Tipo Archivo</span>
                                <asp:RadioButtonList ID="rblTipoCarga" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow">
                                    <asp:ListItem Value="T" Selected="True" CssClass="carga_label">Texto Plano (.txt)</asp:ListItem>
                                    <asp:ListItem Value="E" CssClass="carga_label">Excel(.xls / .xlsx)</asp:ListItem>
                                </asp:RadioButtonList>
                                <input type="button" id="cz-carga-siguiente" class="map-formt cz-form-content-input-button-image cz-form-content-input-button" value="Mapear Formato">
                            </div>
                        </div>
                    </div>
                </div>

                <!--ELIMINAR DATA ANTERIOR-->
                <div class="cz-form-box-content">
                    <div id="cz-form-box-content-title">
                        <img id="cz-form-box-content-title-icon" src="../imagery/all/icons/remove.png" alt="<>" />
                        <div id="cz-form-box-content-title-text">
                            <p>Eliminar antes de cargar</p>
                        </div>
                        </br></br></br>
                        <div class="contenedorCheck" id="contenedorFiles">
                            <asp:Literal ID="ltrLstCheckDeleteFiles" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>

                <div class="cz-form-box-content">
                    <div id="file-uploader">
                    </div>
                    <div id="cz-form-step">
                        <div id="cz-form-step-image">
                            <div id="cz-form-step-image-text">
                                <div id="cz-form-step-image-text-left" class="cz-form-step-image-complete">
                                    <a>Paso 1: Examinar Archivos</a>
                                </div>
                                <div id="cz-form-step-image-text-center">
                                    <a>Paso 2: Confirmar Archivos</a>
                                </div>
                                <div id="cz-form-step-image-text-right">
                                    <a>Paso 3: Resultado</a>
                                </div>
                            </div>
                        </div>
                        <div id="cz-form-step-image-progress-examinar">
                            <div class="cz-form-step-image-progress-title">
                                Examinar archivos a la lista.
                            </div>
                            <div id="cz-form-step-image-progress-examinar-content">
                                <div class="cz-form-step-image-progress-examinar-content-row-head">
                                    <div class="cz-form-step-image-progress-examinar-content-row-name">
                                        Nombre
                                    </div>
                                    <div class="cz-form-step-image-progress-examinar-content-row-size">
                                        Tamaño
                                    </div>
                                    <div class="cz-form-step-image-progress-examinar-content-row-option">
                                        <div class="cz-form-step-image-progress-examinar-content-row-option-remove">
                                        </div>
                                    </div>
                                </div>
                                <div id="cz-form-step-files">
                                </div>
                                <div class="cz-form-step-image-progress-examinar-content-row-file">
                                    <div class="cz-form-step-image-progress-examinar-content-row-name">
                                    </div>
                                    <div class="cz-form-step-image-progress-examinar-content-row-size">
                                    </div>
                                    <div class="cz-form-step-image-progress-examinar-content-row-option">
                                        <div class="cz-form-step-examinar cz-form-step-image-progress-examinar-content-row-option-add cz-form-content-input-button">
                                            +
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cz-form-content-wall">
                        <input type="button" onclick="nextconfirm();" id="cz-carga-siguiente" class="siguiente-confirm cz-form-content-input-button-image cz-form-content-input-button"
                            value="Siguiente" />
                    </div>
                    <div class="cz-form-content-wall" id="siguiente-box-update">
                        <input type="button" onclick="nextupload()" class="cz-form-content-input-button boton-eject"
                            value="Ejecutar" />
                    </div>
                    <div id="cz-form-table">
                        <div class="form-grid-table-outer">
                            <div class="form-grid-table-inner">
                                <div class="form-gridview-data" id="divGridViewData">
                                </div>
                                <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                    <img src="../images/icons/loader/ico_loader-arrow-orange.gif" />
                                    <p>
                                        obteniendo resultados
                                    </p>
                                </div>
                                <div class="divUploadNew" style="display: none; text-align: center;">
                                    <input type="button" onclick="newupload()" class="form-button cz-form-content-input-button"
                                        value="Nueva Carga" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="file-upload-template" style="display: none">
                        <div id="file-boton-box" class="qq-uploader">
                            <div class="qq-upload-button">
                            </div>
                            <div class='boton-ejecutar' style="display: none; position: relative; top: -22px; left: 240px;">
                                <img src='../images/icons/carga/ico_carga_exc.png' />
                            </div>
                        </div>
                        <div id="file-upload-panel">
                            <ul class="qq-upload-list">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cz-form-box-content cz-util-nopad">
                    <div class="cz-form-box-content-subtitle cz-util-pad-min">
                        <p>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
    </form>
    <script type="text/javascript">
        function inicializarUploader() {
            var uploader = new qq.FileUploader({
                element: document.getElementById('file-uploader'),
                action: 'Upload.ashx',
                debug: false,
                sizeLimit: <%= System.Configuration.ConfigurationManager.AppSettings["MAXSIZE_TXT"].ToString() %>,
                allowedExtensions: ['txt'],
                template: $("#file-upload-template").html(),
                fileTemplate: '<li> ' +
                    '		<div class="file-info-space"> ' +
                    '    		<div class="file-info-box"> ' +
                    '				<div class="file-info-border"> ' +
                    '           		<div class="file-info-name"><span class="qq-upload-file"></span></div> ' +
                    '               	<div class="file-info-percentage"><span class="qq-upload-spinner"></span></div> ' +
                    '               	<div class="file-info-size"><span class="qq-upload-size"></span></div> ' +
                    '               	<div class="file-info-name"><a class="qq-upload-cancel" href="#">Cancel</a></div> ' +
                    '               	<div class="file-info-name"><span class="qq-upload-failed-text">Failed</span></div> ' +
                    '          	</div> ' +
                    '			</div> ' +
                    '		</div> ' +
                    '</li> ',
                onProgress: function (id, fileName, loaded, total) {
                    // console.log('onProgress' + total);
                    $('#cz-carga-siguiente').hide();
                },
                onComplete: function (id, file, responseJSON) {
                    $(this.element).find('.qq-upload-list li').each(function () {
                        if ($(this).attr('qqFileId') == id) {
                            $(this).find('.file-info-space').attr('id', "fsp-" + str_validate(file));
                            $(this).find('.file-info-box').attr('id', "fbx-" + str_validate(file));
                            $(this).find('.file-info-border').attr('id', "fbd-" + str_validate(file));
                            //    $(this).find('.file-info-box').append("<div class='boton-ejecutar' id='exc-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "'><img src='../images/icons/carga/ico_carga_exc.png' /></div>");
                            console.log(file);
                            $(this).find(".file-info-box").append("<div class='boton-borrar' id='del-" + str_validate(file) + "' idclass='" + str_validate(file) + "' filename='" + file + "' error='no'><img src='../images/icons/carga/ico_carga_del.png' /></div>");
                            $('.boton-ejecutar').show();
                            $('#cz-carga-siguiente').show();
                        }
                    });

                }
            });
        }

        // in your app create uploader as soon as the DOM is ready
        // don't wait for the window to load  
        window.onload = inicializarUploader;

        function str_validate(text) {

            text = text.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
            text = text.replace(/-/gi, "_");
            text = text.replace(/\s/gi, "-");

            return text;
        }

        // acciones

    </script>

</body>
</html>
