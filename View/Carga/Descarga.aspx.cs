﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Controller.functions;
using System.Data;
using Controller;
using Model.bean;
using System.IO;
using Model;
using System.Web.Services;

namespace Pedidos_Site.Carga
{
    public partial class Carga_Descarga : PageController
    {
        public static String liValidarVisible;
        public static Int32 liRespuestaint;
        public static String deshabilitarCboSupervisor;
        public static String codigoSub = "";
        public static String pkusu = "";
        public static String TipoArticulo = "";

        protected override void initialize()
        {
            if (Session["lgn_id"] == null || Session["lgn_codsupervisor"] == null)
            {
                Session.Clear();
                String lsScript = "parent.document.location.href = '../../default.aspx?acc=SES';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", lsScript, true);
            }
            TipoArticulo = "";
            if (fnObtenerConfiguracionPorCodigo(Constantes.CodigoValoresFuncion.COD_FUNCION_FRACIONAMIENTO).Valor.Equals(Enumerados.FlagHabilitado.T.ToString()))
            {
                TipoArticulo = "PRE";
                liRespuestaint = Operation.flagACIVADO.ACTIVO;
                liValidarVisible = Operation.VACIO.TEXT_VACIO;
            }
            else
            {
                liRespuestaint = Operation.flagACIVADO.DESACTIVADO;
                TipoArticulo = "PRO";
                liValidarVisible = Operation.CSS.STYLE_DISPLAY_NONE;
            }

            if (!IsPostBack)
            {
                String pkUsu = "";
                llenarComboBoxPerfil();
                codigoSub = Session["lgn_codsupervisor"].ToString();
                pkusu = Session["lgn_id"].ToString();
                if (codigoSub == "SUP")
                {
                    cboPerfil.SelectedValue = pkusu;
                    pkUsu = pkusu;
                    deshabilitarCboSupervisor = Operation.CSS.STYLE_DISPLAY_NONE;
                }
                else
                {
                    deshabilitarCboSupervisor = "";
                }
                if (cboPerfil.SelectedValue.Equals("-1"))
                {
                    pkUsu = pkusu;
                }
                llenarComboBoxGrupo(pkusu);
                llenarComboBoxVendedor(pkusu);
                llenarCboArchivo();
                this.txtFecini.Text = Utils.getFechaActual();
                this.txtFecfin.Text = Utils.getFechaActual();
            }
        }

        private void llenarCboArchivo()
        {
            DataTable dt = DescargaController.fnListaArchivo();
            if (dt != null && dt.Rows.Count > 0)
            {
                int contador = 0;
                foreach (DataRow dtr in dt.Rows)
                {
                    cboArchivo.Items.Insert(contador, dtr["NombreArchivo"].ToString());
                    cboArchivo.Items[contador].Value = dtr["CodigoArchivo"].ToString();
                    contador++;
                }
            }
            else
            {
                cboArchivo.Items.Insert(0, "Sin Registro");
                cboArchivo.Items[0].Value = "0";
            }
        }

        [WebMethod]
        public static String GuardarConfiguracion(String cabecera, String detalle)
        {
            try
            {
                List<CabeceraDetalleBean> listaCabeceraDetalleBean = Operation.ListaSplitComa(cabecera);
                String ID = Operation.columnas(listaCabeceraDetalleBean, "ID");
                String NOMBRE = Operation.columnas(listaCabeceraDetalleBean, "NOMBRE");
                String HABILITADO = Operation.columnas(listaCabeceraDetalleBean, "HABILITADO");
                String ORDEN = Operation.columnas(listaCabeceraDetalleBean, "ORDEN");
                String[] IDVec = Operation.ListaSplitPalotes(ID);
                String[] NombreVec = Operation.ListaSplitPalotes(NOMBRE);
                String[] HabilitadoVec = Operation.ListaSplitPalotes(HABILITADO);
                String[] OrdenVec = Operation.ListaSplitPalotes(ORDEN);
                CabeceraDetalleBean loCabeceraDetalleBean = null;
                Int32 retorno = 0;
                for (Int32 i = 0; i < IDVec.Length; i++)
                {
                    if (IDVec[i].Length > 0)
                    {
                        loCabeceraDetalleBean = new CabeceraDetalleBean();
                        loCabeceraDetalleBean.ID = IDVec[i].ToString();
                        loCabeceraDetalleBean.Nombre = NombreVec[i].ToString();
                        loCabeceraDetalleBean.Habilitado = HabilitadoVec[i].ToString();
                        loCabeceraDetalleBean.Orden = OrdenVec[i].ToString();
                        retorno = DescargaController.GuardarConfiguracionDescarga(loCabeceraDetalleBean);
                    }
                }
                if (retorno == 1)
                {
                    List<CabeceraDetalleBean> listaCabeceraDetalle_D = Operation.ListaSplitComa(detalle);
                    String ID_D = Operation.columnas(listaCabeceraDetalle_D, "ID");
                    String NOMBRE_D = Operation.columnas(listaCabeceraDetalle_D, "NOMBRE");
                    String HABILITADO_D = Operation.columnas(listaCabeceraDetalle_D, "HABILITADO");
                    String ORDEN_D = Operation.columnas(listaCabeceraDetalle_D, "ORDEN");
                    String[] IDVec_D = Operation.ListaSplitPalotes(ID_D);
                    String[] NombreVec_D = Operation.ListaSplitPalotes(NOMBRE_D);
                    String[] HabilitadoVec_D = Operation.ListaSplitPalotes(HABILITADO_D);
                    String[] OrdenVec_D = Operation.ListaSplitPalotes(ORDEN_D);
                    CabeceraDetalleBean loCabeceraDetalleBean_D = null;

                    for (Int32 i = 0; i < IDVec_D.Length; i++)
                    {
                        if (IDVec_D[i].Length > 0)
                        {
                            loCabeceraDetalleBean_D = new CabeceraDetalleBean();
                            loCabeceraDetalleBean_D.ID = IDVec_D[i].ToString();
                            loCabeceraDetalleBean_D.Nombre = NombreVec_D[i].ToString();
                            loCabeceraDetalleBean_D.Habilitado = HabilitadoVec_D[i].ToString();
                            loCabeceraDetalleBean_D.Orden = OrdenVec_D[i].ToString();
                            DescargaController.GuardarConfiguracionDescarga(loCabeceraDetalleBean_D);
                        }
                    }
                    return "Se Guardo correctamente.";
                }
                else
                {
                    return "No Hay Registro.";
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [WebMethod]
        public static String BuscarNombreColumna(String codArchivo, String tipoColumna)
        {
            String Resultado = "";

            DataTable lodt = DescargaController.fnBuscarNombreColumCabecera(codArchivo, TipoArticulo, tipoColumna);
            if (lodt != null && lodt.Rows.Count > 0)
            {
                foreach (DataRow loDataRow in lodt.Rows)
                {
                    Resultado += ToolBox.DIV("div" + loDataRow["IDColArchivo"].ToString() + "", "sortable_hijo ui-sortable-handle", "<img class=" + "img" + tipoColumna + " src=\"../images/icons/grid/bg.gif\" />" + ToolBox.CheckboxConClass(loDataRow["IDColArchivo"].ToString(), "check" + tipoColumna, loDataRow["NombreColumna"].ToString(), loDataRow["FlgHabilitado"].ToString(), "checkbox") + " " + loDataRow["NombreColumna"]);
                }
            }
            else
            {
                Resultado = "<h3>No hay Registro.</h3>";
            }
            return Resultado;
        }

        private void llenarcoGrupo()
        {
            List<GrupoBean> loLstGrupoBean = GrupoController.fnListarGrupo();
            if (loLstGrupoBean.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
                for (int i = 0; i < loLstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, loLstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = loLstGrupoBean[i].Grupo_PK.ToString();
                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "0";
            }
        }

        private void llenarComboBoxPerfil()
        {
            List<UsuarioBean> lista = UsuarioController.ListaSupervisores();
            if (lista.Count != 0)
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "-1";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboPerfil.Items.Insert(i + 1, lista[i].nombre);
                    cboPerfil.Items[i + 1].Value = lista[i].id;
                }
            }
            else
            {
                cboPerfil.Items.Insert(0, "Todos");
                cboPerfil.Items[0].Value = "-1";
            }
        }

        private void llenarComboBoxGrupo(String codigo)
        {
            List<GrupoBean> lista = UsuarioController.GruposSupervisoresAsignado(Int32.Parse(codigo), codigoSub);
            if (lista.Count != 0)
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "-1";
                for (int i = 0; i < lista.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                    cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();
                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "-1";
            }
        }

        private void llenarComboBoxVendedor(String codigo)
        {
            List<UsuarioBean> lstGrupoBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(codigo));
            if (lstGrupoBean.Count != 0)
            {
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (int i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].id.ToString();
                }
            }
            else
            {
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "-1";
            }
        }

        public static Boolean validarCampos(String txtf1, String txtf2, String txth3, String txth4)
        {
            Boolean flag = Utils.validarFechaCompleta(txtf1);
            if (!flag) return false;
            flag = Utils.validarFechaCompleta(txtf2);
            if (!flag) return false;
            flag = Utils.validarHoraMinuto(txth3);
            if (!flag) return false;
            flag = Utils.validarHoraMinuto(txth4);
            if (!flag) return false;

            return flag;
        }

        protected void btnDescarga_Click(object sender, EventArgs e)
        {
            if (validarCampos(txtFecini.Text, txtFecfin.Text, txtHoraIni.Value, txtHoraFin.Value))
            {
                ltmensaje.Text = "";
                string pFecInicio = Utils.getStringFechaYYMMDDHHMM(txtFecini.Text + ' ' + txtHoraIni.Value);
                string pFecFin = Utils.getStringFechaYYMMDDHHMM(txtFecfin.Text + ' ' + txtHoraFin.Value);
                String pVendedor = cboVendedor.SelectedValue.ToString();
                String pSupervisor = cboPerfil.SelectedValue.ToString();
                String pgrupo = cboGrupro.SelectedValue.ToString();

                DataSet dsDescarga = null;
                String TipoArticulo = "";
                if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
                {
                    TipoArticulo = "PRE";
                }
                else
                {
                    TipoArticulo = "PRO";
                }

                dsDescarga = ReporteController.descargaCabecera(pFecInicio, pFecFin, TipoArticulo, pVendedor, pSupervisor, pgrupo);

                if ((dsDescarga.Tables[0].Rows.Count > 0) || (dsDescarga.Tables[2].Rows.Count > 0)
                    || (dsDescarga.Tables[6].Rows.Count > 0) || (dsDescarga.Tables[7].Rows.Count > 0) || dsDescarga.Tables[8].Rows.Count > 0)
                {
                    ltmensaje.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Su archivo a comenzado a descargar.\", \"download\");};</script>";
                    DownloadFileUtils.generarZipConArchivoTexto(dsDescarga);
                }
                else
                {
                    ltmensaje.Text = "<script>window.onload = function() {addnotify(\"alert\", \"" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_NODATOS_DESCARGAR) + ".\", \"nodatapend\");};</script>";
                    //Response.Redirect("Descarga.aspx?&mensaje=" + mensaje);
                }
            }
            else
            {
                ltmensaje.Text = "<script>window.onload = function() {addnotify(\"alert\", \"" + IdiomaCultura.getMensaje(IdiomaCultura.WEB_ERROR_DESCARGA_DATOS) + "\", \"verifydate\");};</script>";
            }
        }

        private void LLenarGrupoSup(Int32 codigoPer)
        {
            List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(codigoPer, "SUP");
            if (lstGrupoBean.Count > 0)
            {
                cboGrupro.Items.Clear();
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "-1";
                for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = lstGrupoBean[i].Grupo_PK.ToString();
                }

            }
        }

        protected void cboPerfil_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 codigoPer = Int32.Parse(cboPerfil.SelectedValue);
            List<GrupoBean> lstGrupoBean = UsuarioController.GruposSupervisoresAsignado(codigoPer, "SUP");
            if (lstGrupoBean.Count > 0)
            {
                cboGrupro.Items.Clear();
                cboGrupro.Items.Insert(0, "Todos");
                cboGrupro.Items[0].Value = "-1";
                for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboGrupro.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboGrupro.Items[i + 1].Value = lstGrupoBean[i].Grupo_PK.ToString();
                }
            }
            else
            {
                List<GrupoBean> lista = GrupoController.fnListarGrupo();
                if (lista.Count != 0)
                {
                    cboGrupro.Items.Clear();
                    cboGrupro.Items.Insert(0, "Todos");
                    cboGrupro.Items[0].Value = "-1";
                    for (int i = 0; i < lista.Count; i++)
                    {
                        cboGrupro.Items.Insert(i + 1, lista[i].nombre);
                        cboGrupro.Items[i + 1].Value = lista[i].Grupo_PK.ToString();
                    }
                }
            }
        }

        protected void cboGrupro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 codigo = Int32.Parse(cboGrupro.SelectedValue);
            List<VendedorBean> lstGrupoBean = ReporteController.BuscarVendedoresPorGrupo(codigo);
            if (lstGrupoBean.Count > 0)
            {
                cboVendedor.Items.Clear();
                cboVendedor.Items.Insert(0, "Todos");
                cboVendedor.Items[0].Value = "-1";
                for (Int32 i = 0; i < lstGrupoBean.Count; i++)
                {
                    cboVendedor.Items.Insert(i + 1, lstGrupoBean[i].nombre);
                    cboVendedor.Items[i + 1].Value = lstGrupoBean[i].Usu_PK.ToString();
                }
            }
            else
            {
                List<UsuarioBean> lstUsuarioBean = UsuarioController.ListaVendedorPorSupervisor(Int32.Parse(pkusu));
                if (lstUsuarioBean.Count != 0)
                {
                    cboVendedor.Items.Clear();
                    cboVendedor.Items.Insert(0, "Todos");
                    cboVendedor.Items[0].Value = "-1";
                    for (int i = 0; i < lstUsuarioBean.Count; i++)
                    {
                        cboVendedor.Items.Insert(i + 1, lstUsuarioBean[i].nombre);
                        cboVendedor.Items[i + 1].Value = lstUsuarioBean[i].id.ToString();
                    }
                }
            }
        }

        [WebMethod]
        public static string DescargaTxt(String fechaInicio, String fechaFin)
        {
            String Resultado = "";
            string pFecInicio = fechaInicio + " 00:00"; //YYMMDDHHMM
            string pFecFin = fechaFin + " 23:59"; //YYMMDDHHMM
            String pVendedor = "-1";
            String pSupervisor = "-1";
            String pgrupo = "-1";
            DataSet dsDescarga = null;
            String TipoArticulo = "";

            if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            {
                TipoArticulo = "PRE";
            }
            else
            {
                TipoArticulo = "PRO";
            }

            dsDescarga = ReporteController.descargaCabecera(pFecInicio, pFecFin, TipoArticulo, pVendedor, pSupervisor, pgrupo);

            if ((dsDescarga.Tables[0].Rows.Count > 0))
            {
                Resultado = "Se genero los archivos.";
                generarArchivoTexto(dsDescarga);
            }
            else
            {
                Resultado = "No hay datos para descargar";
            }
            return Resultado;
        }

        public static void generarArchivoTexto(DataSet ds)
        {
            DateTime date = DateTime.Now;
            //String tempFolder = date.Date.ToString("yyyyMMdd") + date.Hour + date.Minute + date.Millisecond;
            String downloadPath = HttpContext.Current.Server.MapPath("~") + "\\downloads";

            if (!Directory.Exists(downloadPath))
            {
                Directory.CreateDirectory(downloadPath);
            }

            deleteFiles(downloadPath);

            foreach (DataTable dt in ds.Tables)
            {
                String fileName = dt.TableName;// +"_" + tempFolder;
                TextFileUtils.createTextFile(dt, downloadPath, fileName);
                /*
                if (dt.Rows.Count > 0) {
                    String fileName = dt.TableName;// +"_" + tempFolder;
                    TextFileUtils.createTextFile(dt, downloadPath, fileName);
                    //TextFileUtils.appendToTextFile(dt, downloadPath, fileName);
                } 
                */
            }
        }

        public static void deleteFiles(String path)
        {
            string[] fileNames = Directory.GetFiles(path);
            foreach (string file in fileNames)
            {
                if (!file.Contains("TextFile.txt"))
                {
                    File.Delete(file);
                }
            }
        }
    }
}