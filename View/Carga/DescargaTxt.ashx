﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Controller;
using Controller.functions;
using Model;

public class DescargaTxt : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        HttpRequest request = context.Request;

        string fechaInicio = context.Request.Params["fechaInicio"];
        string fechaFin = context.Request.Params["fechaInicio"];

        String Resultado = "";
        string pFecInicio = fechaInicio + " 00:00"; //YYMMDDHHMM
        string pFecFin = fechaFin + " 23:59"; //YYMMDDHHMM
        String pVendedor = "-1";
        String pSupervisor = "-1";
        String pgrupo = "-1";
        DataSet dsDescarga = null;
        String TipoArticulo = "";

        Int32 liRespuestaint = 0;

        if (liRespuestaint == Constantes.CodigoValoresFuncion.FRACIONAMIENTO_ACTIVADO)
            TipoArticulo = "PRE";
        else
            TipoArticulo = "PRO";

        dsDescarga = ReporteController.descargaCabecera(pFecInicio, pFecFin, TipoArticulo, pVendedor, pSupervisor, pgrupo);

        if ((dsDescarga.Tables[0].Rows.Count > 0))
        {
            Resultado = "Se genero los archivos.";
            generarArchivoTexto(dsDescarga);
        }
        else
        {
            Resultado = "No hay datos para descargar";
        }

        context.Response.Write("{d:\"" + Resultado + "\"}");
        context.Response.End();
    }

    private static void generarArchivoTexto(DataSet ds)
    {
        DateTime date = DateTime.Now;
        String downloadPath = HttpContext.Current.Server.MapPath("~") + "\\downloads";

        if (!Directory.Exists(downloadPath))
            Directory.CreateDirectory(downloadPath);

        deleteFiles(downloadPath);

        foreach (DataTable dt in ds.Tables)
        {
            String fileName = dt.TableName;
            TextFileUtils.createTextFile(dt, downloadPath, fileName);
        }
    }

    private static void deleteFiles(String path)
    {
        string[] fileNames = Directory.GetFiles(path);
        foreach (string file in fileNames)
        {
            if (!file.Contains("TextFile.txt"))
            {
                File.Delete(file);
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}