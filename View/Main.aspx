﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Main" Codebehind="Main.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TGPTPVM');</script>
    <!-- End Google Tag Manager -->
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title>
        <%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TITULO)%>
    </title>
    <%--<link rel="shortcut icon" href="images/icons/favicon.ico" />--%>
    <link rel="shortcut icon" href="<%="images/icons/shortcuticon" + Controller.GeneralController.obtenerTemaActual(true) + ".ico"%>" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <link href="js/multi-selected/css/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#nservices').parent().click(function (e) {
                unclick();
                var data = [$.cookie('usr') + '@' + $.cookie('psw')];
                var target = ''
                <% if (!nServicesEsEmbebido())
        {
                %>
                target = "_blank";
                <% } %>
                postData('<%=(nServicesExisteConfiguracion() ? System.Configuration.ConfigurationManager.AppSettings["NSERVICES_INSTANCIA"].ToString() : "") %>', data, target);
                e.preventDefault();
                return false;
            });
        });

        function postData(url, data, target) {
            console.log(data);
            console.log(url);
            console.log(target);
            var form = $('<form></form>');
            $(form).hide().attr('method', 'post').attr('action', url);
            if (target != '') {
                $(form).attr('target', target);
            }
            for (i in data) {
                var input = $('<input type="hidden" />').attr('name', i).val(data[i]);
                $(form).append(input);
            }
            $(form).appendTo('body').submit();
        }
    </script>
    <style>
        .containerIcon {
            display: inline-block;
            cursor: pointer;
            color: white;
            display: inline-block;
            font-size: 30px;
        }

        .bar1, .bar2, .bar3 {
            width: 35px;
            height: 5px;
            background-color: #FFF;
            margin: 6px 0;
            transition: 0.5s;
        }

        .change .bar1 {
            /*-webkit-transform: rotate(-45deg) translate(-9px, 6px);
            transform: rotate(-45deg) translate(-9px, 6px);*/
            -webkit-transform: rotate(180deg) translate(0px, 0px);
            transform: rotate(180deg) translate(0px, 0px);
        }

        .change .bar2 {
            /*opacity: 0;*/
            -webkit-transform: rotate(180deg) translate(0px, 0px);
            transform: rotate(180deg) translate(0px, 0px);
        }

        .change .bar3 {
            /*-webkit-transform: rotate(45deg) translate(-8px, -8px);
            transform: rotate(45deg) translate(-8px, -8px);*/
            -webkit-transform: rotate(180deg) translate(0px, 0px);
            transform: rotate(180deg) translate(0px, 0px);
        }
        #img_logo_top{
            margin-left: -5px;
            width: 55px;
            background-size: 140px 30px;
            background-repeat: no-repeat;
            background-image: url(images/logo/login_logoEntel_2.png);
        }
        #img_logo_top .img_logo_topchange{
            width: 150px;
            background-image: url(images/logo/login_logoEntel_2.png);
        }
    </style>
    <script>
        function myFunction(x) {

            x.classList.toggle("change");
        }
    </script>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TGPTPVM"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <form id="form1" runat="server" class="cz-main">
        <div class="cz-submain">
            <div id="cz-box-header2">
                <div id="cz-barr-top">
                    <div id="cz-control-top" class="cz-box-center-width">
                        <div id="img_logo_top">
                        </div>
                        <div id="containerIcon" class="containerIcon"
                            onclick="myFunction(this)">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                        <div id="cz-control-options">
                            <div id="cz-control-user">
                                <div id="cz-control-user-avatar" class="cz-control-user-option">
                                </div>
                                <div class="cz-control-user-option">
                                    <asp:Label ID="lbNomUsuario" runat="server" Text=""></asp:Label>
                                </div>
                                <div id="cz-control-menu" class="cz-control-user-option">
                                    <div id="cz-control-menu-arrow">
                                    </div>
                                    <div id="cz-control-menu-options">
                                        <div class="cz-control-menu-option">
                                            <a href="inicio.aspx">Inicio</a>
                                        </div>
                                        <%if (nServicesExisteConfiguracion())
                                            { %>
                                        <div class="cz-control-menu-option parent">
                                            <a id="nservices">
                                                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_NSERVICES) %>
                                            </a>
                                        </div>
                                        <%} %>

                                        <div class="cz-control-menu-option parent">
                                            <a id="versionApp">Versión</a>
                                        </div>
                                        <div class="cz-control-menu-option parent">
                                            <asp:LinkButton ID="opcSalir" runat="server" OnClick="opcSalir_Click">Salir</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div id="cz-barr-principal">
                    <div id="cz-principal-menu-options">
                        <div id="cz-principal-menu-option-inicio" class="cz-principal-menu-option">
                            <div class="cz-principal-menu-option-button">
                                <div class="cz-principal-menu-option-title link">
                                    <div class="cz-principal-menu-option-image">
                                    </div>
                                    <a href='inicio.aspx'>Inicio</a>
                                </div>
                            </div>
                        </div>
                        <%--<asp:Literal ID="MenuTop" runat="server"></asp:Literal>--%>
                    </div>
                </div>--%>
            </div>
            <%--<div id="cz-util-hidden-top" class="cz-util-hidden cz-util-hidden-height cz-util-hidden-bottom">
                <div class="cz-util-hidden-arrow">
                </div>
            </div>--%>
            <div id="cz-box-body2">
                <div id="cz-menu-lateral-left" class="cz-menu-lateral cz-menu-lateral-left cz-menu-lateral-hide iconView" style="width: 45px;">
                    <div id="cz-menu-lateral-options">
                        <asp:Literal ID="MenuLateral" runat="server"></asp:Literal>
                    </div>
                </div>
                <%--<div id="cz-util-hidden-left" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-right">
                    <div class="cz-util-hidden-arrow">
                    </div>
                </div>--%>
                <div id="cz-box-content" style="left: 45px;">
                    <iframe name="centerFrame" id="centerFrame" width="100%" height="100%" scrolling="yes"
                        frameborder="0"></iframe>
                </div>
            </div>
            <div id="cz-box-footer2">
                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE_P, Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERADOR + Controller.GeneralController.obtenerTemaActual(true)))%>
                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE_BR)%>
            VS <%= ConfigurationManager.AppSettings["VERSION"]%>
                <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE2_BR)%>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        </div>
        <div id="cz-background">
        </div>
        <div id="cz-alert">
            <div id="cz-alert-header">
                <div id="cz-alert-header-title">
                    Mensaje
                </div>
                <div id="cz-alert-close">
                    ×
                </div>
            </div>
            <div id="cz-alert-content">
                Mensaje de prueba.
            </div>
        </div>
    </form>
</body>
</html>
