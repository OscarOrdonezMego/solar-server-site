﻿using System;
using System.Web;
using System.Web.UI;
using Model.bean;
using Controller;
using Controller.functions;
using System.Web.Security;

namespace Pedidos_Site
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String accion = Request["acc"];

            if (accion == null)
            {
                accion = "";
            }

            if (accion.Equals("SES"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Su sesion ha expirado, ingrese nuevamente.\", \"usernregister\");};</script>";
            }

            if (accion.Equals("EXT"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ha cerrado sesión satistactoriamente.\", \"usernregister\");};</script>";
            }

            if (!this.IsPostBack)
            {

                IdiomaCultura.datos = GeneralController.getCultureIdioma(IdiomaCultura.TIPO_WEB);
            }
            btnIngresar.Text = IdiomaCultura.getMensaje(IdiomaCultura.WEB_INGRESAR);

            txtUsuario.Focus();
            GeneralController.subInicializarConfiguracion();

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                String login = this.txtUsuario.Text;
                String clave = this.txtClave.Text;
                String pwcrypt = FormsAuthentication.HashPasswordForStoringInConfigFile(clave, "sha1");

                UsuarioBean loUsuarioBean = UsuarioController.validarUsuario(login, pwcrypt);
                txtClave.Focus();
                if (loUsuarioBean.IdResultado.Equals("1"))
                {
                    Session[SessionManager.THEME_SESSION] = GeneralController.obtenerTemaActual();
                    Session[SessionManager.USER_SESSION] = loUsuarioBean;

                    HttpCookie usr = new HttpCookie("usr");
                    usr.Value = login;
                    HttpCookie psw = new HttpCookie("psw");
                    psw.Value = clave;

                    Response.Cookies.Add(usr);
                    Response.Cookies.Add(psw);


                    Session["lgn_id"] = loUsuarioBean.id;
                    Session["lgn_codigo_usu"] = loUsuarioBean.CodigoUsu;
                    Session["lgn_codigo"] = loUsuarioBean.codigo;
                    Session["lgn_login"] = loUsuarioBean.nombre;
                    Session["lgn_nombre"] = loUsuarioBean.nombre;
                    Session["lgn_perfil"] = loUsuarioBean.perfil;
                    Session["lgn_codsupervisor"] = loUsuarioBean.codSupervisor;
                    Session["lgn_perfilmenu"] = loUsuarioBean.hashRol;
                    Session["lgn_configuracion"] = loUsuarioBean.hashConfiguracion;
                    Session["lgn_Codgrupo"] = loUsuarioBean.Codgrupo;
                    Response.Redirect("Main.aspx");
                }
                else
                {
                    this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"" + IdiomaCultura.getMensajeEncodeHTML(IdiomaCultura.WEB_ERROR) + "\", \"usernregister\");};</script>";
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ExceptionUtils.getHtmlErrorPage(ex));
                HttpContext.Current.Response.End();
            }

        }
    }
}