﻿using System;

namespace Pedidos_Site
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String Error = Request.QueryString["aspxerrorpath"];

            if (Error != null)
            {
                lterror.Text = "en la siguiente ruta: " + Error.ToString();
            }
        }
    }
}