﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.Error" Codebehind="Error.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title><%=ConfigurationManager.AppSettings["APP_NAME"] %> | Nextel del Peru S.A.</title>
    <link rel="shortcut icon" href="images/icons/favicon.ico" />
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" class="cz-main" style="background-color: white;">
        <div class="cz-submain">
            <div id="cz-box-header2">
                <div id="cz-barr-top">
                    <div id="cz-control-top" class="cz-box-center-width">
                        <div id="img_logo_top"></div>
                    </div>
                </div>
            </div>
            <div id="cz-box-body2">
                <div id="cz-box-content">
                    <div class="cz-submain">
                        <div id="cz-form-box">
                            <div class="cz-box-part2" style="text-align: center; margin-top: 50px;">
                                <div style="font-size: 20px;">Ups! Lo sentimos ha ocurrido un error.</div>
                                <div id="cz-error">
                                </div>
                            </div>
                            <div class="cz-box-part2" style="margin-top: 50px;">
                                <div id="cz-error-text">
                                    Se produjo un error en el servidor
                                    <asp:Literal ID="lterror" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
