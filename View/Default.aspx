﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site._Default" Codebehind="Default.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--@@001 - HMONTERO 19/10/2012: Parches generados por Cesar herrera para que funcione en IE 6, 7, 8 y 9--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TGPTPVM');</script>
    <!-- End Google Tag Manager -->
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <title><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_TITULO)%></title>
    <%--<link rel="shortcut icon" href="images/icons/favicon.ico"/>--%>
    <link rel="shortcut icon" href="<%="images/icons/shortcuticon" + Controller.GeneralController.obtenerTemaActual() + ".ico"%>" />
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/cz_main.js" type="text/javascript"></script>
    <link href="css/Forms.css" type="text/css" rel="stylesheet" />
    <style>
        #txtUsuario {
            text-transform: none;
        }
    </style>
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TGPTPVM"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <form id="form1" runat="server" class="cz-main" autocomplete="off">
        <div class="cz-submain cz-login-submain">
            <div id="cz-box-header" class="cz-login-box-header">
                <div id="img_logo_top"></div>
            </div>
            <div id="cz-box-body" class="cz-login-box-body">
                <div class="cz-box-part2">
                    <div id="cz-login-text" class="cz-util-center-box">
                        <%--<p>Conectate a Nextel</p>
                        <p>Conectate a tu Mundo.</p>--%>
                        <p><%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ESLOGAN_01, (Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERADOR + Controller.GeneralController.obtenerTemaActual())))%></p>
                        <p><%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_ESLOGAN_02) %></p>

                    </div>
                </div>
                <div class="cz-box-part2">
                    <div id="cz-login-form" class="cz-util-center-box">
                        <div class="cz-login-lat-left">
                            <div class="img"></div>
                        </div>

                        <div class="cz-login-lat-center">
                            <asp:Literal ID="txtmsg" runat="server"></asp:Literal>
                            <div id="cz-login-head">
                                <asp:Image ID="Image1" ImageUrl="~/imagery/login/icons/users.png" runat="server" />
                                <div id="cz-login-title"><%= Model.bean.IdiomaCultura.getMensaje(Model.bean.IdiomaCultura.WEB_BIENVENIDO_H2)%></div>
                            </div>
                            <div id="cz-login-body">
                                <div id="cz-login-user">
                                    <label>Login</label>
                                    <div id="cz-login-input-user" class="cz-login-input">
                                        <asp:TextBox ID="txtUsuario" placeholder="Login" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="cz-login-pass">
                                    <label>Contraseña</label>
                                    <div id="cz-login-input-pass" class="cz-login-input">
                                        <asp:TextBox ID="txtClave" placeholder="Contraseña" TextMode="Password" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="cz-login-footer">
                                <asp:Button ID="btnIngresar" class="cz-util-slow" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
                            </div>
                        </div>
                        <div class="cz-login-lat-right">
                            <div class="img"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="cz-box-footer" class="cz-login-box-footer cz-util-center-text">
                <p>
                    <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE_P, Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_OPERADOR + Controller.GeneralController.obtenerTemaActual()))%>

                    <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE_BR)%> VS <%= ConfigurationManager.AppSettings["VERSION"]%>

                    <%=Model.bean.IdiomaCultura.getMensajeEncodeHTML(Model.bean.IdiomaCultura.WEB_PIE2_BR)%>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
