﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Pedidos_Site.AcercaDe" Codebehind="AcercaDe.aspx.cs" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--@@001 - HMONTERO 19/10/2012: Parches generados por Cesar herrera para que funcione en IE 6, 7, 8 y 9--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet">
    <%--Inicio @@001--%>
    <link href="css/PopupWindow.css" type="text/css" rel="stylesheet" />
    <link href="css/Forms.css" type="text/css" rel="stylesheet" />
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <%--Fin @@001--%>
    <style>
        .about-box {
            display: table;
            padding: 0px;
            width: 340px;
            height: 140px;
        }

        .about-content-top {
            padding: 23px 0px 29px 0px;
            margin: 0px 0px 0px 20px;
            width: 300px;
        }

        .about-content-bottom {
            float: left;
            padding: 5px 0px;
            width: 340px;
            height: 30px;
        }

        .about-content-message {
            float: left;
            margin-top: 6px;
            margin-left: 15px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: bold;
            color: #E05206;
        }

        .about-content-version {
            float: left;
            margin: 0px;
            padding: 0px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #666;
        }

        #about-content-box-footer {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 340px;
            height: 50px;
            background-color: #E05206;
        }

            #about-content-box-footer p {
                float: left;
                margin: 12px 0px 0px 15px;
                padding: 0px;
                color: #FFF;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 22px;
                font-weight: bold;
            }

            #about-content-box-footer img {
                float: right;
                margin-right: 10px;
                margin-top: 2px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="about-box">
            <div class="about-content-top">
                <div id="img_logo_top"></div>
                <div class="about-content-version">version <%=ConfigurationManager.AppSettings["VERSION"] %></div>
            </div>
            <div class="about-content-bottom">
                <div class="about-content-message">Producto v<%=ConfigurationManager.AppSettings["VERSION"] %> - Entel Peru</div>
            </div>
            <div id="about-content-box-footer">
                <p>Acerca de</p>
                <div id="img_logo_top"></div>
            </div>
        </div>
    </form>
</body>
</html>
