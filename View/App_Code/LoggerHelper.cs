﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Configuration;

namespace Pedidos_Site.App_Code
{
    public class LoggerHelper
    {
        // private static Logger logger = LogManager.GetCurrentClassLogger();
        private Logger logger;
        private string logActive;

        public LoggerHelper()
        {
            logActive = ConfigurationManager.AppSettings["LOG_ACTIVO"];
        }

        public void Error(string Tag, Exception e)
        {
            logger = LogManager.GetLogger("ERROR " + Tag + " : ");
            logger.Error(e);
        }

        public void Debug(string Tag, String message)
        {
            logger = LogManager.GetLogger("DEBUG " + Tag + " : ");
            if (logActive == "1")
                logger.Debug(message);
        }

        public void Debug(string Tag, Object objecto)
        {
            logger = LogManager.GetLogger("DEBUG " + Tag + " : ");
            if (logActive == "1")
                logger.Debug(JsonConvert.SerializeObject(objecto));
        }
    }
}