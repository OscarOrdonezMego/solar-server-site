﻿using Model.bean;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Xml;

public class Util
{
    public static List<Tipo> obtenerTipos()
    {
        String rutaXML = ConfigurationManager.AppSettings["URL_RECURSOS"];
        XmlDocument xml = LeerXmlRemoto(rutaXML);
        List<Tipo> tipos = obtenerTipoAPartirDeXML(xml);
        return tipos;
    }

    private static XmlDocument LeerXmlRemoto(string url)
    {
        WebRequest request = WebRequest.Create(url);
        request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
        WebResponse response = request.GetResponse();
        Stream rssStream = response.GetResponseStream();

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(rssStream);
        return xmlDoc;
    }

    private static List<Tipo> obtenerTipoAPartirDeXML(XmlDocument xml)
    {
        List<Tipo> tipos = new List<Tipo>();
        Tipo tipo;

        List<Recurso> recursos;
        Recurso recurso;

        XmlNodeList nodoTipos = xml.GetElementsByTagName("tipo");
        foreach (XmlElement nodoTipo in nodoTipos)
        {
            tipo = new Tipo();
            tipo.Nombre = nodoTipo.GetAttribute("name");
            recursos = new List<Recurso>();
            XmlNodeList nodoObjetos = nodoTipo.GetElementsByTagName("objeto");
            foreach (XmlElement objeto in nodoObjetos)
            {
                XmlNodeList propiedades = objeto.GetElementsByTagName("propiedad");
                recurso = new Recurso();
                foreach (XmlElement propiedad in propiedades)
                {
                    switch (propiedad.GetAttribute("name"))
                    {
                        case "descripcion":
                            recurso.Descripcion = propiedad.GetAttribute("value");
                            break;
                        case "url":
                            recurso.URL = propiedad.GetAttribute("value");
                            break;
                        case "estado":
                            recurso.Estado = propiedad.GetAttribute("value");
                            break;
                    }
                }
                if (recurso.Estado.Equals(Recurso.ESTADO_ACTIVO)) recursos.Add(recurso);
            }
            tipo.Recursos = recursos;
            tipos.Add(tipo);
        }
        return tipos;
    }
}